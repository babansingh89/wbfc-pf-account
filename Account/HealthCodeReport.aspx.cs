﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;

using System.Web.Script.Services;
using System.Web.Services;


public partial class HealthCodeReport : System.Web.UI.Page
{
    static string StrFormula = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            
            if (!IsPostBack)
            {
                
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    protected void cmdCancelSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string YearMonth, string ChkChanged, string HC, string PWR)
    {
        string JSONVal = "";

        try
        {    
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            String Msg = "";
            StrFormula = "";
            int UserID;
            int SectorID;
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            StrFormula = "";

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, SectorID, ChkChanged, HC, PWR, "", "", "", "", "");

            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    [WebMethod]
    public static string[] HCAutoCompleteData(string YrMonth, string txtHealthCode)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_HealthCode", txtHealthCode, YrMonth, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["HEALTH_CODE"].ToString(), dtRow["HEALTH_CODE"].ToString()));
        }
        return result.ToArray();
    }


    protected DataTable PopulatePWR()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Load_PWRStatus");
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

}