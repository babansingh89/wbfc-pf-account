﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Data.SqlClient;


public partial class LMN_Industry_Code : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
              //  PopulateDropDownlist();

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
               //PopulateDropDownlist();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_Industry_Code_BY_Id", ID);
                Session["ID"] = dtResult.Rows[0]["IndustryCodeID"].ToString();
                dv.DataSource = dtResult;
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlIndustryType")).SelectedValue = dtResult.Rows[0]["IndustryType"].ToString();
                }
                PopulateGrid();

            }
            else if (flag == 2)
            {
                DBHandler.Execute("delete_Industry_code_By_ID", ID);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Industry_Code");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtindustryCode = (TextBox)dv.FindControl("txtindustryCode");
                DropDownList ddlIndustry = (DropDownList)dv.FindControl("ddlIndustryType");
                TextBox txtIndustryCodeDESC = (TextBox)dv.FindControl("txtIndustryCodeDESC");
                TextBox txtAccountCode = (TextBox)dv.FindControl("txtAccountCode");
                //DBHandler.Execute("Insert_Industry_Code", txtindustryCode.Text, ddlIndustry.SelectedValue, txtIndustryCodeDESC.Text, txtAccountCode.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                DBHandler.Execute("Insert_Industry_Code",
                   txtindustryCode.Text.Equals("") ? DBNull.Value : (object)txtindustryCode.Text,
                   ddlIndustry.Text.Equals("") ? DBNull.Value : (object)ddlIndustry.Text,
                   txtIndustryCodeDESC.Text.Equals("") ? DBNull.Value : (object)txtIndustryCodeDESC.Text,
                   txtAccountCode.Text.Equals("") ? DBNull.Value : (object)txtAccountCode.Text,
                   Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));

                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Inserted Successfully.')</script>");
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();

               

            }
            else if (cmdSave.CommandName == "Edit")
            {

                TextBox txtindustryCode = (TextBox)dv.FindControl("txtindustryCode");
                string ID = (string)Session["ID"];
          
                DropDownList ddlIndustry = (DropDownList)dv.FindControl("ddlIndustryType");
                TextBox txtIndustryCodeDESC = (TextBox)dv.FindControl("txtIndustryCodeDESC");
                TextBox txtAccountCode = (TextBox)dv.FindControl("txtAccountCode");

                DBHandler.Execute("Update_Industry_COde",ID, txtindustryCode.Text, ddlIndustry.SelectedValue, txtIndustryCodeDESC.Text, txtAccountCode.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                cmdSave.Text = "Save";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                Session["ID"] = null;

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

   //private void PopulateDropDownlist()
   // {
   //     DataTable DtDropdownlist = DBHandler.GetResult("Industry_Type_dropDownList");
   //     DropDownList ddlIndustry = (DropDownList)dv.FindControl("ddlIndustry");
   //     ddlIndustry.DataSource = DtDropdownlist;
   //     ddlIndustry.DataTextField = "IndustryTypeDesc";
   //     ddlIndustry.DataValueField = "IndustryType";
   //     ddlIndustry.DataBind();
   // }
    protected DataTable drpload()
    {
        DataTable dt = DBHandler.GetResult("Industry_Type_dropDownList");
        return dt;
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
        
}