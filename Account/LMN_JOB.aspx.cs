﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Data.SqlClient;

public partial class LMN_JOB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
                //  PopulateDropDownlist();

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
                //PopulateDropDownlist();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_Job_By_ID", ID);
                Session["ID"] = dtResult.Rows[0]["JobID"].ToString();

                dv.DataSource = dtResult;
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlJObCategoryId")).SelectedValue = dtResult.Rows[0]["JobCategoryID"].ToString();
                    //((DropDownList)dv.FindControl("ddljobdescription")).SelectedValue = dtResult.Rows[0]["JobCategoryID"].ToString();
                }
                PopulateGrid();

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_Job_BY_ID", ID);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_JOB");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                DropDownList ddlJObCategoryId = (DropDownList)dv.FindControl("ddlJObCategoryId");
                TextBox txtJobDescription = (TextBox)dv.FindControl("txtJobDescription");
                TextBox txtJobOrder = (TextBox)dv.FindControl("txtJobOrder");
                TextBox txtTimeToCompleteMin = (TextBox)dv.FindControl("txtTimeToCompleteMin");
                DropDownList ddljobdescription = (DropDownList)dv.FindControl("ddljobdescription");
                DBHandler.Execute("Insert_JOB", ddlJObCategoryId.SelectedValue, txtJobDescription.Text, txtJobOrder.Text, txtTimeToCompleteMin.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]),ddljobdescription.SelectedValue);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Inserted Successfully.')</script>");
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();



            }
            else if (cmdSave.CommandName == "Edit")
            {
                DropDownList ddlJObCategoryId = (DropDownList)dv.FindControl("ddlJObCategoryId");
                TextBox txtJobDescription = (TextBox)dv.FindControl("txtJobDescription");
                TextBox txtJobOrder = (TextBox)dv.FindControl("txtJobOrder");
                TextBox txtTimeToCompleteMin = (TextBox)dv.FindControl("txtTimeToCompleteMin");
                DropDownList ddljobdescription = (DropDownList)dv.FindControl("ddljobdescription");
          
                string ID = (string)Session["ID"];

                DBHandler.Execute("Update_JOB", ID, ddlJObCategoryId.SelectedValue, txtJobDescription.Text, txtJobOrder.Text, txtTimeToCompleteMin.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]), ddljobdescription.SelectedValue);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                cmdSave.Text = "Save";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                Session["ID"] = null;

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    protected DataTable drpload()
    {
        DataTable dt = DBHandler.GetResult("JOb_category_dropDownList");
        return dt;
        
    }
    protected DataTable drpload1()
    {
        DataTable dt = DBHandler.GetResult("JOB_descripTion_DropDownLIST");
        return dt;
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}