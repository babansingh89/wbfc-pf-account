﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_Group_Master.aspx.cs" Inherits="LMN_Group_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
                
        $(document).ready(function () {
            $("input").focus(function () {
                $(this).css("background-color", "#cccccc");
            });
            $("input").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

            $("select").focus(function () {
                $(this).css("background-color", "#cccccc");
            });

            $("select").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

            $("#txtoldgroupid").focus();

            $('#txtoldgroupid').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#txtoldgroupid').val().trim() == '') {
                        alert('Please Type Group ID');
                        $('#txtoldgroupid').focus();
                        return false;
                    }
                    else {
                        DuplicateCheckGroupID();
                        return false;
                    }
                }
            });


            $('#txtoldgroupid').keydown(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 9) {
                    if ($('#txtoldgroupid').val().trim() == '') {
                        alert('Please Type Group ID');
                        $('#txtoldgroupid').focus();
                        return false;
                    }
                    else {
                        DuplicateCheckGroupID();
                        return false;
                    }
                }
            });

            $('#txtgroupname').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#txtgroupname').val().trim() == '') {
                        alert('Please Type Group Name');
                        $('#txtgroupname').focus();
                        return false;
                    }
                    else {
                        $('#DdlGrouptype').focus();
                        return false;
                    }
                }
            });

            $('#DdlGrouptype').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#DdlGrouptype').val().trim() == '') {
                        alert('Please Select Group Type ');
                        $('#DdlGrouptype').focus();
                        return false;
                    }
                    else {
                        $('#txtgroupschedule').focus();
                        return false;
                    }
                }
            });

            $('#txtgroupschedule').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    $('#cmdSave').focus();
                        return false;
                }
            });

        });// Main Closed


        function DuplicateCheckGroupID() {
            var GroupID = '';
            if ($("#txtoldgroupid").val().trim() == '') {
                return false;
            }
            $(".loading-overlay").show();
            if ($("#hdnGroupID").val() == '') {
                GroupID = '0';
            }
            else { GroupID = $("#hdnGroupID").val(); }
            var W = "{OldGroup_ID:'" + $("#txtoldgroupid").val() + "',Group_ID:'" + GroupID + "'}";
            $.ajax({
                type: "POST",
                url: "LMN_Group_Master.aspx/GET_DupChkGroupID",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    var GroupID = jQuery.parseJSON(data.d);
                    if (GroupID != '') {
                        alert('Group ID Already Exists!');
                        $(".loading-overlay").hide();
                        $("#txtoldgroupid").val('');
                        $("#txtoldgroupid").focus();
                        return false;
                    }
                    else { $(".loading-overlay").hide(); $('#txtgroupname').focus(); return false; }

                },
            });
        }

        function beforeSave() {
            $("#frmEcom").validate();
            DuplicateCheckGroupID();
            $("#txtoldgroupid").rules("add", { required: true, messages: { required: "Please Type Group ID" } });
            $("#txtgroupname").rules("add", { required: true, messages: { required: "Please Type Group Name" } });
            $("#DdlGrouptype").rules("add", { required: true, messages: { required: "Please select Group Type" } });
            //$("#txtgroupschedule").rules("add", { required: true, messages: { required: "Please enter group schedule" } });

        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
</script>

   <%-- <script type="text/javascript">
        //Function to allow only numbers to textbox
        //function validate(key) {
        //    //getting key code of pressed key
        //    var keycode = (key.which) ? key.which : key.keyCode;
        //    var phn = document.getElementById('txtCatagoryid');
        //    //comparing pressed keycodes
        //    if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
        //        return false;
        //    }
        //    else {
        //        //Condition to check textbox contains ten numbers or not
        //        if (phn.value.length < 1) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //$(document).ready(function () {
        //    $("#txtCatagoryid").keypress(function (event) {
        //        var inputValue = event.which;
        //         allow letters and whitespaces only.
        //        if ((inputValue > 47 && inputValue < 58) && (inputValue != 32)) {
        //            alert("Please Enter a Character");
        //            event.preventDefault();
        //        }
        //    });
        //});
        //$(document).ready(function () {
        //    $("#txtCatagoryid").keypress(function (event) {
        //        var inputValue = event.which;
        //         allow letters and whitespaces only.
        //        if ((inputValue > 47 && inputValue < 58) && (inputValue != 32)) {
        //            alert("Please Enter a Character");
        //            event.preventDefault();
        //        }
        //    });
        //});

    </script>--%>
    <style type="text/css">
        .auto-style1 {
            font-family: Segoe UI;
            font-size: 12px;
            color: Navy;
            text-align: left;
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    
    <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>

    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Group MASTER</td>
                 <asp:TextBox ID="hdnGroupID" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" style="display:none"></asp:TextBox>
            </tr>
        </table>
        <br />
        <table width="100%" cellpadding="1" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="1" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="1" width="100%">
                                <tr>
                                    <td class="labelCaption">Group ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtoldgroupid" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                       <%-- <asp:TextBox ID="hdnGroupID" Width="100" ClientIDMode="Static" runat="server" style="display:none"></asp:TextBox>--%>
                                    </td>
                                    </tr>
                                 <tr>
                                    <td class="labelCaption">Group Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtgroupname" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                                <tr>
                                    <td class="labelCaption">Group Type &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlGrouptype" autocomplete="off" CssClass="inputbox2" runat="server" Width="210px" Height="25px" AppendDataBoundItems="true" AutoPostBack="false">
                                             <asp:ListItem Text="(Select Item)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="A">Assets</asp:ListItem>
                                            <asp:ListItem Value="L"> Liabilitiy</asp:ListItem>
                                        </asp:DropDownList>
                                       
                                    </td>
                                    </tr>
                                 <tr>
                                    <td class="labelCaption">Group Schedule &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtgroupschedule" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                   </tr>
                                 
                             </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>  
                            <table align="center" cellpadding="1" width="100%">
                                <tr>
                                    <td class="labelCaption">Group ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtoldgroupid" autocomplete="off" MaxLength="100" ClientIDMode="Static" Text='<%# Eval("OldGroupID")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                     <%--  <asp:TextBox ID="hdnGroupID" Width="100" ClientIDMode="Static" runat="server" style="display:none"></asp:TextBox>--%>
                                    </td>
                                    </tr>  
                                 <tr>
                                    <td class="labelCaption">Group Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtgroupname" autocomplete="off" MaxLength="100" ClientIDMode="Static" Text='<%# Eval("GroupName")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                                <tr>
                                    <td class="labelCaption">Group Type &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlGrouptype" autocomplete="off" CssClass="inputbox2" Height="25px" runat="server" Width="210px" AppendDataBoundItems="true" AutoPostBack="false" Text='<%# Eval("GroupType")%>'>
                                             <asp:ListItem Text="(Select Item)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="A">Assets</asp:ListItem>
                                            <asp:ListItem Value="L">Liabilitiy</asp:ListItem>
                                        </asp:DropDownList>

                                    </td>
                                    </tr>
                                   
                                 <tr>
                                    <td class="labelCaption">Group Schedule &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:TextBox ID="txtgroupschedule" autocomplete="off" MaxLength="100" ClientIDMode="Static" Text='<%# Eval("GroupSchedule")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                      
                                    </td>
                                   </tr>
                                 
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="1" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="auto-style1">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <div style="overflow-y: scroll;  width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="GroupID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" Enabled="true" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("GroupID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" Enabled="true" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("GroupID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("GroupID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                                
                                <asp:BoundField DataField="OldGroupID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Group ID" />    
                                <asp:BoundField DataField="GroupName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Group Name" />
                                <asp:BoundField DataField="GroupType" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Group Type" />
                                <asp:BoundField DataField="GroupSchedule" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Group Schedule" />                            
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            </table>
    </div>
</asp:Content>

