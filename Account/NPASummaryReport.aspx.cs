﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class NPASummaryReport : System.Web.UI.Page
{
    static string StrFormula = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }

            if (!IsPostBack)
            {
                this.BindGridViewNPA();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void BindGridViewNPA()
    {

        DataTable NPA = new DataTable();
        NPA.Rows.Add();
        GridNPASummary.DataSource = NPA;
        GridNPASummary.DataBind();
        GridViewNPADetail.DataSource = NPA;
        GridViewNPADetail.DataBind();
        GridViewInsp.DataSource = NPA;
        GridViewInsp.DataBind();
        GridViewInspRec.DataSource = NPA;
        GridViewInspRec.DataBind();
        GridViewNPAOrgCode.DataSource = NPA;
        GridViewNPAOrgCode.DataBind();

        DataSet ds = DBHandler.GetResults("GET_AllUser");

        DataTable dt1 = new DataTable();
        DataTable dt2 = new DataTable();
        dt1 = ds.Tables[0];
        dt2 = ds.Tables[1];
        drpUser.DataSource = dt1;
        drpUser.DataBind();

        GridUserAprovShow.DataSource = dt2;
        GridUserAprovShow.DataBind();

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]    
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string RecFromDate, string RecToDate)
    {
        string JSONVal = "";       
        try    
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            int UserID;
            int SectorID;           
            string StrPaperSize = "";       
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            StrFormula = "{DAT_Acc_Loan.YEAR_MONTH}>='" + RecFromDate + "' and {DAT_Acc_Loan.YEAR_MONTH}<='" + RecToDate + "' and {MST_Sector.SectorID}=" + SectorID + "";                
            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, RecFromDate, RecToDate, SectorID, "", "", "", "", "", "", "");
            
            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_NPASummary(string FromYearMonth, string ToYearMonth)
    {
        string xml = "";
        DataSet ds = DBHandler.GetResults("Acc_Loanee_NPA_Summ", FromYearMonth, ToYearMonth);
        if (ds.Tables.Count > 0)
        {
            xml = ds.GetXml();
        }
        return xml;
    }    

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_NPADetail(string FromYearMonth, string ToYearMonth, int SectorID, string HealthCode)
    {
        string xml = "";
        DataSet ds = DBHandler.GetResults("Acc_Loanee_NPA_Detail", FromYearMonth, SectorID, HealthCode);
        if (ds.Tables.Count > 0)
        { 
            xml = ds.GetXml();
        }
        return xml;
    }
        [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_NPAOrgCode(string FromYearMonth, int SectorID, string HealthCode, int LOANEE_ID,string SearchType)
    {
        string xml = "";
        DataSet ds = DBHandler.GetResults("Acc_NPA_Loanee_OriginalCode", FromYearMonth, SectorID, HealthCode, LOANEE_ID, SearchType);
        if (ds.Tables.Count > 0)
        { 
            xml = ds.GetXml();
        }
        return xml;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_UserName(int LOANEE_ID, string InspDate)
    {
        string xml = "";
        DataSet dtMstUser = DBHandler.GetResults("GET_UserCodeNameByID", Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]), InspDate, LOANEE_ID);
        if (dtMstUser.Tables.Count > 0)
        {
            xml = dtMstUser.GetXml();
        }
        return xml;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_Inspection(int InspectionID, int LoaneeID, string InspectionDate, string InspHeading, string Remarks)
    {
        string msg = "";
        int SecID= Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        int UserID=Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable Insp = DBHandler.GetResult("Save_Inspection", Convert.ToInt32(InspectionID), Convert.ToInt32(LoaneeID), Convert.ToInt32(SecID), InspectionDate, Convert.ToInt32(UserID), Convert.ToString(InspHeading), Convert.ToString(Remarks));
        if (Insp.Rows.Count > 0)
        {
            msg = Insp.Rows[0]["Msg"].ToString();
            
        }
        return msg;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_ApprovAndDel(string Process, int InspectionID, int Approvelvalue)
    {
        string msg = "";
        DataTable Insp = DBHandler.GetResult("GET_ApprovalAndDel", Convert.ToInt32(InspectionID), Process, Approvelvalue);
        if (Insp.Rows.Count > 0)
        {
            msg = Insp.Rows[0]["Msg"].ToString();

        }
        return msg;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_UserPer(int UserID, int AdminFlg, int ApprovalFlg)
    {
        string msg = "";
        DataTable Insp = DBHandler.GetResult("Save_UserPermision", Convert.ToInt32(UserID), AdminFlg, ApprovalFlg);
        if (Insp.Rows.Count > 0)
        {
            msg = Insp.Rows[0]["Msg"].ToString();

        }
        return msg;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_AdmiApproval(int userid)
    {
        string xml = "";
        DataSet dtMstUser = DBHandler.GetResults("GET_UserApproval", userid);
        if (dtMstUser.Tables.Count > 0)
        {
            xml = dtMstUser.GetXml();
        }
        return xml;
    }

    
}