﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_FundFlowMaster.aspx.cs" Inherits="FundFlowMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function beforeSave() {
            $("#frmEcom").validate();

            $("#txtFunFlowTypeDesc").rules("add", { required: true, messages: { required: "Please enter Projection" } });
            $("#txtGrp").rules("add", { required: true, messages: { required: "Please enter Group" } });
           

        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>FUND FLOW MASTER</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>

                                    <td class="labelCaption">Description&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFunFlowTypeDesc" autocomplete="off" MaxLength="50" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Group&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGrp" autocomplete="off" MaxLength="50" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>


                                </tr>
                                <tr>

                                    <td class="labelCaption">Style&nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:DropDownList ID="ddlStyle" autocomplete="off" ClientIDMode="Static" runat="server">
                                            <asp:ListItem Value="N">Normal</asp:ListItem>
                                            <asp:ListItem Value="B">Bold</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">ActiveFlag&nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                   <td align="left">
                                        <asp:DropDownList ID="ddlActiveFlag" autocomplete="off" ClientIDMode="Static" runat="server">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                        
                                    </td>


                                </tr>
                            </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Description&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFunFlowTypeDesc" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("FunFlowTypeDesc") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Group&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGrp" MaxLength="50" autocomplete="off" ClientIDMode="Static" ReadOnly="true" Text='<%# Eval("Grp") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Style&nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlStyle" autocomplete="off" ClientIDMode="Static" runat="server">
                                            <asp:ListItem Text="Normal" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="Bold" Value="B"></asp:ListItem>
                                        </asp:DropDownList>
                                        
                                        
                                    </td>
                                    <td class="labelCaption">ActiveFlag&nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlActiveFlag" autocomplete="off" ClientIDMode="Static" runat="server">
                                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                        </asp:DropDownList>
                                        
                                    </td>
                                </tr>

                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdCancel" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="FundFlowTypeID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField HeaderText="Edit">
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("FundFlowTypeID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Delete">
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete"
                                            OnClientClick='<%#"return Delete("+(Eval("FundFlowTypeID")).ToString()+") " %>'
                                            CommandArgument='<%# Eval("FundFlowTypeID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="FunFlowTypeDesc" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Description" />
                                <asp:BoundField DataField="Grp" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Group" />
                                <asp:BoundField DataField="Rank" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Rank" />
                                <asp:BoundField DataField="Style" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Style" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
