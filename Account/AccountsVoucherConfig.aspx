﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="AccountsVoucherConfig.aspx.cs" Inherits="AccountsVoucherConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="js/AccountsVoucherConfig.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">

    <div align="center" class="borderStyle" style="width: 98%">
        <table class="headingCaption" style="width: 100%" align="center">
            <tr>
                <td>Account Voucher Configuration</td>
            </tr>
        </table>
        <br />

        <asp:FormView ID="dv" runat="server" Width="100%" CellPadding="4" AutoGenerateRows="False"
            DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">

            <InsertItemTemplate>
                <table style="width: 100%" cellpadding="5" align="center" cellspacing="0">
                    <tr>
                        <td style="padding: 5px; width: 100px" class="labelCaption">Proc Name &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtProcName" autocomplete="off" Width="300"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px; width: 100px" class="labelCaption">Proc Type &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtProcType" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px; width: 100px" class="labelCaption">Proc Order &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtProcOrder" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3" class="labelCaption">
                            <hr class="borderStyle" />
                        </td>
                    </tr>
                </table>

                <table style="width: 100%" cellpadding="5" align="center" cellspacing="0">
                    <tr>
                        <td style="padding: 5px; width: 100px" class="labelCaption">Debit GL ID &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtDr_GLId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                            <asp:TextBox ID="hdnDr_GLId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2" Style="display: none;"></asp:TextBox>
                        </td>

                        <td style="padding: 5px;" class="labelCaption">Debit SL ID &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px;" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtDr_SLId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                            <asp:TextBox ID="hdnDr_SLId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2" Style="display: none;"></asp:TextBox>
                        </td>

                        <td style="padding: 5px;" class="labelCaption">Debit SUB ID &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px;" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtDr_SubId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                            <asp:TextBox ID="hdnDr_SubId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2" Style="display: none;"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 5px;" class="labelCaption">CREDIT GL ID &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px;" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtCr_GLId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                            <asp:TextBox ID="hdnCr_GLId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2" Style="display: none;"></asp:TextBox>
                        </td>

                        <td style="padding: 5px;" class="labelCaption">CREDIT SL ID &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px;" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtCr_SLId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                            <asp:TextBox ID="hdnCr_SLId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2" Style="display: none;"></asp:TextBox>
                        </td>

                        <td style="padding: 5px;" class="labelCaption">CREDIT SUB ID &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px;" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left">
                            <asp:TextBox ID="txtCr_SubId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                            <asp:TextBox ID="hdnCr_SubId" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2" Style="display: none;"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="9" class="labelCaption">
                            <hr class="borderStyle" />
                        </td>
                    </tr>

                </table>
            </InsertItemTemplate>
        </asp:FormView>

        <table width="100%" cellpadding="5" align="center" cellspacing="0">
            <tr>
                <td width="70%"></td>
                <td>
                    <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" autocomplete="off"
                        Width="100px" CssClass="save-button DefaultButton" OnClientClick="BeforeSaveValidations()" OnClick="cmdSave_Click" />

                    <asp:Button ID="cmdCancel" runat="server" Text="Refresh" autocomplete="off"
                        Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" />
                </td>

            </tr>

            <tr>
                <td colspan="2" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
        </table>

        <br />

        <asp:GridView ID="tbl" runat="server" Width="100%" autocomplete="off" GridLines="Both" HeaderStyle-BackColor="#87CEEB" ShowFooter="False" AutoGenerateColumns="false" CellSpacing="2"
                        CssClass="GridVendor">
            <AlternatingRowStyle BackColor="Honeydew" />
            <SelectedRowStyle BackColor="#87CEEB" />
            <Columns>
                <asp:BoundField DataField="PROC_NAME" HeaderText="PROC NAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="POST_TYPE" HeaderText="POST Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="POST_ORD" HeaderText="POST Order" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="D_OLD_GL_ID" HeaderText="DEBIT GL ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="D_OLD_SL_ID" HeaderText="DEBIT SL ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="D_OLD_SUB_ID" HeaderText="DEBIT SUB ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="C_OLD_GL_ID" HeaderText="CREDIT GL ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="C_OLD_SL_ID" HeaderText="DREDIT SL ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="C_OLD_SUB_ID" HeaderText="CREDIT SUB ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            </Columns>
            <FooterStyle CssClass="labelCaption" BackColor="#99ccff" Font-Bold="true" />
        </asp:GridView>
    </div>

</asp:Content>

