﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="InsuranceLoneeDetail.aspx.cs" Inherits="InsuranceLoneeDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/InsuranceLoneeDetail.js?v=2"></script>
    <script type="text/javascript">
           
              
        $(document).ready(function () {
            $('#txtRemDate1').datepicker().datepicker();
            $('#txtRemDate1').datepicker({  
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',    
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            $('#txtRemDate2').datepicker().datepicker();
            $('#txtRemDate2').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            $('#txtRemDate3').datepicker().datepicker();
            $('#txtRemDate3').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            //$('#txtPolicyDate').datepicker().datepicker();
            $("[id$=txtPolicyDate]").datepicker().datepicker();
            //$('#txtPolicyDate').datepicker({
            $("[id$=txtPolicyDate]").datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            //$("[id$=txtPolicyExpDate]").datepicker();
            
            //$('#txtPolicyExpDate').datepicker().datepicker();

            $("[id$=txtPolicyExpDate]").datepicker().datepicker();
            $("[id$=txtPolicyExpDate]").datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

        });

        $(function () {
            $("[id$=txtPropertyAmt]").keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;
                   
                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });
        
        $(function () {
            $("[id$=txtPremiumAmt]").keydown(function (e) {
                if (e.shiftKey || e.ctrlKey || e.altKey) {
                    e.preventDefault();
                } else {
                    var key = e.keyCode;

                    if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
                        e.preventDefault();
                    }
                }
            });
        });

    </script>
    <style type="text/css">
        .rightAlign {
            text-align: right;
        }

        .hiddencol {
            display: none;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .vs  {
          pointer-events: none;
        }
        .cssLoanee {
        display:none;
        }
        .HiddenCol{display:none;}  
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="left" style="width:100%">
        <table class="headingCaption" width="100%" align="left">
            <tr>
                <td>Loanee Insurance Detail</td>
            </tr>
        </table>
        <br />
          <table  style="text-align: center; border: solid 2px navy; text-align: center; width: 100%">
            <tr >
                <td>
                    <asp:Panel ID="panSearch" runat="server" Width="100%">
                        <table style="text-align: center; text-align: center; width: 100%">
                            <tr>
                                <td class="labelCaption">Search Loanee Code &nbsp;<span class="require">*</span> </td>
                                <td class="labelCaption">:</td>
                                <td align="left">
                                    <asp:TextBox autocomplete="off" ID="txtSearchRecLoanee" Width="400px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autoSearchInsRecLoanee"></asp:TextBox>
                                    <asp:TextBox autocomplete="off" ID="txtText" Style="display:none" Width="50px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autoSearchInsRecLoanee"></asp:TextBox>
                                </td>
                                <td colspan="0" align="left">
                                    <asp:Button ID="cmdShow" autocomplete="off" runat="server" Text="Search" CommandName="Add" Height="30"
                                            Width="70px" CssClass="save-button DefaultButton" OnClick="cmdShow_Click"/>

                                        <asp:Button ID="cmdShowCancel" autocomplete="off" runat="server" Text="Refresh" Height="30"
                                            Width="70px" CssClass="save-button DefaultButton" OnClick="cmdShowCancel_Click" />

                                    <asp:Button ID="CmdDelete" autocomplete="off" runat="server" Text="Delete" Height="30"
                                            Width="70px" CssClass="save-button DefaultButton" OnClick="CmdDelete_Click" />
                                </td>
                                <td align="left" style="width:250px;"></td>     
                               
                            </tr>
                          
                        </table>
                    </asp:Panel>
                </td>
            </tr>
          
        

            <tr >
                <td>
                    <asp:FormView ID="dv" runat="server" Width="98%" CellPadding="0" AutoGenerateRows="False"
                        DefaultMode="Insert" HorizontalAlign="left" GridLines="None">
                        <InsertItemTemplate>
                            

                            <table align="left" cellpadding="0" width="98%">
                              
                                
                            </table>
                          
                        </InsertItemTemplate>
                    </asp:FormView>
                </td>
            </tr>


           

             <tr>
                
                <td>
                    <asp:Panel ID="Panel1" runat="server" Width="100%" Visible="true">
                        <div style="overflow-y: scroll; height: auto; width: 100%; float: left">
                             
            <asp:GridView ID="grvInsuMaster" autocomplete="off" runat="server" ShowFooter="True" AutoGenerateColumns="false" 
                CellPadding="1"  Font-Bold="true" ForeColor="#333333" GridLines="None" OnRowDeleting="grvInsuMaster_RowDeleting"
                Width="100%" Style="text-align: left;font-size:12px">
                <Columns>
                   
                    <asp:TemplateField HeaderText="Loanee Code"  > 
                        <ItemTemplate>
                            <asp:TextBox ID="txtMstLoanees" autocomplete="off" TextMode="MultiLine" Width="90%" ClientIDMode="Static" CssClass="inputbox2 autosuggestMstLoaneeCode " runat="server"></asp:TextBox>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Loanee ID" >
                         <HeaderStyle CssClass="HiddenCol" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMstLoaneesID" autocomplete="off" Class="clsMstLoaneesID" style="display:grid;" Width="100px" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </ItemTemplate>
                         <ItemStyle CssClass="HiddenCol" />
                    </asp:TemplateField>


					<asp:TemplateField HeaderText="Ins Code"  > 
                        <ItemTemplate>
                            <asp:TextBox ID="txtMstInsCode" autocomplete="off" Width="100px" ClientIDMode="Static" CssClass="inputbox2 autoInsCode"  runat="server"></asp:TextBox>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ins ID">
                         <HeaderStyle CssClass="HiddenCol" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtMstInsID" autocomplete="off" Class="clsMstInsID" style="display:grid;" Width="0px" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </ItemTemplate>
                          <ItemStyle CssClass="HiddenCol" />
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <asp:TextBox ID="txtMstInsName" autocomplete="off" TextMode="MultiLine" Class="inputbox2 clsMstName" Width="90%" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Address">
                        <ItemTemplate>
                            <asp:TextBox ID="txtMstAddress" autocomplete="off" TextMode="MultiLine" Class="inputbox2 clsMstAddrs" Width="90%" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Status ">
                        <ItemTemplate>
                             <asp:DropDownList ID="ddlMstStatus" autocomplete="off" DataSource='<%# LoaneeStatus() %>'
                                     DataValueField="L_STATUS"
                                    DataTextField="LoaneeStatus" Width="110px" Height="25px" runat="server" ForeColor="#18588a"
                                    AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                    <asp:ListItem Text="Select Status" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderText="Ins Paid ">
                        <ItemTemplate>
                              <asp:DropDownList ID="DrpInsPaids" autocomplete="off" class="DrpInsPaid" ClientIDMode="Static" AppendDataBoundItems="true" Width="120px" Height="25px" CssClass="inputbox2" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="Select Ins Paid" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Party" Value="P"></asp:ListItem>
                                            <asp:ListItem Text="Wbfc" Value="W"></asp:ListItem>
                                        </asp:DropDownList>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                   

                    <asp:TemplateField HeaderText="Row ID">
                        <HeaderStyle CssClass="HiddenCol" />
                        <ItemTemplate>
                           <asp:TextBox ID="txtMstRowID" autocomplete="off" Class="clsMstRowID" Width="0px" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </ItemTemplate>
                         <ItemStyle CssClass="HiddenCol" />
                        <FooterStyle HorizontalAlign="Right" />
                        <FooterTemplate>
                            <asp:Button ID="ButtonAddMst" autocomplete="off" runat="server" Text="Add New Row" OnClick="ButtonAddMst_Click" />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:CommandField ShowDeleteButton="true"  DeleteImageUrl="~/Images/cross.png" ButtonType="Image" ItemStyle-Width="25px" />

                </Columns>
                <FooterStyle BackColor="#18588a" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#EFF3FB" />
                <EditRowStyle BackColor="#2461BF" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="left" />
                <HeaderStyle BackColor="#18588a" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
                        </div>
                      
                    </asp:Panel>
                </td>
            </tr>


            <tr>
                <td>
                    <asp:Panel ID="panGrd" runat="server" Width="100%" Visible="true">
                        <div style="overflow-y: scroll; height: auto; width: 100%; float: left">
                             
            <asp:GridView ID="grvInsuDetails" runat="server" ShowFooter="True" AutoGenerateColumns="false" 
                CellPadding="1"  Font-Bold="true" ForeColor="#333333" GridLines="None" OnRowDeleting="grvInsuDetails_RowDeleting"
                Width="100%" Style="text-align: left;font-size:12px">
                <Columns>
                   
                    <asp:TemplateField HeaderText="Loanee Code"  > 
                        <ItemTemplate>
                            <asp:TextBox ID="txtLoanees" autocomplete="off" TextMode="MultiLine" Width="90%" ClientIDMode="Static" CssClass="inputbox2 autosuggestDetLoaneeCode" runat="server"></asp:TextBox>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Loanee ID">
                         <HeaderStyle CssClass="HiddenCol" />
                        <ItemTemplate>
                            <asp:TextBox ID="txtLoaneesID" autocomplete="off" Class="clsDetLoaneesID" style="display:grid;" Width="100px" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </ItemTemplate>
                         <ItemStyle CssClass="HiddenCol" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Property Type">
                        <ItemTemplate>
                             <asp:DropDownList ID="DrpPropType"  autocomplete="off" DataSource='<%# PropertyType() %>'
                                    DataValueField="ins_type"
                                    DataTextField="ins_type" Width="110px" Height="25px" runat="server" ForeColor="#18588a"
                                    AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                    <asp:ListItem Text="Select Property" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Policy Type">
                        <ItemTemplate>
                             <asp:DropDownList ID="drpPolicyType" autocomplete="off" DataSource='<%# PolicyType() %>'
                                    DataValueField="POLICY_TYPE"
                                    DataTextField="POLICY_TYPE" Width="100px" Height="25px" runat="server" ForeColor="#18588a"
                                    AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                    <asp:ListItem Text="Select Policy" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Property Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPropertyAmt" autocomplete="off" MaxLength="9" Width="100px" ClientIDMode="Static" CssClass="inputbox2" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Policy No.">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPlocyNo" autocomplete="off" MaxLength="30" Width="110px" ClientIDMode="Static" CssClass="inputbox2" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Policy Date">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPolicyDate" autocomplete="off" MaxLength="10"  ReadOnly="false" Width="96px" ClientIDMode="AutoID" CssClass="inputbox2" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Policy Exp. Date">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPolicyExpDate" autocomplete="off" MaxLength="10" Width="96px" ClientIDMode="AutoID" CssClass="inputbox2" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Premium Amount">
                        <ItemTemplate>
                            <asp:TextBox ID="txtPremiumAmt" autocomplete="off" MaxLength="9" Width="100px" ClientIDMode="Static" CssClass="inputbox2" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Row ID">
                          <HeaderStyle CssClass="HiddenCol" />
                        <ItemTemplate>
                             <asp:TextBox ID="txtDetRowID" autocomplete="off" Class="clsDetRowID" Width="100px" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </ItemTemplate>
                         <ItemStyle CssClass="HiddenCol" />
                        <FooterStyle HorizontalAlign="Right" />
                        <FooterTemplate>
                            <asp:Button ID="ButtonAdd" autocomplete="off" runat="server" Text="Add New Row" OnClick="ButtonAdd_Click" />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:CommandField ShowDeleteButton="true"  DeleteImageUrl="~/Images/cross.png" ButtonType="Image" ItemStyle-Width="25px" />

                </Columns>
                <FooterStyle BackColor="#18588a" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#EFF3FB" />
                <EditRowStyle BackColor="#2461BF" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="left" />
                <HeaderStyle BackColor="#18588a" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
                        </div>
                      
                    </asp:Panel>
                </td>
            </tr>
                   
         
            <table style="text-align: center; border: solid 2px navy; text-align: center; width: 100%">
                <tr>
                    <td>

                    <asp:Button ID="btnSave" autocomplete="off" runat="server" Text="Save"  OnClientClick='javascript: return FunSave()' CssClass="save-button DefaultButton" />
                    </td>
                    <td>
                     <asp:Button ID="cmdCancel" autocomplete="off" runat="server" Text="Cancel" OnClick="cmdCancel_Click" CssClass="save-button DefaultButton" />
                    </td>
                    <td></td>
                    <td></td>
                </tr>

            </table>
          <%-- =============================================================--%>        
        </table>
    </div>
    
</asp:Content>
