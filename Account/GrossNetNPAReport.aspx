﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="GrossNetNPAReport.aspx.cs" Inherits="GrossNetNPAReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="js/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert('Only Numeric Value Accept!');
                return false;
            }
            return true;
        }
        $(document).ready(function () {
            $("#txtFrmDate").focus();
            $("#txtFrmDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    if ($("#txtFrmDate").val().trim() == '') {
                        alert('Please Enter Year & Month....!');
                        $("#txtFrmDate").focus();
                        return false;
                    }
                    if ($('#txtFrmDate').val().length < 6) {
                        alert("Please Enter Valid Year And Month");
                        $('#txtFrmDate').focus();
                        return false;
                    }
                    $("#btnGenerateReport").focus();
                    return false;
                    //else {
                    //    $("#btnGenerateReport").focus();
                    //    return false;
                    //}
                }
            });
        });
        
        function FunNPASummary() {
            var FromYearMonth = '';
            var msg = '';
            var SecID = '';
            if ($('#txtFrmDate').val().trim()== '') {
                alert("Please Enter From Year And Month");
                $('#txtFrmDate').focus();
                return false;
            }
            if ($('#txtFrmDate').val().length < 6) {
                alert("Please Enter Valid Year And Month");
                $('#txtFrmDate').focus();
                return false;   
            }
            var CurDate = $('#txtFrmDate').val().substring(6, 4) + '/' + '01/' + $('#txtFrmDate').val().substring(4, 0);
           
            var date = new Date(CurDate);
          //  var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var LenMonth = (lastDay.getMonth() + 1);
            if (parseInt(LenMonth) > 0 && parseInt(LenMonth) < 10) { LenMonth = '0' + LenMonth; }
            var lastDayWithSlashes = (lastDay.getDate()) + '.' + LenMonth + '.' + lastDay.getFullYear();
            msg = 'PERCENTAGE OF GROSS NPA AND NET NPA AS ON ' + lastDayWithSlashes;
           
            FromYearMonth = $('#txtFrmDate').val();
            SecID = $('#ddlUserSector').val();
            if (SecID == '') { SecID = 1;}
            ToYearMonth = '0';
            $(".loading-overlay").show();
            var W = "{FromYearMonth:'" + FromYearMonth + "',SecID:" + SecID + "}";
            $.ajax({
                type: "POST",
                url: "GrossNetNPAReport.aspx/GET_NPASummary",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (D) {
                    var xmlDoc = $.parseXML(D.d);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");
                      
                    var Per = 0;
                    var GrandPer = 0;
                    var GrandProv = 0;
                    var GrandAfterProv = 0;
                    var GrandNPA = 0;
                    var AfterProv = 0;
                    var AfterProvPer = 0;
                    var TotPrOther = 0;
                    var TotNPAPr = 0;
                    var NetNPA = 0;
                    $("#GridGrossNPA").empty();
                    $("#GridGrossNPA").append("<tr style='background-color:navy;color:white;font-weight:bold;font-size:14px;'><th>Category</th><th style='text-align:right'>Pr. + Other Outstanding(&#x20B9) </th><th style='text-align:right'>Gross NPA (%) </th><th style='text-align:right'>Provisioning(&#x20B9) </th><th style='text-align:right'>Net Asset After Provisioning(&#x20B9) </th><th style='text-align:right'>Net NPA (%) </th></tr>");
                    //Coding For Total Pr. + Other Outstanding
                    $(xml).find("Table").each(function () {
                        TotPrOther = parseFloat(TotPrOther) + parseFloat($(this).find("GROSS_NPA_ASSETS").text());
                        GrandAfterProv = parseFloat(GrandAfterProv) + parseFloat($(this).find("GROSS_NPA_ASSETS").text()) - parseFloat($(this).find("PROV").text());

                    });
                    //Coding For Health Code, Pr. + Other Outstanding
                    if (Table.length > 0) {
                        $(xml).find("Table").each(function () {
                            Per = 0;
                            AfterProvPer = 0;
                            Per = parseFloat($(this).find("GROSS_NPA_ASSETS").text()) / parseFloat(TotPrOther) * 100;
                            AfterProv = parseFloat($(this).find("GROSS_NPA_ASSETS").text()) - parseFloat($(this).find("PROV").text());
                            AfterProvPer = parseFloat(AfterProv) / parseFloat(GrandAfterProv) * 100;
                            $("#GridGrossNPA").append("<tr style=';font-size:13px;'><td align='center' style='height:13px;width:100px; padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("HEALTH_CODE_GRP").text() + "</td> <td align='right' style='height:13px;width:300px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("GROSS_NPA_ASSETS").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       Per.toFixed(2).toString() + "</td> <td align='right' style='height:13px;width:300px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("PROV").text() + "</td> <td align='right' style='height:13px;width:300px;padding: 5px;color:navy;font-weight:bold;'>" + //cursor:pointer;
                                       AfterProv.toFixed(2).toString() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" + //cursor:pointer;
                                     AfterProvPer.toFixed(2).toString() + "</td></tr>");

                            GrandPer = parseFloat(GrandPer) + parseFloat(Per);
                            GrandProv = parseFloat(GrandProv) + parseFloat($(this).find("PROV").text());
                            TotNPAPr = parseFloat(TotNPAPr) + parseFloat(AfterProvPer);
                            //GrandAfterProv = parseFloat(GrandAfterProv) + parseFloat(AfterProv);
                            if ($(this).find("HEALTH_CODE_GRP").text() != 'A') {
                                GrandNPA = parseFloat(GrandNPA) + parseFloat(Per);
                                NetNPA = parseFloat(NetNPA) + parseFloat(AfterProvPer);
                            }
                            
                            
                        });
                        //Coding For %
                        $("#GridGrossNPA").append("<tr style=';font-size:14px;'><td align='center' style='height:13px;width:100px; padding: 5px;color:Red;font-weight:bold;'>" +
                                            'TOTAL' + "</td> <td align='right' style='height:13px;width:300px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(TotPrOther).toFixed(2) + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(GrandPer).toFixed(2) + "</td> <td align='right' style='height:13px;width:300px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(GrandProv).toFixed(2) + "</td> <td align='right' style='height:13px;width:300px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(GrandAfterProv).toFixed(2) + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(TotNPAPr).toFixed(2) + "</td></tr>");
                        //Coding For Gross NPA	
                        $("#GridGrossNPA").append("<tr style=';font-size:14px;'><td align='center' style='height:13px;width:100px; padding: 5px;color:Green;font-weight:bold;'>" +
                                            'GROSS NPA' + "</td> <td align='right' style='height:13px;width:300px;padding: 5px;color:Green;font-weight:bold;'>" +
                                            '' + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:Green;font-weight:bold;'>" +
                                            parseFloat(GrandNPA).toFixed(2) + "</td> <td align='right' style='height:13px;color:Green;width:300px;padding: 5px;font-weight:bold;'>" +
                                            '' + "</td> <td align='right' style='height:13px;width:300px;padding: 5px;font-weight:bold;color:Green;'>" +
                                            'NET NPA' + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;font-weight:bold;color:Green;'>" +
                                            parseFloat(NetNPA).toFixed(2) + "</td></tr>");
                    }
                    $("#lblmsg").text(msg);
                    $(".loading-overlay").hide();
                    return false;
                },
                error: function (result) {
                    alert("Error Records Data NPA Summary");
                    return false;
                }
            });

        }

        $(document).ready(function () {
            $("#btnPrint").click(function (event) {
                if ($('#txtFrmDate').val().trim() == '') {
                    alert("Please Enter From Year And Month");
                    $('#txtFrmDate').focus();
                    return false;
                }
                if ($('#txtFrmDate').val().length < 6) {
                    alert("Please Enter Valid Year And Month");
                    $('#txtFrmDate').focus();
                    return false;
                }

                ShowReport();
                return false;
            });
        });

        function ShowReport() {
            //alert('Comming Soon.........Please Wait........!'); return false;
            if ($('#txtFrmDate').val().trim()== '') {
                alert("Please Enter From Year And Month");
                $('#txtFrmDate').focus();
                return false;
            }
            if ($('#txtFrmDate').val().length < 6) {
                alert("Please Enter Valid Year And Month");
                $('#txtFrmDate').focus();
                return false;   
            }

            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            var RecFromDate = '';
                FormName = "GrossNetNPAReport.aspx";
                ReportName = "GROSSNPAAndNETNPAReport";
                ReportType = "GROSS NPA & NET NPA SUMMARY";
                RecFromDate = $('#txtFrmDate').val();


            var E = '';
            E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  RecFromDate:'" + RecFromDate + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/SetReportValue',
                data: E,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var show = response.d;
                    if (show == "OK") {
                        window.open("ReportView.aspx?E=Y");
                    }
                },
                error: function (response) {
                    var responseText;
                    responseText = JSON.parse(response.responseText);
                    alert("Error : " + responseText.Message);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

            
        //function ShowCurrentDate() {
        //    var today = new Date();
        //    var dd = today.getDate();
        //    var mm = today.getMonth() + 1; //January is 0!
        //    var yyyy = today.getFullYear();

        //    if (dd < 10) {
        //        dd = '0' + dd;
        //    }

        //    if (mm < 10) {
        //        mm = '0' + mm;
        //    }

        //    today = dd + '/' + mm + '/' + yyyy;

        //    $("#txtInspectionDate").val(today);

        //    ModifyrowCount = '0';
        //}

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>GROSS NPA & NET NPA SUMMARY</td>
            </tr>
        </table>

         <div class="loading-overlay" style="width:100%;height:150%;">
            <div class="loadwrapper" style="width:100%;height:150%;">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>
          
        <br />
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">Year&Month&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td ">
                    <asp:TextBox ID="txtFrmDate" autocomplete="off" ReadOnly="false" MaxLength="6" placeholder="YYYYYMM" Width="120px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                </td>
               
                <td>
                    <asp:Button ID="btnGenerateReport" autocomplete="off" runat="server" Height="35px" Width="80px" Text="Show" CssClass="save-button DefaultButton" OnClientClick="FunNPASummary();return false" />
                    <asp:Button ID="btnCancel" autocomplete="off" runat="server" Text="Cancel " Height="35px" Width="80px" CssClass="save-button" OnClick="btnCancel_Click" />
                    <asp:Button ID="btnPrint" autocomplete="off" runat="server" Text="Print" Height="35px" Width="80px" CssClass="save-button DefaultButton" />
                </td>
            </tr>
            <tr style="width:100%">
                <td></td>
                <td></td>
            </tr>
        </table>

        <table>
            <tr style="width: 100%">
                <td></td>
                <td></td>
            </tr>    
        </table>
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:Label ID="lblmsg" autocomplete="off" runat="server" Style="text-align:center;" Width="100%" ClientIDMode="Static" Text="" Font-Bold="true" ForeColor="Green" Font-Size="16px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="DialogAllPartNo">
                        <asp:GridView ID="GridGrossNPA" autocomplete="off" runat="server" AutoGenerateColumns="false" HeaderStyle-BackColor="Navy" ForeColor="White" Font-Bold="true" Font-Size="13px"
                            PageSize="5" Width="100%" Style="border: 2px solid #59bdcc;" AllowPaging="true">
                            <Columns>
                                <asp:BoundField HeaderText="Category" ItemStyle-Width="100" />
                                <asp:BoundField HeaderText="Pr. + Other Outstanding" ItemStyle-Width="300" />
                                <asp:BoundField HeaderText="Gross NPA (%)" ItemStyle-Width="200" />
                                <asp:BoundField HeaderText="Provisioning" ItemStyle-Width="300" />
                                <asp:BoundField HeaderText="Net Asset After Provisioning" ItemStyle-Width="300" />
                                 <asp:BoundField HeaderText="Net NPA (%)" ItemStyle-Width="200" />
                            </Columns>
                        </asp:GridView>
                       <%-- <asp:GridView ID="GridNetNPA" runat="server" AutoGenerateColumns="false" 
                            PageSize="5" Width="100%" Style="border: 1px solid #59bdcc" AllowPaging="true">
                            <Columns>
                                <asp:BoundField HeaderText="Category" ItemStyle-Width="10" />
                                <asp:BoundField HeaderText="%" ItemStyle-Width="400" />
                            </Columns>
                        </asp:GridView>--%>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
