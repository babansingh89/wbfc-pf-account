﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Services;
using System.Data.SqlClient;
using System.Threading;
using System.Web.Script.Services;

public partial class PaymentReceived : System.Web.UI.Page
{
    static string StrFormula = "";
    protected string pay_current_Date = "";
    protected string YearMonth = "";
    protected string h_code = "";
    protected string s_type = "";
    static string hdnRcvID = "";
    static string hdnLoaneeID = "";
    static string hdnBankID="";
    static string hdnDraweeBank = "";
    static string vchNo = "";
    static string vchType = "";
    static string vchYearMonth = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            panGrd.Visible = false;
            if (!IsPostBack)
            {
                Get_YearMonth();
                Get_CurrentFinancialYear();
                txtYearmonth.Text = YearMonth;
            }
            else
            {
                TextBox txtDate = (TextBox)dv.FindControl("txtDatePay");
                pay_current_Date = txtDate.Text;
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Acc_VchInstType");
            return dt;         
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void Get_CurrentFinancialYear()
    {
        try
        {
            string FinancialYear = "";
            DataTable dtResult = DBHandler.GetResult("Get_Financial_Year");
            if (dtResult.Rows.Count > 0)
            {
                pay_current_Date = dtResult.Rows[0]["cur_date"].ToString();
                FinancialYear = dtResult.Rows[0]["Financial_Year"].ToString();
                //First_Date = "01" + "/04/" + FinancialYear.Substring(0, 4);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private void Get_YearMonth()
    {
        try
        {
            DataTable dtResult = DBHandler.GetResult("Get_Year_Month", Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
            if (dtResult.Rows.Count > 0)
            {
                YearMonth = dtResult.Rows[0]["YEAR_MONTH"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string[] LoaneeAutoCompleteData(string LoaneeCode)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_LoaneeIDByCode", LoaneeCode, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["LOANEE_NAME1"].ToString(), dtRow["LOANEE_UNQ_ID"].ToString()));
        }
        return result.ToArray();
    }

    [WebMethod]
    public static Loanee[] GET_LoaneeSattlTypeHeealthCode(string LoaneeUNQID, string YearMonth)
    {
        List<Loanee> Detail = new List<Loanee>();

        DataTable dtGetData = DBHandler.GetResult("GET_LoaneeSattlTypeHeealthCode", Convert.ToInt64(LoaneeUNQID), YearMonth);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            Loanee DataObj = new Loanee();
            DataObj.Loanee_SettlementType = dtRow["s_type"].ToString();
            DataObj.Loanee_HealthCode = dtRow["health_code"].ToString();
            Detail.Add(DataObj);

        }
        return Detail.ToArray();
    }
    public class Loanee 
    {
        public string Loanee_SettlementType { get; set; }
        public string Loanee_HealthCode { get; set; }
    }

    [WebMethod]
    public static string[] BankAutoCompleteData(string Bank)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_Bank", Bank, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["GL_NAME1"].ToString(), dtRow["GL_ID"].ToString()));
        }
        return result.ToArray();
    }

    [WebMethod]
    public static string[] SearchAutoCompleteData(string LoaneeID, string Yearmonth)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_PaymentReceived", LoaneeID, Yearmonth, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["LOANEE_NAME"].ToString(), dtRow["RCV_ADV_ID"].ToString()));
        }
        return result.ToArray();
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string loaneeenqid = hdnLoaneeID;
            if (loaneeenqid == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Select a Loanee .')</script>");
                return;
            }
            PopulateGrid();

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void txtAmountTextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox txtDate = (TextBox)dv.FindControl("txtDatePay");
            string loaneeenqid = hdnLoaneeID;
            if (loaneeenqid == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Select a Loanee .')</script>");
                return;
            }
            if (tbl.Rows.Count > 0)
            {
                PopulateGrid();
            }
            txtDate.Focus();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            TextBox txtLoaneeSearch = (TextBox)dv.FindControl("txtLoaneeSearch");
            DropDownList ddlRepaymentTerm = (DropDownList)dv.FindControl("ddlRepaymentTerm");
            TextBox txtHealthCode = (TextBox)dv.FindControl("txtHealthCode");
            string str = hdnLoaneeID;
            TextBox txtDate = (TextBox)dv.FindControl("txtDatePay");
            TextBox txtAmount = (TextBox)dv.FindControl("txtAmount");
            DateTime dtText = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);          
            DataTable dtResult = DBHandler.GetResult("Acc_Loanee_Os_Get", Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()), Convert.ToInt64(hdnLoaneeID),
                                      dtText.ToShortDateString(), txtAmount.Text);
            if (dtResult.Rows.Count > 0)
            {
                panGrd.Visible = true;
                tbl.DataSource = dtResult;
                tbl.DataBind();

                decimal DUE_AMT = dtResult.AsEnumerable().Sum(row => row.Field<decimal>("DUE_AMT"));
                //decimal ACTUAL_AMT = dtResult.AsEnumerable().Sum(row => row.Field<decimal>("ACTUAL_AMT"));

                tbl.FooterRow.Cells[1].Text = "Total";
                tbl.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;

                tbl.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                tbl.FooterRow.Cells[2].Text = DUE_AMT.ToString("F2");

                tbl.FooterRow.Cells[4].Text = "Transaction to Sundry not consider.";
                tbl.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Left;

                ddlRepaymentTerm.Enabled = false;
                txtHealthCode.Enabled = false;
                txtLoaneeSearch.Enabled = false;
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No record found.')</script>");
            }    
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdBtn = (Button)sender;

            if (cmdBtn.CommandName == "Add")
            {
                double totActualAmt = 0.00;
                int cnt = 1;
                string str = hdnLoaneeID;
                DropDownList ddlRepaymentTerm = (DropDownList)dv.FindControl("ddlRepaymentTerm");
                TextBox txtHealthCode = (TextBox)dv.FindControl("txtHealthCode");
                TextBox txtAmount = (TextBox)dv.FindControl("txtAmount");
                TextBox txtDate = (TextBox)dv.FindControl("txtDatePay");
                DateTime dtText = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                TextBox txtDraweeBank = (TextBox)dv.FindControl("txtDraweeBank");
                DropDownList ddlInstrumentType = (DropDownList)dv.FindControl("ddlInstrumentType");
                TextBox txtNo = (TextBox)dv.FindControl("txtNo");
                string RvcAdvID = "";

                for (int i = 0; i <= tbl.Rows.Count - 1; i++)
                {
                    panGrd.Visible = true;
                    string due_Date = tbl.Rows[i].Cells[4].Text;
                    if (due_Date == "&nbsp;")
                    {
                        TextBox actual_Amount = tbl.Rows[i].Cells[3].FindControl("txtActAmount") as TextBox;
                        string act_amt = actual_Amount.Text.Equals("") ? "0.00" : actual_Amount.Text;                        
                        totActualAmt = totActualAmt + Convert.ToDouble(act_amt);
                    }                
                }
                if (totActualAmt != Convert.ToDouble(txtAmount.Text))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record not saved successfully. Sum of Actual Amount must be same as Total Amount. Transaction to Sundry not consider.')</script>");
                    return;
                }

                SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlCommand cmd1 = new SqlCommand();
                    SqlTransaction transaction;
                    SqlDataReader reader;
                    db.Open();
                    transaction = db.BeginTransaction();
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "Insert_PaymentReceived";

                        cmd.Parameters.Add("@LOANEE_UNQ_ID", SqlDbType.BigInt).Value = Convert.ToInt64(hdnLoaneeID);
                        cmd.Parameters.Add("@HEALTH_CODE", SqlDbType.VarChar).Value = (txtHealthCode.Text.Equals("") ? DBNull.Value : (object)txtHealthCode.Text);
                        cmd.Parameters.Add("@SETTLEMENT_TYPE", SqlDbType.VarChar).Value = (ddlRepaymentTerm.SelectedValue.Equals("") ? DBNull.Value : (object)ddlRepaymentTerm.SelectedValue);
                        cmd.Parameters.Add("@BANK_ID", SqlDbType.VarChar).Value = (hdnBankID.Equals("") ? DBNull.Value : (object)hdnBankID);
                        cmd.Parameters.Add("@RCV_AMT", SqlDbType.Money).Value = (txtAmount.Text.Equals("") ? 0 : (object)Convert.ToDecimal(txtAmount.Text));
                        cmd.Parameters.Add("@RCV_DATE", SqlDbType.Date).Value = (txtDate.Text.Equals("") ? DBNull.Value : (object)dtText.ToShortDateString());
                        cmd.Parameters.Add("@DRAWEE_BANK", SqlDbType.VarChar).Value = (txtDraweeBank.Text.Equals("") ? DBNull.Value : (object)txtDraweeBank.Text);
                        cmd.Parameters.Add("@INST_TYPE", SqlDbType.VarChar).Value = (ddlInstrumentType.SelectedValue.Equals("") ? DBNull.Value : (object)ddlInstrumentType.SelectedValue);
                        cmd.Parameters.Add("@INST_NO", SqlDbType.VarChar).Value = (txtNo.Text.Equals("") ? DBNull.Value : (object)txtNo.Text);
                        cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString());
                        cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

                        cmd.Connection = db;
                        cmd.Transaction = transaction;

                        reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            RvcAdvID = reader["RcvAdvID"].ToString();
                        }
                        reader.Close();
                        cnt++;   // SQL 1 ERROR

                        if (RvcAdvID != "")
                        {
                            if (tbl.Rows.Count > 0)
                            {
                                for (int i = 0; i <= tbl.Rows.Count - 1; i++)
                                {
                                    string sub_id = tbl.Rows[i].Cells[0].Text;
                                    string due_amount = tbl.Rows[i].Cells[2].Text;

                                    TextBox actual_Amount = tbl.Rows[i].Cells[3].FindControl("txtActAmount") as TextBox;
                                    string act_amt = actual_Amount.Text;

                                    DateTime dtdue_Date = new DateTime();
                                    string due_Date = tbl.Rows[i].Cells[4].Text;
                                    if (due_Date != "&nbsp;")
                                    {
                                       dtdue_Date = DateTime.ParseExact(due_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        due_Date = "";
                                    }
                                    

                                    TextBox Remarks = tbl.Rows[i].Cells[5].FindControl("txtRemarks") as TextBox;
                                    string remarks = Remarks.Text;

                                    cmd = new SqlCommand();
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.CommandText = "Insert_PaymentReceivedDetails";

                                    cmd.Parameters.Add("@RCV_ADV_ID", SqlDbType.VarChar).Value = RvcAdvID;
                                    cmd.Parameters.Add("@SubId", SqlDbType.VarChar).Value = sub_id;
                                    cmd.Parameters.Add("@DueAmt", SqlDbType.Money).Value = Convert.ToDecimal(due_amount);
                                    cmd.Parameters.Add("@ActualAmt", SqlDbType.Money).Value = (act_amt.Equals("") ? 0 : (object)Convert.ToDecimal(act_amt));
                                    cmd.Parameters.Add("@DueDate", SqlDbType.Date).Value = (due_Date.Equals("") ? DBNull.Value : (object)dtdue_Date.ToShortDateString());
                                    cmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = (remarks.Equals("") ? DBNull.Value : (object)remarks);
                                    cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString());
                                    cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

                                    cmd.Connection = db;
                                    cmd.Transaction = transaction;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            cnt++;    // SQL 2 ERROR


                            cmd = new SqlCommand();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "Update_PaymentReceived_AdvStatus";

                            cmd.Parameters.Add("@RCV_ADV_ID", SqlDbType.VarChar).Value = RvcAdvID;
                            cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString());
                            cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

                            cmd.Connection = db;
                            cmd.Transaction = transaction;
                            cmd.ExecuteNonQuery();

                            cnt++;    // SQL 3 ERROR
                        }
                        transaction.Commit();
                        Thread.Sleep(1000);
                    }
                    catch (SqlException sqlError)
                    {
                        transaction.Rollback();
                        db.Close();
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('***SQL " + cnt + " Error***')</script>");
                    }
                    db.Close();

                    tbl.DataSource = null;
                    panGrd.Visible = false;
                    panSearch.Visible = false;
                    dv.DataSource = null;
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Completed Successfully.')</script>");
                    DataTable dtVchNo = DBHandler.GetResult("Get_VchNoFromPaymentReceived", RvcAdvID, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
                    if (dtVchNo.Rows.Count > 0)
                    {

                        dv.Visible = false;
                        panVch.Visible = true;
                        lblVchNo.Text = dtVchNo.Rows[0]["VoucherNo"].ToString();
                        vchNo = dtVchNo.Rows[0]["VoucherNo"].ToString();
                        vchType = dtVchNo.Rows[0]["VCH_TYPE"].ToString();
                        vchYearMonth = dtVchNo.Rows[0]["YEAR_MONTH"].ToString();
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No Voucher No Found.')</script>");
                    }
                }
                catch (Exception ex)
                {
                    db.Close();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
                }
            }
            else if (cmdBtn.CommandName == "Edit")
            {
                string RcvID = hdnRcvID;
                double totActualAmt = 0.00;
                int cnt = 1;
                string str = hdnLoaneeID;
                DropDownList ddlRepaymentTerm = (DropDownList)dv.FindControl("ddlRepaymentTerm");
                TextBox txtHealthCode = (TextBox)dv.FindControl("txtHealthCode");
                TextBox txtAmount = (TextBox)dv.FindControl("txtAmount");
                TextBox txtDate = (TextBox)dv.FindControl("txtDatePay");
                DateTime dtText = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                TextBox txtDraweeBank = (TextBox)dv.FindControl("txtDraweeBank");
                DropDownList ddlInstrumentType = (DropDownList)dv.FindControl("ddlInstrumentType");
                TextBox txtNo = (TextBox)dv.FindControl("txtNo");
                //string RvcAdvID = "";

                for (int i = 0; i <= tbl.Rows.Count - 1; i++)
                {
                    panGrd.Visible = true;
                    string due_Date = tbl.Rows[i].Cells[4].Text;
                    if (due_Date == "&nbsp;")
                    {
                        TextBox actual_Amount = tbl.Rows[i].Cells[3].FindControl("txtActAmount") as TextBox;
                        string act_amt = actual_Amount.Text.Equals("") ? "0.00" : actual_Amount.Text;
                        totActualAmt = totActualAmt + Convert.ToDouble(act_amt);

                    } 
                }
                if (totActualAmt != Convert.ToDouble(txtAmount.Text))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record not saved successfully. Sum of Actual Amount must be same as Total Amount. Transaction to Sundry not consider.')</script>");
                    return;
                }

                SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlTransaction transaction;
                    SqlDataReader reader;
                    db.Open();
                    transaction = db.BeginTransaction();
                    try
                    {
                        cmd = new SqlCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "Update_PaymentReceived_AdvStatus_N";

                        cmd.Parameters.Add("@RCV_ADV_ID", SqlDbType.VarChar).Value = RcvID;
                        cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString());
                        cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

                        cmd.Connection = db;
                        cmd.Transaction = transaction;
                        cmd.ExecuteNonQuery();

                        cnt++;    // SQL 1 ERROR


                        cmd = new SqlCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "Update_PaymentReceivedMaster";

                        cmd.Parameters.Add("@RCV_ADV_ID", SqlDbType.VarChar).Value = RcvID;
                        cmd.Parameters.Add("@LOANEE_UNQ_ID", SqlDbType.BigInt).Value = Convert.ToInt64(hdnLoaneeID);
                        cmd.Parameters.Add("@HEALTH_CODE", SqlDbType.VarChar).Value = (txtHealthCode.Text.Equals("") ? DBNull.Value : (object)txtHealthCode.Text);
                        cmd.Parameters.Add("@SETTLEMENT_TYPE", SqlDbType.VarChar).Value = (ddlRepaymentTerm.SelectedValue.Equals("") ? DBNull.Value : (object)ddlRepaymentTerm.SelectedValue);
                        cmd.Parameters.Add("@BANK_ID", SqlDbType.VarChar).Value = (hdnBankID.Equals("") ? DBNull.Value : (object)hdnBankID);
                        cmd.Parameters.Add("@RCV_AMT", SqlDbType.Money).Value = (txtAmount.Text.Equals("") ? 0 : (object)Convert.ToDecimal(txtAmount.Text));
                        cmd.Parameters.Add("@RCV_DATE", SqlDbType.Date).Value = (txtDate.Text.Equals("") ? DBNull.Value : (object)dtText.ToShortDateString());
                        cmd.Parameters.Add("@DRAWEE_BANK", SqlDbType.VarChar).Value = (txtDraweeBank.Text.Equals("") ? DBNull.Value : (object)txtDraweeBank.Text);
                        cmd.Parameters.Add("@INST_TYPE", SqlDbType.VarChar).Value = (ddlInstrumentType.SelectedValue.Equals("") ? DBNull.Value : (object)ddlInstrumentType.SelectedValue);
                        cmd.Parameters.Add("@INST_NO", SqlDbType.VarChar).Value = (txtNo.Text.Equals("") ? DBNull.Value : (object)txtNo.Text);
                        cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString());
                        cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

                        cmd.Connection = db;
                        cmd.Transaction = transaction;
                        cmd.ExecuteNonQuery();
                        cnt++;   // SQL 2 ERROR

                            if (tbl.Rows.Count > 0)
                            {
                                for (int i = 0; i <= tbl.Rows.Count - 1; i++)
                                {
                                    string sub_id = tbl.Rows[i].Cells[0].Text;
                                    string due_amount = tbl.Rows[i].Cells[2].Text;

                                    TextBox actual_Amount = tbl.Rows[i].Cells[3].FindControl("txtActAmount") as TextBox;
                                    string act_amt = actual_Amount.Text;

                                    DateTime dtdue_Date = new DateTime();
                                    string due_Date = tbl.Rows[i].Cells[4].Text;
                                    if (due_Date != "&nbsp;")
                                    {
                                        dtdue_Date = DateTime.ParseExact(due_Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else
                                    {
                                        due_Date = "";
                                    }

                                    TextBox Remarks = tbl.Rows[i].Cells[5].FindControl("txtRemarks") as TextBox;
                                    string remarks = Remarks.Text;

                                    cmd = new SqlCommand();
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.CommandText = "Update_PaymentReceivedDetail";

                                    cmd.Parameters.Add("@RCV_ADV_ID", SqlDbType.VarChar).Value = RcvID;
                                    cmd.Parameters.Add("@SubId", SqlDbType.VarChar).Value = sub_id;
                                    cmd.Parameters.Add("@DueAmt", SqlDbType.Money).Value = Convert.ToDecimal(due_amount);
                                    cmd.Parameters.Add("@ActualAmt", SqlDbType.Money).Value = (act_amt.Equals("") ? 0 : (object)Convert.ToDecimal(act_amt));
                                    cmd.Parameters.Add("@DueDate", SqlDbType.Date).Value = (due_Date.Equals("") ? DBNull.Value : (object)dtdue_Date.ToShortDateString());
                                    cmd.Parameters.Add("@Remarks", SqlDbType.VarChar).Value = (remarks.Equals("") ? DBNull.Value : (object)remarks);
                                    cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString());
                                    cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

                                    cmd.Connection = db;
                                    cmd.Transaction = transaction;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            cnt++;    // SQL 3 ERROR

                            cmd = new SqlCommand();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandText = "Update_PaymentReceived_AdvStatus";

                            cmd.Parameters.Add("@RCV_ADV_ID", SqlDbType.VarChar).Value = RcvID;
                            cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString());
                            cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

                            cmd.Connection = db;
                            cmd.Transaction = transaction;
                            cmd.ExecuteNonQuery();

                            cnt++;    // SQL 4 ERROR
                        transaction.Commit();
                        Thread.Sleep(1000);
                    }
                    catch (SqlException sqlError)
                    {
                        transaction.Rollback();
                        db.Close();
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('***SQL " + cnt + " Error***')</script>");
                    }
                    db.Close();

                    tbl.DataSource = null;
                    panGrd.Visible = false;
                    panSearch.Visible = false;
                    dv.DataSource = null;
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Completed Successfully.')</script>");
                    DataTable dtVchNo = DBHandler.GetResult("Get_VchNoFromPaymentReceived", RcvID, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
                    if (dtVchNo.Rows.Count > 0)
                    {

                        dv.Visible = false;
                        panVch.Visible = true;
                        lblVchNo.Text = dtVchNo.Rows[0]["VoucherNo"].ToString();
                        vchNo = dtVchNo.Rows[0]["VoucherNo"].ToString();
                        vchType = dtVchNo.Rows[0]["VCH_TYPE"].ToString();
                        vchYearMonth = dtVchNo.Rows[0]["YEAR_MONTH"].ToString();
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No Voucher No Found.')</script>");
                    }
                }
                catch (Exception ex)
                {
                    db.Close();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
                }
            }             
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdshow_Click(object sender, EventArgs e)
    {
        try
        {
           
            string RcvID = hdnRcvID;
            TextBox txtLoaneeSearch = (TextBox)dv.FindControl("txtLoaneeSearch");
            DropDownList ddlRepaymentTerm = (DropDownList)dv.FindControl("ddlRepaymentTerm");
            TextBox txtBank = (TextBox)dv.FindControl("txtBank");
            TextBox txtHealthCode = (TextBox)dv.FindControl("txtHealthCode");
            TextBox txtAmount = (TextBox)dv.FindControl("txtAmount");
            TextBox txtDate = (TextBox)dv.FindControl("txtDatePay");
            DateTime dtText = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            TextBox txtDraweeBank = (TextBox)dv.FindControl("txtDraweeBank");
            DropDownList ddlInstrumentType = (DropDownList)dv.FindControl("ddlInstrumentType");
            TextBox txtNo = (TextBox)dv.FindControl("txtNo");

            DataTable dtResult = DBHandler.GetResult("Get_PaymentReceivedMaster",RcvID, Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()));
            if (dtResult.Rows.Count > 0)
            {
                hdnLoaneeID = dtResult.Rows[0]["LOANEE_UNQ_ID"].ToString();
                hdnBankID = dtResult.Rows[0]["BANK_ID"].ToString();
                txtLoaneeSearch.Text = dtResult.Rows[0]["LOANEE_NAME"].ToString();
                ddlRepaymentTerm.SelectedValue = dtResult.Rows[0]["SETTLEMENT_TYPE"].ToString();
                txtHealthCode.Text = dtResult.Rows[0]["HEALTH_CODE"].ToString();
                txtBank.Text = dtResult.Rows[0]["BANKID"].ToString();
                txtAmount.Text = Convert.ToDecimal(dtResult.Rows[0]["RCV_AMT"].ToString()).ToString("F2");
                //txtDate.Text = dtResult.Rows[0]["RCV_DATE"].ToString();
                pay_current_Date = dtResult.Rows[0]["RCV_DATE"].ToString();    /// to set date
                txtDraweeBank.Text = dtResult.Rows[0]["DRAWEE_BANK"].ToString();
                ddlInstrumentType.SelectedValue = dtResult.Rows[0]["INST_TYPE"].ToString();
                txtNo.Text = dtResult.Rows[0]["INST_NO"].ToString();
            }
            DataTable dtResultDetail = DBHandler.GetResult("Get_PaymentReceivedDetail", RcvID, Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()));
            if (dtResult.Rows.Count > 0)
            {
                panGrd.Visible = true;
                tbl.DataSource = dtResultDetail;
                tbl.DataBind();

                decimal DUE_AMT = dtResultDetail.AsEnumerable().Sum(row => row.Field<decimal>("DUE_AMT"));
                //decimal ACTUAL_AMT = dtResult.AsEnumerable().Sum(row => row.Field<decimal>("ACTUAL_AMT"));

                tbl.FooterRow.Cells[1].Text = "Total";
                tbl.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;

                tbl.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                tbl.FooterRow.Cells[2].Text = DUE_AMT.ToString("F2");

                tbl.FooterRow.Cells[4].Text = "Transaction to Sundry not consider.";
                tbl.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Left;
            }

            Button cmdSearch = (Button)dv.FindControl("cmdSearch");
            Button cmdCancel = (Button)dv.FindControl("cmdCancel");
            cmdSearch.Visible = false;
            cmdCancel.Visible = false;

            txtText.Enabled = false;
            txtYearmonth.Enabled = false;

            cmdUpdate.CommandName = "Edit";
            cmdUpdate.Text = "Update";
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string YearMonth, string VchNoFrom, string VchNoTo, string VchType)
    {
        string JSONVal = "";

        try
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            String Msg = "";
            StrFormula = "";
            int UserID;
            int SectorID;
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            StrFormula = "";

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, VchType, VchNoFrom, VchNoTo, SectorID, "", "", "", "", "");

            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    protected void tbl_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[4].ColumnSpan = 2;
            e.Row.Cells.RemoveAt(5);
        }
    }
    
    [WebMethod]
    public static string GET_SetRcvID(int type, string StrhdnRcvID)
    {
        string msg = "";
        if (type == 0) { hdnRcvID = ""; }
        else { hdnRcvID = StrhdnRcvID; }
        return msg;
    }

    [WebMethod]
    public static string GET_SetLoaneeID(int type, string StrLoaneeID)
    {
        string msg = "";
        if (type == 0) { hdnLoaneeID = ""; }
        else { hdnLoaneeID = StrLoaneeID; }
        return msg;
    }
    [WebMethod]
    public static string GET_SetBankID(int type, string StrBankID)
    {
        string msg = "";
        if (type == 0) { hdnBankID = ""; }
        else { hdnBankID = StrBankID; }
        return msg;
    }

    [WebMethod]
    public static string GET_DraweeBank(int type, string StrDraweeBank)
    {
        string msg = "";
        if (type == 0) { hdnDraweeBank = ""; }
        else { hdnDraweeBank = StrDraweeBank; }
        return msg;
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdShowCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdDone_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}