﻿$(document).ready(function () {
    $('#txtGLCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            if ($("#txtGLCode").val().trim() != '') {
                if ($("#txtGLName").val().trim() != '') {
                    BindGridGLCode();
                    $('#txtSearchGL').val($('#txtGLCode').val());
                }
                BindGlName($('#txtGLCode').val());
                $('#txtGLCode').focus();

                return false;
            }
            else {
                $('#hdnGLID').val('');
                $('#txtGLCode').val('');
                $('#txtGLName').val('');

                BindGridGLCode();
                $('#txtSearchGL').focus();
                return false;
            }

        }

    });
});
//========== End Unit Code Keypress 

$(document).ready(function () {
    $('#txtGLCode').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtGLName').val('');
            $('#hdnGLID').val('');
        }
        if (iKeyCode == 46) {
            if ($('#txtGLCode').val().length < 3) {
                $('#txtGLName').val('');
                $('#hdnGLID').val('');
            }
        }
    });
});

// ====== [ GetUnit() Removed ]

//============= Start Display Group Name
function BindGlName(GLCode) {
    var StrGLCode = '';
    StrGLCode = GLCode;
    //   $(".loading-overlay").show();
    var W = "{StrGLCode:'" + StrGLCode + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Sl.aspx/GET_GLNameByGLID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var strGLID = data.d[0].GL_ID;
                var strOldGlID = data.d[0].OLD_GL_ID;
                var strGlName = data.d[0].GL_NAME;

                $('#hdnGLID').val('');
                $('#hdnGLID').val(strGLID);
                $('#txtGLName').val('');
                $('#txtGLName').val(strGlName);
                $('#txtGLCode').val('');
                $('#txtGLCode').val(strOldGlID);
               
                // $(".loading-overlay").hide();
            }
            else {
                alert("No Records Data");
                $('#hdnGLID').val('');
                $('#txtGLCode').val('');
                $('#txtGLName').val('');
                // $(".loading-overlay").hide();
                BindGridGLCode();
                $('#txtGLCode').focus();
            }
        },
        error: function (result) {
            alert("Error Records Data...");
            //   $(".loading-overlay").hide();

        }
    });
}
//============= End Display Unit Name

//==============Start Unit Popup

function BindGridGLCode() {
    //  $(".loading-overlay").show();
    var W = "{}";

    $.ajax({
        type: "POST",
        url: "Acc_Sl.aspx/Search_GlCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            $("#grdGL").empty();
            if (data.d.length > 0) {
                $("#grdGL").append("<tr><th>Gl Code</th><th>Gl Name</th></tr>");

                for (var i = 0; i < data.d.length; i++) {
                    $("#grdGL").append("<tr><td>" +
                    data.d[i].OLD_GL_ID + "</td> <td>" +
                    data.d[i].GL_NAME + "</td></tr>");
                }
                GlPopup();
                //===================== START Search is not Blank Record Display in GridView
                if ($('#txtSearchGL').val() != '') {
                    var rows;
                    var coldata;
                    $('#grdGL').find('tr:gt(0)').hide();
                    var data = $('#txtSearchGL').val();
                    var len = data.length;
                    if (len > 0) {
                        $('#grdGL').find('tbody tr').each(function () {
                            coldata = $(this).children().eq(0);
                            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                            if (temp === 0) {
                                $(this).show();
                            }
                        });
                    }
                    else {
                        $('#grdGL').find('tr:gt(0)').show();

                    }
                }

                $(document).ready(function () {
                    $("[id*=grdGL] tbody tr").click("click", function () {
                        var strVal = $(this).children("td:eq(0)").text();
                        var valUnitName = $(this).children("td:eq(1)").text();
                        $("#txtSearchGL").val(strVal);
                        $("#txtGLName").val(valUnitName);
                        $("#txtSearchGL").focus();
                    });
                });
                //===================== END Selected Gridview in Text Box

            }
            else {
                alert("No Records Data");
                //     $(".loading-overlay").hide();
                $('#txtGrpCode').focus();
            }
        },
        error: function (result) {
            alert("Error Records Data");
            //     $(".loading-overlay").hide();

        }
    });
}
///======================= START UNIT keypress
$(document).ready(function () {
    $("#txtSearchGL").keypress(function (e) {
        if (e.keyCode == 13) {
            var GridGroup = document.getElementById("grdGL");
            var strVal = '';
            if ($('#txtSearchGL').val().trim() == '') {
                alert("Select a Gl");
                $('#txtSearchGL').val('');
                $("#txtSearchGL").focus();
                return false;
            }
            if ($('#txtSearchGL').val() != '') {
                for (var row = 1; row < GridGroup.rows.length; row++) {
                    var GridGroup_Cell = GridGroup.rows[row].cells[0];
                    var valueGrdCell = GridGroup_Cell.textContent.toString();
                    if (valueGrdCell == $('#txtSearchGL').val()) {
                        strVal = valueGrdCell;
                        break;
                    }
                }

                if (strVal != $('#txtSearchGL').val()) {
                    alert("Invalid GL ID (GL ID is not in List)");
                    $("#txtSearchGL").focus();
                    return false;
                }
                else {
                    $("#txtGLCode").val($("#txtSearchGL").val());
                    BindGlName($('#txtGLCode').val());
                    $("#txtGLCode").focus();
                    $("#dialogGL").dialog('close');
                    return false;
                }
            }
        }
    });
});

//==============End  Unit Popup

function GlPopup() {
    $("#dialogGL").dialog({
        title: "GL Search",
        width: 550,

        buttons:
        {
            Ok: function () {

                var GridGroup = document.getElementById("grdGL");
                var strVal = '';
                if ($('#txtSearchGL').val().trim() == '') {
                    alert("Select a Gl");
                    $('#txtSearchGL').val('');
                    $("#txtSearchGL").focus();
                    return false;
                }
                if ($('#txtSearchGL').val() != '') {
                    for (var row = 1; row < GridGroup.rows.length; row++) {
                        var GridGroup_Cell = GridGroup.rows[row].cells[0];
                        var valueGrdCell = GridGroup_Cell.textContent.toString();
                        if (valueGrdCell == $('#txtSearchGL').val()) {
                            strVal = valueGrdCell;
                            break;
                        }
                    }

                    if (strVal != $('#txtSearchGL').val()) {
                        alert("Invalid GL ID (GL ID is not in List)");
                        $("#txtSearchGL").focus();
                        return false;
                    }
                    else {
                        $("#txtGLCode").val($("#txtSearchGL").val());
                        BindGlName($('#txtGLCode').val());
                        $("#txtGLCode").focus();
                        $("#dialogGL").dialog('close');
                        return false;
                    }
                }
            }
        },
        modal: true
    });
}

$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtSearchGL').keyup(function () {
        $('#grdGL').find('tr:gt(0)').hide();
        var data = $('#txtSearchGL').val();
        var len = data.length;
        if (len > 0) {
            $('#grdGL').find('tbody tr').each(function () {
                coldata = $(this).children().eq(0);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        } else {
            $('#grdGL').find('tr:gt(0)').show();
        }

    });
});
