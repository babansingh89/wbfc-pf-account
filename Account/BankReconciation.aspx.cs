﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

public partial class BankReconciation : System.Web.UI.Page
{
    static String ConnString = "";
    static string StrFormula = "";
    static string BRSDate = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            //panGrd.Visible = false;

            if (!IsPostBack)
            {
                
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    //[WebMethod]
    //public static string[] BankAutoCompleteData(string Bank)
    //{
    //    List<string> result = new List<string>();
    //    DataTable dtLoaneeData = DBHandler.GetResult("Search_Bank", Bank, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
    //    foreach (DataRow dtRow in dtLoaneeData.Rows)
    //    {
    //        result.Add(string.Format("{0}|{1}", dtRow["GL_NAME1"].ToString(), dtRow["GL_ID"].ToString()));
    //    }
    //    return result.ToArray();
    //}

    [WebMethod]
    public static string[] BankAutoCompleteData(string con, string Bank)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_Bank_BankReconciation_Pf", con, Bank, 
            Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]), HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["GL_NAME1"].ToString(), dtRow["GL_ID"].ToString()));
        }
        return result.ToArray();
    }


    [WebMethod]
    public static All_ID[] BankAutoID(string BankID)
    {
        List<All_ID> result = new List<All_ID>();
        DataTable dtLoaneeData = DBHandler.GetResult("Get_AllID_Pf", BankID,  HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            All_ID DataObj = new All_ID();
            DataObj.GL_ID = dtRow["GL_ID"].ToString();
            DataObj.SL_ID = dtRow["SL_ID"].ToString();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            result.Add(DataObj);
            //result.Add(string.Format("{0}|{1}", dtRow["GL_NAME1"].ToString(), dtRow["GL_ID"].ToString()));
        }
        return result.ToArray();
    }
    public class All_ID
    {
        public string GL_ID { get; set; }
        public string SL_ID { get; set; }
        public string OLD_GL_ID { get; set; }
    }



    protected void cmdRefreshSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Report_Paravalue(string FormName, string ReportName, string ReportType, string Date, string GL_ID, string SL_ID)
    {
        string JSONVal = "";
        try
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            String Msg = "";
            StrFormula = "";
            int UserID;
            int SectorID;
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            StrFormula = "";

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, Date, GL_ID, SL_ID, SectorID, "", "", "", "", "", "");

            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }
}