﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_Acc_Scheme.aspx.cs" Inherits="LMN_Acc_Scheme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <%--   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <script type="text/jscript" src="js/BondScheme.js"></script>
    <script type="text/javascript">
        

        function beforeSave() {
            $("#frmEcom").validate();

            $("#txtCode").rules("add", { required: true, messages: { required: "Please Enter Code" } });
            $("#txtName").rules("add", { required: true, messages: { required: "Please Enter Name" } });
            $("#txtGLID").rules("add", { required: true, messages: { required: "Please Select GL ID" } });
            $("#DdlType").rules("add", { required: true, messages: { required: "Please Select Type" } });
            $("#txtTotalAmmount").rules("add", { required: true, messages: { required: "Please Enter Total Ammount" } });
            $("#txtFrom").rules("add", { required: true, messages: { required: "Please Select a Date" } });
            $("#txtTo").rules("add", { required: true, messages: { required: "Please Select a Date" } });
            $("#txtSingleUnitrate").rules("add", { required: true, messages: { required: "Please Enter Single Unit Rate" } });
            $("#txtInterestRate").rules("add", { required: true, messages: { required: "Please Enter Interest Rate" } });
            $("#ddlRepaymentmode").rules("add", { required: true, messages: { required: "Please Select Repayment Mode" } });
            //$("#txtISIN_NO").rules("add", { required: true, messages: { required: "Please enter ISIN NO" } });
            //$("#txtDPID_NO").rules("add", { required: true, messages: { required: "Please enter DPID NO" } });
            $("#DdlSlr").rules("add", { required: true, messages: { required: "Please Select SLR or NSLR" } });
            $("#txtPeriodDays").rules("add", { required: true, messages: { required: "Please Enter Period Days" } });
            $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please Select a unit" } });
            DuplicateCheckSchemeID();
        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }

        $(document).ready(function () {
            $("#txtPeriodDays").keypress(function (e) {
                if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
                    alert("Please Enter Numeric Value");

                    return false;
                }
            });
        });
      
        $(document).ready(function () {
            $('#txtFrom').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',

                onSelect: function (selected) {
                    $("#txtTo").datepicker("option", "minDate", selected)
                }
            });

        });

       

        $(document).ready(function () {
            $('#txtTo').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',
                onSelect: function (selected) {
                    $("#txtFrom").datepicker("option", "maxDate", selected)

                }
            });

        });

        $(document).ready(function () {
            $("#txtTotalAmmount,#txtSingleUnitrate,#txtInterestRate").keypress(function (event) {
                 if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
                    alert("Please Enter Numeric Value");
                    return false;
                } // prevent if not number/dot

                if (event.which == 46
                && $(this).val().indexOf('.') != -1) {
                    alert("Please Enter Numeric Value");
                    return false;
                } // prevent if already dot
            });
        });

      
        $(document).ready(function () {
            if ($('#txtCode').val() != "") {
                //$('#DdlType').attr("disabled", "disabled");
            }
            $('#txtName').keypress(function (e) {
                var regex = new RegExp("^[\\sa-zA-Z]*$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                //else {
                //    e.preventDefault();
                //    alert('Please Enter Alphabate');
                //    return false;
                //}
            });
        });
        

</script>

    <style type="text/css">
        .TableHeaderLeft {
            font-family:Segoe UI;
	        font-size:12px;	
	        color:Navy;
	        text-align:left;
        }

        .TableHeaderRight {
            font-family:Segoe UI;
	        font-size:12px;	
	        color:Navy;
	        text-align:right;
        }

        .TableHeaderCenter {
            font-family:Segoe UI;
	        font-size:12px;	
	        color:Navy;
	        text-align:center;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
     

      <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>

    <%-- ================ Group Code POP UP GRIDVIEW =====================  --%>
        <div id="dialogGL" style="display: none;font-size:12px;font-weight:bold;">
              <td class="labelCaption" style="font-size:12px;font-weight:bold"> Searching By  &nbsp;&nbsp;<span style="font-size:12px;font-weight:bold" class="require"></span> </td>
             <asp:RadioButton ID="RBGlId" autocomplete="off" runat="server" onchange="FunGLID();" Checked="true" Text="GL ID" GroupName="Software" Font-Bold="true" Font-Names="Courier New" Font-Size="Large" ForeColor="Navy"/>
            <asp:RadioButton ID="RBGlName" autocomplete="off" runat="server" onchange="FunGLName();" Text="GL Name" GroupName="Software" Font-Bold="true" Font-Names="Courier New" Font-Size="Large" ForeColor="Navy"/>
            <asp:TextBox ID="txtSearchGL" autocomplete="off" runat="server" Width="512px" CssClass="inputbox2" MaxLength="150"></asp:TextBox>

            <asp:GridView ID="grdGL" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc; width: 520px;" AutoGenerateColumns="false" CssClass="GridVendor" PageSize="5" AllowPaging="true">
                <Columns>
                    <asp:BoundField DataField="OLD_GL_ID" HeaderText="Group ID" ItemStyle-Width="150" />
                    <asp:BoundField DataField="GL_NAME" HeaderText="Group Name" ItemStyle-Width="150" />
                </Columns>
            </asp:GridView>

        </div>
        <%-- ================ END   Group Code POP UP GRIDVIEW =====================  --%>



    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Bond Scheme Master</td>
            </tr>
        </table>
        <br />
        
        <table width="100%" cellpadding="0" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="100%" CellPadding="2" AutoGenerateRows="False"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                        <InsertItemTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td class="labelCaption">Code &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCode" autocomplete="off"  ClientIDMode="Static" Width="250px" runat="server" CssClass="inputbox2" MaxLength="10"></asp:TextBox>
                                        <asp:TextBox ID="hdnSchemeID" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" style="display:none"></asp:TextBox>
                                    </td>

                                     <td class="labelCaption">Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtName" autocomplete="off"  ClientIDMode="Static" Width="250px" runat="server" CssClass="inputbox2" ></asp:TextBox>
                                    </td>

                                </tr>
                                
                                <tr>
                                     <td class="labelCaption">GL ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGLID" autocomplete="off" ReadOnly="false"  ClientIDMode="Static" Width="250px" runat="server" CssClass="inputbox2" ></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Scheme Type &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                          <asp:DropDownList autocomplete="off" ID="DdlType" CssClass="inputbox2" Width="258px" Height="25px" runat="server" AppendDataBoundItems="true" AutoPostBack="false">
                                             <asp:ListItem Text="(Select Scheme Type)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="B">Bonds</asp:ListItem>
                                            <asp:ListItem Value="D">Deposit</asp:ListItem>
                                              <asp:ListItem Value="S">Share</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                   

                                    <td class="labelCaption">Issue Open From &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" autocomplete="off" runat="server" Width="250px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                     <td class="labelCaption">Issue Open To &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTo" autocomplete="off" runat="server" Width="250px" ClientIDMode="Static"  CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>
                                
                                 <tr>
                                   

                                      <td class="labelCaption">Total Ammount &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTotalAmmount" autocomplete="off" Width="250px" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                     <td class="labelCaption">Single Unit Rate &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSingleUnitrate" autocomplete="off" runat="server" Width="250px" ClientIDMode="Static"  CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                 
                                 <tr>
                                    <td class="labelCaption">Interest rate &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInterestRate" autocomplete="off" runat="server" Width="250px" ClientIDMode="Static"  CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                     <td class="labelCaption">Repayment Mode &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlRepaymentmode" autocomplete="off" runat="server" CssClass="inputbox2" Width="258px" Height="25px" AppendDataBoundItems="true" AutoPostBack="false">
                                             <asp:ListItem Text="(Select Repayment Mode)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="M">Monthly</asp:ListItem>
                                            <asp:ListItem Value="Q">Quarterly</asp:ListItem>
                                            <asp:ListItem Value="H">Half-Yearly</asp:ListItem>
                                            <asp:ListItem Value="Y">Yearly</asp:ListItem>
                                            <asp:ListItem Value="F">Maturity</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                </tr>
                                
                                <tr>
                                    <td class="labelCaption">ISIN NO &nbsp;&nbsp;<span class="require"></span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtISIN_NO" autocomplete="off" runat="server" Width="250px" ClientIDMode="Static"  CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">DPID NO &nbsp;&nbsp;<span class="require"></span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDPID_NO" autocomplete="off" runat="server" Width="250px" ClientIDMode="Static"  CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCaption">SLR / NSLR&nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlSlr" autocomplete="off" runat="server" CssClass="inputbox2" Width="258px" Height="25px" AppendDataBoundItems="true" AutoPostBack="false">
                                             <asp:ListItem Text="(Select Repayment Mode)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="S">SLR</asp:ListItem>
                                            <asp:ListItem Value="N">NSLR</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td class="labelCaption">Period (in Month)&nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPeriodDays" autocomplete="off" runat="server" Width="250px" ClientIDMode="Static"  CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td style="width:100px;text-align:right"></td>
                                </tr>
                                
                            </table>
                        </InsertItemTemplate>


                        <EditItemTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                 <tr>
                                    <td class="labelCaption">Code &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCode" autocomplete="off"  ClientIDMode="Static" Width="250px" runat="server" CssClass="inputbox2" MaxLength="10" Text='<%# Eval("OLD_SCHEME_ID") %>'></asp:TextBox>
                                        <asp:TextBox ID="hdnSchemeID" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" style="display:none" Text='<%# Eval("SCHEME_ID") %>'></asp:TextBox>
                                    </td>

                                     <td class="labelCaption">Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtName" autocomplete="off"  ClientIDMode="Static" Width="250px" runat="server" CssClass="inputbox2" Text='<%# Eval("SCHEME_NAME") %>'></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>

                                    <td class="labelCaption">GL ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGLID" autocomplete="off"  ClientIDMode="Static" Width="250px" runat="server" CssClass="inputbox2" Text='<%# Eval("GL_ID") %>' ></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Scheme Type &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                          <asp:DropDownList ID="DdlType" autocomplete="off" CssClass="inputbox2" Width="258px" Height="25px" runat="server" AppendDataBoundItems="true"  AutoPostBack="false" Text='<%# Eval("SCHEME_TYPE") %>' Enabled="false">
                                             <asp:ListItem Text="(Select Scheme Type)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="B">Bonds</asp:ListItem>
                                            <asp:ListItem Value="D">Deposit</asp:ListItem>
                                              <asp:ListItem Value="S">Share</asp:ListItem>

                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Issue Open From &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFrom" autocomplete="off" runat="server" ClientIDMode="Static" Width="250px" CssClass="inputbox2" Text='<%# Eval("SCHEME_FROM") %>'></asp:TextBox>
                                    </td>

                                     <td class="labelCaption">Issue Open To &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTo" autocomplete="off" runat="server" ClientIDMode="Static" Width="250px" CssClass="inputbox2" Text='<%# Eval("SCHEME_TO") %>'></asp:TextBox>
                                    </td>

                                </tr>
                                 <tr>
                                      <td class="labelCaption">Total Ammount &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTotalAmmount" autocomplete="off" runat="server" Width="250px" ClientIDMode="Static" CssClass="inputbox2" Text='<%# Eval("AMOUNT") %>'></asp:TextBox>
                                    </td>

                                     <td class="labelCaption">Single Unit Rate &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSingleUnitrate" autocomplete="off" runat="server" ClientIDMode="Static" Width="250px" CssClass="inputbox2" Text='<%# Eval("UNIT_RATE") %>'></asp:TextBox>
                                    </td>

                                </tr>
                                 <tr>
                                    <td class="labelCaption">Interest rate &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInterestRate" autocomplete="off" runat="server" ClientIDMode="Static" Width="250px" CssClass="inputbox2" Text='<%# Eval("INT_RATE") %>'></asp:TextBox>
                                    </td>

                                      <td class="labelCaption">Repayment Mode &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlRepaymentmode" autocomplete="off" runat="server" CssClass="inputbox2" Width="258px" Height="25px" AppendDataBoundItems="true" AutoPostBack="false" Text='<%# Eval("REPAYMENT_MODE") %>'>
                                             <asp:ListItem Text="(Select Repayment Mode)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="M">Monthly</asp:ListItem>
                                            <asp:ListItem Value="Q">Quarterly</asp:ListItem>
                                            <asp:ListItem Value="H">Half-Yearly</asp:ListItem>
                                            <asp:ListItem Value="Y">Yearly</asp:ListItem>
                                            <asp:ListItem Value="F">Maturity</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">ISIN NO &nbsp;&nbsp;<span class="require"></span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtISIN_NO" autocomplete="off" runat="server" ClientIDMode="Static" Width="250px" CssClass="inputbox2"  Text='<%# Eval("ISIN_NO") %>'></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">DPID NO &nbsp;&nbsp;<span class="require"></span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDPID_NO" autocomplete="off" runat="server" ClientIDMode="Static" Width="250px" CssClass="inputbox2"  Text='<%# Eval("DPID_NO") %>'></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="labelCaption">SLR / NSLR&nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlSlr" autocomplete="off" runat="server" CssClass="inputbox2" Width="258px" Height="25px" AppendDataBoundItems="true" AutoPostBack="false" Text='<%# Eval("SLR_NSLR") %>'>
                                             <asp:ListItem Text="(Select Repayment Mode)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="S">SLR</asp:ListItem>
                                            <asp:ListItem Value="N">NSLR</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td class="labelCaption">Period (in Month)&nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPeriodDays" autocomplete="off" runat="server" ClientIDMode="Static" Width="250px" CssClass="inputbox2" Text='<%# Eval("PERIOD_DAYS") %>'></asp:TextBox>
                                    </td>
                                    <td style="width:100px;text-align:right"></td>
                                </tr>
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
             <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="SCHEME_ID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader"/>
                                    <ItemTemplate >
                                        <asp:ImageButton CommandName='Select' autocomplete="off" ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("SCHEME_ID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton CommandName='Del' autocomplete="off" ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("SCHEME_ID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("SCHEME_ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                            
                                <asp:BoundField DataField="OLD_SCHEME_ID" ItemStyle-CssClass="TableHeaderLeft" HeaderStyle-CssClass="TableHeader" HeaderText="Code" />
                                <asp:BoundField DataField="SCHEME_NAME" ItemStyle-CssClass="TableHeaderLeft" HeaderStyle-CssClass="TableHeader" HeaderText="Name" />
                                <asp:BoundField DataField="GL_ID"  ItemStyle-CssClass="TableHeaderCenter" HeaderStyle-CssClass="TableHeader" HeaderText="GL ID" />
                                <asp:BoundField DataField="SCHEME_TYPE"  ItemStyle-CssClass="TableHeaderCenter" HeaderStyle-CssClass="TableHeader" HeaderText="Scheme Type" />
                                <asp:BoundField DataField="AMOUNT" ItemStyle-CssClass="TableHeaderRight" HeaderStyle-CssClass="TableHeader" HeaderText="Total Ammount" />  
                                <asp:BoundField DataField="SCHEME_FROM" ItemStyle-CssClass="TableHeaderCenter" HeaderStyle-CssClass="TableHeader" HeaderText="Issue Open From" />
                                <asp:BoundField DataField="SCHEME_TO" ItemStyle-CssClass="TableHeaderCenter" HeaderStyle-CssClass="TableHeader" HeaderText="Issue Open To" /> 
                                <asp:BoundField DataField="UNIT_RATE" ItemStyle-CssClass="TableHeaderRight" HeaderStyle-CssClass="TableHeader" HeaderText="Single Unit Rate" /> 
                                <asp:BoundField DataField="INT_RATE" ItemStyle-CssClass="TableHeaderRight" HeaderStyle-CssClass="TableHeader" HeaderText="Interest Rate" />
                                <asp:BoundField DataField="REPAYMENT_MODE" ItemStyle-CssClass="TableHeaderCenter" HeaderStyle-CssClass="TableHeader" HeaderText="Repayment Mode" />
                                <%--<asp:BoundField DataField="ISIN_NO" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="ISIN NO" />--%>   
                                <%--<asp:BoundField DataField="DPID_NO" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="DPID No" />--%>
                                <asp:BoundField DataField="SLR_NSLR" ItemStyle-CssClass="TableHeaderCenter" HeaderStyle-CssClass="TableHeader" HeaderText="SLR / NSLR" />
                                <%--<asp:BoundField DataField="PERIOD_DAYS" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Peroid Days" />--%>
                                                      
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

