﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;

public partial class Fixed_Asset_Location : System.Web.UI.Page
{
    static string hdnID = "";
    static int ddlMLocID;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
                Mainloc();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
        
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
                Mainloc();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    
    public void UpdateDeleteRecord(int flag, string ID) 
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_FixedAssetLocID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                DropDownList ddlMLoc = (DropDownList)dv.FindControl("ddlMLoc");
                //ddlMLoc.SelectedValue = dtResult.Rows[0]["LOC_ID"].ToString();
                ddlMLoc.SelectedValue = dtResult.Rows[0]["P_LOC_ID"].ToString();
                hdnID = dtResult.Rows[0]["LOC_ID"].ToString();
            }
            else if(flag==2)
            {
                DBHandler.Execute("Delete_FixedAssetLocID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch(Exception ex)
        {
            throw new Exception(ex.Message);
        }     
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
         {
             Button cmdSave = (Button)sender;
            
            if (cmdSave.CommandName == "Add")
             {
                 TextBox txtLDesc = (TextBox)dv.FindControl("txtLDesc");
                 DropDownList ddlMLoc = (DropDownList)dv.FindControl("ddlMLoc");
                if (ddlMLoc.SelectedValue == "") { ddlMLocID = 0; }
                DBHandler.Execute("Insert_FixedAssetLoc", txtLDesc.Text,
                     ddlMLocID,//ddlMLoc.SelectedValue.Equals("") ? DBNull.Value : (object)Convert.ToInt32(ddlMLoc.SelectedValue),
                     Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                 cmdSave.Text = "Create";
                 dv.ChangeMode(FormViewMode.Insert);
                 PopulateGrid();
                
                 dv.DataBind();
                 ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
             }
             else if (cmdSave.CommandName == "Edit")
             {
                 TextBox txtLDesc = (TextBox)dv.FindControl("txtLDesc");
                 DropDownList ddlMLoc = (DropDownList)dv.FindControl("ddlMLoc");
                string ID = hdnID;
                if (ddlMLoc.SelectedValue == "") { ddlMLocID = 0; }
                DBHandler.Execute("Update_FixedAssetLocID", Convert.ToInt32(hdnID), txtLDesc.Text, ddlMLocID, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                 cmdSave.Text = "Create";
                 dv.ChangeMode(FormViewMode.Insert);
                 PopulateGrid();
                 dv.DataBind();
                 ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
             }
         }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "edt")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch(Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    public void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_FixedAssetLoc");
            gv.DataSource = dt;
            gv.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public DataTable Mainloc()
    {
        try
        { 
         //DropDownList ddlMLoc=(DropDownList)dv.FindControl("ddlMLoc");
         DataTable dt = DBHandler.GetResult("Get_FixedAssetLocation");
         //ddlMLoc.DataSource=dt;
         //ddlMLoc.DataValueField = "LOC_ID";
         //ddlMLoc.DataTextField = "LOC_DESC";
         //ddlMLoc.DataBind();
         return dt;
         
        }
        catch(Exception ex)
        {
         throw new Exception(ex.Message);
        }
    }
}