﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;

public partial class LMN_Industry_Type : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_Industry_Type_BY_ID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                PopulateGrid();

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_Industry_Type", ID);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();
            }
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtindustrytype = (TextBox)dv.FindControl("txtindustrytype");
                TextBox txtIndusTypeDesc = (TextBox)dv.FindControl("txtIndusTypeDesc");
                DBHandler.Execute("Insert_Industry_Type", txtindustrytype.Text,txtIndusTypeDesc.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Inserted Successfully.')</script>");
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();
                
            }
            else if (cmdSave.CommandName == "Edit")
            {

                TextBox txtindustrytype = (TextBox)dv.FindControl("txtindustrytype");
                TextBox txtIndusTypeDesc = (TextBox)dv.FindControl("txtIndusTypeDesc");

                //string ID = ((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("Update_industry_type",txtindustrytype.Text,txtIndusTypeDesc.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                cmdSave.Text = "Save";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
        //
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Industry_Type");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //protected DataTable drpload()
    //{
    //    try
    //    {
    //        DataTable dt = DBHandler.GetResult("LoadCountry");
    //        return dt;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}
}