﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;   
using System.Text;
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Globalization;

public partial class LMN_Group_Master : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_GroupType_BY_ID_PF", ID);
                Session["ID"] = ID;
                hdnGroupID.Text = ID;
                dv.DataSource = dtResult;
                dv.DataBind();

            }
            else if (flag == 2)
            {
                DataTable dtGroupID = DBHandler.GetResult("Get_CheckGroupIDOnGL_Pf", ID);
                if (dtGroupID.Rows.Count > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Can Not Delete Record! This Group ID is Already Used On GL Master')</script>");

                }
                else
                {
                    DBHandler.Execute("Delete_Group_Type_Pf", ID);
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;

            if (cmdSave.CommandName == "Add")
            {
                
                TextBox valtxtoldgroupid = (TextBox)dv.FindControl("txtoldgroupid");
                TextBox valGroupName = (TextBox)dv.FindControl("txtgroupname");
                DropDownList valGroupType = (DropDownList)dv.FindControl("DdlGrouptype");
                TextBox valGroupSchedule = (TextBox)dv.FindControl("txtgroupschedule");
                DBHandler.Execute("Insert_Lmn_Groupmaster_Pf", valtxtoldgroupid.Text, valGroupName.Text, valGroupType.Text, valGroupSchedule.Text, 
                    Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]),Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]),
                    HttpContext.Current.Session["Menu_For"].ToString());
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {
                String ID = (String)Session["ID"];

                TextBox valtxtoldgroupid = (TextBox)dv.FindControl("txtoldgroupid");
                TextBox valGroupName = (TextBox)dv.FindControl("txtgroupname");
                DropDownList valGroupType = (DropDownList)dv.FindControl("DdlGrouptype");
                TextBox valGroupSchedule = (TextBox)dv.FindControl("txtgroupschedule");
                DBHandler.Execute("Update_Group_Type_Pf", ID, valtxtoldgroupid.Text, valGroupName.Text, valGroupType.Text, valGroupSchedule.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                Session["ID"] = null;
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    private void PopulateGrid()
    {
        try
        {
            string a = HttpContext.Current.Session["Menu_For"].ToString();
            DataTable dt = DBHandler.GetResult("Get_Group_type_PF", HttpContext.Current.Session["Menu_For"].ToString());
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //============================ Start Duplicate Check Group ID Coding Start here===================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DupChkGroupID(string OldGroup_ID, string Group_ID)
    {
        string GroupID = "";
      
        DataTable dtGetData = DBHandler.GetResult("Get_DupCheckGroupID_Pf", OldGroup_ID, Group_ID, HttpContext.Current.Session["Menu_For"].ToString());
        if (dtGetData.Rows.Count > 0)
        {
            GroupID = Convert.ToString(dtGetData.Rows[0]["OldGroupID"]);
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(GroupID);
    }
    //============================End Duplicate Check Group ID Coding Start here===================================//

}