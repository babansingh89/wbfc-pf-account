﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using System.Configuration;

public partial class SubLedgerReport : System.Web.UI.Page
{
    static DataTable MST_Table;
    static DataTable[] DT_Details = new DataTable[100];
    static int MaxLength = -1, TempMax = 0;
    static string FromDate;
    static string ToDate;
    static decimal DRAmount = 0;
    static decimal CRAmount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateGrid();
            MST_Table = null;
            if (MaxLength != -1)
            {
                for (int i = 0; i <= MaxLength; i++)
                {
                    DT_Details[i] = null;
                }
            }
            MaxLength = -1;
            TempMax = 0;
            FromDate = "";
            ToDate = "";
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("VCH_DATE");
            dt.Columns.Add("DOC_TYPE");
            dt.Columns.Add("PARTICULARS");
            dt.Columns.Add("BK_GLSL_CODE");
            dt.Columns.Add("DEBIT");
            dt.Columns.Add("CREDIT");
            dt.Rows.Add();
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected DataTable DataTableGLload()
    {
       
        DataTable dt = DBHandler.GetResult("Get_GLForSubLDropDown_Pf");
        return dt;
    }

    protected void ddlGLdetails_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTableSubLedgerLoad();
    }

    protected void DataTableSubLedgerLoad()
    {
        RadioButtonList ddlGLdetails = (RadioButtonList)dv.FindControl("ddlGLdetails");
        CheckBoxList chkSubLedger = (CheckBoxList)dv.FindControl("chkSubLedger");
        CheckBox chkAllSubLedger = (CheckBox)dv.FindControl("chkAllSubLedger");
        DropDownList ddlUserSector = Master.FindControl("ddlUserSector") as DropDownList;
        int SecID = Convert.ToInt32(ddlUserSector.SelectedItem.Value);
        chkSubLedger.Items.Clear();
        string str = ddlGLdetails.SelectedValue;
        DataTable dt = DBHandler.GetResult("Get_SubGLByGLID_Pf", str, SecID);
        chkSubLedger.DataSource = dt;
        chkSubLedger.DataValueField = "SL_ID";
        chkSubLedger.DataTextField = "GL_NAME";
        chkSubLedger.DataBind();
        chkAllSubLedger.Checked = false;

    }

    protected DataTable DataTableSector()
    {
        DataTable dt = DBHandler.GetResult("Get_UserWiseSector_Pf", Session[SiteConstants.SSN_INT_USER_ID], HttpContext.Current.Session["Menu_For"].ToString());
        return dt;
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        int SecLen = 0;
        DRAmount = 0;
        CRAmount = 0;
        TextBox txtFromDate = (TextBox)dv.FindControl("txtFromDate");
        TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
        CheckBox chkNarration = (CheckBox)dv.FindControl("chkNarration");
        CheckBox chkInstrument = (CheckBox)dv.FindControl("chkInstrument");
        RadioButtonList ddlGLdetails = (RadioButtonList)dv.FindControl("ddlGLdetails");
        CheckBoxList chkSubLedger = (CheckBoxList)dv.FindControl("chkSubLedger");
        CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");

        FromDate = txtFromDate.Text.ToString();
        ToDate = txtToDate.Text.ToString();
        DateTime fDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);
        DateTime tDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);

        string Narration;
        if (chkNarration.Checked)
        {
            Narration = "y";
        }
        else
        {
            Narration = "n";
        }
        string Instrument;
        if (chkInstrument.Checked)
        {
            Instrument = "y";
        }
        else
        {
            Instrument = "n";
        }

        string GLDetails = ddlGLdetails.SelectedValue.ToString();

        string SubLedgerDetails = "";
        for (int i = 0; i < chkSubLedger.Items.Count; i++)
        {
            if (chkSubLedger.Items[i].Selected)
            {
                SubLedgerDetails += chkSubLedger.Items[i].Value.ToString() + ",";
            }
        }  

        string SectorDetails = "";
        for (int i = 0; i < ddlSector.Items.Count; i++)
        {
            if (ddlSector.Items[i].Selected)
            {
                SectorDetails += ddlSector.Items[i].Value.ToString() + ",";
                SecLen++;
            }
        }

        //===================== Start Sector Name , Address And Org Name code Here ==============================//

        txtOrgName.Text = "";
        txtOrgAddress.Text = "";
        DataTable dtSec = DBHandler.GetResult("GET_SecNameWithAddress_Pf", SectorDetails, SecLen);
        if (dtSec.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dtSec.Rows)
            {
                if (txtOrgName.Text == "")
                {
                    txtOrgName.Text = dtRow["OrgName"].ToString();
                    txtOrgAddress.Text = dtRow["Sector"].ToString();
                }
                else
                {
                    txtOrgAddress.Text = txtOrgAddress.Text + " , " + dtRow["Sector"].ToString();
                }
            }
        }

        lblOffice1.Text = txtOrgName.Text;
        lblOffice2.Text = txtOrgAddress.Text;
        //===================== End Sector Name , Address And Org Name code Here ==============================//

        if (FromDate == "" || ToDate == "" || GLDetails == "" || SubLedgerDetails == "" || SectorDetails == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Select Required Fields Prior To Report Generation!')</script>");
        }
        else if (fDate > tDate)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('From Date Cannot Be Greater Than To Date!')</script>");
        }
        else
        {
            MST_Table = DBHandler.GetResult("Acc_Sl_Ledger_Pf", FromDate, ToDate, GLDetails, SubLedgerDetails, Narration, Instrument, SectorDetails);
            //string OLD_GL_ID;

            if (MST_Table.Rows.Count > 0)
            {
                string temp = "";
                int i = -1;
                  foreach (DataRow Mst_Obj in MST_Table.Rows)
                {
                    if (temp == "" || temp != Mst_Obj["OLD_SL_ID"].ToString())
                    {
                        i = i + 1;
                        MaxLength = i;
                        DT_Details[i] = new DataTable();
                        DT_Details[i].Columns.Add("SL_NO");
                        DT_Details[i].Columns.Add("GL_ID");
                        DT_Details[i].Columns.Add("OLD_GL_ID");
                        DT_Details[i].Columns.Add("GL_NAME");
                        DT_Details[i].Columns.Add("SL_ID");
                        DT_Details[i].Columns.Add("OLD_SL_ID");
                        DT_Details[i].Columns.Add("BK_GL_NAME");
                        DT_Details[i].Columns.Add("BK_GLSL_CODE");
                        DT_Details[i].Columns.Add("VCH_DATE");
                        DT_Details[i].Columns.Add("DOC_TYPE");
                        DT_Details[i].Columns.Add("PARTICULARS");
                        DT_Details[i].Columns.Add("DEBIT");
                        DT_Details[i].Columns.Add("CREDIT");
                        DT_Details[i].Rows.Add();

                        DT_Details[i].Rows.RemoveAt(0);

                        DT_Details[i].Rows.Add(Mst_Obj.ItemArray);

                        temp = Mst_Obj["OLD_SL_ID"].ToString();
                        if (Mst_Obj["PARTICULARS"].ToString() != "OPENING BALANCE" && Mst_Obj["PARTICULARS"].ToString() != "CLOSING BALANCE")
                        {
                            DRAmount = DRAmount + Convert.ToDecimal(Mst_Obj["DEBIT"]);
                            CRAmount = CRAmount + Convert.ToDecimal(Mst_Obj["CREDIT"]);
                        }
                    }
                    else
                    {
                        DT_Details[i].Rows.Add(Mst_Obj.ItemArray);
                        if (Mst_Obj["PARTICULARS"].ToString() != "OPENING BALANCE" && Mst_Obj["PARTICULARS"].ToString() != "CLOSING BALANCE")
                        {
                            DRAmount = DRAmount + Convert.ToDecimal(Mst_Obj["DEBIT"]);
                            CRAmount = CRAmount + Convert.ToDecimal(Mst_Obj["CREDIT"]);
                        }
                    }
                }
            }

            if (MaxLength != -1)
            {
                tbl.DataSource = DT_Details[0];
                tbl.DataBind();
                lblPage.Text = "Page : " + Convert.ToString(TempMax + 1) + " of " + Convert.ToString(MaxLength + 1);
                lblDuration.Text = "SUB LEDGER FROM  " + FromDate + "  TO  " + ToDate;
                lblReportName.Text = DT_Details[0].Rows[0]["GL_NAME"].ToString() + " (" + DT_Details[0].Rows[0]["OLD_SL_ID"].ToString() + ")";

                if (DT_Details[0].Rows.Count <= 15)
                {
                    double TotalDebit = 0, TotalCredit = 0;
                    tbl.FooterRow.Cells[0].Text = "Total :";
                    tbl.FooterRow.Cells[2].Text = "Total Transaction During the period **** DEBIT: " + DRAmount + "**** CREDIT: " +CRAmount;
                    tbl.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Center;
                   
                    foreach (DataRow dtRow in DT_Details[0].Rows)
                    {
                        TotalDebit += Convert.ToDouble(dtRow["DEBIT"].ToString());
                        TotalCredit += Convert.ToDouble(dtRow["CREDIT"].ToString());
                    }
                    tbl.FooterRow.Cells[4].Text = String.Format("{0:0.00}",TotalDebit);
                    tbl.FooterRow.Cells[5].Text = String.Format("{0:0.00}",TotalCredit);

                   }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No Record Found With These Specifications!')</script>");
            }
        }

    }

   
    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        try
        {
            if (MaxLength == -1)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Generate Report Prior To Report Redirection!')</script>");
            }
            else
            {
                if (TempMax > 0)
                {
                    TempMax -= 1;
                    tbl.DataSource = DT_Details[TempMax];
                    tbl.DataBind();
                    lblDuration.Text = "SUB LEDGER FROM " + FromDate + " TO " + ToDate;
                    lblReportName.Text = DT_Details[TempMax].Rows[1]["GL_NAME"].ToString() + " " + DT_Details[TempMax].Rows[1]["OLD_SL_ID"].ToString();
                    lblPage.Text = "Page : " + Convert.ToString(TempMax + 1) + " of " + Convert.ToString(MaxLength + 1);

                    if (DT_Details[TempMax].Rows.Count <= 15)
                    {
                        double TotalDebit = 0, TotalCredit = 0;
                        tbl.FooterRow.Cells[0].Text = "Total :";
                        foreach (DataRow dtRow in DT_Details[TempMax].Rows)
                        {
                            TotalDebit += Convert.ToDouble(dtRow["DEBIT"].ToString());
                            TotalCredit += Convert.ToDouble(dtRow["CREDIT"].ToString());
                        }
                        tbl.FooterRow.Cells[4].Text = String.Format("{0:0.00}", TotalDebit);
                        tbl.FooterRow.Cells[5].Text = String.Format("{0:0.00}", TotalCredit);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Reached Start Page! You Cannot Go Previous!')</script>");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            if (MaxLength == -1)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Generate Report Prior To Report Redirection!')</script>");
            }
            else
            {
                if (TempMax < MaxLength)
                {
                    TempMax += 1;
                    tbl.DataSource = DT_Details[TempMax];
                    tbl.DataBind();
                    lblDuration.Text = "SUB LEDGER FROM " + FromDate + " TO " + ToDate;
                    lblReportName.Text = DT_Details[TempMax].Rows[1]["GL_NAME"].ToString() + "  (" + DT_Details[TempMax].Rows[1]["OLD_SL_ID"].ToString() + ")";
                    lblPage.Text = "Page : " + Convert.ToString(TempMax + 1) + " of " + Convert.ToString(MaxLength + 1);

                    if (DT_Details[TempMax].Rows.Count <= 15)
                    {
                        double TotalDebit = 0, TotalCredit = 0;
                        tbl.FooterRow.Cells[0].Text = "Total :";
                        foreach (DataRow dtRow in DT_Details[TempMax].Rows)
                        {
                            TotalDebit += Convert.ToDouble(dtRow["DEBIT"].ToString());
                            TotalCredit += Convert.ToDouble(dtRow["CREDIT"].ToString());
                        }
                        tbl.FooterRow.Cells[4].Text = String.Format("{0:0.00}", TotalDebit);
                        tbl.FooterRow.Cells[5].Text = String.Format("{0:0.00}", TotalCredit);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Reached END Page! You Cannot Go Next!')</script>");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        tbl.PageIndex = e.NewPageIndex;
        tbl.DataSource = DT_Details[TempMax];
        tbl.DataBind();
        if (Convert.ToInt32(tbl.PageCount) == (Convert.ToInt32(tbl.PageIndex) + 1) || DT_Details[TempMax].Rows.Count <= 15 || (DT_Details[TempMax].Rows.Count % 15 == 0 && Convert.ToInt32(tbl.PageCount) == Convert.ToInt32(tbl.PageIndex)))
        {
            double TotalDebit = 0, TotalCredit = 0;
            tbl.FooterRow.Cells[0].Text = "Total :";
            tbl.FooterRow.Cells[2].Text = "Total Transaction During the period **** DEBIT: " + DRAmount + "**** CREDIT: " + CRAmount;
            tbl.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Center;
            foreach (DataRow dtRow in DT_Details[TempMax].Rows)
            {
                TotalDebit += Convert.ToDouble(dtRow["DEBIT"].ToString());
                TotalCredit += Convert.ToDouble(dtRow["CREDIT"].ToString());
            }
            tbl.FooterRow.Cells[4].Text = String.Format("{0:0.00}", TotalDebit);
            tbl.FooterRow.Cells[5].Text = String.Format("{0:0.00}", TotalCredit);
        }
    }

    [WebMethod]
    public static DataTableDetails[] GET_PrintSingleDataTable()
    {
        List<DataTableDetails> Detail = new List<DataTableDetails>();
        foreach (DataRow dtRow in DT_Details[TempMax].Rows)
        {
            DataTableDetails DataObj = new DataTableDetails();

            DataObj.SL_NO = dtRow["SL_NO"].ToString();
            DataObj.GL_ID = dtRow["GL_ID"].ToString();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            DataObj.SL_ID = dtRow["SL_ID"].ToString();
            DataObj.OLD_SL_ID = dtRow["OLD_SL_ID"].ToString();
            DataObj.BK_GL_NAME = dtRow["BK_GL_NAME"].ToString();
            DataObj.BK_GLSL_CODE = dtRow["BK_GLSL_CODE"].ToString();
            DataObj.VCH_DATE = dtRow["VCH_DATE"].ToString();
            DataObj.DOC_TYPE = dtRow["DOC_TYPE"].ToString();
            DataObj.PARTICULARS = dtRow["PARTICULARS"].ToString();
            DataObj.DEBIT = dtRow["DEBIT"].ToString();
            DataObj.CREDIT = dtRow["CREDIT"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }

    public class DataTableDetails //Class for binding data
    {
        public string SL_NO { get; set; }
        public string GL_ID { get; set; }
        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
        public string SL_ID { get; set; }
        public string OLD_SL_ID { get; set; }
        public string BK_GL_NAME { get; set; }
        public string VCH_DATE { get; set; }
        public string DOC_TYPE { get; set; }
        public string PARTICULARS { get; set; }
        public string DEBIT { get; set; }
        public string CREDIT { get; set; }
        public string BK_GLSL_CODE { get; set; }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GET_PrintMultiTableData()
    {
        int MaxDivider = MaxLength + 1;
        int Count;
        string[] PrintString = new string[MaxDivider * 2];
        double DebitTotal, CreditTotal;
        for (int i = 0; i <= MaxLength; i++)
        { 
            string GlName = "", OldSLName = "";
            Count = 0;
            PrintString[i] = "";
            DebitTotal = 0.00;
            CreditTotal = 0.00;
            foreach (DataRow dtRow in DT_Details[i].Rows)
            {
                Count += 1;
                PrintString[i] += "<tr><td>" + dtRow["VCH_DATE"].ToString() + "</td><td>" + dtRow["DOC_TYPE"].ToString() + "</td><td>" + dtRow["PARTICULARS"].ToString() + "</td><td>" + dtRow["BK_GLSL_CODE"].ToString() + "</td><td align='right'>" + dtRow["DEBIT"].ToString() + "</td><td align='right'>" + dtRow["CREDIT"].ToString() + "</td></tr>";
                DebitTotal = DebitTotal + Convert.ToDouble(dtRow["DEBIT"].ToString());
                CreditTotal = CreditTotal + Convert.ToDouble(dtRow["CREDIT"].ToString());
                if (Count == DT_Details[i].Rows.Count - 1)
                {
                    GlName = dtRow["GL_NAME"].ToString(); //GL_NAME
                    OldSLName = dtRow["OLD_SL_ID"].ToString();
                }
            } 
            PrintString[i] += "<tr><td colspan='6'><label>=====================================================================================================================</label></td></tr>";
            PrintString[i] += "<tr><td>TOTAL:</td><td></td><td></td><td></td><td align='right'>" + DebitTotal + "</td><td align='right'>" + CreditTotal + "</td></tr>";
            PrintString[i] += "<tr><td colspan='6'><label>=====================================================================================================================</label></td></tr>";
            PrintString[i + MaxDivider] = "<label>" + GlName + " (" + OldSLName + ")" + "</label>";
        }
        return PrintString;
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            MST_Table = null;
            if (MaxLength != -1)
            {
                for (int i = 0; i <= MaxLength; i++)
                {
                    DT_Details[i] = null;
                }
            }
            MaxLength = -1;
            TempMax = 0;
            FromDate = "";
            ToDate = "";
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}