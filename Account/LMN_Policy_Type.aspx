﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_Policy_Type.aspx.cs" Inherits="LMN_Policy_Type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function beforeSave() {
            $("#frmEcom").validate();

            $("#TxtPolicyTypeDESC").rules("add", { required: true, messages: { required: "Policy Type description can't be empty" } });
        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>
   <asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>LMN POLICY TYPE MASTER</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                               <tr>
                                    <td class="labelCaption">Policy Type Desc&nbsp;&nbsp<span="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtPolicyTypeDESC" autocomplete="off"  ClientIDMode="Static" runat="server" CssClass="inputbox2" ></asp:TextBox>
                                    </td>
                               </tr>
                            </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td>
                                        <td class="labelCaption">Policy Type Desc&nbsp;&nbsp<span="require">*</span> </td>
                                        <td class="labelCaption">:</td>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TxtPolicyTypeDESC" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("PolicyTypeDESC") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                    
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="PolicyTypeID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("PolicyTypeID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("PolicyTypeID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("PolicyTypeID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                                <asp:BoundField DataField="PolicyTypeDESC" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Policy type Desc" />                                                                
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

