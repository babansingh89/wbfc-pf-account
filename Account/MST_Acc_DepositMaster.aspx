﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MST_Acc_DepositMaster.aspx.cs" Inherits="MST_Acc_DepositMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

	<style type="text/css">


			.textbox {
			border: 1px solid #c4c4c4;
			height: 15px;
			width: 210px;
			font-size: 13px;
			padding: 4px 4px 4px 4px;
			border-radius: 4px;
			-moz-border-radius: 4px;
			-webkit-border-radius: 4px;
			box-shadow: 0px 0px 8px #d9d9d9;     
			-moz-box-shadow: 0px 0px 8px #d9d9d9;
			-webkit-box-shadow: 0px 0px 8px #d9d9d9;
		}

			.textbox:focus {
				outline: none;
				border: 1px solid #7bc1f7;
				box-shadow: 0px 0px 8px #7bc1f7;
				-moz-box-shadow: 0px 0px 8px #7bc1f7;
				-webkit-box-shadow: 0px 0px 8px #7bc1f7;
			}
            body {
			font-family: Arial;
			font-size: 10pt;
		}

		td {
		}

		table {
			border: 1px solid #ccc;
			border-collapse: collapse;
			background-color: #fff;
			font-size: 10pt !important;
			font-family: Arial !important;
		}

			table th {
				background-color: #B8DBFD;
				color: #333;
				font-weight: bold;
			}

			table, table table td {
				border: 0px solid #ccc;
			}
		/*table, th, td {
			cellpadding:1px;
			cellspacing:1px;
		}*/
		th {
			background: lightskyblue;
		}

		.selected_row {
			background-color: #A1DCF2 !important;
		}

		.Gridposition {
			margin-left: 10px;
			width: 300px;
		}

		.textposition {
			margin-left: 1px;
			width: 295px;
		}

		.GridFolioDes {
			margin-left: 1px;
			width: 570px;
		}

		.Foliotextposition {
			margin-left: 2px;
			width: 565px;
		}

		.TextSearchVendor {
			margin-left: 1px;
			width: 516px;
		}

		.GridVendor {
			margin-left: 1px;
			width: 520px;
		}

		.GridAllIndent {
			margin-left: 1px;
			width: 800px;
		}

		.input3 {
			margin-left: 1px;
			width: 200px;
		}

		/*=================================================*/
		.Grid {
			border: solid 1px #59bdcc;
			border-collapse: collapse;
			width: 100%;
			text-align: center;
		}

			.Grid td {
				padding: 2px;
				border: solid 1px #59bdcc;
				color: navy;
				background-color: #e6f3be;
			}

			.Grid th {
				padding: 4px 2px;
				background: #609eb3;
				font-size: 0.9em;
			}

		.auto-style1 {
			width: 802px;
		}

		.auto-style2 {
			font-family: Segoe UI;
			font-size: 12px;
			color: Navy;
			text-align: left;
			width: 802px;
		}
                    
        .blink {
              animation: blink-animation 1s steps(5, start) infinite;
              -webkit-animation: blink-animation 1s steps(5, start) infinite;
            }
            @keyframes blink-animation {
              to {
                visibility: hidden;
              }
            }
            @-webkit-keyframes blink-animation {
              to {
                visibility: hidden;
              }
            }

	</style>


    <script type="text/javascript">
        //Global Variable
        var gblSchemeID = '';
        var gblSchemeType = '';


        // Show Message on pageload..
        function LoadPage(result) {
            alert(result);
        }

        $(document).ready(function () {

            $("input").focus(function () {
                $(this).css("background-color", "#cccccc");
            });
            $("input").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

            $("select").focus(function () {
                $(this).css("background-color", "#cccccc");
            });

            $("select").blur(function () {
                $(this).css("background-color", "#ffffff");
            });


            $('input[type="text"]').attr('autocomplete', 'off');
            $('input[type="text"]').bind("cut copy paste", function (e) {
                e.preventDefault();
            });

            $('#txtIssue_DT, #txtPeriod').on('keydown', function (e) {
                e.preventDefault();
            });

        });

        $(document).ready(function () {
            $("#txtDeposit_ID").keydown(function (e) {
                if (e.keyCode == 9 || e.keyCode == 13) {
                    if ($('#txtDeposit_ID').val().trim() == '') {
                        $('#txtQty').focus();
                        return false;
                    }
                    else {
                        CheckDuplicateBondID();
                        $('#txtQty').focus();
                        return false;
                    }
                }
            });
        });

        function CheckDuplicateBondID() {
            var Y = "{txtDeposit_ID:'" + $('#txtDeposit_ID').val().trim() + "', gblSchemeID:'" + gblSchemeID + "',gblSchemeType:'" + gblSchemeType + "',SectorID:'" + $('#ddlUserSector').val() + "'}";
            $.ajax({
                type: "POST",
                url: "MST_Acc_DepositMaster.aspx/Check_Deposit_ID",
                contentType: "application/json;charset=utf-8",
                data: Y,
                dataType: "json",
                success: function (data) {
                    if (data.d == '0') {
                        alert("Bond Holder ID Already Exist");
                        $('#txtDeposit_ID').focus();
                        return false;
                    }
                    else {
                        //
                    }
                }
            });
        }


        $(document).ready(function () {
            if ($('#txtSchemeID').val() == '') { $('#txtSchemeID').focus(); }
            $("#txtSaveEdit").val('Add');
            $("#txtF15H_FOR_FY").keydown(function (event) {
                if (event.which == 8)
                { return false; }
                event.preventDefault();
                //  BindGetSchemecode();
            });
            //$("#txtDeposit_ID").keydown(function (event) {
            //    if (event.which == 8)
            //    { return false; }
            //    event.preventDefault();
            //});
            //$("#txtMaturity_DT").keydown(function (event) {
            //    if (event.which == 8)
            //    { return false; }
            //    event.preventDefault();
            //});
            $("#txtDistinctive_End").keydown(function (event) {
                if (event.which == 8)
                { return false; }
                event.preventDefault();
            });

            $("#txtSchemeID").keydown(function (event) {
                if (event.which == 8)
                { return false; }
                event.preventDefault();
                //  BindGetSchemecode();
            });
            $("#txtBranchID").keydown(function (event) {
                if (event.which == 8)
                { return false; }
                event.preventDefault();
            });
            $("#txtTDSID").keydown(function (event) {
                if (event.which == 8)
                { return false; }
                event.preventDefault();

            });

            $(document).ready(function () {

                $("#txtPeriod").keydown(function (evt) {
                    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
                        //alert("Please Enter Key Press Or Tab Key Press For Display GL ID");
                        return false;
                    }
                });

                //$("#txtMaturity_DT").keydown(function (evt) {
                //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                //    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
                //        alert("Please Enter Key Press Or Tab Key Press For Display GL ID");
                //        return false;
                //    }
                //});

                $("#txtSchemeID").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        if ($("#txtSchemeID").val().trim() == '') {
                            BindGetSchemecode();
                            return false;
                        }

                        else {
                            $("#txtSchemeName").focus();
                            return false;
                        }
                    }
                });

                $("#txtSchemeName").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        if ($("#txtSchemeName").val().trim() == '') {
                            BindGetSchemecode();
                            return false;
                        }

                        else {
                            $("#CheckBox1").focus();
                            return false;
                        }
                    }
                });

                $("#CheckBox1").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {

                        $("#txtBranchID").focus();
                        return false;
                    }
                });

                $("#txtBranchID").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        if ($("#txtBranchID").val().trim() == '') {
                            BindGetSectorcode();
                            return false;
                        }

                        else {
                            $("#txtBranchName1").focus();
                            return false;
                        }
                    }
                });

                $("#txtBranchName1").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        if ($("#txtBranchName1").val().trim() == '') {
                            BindGetSectorcode();
                            return false;
                        }

                        else {
                            $("#txtTDSID").focus();//$("#txtF15H_FOR_FY").focus();
                            return false;
                        }
                    }
                });

                //$("#txtBranchName1").keydown(function (e) {
                //    if (e.keyCode == 13 || e.keyCode == 9) {
                //        $("#txtTDSID").focus();
                //        return false;

                //    }
                //    });

                $("#txtTDSID").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        if ($("#txtTDSID").val().trim() == '') {
                            BindGetTDScode();
                            return false;
                        }

                        else {
                            $("#txtTDSName").focus();//$("#txtF15H_FOR_FY").focus();
                            return false;
                        }
                    }
                });

                $("#txtTDSName").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        if ($("#txtTDSName").val().trim() == '') {
                            BindGetTDScode();
                            return false;
                        }

                        else {
                            $("#txtApplication_DT").focus();//$("#txtF15H_FOR_FY").focus();
                            return false;
                        }
                    }
                });

                $("#txtApplication_DT").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAppl_No").focus();
                        $("#txtApplication_DT").datepicker("hide");
                        return false;

                    }
                });

                $("#txtAppl_No").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtDeposit_ID").focus();
                        return false;
                    }
                });

                //$("#txtDeposit_ID").keydown(function (e) {
                //    if (e.keyCode == 13 || e.keyCode == 9) {
                //        $("#txtQty").focus();
                //        return false;
                //    }
                //});

                $("#txtQty").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        if ($("#txtQty").val().trim() == '') {
                            alert("Please Enter Quantity!!");
                            $('#txtQty').focus();
                            return false;
                        }
                        else {
                            $("#txtDistinctive_Start").focus();
                            return false;
                        }
                    }
                });

                //$("#txtIssue_DT").keydown(function (e) {
                //    if (e.keyCode == 13 || e.keyCode == 9) {
                //        $("#txtDistinctive_Start").focus();
                //        return false;

                //    }
                //});

                $("#txtDistinctive_Start").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAllotment_DT").focus();
                        return false;

                    }
                });

                $("#txtAllotment_DT").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtName").focus();
                        $("#txtAllotment_DT").datepicker("hide");


                        var AllotmentDate = $("#txtAllotment_DT").val().trim().split("/");
                        NewAllotmentDate = AllotmentDate[2] + "-" + AllotmentDate[1] + "-" + AllotmentDate[0];
                        var dt = new Date(NewAllotmentDate);

                        var ValPeriod = parseInt($("#txtPeriod").val());
                        if (ValPeriod != '') {
                            dt.setMonth(dt.getMonth() + parseInt(ValPeriod));
                        } else { alert("Please enter a Period In Days!"); $("#txtPeriod").focus(); $("#txtAllotment_DT").val(""); return false; }

                        var NewDate = '';
                        var day = dt.getDate();
                        var month = dt.getMonth() + 1; //January is 0!
                        var year = dt.getFullYear();
                        if (day < 10) {
                            day = '0' + day;
                        }

                        if (month < 10) {
                            month = '0' + month;
                        }
                        NewDate = day + '/' + month + '/' + year;


                        if ($("#txtPeriod").val() != '') { $("#txtMaturity_DT").val(NewDate); }
                        $("#txtRepaid_DT").datepicker("option", "maxDate", NewDate);

                        return false;

                    }
                });

                

                $("#txtName").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAddr1").focus();
                        return false;

                    }
                });

                $("#txtAddr1").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAddr2").focus();
                        return false;

                    }
                });

                $("#txtAddr2").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAddr3").focus();
                        return false;

                    }
                });

                $("#txtAddr3").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAddr4").focus();
                        return false;

                    }
                });

                $("#txtAddr4").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtPin").focus();
                        return false;

                    }
                });

                $("#txtPin").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtRepaid_DT").focus();
                        return false;

                    }
                });

                $("#txtRepaid_DT").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAppl_Amount").focus();
                        $("#txtRepaid_DT").datepicker("hide");
                        return false;

                    }
                });

                $("#txtAppl_Amount").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtPan").focus();
                        return false;

                    }
                });

                $("#txtMaturity_DT").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtPan").focus();
                        $("#txtMaturity_DT").datepicker("hide");
                        return false;

                    }
                });

                $("#txtPan").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtIssue_Amount").focus();
                        return false;

                    }
                });

                $("#txtIssue_Amount").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtPhone").focus();
                        return false;

                    }
                });

                $("#txtPhone").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#ddlpayment_Mode").focus();
                        return false;

                    }
                });

                $("#ddlpayment_Mode").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtDPID_No").focus();
                        return false;

                    }
                });

                $("#txtDPID_No").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtRemarks").focus();
                        return false;

                    }
                });

                $("#txtRemarks").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#ddlNomination").focus();
                        return false;

                    }
                });

                $("#ddlNomination").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAppl_DT").focus();
                        return false;

                    }
                });

                $("#txtAppl_DT").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtDrn_No").focus();
                        $("#txtAppl_DT").datepicker("hide");
                        return false;

                    }
                });

                $("#txtDrn_No").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtISSIN_No").focus();
                        return false;

                    }
                });

                $("#txtISSIN_No").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtBankName").focus();
                        return false;

                    }
                });

                $("#txtBankName").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtBranchName").focus();
                        return false;

                    }
                });

                $("#txtBranchName").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtAccNo").focus();
                        return false;

                    }
                });

                $("#txtAccNo").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#txtRtgsIfsc").focus();
                        return false;

                    }
                });

                $("#txtRtgsIfsc").keydown(function (e) {
                    if (e.keyCode == 13 || e.keyCode == 9) {
                        $("#cmdSave").focus();
                        return false;

                    }
                });


            }); // Main function cloase

            $("#txtSchemeID").click(function (event) {
                //  event.preventDefault();
                //BindGetSchemecode();
            });
            $("#txtBranchID").click(function (event) {
                //BindGetSectorcode();
                //  event.preventDefault();
            });

            $("#txtTDSID").click(function (event) {
                //BindGetTDScode();
                //  event.preventDefault();

            });
            $(".DefaultButton").click(function (event) {
                //alert('prevent');
                event.preventDefault();
            });

            $('#txtDistinctive_Start').keyup(function () {
                //alert("ok");
                if ($('#txtQty').val().trim() != '') {
                    if ($('#txtDistinctive_Start').val().trim() != '') {
                        $('#txtDistinctive_End').val(parseInt(parseInt($('#txtDistinctive_Start').val()) + parseInt($('#txtQty').val())) - 1);
                    }
                    else {
                        $('#txtDistinctive_End').val('');
                    }
                }
                else {
                    alert("Please Enter Quantity!!"); $('#txtDistinctive_Start').val(""); $('#txtQty').focus(); return false;
                }

            });

            $('#CheckBox1').click(function () {
                if ($(this).is(':checked')) {
                    //  displayNote(); 
                    var dt = new Date();
                    var m = '';
                    m = dt.getMonth();
                    var y = dt.getFullYear();
                    var d = dt.getDate();
                    if (m < 10) {
                        m = m + 1;
                        m = '0' + m;
                    }

                    else {
                        m = m + 1;
                    }

                    if (d < 10)
                    { d = '0' + d; }
                    //var CurrentDate = y + '-' + (m + 1) + '-' + d;
                    var CurrentDate = d + '/' + m + '/' + y;
                    //var CurrentDate = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
                    var W = "{CurrentDate:'" + CurrentDate + "'}";
                    //  alert(W);
                    $.ajax({
                        type: "POST",
                        url: "MST_Acc_DepositMaster.aspx/GET_FinancialYear",
                        contentType: "application/json;charset=utf-8",
                        data: W,
                        dataType: "json",
                        success: function (data) {
                            $('#txtF15H_FOR_FY').val(data.d);
                        }
                    });
                } else { $('#txtF15H_FOR_FY').val(""); }
            });
        });

        //function displayNote() {
        //    var d = new Date();
        //    var n = d.getFullYear();
        //    var tn = d.getFullYear() - 1;
        //    var fyd = (tn + "- " + n);
        //    //alert(fyd);
        //    document.getElementById("txtF15H_FOR_FY").value = fyd;
        //}

        function fnValidateQuty() {
            //alert(quty);
        }

        function fnValidatePAN(Obj) {                                           ///PAN Number 
            if (Obj == null) Obj = window.event.srcElement;
            if (Obj.value != "") {
                ObjVal = Obj.value;
                var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                var code = /([C,P,H,F,A,T,B,L,J,G])/;
                var code_chk = ObjVal.substring(3, 4);
                if (ObjVal.search(panPat) == -1) {
                    alert("Invalid Pan No");
                    //  Obj.focus();
                    $("#txtPan").val("");
                    $("#txtPan").focus();
                    return false;
                }
                if (code.test(code_chk) == false) {
                    alert("Invaild PAN Card No.");
                    $("#txtPan").val("");
                    $("#txtPan").focus();
                    return false;
                }
            }
        }

        //function validatePhone(txtPhone) {                             ///phone number
        //    var a = document.getElementById(txtPhone).value;
        //    var filter = /^[0-9-+]+$/;
        //    if (filter.test(a)) {
        //        return true;
        //    }
        //    else {
        //        alert("Invaild Phone No.");
        //        return false;
        //    }
        //}​

        $(document).ready(function () {                                     //Datepicker    
            $('#txtAppl_DT').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',
                autoclose: true,
                onSelect: function (event) {
                    //$('#txtAppl_DT').datepicker("hide");
                    dialogpopDivShowModalPopup();
                }
            });

            //$('#txtIssue_DT').datepicker({
            //    showOtherMonths: true,
            //    selectOtherMonths: true,
            //    closeText: 'X',
            //    showAnim: 'drop',
            //    showButtonPanel: true,
            //    duration: 'slow',
            //    dateformat: 'dd/mm/yyyy',
            //    onSelect: function (event) {
            //        $('#txtIssue_DT').focus();
            //    }
            //});



            $('#txtRepaid_DT').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',
                onSelect: function (event) {
                    $('#txtRepaid_DT').focus();
                }
            });
            $('#txtApplication_DT').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',
                onSelect: function (event) {
                    $('#txtApplication_DT').focus();
                }
            });

            $('#txtCheque_DT').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',
                onSelect: function (event) {
                    $('#txtCheque_DT').focus();
                }
            });

            $('#txtMaturity_DT').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',
                autoclose: true,
                onSelect: function (event) {
                    $('#txtMaturity_DT').focus();
                }
            });

        });

        //function displayNote() {        //15H_FOR_FY        
        //    var d = new Date();
        //    var n = d.getFullYear();
        //    var tn = d.getFullYear() - 1;
        //    var fyd = (tn + "- " + n);
        //    //alert(fyd);
        //    document.getElementById("txtF15H_FOR_FY").value = fyd;
        //}
        $(document).ready(function () {
            $('#txtAllotment_DT').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',
                onSelect: function (event) {

                    $("#txtRepaid_DT").datepicker("option", "minDate", event)

                    var AllotmentDate = $("#txtAllotment_DT").val().trim().split("/");
                    //NewAllotmentDate = AllotmentDate[1] + "-" + AllotmentDate[0] + "-" + AllotmentDate[2];
                    NewAllotmentDate = AllotmentDate[2] + "-" + AllotmentDate[1] + "-" + AllotmentDate[0];

                    var dt = new Date(NewAllotmentDate);

                    var ValPeriod = parseInt($("#txtPeriod").val());
                    if (ValPeriod != '') {
                        dt.setMonth(dt.getMonth() + parseInt(ValPeriod));
                    } else { alert("Please enter a Period In Days!"); $("#txtPeriod").focus(); $("#txtAllotment_DT").val(""); return false; }

                    var NewDate = '';
                    var day = dt.getDate();
                    var month = dt.getMonth() + 1; //January is 0!
                    var year = dt.getFullYear();
                    if (day < 10) {
                        day = '0' + day;
                    }

                    if (month < 10) {
                        month = '0' + month;
                    }
                    NewDate = AllotmentDate[0] + '/' + AllotmentDate[1] + '/' + year;

                    
                    if ($("#txtPeriod").val() != '') { $("#txtMaturity_DT").val(NewDate); }
                    $("#txtRepaid_DT").datepicker("option", "maxDate", NewDate);
                    $('#txtAllotment_DT').focus();
                }
            });

        });

        $(document).ready(function () {
            $("#txtChequeNo").keypress(function (event) {
                if (event.which < 46
                || event.which > 59) {
                    alert("Please Enter Numeric Value");
                    return false;
                } // prevent if not number/dot

                if (event.which == 46
                && $(this).val().indexOf('.') != -1) {
                    //alert("Please Enter Numeric Value");
                    return false;
                } // prevent if already dot
            });
        });

        $(document).ready(function () {
            $("#txtAppl_Amount").keypress(function (event) {
                if (event.which < 46
                || event.which > 59) {
                    alert("Please Enter Numeric Value");
                    return false;
                } // prevent if not number/dot

                if (event.which == 46
                && $(this).val().indexOf('.') != -1) {
                    //alert("Please Enter Numeric Value");
                    return false;
                } // prevent if already dot
            });
        });
        $(document).ready(function () {
            $("#txtIssue_Amount").keypress(function (event) {
                if (event.which < 46
                || event.which > 59) {
                    alert("Please Enter Numeric Value");
                    return false;
                } // prevent if not number/dot

                if (event.which == 46
                && $(this).val().indexOf('.') != -1) {
                    // alert("Please Enter Numeric Value");
                    return false;
                } // prevent if already dot
            });
        });
        $(document).ready(function () {
            $("#txtQty").keypress(function (e) {
                if (e.which > 31 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter Numeric Value");
                    return false;
                }
            });
            $("#txtDistinctive_Start").keypress(function (e) {
                if (e.which > 31 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter Numeric Value");
                    return false;
                }
            });
            $("#txtDistinctive_End").keypress(function (e) {
                if (e.which > 31 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter Numeric Value");
                    return false;
                }
            });
            $("#txtPeriod").keypress(function (e) {
                if (e.which > 31 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter Numeric Value");
                    return false;
                }
            });
            $("#txtPin").keypress(function (e) {
                if (e.which > 31 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter Numeric Value");
                    return false;
                }
            });
        });

        function beforeSave() {

            //    SchemeID:'" + $('#txtSchemeID').val() + "',SchemeType:'" + $('#txtSchemeType').val() + "',BranchID:'" + $('#txtBranchID').val() +
            //     "',TDSID:'" + $('#txtTDSID').val() + "',Appl_DT:'" + $('#txtAppl_DT').val() + "', " +
            //     " Appl_Amount:'" + $('#txtAppl_Amount').val() + "',Issue_DT:'" + $('#txtIssue_DT').val() + "',Deposit_ID:'" + $('#txtDeposit_ID').val() + "',Issue_Amount:'" + $('#txtIssue_Amount').val() +
            //     "',txtQty:'" + $('#txtQty').val() + "',Distinctive_Start:'" + $('#txtDistinctive_Start').val() + "',Distinctive_End:'" + $('#txtDistinctive_End').val() +
            //      "',Period:'" + $('#txtPeriod').val() + "',Maturity_DT:'" + $('#txtMaturity_DT').val() + "',Repaid_DT:'" + $('#txtRepaid_DT').val() + "',Name:'" + $('#txtName').val() +
            //     "',Addr1:'" + $('#txtAddr1').val() + "',Addr2:'" + $('#txtAddr2').val() + "',Addr3:'" + $('#txtAddr3').val() + "',Addr4:'" + $('#txtAddr4').val() +
            //      "',Phone:'" + $('#txtPhone').val() + "',ddlpayment_Mode:'" + $('#ddlpayment_Mode').val() + "',ddlNomination:'" + $('#ddlNomination').val() + "',DPID_No:'" + $('#txtDPID_No').val() +
            //      "',ISSIN_No:'" + $('#txtISSIN_No').val() + "',Pan:'" + $('#txtPan').val() + "',Allotment_DT:'" + $('#txtAllotment_DT').val() + "',BranchName:'" + $('#txtBranchName').val() +
            //       "',BranchName:'" + $('#txtBranchName').val() + "',Application_DT:'" + $('#txtApplication_DT').val() + "',txtBankName:'" + $('#txtBankName').val() + "',txtAccNo:'" + $('#txtAccNo').val() +
            //    "',RtgsIfsc:'" + $('#txtRtgsIfsc').val() + "'}";

            //if ($('#txtSchemeID').val() != "")
            //{
            //    if ($('#txtDeposit_ID').val() != "") { CheckDuplicateBondID();}
            //}

            
            if ($('#txtSchemeID').val() == "" || $('#txtSchemeType').val() == "" || $('#txtBranchID').val() == "" || $('#txtTDSID').val() == "" ||
               $('#txtPeriod').val() == "" || $('#txtName').val() == "" || $('#txtAddr1').val() == "" || $('#txtAppl_Amount').val() == "" ||
                $('#ddlpayment_Mode').val() == "0" || $('#ddlNomination').val() == "0" || $('#txtAppl_No').val() == "" || $('#txtQty').val() == "")
            { alert("Please Fill up all mandatory fields!!"); return false; } else { return true; }

            if ($("#cmdSave").val() != 'Update') {
                if ($('#txtApplication_DT').val().trim() == "") {
                    alert('Please Select Application Date');
                    $('#txtApplication_DT').focus();
                    return false
                }

                if ($('#txtPin').val().trim() == "") {
                    alert('Please Type Pin');
                    $('#txtPin').focus();
                    return false
                }

                //var Y = "{txtDeposit_ID:'" + $('#txtDeposit_ID').val().trim() + "', gblSchemeID:'" + gblSchemeID + "',gblSchemeType:'" + gblSchemeType + "',SectorID:'" + $('#ddlUserSector').val() + "'}";
                //alert(Y);
                //$.ajax({
                //    type: "POST",
                //    url: "MST_Acc_DepositMaster.aspx/Check_Deposit_ID",
                //    contentType: "application/json;charset=utf-8",
                //    data: Y,
                //    dataType: "json",
                //    success: function (data) {
                //        if (data.d == '0') {
                //            alert("Bond Holder ID Already Exist");
                //            $('#txtDeposit_ID').focus();
                //            return false;
                //        }
                //        else {
                //            //
                //        }
                //    }
                //});

            }
            else { return true; }

            //$("#frmEcom").validate();
            //$("#txtSchemeID").rules("add", { required: true, messages: { required: "Please Select SchemeID" } });
            //$("#txtSchemeName").rules("add", { required: true, messages: { required: "Please Select SchemeName" } });
            //$("#txtBranchID").rules("add", { required: true, messages: { required: "Please Select Branch Nmae" } });
            //$("#txtBranchName1").rules("add", { required: true, messages: { required: "Please Select Branch Nmae" } });
            //$("#txtIssue_Amount").rules("add", { required: true, messages: { required: "Please Enter Issue Amount " } });
            //$("#txtName").rules("add", { required: true, messages: { required: "Please Enter Name " } });
            //$("#txtAddr1").rules("add", { required: true, messages: { required: "Please Enter Address " } });
            //$("#txtNomination").rules("add", { required: true, messages: { required: "Please Enter Nomination " } });   
        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }

        // ==================== Start voucher window keypress Type Not Allowed Coding Here =========================//    Scheme 
        $(document).ready(function () {
            //$('#txtAppl_DT').keypress(function (evt) {        
            //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            //    if (iKeyCode == 13) {

            //        $('#dialogpopDiv')
            //        dialogpopDivShowModalPopup();

            //        if ($('#txtAppl_DT').val() != '') {
            //            dialogpopDivShowModalPopup();
            //            //$('#txtSearchSchemeName').val($('#txtAppl_DT').val());
            //            return false;
            //        }
            //        else {
            //            $('#txtSearchSchemeName').val('');
            //            dialogpopDivShowModalPopup();
            //            return false;
            //        }
            //    }

            //});
        });

        function dialogpopDivShowModalPopup() {
            $("#dialogpopDiv").dialog({
                title: "voucher window",
                width: 600,

                buttons: {
                    Submit: function () {
                        BindGetVouchercode();
                        //$("#dialogSchmeNo").dialog('close');  
                        // $("#dialogpopDiv").dialog('close');                   
                    }
                },
                modal: true
            });
        }
        // ==================== End voucher window keypress Type Not Allowed Coding Here =========================//

        //==============Start Scheme Name Popup coding here=====================================//

        //function BindGetVouchercode(ChequeNo, Cheque_DT, DraweeBank, ArrangerName, DepositeAt, SchemeID, SchemeType, BranchID, TDSID, Appl_No, Appl_DT, Appl_Amount, Issue_DT, Deposit_ID, Issue_Amount, Qty, Distinctive_Start, Distinctive_End, Period, Maturity_DT, Repaid_DT, Name, Addr1, Addr2, Addr3, Addr4, Pin, Phone, ddlpayment_Mode, Remarks, ddlNomination, DPID_No, Drn_No, ISSIN_No, Pan, F15H_FOR_FY, Allotment_DT, BranchName, AccNo,RtgsIfsc) {
        function BindGetVouchercode() {
            //  $(".loading-overlay").show();
            //var W = "{ChequeNo:'" + $('txtChequeNo').val() + "', Cheque_DT:'" + $('#txtCheque_DT').val() + "',DraweeBank:'" + $('#txtDraweeBank').val() + "',ArrangerName:'" + $('#txtArrangerName').val() +
            //       "',DepositeAt:'" + $('#txtDepositeAt').val() + "',SchemeID:'" + $('#txtSchemeID').val() + "',SchemeType:'" + $('#txtSchemeType').val() + "',BranchID:'" + $('#txtBranchID').val() +
            //     "',TDSID:'" + $('#txtTDSID').val() + "',Appl_DT:'" + $('#txtAppl_DT').val() + "', " +
            //     " Appl_Amount:'" + $('#txtAppl_Amount').val() + "',Issue_DT:'" + $('#txtIssue_DT').val() + "',Deposit_ID:'" + $('#txtDeposit_ID').val() + "',Issue_Amount:'" + $('#txtIssue_Amount').val() +
            //     "',txtQty:'" + $('#txtQty').val() + "',Distinctive_Start:'" + $('#txtDistinctive_Start').val() + "',Distinctive_End:'" + $('#txtDistinctive_End').val() +
            //      "',Period:'" + $('#txtPeriod').val() + "',Maturity_DT:'" + $('#txtMaturity_DT').val() + "',Repaid_DT:'" + $('#txtRepaid_DT').val() + "',Name:'" + $('#txtName').val() +
            //     "',Addr1:'" + $('#txtAddr1').val() + "',Addr2:'" + $('#txtAddr2').val() + "',Addr3:'" + $('#txtAddr3').val() + "',Addr4:'" + $('#txtAddr4').val() +
            //      "',Phone:'" + $('#txtPhone').val() + "',ddlpayment_Mode:'" + $('#ddlpayment_Mode').val() + "',ddlNomination:'" + $('#ddlNomination').val() + "',DPID_No:'" + $('#txtDPID_No').val() +
            //      "',ISSIN_No:'" + $('#txtISSIN_No').val() + "',Pan:'" + $('#txtPan').val() + "',Allotment_DT:'" + $('#txtAllotment_DT').val() + "',BranchName:'" + $('#txtBranchName').val() +
            //       "',BranchName:'" + $('#txtBranchName').val() + "',Application_DT:'" + $('#txtApplication_DT').val() + "',txtBankName:'" + $('#txtBankName').val() + "',txtAccNo:'" + $('#txtAccNo').val() +
            //    "',RtgsIfsc:'" + $('#txtRtgsIfsc').val() + "'}";
            if ($('#txtChequeNo').val().trim() == "" || $('#txtCheque_DT').val().trim() == "" || $('#txtDraweeBank').val().trim() == "" || $('#txtArrangerName').val().trim() == "" || $('#txtDepositeAt').val().trim() == "")
            { alert("Fill Up the all fields!!"); return false; }

            var W = "{ChequeNo:'" + $('#txtChequeNo').val() + "', Cheque_DT:'" + $('#txtCheque_DT').val() + "',DraweeBank:'" + $('#txtDraweeBank').val() +
                    "',ArrangerName:'" + $('#txtArrangerName').val() + "',DepositeAt:'" + $('#txtDepositeAt').val() + "'}";
            //  alert(W);
            $.ajax({
                type: "POST",
                url: "MST_Acc_DepositMaster.aspx/SET_GLNameOnSession",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    if (data.d == "insert") {
                        alert("Record Save for Final Submit.");
                    }
                    if (data.d == "Update") {
                        alert("Record Update for Final Submit.");
                    }
                    //  $("#dialogpopDiv").dialog('close');
                    //  InsertMode = "Add";           // CLEAR ALL TEXTBOXES
                    $('#txtChequeNo').val('');
                    $('#txtCheque_DT').val('');
                    $('#txtDraweeBank').val('');
                    $('#txtTotalDebit').val('');
                    $('#txtDraweeBank').val('');
                    $('#txtArrangerName').val('');
                    $('#txtDepositeAt').val('');
                    $("#dialogpopDiv").dialog('close');
                    //  }
                    //$('#txtSchemeID').val('');
                    //$('#txtSchemeType').val('');
                    //$('#txtBranchID').val('');
                    //$('#txtTDSID').val('');
                    //$('#txtAppl_No').val('');
                    //$('#txtAppl_DT').val('');
                    //$('#txtAppl_Amount').val('');
                    //$('#txtIssue_DT').val('');
                    //$('#txtDeposit_ID').val('');
                    //$('#txtIssue_Amount').val('');
                    //$('#txtQty').val('');
                    //$('#txtDistinctive_Start').val('');
                    //$('#txtDistinctive_End').val('');
                    //$('#txtPeriod').val('');
                    //$('#txtMaturity_DT').val('');
                    //$('#txtRepaid_DT').val('');
                    //$('#txtName').val('');
                    //$('#txtAddr1').val('');
                    //$('#txtAddr2').val('');
                    //$('#txtAddr3').val('');
                    //$('#txtAddr4').val('');
                    //$('#txtPin').val('');
                    //$('#txtPhone').val('');
                    //$('#ddlpayment_Mode').val('');
                    //$('#txtRemarks').val('');
                    //$('#ddlNomination').val('');
                    //$('#txtDPID_No').val('');
                    //$('#txtDrn_No').val('');
                    //$('#txtISSIN_No').val('');
                    //$('#txtPan').val('');
                    //$('#txtF15H_FOR_FY').val('');
                    //$('#txtAllotment_DT').val('');
                    //$('#txtBranchName').val('');
                    //$('#txtApplication_DT').val('');
                    //$('#txtBankName').val('');
                    //$('#txtAccNo').val('');
                    //$('#txtRtgsIfsc').val('');               

                },
                error: function (result) {
                    alert("Error Records Data");
                    //   $(".loading-overlay").hide();
                }

            });
        }

        //============================================End Voucher Window==============================================================//

        // ==================== Start Scheme Name keypress Type Not Allowed Coding Here =========================//    Scheme 
        //$(document).ready(function () {   // sk code hide 13-02-17
        //    $('#txtSchemeName').click(function () {
        //        BindGetSchemecode();
        //    });
        //    $('#txtSchemeName').keyup(function () {
        //        if ($('#txtSchemeName').val().trim() == "")
        //        {
        //            //$('#txtSchemeID').val(""); //skm chage coding
        //            $('#txtSchemeType').val("");
        //        }
        //    });


        //$('#txtSchemeName').keypress(function (evt) {
        //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        //    if (iKeyCode == 13) {
        //        if ($('#txtSchemeName').val() != '') {
        //            BindGetSchemecode();
        //            $('#txtSearchSchemeName').val($('#txtSchemeName').val());
        //            return false;
        //        }
        //        else {
        //            $('#txtSearchSchemeName').val('');
        //            BindGetSchemecode();
        //            return false;
        //        }
        //    }
        //    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
        //        alert("Please Select Scheme Name");
        //        BindGetSchemecode();
        //        return false;
        //    }
        //});
        //});
        // ==================== End Scheme Name keypress Type Not Allowed Coding Here =========================//

        //==============Start Scheme Name Popup coding here=====================================//

        function BindGetSchemecode() {
            $(".loading-overlay").show();
            var W = "{}";
            $.ajax({
                type: "POST",
                url: "MST_Acc_DepositMaster.aspx/GET_Scheme",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    $("#GridViewSchemeName").empty();
                    if (data.d.length > 0) {
                        $("#GridViewSchemeName").append("<tr><th style='display:none;'>SCHEME ID</th><th>SCHEME NAME</th><th>OLD SCHEME ID</th><th>SCHEME FROM</th><th>REPAYMENT MODE</th><th>SLR NSLR</th><th>PERIOD DAYS</th><th>SCHEME TYPE</th><th>ISIN NO</th><th style='display:none;'>DPID NO</th></tr>");
                        SchemeNameShowModalPopup();
                        for (var i = 0; i < data.d.length; i++) {
                            $("#GridViewSchemeName").append("<tr><td style='display:none;'>" +
                            data.d[i].SCHEME_ID + "</td> <td>" +
                            data.d[i].SCHEME_NAME + "</td> <td>" +
                            data.d[i].OLD_SCHEME_ID + "</td> <td>" +
                            data.d[i].SCHEME_FROM + "</td> <td>" +
                            data.d[i].REPAYMENT_MODE + "</td> <td>" +
                            data.d[i].SLR_NSLR + "</td> <td>" +
                            data.d[i].PERIOD_DAYS + "</td> <td>" +
                            data.d[i].SCHEME_TYPE + "</td> <td>" +
                            data.d[i].ISIN_NO + "</td> <td style='display:none;'>" +
                            data.d[i].DPID_NO + "</td></tr>");
                        }
                        //===================== Start Folio Number Search is not Blank Then Foloi Number Record Display in GridView ===========================
                        if ($('#txtSearchSchemeName').val() != '') {
                            var rows;
                            var coldata;
                            $('#GridViewSchemeName').find('tr:gt(0)').hide();
                            var data = $('#txtSearchSchemeName').val();
                            var len = data.length;
                            if (len > 0) {
                                $('#GridViewSchemeName').find('tbody tr').each(function () {
                                    coldata = $(this).children().eq(1);
                                    var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                                    if (temp === 0) {
                                        $(this).show();
                                    }

                                });
                            } else {
                                $('#GridViewSchemeName').find('tr:gt(0)').show();

                            }
                        }


                        //===================== End Foloi Number Search is not Blank Then Foloi Number Record Display in GridView ===========================

                        $(".loading-overlay").hide();
                        $("[id*=GridViewSchemeName] td").bind("click", function () {
                            var row = $(this).parent();
                            $("[id*=GridViewSchemeName] tr").each(function () {
                                if ($(this)[0] != row[0]) {
                                    $("td", this).removeClass("selected_row");
                                }
                            });

                            $("td", row).each(function () {
                                if (!$(this).hasClass("selected_row")) {
                                    $(this).addClass("selected_row");
                                } else {
                                    $(this).removeClass("selected_row");
                                }
                            });
                        });


                        //===================== Start Selected Gridview Value Display in Text Box ===========================//bind
                        $(document).ready(function () {
                            $("[id*=GridViewSchemeName] tbody tr").click("click", function () {

                                var ValSchemeName = $(this).children("td:eq(2)").text();
                                $("#txtSearchSchemeName").val(ValSchemeName);
                                $("#txtSearchSchemeName").focus();
                            });
                        });
                        //===================== End Selected Gridview Value Display in Text Box ===========================//
                    }
                    else {
                        alert("No Records Data");
                        $(".loading-overlay").hide();
                        //$('#cmdFolioNo').focus();
                    }
                },
                error: function (result) {
                    alert("Error Records Data");
                    $(".loading-overlay").hide();

                }

            });
        }


        $(document).ready(function () {
            $("#txtSearchSchemeName").keyup(function (e) {
                //if ($('#txtSearchSchemeName').val() != '') {
                var rows;
                var coldata;
                $('#GridViewSchemeName').find('tr:gt(0)').hide();
                var data = $('#txtSearchSchemeName').val();
                var len = data.length;
                if (len > 0) {
                    $('#GridViewSchemeName').find('tbody tr').each(function () {
                        coldata = $(this).children().eq(2);
                        var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                        if (temp === 0) {
                            $(this).show();
                        }

                    });
                } else {
                    $('#GridViewSchemeName').find('tr:gt(0)').show();

                }
                //}
            });
        });

        function SchemeNameShowModalPopup() {
            $("#dialogSchmeNo").dialog({
                title: "Scheme  Search",
                width: 620,
                //  height:400,

                buttons: {
                    Ok: function () {
                        var GridViewSchemeName = document.getElementById("GridViewSchemeName");
                        var ValSchemeName = '';
                        var ValSchemeType = '';
                        var ValSchemeID = '';
                        var ValISIN = '';
                        var DPidNo = '';
                        var IssueFromdt = '';
                        if ($('#txtSearchSchemeName').val().trim() == '') {
                            alert("Please Select Scheme Name");
                            $('#txtSearchSchemeName').val('');
                            $("#txtSearchSchemeName").focus();
                            return false;
                        }
                        if ($('#txtSearchSchemeName').val() != '') {
                            for (var row = 1; row < GridViewSchemeName.rows.length; row++) {
                                var GridSchemeName_Cell = GridViewSchemeName.rows[row].cells[1];
                                var valueSchemName = GridSchemeName_Cell.textContent.toString();

                                var GridSchemeID_Cell = GridViewSchemeName.rows[row].cells[2];
                                var valueSchemID = GridSchemeID_Cell.textContent.toString();
                                if (valueSchemID == $('#txtSearchSchemeName').val()) {
                                    ValSchemeName = valueSchemName;

                                    ValSchemeID = GridViewSchemeName.rows[row].cells[0].textContent.toString();
                                    ValScheme_Old_ID = GridViewSchemeName.rows[row].cells[2].textContent.toString();
                                    ValScheme_FROM = GridViewSchemeName.rows[row].cells[3].textContent.toString();

                                    ValREPAYMENT_MODE = GridViewSchemeName.rows[row].cells[4].textContent.toString();
                                    ValSLR_NSLR = GridViewSchemeName.rows[row].cells[5].textContent.toString();
                                    ValPERIOD_DAYS = GridViewSchemeName.rows[row].cells[6].textContent.toString();
                                    ValSchemeType = GridViewSchemeName.rows[row].cells[7].textContent.toString();
                                    ValISIN = GridViewSchemeName.rows[row].cells[8].textContent.toString();
                                    DPidNo = GridViewSchemeName.rows[row].cells[9].textContent.toString();
                                    

                                    gblSchemeID = ValSchemeID; gblSchemeType = ValSchemeType;

                                    break;
                                }
                            }

                            if (valueSchemID != $('#txtSearchSchemeName').val()) {
                                alert("Invalid Scheme (Scheme is Not in List)");
                                $("#txtSearchSchemeName").focus();
                                return false;
                            }
                            else {
                                if ($('#ddlUserSector').val() == "")
                                { alert("Please select a sector !!"); return false; }
                                //var Y = "{ValSchemeID:'" + ValSchemeID + "',ValSchemeType:'" + ValSchemeType + "',SectorID:'" + $('#ddlUserSector').val() + "'}";
                                //$.ajax({
                                //    type: "POST",
                                //    url: "MST_Acc_DepositMaster.aspx/GET_Deposit_ID",
                                //    contentType: "application/json;charset=utf-8",
                                //    data: Y,
                                //    dataType: "json",
                                //    success: function (data) {
                                //        $('#txtDeposit_ID').val(data.d);
                                //    }
                                //});
                                $("#txtSchemeName").val($("#txtSearchSchemeName").val());
                                //  $("#txtSchemeID").val(ValSchemeID);
                                $("#ddlpayment_Mode").val(ValREPAYMENT_MODE);
                                $("#txtPeriod").val(ValPERIOD_DAYS);
                                $("#txtSchemeName").val(ValSchemeName);
                                $('#txtIssue_DT').val(ValScheme_FROM);
                                //  $("#txtSchemeType").val(ValSchemeType);
                                $("#txtSchemeType").val(ValSchemeID);//0000000002 
                                $("#txtScheme_ID1").val(ValSchemeType);//B
                                $("#txtSchemeID").val(ValScheme_Old_ID);
                                $("#txtISSIN_No").val(ValISIN);
                                $("#txtDPID_No").val(DPidNo);
                                $("#txtSchemeName").focus();
                                $("#txtMaturity_DT").datepicker("option", "minDate", ValScheme_FROM)
                                $("#dialogSchmeNo").dialog('close');
                                return false;
                            }

                        }
                    }
                },
                modal: true
            });
        }

        //==============End Scheme Name Popup coding here=====================================//

        $(document).ready(function () {
            $("#txtSearchSchemeName").keypress(function (e) {
                if (e.keyCode == 13) {

                    var GridViewSchemeName = document.getElementById("GridViewSchemeName");
                    var ValSchemeName = '';
                    var ValSchemeType = '';
                    var ValSchemeID = '';
                    var ValISIN = '';
                    var DPidNo = '';
                    if ($('#txtSearchSchemeName').val().trim() == '') {
                        alert("Please Select Scheme Name");
                        $('#txtSearchSchemeName').val('');
                        $("#txtSearchSchemeName").focus();
                        return false;
                    }
                    if ($('#txtSearchSchemeName').val() != '') {
                        for (var row = 1; row < GridViewSchemeName.rows.length; row++) {
                            var GridSchemeName_Cell = GridViewSchemeName.rows[row].cells[1];
                            var valueSchemName = GridSchemeName_Cell.textContent.toString();
                            //if (valueSchemName == $('#txtSearchSchemeName').val()) {
                            var GridSchemeID_Cell = GridViewSchemeName.rows[row].cells[2];
                            var valueSchemID = GridSchemeID_Cell.textContent.toString();
                            if (valueSchemID == $('#txtSearchSchemeName').val()) {
                                ValSchemeName = valueSchemName;

                                ValSchemeID = GridViewSchemeName.rows[row].cells[0].textContent.toString();
                                ValScheme_Old_ID = GridViewSchemeName.rows[row].cells[2].textContent.toString();
                                ValScheme_FROM = GridViewSchemeName.rows[row].cells[3].textContent.toString();
                                ValREPAYMENT_MODE = GridViewSchemeName.rows[row].cells[4].textContent.toString();
                                ValSLR_NSLR = GridViewSchemeName.rows[row].cells[5].textContent.toString();
                                ValPERIOD_DAYS = GridViewSchemeName.rows[row].cells[6].textContent.toString();
                                ValSchemeType = GridViewSchemeName.rows[row].cells[7].textContent.toString();
                                ValISIN = GridViewSchemeName.rows[row].cells[8].textContent.toString();
                                DPidNo = GridViewSchemeName.rows[row].cells[9].textContent.toString();
                                
                                gblSchemeID = ValSchemeID; gblSchemeType = ValSchemeType;
                                break;
                            }
                        }

                        if (valueSchemID != $('#txtSearchSchemeName').val()) {
                            alert("Invalid Scheme (Scheme is Not in List)");
                            $("#txtSearchSchemeName").focus();
                            return false;
                        }
                        else {
                            if ($('#ddlUserSector').val() == "")
                            { alert("Please select a sector !!"); return false; }

                            
                            //var Y = "{ValSchemeID:'" + ValSchemeID + "',ValSchemeType:'" + ValSchemeType + "',SectorID:'" + $('#ddlUserSector').val() + "'}";
                            //$.ajax({
                            //    type: "POST",
                            //    url: "MST_Acc_DepositMaster.aspx/GET_Deposit_ID",
                            //    contentType: "application/json;charset=utf-8",
                            //    data: Y,
                            //    dataType: "json",
                            //    success: function (data) {
                            //        $('#txtDeposit_ID').val(data.d);
                            //    }
                            //});

                            $("#txtSchemeName").val($("#txtSearchSchemeName").val());
                            //  $("#txtSchemeID").val(ValSchemeID);
                            $("#ddlpayment_Mode").val(ValREPAYMENT_MODE);
                            $("#txtPeriod").val(ValPERIOD_DAYS);
                            $("#txtSchemeName").val(ValSchemeName);
                            $('#txtIssue_DT').val(ValScheme_FROM);
                            //  $("#txtSchemeType").val(ValSchemeType);
                            $("#txtSchemeType").val(ValSchemeID);//0000000002 
                            $("#txtScheme_ID1").val(ValSchemeType);//B
                            $("#txtSchemeID").val(ValScheme_Old_ID);
                            $("#txtISSIN_No").val(ValISIN);
                            $("#txtDPID_No").val(DPidNo);
                            $("#txtMaturity_DT").datepicker("option", "minDate", ValScheme_FROM)
                            $("#txtSchemeName").focus();
                            $("#dialogSchmeNo").dialog('close');
                            return false;
                        }

                    }
                } // keycode=13 close

            });
        });
        // ==================== Start Sector Name keypress Type Not Allowed Coding Here =========================//       Sector/Branch
        //$(document).ready(function () {

        //    $('#txtBranchName1').click(function () {
        //        BindGetSectorcode();
        //    });
        //    $('#txtBranchName1').keyup(function () {
        //        if ($('#txtBranchName1').val().trim() == "") {
        //            $('#txtBranchID').val("");

        //        }
        //    });

        //$('#txtBranchName1').keypress(function (evt) {       
        //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        //    if (iKeyCode == 13) {
        //        if ($('#txtBranchName1').val() != '') {
        //            BindGetSectorcode();
        //            $('#txtSearchSectorName').val($('#txtBranchName1').val());
        //            return false;
        //        }
        //        else {
        //            $('#txtSearchSectorName').val('');
        //            BindGetSectorcode();
        //            return false;
        //        }
        //    }
        //    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
        //        alert("Please Select Sector Name");
        //        BindGetSectorcode();
        //        return false;
        //    }
        //});
        //});
        // ==================== End Sector Name keypress Type Not Allowed Coding Here =========================//

        //==============Start Sector/Branch Popup coding here=====================================//
        function BindGetSectorcode() {

            $(".loading-overlay").show();
            var W = "{}";
            $.ajax({
                type: "POST",
                url: "MST_Acc_DepositMaster.aspx/GET_Sector",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    $("#GridViewSectorName").empty();
                    if (data.d.length > 0) {
                        $("#GridViewSectorName").append("<tr><th style='display:none;'>SECTOR ID</th><th>Branch CODE</th><th>Branch Name</th></tr>");
                        SectorNameShowModalPopup();
                        for (var i = 0; i < data.d.length; i++) {
                            $("#GridViewSectorName").append("<tr><td style='display:none;'>" +
                            data.d[i].SectorID + "</td> <td>" +
                            data.d[i].SectorCode + "</td> <td>" +
                            data.d[i].SectorName + "</td> </tr>");

                        }

                        //===================== Start Sector Search is not Blank Then Sector Record Display in GridView ===========================
                        if ($('#txtSearchSectorName').val() != '') {
                            var rows;
                            var coldata;
                            $('#GridViewSectorName').find('tr:gt(0)').hide();
                            var data = $('#txtSearchSectorName').val();
                            var len = data.length;
                            if (len > 0) {
                                $('#GridViewSectorName').find('tbody tr').each(function () {
                                    coldata = $(this).children().eq(1);
                                    var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                                    if (temp === 0) {
                                        $(this).show();
                                    }

                                });
                            } else {
                                $('#GridViewSectorName').find('tr:gt(0)').show();

                            }
                        }
                        //===================== End Sector Search is not Blank Then Sector Record Display in GridView ===========================

                        $(".loading-overlay").hide();
                        $("[id*=GridViewSectorName] td").bind("click", function () {
                            var row = $(this).parent();
                            $("[id*=GridViewSectorName] tr").each(function () {
                                if ($(this)[0] != row[0]) {
                                    $("td", this).removeClass("selected_row");
                                }
                            });

                            $("td", row).each(function () {
                                if (!$(this).hasClass("selected_row")) {
                                    $(this).addClass("selected_row");
                                } else {
                                    $(this).removeClass("selected_row");
                                }
                            });
                        });


                        //===================== Start Selected Gridview Value Display in Text Box ===========================//bind
                        $(document).ready(function () {
                            $("[id*=GridViewSectorName] tbody tr").click("click", function () {

                                var ValSectorName = $(this).children("td:eq(1)").text();
                                $("#txtSearchSectorName").val(ValSectorName);
                                $("#txtSearchSectorName").focus();
                            });
                        });
                        //===================== End Selected Gridview Value Display in Text Box ===========================//


                    }
                    else {
                        alert("No Records Data");
                        $(".loading-overlay").hide();
                        //$('#cmdFolioNo').focus();
                    }
                },
                error: function (result) {
                    alert("Error Records Data");
                    $(".loading-overlay").hide();

                }

            });
        }
        function SectorNameShowModalPopup() {
            $("#dialogSectorNo").dialog({
                title: "Bank Name Search",
                width: 600,

                buttons: {
                    Ok: function () {
                        var GridViewSectorName = document.getElementById("GridViewSectorName");
                        var ValSectorName = '';
                        var ValSectorID = '';
                        var ValSectorCode = '';

                        if ($('#txtSearchSectorName').val().trim() == '') {
                            alert("Please Select Sector Name");
                            $('#txtSearchSectorName').val('');
                            $("#txtSearchSectorName").focus();
                            return false;
                        }
                        if ($('#txtSearchSectorName').val() != '') {
                            for (var row = 1; row < GridViewSectorName.rows.length; row++) {
                                var GridSectorName_Cell = GridViewSectorName.rows[row].cells[1];
                                var valueSectorName = GridSectorName_Cell.textContent.toString();
                                if (valueSectorName == $('#txtSearchSectorName').val()) {
                                    ValSectorName = valueSectorName;
                                    ValSectorID = GridViewSectorName.rows[row].cells[0].textContent.toString();
                                    ValSectorCode = GridViewSectorName.rows[row].cells[2].textContent.toString();
                                    ValSectorName = GridViewSectorName.rows[row].cells[1].textContent.toString();
                                    break;
                                }
                            }

                            if (ValSectorName != $('#txtSearchSectorName').val()) {
                                alert("Invalid Sector (Sector is Not in List)");
                                $("#txtSearchSectorName").focus();
                                return false;
                            }

                            else {

                                //GetItemDescription($("#txtSchemeName").val());
                                $("#txtBranchName1").val($("#txtSearchSectorName").val());
                                //  $("#txtBranchID").val(ValSectorID);
                                $("#txtBranchID").val(ValSectorName);
                                $("#txtBranchName1").val(ValSectorCode);
                                $("#txtBranchName1").focus();
                                $("#dialogSectorNo").dialog('close');
                                return false;
                            }

                        }
                    }
                },
                modal: true
            });
        }
        //==============End Branch Popup coding here=====================================//
        $(document).ready(function () {
            $("#txtSearchSectorName").keypress(function (e) {
                if (e.keyCode == 13) {

                    var GridViewSectorName = document.getElementById("GridViewSectorName");
                    var ValSectorName = '';
                    var ValSectorID = '';
                    var ValSectorCode = '';

                    if ($('#txtSearchSectorName').val().trim() == '') {
                        alert("Please Select Sector Name");
                        $('#txtSearchSectorName').val('');
                        $("#txtSearchSectorName").focus();
                        return false;
                    }
                    if ($('#txtSearchSectorName').val() != '') {
                        for (var row = 1; row < GridViewSectorName.rows.length; row++) {
                            var GridSectorName_Cell = GridViewSectorName.rows[row].cells[1];
                            var valueSectorName = GridSectorName_Cell.textContent.toString();
                            if (valueSectorName == $('#txtSearchSectorName').val()) {
                                ValSectorName = valueSectorName;
                                ValSectorID = GridViewSectorName.rows[row].cells[0].textContent.toString();
                                ValSectorCode = GridViewSectorName.rows[row].cells[2].textContent.toString();
                                ValSectorName = GridViewSectorName.rows[row].cells[1].textContent.toString();
                                break;
                            }
                        }

                        if (ValSectorName != $('#txtSearchSectorName').val()) {
                            alert("Invalid Sector (Sector is Not in List)");
                            $("#txtSearchSectorName").focus();
                            return false;
                        }

                        else {

                            //GetItemDescription($("#txtSchemeName").val());
                            $("#txtBranchName1").val($("#txtSearchSectorName").val());
                            //  $("#txtBranchID").val(ValSectorID);
                            $("#txtBranchID").val(ValSectorName);
                            $("#txtBranchName1").val(ValSectorCode);
                            $("#txtBranchName1").focus();
                            $("#dialogSectorNo").dialog('close');
                            return false;
                        }

                    }

                } //keycode=13 close
            });
        });
        //====================================================Start TDS Popup coding here=====================================================================//

        // ==================== Start TDS keypress Type Not Allowed Coding Here =========================//  
        //$(document).ready(function () {
        //    $('#txtTDSName').click(function (event) {
        //        BindGetTDScode();
        //    });
        //    $('#txtTDSName').keyup(function () {
        //        if ($('#txtTDSName').val().trim() == "")
        //        { $('#txtTDSID').val(""); }
        //    });

        //$('#txtTDSName').keypress(function (evt) {
        //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        //    if (iKeyCode == 13) {
        //        if ($('#txtTDSName').val() != '') {
        //            BindGetTDScode();
        //            $('#txtSearchTds').val($('#txtSearchTds').val());
        //            return false;
        //        }
        //        else {
        //            $('#txtSearchTds').val('');
        //            BindGetTDScode();
        //            return false;
        //        }
        //    }
        //    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
        //        alert("Please Select TDS Name");
        //        BindGetTDScode();
        //        return false;
        //    }
        //});
        //});

        // ==================== End TDS keypress Type Not Allowed Coding Here ==========================//
        function BindGetTDScode() {
            $(".loading-overlay").show();
            var W = "{}";
            $.ajax({
                type: "POST",
                url: "MST_Acc_DepositMaster.aspx/GET_TdsName",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    $("#GridViewTds").empty();
                    if (data.d.length > 0) {
                        $("#GridViewTds").append("<tr><th>TDS ID</th><th>TDS DESC</th></tr>");
                        TdsNameShowModalPopup();
                        for (var i = 0; i < data.d.length; i++) {
                            $("#GridViewTds").append("<tr><td>" +
                            data.d[i].TDS_ID + "</td> <td>" +
                            data.d[i].TDS_DESC + "</td></tr>");
                        }
                        //===================== Start Tds Name Search is not Blank Then Tds Nmae Record Display in GridView ===========================
                        if ($('#txtSearchTds').val() != '') {
                            var rows;
                            var coldata;
                            $('#GridViewTds').find('tr:gt(0)').hide();
                            var data = $('#txtSearchTds').val();
                            var len = data.length;
                            if (len > 0) {
                                $('#GridViewTds').find('tbody tr').each(function () {
                                    coldata = $(this).children().eq(1);
                                    var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                                    if (temp === 0) {
                                        $(this).show();
                                    }

                                });
                            } else {
                                $('#GridViewTds').find('tr:gt(0)').show();

                            }
                        }


                        //===================== End Tds Name Search is not Blank Then Tds Nmae Record Display in GridView ===========================

                        $(".loading-overlay").hide();
                        $("[id*=GridViewTds] td").bind("click", function () {
                            var row = $(this).parent();
                            $("[id*=GridViewTds] tr").each(function () {
                                if ($(this)[0] != row[0]) {
                                    $("td", this).removeClass("selected_row");
                                }
                            });

                            $("td", row).each(function () {
                                if (!$(this).hasClass("selected_row")) {
                                    $(this).addClass("selected_row");
                                } else {
                                    $(this).removeClass("selected_row");
                                }
                            });
                        });


                        //===================== Start Selected Gridview Value Display in Text Box ===========================//bind
                        $(document).ready(function () {
                            $("[id*=GridViewTds] tbody tr").click("click", function () {

                                var ValTds = $(this).children("td:eq(1)").text();
                                $("#txtSearchTds").val(ValTds);
                                $("#txtSearchTds").focus();
                            });
                        });
                        //===================== End Selected Gridview Value Display in Text Box ===========================//
                    }
                    else {
                        alert("No Records Data");
                        $(".loading-overlay").hide();

                    }
                },
                error: function (result) {
                    alert("Error Records Data");
                    $(".loading-overlay").hide();
                }
            });
        }
        function TdsNameShowModalPopup() {
            $("#dialogTDS").dialog({
                title: "Tds Name  Search",
                width: 600,

                buttons: {
                    Ok: function () {
                        var GridViewTds = document.getElementById("GridViewTds");
                        var ValTdsID = '';
                        var ValTds = '';
                        var ValTds_Desc = '';
                        if ($('#txtSearchTds').val().trim() == '') {
                            alert("Please Select TDS Name");
                            $('#txtSearchTds').val('');
                            $("#txtSearchTds").focus();
                            return false;
                        }
                        if ($('#txtSearchTds').val() != '') {
                            for (var row = 1; row < GridViewTds.rows.length; row++) {
                                var GridViewTds_Cell = GridViewTds.rows[row].cells[1];
                                var valueTds = GridViewTds_Cell.textContent.toString();
                                if (valueTds == $('#txtSearchTds').val()) {
                                    ValTds = valueTds;
                                    ValTdsID = GridViewTds.rows[row].cells[0].textContent.toString();

                                    break;
                                }
                            }

                            if (ValTds != $('#txtSearchTds').val()) {
                                alert("Invalid TDS Name(TDS Name is Not in List)");
                                $("#txtSearchTds").focus();
                                return false;
                            }

                            else {


                                $("#dialogTDS").dialog('close');
                                $("#txtTDSName").val($("#txtSearchTds").val());
                                $("#txtTDSID").val(ValTdsID);
                                //$("#txtTDSName").val(ValISIN);


                                return false;
                            }

                        }
                    }
                },
                modal: true
            });
        }
        //====================================================End TDS Popup coding here=====================================================================//

        $(document).ready(function () {
            $("#txtSearchTds").keypress(function (e) {
                if (e.keyCode == 13) {


                    var GridViewTds = document.getElementById("GridViewTds");
                    var ValTdsID = '';
                    var ValTds = '';
                    var ValTds_Desc = '';
                    if ($('#txtSearchTds').val().trim() == '') {
                        alert("Please Select TDS Name");
                        $('#txtSearchTds').val('');
                        $("#txtSearchTds").focus();
                        return false;
                    }
                    if ($('#txtSearchTds').val() != '') {
                        for (var row = 1; row < GridViewTds.rows.length; row++) {
                            var GridViewTds_Cell = GridViewTds.rows[row].cells[1];
                            var valueTds = GridViewTds_Cell.textContent.toString();
                            if (valueTds == $('#txtSearchTds').val()) {
                                ValTds = valueTds;
                                ValTdsID = GridViewTds.rows[row].cells[0].textContent.toString();

                                break;
                            }
                        }

                        if (ValTds != $('#txtSearchTds').val()) {
                            alert("Invalid TDS Name(TDS Name is Not in List)");
                            $("#txtSearchTds").focus();
                            return false;
                        }

                        else {


                            $("#dialogTDS").dialog('close');
                            $("#txtTDSName").val($("#txtSearchTds").val());
                            $("#txtTDSID").val(ValTdsID);
                            //$("#txtTDSName").val(ValISIN);


                            return false;
                        }

                    }

                }
            });
        });

        // ==================== Start GL Voucher Window keypress Type Not Allowed Coding Here =========================//       GL_master
        $(document).ready(function () {
            $('#txtDepositeAt').click(function (event) {
                BindGetSubWidowcode();
            });
            $('#txtDepositeAt').keyup(function () {
                if ($('#txtDepositeAt').val().trim() == "")
                { }
            });
            //$('#txtDepositeAt').keypress(function (evt) {
            //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            //    if (iKeyCode == 13) {
            //        if ($('#txtDepositeAt').val() != '') {
            //            BindGetSubWidowcode();
            //            $('#txtSearchGlName').val($('#txtDepositeAt').val());
            //            return false;
            //        }
            //        else {
            //            $('#txtSearchGlName').val('');
            //            BindGetSubWidowcode();
            //            return false;
            //        }
            //    }
            //    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
            //        alert("Please Select GL Name");
            //        BindGetSubWidowcode();
            //        return false;
            //    }
            //});
        });
        //==============Start GL Name Popup coding here==============================================//
        function BindGetSubWidowcode() {
            $(".loading-overlay").show();
            var W = "{}";
            $.ajax({
                type: "POST",
                url: "MST_Acc_DepositMaster.aspx/GET_GLName",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    $("#GridViewGLName").empty();
                    if (data.d.length > 0) {
                        $("#GridViewGLName").append("<tr><th style='display:none;'>SL ID</th><th>Bank Code</th><th>Bank NAME</th></tr>");
                        GLNameShowModalPopup();
                        for (var i = 0; i < data.d.length; i++) {
                            $("#GridViewGLName").append("<tr><td style='display:none;'>" +
                            data.d[i].SL_ID + "</td> <td>" +
                            data.d[i].OLD_SL_ID + "</td> <td>" +
                            data.d[i].GL_NAME + "</td></tr>");
                        }
                        //===================== Start GL Name Search is not Blank Then Gl Name Record Display in GridView ===========================
                        if ($('#txtSearchGlName').val() != '') {
                            var rows;
                            var coldata;
                            $('#GridViewGLName').find('tr:gt(0)').hide();
                            var data = $('#txtSearchGlName').val();
                            var len = data.length;
                            if (len > 0) {
                                $('#GridViewGLName').find('tbody tr').each(function () {
                                    coldata = $(this).children().eq(1);
                                    var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                                    if (temp === 0) {
                                        $(this).show();
                                    }

                                });
                            } else {
                                $('#GridViewGLName').find('tr:gt(0)').show();

                            }
                        }


                        //===================== End GL Name Search is not Blank Then GL Number Record Display in GridView ===========================

                        $(".loading-overlay").hide();
                        $("[id*=GridViewGLName] td").bind("click", function () {
                            var row = $(this).parent();
                            $("[id*=GridViewGLName] tr").each(function () {
                                if ($(this)[0] != row[0]) {
                                    $("td", this).removeClass("selected_row");
                                }
                            });

                            $("td", row).each(function () {
                                if (!$(this).hasClass("selected_row")) {
                                    $(this).addClass("selected_row");
                                } else {
                                    $(this).removeClass("selected_row");
                                }
                            });
                        });


                        //===================== Start Selected Gridview Value Display in Text Box ===========================//bind
                        $(document).ready(function () {
                            $("[id*=GridViewGLName] tbody tr").click("click", function () {

                                var ValGLName = $(this).children("td:eq(2)").text();
                                $("#txtSearchGlName").val(ValGLName);
                                $("#txtSearchGlName").focus();
                            });
                        });
                        //===================== End Selected Gridview Value Display in Text Box ===========================//
                    }
                    else {
                        alert("No Records Data");
                        $(".loading-overlay").hide();

                    }
                },
                error: function (result) {
                    alert("Error Records Data");
                    $(".loading-overlay").hide();

                }

            });
        }

        function GLNameShowModalPopup() {
            $("#dialogSubVoucher").dialog({
                title: "GL Name Search",
                width: 600,

                buttons: {
                    Ok: function () {
                        // alert("ok");
                        var GridViewGLName = document.getElementById("GridViewGLName");
                        var valID = '';
                        var ValGL_Old_ID = '';
                        var ValGLName = '';
                        if ($('#txtSearchGlName').val().trim() == '') {
                            alert("Please Select GL Name");
                            $('#txtSearchGlName').val('');
                            $("#txtSearchGlName").focus();
                            return false;
                        }
                        if ($('#txtSearchGlName').val() != '') {

                            for (var row = 1; row < GridViewGLName.rows.length; row++) {

                                var GridGLName_Cell = GridViewGLName.rows[row].cells[2];

                                var valueGLName = GridGLName_Cell.textContent.toString();
                                if (valueGLName == $('#txtSearchGlName').val()) {
                                    ValGLName = valueGLName;

                                    ValGLID = GridViewGLName.rows[row].cells[0].textContent.toString();
                                    ValGL_Old_ID = GridViewGLName.rows[row].cells[1].textContent.toString();
                                    ValGLName = GridViewGLName.rows[row].cells[2].textContent.toString();
                                    //alert(ValGLID);

                                    break;
                                }
                            }

                            if (ValGLName != $('#txtSearchGLName').val()) {
                                // $("#txtDepositeAt").val(ValGLID);
                                $("#txtDepositeAt").val(ValGL_Old_ID);
                                $("#dialogSubVoucher").dialog('close');
                                //alert("Invalid GL Name (GL is Not in List)");
                                $("#txtSearchGLName").focus();
                                return false;
                            }
                            else {
                                //alert("1234");
                                $("#txtDepositeAt").val($("#txtSearchGLName").val());
                                //   $("#txtDepositeAt").val(ValGLID);
                                $("#txtDepositeAt").val(ValGL_Old_ID);
                                $("#dialogSubVoucher").dialog('close');
                                return false;
                            }

                        }
                    }
                },
                modal: true
            });
        }


        $(document).ready(function () {
            //autocompleteSchemeID();
            //autocompleteName();
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
        });

        function autocompleteSchemeID() {
            $("#txtSearchSchemeID").autocomplete({
                source: function (request, response) {
                    var SchemeID = $("#txtSearchSchemeID").val();
                    $("#txtSearchName").val('');
                    $("#txtSearchNameSchemeCode").val('');
                    var W = "{'SchemeID':'" + SchemeID + "'}";
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "MST_Acc_DepositMaster.aspx/GET_AutoComplete_SchemeID",
                        data: W,
                        dataType: "json",
                        success: function (data) {
                            //response(data.d);
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split("|")[0],
                                    val: item,
                                }
                            }))
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                //   appendTo: "#autocompletePensionerName",
                minLength: 0,
                select: function (e, i) {
                    var arr = i.item.val.split("|");
                    $("#txtSearchSchemeCode").val(arr[1]);
                    //return false;
                }
            }).click(function () {
                $(this).data("autocomplete").search($(this).val());
            });
        }


        function autocompleteName() {
            $("#txtSearchName").autocomplete({
                source: function (request, response) {
                    var BoundName = $("#txtSearchName").val();
                    $("#txtSearchSchemeID").val('');
                    $("#txtSearchSchemeCode").val('');
                    var W = "{'BoundName':'" + BoundName + "'}";
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "MST_Acc_DepositMaster.aspx/GET_AutoComplete_Name",
                        data: W,
                        dataType: "json",
                        success: function (data) {
                            //response(data.d);
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split("|")[0], // + '<>' + item.split("|")[1],
                                    val: item,
                                }
                            }))
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                //   appendTo: "#autocompletePensionerName",
                minLength: 0,
                select: function (e, i) {
                    var arr = i.item.val.split("|");
                    $("#txtSearchNameSchemeCode").val(arr[1]);
                    //return false;
                }
            }).click(function () {
                $(this).data("autocomplete").search($(this).val());
            });
        }

        function ShowBoundRecords() {
            SearchBoundRecords(0, 0);
        }

        $(document).ready(function () {
            $('#txtSearchName').keyup(function () {
                var ValName = '';
                ValName = $("#txtSearchName").val();
                $("#txtSearchSchemeID").val('');
                $("#txtSearchSchemeCode").val('');
                if ($('#txtSearchName').val().trim() != '') {
                    SearchBoundRecords(0, $("#txtSearchName").val());
                }
                else { SearchBoundRecords(0, 0); }
            });
        });

        $(document).ready(function () {
            $('#txtSearchSchemeID').keyup(function () {
                $("#txtSearchName").val('');
                $("#txtSearchNameSchemeCode").val('');
                if ($('#txtSearchSchemeID').val().trim() != "") {
                    SearchBoundRecords($("#txtSearchSchemeID").val(), 0);
                }
                else { SearchBoundRecords(0, 0); }
            });
        });

        function SearchBoundRecords(ValSchemeID, ValBondName) {
            $(".loading-overlay").show();
            var SchemeID = "";
            var BondName = "";
            SchemeID = ValSchemeID;
            BondName = ValBondName;

            var W = "{'SchemeID':'" + SchemeID + "','BondName':'" + BondName + "'}";
            //alert(W);
            $.ajax({
                type: "POST",
                url: "MST_Acc_DepositMaster.aspx/GET_FillGrid",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    $("#tbl").empty();
                    if (data.d.length > 0) {
                        $("#tbl").append("<tr><th style='width:110px'>Scheme ID </th><th>Issue Date</th><th>Quantity</th><th>Issue Amount</th><th style='width:300px'>Name</th><th style='width:200px'>Address</th><th>Maturity Date</th><th>Edit</th><th>Delete</th><th style='display:none'>Deposit ID</th><th style='display:none'>Scheme Code</th><th style='display:none'>Bond ID</th><th style='display:none'>Scheme Type</th></tr>");//
                        BoundRecordsShowModalPopup();
                        for (var i = 0; i < data.d.length; i++) {
                            $("#tbl").append("<tr><td style='text-align:left'>" +
                                    data.d[i].SchemeID + "</td> <td style='text-align:center'>" +
                                    data.d[i].IssueDate + "</td> <td style='text-align:center'>" +
                                    data.d[i].Qty + "</td> <td style='text-align:right'>" +
                                    data.d[i].IssueAmount + "</td> <td style='text-align:left'>" +
                                    data.d[i].Name + "</td> <td style='text-align:left'>" +
                                    data.d[i].ADDR + "</td> <td style='text-align:left'>" +
                                    data.d[i].MATURITY_DT + "</td> <td style='text-align:center;cursor:pointer'>" +
                                   "<div onClick='EditViewBondID(" + i + ");'><img src='images/Edit.jpg' id='EditViewBondID_" + i + "'/>" + "</td> <td style='text-align:center;cursor:pointer'>" +
                                   "<div onClick='CencelViewBondID(" + i + ");'><img src='images/Delete.gif' id='PrintViewBondID_" + i + "'/>" + "</td> <td style='display:none'>" +
                                   data.d[i].DepositID + "</td> <td style='display:none'>" +
                                   data.d[i].SchemeCode + "</td> <td style='display:none'>" +
                                   i + "</td> <td style='display:none'>" +
                                   data.d[i].SchemeType + "</td></tr>");

                        }


                        $(".loading-overlay").hide();
                        $("#txtSaveEdit").val('Edit');
                        $(function () {
                            $("[id*=tbl] td").bind("click", function () {
                                var row = $(this).parent();
                                $("[id*=tbl] tr").each(function (rowindex) {
                                    if ($(this)[0] != row[0]) {
                                        $("td", this).removeClass("selected_row");
                                    }
                                });

                                $("td", row).each(function () {
                                    if (!$(this).hasClass("selected_row")) {
                                        $(this).addClass("selected_row");
                                    } else {
                                        $(this).removeClass("selected_row");
                                    }
                                });
                            });
                        });
                        // Paging Function Calling
                    }
                    else {
                        //alert("No Records Data");
                        $(".loading-overlay").hide();
                        $("#tbl").empty();
                        $("#tbl").append("<tr><th style='width:90px'>Scheme ID </th><th>Issue Date</th><th>Quantity</th><th>Issue Amount</th><th style='width:300px'>Name</th><th style='width:200px'>Address</th><th>Maturity Date</th><th>Edit</th><th>Delete</th><th style='display:none'>Deposit ID</th><th style='display:none'>Scheme Code</th><th style='display:none'>Bond ID</th><th style='display:none'>Scheme Type</th></tr>");//
                        if ($("#txtSearchSchemeID").val() != '') {
                            $('#txtSearchSchemeID').focus();
                        }
                        if ($("#txtSearchName").val() != '') {
                            $('#txtSearchName').focus();
                        }
                        return false;

                    }
                },
                error: function (result) {
                    alert("Error Records Data");
                    $(".loading-overlay").hide();
                    return false;
                }
            });
        }

        function EditViewBondID(EditBondid) {
            $('#txtDeposit_ID').on('keydown', function (e) {
                e.preventDefault();
            });

            var GridBond = document.getElementById("tbl");
            var valSchType = '';
            var ValDepositID = '';
            var ValSchemeID = '';
            for (var row = 1; row < GridBond.rows.length; row++) {

                var GridDeposit_Cell = GridBond.rows[row].cells[9];
                var valueDepositeid = GridDeposit_Cell.textContent.toString();

                var GridScheme_Cell = GridBond.rows[row].cells[10];
                var valueSchemeid = GridScheme_Cell.textContent.toString();

                var GridBond_Cell = GridBond.rows[row].cells[11];
                var valueBondid = GridBond_Cell.textContent.toString();

                var GridSchType_Cell = GridBond.rows[row].cells[12];
                var valueSchType = GridSchType_Cell.textContent.toString();

                if (valueBondid == EditBondid) {
                    valSchType = valueSchType;
                    ValDepositID = valueDepositeid;
                    ValSchemeID = valueSchemeid;
                    break;
                }
            }

            var W = "{'DepositID':'" + ValDepositID + "','SchemeID':'" + ValSchemeID + "','SchType':'" + valSchType + "'}";
            $.ajax({
                type: "POST",
                url: "MST_Acc_DepositMaster.aspx/GET_ShowBond",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        $("#txtSchemeID").val(data.d[0].OLD_SCHEME_ID);

                        $("#txtSchemeName").val(data.d[0].SCHEME_NAME);
                        $("#txtScheme_ID1").val(data.d[0].SCHEME_ID);
                        $("#txtSchemeType").val(data.d[0].SCHEME_TYPE);
                        $("#txtBranchID").val(data.d[0].BRANCH_ID);
                        $("#txtBranchName1").val(data.d[0].SectorName);
                        $("#txtTDSID").val(data.d[0].TDS_ID);
                        $("#txtTDSName").val(data.d[0].TDS_DESC);
                        $("#txtAppl_No").val(data.d[0].APPL_NO);
                        $("#txtAppl_DT").val(data.d[0].APPL_DT);
                        $("#txtAppl_Amount").val(data.d[0].APPL_AMOUNT);
                        $("#txtIssue_DT").val(data.d[0].ISSUE_DT);
                        $("#txtDeposit_ID").val(data.d[0].DEPOSIT_ID);
                        $("#txtIssue_Amount").val(data.d[0].ISSU_AMOUNT);
                        $("#txtQty").val(data.d[0].QTY);
                        $("#txtDistinctive_Start").val(data.d[0].DISTINCTIVE_START);
                        $("#txtDistinctive_End").val(data.d[0].DISTINCTIVE_END);
                        $("#txtPeriod").val(data.d[0].PERIOD_DAYS);
                        $("#txtMaturity_DT").val(data.d[0].MATURITY_DT);
                        $("#txtRepaid_DT").val(data.d[0].REPAID_DT);
                        $("#txtName").val(data.d[0].APPLICANT_NAME);
                        $("#txtAddr1").val(data.d[0].ADDR1);
                        $("#txtAddr2").val(data.d[0].ADDR2);
                        $("#txtAddr3").val(data.d[0].ADDR3);
                        $("#txtAddr4").val(data.d[0].ADDR4);
                        $("#txtPin").val(data.d[0].PIN);
                        $("#txtPhone").val(data.d[0].PHONE);
                        $("#ddlpayment_Mode").val(data.d[0].PAYMENT_MODE);
                        document.getElementById("ddlpayment_Mode").disabled = true;
                        $("#txtRemarks").val(data.d[0].REMARKS);
                        $("#ddlNomination").val(data.d[0].NOMINATION);
                        document.getElementById("ddlNomination").disabled = true;
                        $("#txtDPID_No").val(data.d[0].DPID_NO);
                        $("#txtDrn_No").val(data.d[0].DRN_NO);
                        $("#txtISSIN_No").val(data.d[0].ISIN_NO);
                        $("#txtPan").val(data.d[0].PAN);

                        var CheckboxForm15 = data.d[0].FORM15H;

                        if (CheckboxForm15 == 'Y') {
                            document.getElementById("CheckBox1").checked = true;
                        }

                        else { document.getElementById("CheckBox1").checked = false; }

                        $("#txtF15H_FOR_FY").val(data.d[0].F15H_FOR_FY);
                        $("#txtBankName").val(data.d[0].BANK_NAME);
                        $("#txtBranchName").val(data.d[0].BRANCH_NAME);
                        $("#txtAccNo").val(data.d[0].BANK_ACC_NO);

                        $("#txtDepositeAt").val(data.d[0].BANK_CD);

                        $("#txtChequeNo").val(data.d[0].CHQ_NO);
                        $("#txtCheque_DT").val(data.d[0].CHQ_DT);
                        $("#txtAllotment_DT").val(data.d[0].ALLOTMENT_DT);
                        $("#txtDraweeBank").val(data.d[0].DRAWEE_BANK);
                        $("#txtArrangerName").val(data.d[0].ARRANGER_NAME);
                        $("#txtApplication_DT").val(data.d[0].APPLICATION_DATE);
                        $("#txtRtgsIfsc").val(data.d[0].RTGS_IFSC_NO);
                        var SchFrom = data.d[0].SCHEME_FROM;
                        $("#txtMaturity_DT").datepicker("option", "minDate", SchFrom)


                        $("#cmdSave").val('Update');
                        $("#dialogSearchBound").dialog('close');

                        $(".loading-overlay").hide();
                    }
                    else {
                        alert("No Records Data");
                        $(".loading-overlay").hide();
                    }
                },
                error: function (result) {
                    alert("Error Records Data...");
                    $(".loading-overlay").hide();

                }
            });

        }

        function CencelViewBondID(DelBondSchemeID) {
            $(".loading-overlay").show();
            var GridBond = document.getElementById("tbl");
            var valSchType = '';
            var ValDepositID = '';
            var ValSchemeID = '';
            for (var row = 1; row < GridBond.rows.length; row++) {

                var GridDeposit_Cell = GridBond.rows[row].cells[9];
                var valueDepositeid = GridDeposit_Cell.textContent.toString();

                var GridScheme_Cell = GridBond.rows[row].cells[10];
                var valueSchemeid = GridScheme_Cell.textContent.toString();

                var GridBond_Cell = GridBond.rows[row].cells[11];
                var valueBondid = GridBond_Cell.textContent.toString();

                var GridSchType_Cell = GridBond.rows[row].cells[12];
                var valueSchType = GridSchType_Cell.textContent.toString();

                if (parseInt(valueBondid) == parseInt(DelBondSchemeID)) {
                    valSchType = valueSchType;
                    ValDepositID = valueDepositeid;
                    ValSchemeID = valueSchemeid;
                    break;
                }
            }

            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("Do you want to Cancel Indent Record?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }

            if (confirm_value.value == "Yes") {
                var W = "{'DepositID':'" + ValDepositID + "','SchemeID':'" + ValSchemeID + "','SchType':'" + valSchType + "'}";
                $.ajax({
                    type: "POST",
                    url: "MST_Acc_DepositMaster.aspx/GET_Delete_Bond",
                    contentType: "application/json;charset=utf-8",
                    data: W,
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length > 0) {
                            var msg = data.d;
                            SearchBoundRecords(0, 0);
                            alert(msg);
                            $(".loading-overlay").hide();
                        }
                        else {
                            alert("No Records Data");
                            $(".loading-overlay").hide();
                        }
                    },
                    error: function (result) {
                        alert("Error Records Data...");
                        $(".loading-overlay").hide();

                    }  
                });
            }
            else { $(".loading-overlay").hide(); return false; }
        }

        function BoundRecordsShowModalPopup() {
            $("#dialogSearchBound").dialog({
                title: "Bond Records Display By Search Part",
                width: 900,

                buttons: {
                    Ok: function () {
                        $("#dialogSearchBound").dialog('close');
                        // alert("ok");

                    }
                },
                modal: true
            });
        }





        //var GridItemDetail = document.getElementById("tbl");
        //var tbody = GridItemDetail.getElementsByTagName("tbody")[0];


        //tbody.onclick = function (e) {
        //    e = e || window.event;
        //    var data = [];
        //    var target = e.srcElement || e.target;
        //    while (target && target.nodeName !== "TR") {
        //        target = target.parentNode;
        //    }

        //    if (target) {
        //        var cells = target.getElementsByTagName("td");
        //        for (var i = 0; i < cells.length; i++) {

        //            if (i == 1) {
        //                a = cells[i].textContent;
        //            }
        //            if (i == 7) {
        //                b = cells[i].textContent;
        //            }
        //            if (i == 8) {
        //                c = cells[i].textContent;
        //            }

        //        }
        //    }
        //};


        //alert(a);
        //alert(b);
        //alert(c);




    </script>
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
	<div align="center">
		<table class="headingCaption" width="98%" align="center">
			<tr>
				<td>BOND MASTER</td>
			</tr>
		</table>  
		<br />
		<table width="100%" cellpadding="6" class="borderStyle" align="center" cellspacing="0">
			<tr>
				<td class="auto-style1">
					<asp:FormView ID="dv" runat="server" Width="100%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
						OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
						<InsertItemTemplate>

                            <table align="center" cellpadding="2" width="100%">
                               
                                <tr>
                                    <td>   
                                       <%-- class="blink"--%>
                                        <a href="javascript:ShowBoundRecords()" autocomplete="off" runat="server" clientidmode="Static" style="color:red;font-size:16px;" >Click For Search Bond Master</a> <%--class="blink"--%>
                                        <asp:TextBox ID="txtSaveEdit" autocomplete="off"  Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   
                                    </td>
                                   <%-- <td style="font-size:12px;color:navy" align="left" >Scheme ID &nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span> </td>
                                    <td align="left" style="width:2px">                
                                        <asp:TextBox ID="txtSearchSchemeID" Width="200px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>  
                                         <asp:TextBox ID="txtSearchSchemeCode" Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   
                                    </td>

                                     <td style="font-size:12px;color:navy" align="left" >Name &nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>    </td>
                                    <td align="right" style="width:20px">                
                                        <asp:TextBox ID="txtSearchName" Width="200px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>  
                                         <asp:TextBox ID="txtSearchNameSchemeCode"  Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   

                                    </td>--%>

                                    <%--<td align="right">
                                         <asp:Button ID="btnSearchBound" runat="server" Text="Search" OnClientClick="ShowBoundRecords()"  Width="100px" CssClass="save-button DefaultButton"/>
                                        <asp:Button ID="btnSearchCancel" runat="server" Text="Cancel " OnClick="btnSearchCancel_Click"  Width="100px" CssClass="save-button"/>
                                        <asp:TextBox ID="txtSaveEdit"  Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   
                                    </td>
                                    <td align="right" style="width:180px"></td>--%>
                                    
                                </tr>
                                <tr>
                                    <td colspan="9" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>

                            </table>

							<table align="center" cellpadding="4" width="100%">
								<tr>

									<td class="labelCaption">Issue Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>

									<td align="left">
										<asp:TextBox ID="txtSchemeID" autocomplete="off" ClientIDMode="Static"  runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">

										<asp:TextBox ID="txtSchemeName" autocomplete="off" MaxLength="500" ClientIDMode="Static" placeholder="Scheme Name" runat="server" CssClass="textbox"></asp:TextBox>
										<asp:TextBox ID="txtSchemeType" autocomplete="off" Style="display: none;"  ClientIDMode="Static" runat="server" Text=""></asp:TextBox>
										 <asp:TextBox ID="txtScheme_ID1" autocomplete="off" Style="display: none;"  ClientIDMode="Static" runat="server" Text=""></asp:TextBox>
									</td>
									<td class="labelCaption">Form 15H<span class="require"></span> </td>
									<td align="left">
										<asp:CheckBox ID="CheckBox1" autocomplete="off" ClientIDMode="Static" runat="server" /><%--onClick="displayNote()" --%>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">Branch Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBranchID" autocomplete="off" MaxLength="50"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">
										<asp:TextBox ID="txtBranchName1" autocomplete="off" MaxLength="500" ClientIDMode="Static" runat="server" placeholder="Branch Name" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Valid For F.Y<span class="require"></span> </td>
									<td align="left">
										<asp:TextBox ID="txtF15H_FOR_FY" autocomplete="off" Width="100px"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>

									</td>

								</tr>
								<tr>
									<td class="labelCaption">TDS Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtTDSID" autocomplete="off" MaxLength="10"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">
										<asp:TextBox ID="txtTDSName" autocomplete="off" MaxLength="500" ClientIDMode="Static" runat="server" placeholder="TDS Name" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Application Date<span class="require"></span> </td>
									<td align="left">
										<asp:TextBox ID="txtApplication_DT" autocomplete="off" MaxLength="10" Width="210px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Application No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_No" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Bond Holder ID&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDeposit_ID" autocomplete="off" Style="text-transform:uppercase" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>                                  

								</tr>
								<tr>
									<td class="labelCaption">Quantity.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtQty" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>




									<td class="labelCaption">Issue Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtIssue_DT" autocomplete="off" MaxLength="10" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

									<td class="labelCaption">Quantity Distinctive No. Start&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDistinctive_Start" autocomplete="off" ClientIDMode="Static" onblur="fnValidateQuty(this);" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Quantity Distinctive No. End&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDistinctive_End" autocomplete="off" ClientIDMode="Static"  runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
								</tr>
								<tr>
									<td class="labelCaption">Period In Months&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPeriod" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Allotment Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAllotment_DT" autocomplete="off" MaxLength="10" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

								<tr>
									<td class="labelCaption">Name&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Repaid Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtRepaid_DT" autocomplete="off" MaxLength="10" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

								<tr>
									<td class="labelCaption">Address&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr1" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Application Amount&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_Amount" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>

								<tr>
								<tr>
									<td class="labelCaption">Address 2&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr2" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Maturity Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtMaturity_DT" autocomplete="off" ClientIDMode="Static" MaxLength="10" runat="server"  CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Address 3&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr3" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Pan&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPan" autocomplete="off" onblur="fnValidatePAN(this);" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Address 4&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr4" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Allotment Amount&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtIssue_Amount" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">PIN&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPin" autocomplete="off" MaxLength="6" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Phone&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPhone" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">RePayment Mode&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:DropDownList ID="ddlpayment_Mode" autocomplete="off" Width="220px" Height="25px" CssClass="textbox" runat="server">
											<asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
											<asp:ListItem Text="Monthly" Value="M"></asp:ListItem>
											<asp:ListItem Text="Quarterly" Value="Q"></asp:ListItem>
											<asp:ListItem Text="Half-Yearly" Value="H"></asp:ListItem>
											<asp:ListItem Text="Yearly" Value="Y"></asp:ListItem>
											<asp:ListItem Text="Full" Value="F"></asp:ListItem>
										</asp:DropDownList>
									</td>
									<%--</td>--%>
									<td class="labelCaption">DPID No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDPID_No" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>

								<tr>
									<%--</td>--%>
									<td class="labelCaption">Remarks&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left" colspan="4">
										<asp:TextBox ID="txtRemarks" autocomplete="off" Width="850px" Height="25" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<%--</td>--%>
									<td class="labelCaption">Nomination&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:DropDownList ID="ddlNomination" autocomplete="off" Width="220px" Height="25px" CssClass="textbox" runat="server">
											<asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
											<asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
											<asp:ListItem Text="No" Value="N"></asp:ListItem>
										</asp:DropDownList>
									</td>
									<td class="labelCaption">Encashment Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_DT" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								 
								</tr>
								<tr>
									<td class="labelCaption">Client ID /DRN No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDrn_No" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">ISIN No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtISSIN_No" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Bank Name&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBankName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>


									<td class="labelCaption">Branch Name &nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBranchName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">Account No.&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAccNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>


									<td class="labelCaption">RTGS/IFSC No.&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtRtgsIfsc" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>


								</tr>
							</table>
						</InsertItemTemplate>
						<EditItemTemplate>
							<table align="center" cellpadding="4" width="100%">
								<tr>
									<td class="labelCaption">Issue Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtSchemeID" autocomplete="off" ClientIDMode="Static"  Enabled="false" Text='<%# Eval("OLD_SCHEME_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left"> 
										<asp:TextBox ID="txtSchemeName" autocomplete="off" MaxLength="500" ClientIDMode="Static" Enabled="false" Text='<%# Eval("SCHEME_NAME") %>' runat="server" CssClass="textbox"></asp:TextBox>
										<asp:TextBox ID="txtSchemeType" autocomplete="off" Style="display: none;" ReadOnly="true" Text='<%# Eval("SCHEME_ID") %>' ClientIDMode="Static" runat="server" ></asp:TextBox>
										<asp:TextBox ID="txtScheme_ID1" autocomplete="off" Text='<%# Eval("SCHEME_TYPE") %>' Style="display: none;"   ClientIDMode="Static" runat="server" ></asp:TextBox>
									</td>
									<td class="labelCaption">Form 15H<span class="require"></span> </td>
									<td align="left">
										<asp:CheckBox ID="CheckBox1" autocomplete="off" ClientIDMode="Static" runat="server"  /><%--onClick="displayNote()"--%>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Branch Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBranchID" autocomplete="off"  MaxLength="50" ClientIDMode="Static"   Text='<%# Eval("BRANCH_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">
										<asp:TextBox ID="txtBranchName1" autocomplete="off" MaxLength="500" ClientIDMode="Static" Text='<%# Eval("SectorName") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Valid For F.Y<span class="require"></span> </td>
									<td align="left">
										<asp:TextBox ID="txtF15H_FOR_FY" autocomplete="off" Width="100px"  ClientIDMode="Static" Text='<%# Eval("F15H_FOR_FY") %>' runat="server" CssClass="textbox"></asp:TextBox>

									</td>

								</tr>
								<tr>
									<td class="labelCaption">TDS Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtTDSID" autocomplete="off" MaxLength="10" ClientIDMode="Static"  Text='<%# Eval("TDS_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">
										<asp:TextBox ID="txtTDSName" autocomplete="off" MaxLength="500" ClientIDMode="Static" Text='<%# Eval("TDS_DESC") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
                                    <td class="labelCaption">Application Date<span class="require">*</span> </td>
									<td align="left">
										<asp:TextBox ID="txtApplication_DT" autocomplete="off"  Width="210px" Text='<%# Eval("APPLICATION_DATE") %>' ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Application No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_No" autocomplete="off" ClientIDMode="Static" MaxLength="10" Text='<%# Eval("APPL_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Bond No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDeposit_ID" autocomplete="off" ClientIDMode="Static"  Text='<%# Eval("DEPOSIT_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								   <%-- <td class="labelCaption">Encashment Date &nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_DT" ClientIDMode="Static" Text='<%# Eval("APPL_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>--%>

								</tr>
								<tr>
									  <td class="labelCaption">Quantity.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtQty" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("QTY") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<%--<td class="labelCaption">Bond No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDeposit_ID" ClientIDMode="Static" ReadOnly="true" Text='<%# Eval("DEPOSIT_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>--%>
									<td class="labelCaption">Issue Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtIssue_DT" autocomplete="off"  ClientIDMode="Static" Text='<%# Eval("ISSUE_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

									<td class="labelCaption">Quantity Distinctive No. Start&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDistinctive_Start" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("DISTINCTIVE_START") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Quantity Distinctive No. End&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDistinctive_End" autocomplete="off" ClientIDMode="Static"  Text='<%# Eval("DISTINCTIVE_END") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
								</tr>
								<tr>
									<td class="labelCaption">Period In Days&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPeriod" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("PERIOD_DAYS") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Allotment Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAllotment_DT" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("ALLOTMENT_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

								<tr>
									<td class="labelCaption">Name&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtName" autocomplete="off" ClientIDMode="Static" MaxLength="100" Text='<%# Eval("APPLICANT_NAME") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Repaid Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtRepaid_DT" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("REPAID_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

								<tr>
									<td class="labelCaption">Address&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr1" autocomplete="off" ClientIDMode="Static" MaxLength="50" Text='<%# Eval("ADDR1") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Application Amount &nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_Amount" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("APPL_AMOUNT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>

								<tr>
								<tr>
									<td class="labelCaption">Address 2&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr2" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("ADDR2") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Maturity Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtMaturity_DT1" autocomplete="off" ClientIDMode="Static" MaxLength="10"  IDMode="Static" Text='<%# Eval("MATURITY_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Address 3&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr3" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("ADDR3") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Pan&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPan" autocomplete="off" MaxLength="12" onblur="fnValidatePAN(this);" ClientIDMode="Static" Text='<%# Eval("PAN") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Address 4&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr4" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("ADDR4") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

									<td class="labelCaption">Issue Amount&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtIssue_Amount" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("ISSU_AMOUNT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">PIN&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPin" autocomplete="off" MaxLength="6" ClientIDMode="Static" Text='<%# Eval("PIN") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Phone&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPhone" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("PHONE") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">RePayment Mode&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:DropDownList ID="ddlpayment_Mode" autocomplete="off" Enabled="False" Width="220px" Height="25px" CssClass="textbox" Text='<%# Eval("PAYMENT_MODE") %>' runat="server">
											<asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
											<asp:ListItem Text="Monthly" Value="M"></asp:ListItem>
											<asp:ListItem Text="Quarterly" Value="Q"></asp:ListItem>
											<asp:ListItem Text="Half-Yearly" Value="H"></asp:ListItem>
											<asp:ListItem Text="Yearly" Value="Y"></asp:ListItem>
											<asp:ListItem Text="Full" Value="F"></asp:ListItem>
										</asp:DropDownList>
									</td>
									</td>
									<td class="labelCaption">DPID No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDPID_No" autocomplete="off" ClientIDMode="Static" MaxLength="20" Text='<%# Eval("DPID_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>

								<tr>
									</td>
									<td class="labelCaption">Remarks&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left" colspan="4">
										<asp:TextBox ID="txtRemarks" autocomplete="off" Width="850px" Height="25px" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("REMARKS") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									</td>
									<td class="labelCaption">Nomination&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>

									<td align="left">
										<asp:DropDownList ID="ddlNomination" autocomplete="off" Enabled="False" Width="220px" Height="25px" CssClass="textbox" Text='<%# Eval("NOMINATION") %>' runat="server">
											<asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
											<asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
											<asp:ListItem Text="No" Value="N"></asp:ListItem>
										</asp:DropDownList>

									</td>
									 <td class="labelCaption">Encashment Date &nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_DT" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("APPL_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								  <%--  <td class="labelCaption">Quantity.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtQty" ClientIDMode="Static" Text='<%# Eval("QTY") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>--%>

								</tr>

								<tr>
									<td class="labelCaption">Client ID /DRN No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDrn_No" autocomplete="off" ClientIDMode="Static" MaxLength="20" Text='<%# Eval("DRN_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">ISIN No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtISSIN_No" autocomplete="off" ClientIDMode="Static" MaxLength="20" Text='<%# Eval("ISIN_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Bank Name&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBankName" autocomplete="off" ClientIDMode="Static" MaxLength="100" Text='<%# Eval("BANK_NAME") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>


									<td class="labelCaption">Branch Name &nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBranchName" autocomplete="off" ClientIDMode="Static" MaxLength="100" Text='<%# Eval("BRANCH_NAME") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">Account No.&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAccNo" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("BANK_ACC_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>


									<td class="labelCaption">RTGS/IFSC No.&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtRtgsIfsc" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("RTGS_IFSC_NO") %>' MaxLength="100" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
							</table>
						</EditItemTemplate>
						<FooterTemplate>
							<table align="center" cellpadding="5" width="100%">
								<tr>
									<td colspan="4" class="labelCaption">
										<hr class="borderStyle" />
									</td>
								</tr>
								<tr>
									<td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
									<td>&nbsp;</td>
									<td align="left">
										<div style="float: left; margin-left: 200px;">
											<asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" OnClick="cmdSave_Click" autocomplete="off"
												Width="100px" CssClass="save-button " OnClientClick='javascript: return beforeSave()' />
											<asp:Button ID="cmdCancel" runat="server" Text="Refresh" autocomplete="off"
												Width="100px" CssClass="save-button" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()'/>

                                            <%--OnClientClick='javascript: return unvalidate()'--%>
										</div>
									</td>
								</tr>
							</table>
						</FooterTemplate>
					</asp:FormView>
				</td>
			</tr>
			<%--<tr>
				<td colspan="4" class="auto-style2">
					<hr class="borderStyle" />
				</td>
			</tr>--%>
            
			<tr>
				<td class="auto-style1">
					
                         

					<%-- <%-- ================ START Scheme  Code POP UP GRIDVIEW =====================  --%>
					<div id="dialogSchmeNo" style=" display: none">

						<asp:TextBox ID="txtSearchSchemeName" autocomplete="off" style="text-transform:uppercase;" runat="server" MaxLength="780" CssClass="Foliotextposition"></asp:TextBox>
						<div id="dvpop" style="overflow-y:scroll;max-height:400px;">
						<asp:GridView ID="GridViewSchemeName" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
							PageSize="9" CssClass="GridFolioDes" AllowPaging="true">
							<Columns>
								<asp:BoundField DataField="OLD_SCHEME_ID" HeaderText="OLD SCHEME ID" ItemStyle-Width="100" />
								<asp:BoundField DataField="SCHEME_NAME" HeaderText="SCHEME NAME" ItemStyle-Width="200" />
								<asp:BoundField DataField="SCHEME_FROM" HeaderText="SCHEME FROM" ItemStyle-Width="100" />
								<asp:BoundField DataField="REPAYMENT_MODE" HeaderText="REPAYMENT MODE" ItemStyle-Width="50" />
								<asp:BoundField DataField="SLR_NSLR" HeaderText="SLR NSLR" ItemStyle-Width="30" />
								<asp:BoundField DataField="PERIOD_DAYS" HeaderText="PERIOD DAYS" ItemStyle-Width="50" />
								<asp:BoundField DataField="SCHEME_TYPE" HeaderText="SCHEME TYPE" ItemStyle-Width="100" />
								<asp:BoundField DataField="ISIN_NO" HeaderText="ISIN NO" ItemStyle-Width="20" />
								<asp:BoundField DataField="DPID_NO" HeaderText="DPID NO" ItemStyle-Width="20" />
							</Columns>
						</asp:GridView>
							</div>
					</div>
					<%-- ================ END Scheme Code POP UP GRIDVIEW =====================  --%>


					<%--================ START voucher window  Code POP UP GRIDVIEW ===================== --%>
					<div id="dialogpopDiv" style="display: none">
						<table id="window123">
							<tr>
								<th style="padding:5px;">Cheque No.</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtChequeNo" runat="server" CssClass="textbox"></asp:TextBox></td>
								<th style="padding:5px;">Cheque Date</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtCheque_DT" autocomplete="off" runat="server" CssClass="textbox"></asp:TextBox></td>
							</tr>

							<tr>
								<th style="padding:5px;">Drawee Bank</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtDraweeBank" autocomplete="off" runat="server" CssClass="textbox"></asp:TextBox></td>

								<th style="padding:5px;">Arranger Name</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtArrangerName" autocomplete="off" runat="server" CssClass="textbox"></asp:TextBox></td>
							</tr>
							<tr>
								<th style="padding:5px;">Deposited At</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtDepositeAt" autocomplete="off" runat="server" CssClass="textbox"></asp:TextBox></td>
							</tr>

						</table>

					</div>
					<%-- ================ END Sector Code POP UP GRIDVIEW =====================  --%>


					<%-- ================ START Sector  Code POP UP GRIDVIEW ===================== --%>
					<div id="dialogSectorNo" style="display: none">

						<asp:TextBox ID="txtSearchSectorName" autocomplete="off" runat="server" MaxLength="480" CssClass="Foliotextposition"></asp:TextBox>
						<div id="123" style="overflow-y:scroll;max-height:400px;">
						<asp:GridView ID="GridViewSectorName" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
							PageSize="5" CssClass="GridFolioDes" AllowPaging="true">
							<Columns>
								<asp:BoundField DataField="SectorID" HeaderText="Sector ID" ItemStyle-Width="200" />
								<asp:BoundField DataField="SectorCode" HeaderText="Sector Code" ItemStyle-Width="200" />
								<asp:BoundField DataField="SectorName" HeaderText="SectorName" ItemStyle-Width="300" />

							</Columns>
						</asp:GridView>
							</div>
					</div>
					<%-- ================ END Sector Code POP UP GRIDVIEW =====================  --%>


					<%-- ================ START Tds  Code POP UP GRIDVIEW =====================  --%>
					<div id="dialogTDS" style="display: none">

						<asp:TextBox ID="txtSearchTds" autocomplete="off" runat="server" MaxLength="480" CssClass="Foliotextposition"></asp:TextBox>
						 <div id="142" style="overflow-y:scroll;max-height:400px;">
						<asp:GridView ID="GridViewTds" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
							PageSize="5" CssClass="GridFolioDes" AllowPaging="true">
							<Columns>
								<asp:BoundField DataField="TDS_ID" HeaderText="TDS_ID" ItemStyle-Width="200" />
								<asp:BoundField DataField="TDS_DESC" HeaderText="TDS_DESC TYPE" ItemStyle-Width="300" />
							</Columns>
						</asp:GridView>
							 </div>
					</div>
					<%-- ================ END Tds Code POP UP GRIDVIEW =====================  --%>

					<%-- ================ START GL Voucher  Window POP UP GRIDVIEW ===================== --%>
					<div id="dialogSubVoucher" style="display: none">

						<asp:TextBox ID="txtSearchGlName" autocomplete="off" runat="server" MaxLength="480" CssClass="Foliotextposition"></asp:TextBox>
						<div id="vd212" style="overflow-y:scroll;max-height:400px;">
						<asp:GridView ID="GridViewGLName" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
							PageSize="5" CssClass="GridFolioDes" AllowPaging="true">
							<Columns>
								<asp:BoundField DataField="SL_ID" HeaderText="SL ID" ItemStyle-Width="200" />
								<asp:BoundField DataField="OLD_SL_ID" HeaderText="OLD SL ID Code" ItemStyle-Width="200" />
								<asp:BoundField DataField="GL_NAME" HeaderText="GL NAME" ItemStyle-Width="300" />
							</Columns>
						</asp:GridView>
							</div>
					</div>
					<%-- ================ END GL Voucher Window Code POP UP GRIDVIEW =====================  --%>

                    <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>
                       
	               <%-- =================== Bound Records Display By Search Part =============--%>
                    
                    <div id="dialogSearchBound" runat="server" style="display: none">
                        <asp:Label ID="Label1" autocomplete="off" runat="server" Text="Scheme ID : "></asp:Label>
                        <asp:TextBox ID="txtSearchSchemeID" autocomplete="off" Width="240px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>  
                        <asp:TextBox ID="txtSearchSchemeCode" autocomplete="off" Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   

                        <asp:Label ID="Label2" runat="server" autocomplete="off" Text="Name : "></asp:Label>
                        <asp:TextBox ID="txtSearchName" autocomplete="off" Width="340px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>  
                        <asp:TextBox ID="txtSearchNameSchemeCode" autocomplete="off"  Width="10px" style="text-transform:uppercase;display:none;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   

						<%--<asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
							AutoGenerateColumns="false" DataKeyNames="DEPOSIT_ID" OnRowCommand="tbl_RowCommand" 
                            CellPadding="2" CellSpacing="2" AllowPaging="false">--%>
                        <asp:GridView ID="tbl" Width="100%" autocomplete="off" ClientIDMode="Static" style="border:2px solid #59bdcc"  
                            clientID="tbl" runat="server" AutoGenerateColumns="false">
							<AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                 <asp:BoundField DataField="OLD_SCHEME_ID" HeaderText="SCHEME ID" ItemStyle-Width="200" /> 
                                <asp:BoundField DataField="ISSUE_DT" HeaderText="ISSUE DATE" ItemStyle-Width="300" /> 
                                <asp:BoundField DataField="QTY" HeaderText="QTY" ItemStyle-Width="50" /> 
                                <asp:BoundField DataField="ISSU_AMOUNT" HeaderText="ISSUE AMOUNT" ItemStyle-Width="50" />  
                                <asp:BoundField DataField="APPLICANT_NAME" HeaderText="Name" ItemStyle-Width="400" />                
                                <asp:BoundField DataField="ADDR" HeaderText="Address" ItemStyle-Width="200" />                
                                <asp:BoundField DataField="MATURITY_DT" HeaderText="MATURITY DATE" ItemStyle-Width="300" /> 
                                
                                <asp:BoundField DataField="DEPOSITSCHEME_ID" HeaderText="Edit" ItemStyle-Width="50" />
                                <asp:BoundField DataField="DEPOSITSCHEME_ID" HeaderText="Delete" ItemStyle-Width="50" />

                                <asp:BoundField DataField="DEPOSIT_ID" HeaderText="DEPOSIT_ID" ItemStyle-Width="50" />
                                <asp:BoundField DataField="SCHEME_ID" HeaderText="SCHEME_ID" ItemStyle-Width="50" />
                                <asp:BoundField DataField="BOND_ID" HeaderText="BOND_ID" ItemStyle-Width="50" />
                                <asp:BoundField DataField="SCHEME_TYPE" HeaderText="SCHEME TYPE" ItemStyle-Width="300" /> 
                            </Columns>

						</asp:GridView>
							</div>

				</td>
			</tr>
		</table>
	</div>
</asp:Content>
