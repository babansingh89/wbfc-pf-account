﻿<%@ Page Language="C#" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ReportView.aspx.cs" Inherits="ReportView" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<html xmlns="http://www.w3.org/1999/xhtml">
  
<script src="crystalreportviewers13/js/crviewer/crv.js"></script>

<head id="Head1" runat="server">
    <title>Report Viewer</title>

    <style type="text/css">
        #table1
        {       
            height: 29px;      
        }
                  
                        
        .style5      
        {            
            width: 157px;
        }
                
        .style8
        {
            width: 4px;
        }
        
        .style9
        {
            width: 70px;
        }
        
    </style>                   
        
</head>                    
<body>
    <form id="form1" runat="server">

        <div id="dvReport">

            <table style="width: 100%; text-align: center;" id='table1'>
                <tr>

                    <td style="text-align: left; background-color: #E1F0FF;">
                        <div style="float: left; width: 1072px; height: 27px;">
                            <div style="float: left; width: 150px;">
                                <asp:RadioButtonList ID="rbFormat" autocomplete="off" runat="server" RepeatDirection="Horizontal" Width="130px">
                                    <%-- <asp:ListItem Text="Excel" Value="Excel" Enabled="false" />--%>
                                    <asp:ListItem Text="PDF" Value="PDF" Selected="True" />
                                </asp:RadioButtonList>

                            </div>
                            <div style="float: left; width: 200px;">
                                &nbsp;&nbsp;
            <asp:Button ID="btnExport" autocomplete="off" Text="Export File" runat="server" OnClick="ExportPDF" />

                                &nbsp;&nbsp;
                 <asp:Button ID="CmdPrint" autocomplete="off" runat="server" Text="Print"
                     Width="60px" OnClick="OnConfirmprint" OnClientClick="DirectPrint()" />

                            </div>
                            <div style="float: left; width: 320px;">

                                <table style="width: 124%; text-align: center;">
                                    <tr>
                                        <td style="padding: 2px;" class="style9"><span class="headFont">Paper Size </span></td>
                                        <td style="padding: 2px;" class="style8">:</td>
                                        <td style="padding: 2px; text-align: left;" class="style5">
                                            <asp:DropDownList ID="ddlPaperSize" autocomplete="off" Width="100px" Height="22px" CssClass="textbox" Enabled="True" runat="server">
                                                <asp:ListItem Value="0">Select Paper Size</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>

                                    </tr>
                                </table>

                            </div>

                        </div>

                    </td>

                </tr>

            </table>
            <div>
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" autocomplete="off"
                    Height="100%" Width="100%" GroupTreeStyle-ShowLines="False"
                    HasCrystalLogo="False" HasDrilldownTabs="False" HasDrillUpButton="False"
                    HasExportButton="False" HasPrintButton="True" HasToggleGroupTreeButton="False"
                    ShowAllPageIds="True" ReuseParameterValuesOnRefresh="true"
                    ToolPanelView="None" ViewStateMode="Enabled" OnDisposed="Page_Unload" />



            </div>

        </div>

        &nbsp;       
    <script type="text/javascript">

        function DirectPrint() {

            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("Do you want to Print data?")) {
                confirm_value.value = "Yes";

            } else {
                confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
            if (confirm_value = "Yes") {

            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    </script>
    </form>
</body>
</html>
