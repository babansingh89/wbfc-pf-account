﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MST_Acc_DepositMaster.aspx.cs" Inherits="MST_Acc_DepositMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript" src="js/BondMaster.js"></script>

	<style type="text/css">
			.textbox {
			border: 1px solid #c4c4c4;
			height: 15px;
			width: 210px;
			font-size: 13px;
			padding: 4px 4px 4px 4px;
			border-radius: 4px;
			-moz-border-radius: 4px;
			-webkit-border-radius: 4px;
			box-shadow: 0px 0px 8px #d9d9d9;
			-moz-box-shadow: 0px 0px 8px #d9d9d9;
			-webkit-box-shadow: 0px 0px 8px #d9d9d9;
		}

			.textbox:focus {
				outline: none;
				border: 1px solid #7bc1f7;
				box-shadow: 0px 0px 8px #7bc1f7;
				-moz-box-shadow: 0px 0px 8px #7bc1f7;
				-webkit-box-shadow: 0px 0px 8px #7bc1f7;
			}

	</style>

	<style type="text/css">
		body {
			font-family: Arial;
			font-size: 10pt;
		}

		td {
		}

		table {
			border: 1px solid #ccc;
			border-collapse: collapse;
			background-color: #fff;
			font-size: 10pt !important;
			font-family: Arial !important;
		}

			table th {
				background-color: #B8DBFD;
				color: #333;
				font-weight: bold;
			}

			table, table table td {
				border: 0px solid #ccc;
			}
		/*table, th, td {
			cellpadding:1px;
			cellspacing:1px;
		}*/
		th {
			background: lightskyblue;
		}

		.selected_row {
			background-color: #A1DCF2 !important;
		}

		.Gridposition {
			margin-left: 10px;
			width: 300px;
		}

		.textposition {
			margin-left: 1px;
			width: 295px;
		}

		.GridFolioDes {
			margin-left: 1px;
			width: 570px;
		}

		.Foliotextposition {
			margin-left: 2px;
			width: 565px;
		}

		.TextSearchVendor {
			margin-left: 1px;
			width: 516px;
		}

		.GridVendor {
			margin-left: 1px;
			width: 520px;
		}

		.GridAllIndent {
			margin-left: 1px;
			width: 800px;
		}

		.input3 {
			margin-left: 1px;
			width: 200px;
		}

		/*=================================================*/
		.Grid {
			border: solid 1px #59bdcc;
			border-collapse: collapse;
			width: 100%;
			text-align: center;
		}

			.Grid td {
				padding: 2px;
				border: solid 1px #59bdcc;
				color: navy;
				background-color: #e6f3be;
			}

			.Grid th {
				padding: 4px 2px;
				background: #609eb3;
				font-size: 0.9em;
			}

		.auto-style1 {
			width: 802px;
		}

		.auto-style2 {
			font-family: Segoe UI;
			font-size: 12px;
			color: Navy;
			text-align: left;
			width: 802px;
		}
	</style>
	<script type="text/javascript">       

	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
	<script type="text/javascript" src="js/BondMaster.js"></script>
	<div align="center">
		<table class="headingCaption" width="98%" align="center">
			<tr>
				<td>BOND MASTER</td>
			</tr>
		</table>  
		<br />
		<table width="100%" cellpadding="6" class="borderStyle" align="center" cellspacing="0">
			<tr>
				<td class="auto-style1">
					<asp:FormView ID="dv" runat="server" Width="100%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
						OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
						<InsertItemTemplate>

                            <table align="center" cellpadding="2" width="100%">
                               
                                <tr>
                                    <td>   

                                        <a href="javascript:ShowBoundRecords()">Bound Master Search</a>
                                        <asp:TextBox ID="txtSaveEdit" autocomplete="off" Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   
                                    </td>
                                   <%-- <td style="font-size:12px;color:navy" align="left" >Scheme ID &nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span> </td>
                                    <td align="left" style="width:2px">                
                                        <asp:TextBox ID="txtSearchSchemeID" Width="200px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>  
                                         <asp:TextBox ID="txtSearchSchemeCode" Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   
                                    </td>

                                     <td style="font-size:12px;color:navy" align="left" >Name &nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>    </td>
                                    <td align="right" style="width:20px">                
                                        <asp:TextBox ID="txtSearchName" Width="200px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>  
                                         <asp:TextBox ID="txtSearchNameSchemeCode"  Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   

                                    </td>--%>

                                    <%--<td align="right">
                                         <asp:Button ID="btnSearchBound" runat="server" Text="Search" OnClientClick="ShowBoundRecords()"  Width="100px" CssClass="save-button DefaultButton"/>
                                        <asp:Button ID="btnSearchCancel" runat="server" Text="Cancel " OnClick="btnSearchCancel_Click"  Width="100px" CssClass="save-button"/>
                                        <asp:TextBox ID="txtSaveEdit"  Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   
                                    </td>
                                    <td align="right" style="width:180px"></td>--%>
                                    
                                </tr>
                                <tr>
                                    <td colspan="9" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>

                            </table>

							<table align="center" cellpadding="4" width="100%">
								<tr>

									<td class="labelCaption">Issue Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>

									<td align="left">
										<asp:TextBox ID="txtSchemeID" autocomplete="off" ClientIDMode="Static"  runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">

										<asp:TextBox ID="txtSchemeName" autocomplete="off" MaxLength="500" ClientIDMode="Static" placeholder="Scheme Name" runat="server" CssClass="textbox"></asp:TextBox>
										<asp:TextBox ID="txtSchemeType" autocomplete="off" Style="display: none;"  ClientIDMode="Static" runat="server" Text=""></asp:TextBox>
										 <asp:TextBox ID="txtScheme_ID1" autocomplete="off" Style="display: none;"  ClientIDMode="Static" runat="server" Text=""></asp:TextBox>
									</td>
									<td class="labelCaption">Form 15H<span class="require"></span> </td>
									<td align="left">
										<asp:CheckBox ID="CheckBox1" autocomplete="off" ClientIDMode="Static" runat="server" /><%--onClick="displayNote()" --%>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">Branch Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBranchID" autocomplete="off" MaxLength="50"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">
										<asp:TextBox ID="txtBranchName1" autocomplete="off" MaxLength="500" ClientIDMode="Static" runat="server" placeholder="Branch Name" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Valid For F.Y<span class="require"></span> </td>
									<td align="left">
										<asp:TextBox ID="txtF15H_FOR_FY" autocomplete="off" Width="100px"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>

									</td>

								</tr>
								<tr>
									<td class="labelCaption">TDS Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtTDSID" autocomplete="off" MaxLength="10"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">
										<asp:TextBox ID="txtTDSName" autocomplete="off" MaxLength="500" ClientIDMode="Static" runat="server" placeholder="TDS Name" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Application Date<span class="require">*</span> </td>
									<td align="left">
										<asp:TextBox ID="txtApplication_DT" autocomplete="off" Width="210px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Application No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_No" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Bond No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDeposit_ID" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>                                  

								</tr>
								<tr>
									<td class="labelCaption">Quantity.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtQty" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>




									<td class="labelCaption">Issue Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtIssue_DT" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

									<td class="labelCaption">Quantity Distinctive No. Start&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDistinctive_Start" autocomplete="off" ClientIDMode="Static" onblur="fnValidateQuty(this);" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Quantity Distinctive No. End&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDistinctive_End" autocomplete="off" ClientIDMode="Static"  runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
								</tr>
								<tr>
									<td class="labelCaption">Period In Days&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPeriod" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Allotment Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAllotment_DT" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

								<tr>
									<td class="labelCaption">Name&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Repaid Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtRepaid_DT" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

								<tr>
									<td class="labelCaption">Address&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr1" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Application Amount&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_Amount" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>

								<tr>
								<tr>
									<td class="labelCaption">Address 2&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr2" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Maturity Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtMaturity_DT" autocomplete="off" ClientIDMode="Static" runat="server"  CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Address 3&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr3" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Pan&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPan" autocomplete="off" onblur="fnValidatePAN(this);" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Address 4&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr4" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Issue Amount&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtIssue_Amount" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">PIN&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPin" autocomplete="off" MaxLength="6" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Phone&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPhone" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">RePayment Mode&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:DropDownList ID="ddlpayment_Mode" autocomplete="off" Width="220px" Height="25px" CssClass="textbox" runat="server">
											<asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
											<asp:ListItem Text="Monthly" Value="M"></asp:ListItem>
											<asp:ListItem Text="Quarterly" Value="Q"></asp:ListItem>
											<asp:ListItem Text="Hafeyearly" Value="H"></asp:ListItem>
											<asp:ListItem Text="Yearly" Value="Y"></asp:ListItem>
											<asp:ListItem Text="Full" Value="F"></asp:ListItem>
										</asp:DropDownList>
									</td>
									<%--</td>--%>
									<td class="labelCaption">DPID No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDPID_No" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>

								<tr>
									<%--</td>--%>
									<td class="labelCaption">Remarks&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left" colspan="4">
										<asp:TextBox ID="txtRemarks" autocomplete="off" Width="930px" Height="25" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<%--</td>--%>
									<td class="labelCaption">Nomination&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:DropDownList ID="ddlNomination" autocomplete="off" Width="220px" Height="25px" CssClass="textbox" runat="server">
											<asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
											<asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
											<asp:ListItem Text="No" Value="N"></asp:ListItem>
										</asp:DropDownList>
									</td>
									<td class="labelCaption">Encashment Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_DT" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								 
								</tr>
								<tr>
									<td class="labelCaption">Client ID /DRN No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDrn_No" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">ISIN No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtISSIN_No" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Bank Name&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBankName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>


									<td class="labelCaption">Branch Name &nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBranchName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">Account No.&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAccNo" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>


									<td class="labelCaption">RTGS/IFSC No.&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtRtgsIfsc" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>


								</tr>
							</table>
						</InsertItemTemplate>
						<EditItemTemplate>
							<table align="center" cellpadding="4" width="100%">
								<tr>
									<td class="labelCaption">Issue Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtSchemeID" autocomplete="off" ClientIDMode="Static"  Enabled="false" Text='<%# Eval("OLD_SCHEME_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left"> 
										<asp:TextBox ID="txtSchemeName" autocomplete="off" MaxLength="500" ClientIDMode="Static" Enabled="false" Text='<%# Eval("SCHEME_NAME") %>' runat="server" CssClass="textbox"></asp:TextBox>
										<asp:TextBox ID="txtSchemeType" autocomplete="off" Style="display: none;" ReadOnly="true" Text='<%# Eval("SCHEME_ID") %>' ClientIDMode="Static" runat="server" ></asp:TextBox>
										<asp:TextBox ID="txtScheme_ID1" autocomplete="off" Text='<%# Eval("SCHEME_TYPE") %>' Style="display: none;"   ClientIDMode="Static" runat="server" ></asp:TextBox>
									</td>
									<td class="labelCaption">Form 15H<span class="require"></span> </td>
									<td align="left">
										<asp:CheckBox ID="CheckBox1" autocomplete="off" ClientIDMode="Static" runat="server"  /><%--onClick="displayNote()"--%>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Branch Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBranchID" autocomplete="off" MaxLength="50" ClientIDMode="Static"   Text='<%# Eval("BRANCH_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">
										<asp:TextBox ID="txtBranchName1" autocomplete="off" MaxLength="500" ClientIDMode="Static" Text='<%# Eval("SectorName") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Valid For F.Y<span class="require"></span> </td>
									<td align="left">
										<asp:TextBox ID="txtF15H_FOR_FY" autocomplete="off" Width="100px"  ClientIDMode="Static" Text='<%# Eval("F15H_FOR_FY") %>' runat="server" CssClass="textbox"></asp:TextBox>

									</td>

								</tr>
								<tr>
									<td class="labelCaption">TDS Code&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtTDSID" autocomplete="off" MaxLength="10" ClientIDMode="Static"  Text='<%# Eval("TDS_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td align="left">
										<asp:TextBox ID="txtTDSName" autocomplete="off" MaxLength="500" ClientIDMode="Static" Text='<%# Eval("TDS_DESC") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
                                    <td class="labelCaption">Application Date<span class="require">*</span> </td>
									<td align="left">
										<asp:TextBox ID="txtApplication_DT" autocomplete="off" Width="210px" Text='<%# Eval("APPLICATION_DATE") %>' ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Application No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_No" autocomplete="off" ClientIDMode="Static" MaxLength="10" Text='<%# Eval("APPL_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Bond No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDeposit_ID" autocomplete="off" ClientIDMode="Static"  Text='<%# Eval("DEPOSIT_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								   <%-- <td class="labelCaption">Encashment Date &nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_DT" ClientIDMode="Static" Text='<%# Eval("APPL_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>--%>

								</tr>
								<tr>
									  <td class="labelCaption">Quantity.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtQty" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("QTY") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<%--<td class="labelCaption">Bond No.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDeposit_ID" ClientIDMode="Static" ReadOnly="true" Text='<%# Eval("DEPOSIT_ID") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>--%>
									<td class="labelCaption">Issue Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtIssue_DT" autocomplete="off"  ClientIDMode="Static" Text='<%# Eval("ISSUE_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

									<td class="labelCaption">Quantity Distinctive No. Start&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDistinctive_Start" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("DISTINCTIVE_START") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Quantity Distinctive No. End&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDistinctive_End" autocomplete="off" ClientIDMode="Static"  Text='<%# Eval("DISTINCTIVE_END") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
								</tr>
								<tr>
									<td class="labelCaption">Period In Days&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPeriod" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("PERIOD_DAYS") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Allotment Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAllotment_DT" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("ALLOTMENT_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

								<tr>
									<td class="labelCaption">Name&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtName" autocomplete="off" ClientIDMode="Static" MaxLength="100" Text='<%# Eval("APPLICANT_NAME") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Repaid Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtRepaid_DT" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("REPAID_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>

								<tr>
									<td class="labelCaption">Address&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr1" autocomplete="off" ClientIDMode="Static" MaxLength="50" Text='<%# Eval("ADDR1") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Application Amount &nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_Amount" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("APPL_AMOUNT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>

								<tr>
								<tr>
									<td class="labelCaption">Address 2&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr2" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("ADDR2") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Maturity Date&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtMaturity_DT" autocomplete="off" ClientIDMode="Static"  IDMode="Static" Text='<%# Eval("MATURITY_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Address 3&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr3" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("ADDR3") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Pan&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPan" autocomplete="off" MaxLength="12" onblur="fnValidatePAN(this);" ClientIDMode="Static" Text='<%# Eval("PAN") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Address 4&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAddr4" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("ADDR4") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

									<td class="labelCaption">Issue Amount&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtIssue_Amount" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("ISSU_AMOUNT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">PIN&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPin" autocomplete="off" MaxLength="6" ClientIDMode="Static" Text='<%# Eval("PIN") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">Phone&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtPhone" autocomplete="off" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("PHONE") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">RePayment Mode&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:DropDownList ID="ddlpayment_Mode" autocomplete="off" Enabled="False" Width="220px" Height="25px" CssClass="textbox" Text='<%# Eval("PAYMENT_MODE") %>' runat="server">
											<asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
											<asp:ListItem Text="Monthly" Value="M"></asp:ListItem>
											<asp:ListItem Text="Quarterly" Value="Q"></asp:ListItem>
											<asp:ListItem Text="Hafeyearly" Value="H"></asp:ListItem>
											<asp:ListItem Text="Yearly" Value="Y"></asp:ListItem>
											<asp:ListItem Text="Full" Value="F"></asp:ListItem>
										</asp:DropDownList>
									</td>
									</td>
									<td class="labelCaption">DPID No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDPID_No" autocomplete="off" ClientIDMode="Static" MaxLength="20" Text='<%# Eval("DPID_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>

								<tr>
									</td>
									<td class="labelCaption">Remarks&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left" colspan="4">
										<asp:TextBox ID="txtRemarks" autocomplete="off" Width="930px" Height="25px" MaxLength="50" ClientIDMode="Static" Text='<%# Eval("REMARKS") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									</td>
									<td class="labelCaption">Nomination&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>

									<td align="left">
										<asp:DropDownList ID="ddlNomination" autocomplete="off" Enabled="False" Width="220px" Height="25px" CssClass="textbox" Text='<%# Eval("NOMINATION") %>' runat="server">
											<asp:ListItem Text="SELECT" Value="0"></asp:ListItem>
											<asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
											<asp:ListItem Text="No" Value="N"></asp:ListItem>
										</asp:DropDownList>

									</td>
									 <td class="labelCaption">Encashment Date &nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAppl_DT" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("APPL_DT") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								  <%--  <td class="labelCaption">Quantity.&nbsp;&nbsp;<span class="require">*</span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtQty" ClientIDMode="Static" Text='<%# Eval("QTY") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>--%>

								</tr>

								<tr>
									<td class="labelCaption">Client ID /DRN No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtDrn_No" autocomplete="off" ClientIDMode="Static" MaxLength="20" Text='<%# Eval("DRN_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
									<td class="labelCaption">ISIN No&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtISSIN_No" autocomplete="off" ClientIDMode="Static" MaxLength="20" Text='<%# Eval("ISIN_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
								<tr>
									<td class="labelCaption">Bank Name&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBankName" autocomplete="off" ClientIDMode="Static" MaxLength="100" Text='<%# Eval("BANK_NAME") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>


									<td class="labelCaption">Branch Name &nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtBranchName" autocomplete="off" ClientIDMode="Static" MaxLength="100" Text='<%# Eval("BRANCH_NAME") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>

								</tr>
								<tr>
									<td class="labelCaption">Account No.&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtAccNo" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("BANK_ACC_NO") %>' runat="server" CssClass="textbox"></asp:TextBox>
									</td>


									<td class="labelCaption">RTGS/IFSC No.&nbsp;&nbsp;<span class="require"></span> </td>
									<td class="labelCaption">:</td>
									<td align="left">
										<asp:TextBox ID="txtRtgsIfsc" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("RTGS_IFSC_NO") %>' MaxLength="100" runat="server" CssClass="textbox"></asp:TextBox>
									</td>
								</tr>
							</table>
						</EditItemTemplate>
						<FooterTemplate>
							<table align="center" cellpadding="5" width="100%">
								<tr>
									<td colspan="4" class="labelCaption">
										<hr class="borderStyle" />
									</td>
								</tr>
								<tr>
									<td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
									<td>&nbsp;</td>
									<td align="left">
										<div style="float: left; margin-left: 200px;">
											<asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" OnClick="cmdSave_Click" autocomplete="off"
												Width="100px" CssClass="save-button " OnClientClick='javascript: return beforeSave()' />
											<asp:Button ID="cmdCancel" runat="server" Text="Refresh" autocomplete="off"
												Width="100px" CssClass="save-button" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()'/>

                                            <%--OnClientClick='javascript: return unvalidate()'--%>
										</div>
									</td>
								</tr>
							</table>
						</FooterTemplate>
					</asp:FormView>
				</td>
			</tr>
			</table>
			<%--<tr>--%>
				<%--<td class="auto-style1">--%>
					


                    













					<%-- <%-- ================ START Scheme  Code POP UP GRIDVIEW =====================  --%>
					<div id="dialogSchmeNo" style=" display: none">

						<asp:TextBox ID="txtSearchSchemeName" autocomplete="off" runat="server" MaxLength="780" CssClass="Foliotextposition"></asp:TextBox>
						<div id="dvpop" autocomplete="off" runat="server"  clientidmode="Static" style="overflow-y:scroll;max-height:400px;">
						<asp:GridView ID="GridViewSchemeName" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
							PageSize="9" CssClass="GridFolioDes" AllowPaging="true">
							<Columns>
								<asp:BoundField DataField="OLD_SCHEME_ID" HeaderText="OLD SCHEME ID" ItemStyle-Width="100" />
								<asp:BoundField DataField="SCHEME_NAME" HeaderText="SCHEME NAME" ItemStyle-Width="200" />
								<asp:BoundField DataField="SCHEME_FROM" HeaderText="SCHEME FROM" ItemStyle-Width="100" />
								<asp:BoundField DataField="REPAYMENT_MODE" HeaderText="REPAYMENT MODE" ItemStyle-Width="50" />
								<asp:BoundField DataField="SLR_NSLR" HeaderText="SLR NSLR" ItemStyle-Width="30" />
								<asp:BoundField DataField="PERIOD_DAYS" HeaderText="PERIOD DAYS" ItemStyle-Width="50" />
								<asp:BoundField DataField="SCHEME_TYPE" HeaderText="SCHEME TYPE" ItemStyle-Width="100" />
								<asp:BoundField DataField="ISIN_NO" HeaderText="ISIN NO" ItemStyle-Width="20" />
								<asp:BoundField DataField="DPID_NO" HeaderText="DPID NO" ItemStyle-Width="20" />
							</Columns>
						</asp:GridView>
							</div>
					</div>
					<%-- ================ END Scheme Code POP UP GRIDVIEW =====================  --%>


					<%--================ START voucher window  Code POP UP GRIDVIEW ===================== --%>
					<div id="dialogpopDiv" style="display: none">
						<table id="window123">
							<tr>
								<th style="padding:5px;">Cheque No.</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtChequeNo" runat="server" CssClass="textbox"></asp:TextBox></td>
								<th style="padding:5px;">Cheque Date</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtCheque_DT" autocomplete="off" runat="server" CssClass="textbox"></asp:TextBox></td>
							</tr>

							<tr>
								<th style="padding:5px;">Drawee Bank</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtDraweeBank" autocomplete="off" runat="server" CssClass="textbox"></asp:TextBox></td>

								<th style="padding:5px;">Arranger Name</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtArrangerName" autocomplete="off" runat="server" CssClass="textbox"></asp:TextBox></td>
							</tr>
							<tr>
								<th style="padding:5px;">Deposited At</th>
								<td style="padding:5px;">
									<asp:TextBox ID="txtDepositeAt" autocomplete="off" runat="server" CssClass="textbox"></asp:TextBox></td>
							</tr>

						</table>

					</div>
					<%-- ================ END Sector Code POP UP GRIDVIEW =====================  --%>


					<%-- ================ START Sector  Code POP UP GRIDVIEW ===================== --%>
					<div id="dialogSectorNo" style="display: none">

						<asp:TextBox ID="txtSearchSectorName" autocomplete="off" runat="server" MaxLength="480" CssClass="Foliotextposition"></asp:TextBox>
						<div id="Div123" autocomplete="off" runat="server" clientidmode="Static" style="overflow-y:scroll;max-height:400px;">
						<asp:GridView ID="GridViewSectorName" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
							PageSize="5" CssClass="GridFolioDes" AllowPaging="true">
							<Columns>
								<asp:BoundField DataField="SectorID" HeaderText="Sector ID" ItemStyle-Width="200" />
								<asp:BoundField DataField="SectorCode" HeaderText="Sector Code" ItemStyle-Width="200" />
								<asp:BoundField DataField="SectorName" HeaderText="SectorName" ItemStyle-Width="300" />

							</Columns>
						</asp:GridView>
							</div>
					</div>
					<%-- ================ END Sector Code POP UP GRIDVIEW =====================  --%>


					<%-- ================ START Tds  Code POP UP GRIDVIEW =====================  --%>
					<div id="dialogTDS" style="display: none">

						<asp:TextBox ID="txtSearchTds" autocomplete="off" runat="server" MaxLength="480" CssClass="Foliotextposition"></asp:TextBox>
						 <div id="Div142" autocomplete="off" runat="server" clientidmode="Static" style="overflow-y:scroll;max-height:400px;">
						<asp:GridView ID="GridViewTds" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
							PageSize="5" CssClass="GridFolioDes" AllowPaging="true">
							<Columns>
								<asp:BoundField DataField="TDS_ID" HeaderText="TDS_ID" ItemStyle-Width="200" />
								<asp:BoundField DataField="TDS_DESC" HeaderText="TDS_DESC TYPE" ItemStyle-Width="300" />
							</Columns>
						</asp:GridView>
							 </div>
					</div>
					<%-- ================ END Tds Code POP UP GRIDVIEW =====================  --%>

					<%-- ================ START GL Voucher  Window POP UP GRIDVIEW ===================== --%>
					<div id="dialogSubVoucher" style="display: none">

						<asp:TextBox ID="txtSearchGlName" autocomplete="off" runat="server" MaxLength="480" CssClass="Foliotextposition"></asp:TextBox>
						<div id="vd212" style="overflow-y:scroll;max-height:400px;">
						<asp:GridView ID="GridViewGLName" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
							PageSize="5" CssClass="GridFolioDes" AllowPaging="true">
							<Columns>
								<asp:BoundField DataField="SL_ID" HeaderText="SL ID" ItemStyle-Width="200" />
								<asp:BoundField DataField="OLD_SL_ID" HeaderText="OLD SL ID Code" ItemStyle-Width="200" />
								<asp:BoundField DataField="GL_NAME" HeaderText="GL NAME" ItemStyle-Width="300" />
							</Columns>
						</asp:GridView>
							</div>
					</div>
					<%-- ================ END GL Voucher Window Code POP UP GRIDVIEW =====================  --%>

                    <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>
                       
	               <%-- =================== Bound Records Display By Search Part =============--%>
                    
                    <div id="dialogSearchBound" runat="server" style="display: none">
                        <asp:Label ID="Label1" autocomplete="off" runat="server" Text="Scheme ID : "></asp:Label>
                        <asp:TextBox ID="txtSearchSchemeID" autocomplete="off" Width="240px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>  
                        <asp:TextBox ID="txtSearchSchemeCode" autocomplete="off" Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   

                        <asp:Label ID="Label2" runat="server" autocomplete="off" Text="Name : "></asp:Label>
                        <asp:TextBox ID="txtSearchName" autocomplete="off" Width="340px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>  
                        <asp:TextBox ID="txtSearchNameSchemeCode"  autocomplete="off" Width="10px" style="text-transform:uppercase;display:none;"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>                   

						<%--<asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
							AutoGenerateColumns="false" DataKeyNames="DEPOSIT_ID" OnRowCommand="tbl_RowCommand" 
                            CellPadding="2" CellSpacing="2" AllowPaging="false">--%>
                        <asp:GridView ID="tbl" autocomplete="off" Width="100%"  ClientIDMode="Static" style="border:2px solid #59bdcc"  
                            clientID="tbl" runat="server" AutoGenerateColumns="false">
							<AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                 <asp:BoundField DataField="OLD_SCHEME_ID" HeaderText="SCHEME ID" ItemStyle-Width="200" /> 
                                <asp:BoundField DataField="ISSUE_DT" HeaderText="ISSUE DATE" ItemStyle-Width="300" /> 
                                <asp:BoundField DataField="QTY" HeaderText="QTY" ItemStyle-Width="50" /> 
                                <asp:BoundField DataField="ISSU_AMOUNT" HeaderText="ISSUE AMOUNT" ItemStyle-Width="50" />  
                                <asp:BoundField DataField="APPLICANT_NAME" HeaderText="Name" ItemStyle-Width="400" />                
                                <asp:BoundField DataField="ADDR" HeaderText="Address" ItemStyle-Width="200" />                
                                <asp:BoundField DataField="MATURITY_DT" HeaderText="MATURITY DATE" ItemStyle-Width="300" /> 
                                
                                <asp:BoundField DataField="DEPOSITSCHEME_ID" HeaderText="Edit" ItemStyle-Width="50" />
                                <asp:BoundField DataField="DEPOSITSCHEME_ID" HeaderText="Delete" ItemStyle-Width="50" />

                                <asp:BoundField DataField="DEPOSIT_ID" HeaderText="DEPOSIT_ID" ItemStyle-Width="50" />
                                <asp:BoundField DataField="SCHEME_ID" HeaderText="SCHEME_ID" ItemStyle-Width="50" />
                                <asp:BoundField DataField="BOND_ID" HeaderText="BOND_ID" ItemStyle-Width="50" />
                                <asp:BoundField DataField="SCHEME_TYPE" HeaderText="SCHEME TYPE" ItemStyle-Width="300" /> 
                            </Columns>

						</asp:GridView>
							</div>
					</div>

				<%--</td>
			</tr>
		</table>
	</div>--%>
</asp:Content>
