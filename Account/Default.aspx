<%@ Page Language="C#" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>WEST BENGAL FINANCIAL CORPORATION</title>
<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" type="image/ico" href="favicon.ico" />
<link href="css/homestyle.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.12.1.js"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">


</script>
</head>
<body>
    <form runat="server" id="form1">
        <div id="main_wraper">
            <div class="top_bg">
                <p>
                    <img src="images/logo.gif" alt="logo" runat="server" /></p>
            </div>
            <div class="banner_bg">
                <p class="left">
                    <img src="images/banner.jpg" alt="banner" runat="server"  /></p>
                <div class="right_pannel">
                    <p style="font-size:22px;text-align:center;">
                        Welcome to Control Panel of Accounts 
                    </p>
                    <br />
                    <br />
                    <br />
                    <br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <form action="form" method="get">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="justify22">
                                        <tr>
                                            <td style="width:28%;text-align:left">User Name</td>
                                            <td style="width:4%;text-align:left;">:</td>
                                            <td style="width:68%;text-align:left">
                                                <asp:TextBox id="txtUserID" runat="server" clientidmode="Static" autocomplete="off" name="txtUserID" class="text_field"></asp:TextBox>
                                                <%--<input type="text" id="txtUserID" runat="server" clientidmode="Static" autocomplete="off" name="txtUserID" class="text_field" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">&nbsp;</td>
                                            <td align="left">&nbsp;</td>
                                            <td align="left">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left">Password</td>
                                            <td align="left">:</td>
                                            <td align="left">
                                                <asp:TextBox id="txtPWD" name="txtPWD" type="password" runat="server" clientidmode="Static" autocomplete="off" class="text_field"></asp:TextBox>
                                                <%--<input id="txtPWD" name="txtPWD" type="password" runat="server" clientidmode="Static" autocomplete="off" class="text_field" />--%></td>
                                        </tr>
                                        <tr>
                                            <td align="left">&nbsp;</td>
                                            <td align="left">&nbsp;</td>
                                            <td align="left">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="left">&nbsp;</td>
                                            <td align="left">&nbsp;</td>
                                            <td align="left">
                                                <asp:Button ID="Submit" runat="server" autocomplete="off" OnClientClick="javascript: return frmvalidate();" ClientIDMode="Static"
                                                    OnClick="btnSubmit_Clicked" CssClass="submit_bg" Text="" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                        </tr>
                                        
                                    </table>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <div style="font-family: Verdana; font-size: 14px; font-weight: bold; color: lemonchiffon;">
                                </div>
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
                <p class="clr"></p>
            </div>
            <div class="scorl_bg">
               
            </div>
            <br />
            <div class="footer_bg">
                <p class="text03" align="center">Webel Technology Limited - Powered BY EIIPL</p>
            </div>
        </div>
    </form>
</body>
</html>