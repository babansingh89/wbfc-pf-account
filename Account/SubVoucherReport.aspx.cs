﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;

using System.Web.Script.Services;
using System.Web.Services;


public partial class SubVoucherReport : System.Web.UI.Page
{
    static string StrFormula = "";

    protected string YearMonth = "";
    protected string VchType = "";
    protected string VchDate = "";
    protected string VchNo = "";
    protected string VchStatus = "";
    protected string str = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            
            if (!IsPostBack)
            {
                
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string[] GLAutoCompleteData(string GL)
    {
        List<string> Detail = new List<string>();
        //int Sectorid = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        try
        {
            DataTable dt = DBHandler.GetResult("GET_AllDetail_ByOldSLId_Pf", GL, HttpContext.Current.Session["Menu_For"].ToString());
            foreach (DataRow dtRow in dt.Rows)
            {
                Detail.Add(dtRow["GL_ID"].ToString() + "|" + dtRow["SL_ID"].ToString() + "|" + dtRow["SUBTYPE"].ToString() + "|" + dtRow["GL"].ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Detail.ToArray();
    }
    [WebMethod]
    public static string[] SLAutoCompleteData(string SL, string SubType)
    {
        List<string> Detail = new List<string>();
        int Sectorid = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        try
        {
            DataTable dt = DBHandler.GetResult("GET_AllDetail_ByLoanEmpId_Pf", SL, SubType, Sectorid, HttpContext.Current.Session["Menu_For"].ToString());
            foreach (DataRow dtRow in dt.Rows)
            {
                Detail.Add(dtRow["ID"].ToString() + "|" + dtRow["NAME"].ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Detail.ToArray();
    }


    protected void cmdCancelSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string FromDT, string ToDT, string GLID, string SLID, string SubType, string SL_SUB_Code)
    {
        string JSONVal = "";

        try
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            String Msg = "";
            StrFormula = "";
            int UserID;
            int SectorID;
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            DateTime dtFromDT = DateTime.ParseExact(FromDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtToDT = DateTime.ParseExact(ToDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            StrFormula = "";

            if (SL_SUB_Code == "")
            {
                SL_SUB_Code = "0";
            }

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, dtFromDT.ToShortDateString(), dtToDT.ToShortDateString(), GLID, SLID, SubType, SL_SUB_Code, SectorID, "", "", "");

            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }
}