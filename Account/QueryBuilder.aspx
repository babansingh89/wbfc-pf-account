﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="QueryBuilder.aspx.cs" Inherits="QueryBuilder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<script type="text/javascript" src="js/SanctionReport.js"></script>--%>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert('Only Numeric Value Accept!');
                return false;
            }
            return true;
        }

        $(document).ready(function () {
            $("#txtQueryBuild").focus();
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
            //$("#txtFrmDate").focus();
            //$("#ddlReportName").val('RR');

            $("#ddlReportName").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#txtFrmDate").focus();
                    return false;
                }
            });

            $("#txtFrmDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#txtToDate").focus();
                    return false;
                }
            });

            $("#txtToDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#btnGenerateReport").focus();
                    return false;
                }
            });

            $("#btnGenerateReport").click(function (event) {
                if ($("#ddlReportName").val() == "0") {
                    alert("Select a Report Name!");
                    $("#ddlReportName").focus();
                    return false;
                }
                if ($("#txtFrmDate").val() == "") {
                    alert("Please Enter Recovery From Year&Month!");
                    $("#txtFrmDate").focus();
                    return false;
                }
                if ($("#txtToDate").val() == "") {
                    alert("Please Enter Recovery To Year&Month!");
                    $("#txtToDate").focus();
                    return false;
                }
                if ($("#txtFrmDate").val().length < 6) {
                    alert('Please Enter Valid Recovery From Year&Month!');
                    $("#txtFrmDate").focus();
                    return false;
                }
                if ($("#txtToDate").val().length < 6) {
                    alert('Please Enter Valid Recovery To Year&Month!');
                    $("#txtFrmDate").focus();
                    return false;
                }
                if ($("#txtFrmDate").val() > $("#txtToDate").val()) {
                    alert('Recovery From Year&Month Shoulb be Less Than Or Qual To Recovery To Year&Month!');
                    $("#txtFrmDate").focus();
                    return false;
                }
               
                ShowReport();
                return false;
            });

            function ShowReport() {
                var FormName = '';
                var ReportName = '';
                var ReportType = '';
                var RecFromDate = '';
                var RecToDate = '';
                if ($("#ddlReportName").val() == "RR") {
                    FormName = "RecoveryReport.aspx";
                    ReportName = "Recovery";
                    ReportType = "Recovery Report";
                }
                RecFromDate = $('#txtFrmDate').val();
                RecToDate = $('#txtToDate').val();

                var E = '';
                E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  RecFromDate:'" + RecFromDate + "',  RecToDate:'" + RecToDate + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/SetReportValue',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var show = response.d;
                        if (show == "OK") {
                            window.open("ReportView.aspx?E=Y");
                        }

                    },
                    error: function (response) {
                        var responseText;
                        responseText = JSON.parse(response.responseText);
                        alert("Error : " + responseText.Message);
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            
        });

        //$("#btnPrint").click(function () {

        function CreateGridHeader(id) {
            var print = document.getElementById(id);
            var w = window.open("", "");
            var headContent = document.getElementsByTagName('head')[0].outerHTML;
            var writeMe = ('<html><body><head>' + headContent + '</head>');
            writeMe += (print.outerHTML);
            writeMe += ('</body></html>');
            w.document.write(writeMe);
            w.document.close();
            w.focus();
            w.setTimeout(function () {
                w.print();
                w.close();
            }, 1000);
        }

           
            function CreateGridHeader1() {
                var gridName = document.getElementById("GridSQL"); //document.getElementById('GridSQL');
            //get reference to the gridcontrol
            var gridCtl = document.getElementById(gridName);
            alert('hi');
            //create a new table and copy all the attributes of grid to this table
            //store the header cells width in a array
            var headerCellWidths = new Array();
            for (var i = 0; i < gridCtl.getElementsByTagName("th").length; i++) {
                headerCellWidths[i] = gridCtl.getElementsByTagName("th")[i].offsetWidth;
            }

            //create a new table element
            var table = document.createElement("table");

            //copy all attributes from grid to this new table
            for (index = 0; index < gridCtl.attributes.length; index++) {
                if (gridCtl.attributes[index].specified && gridCtl.attributes[index].name != "id") {
                    table.setAttribute(gridCtl.attributes[index].name, gridCtl.attributes[index].value);
                }
            }

            //get the width of the gridview, in pixels.
            //The value contains the width with the padding, scrollBar, and the border,
            //but does not include the margin.
            var gridWidth = gridCtl.offsetWidth;

            //set the width of the grid to the table       
            table.style.width = gridWidth + "px";

            //create the new table body element
            table.appendChild(document.createElement("tbody"));

            //move the header row from the gridview to the body of this new table
            table.getElementsByTagName("tbody")[0].appendChild(gridCtl.getElementsByTagName("tr")[0]);

            //get all the cells from the header row
            var tableCells = table.getElementsByTagName("th");

            //we dont need to set the width for all rows
            //setting it for one row should do
            var gridRow = gridCtl.getElementsByTagName("tr")[0];

            //copy the width of the previously saved header rows to new header row
            // and first row of the grid
            for (var i = 0; i < tableCells.length; i++) {
                tableCells[i].style.width = headerCellWidths[i] + "px";
                gridRow.getElementsByTagName("td")[i].style.width = headerCellWidths[i] + "px";
            }

            //get the parent div of the grid control
            var rootDiv = gridCtl.parentNode;

            //remove the grid control from this div
            rootDiv.removeChild(gridCtl);

            //add the grid to a new div
            var header = document.createElement("div");
            header.appendChild(table);
            rootDiv.appendChild(header);

            //create a new child div and add the grid control to it
            var childDiv = document.createElement("div");
            gridWidth = parseInt(gridWidth) + 20;
            //this would create the scroll
            childDiv.style.cssText = "overflow:auto;height:300px;width:" + gridWidth + "px";
            childDiv.appendChild(gridCtl);

            //add the div to the root div
            rootDiv.appendChild(childDiv);
        }
        //});

        function jsFunction(value) {
            var SqlQry=''
            if ($("#txtQueryBuild").val() == '') {
                SqlQry = 'Select *from wbfcerp.' + value;
                $("#txtQueryBuild").val(SqlQry);
                $("#txtQueryBuild").focus();
                return false;
            }
            else { SqlQry = $("#txtQueryBuild").val() + ',wbfcerp.' + value; $("#txtQueryBuild").val(SqlQry); $("#txtQueryBuild").focus(); return false;}
        }

        function exportToExcel() {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('div[id$=divTableDataHolder]').html()));
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="100%" align="center">
            <tr>
                <td>QUERY BUILDER</td>
            </tr>
        </table>
        </div>
       
    <div >
         <table style= "border: 2px solid #18588a; width:100%;">
            <tr>  
                <td> <span style="font-size:15px;font-weight:bold;color:#59bdcc;"> Table Name :</span> </td>
                <td>
                    <asp:DropDownList ID="DrpTable" runat="server" autocomplete="off" Style="border: 2px solid #18588a;" Height="25px" Width="100%" AutoPostBack="false" 
                        ClientIDMode="Static" DataTextField="name" onchange="jsFunction(this.value);" >
                    </asp:DropDownList>
                </td>  
                                
                <td>
                    <asp:Button ID="btnSQL" autocomplete="off" runat="server" ClientIDMode="Static" OnClick="btnSQL_Click" Height="30px" Width="80px"  Text="Execute" CssClass="save-button" />
                </td>
                <td>
                    <asp:Button ID="btnExcel" autocomplete="off" OnClientClick="exportToExcel()" runat="server" ClientIDMode="Static" Height="30px" Width="80px"  Text="Excel" CssClass="save-button DefaultButton" />
                </td>
                <td>
                    <asp:Button ID="btnPrint" autocomplete="off" ClientIDMode="Static" Width="80px" Text="Print" OnClientClick="CreateGridHeader('GridSQL');" Height="30px" runat="server" CssClass="save-button DefaultButton"></asp:Button>
                </td>
                <td>
                    <asp:Button ID="btnCancel" autocomplete="off" ClientIDMode="Static" Width="80px" OnClick="btnCancel_Click" Text="Refresh" Height="30px" runat="server" CssClass="save-button"></asp:Button>
                </td>
                <td></td>
                <td>
                    <asp:Label ID="lblRec" autocomplete="off" runat="server" ClientIDMode="Static" Text="" Font-Bold="true" Font-Size="15px" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
        </div>
    <div>
        <table style= "border: 2px solid #18588a; width:100%;">
            <%--<tr>
                <asp:TextBox ID="TextBox1" runat="server" Width="100%"></asp:TextBox>
            </tr>--%>

            <tr>
                <td>
                <asp:TextBox ID="txtQueryBuild" autocomplete="off" MaxLength="8000" TextMode="MultiLine" ClientIDMode="Static" Height="200px" runat="server" style= "border: 2px solid #18588a; width:99%;"></asp:TextBox>
            </td>
                    </tr>

        </table>
        </div>
    <div id="divTableDataHolder" runat="server" style="width:1120px; overflow: scroll; height: 500px;">
        <table style="width:100%">
            <tr>
                <td>
                    <asp:GridView ID="GridSQL" autocomplete="off" Style="border: 1px solid #18588a; font-size:12px;" HeaderStyle-BackColor="#18588a" HeaderStyle-ForeColor="white" 
                        runat="server" ClientIDMode="Static" Height="300px" >
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
