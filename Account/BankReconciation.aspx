﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BankReconciation.aspx.cs" Inherits="BankReconciation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        var hdnBankID = '';
        $(document).ready(function () {
            $('#txtBank').focus();
            $('#txtBank').keypress(function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 13) {
                $('#txtDate').focus();
                return false;
            }
            });

            $('#txtDate').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    $('#btnCreate').focus();
                    return false;
                }
            });

        });




        $(document).ready(function () {
            $('input[type=text]').bind('copy paste cut', function (e) {
                e.preventDefault();
            });
            SearchBank();
            $("#rbbank").change(function () {
                if (document.getElementById("rbbank").checked == true) {
                    $("#LblBank").text("Bank Name");
                    $('#txtGL_ID').val('');
                    $('#txtSL_ID').val('');
                    $('#txtOld_SL_Id').val(''); 
                    $('#txtBank').val('');
                    $('#txtBank').focus();
                } 
            })
            $("#rbCode").change(function () {
                if (document.getElementById("rbCode").checked == true) {
                    $("#LblBank").text("Bank Code");
                    $('#txtGL_ID').val('');
                    $('#txtSL_ID').val('');
                    $('#txtOld_SL_Id').val('');
                    $('#txtBank').val('');
                    $('#txtBank').focus();
                }
            })
        });       
        function SearchBank() {            
            
            $(".autosuggestBank").autocomplete({
                source: function (request, response) {
                    var con = 'code';
                    if (document.getElementById("rbCode").checked == true) {
                        con = 'code';
                    }
                    if (document.getElementById("rbbank").checked == true) {
                        con = 'name';
                    }

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "BankReconciation.aspx/BankAutoCompleteData",
                        data: "{'con':'"+ con +"','Bank':'" + document.getElementById('txtBank').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (e, i) {
                    //$("#txtLoaneeIDSearch").val(i.item.val);
                    //$("#hdnBankID").val(i.item.val);
                    hdnBankID = i.item.val;
                    $.ajax({
                        type: "POST",
                        url: "BankReconciation.aspx/BankAutoID",
                        contentType: "application/json;charset=utf-8",
                        data: "{'BankID':'" + hdnBankID + "'}", //$("#hdnBankID").val()
                        dataType: "json",
                        success: function (data) {
                            var strGL_ID = data.d[0].GL_ID; 
                            var strSL_ID = data.d[0].SL_ID;
                            var strOLD_GL_ID = data.d[0].OLD_GL_ID;

                            $('#txtGL_ID').val('');
                            $('#txtSL_ID').val('');
                            $('#txtOld_SL_Id').val('');

                            $('#txtGL_ID').val(strGL_ID);
                            $('#txtSL_ID').val(strSL_ID);
                            $('#txtOld_SL_Id').val(strOLD_GL_ID);
                        },
                        error: function (result) {
                            alert("Error Records Data...");
                            $(".loading-overlay").hide();

                        }
                    });

                }
            });
        };

        $(document).ready(function () {
            $('#txtDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateFormat: 'dd/mm/yy'
            });
            $('#txtBank').keydown(function (evt) {               /// for Backspace
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 8) {
                    hdnBankID = '';//$('#hdnBankID').val('');
                    $('#txtGL_ID').val('');
                    $('#txtSL_ID').val('');
                    $('#txtOld_SL_Id').val('');
                }
            });
            $('#txtBank').keyup(function (evt) {                  /// fro Delete     
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode === 46) {
                    hdnBankID = '';//$('#hdnBankID').val('');
                    $('#txtGL_ID').val('');
                    $('#txtSL_ID').val('');
                    $('#txtOld_SL_Id').val('');
                }
            });
 
            $("#txtBank").change(function () {
                //$(this).select();
                if ($(this).select()) {
                    //$('#hdnBankID').val('');
                    //$('#txtGL_ID').val('');
                    //$('#txtSL_ID').val('');
                    //$('#txtOld_SL_Id').val('');
                    //alert('changed');
                }
            });
        });

        function beforeSave() {
            $("#frmEcom").validate();
            $("#txtBank").rules("add", { required: true, messages: { required: "Select a Bank" } });
            $("#txtDate").rules("add", { required: true, messages: { required: "Select Date" } });

           if (hdnBankID == '') { //$('#hdnBankID').val().trim()
                alert("Select a correct Bank");
                $('#txtBank').val('');
                $('#txtBank').focus();
                return false;
            }
        }

        function PrintOpentab() {
            
            var GL_ID = '';
            var SL_ID = '';
            var Old_SL_ID = '';
            var Date = '';
            var FormName = '';
            var ReportName = '';
            var ReportType = '';
            FormName = "BankReconciation.aspx";
            ReportName = "brs";
            ReportType = "Bank Reconciation Statement";
            GL_ID = $('#txtGL_ID').val();
            SL_ID = $('#txtSL_ID').val();
            Old_SL_ID = $('#txtOld_SL_Id').val();

            

            //var BRSDate = $("#txtDate").val().split("/");
            //Date = BRSDate[2] + "-" + BRSDate[1] + "-" + BRSDate[0];

            var BRSDate = $("#txtDate").val();
            Date = BRSDate;  
            
            if ($('#txtBank').val() == '') {
                alert("Select Bank!");
                $('#txtBank').focus();
                return false;
            }
            if ($("#txtDate").val().trim() == '') {
                alert('Please Select BRS as on');
                $("#txtDate").focus();
                return false;
            }

                var E = '';
                E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "', Date:'" + Date + "', GL_ID:'" + GL_ID + "', SL_ID:'" + SL_ID + "'}";
                $.ajax({
                    type: "POST",
                    url: "BankReconciation.aspx/Report_Paravalue",
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        //jsmsg = JSON.parse(msg.d);
                        //window.open("ReportView.aspx?");
                        var show = response.d;
                        if (show == "OK") {
                            window.open("ReportView.aspx?E=Y");
                        }
                    }
                });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Bank Reconciation Statement</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">

            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                     <td class="labelCaption"></td>
                                     <td class="labelCaption"></td>
                                    <td>
                                        <asp:RadioButton ID="rbCode" autocomplete="off" font-size="13px" Text="Bank Code" runat="server" GroupName="a" Checked="true" Font-Bold="true" class="labelCaption"/>
                                        <asp:RadioButton ID="rbbank" autocomplete="off" font-size="13px" Text="Bank Name" runat="server" GroupName="a" Font-Bold="true" class="labelCaption"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><%--Bank--%>
                                        <asp:Label id="LblBank" runat="server" Text="Bank Code"></asp:Label>
                                    </td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtBank" autocomplete="off" PlaceHolder="---(Select Bank)---" Width="400px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestBank"></asp:TextBox>
                                        <asp:TextBox ID="txtGL_ID" autocomplete="off"  style="display:none" ReadOnly="true" Width="70px" ClientIDMode="Static" runat="server" CssClass="inputbox2" ></asp:TextBox>
                                        <asp:TextBox ID="txtSL_ID" autocomplete="off" style="display:none" ReadOnly="true" Width="70px" ClientIDMode="Static" runat="server" CssClass="inputbox2" ></asp:TextBox>
                                        <asp:TextBox ID="txtOld_SL_Id" autocomplete="off" style="display:none" ReadOnly="true" Width="70px" ClientIDMode="Static" runat="server" CssClass="inputbox2" ></asp:TextBox>
                                        
                                    </td>
                                    <td class="labelCaption">BRS as on</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtDate" autocomplete="off" Width="100px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnCreate" runat="server" Text="Print" CommandName="create" autocomplete="off"
                                            Width="90px" Height="40px" CssClass="save-button DefaultButton" OnClientClick="PrintOpentab(); return false;" />
                                        <asp:Button ID="cmdRefreshSearch" runat="server" Text="Refresh" autocomplete="off"
                                            Width="90px" Height="40px" CssClass="save-button DefaultButton" OnClick="cmdRefreshSearch_Click" OnClientClick='javascript: return unvalidate()' />
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>
