﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Acc_HC_Master.aspx.cs" Inherits="Acc_HC_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div class="loading-overlay">
        <div class="loadwrapper">
            <div class="ajax-loader-outer">Loading...</div>
        </div>
    </div>
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Health Code Parameter</td>
                 <asp:TextBox ID="hdnGroupID" Width="100" ClientIDMode="Static" autocomplete="off" runat="server" style="display:none"></asp:TextBox>
            </tr>
        </table>
        <br />
        <table width="100%" cellpadding="1" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="1" AutoGenerateRows="False" ClientIDMode="Static" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="1" width="100%">
                                 <tr>
                                    <td class="labelCaption" width="150">Parameter Description &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left" width="650">
                                        <asp:TextBox ID="txtPdesc"  Width="600" MaxLength="200" ClientIDMode="Static" runat="server" autocomplete="off" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption" width="50">Value &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPvalue" Width="80" MaxLength="10" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                 </tr>
                             </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>  
                            <table align="center" cellpadding="1" width="100%">
                                 
                                 <tr>
                                    <td class="labelCaption" width="150">Parameter Description &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left" width="650">
                                        <asp:TextBox ID="txtPdesc" MaxLength="200" Width="600" ClientIDMode="Static" autocomplete="off" Text='<%# Eval("Pdesc")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption" width="50">Value &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:TextBox ID="txtPvalue" Width="80" MaxLength="10" ClientIDMode="Static" autocomplete="off" Text='<%# Eval("Pvalue")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                 </tr>
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="1" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" ClientIDMode="Static" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" ClientIDMode="Static" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="auto-style1">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <div style="overflow-y: scroll;  width: 100%">
                        <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="Psl" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName='Select' ImageUrl="images/Edit.jpg" Enabled="true" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ClientIDMode="Static" autocomplete="off" ID="btnEdit" CommandArgument='<%# Eval("Psl") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton CommandName='Del' ImageUrl="images/Delete.gif" Enabled="true" runat="server" ID="btnDelete" 
                                   ClientIDMode="Static" autocomplete="off" OnClientClick='<%#"return Delete("+(Eval("Psl")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("Psl") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                                
                                <asp:BoundField DataField="Psl" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Parameter Sl. No." />    
                                <asp:BoundField DataField="Pdesc" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Parameter Description" />
                                <asp:BoundField DataField="Pvalue" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Parameter Value" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

