﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="RecoveryReport.aspx.cs" Inherits="RecoveryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<script type="text/javascript" src="js/SanctionReport.js"></script>--%>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert('Only Numeric Value Accept!');
                return false;
            }
            return true;
        }

        $(document).ready(function () {
            $("#txtFrmDate").focus();
            //$("#ddlReportName").val('RR');

            $("#ddlReportName").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#txtFrmDate").focus();
                    return false;
                }
            });

            $("#txtFrmDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#txtToDate").focus();
                    return false;
                }
            });

            $("#txtToDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#btnGenerateReport").focus();
                    return false;
                }
            });

            $("#btnGenerateReport").click(function (event) {
                if ($("#ddlReportName").val() == "0") {
                    alert("Select a Report Name!");
                    $("#ddlReportName").focus();
                    return false;
                }
                if ($("#txtFrmDate").val() == "") {
                    alert("Please Enter Recovery From Year&Month!");
                    $("#txtFrmDate").focus();
                    return false;
                }
                if ($("#txtToDate").val() == "") {
                    alert("Please Enter Recovery To Year&Month!");
                    $("#txtToDate").focus();
                    return false;
                }
                if ($("#txtFrmDate").val().length < 6) {
                    alert('Please Enter Valid Recovery From Year&Month!');
                    $("#txtFrmDate").focus();
                    return false;
                }
                if ($("#txtToDate").val().length < 6) {
                    alert('Please Enter Valid Recovery To Year&Month!');
                    $("#txtFrmDate").focus();
                    return false;
                }
                if ($("#txtFrmDate").val() > $("#txtToDate").val()) {
                    alert('Recovery From Year&Month Shoulb be Less Than Or Qual To Recovery To Year&Month!');
                    $("#txtFrmDate").focus();
                    return false;
                }
               
                ShowReport();
                return false;
            });

            function ShowReport() {
                var FormName = '';
                var ReportName = '';
                var ReportType = '';
                var RecFromDate = '';
                var RecToDate = '';
                FormName = "RecoveryReport.aspx";
                RecFromDate = $('#txtFrmDate').val();
                RecToDate = $('#txtToDate').val();
                var RptType = $("#DropDownList1").val();

                if (RptType == 'D') {
                    ReportName = "LoaneeRecovery";
                    ReportType = "District Wise Recovery Report";
                }
                else if (RptType == 'i') {
                    ReportName = "LoaneeRecovery";
                    ReportType = "Industry Wise Recovery Report";
                }
                else if (RptType == 'L') {
                    ReportName = "LoaneeRecovery";
                    ReportType = "Loanee Wise Recovery Report";
                }
                else if (RptType == 'S') {
                    ReportName = "LoaneeRecovery";
                    ReportType = "Sector Wise Recovery Report";
                }
                else if (RptType == 'H') {
                    ReportName = "HCRecovery";
                    ReportType = "Health Wise Recovery Report";
                }
                else if (RptType == 'M') {
                    ReportName = "MonthRecovery";
                    ReportType = "Month Wise Recovery Report";
                }
                
                var E = '';
                E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  RecFromDate:'" + RecFromDate + "',  RecToDate:'" + RecToDate + "',RptType:'" + RptType +"'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/SetReportValue',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var show = response.d;
                        if (show == "OK") {
                            window.open("ReportView.aspx?E=Y");
                        }

                    },
                    error: function (response) {
                        var responseText;
                        responseText = JSON.parse(response.responseText);
                        alert("Error : " + responseText.Message);
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            
        });
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="100%" align="center">
            <tr>
                <td>RECOVERY REPORT</td>
            </tr>
        </table>
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            

            <tr>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">Recovery From Year&Month<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="text-align:left">
                    <asp:TextBox ID="txtFrmDate" autocomplete="off" ReadOnly="false" MaxLength="6" placeholder="YYYYYMM" Width="190px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                </td>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">Recovery To Year&Month<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>
                    <asp:TextBox ID="txtToDate" autocomplete="off" ReadOnly="false" MaxLength="6" placeholder="YYYYYMM" Width="130px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" style="font-size: 15px; color: navy; font-weight: bold;">Report Name<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="width: 50px;">
                    <asp:DropDownList ID="ddlReportName" autocomplete="off" runat="server" Width="200px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                        <%--<asp:ListItem Value="0">(Select Report Name)</asp:ListItem>--%>
                        <asp:ListItem Value="RR">Recovery Report</asp:ListItem>
                    </asp:DropDownList>
                </td>
               <td align="right" style="font-size: 15px; color: navy; font-weight: bold;">Report Type<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="width: 50px;text-align:center;">
                    <asp:DropDownList ID="DropDownList1" autocomplete="off" runat="server" Width="140px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                        
                        <asp:ListItem Value="D">District Wise</asp:ListItem>
                        <asp:ListItem Value="i">Industry Wise</asp:ListItem>
                         <asp:ListItem Value="L">Loanee Wise</asp:ListItem>
                        <asp:ListItem Value="H">Health Code Wise</asp:ListItem>
                        <asp:ListItem Value="M">Month Wise</asp:ListItem>
                        <asp:ListItem Value="S">Sector Wise</asp:ListItem>
                    </asp:DropDownList>

                </td>
                <td style="width:20%"></td>
            </tr>   
           
        </table>

       <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <%--<tr style="width: 100%">
                <td></td>
                <td></td>
            </tr>   --%> 
           
             <tr style="width: 100%;text-align:center;">
                 <td style="width:40%;text-align:left;"></td>
                <td >
                    <asp:Button ID="btnGenerateReport" autocomplete="off" runat="server" Text="Report" Width="100px" CssClass="save-button DefaultButton" />
                    <asp:Button ID="btnCancel" autocomplete="off" runat="server" Text="Cancel " Width="100px" CssClass="save-button" OnClick="btnCancel_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
