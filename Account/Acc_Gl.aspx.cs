﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Data.SqlClient;

public partial class Acc_Gl : System.Web.UI.Page
{
    static DataTable dtAccGl;
    static int AccGlID;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            if (!IsPostBack)
            {
                PopulateGrid();
                PopulateGrid1();
                PopulateGridgrdAccGLDtl();
                PopulateGridSearchGLCode();
                MaxYearMonth();
                
                HttpContext.Current.Session["AccGlDetail"] = null;
                dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];
            }
             CheckBox chkUnitOnly = (CheckBox)dv.FindControl("chkUnitOnly");           
               
        }
        catch (Exception ex) 
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void MaxYearMonth()
    {
        TextBox hdnMaxYearMonth = (TextBox)dv.FindControl("hdnMaxYearMonth");
        TextBox txtGrpCode = (TextBox)dv.FindControl("txtGrpCode");

        int a = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        string b = HttpContext.Current.Session["Menu_For"].ToString();
        DataTable dtLoaneeData = DBHandler.GetResult("GET_MaxYearMonth_Pf", 
            Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]), HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
           hdnMaxYearMonth.Text= dtRow["YEAR_MONTH"].ToString();
           txtGrpCode.Focus();
        }
    }

    [WebMethod]
    public static string[] AccGlAutoCompleteData(string OLD_GL_ID)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_AccGl_Pf", OLD_GL_ID, 
            Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]), HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["GL_NAME1"].ToString(), dtRow["GL_ID"].ToString()));
        }
        return result.ToArray();
    }


    [WebMethod]
    public static GroupCode[] GET_GroupNameByGrpID(string StrGroupCode)
    {
        //int UserID;
        List<GroupCode> Detail = new List<GroupCode>();
        //UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_GroupNameByGroupID_Pf", StrGroupCode, HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GroupCode DataObj = new GroupCode();
            DataObj.GroupID = dtRow["GroupID"].ToString();
            DataObj.OldGroupID = dtRow["OldGroupID"].ToString();
            DataObj.GroupName  = dtRow["GroupName"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GroupCode //Class for binding data
    {
        public string GroupID { get; set; }
        public string OldGroupID { get; set; }
        public string GroupName { get; set; }
    }

    [WebMethod]
    public static GroupName1[] GET_GroupName()
    {
        List<GroupName1> Detail = new List<GroupName1>();
        DataTable dtGetData = DBHandler.GetResult("Get_GroupNameAll_Pf", HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GroupName1 DataObj = new GroupName1();
           // DataObj.GroupID = dtRow["GroupID"].ToString();
            DataObj.OldGroupID = dtRow["OldGroupID"].ToString();
            DataObj.GroupName = dtRow["GroupName"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GroupName1 //Class for binding data
    {
        //public string GroupID { get; set; }
        public string OldGroupID { get; set; }
        public string GroupName { get; set; }
    }

    [WebMethod]
    public static CheckGlCode[] Chk_GLCode(string StrGLCode, string StrGLID)
    {
        //int UserID;
        List<CheckGlCode> Detail = new List<CheckGlCode>();
        //UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("CkhGlCode_Pf", StrGLCode, StrGLID, HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            CheckGlCode DataObj = new CheckGlCode();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class CheckGlCode //Class for binding data
    {
        public string OLD_GL_ID { get; set; }
    }


    [WebMethod]
    public static CheckGlName[] Chk_GLName(string StrGLName, string StrGLID)
    {
        List<CheckGlName> Detail = new List<CheckGlName>();
        DataTable dtGetData = DBHandler.GetResult("CkhGlName_Pf", StrGLName, StrGLID, HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            CheckGlName DataObj = new CheckGlName();
            DataObj.GlName = dtRow["GL_NAME"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class CheckGlName //Class for binding data
    {
        public string GlName { get; set; }
    }  

    [WebMethod]
    public static String[] GET_DelGLRecord(string GL_ID)
    {
        int SectorID;
        List<string> Detail = new List<string>();
        
        //UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtGetData = DBHandler.GetResult("Delete_GLRecords_Pf", GL_ID, SectorID);
        Detail.Add("ok");

        return Detail.ToArray();
    }
       
    [WebMethod]
    public static string[] GET_GlNoOnVoucher(string GL_ID)
    {
        //int UserID;
        List<string> Detail = new List<string>();
        //UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("CkhGlCodeOnVoucher_Pf", GL_ID);
        if (dtGetData.Rows.Count > 0)
        {
            Detail.Add("Can Not Delete Record!This GL Code Already Used On Voucher");
        }
        else {
            DataTable dtGetSL = DBHandler.GetResult("Get_GLIDCheckOnSL_Pf", GL_ID);
            if (dtGetSL.Rows.Count > 0)
            {
                Detail.Add("Can Not Delete Record!This GL Code Already Used On SL Master");
            }
        
        }
        return Detail.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_AccGlDetailsOnSession(string YearMonth, string OpenDebit, string OpenCredit, string TotalDebit, string TotalCredit)
    {
        string JSONVal = "";
        try
        {
            
            if (HttpContext.Current.Session["AccGlDetail"] != null)
            {
                DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                if (dtdetail.Rows.Count > 0)
                {
                    DataRow dtrow = dtdetail.NewRow();
                    dtrow["YearMonth"] = YearMonth;
                    dtrow["OpenDebit"] = OpenDebit;
                    dtrow["OpenCredit"] = OpenCredit;
                    dtrow["TotalDebit"] = TotalDebit;
                    dtrow["TotalCredit"] = TotalCredit;
                    dtdetail.Rows.Add(dtrow);
                    HttpContext.Current.Session["AccGlDetail"] = dtdetail;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("YearMonth");
                dt.Columns.Add("OpenDebit");
                dt.Columns.Add("OpenCredit");
                dt.Columns.Add("TotalDebit");
                dt.Columns.Add("TotalCredit");

                DataRow dtrow = dt.NewRow();
                dtrow["YearMonth"] = YearMonth;
                dtrow["OpenDebit"] = OpenDebit;
                dtrow["OpenCredit"] = OpenCredit;
                dtrow["TotalDebit"] = TotalDebit;
                dtrow["TotalCredit"] = TotalCredit;

                dt.Rows.Add(dtrow);
                HttpContext.Current.Session["AccGlDetail"] = dt;
            }

            string result = "success";
            JSONVal = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_AccGlRecord(string GlID, string Group_ID, string OldGlID, string OldSlID, string GlName, string GlMaster, string GlType, string Schedule, string chkSectorID)
    {
        int UserID, SectorID;

        UserID   = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        string JSonVal = "";
        string ConString = "";
        string GL_ID = "";
   //     ConString = DataAccess.DBHandler.GetConnectionString();   
        try
        {
            if (GlID == "")
            {
                SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlTransaction transaction;
                    SqlDataReader reader;
                    db.Open();
                    transaction = db.BeginTransaction();
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "Insert_AccGl_Pf";

                        cmd.Parameters.Add("@GROUP_ID", SqlDbType.VarChar).Value = Group_ID;
                        cmd.Parameters.Add("@OLD_GL_ID", SqlDbType.VarChar).Value = OldGlID;
                        cmd.Parameters.Add("@OLD_SL_ID", SqlDbType.VarChar).Value = OldSlID;
                        cmd.Parameters.Add("@GL_NAME", SqlDbType.VarChar).Value = GlName;
                        cmd.Parameters.Add("@GL_MASTER", SqlDbType.VarChar).Value = GlMaster;
                        cmd.Parameters.Add("@GL_TYPE", SqlDbType.VarChar).Value = GlType;
                        cmd.Parameters.Add("@SCHEDULE", SqlDbType.VarChar).Value = Schedule;
                        cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = chkSectorID.Equals("N") ? DBNull.Value : (object)SectorID;
                        cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = UserID;

                        cmd.Connection = db;
                        cmd.Transaction = transaction;

                        reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            GL_ID = reader["GL_ID"].ToString();
                        }
                        reader.Close();

                        DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                        if (dtdetail != null)           /// if there dont have any data into detail.
                        {
                            if (dtdetail.Rows.Count > 0)
                            {
                                foreach (DataRow objDR in dtdetail.Rows)
                                {
                                    cmd = new SqlCommand();
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.CommandText = "Insert_AccGlOpning_Pf";

                                    cmd.Parameters.Add("@GL_ID", SqlDbType.VarChar).Value = GL_ID;
                                    cmd.Parameters.Add("@YEAR_MONTH", SqlDbType.VarChar).Value = objDR["YearMonth"];
                                    cmd.Parameters.Add("@OpenDEBIT", SqlDbType.Money).Value = Convert.ToDecimal(objDR["OpenDebit"]);
                                    cmd.Parameters.Add("@OpenCREDIT", SqlDbType.Money).Value = Convert.ToDecimal(objDR["OpenCredit"]);
                                    cmd.Parameters.Add("@DEBIT", SqlDbType.Money).Value = Convert.ToDecimal(objDR["TotalDebit"]);
                                    cmd.Parameters.Add("@CREDIT", SqlDbType.Money).Value = Convert.ToDecimal(objDR["TotalCredit"]);
                                    cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(SectorID);
                                    cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(UserID);

                                    cmd.Connection = db;
                                    cmd.Transaction = transaction;
                                    cmd.ExecuteNonQuery();
                                }                               
                            }
                        }
                        transaction.Commit();
                        HttpContext.Current.Session["AccGlDetail"] = null;
                        dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                        db.Close();
                    }
                    catch (SqlException sqlError)
                    {
                        transaction.Rollback();
                        db.Close();                       
                    }
                }
                catch (Exception ex)
                {
                    db.Close();             
                }
                               
                //DataTable dtResult = DBHandler.GetResult("Insert_AccGl",  Group_ID, OldGlID, OldSlID, GlName, GlMaster, GlType, Schedule, chkSectorID.Equals("N") ? DBNull.Value : (object)SectorID, UserID);
                //if (dtResult.Rows.Count > 0)
                //{
                //    GL_ID = dtResult.Rows[0]["GL_ID"].ToString();
                //}

                //DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                //if (dtdetail.Rows.Count > 0)
                //{
                //    foreach (DataRow objDR in dtdetail.Rows)
                //    {
                //        DBHandler.Execute("Insert_AccGlOpning", GL_ID, objDR["YearMonth"], Convert.ToDecimal(objDR["OpenDebit"]), Convert.ToDecimal(objDR["OpenCredit"]), Convert.ToDecimal(objDR["TotalDebit"]), Convert.ToDecimal(objDR["TotalCredit"]), SectorID, UserID);
                //    }
                //}
                //HttpContext.Current.Session["AccGlDetail"] = null;
                //dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];
            }
            else
            {
                DBHandler.Execute("Update_AccGl_Pf", Group_ID,  GlID,  OldGlID, OldSlID, GlName, GlMaster, GlType, Schedule, chkSectorID.Equals("N") ? DBNull.Value : (object)SectorID, UserID);

                //DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                //if (dtdetail.Rows.Count > 0)
                //{
                //    foreach (DataRow objDR in dtdetail.Rows)
                //    {   
                       
                //        DBHandler.Execute("Delete_AccGlOpning", GlID, objDR["YearMonth"],SectorID);

                //            DBHandler.Execute("Insert_AccGlOpning", GlID, objDR["YearMonth"], Convert.ToDecimal(objDR["OpenDebit"]), Convert.ToDecimal(objDR["OpenCredit"]), Convert.ToDecimal(objDR["TotalDebit"]), Convert.ToDecimal(objDR["TotalCredit"]), SectorID, UserID);

                //        //cmd = new SqlCommand("Exec Save_IssueDetail " + Convert.ToInt32(IntIssueId) + ",'" + IssueNo + "','" + IssueDate + "'," + Convert.ToInt16(SrNo) + "," + Convert.ToInt16(ReqID) + "," + Convert.ToInt32(objDR["FolioID"]) + ",'" + Convert.ToDecimal(objDR["IssuedQuantity"]) + "'," + Convert.ToInt16(UNIT_COMBO_CODE) + ", " + Convert.ToInt16(UserID) + "", db, transaction);
                //        //cmd.ExecuteNonQuery();
                //        //SrNo++;
                //    }
                //}
                HttpContext.Current.Session["AccGlDetail"] = null;
                dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];

            }
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
     

        return JSonVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DeleteSessionAccGl(string ValYearMonth)
    {
        StringBuilder objSB = new StringBuilder();
        string JSONVal = string.Empty;
        string strYearMon = "";
        DataTable dtDetails = (DataTable)HttpContext.Current.Session["AccGlDetail"];
        if (HttpContext.Current.Session["AccGlDetail"] != null)
        {
            strYearMon = ValYearMonth;
            try
            {
                if (dtDetails.Rows.Count == 1)
                {
                    HttpContext.Current.Session["AccGlDetail"] = null;
                    dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                }
                else
                {
                    dtDetails = dtDetails.AsEnumerable().Where(x => x.Field<String>("FolioID") != strYearMon).CopyToDataTable();
                    HttpContext.Current.Session["AccGlDetail"] = dtDetails;
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session["AccGlDetail"] = null;
            }
        }
        return JSONVal;
    }


    [WebMethod]
    public static ViewAccGL[] View_AccGL(string GL_ID)
    {
        List<ViewAccGL> Detail = new List<ViewAccGL>();
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtGetData = DBHandler.GetResult("View_AccGL_Pf", GL_ID, SectorID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ViewAccGL DataObj = new ViewAccGL();
            DataObj.GL_ID = dtRow["GL_ID"].ToString();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.group_name = dtRow["group_name"].ToString();
            DataObj.group_code = dtRow["group_code"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            DataObj.GL_MASTER = dtRow["GL_MASTER"].ToString();
            DataObj.GL_TYPE = dtRow["GL_TYPE"].ToString();
            DataObj.SCHEDULE = dtRow["SCHEDULE"].ToString();
            DataObj.SectorID = dtRow["SectorID"].ToString();
            DataObj.GROUP_ID = dtRow["GROUP_ID"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class ViewAccGL //Class for binding data
    {
        public string GL_ID { get; set; }
        public string OLD_GL_ID { get; set; }
        public string group_name { get; set; }
        public string group_code { get; set; }
        public string GL_NAME { get; set; }
        public string GL_MASTER { get; set; }
        public string GL_TYPE { get; set; }
        public string SCHEDULE { get; set; }
        public string SectorID { get; set; }
        public string GROUP_ID { get; set; }
    }

    [WebMethod]
    public static ViewAccGLDetail[] View_AccGLOpening(string GL_ID)
    {
        List<ViewAccGLDetail> Detail = new List<ViewAccGLDetail>();
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtGetData = DBHandler.GetResult("View_AccGLDetail_Pf", GL_ID, SectorID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ViewAccGLDetail DataObj = new ViewAccGLDetail();
            DataObj.YEAR_MONTH = dtRow["YEAR_MONTH"].ToString();
            DataObj.GL_ID = dtRow["GL_ID"].ToString();
            DataObj.OPBAL = dtRow["OPBAL"].ToString();
            DataObj.CLBAL = dtRow["CLBAL"].ToString();
            DataObj.DEBIT = dtRow["DEBIT"].ToString();
            DataObj.CREDIT = dtRow["CREDIT"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class ViewAccGLDetail //Class for binding data
    {
        public string YEAR_MONTH { get; set; }
        public string GL_ID { get; set; }
        public string OPBAL { get; set; }
        public string CLBAL { get; set; }
        public string DEBIT { get; set; }
        public string CREDIT { get; set; }
    }
   

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {

            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            //////DataTable dt = DBHandler.GetResult("Get_LMN_LoanType");
            //////tbl.DataSource = dt;
            //////tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGridSearchGLCode()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("GL_ID");
            dt.Columns.Add("OLD_GL_ID");
            dt.Columns.Add("GL_NAME");
            dt.Rows.Add();
            grdSearchGlCode.DataSource = dt;
            grdSearchGlCode.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGrid1()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("OldGroupID");
            dt.Columns.Add("GroupName");
            dt.Rows.Add();
            grdGroup.DataSource = dt;
            grdGroup.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGridgrdAccGLDtl()
    {
        try
        {
            GridView grdAccGLDtl = (GridView)dv.FindControl("grdAccGLDtl");
            DataTable dt = new DataTable();
            dt.Columns.Add("ItemID");
            dt.Columns.Add("YearMonth");
            dt.Columns.Add("OpeningDebit");
            dt.Columns.Add("OpeningCredit");
            dt.Columns.Add("TotalDebit");
            dt.Columns.Add("TotalCredit");
            dt.Columns.Add("ClosingDebit");
            dt.Columns.Add("ClosingCredit");
            dt.Rows.Add();
            grdAccGLDtl.DataSource = dt;
            grdAccGLDtl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DeleteSessionGLDetail(int YearMonth)
    {
        StringBuilder objSB = new StringBuilder();
        string JSONVal = string.Empty;
        DataTable dtDetails = (DataTable)HttpContext.Current.Session["AccGlDetail"];
        if (HttpContext.Current.Session["AccGlDetail"] != null)
        {
            try
            {
                if (dtDetails.Rows.Count == 1)
                {
                    HttpContext.Current.Session["AccGlDetail"] = null;
                    dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                }
                else
                {
                    dtDetails = dtDetails.AsEnumerable().Where(x => x.Field<string>("YearMonth") != Convert.ToString(YearMonth)).CopyToDataTable();
                    HttpContext.Current.Session["AccGlDetail"] = dtDetails;
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session["AccGlDetail"] = null;
            }
        }
        return JSONVal;
    }












}