﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

public partial class QueryBuilder : System.Web.UI.Page
{
    static string StrFormula = "";
    static string ConvertDate;
    static string ChkSql = "select isnull(AdminFlag,0) as AdminFlag from inventory.MST_User where userid=";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }

            if (!IsPostBack)
            {
                
                GetTable();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    public void GetTable()
    {
        DrpTable.Items.Clear();
        DataTable SQLQuery = DBHandler.GetResult("Get_ShowAllTable");
        DrpTable.DataSource = SQLQuery;
        DrpTable.DataBind();

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void btnSQL_Click(object sender, EventArgs e)
    {
        int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        string SQLRunchk = "";
        if (txtQueryBuild.Text == "")
        {
            Response.Write("<script>alert('Please Type Query For Result.....!')</script>");
            txtQueryBuild.Focus();
        }
        else
        {
            string SqlQuery = txtQueryBuild.Text;
            SQLRunchk = ChkSql + Convert.ToString(UserID);
            string MatchQry = Regex.Match(SqlQuery.ToString().ToUpper(), "INSERT|UPDATE|DELETE|TRUNCATE|DROP").Value;
            if (MatchQry == "INSERT" || MatchQry == "UPDATE" || MatchQry == "DELETE" || MatchQry == "TRUNCATE" || MatchQry == "DROP")
            {
                Response.Write("<script>alert('Dont Userd INSERT Or UPDATE Or DELETE Or TRUNCATE Or DROP Query .....! Only SELECT Query Used')</script>");
                txtQueryBuild.Focus();
                //DataTable SQLChk = DBHandler.GetResult("Get_SQLQueryBuild", SQLRunchk);
                //if (SQLChk.Rows.Count > 0)
                //{
                //    if (Convert.ToString(SQLChk.Rows[0]["AdminFlag"]) == "0")
                //    {
                //        Response.Write("<script>alert('Dont Userd INSERT Or UPDATE Or DELETE Or TRUNCATE Or DROP Query .....! Only SELECT Query Used')</script>");
                //        txtQueryBuild.Focus();
                //    }
                //    else
                //    {
                //        DataTable SQLQuery = DBHandler.GetResult("Get_SQLQueryBuild", SqlQuery);
                //        GridSQL.DataSource = SQLQuery;
                //        GridSQL.DataBind();
                //        lblRec.Text = "Total No. Of Record's :   " + SQLQuery.Rows.Count.ToString();
                //        GetTable();

                //    }
                //}
            }

            else
            {
                DataTable SQLQuery = DBHandler.GetResult("Get_SQLQueryBuild", SqlQuery);
                GridSQL.DataSource = SQLQuery;
                GridSQL.DataBind();
                lblRec.Text = "Total No. Of Record's :   " + SQLQuery.Rows.Count.ToString();
                GetTable();
            }
        }
    }

    [WebMethod(EnableSession = true)]    
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string RecFromDate, string RecToDate)
    {
        string JSONVal = "";       
        try    
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            int UserID;
            int SectorID;           
            string StrPaperSize = "";       
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            StrFormula = "{DAT_Acc_Loan.YEAR_MONTH}>='" + RecFromDate + "' and {DAT_Acc_Loan.YEAR_MONTH}<='" + RecToDate + "' and {MST_Sector.SectorID}=" + SectorID + "";                
            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, RecFromDate, RecToDate, SectorID, "", "", "", "", "", "", "");
            
            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    
}