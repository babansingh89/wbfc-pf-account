﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_Category.aspx.cs" Inherits="LMN_Category" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
    $(document).ready(function () {

    });

    function beforeSave() {
        $("#frmEcom").validate();
        $("#txtCatagoryid").rules("add", { required: true, messages: { required: "Please enter Category id" } });
        $("#txt_categoryname").rules("add", { required: true, messages: { required: "Please enter Category Type" } });
        $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please select Unit Name" } });
       
        
    }

    function Delete(id) {
        if (confirm("Are You sure you want to delete?")) {
            $("#frmEcom").validate().currentForm = '';
            return true;
        } else {
            return false;
        }
    }
    //$("#btndelete").click(function (event) {
    //    if (confirm("Do you want to Cancel Challan Record?")) {
    //        confirm_value.value = "Yes";

    //    } else {
    //        confirm_value.value = "No";
    //    }

    //    if (confirm_value.value == "No") {
    //        return false;
    //    }
    //});

    function unvalidate() {
        $("#frmEcom").validate().currentForm = '';
        return true;
    }
</script>

   <script type="text/javascript">
        //Function to allow only numbers to textbox
        //function validate(key) {
        //    //getting key code of pressed key
        //    var keycode = (key.which) ? key.which : key.keyCode;
        //    var phn = document.getElementById('txtCatagoryid');
        //    //comparing pressed keycodes
        //    if (!(keycode == 8 || keycode == 46) && (keycode < 48 || keycode > 57)) {
        //        return false;
        //    }
        //    else {
        //        //Condition to check textbox contains ten numbers or not
        //        if (phn.value.length < 1) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
    
        $(document).ready(function () {
            $("#txtCatagoryid").keypress(function (event) {
                var inputValue = event.which;
                // allow letters and whitespaces only.
                if ((inputValue > 47 && inputValue < 58) && (inputValue != 32)) {
                    alert("Please Enter a Character");
                    event.preventDefault();
                }
            });
        });
        $(document).ready(function () {
            $("#txtCatagoryid").keypress(function (event) {
                var inputValue = event.which;
                // allow letters and whitespaces only.
                if ((inputValue > 47 && inputValue < 58) && (inputValue != 32)) {
                    alert("Please Enter a Character");
                    event.preventDefault();
                }
            });
        });
        
    </script>
    <style type="text/css">
        .auto-style1 {
            font-family: Segoe UI;
            font-size: 12px;
            color: Navy;
            text-align: left;
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>CATEGORY TYPE MASTER</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Category ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCatagoryid" autocomplete="off" MaxLength="1"  ClientIDMode="Static" runat="server"  CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                                 <tr>
                                    <td class="labelCaption">Category Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txt_categoryname" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                               
                            </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>                                     
                                    <td class="labelCaption">Category ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCatagoryid" autocomplete="off" MaxLength="1" ReadOnly="true" ClientIDMode="Static" Text='<%# Eval("CategoryID") %>' 
                                            runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>                                     
                                    <td class="labelCaption">Category Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txt_categoryname" autocomplete="off" MaxLength="100" ClientIDMode="Static" Text='<%# Eval("CategoryName") %>' 
                                            runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                    
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="auto-style1">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <div style="overflow-y: scroll;  width: 100%">

                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="CategoryID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false" >
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("CategoryID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("CategoryID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("CategoryID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>      
                                 <asp:BoundField DataField="CategoryID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Category ID" />                          
                                <asp:BoundField DataField="CategoryName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Category Name" />                
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            </table>
    </div>
</asp:Content>