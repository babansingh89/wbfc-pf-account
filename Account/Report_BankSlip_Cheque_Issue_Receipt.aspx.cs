﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;
using System.Globalization;
using System.Data.SqlTypes;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;

public partial class Report_BankSlip_Cheque_Issue_Receipt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Print_ReportTypeWise(string FormName, string ReportName, string ReportType, string StartDate, string EndDate, string Bank, string StratVoucharNo, string EndVoucharNo)
    {
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
        string JSONVal = "";
        String Msg = "";
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        int SectorId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        string sc = "1";
        DateTime StartDate1 = DateTime.ParseExact(StartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        DateTime EndDate1 = DateTime.ParseExact(EndDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //string Startdate = StartDate1.ToString("yyyy/MM/dd");
        //string Enddate = EndDate1.ToString("yyyy/MM/dd");
       // string Startdate = StartDate1.ToString("yyyy-MM-dd");
       // string Enddate = EndDate1.ToString("yyyy-MM-dd");
        string StrFormula = "";

        JavaScriptSerializer jscript=new JavaScriptSerializer();
        if (ReportType == "Bank Slip")
        { 
            
           int EndVoucharNo1 = Convert.ToInt32(EndVoucharNo.ToString());
            int StratVoucharNo1 = Convert.ToInt32(StratVoucharNo.ToString());
            //object bankSelect = Bank.Trim() == "" ? DBNull.Value : Bank as object;
            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, StartDate, EndDate, StratVoucharNo, EndVoucharNo, Bank, SectorId, HttpContext.Current.Session["Menu_For"].ToString(), "", "", "");
             //jscript = new JavaScriptSerializer();
            JSONVal = "OK";
            
        }
        if (ReportType == "Cheque Issue")
        {

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue",UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, StartDate, EndDate, SectorId, HttpContext.Current.Session["Menu_For"].ToString(), "", "", "", "", "", "");
             //jscript = new JavaScriptSerializer();
            JSONVal = "OK";
            
        }
        if (ReportType == "Cheque Receipt")
        {
            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, StartDate, EndDate, SectorId, HttpContext.Current.Session["Menu_For"].ToString(), "", "", "", "", "", "");
             //jscript = new JavaScriptSerializer();
            JSONVal = "OK";
            
        }
        //return jscript.Serialize(Msg);
        return JSONVal;
    }
    [WebMethod]
    public static string[] BankAutoCompleteData(string Bank,string con)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_Bank_Report_Pf", Bank, 
            Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]), con,
            HttpContext.Current.Session["Menu_For"].ToString(), con);
        result.Add("All|All");
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["GL_NAME1"].ToString(), dtRow["GL_ID"].ToString()));
        }
        return result.ToArray();
    }
    protected void cmdReset_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}