﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LoanInterestReport.aspx.cs" Inherits="LoanInterestReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/LoanInterestReport.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#txtYearmonth').attr({ maxLength: 6 });
            $('#txtYearmonth').keypress(function (evt) {

                if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                    alert("Please Enter a Numeric Value");
                    return false;
                }

            });
        });

        function beforeSave() {
            $("#frmEcom").validate();
            $("#txtYearmonth").rules("add", { required: true, messages: { required: "Please enter Year & Month" } });
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }

        function PrintDiv() {
            var divToPrint = $('#divLoanInt').html().trim();

            if (divToPrint != "") {
                var popupWin = window.open('', '_blank', '');
                popupWin.document.open();
                popupWin.document.write('<html><head><title>Print</title>');
                popupWin.document.write("<link href='css/wbfdc-style1.css' rel='stylesheet' type='text/css' />");
                popupWin.document.write('</head><body onload="window.print()"><center>' + divToPrint + '</center></body></html>');
                popupWin.document.close();
            } else {
                alert('No Data To Print');
            }
        }

        $(document).ready(function () {
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Loan Interest Report</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td colspan="6">
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td style="height: 15px;" align="left">Loan Interest Report</td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">

                                <tr>  
                                    <td>
                                        <div id="searchYM" style="width: 100%;">
                                            <table align="center" width="100%">
                                                <tr >
                                                    <td class="labelCaption">Year & Month(yyyymm) <span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtYearmonth" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                   <td>
                                                     <input type='radio' name='chk' autocomplete="off" runat="server" clientidmode="Static" id='RDStanderd'/><font size="3px" color="navy"><b> Standerd </b></font> <%--checked="checked"--%>
                                                        <input type='radio' autocomplete="off" runat="server" clientidmode="Static" name='chk' id='RDNPA' /><font size="3px" color="navy"><b> NPA </b></font>
                                                       </td>
                                                    <td style="width:300px"></td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px;" class="labelCaption">Sector&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                                    <td style="padding: 5px;" align="left" colspan="4">
                                                        <div style="height: 30px; width: 100%; overflow-y: scroll;">
                                                            <asp:CheckBoxList ID="ddlSector" autocomplete="off" class="ddlSector" DataSource='<%# drpload() %>' RepeatColumns="6"
                                                                RepeatDirection="Vertical" OnSelectedIndexChanged="ddlSector_SelectedIndexChanged" AutoPostBack="true"
                                                                DataValueField="SectorID" CssClass="textbox" Font-Size="12px" Font-Bold="true" Width="98%"
                                                                DataTextField="SectorName" runat="server" ClientIDMode="Static"
                                                                AppendDataBoundItems="false">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <div style="float: right; margin-left: 200px;">
                                            <asp:Button ID="btnPrint" runat="server" Text="Print" CommandName="print" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" />
                                            <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="search" Visible="false" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSearch_Click" />
                                            <asp:Button ID="cmdCancelSearch" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancelSearch_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                        <%--<FooterTemplate>
                            
                        </FooterTemplate>--%>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Panel ID="panGrd" runat="server" autocomplete="off" Width="100%" Visible="false">
                        <div style="float: left">
                            <a onclick="javascript:PrintDiv();" autocomplete="off" runat="server" style="font-size: 12px; color: Black; font-family: Segoe UI; font-weight: bold;" href="javascript:void()"><b>Print Loan Interest Report</b></a>
                        </div>
                        <div id="divLoanInt" style=" width: 100%;">
                            <asp:GridView ID="tbl" runat="server" autocomplete="off" Width="100%" align="center" GridLines="Both" ShowFooter="false" OnDataBound = "OnDataBound" OnRowDataBound="tbl_OnRowDataBound" OnRowCreated = "OnRowCreated"
                                AutoGenerateColumns="false" DataKeyNames="LOANEE_CODE, SUB_CODE " CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <SelectedRowStyle BackColor="#87CEEB" />
                                <Columns>
                                    <asp:BoundField DataField="LOANEE_TYPE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Loanee Type" />
                                    <asp:BoundField DataField="LOANEE_CODE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Loanee Code" />
                                    <asp:BoundField DataField="HEALTH_CODE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Health Code" />
                                    <asp:BoundField DataField="LOANEE_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Loanee Name" />
                                    <asp:BoundField DataField="SUB_CODE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sub Code" />
                                    <%--<asp:BoundField DataField="SUB_CODE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sub Code" />--%>
                                    <asp:BoundField DataField="AMOUNT"  DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Interest" />
                                    <asp:BoundField DataField="penalty"  DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Penalty" />
                                    <asp:BoundField DataField="rebate"  DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Rebate" />
                                </Columns>
                                <FooterStyle CssClass="labelCaption" BackColor ="#99ccff"/>
                            </asp:GridView>
                        </div>
                    </asp:Panel>

                </td>
            </tr>

            <tr>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>
