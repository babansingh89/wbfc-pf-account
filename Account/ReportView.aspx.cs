﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;                 
using System.Text;
using System.Web.Script.Services;
using System.Globalization;        
using System.Web.Script;      
using System.Web.Script.Serialization;        
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Management;
using WebUtility;

public partial class ReportView : System.Web.UI.Page        
{
    string @RptFieldParameter = "@RptFieldParameter";
    static string StrFormtName = "";
    static int IntuserId = 0;
    static string StrFormula = "";
    static string ReportType = "";
    static string StruserId = "";
    static string StrReporttName = "";
    static string StrPaperSize = "";
    static int ColumnCount = 0; 
    static string PDFRptName = "";       
    String ConnString = "";      
    CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();

    protected void Page_Load(object sender, EventArgs e)            
    {
        try
        {
            if (Request.QueryString.Count > 0)
            {
                if (Request.QueryString["E"] != null)
                {
                    ExportPDFReport();
                }
            }
            else
            {
                BindReport(crystalReport);
            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }    
    }
    protected void ExportPDFReport()
    {

        ReportDocument crystalReport = new ReportDocument();
        BindReport(crystalReport);

        ExportFormatType formatType = ExportFormatType.NoFormat;
        formatType = ExportFormatType.PortableDocFormat;
        //switch (rbFormat.SelectedItem.Value)
        //{
        //    case "Excel":
        //        formatType = ExportFormatType.Excel;
        //        break;
        //    case "PDF":
                
        //        break;
        //}
        crystalReport.ExportToHttpResponse(formatType, Response, false, PDFRptName);
        Response.End();
    }
    protected void ExportPDF(object sender, EventArgs e)
    {

        ReportDocument crystalReport = new ReportDocument();
        BindReport(crystalReport);
      
        ExportFormatType formatType = ExportFormatType.NoFormat;
        switch (rbFormat.SelectedItem.Value)
        {
            case "Excel":
                formatType = ExportFormatType.Excel;
                break;
            case "PDF":
                formatType = ExportFormatType.PortableDocFormat;
                break;          
        }
        crystalReport.ExportToHttpResponse(formatType, Response, true, PDFRptName);
        Response.End();
    }

    private void BindReport(ReportDocument crystalReport)
    {
        try
        {
            StrFormula = "";
            IntuserId = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            StrFormtName = Convert.ToString(HttpContext.Current.Session[SiteConstants.StrFormName]);
            string ProcReportName = Convert.ToString(HttpContext.Current.Session[SiteConstants.StrReportName]);
            ReportType = Convert.ToString(HttpContext.Current.Session[SiteConstants.StrReportType]);
            ConnString = DataAccess.DBHandler.GetConnectionString();

            DataSet DS = DBHandler.GetResults("Get_Find_Report", IntuserId, StrFormtName, ProcReportName, ReportType);
            DataTable DtRptVal = new DataTable();
            DtRptVal = DS.Tables[0];
                     
            if (DtRptVal.Rows.Count > 0)
            {
                if (DtRptVal.Rows[0]["Formula"] != "")
                {
                    StrFormula = DtRptVal.Rows[0]["Formula"].ToString();
                }

                if (DtRptVal.Rows[0]["ReportName"] != "")
                {
                    StrReporttName = DtRptVal.Rows[0]["ReportName"].ToString();
                    PDFRptName = StrReporttName;
                    StrReporttName = StrReporttName + ".rpt";

                }

                if (DtRptVal.Rows[0]["UserId"] != "")
                {
                    StruserId = DtRptVal.Rows[0]["UserId"].ToString();
                }

                if (DtRptVal.Rows[0]["PaperSize"] != "")
                {
                    StrPaperSize = DtRptVal.Rows[0]["PaperSize"].ToString();
                    ddlPaperSize.Items.Clear();
                    ddlPaperSize.Items.Add(StrPaperSize);
                    if (StrPaperSize == "")
                    {
                        SetPaperSize();
                    }
                }

                crystalReport.Load(Server.MapPath("~/Reports/" + StrReporttName + "")); // Report Name Coding Here
                if (StrFormula != "")
                {
                    crystalReport.RecordSelectionFormula = StrFormula;
                }

                crystalReport.Refresh();
                crystalReport.DataSourceConnections[0].SetConnection(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
                crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
                crystalReport.SetDatabaseLogon(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
                crystalReport.Refresh();

                if (crystalReport.Subreports.Count > 0)
                {
                    crystalReport.Subreports[0].DataSourceConnections[0].SetConnection(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
                    crystalReport.Subreports[0].DataSourceConnections[0].IntegratedSecurity = false;
                    crystalReport.Subreports[0].SetDatabaseLogon(Utility.ServerName(ConnString), Utility.DatabaseName(ConnString), Utility.UserID(ConnString), Utility.Password(ConnString));
                }
                 

                for (ColumnCount = 1; ColumnCount <= 10; ColumnCount++)
                {
                    if (DtRptVal.Rows[0]["Parameter" + Convert.ToString(ColumnCount)].ToString() != "")
                    {
                        @RptFieldParameter = @RptFieldParameter + Convert.ToString(ColumnCount).ToString();
                        //crystalReport.SetParameterValue(@RptFieldParameter + Convert.ToString(ColumnCount), DtRptVal.Rows[0]["Parameter" + Convert.ToString(ColumnCount)].ToString());
                        //crystalReport.SetParameterValue(@RptFieldParameter, DtRptVal.Rows[0]["Parameter" + Convert.ToString(ColumnCount)].ToString());
                        crystalReport.SetParameterValue(crystalReport.ParameterFields[ColumnCount - 1].Name, DtRptVal.Rows[0]["Parameter" + Convert.ToString(ColumnCount)].ToString());

                    }
                }
                //crystalReport.ReadRecords();
            }

            CrystalReportViewer1.DisplayToolbar = true;
            CrystalReportViewer1.Zoom(125);  // Page Width
            CrystalReportViewer1.Visible = true;
            CrystalReportViewer1.ReportSource = crystalReport;
            CrystalReportViewer1.DataBind();
            //Response.Write(crystalReport.ParameterFields[1].Name);
            //ExportFormatType formatType = ExportFormatType.NoFormat;
            //formatType = ExportFormatType.PortableDocFormat;

            //crystalReport.ExportToHttpResponse(formatType, Response, false, PDFRptName);
            //Response.End();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
        

    }

    protected void SetPaperSize()
    {
        ddlPaperSize.Items.Clear();
        ddlPaperSize.Items.Add("Page - A4");
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        this.crystalReport.Close();
        this.crystalReport.Dispose();
    }

    public void OnConfirmprint(object sender, EventArgs e)
    {
        try
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {

                ReportDocument crystalReport = new ReportDocument();
                BindReport(crystalReport);

                ExportFormatType formatType = ExportFormatType.NoFormat;
                switch (rbFormat.SelectedItem.Value)
                {
                    case "Excel":
                        formatType = ExportFormatType.Excel;
                        break;
                    case "PDF":
                        formatType = ExportFormatType.PortableDocFormat;
                        break;
                }

                crystalReport.ExportToHttpResponse(formatType, Response, false, PDFRptName);
                Response.End();

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
        
    }
    
}