﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="AccountPosition.aspx.cs" Inherits="AccountPosition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="js/AccountPosition.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
     <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>ACCOUNT POSITION</td>
            </tr>            
        </table>                
        <br/>      
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
             <tr>
                <td align="right" style="font-size:15px; ">Report Name&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                 <td>
                        <asp:DropDownList ID="ddlReportName" autocomplete="off" runat="server" Width="200px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                         <asp:ListItem Value="0">(Select Report Name)</asp:ListItem>
                         <asp:ListItem Value="AP">Accounts Position</asp:ListItem>
                         <asp:ListItem Value="MInRA">Monthly Installment Advice</asp:ListItem> <%--Monthly Installment and Repayment Advise--%>
                         <asp:ListItem Value="MRS">Monthly Installment Statement</asp:ListItem> <%--Monthly Repayment Statement--%>
                         <asp:ListItem Value="IRA">Quarterly Interest Advise</asp:ListItem>      <%--Interest Repayment Advise--%>                                        
                         <asp:ListItem Value="APLD">Account Position Loan Detail</asp:ListItem>  
                         <asp:ListItem Value="APVD">Account Position Voucher Detail</asp:ListItem>  
                         <asp:ListItem Value="DIS">Account Statement of Disbursement</asp:ListItem>  
                         <asp:ListItem Value="SANC">Account Statement of Sanction</asp:ListItem>  
                      </asp:DropDownList>
                </td>
                 <td></td>
            </tr>       
            <tr id="ym">
                <td style="font-size:15px;" align="right">Year Month <span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>                  
                      <asp:TextBox ID="txtYearMonth" autocomplete="off" MaxLength="6" Width="200px" placeholder="YYYYMM" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>                          
                <td></td>                   
            </tr>
            <tr id="tdStatement" style="display:none;">
                 <td  align="right" style="font-size:15px;">From Date&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>                  
                    <asp:TextBox ID="txtFrmDate" autocomplete="off" ReadOnly="true" placeholder="dd/mm/yyyy" Width="200px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>                 
                    </td>
                <td  align="right" style="font-size:15px;">To Date&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>                  
                    <asp:TextBox ID="txtToDate" autocomplete="off" ReadOnly="true" placeholder="dd/mm/yyyy" Width="200px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>                 
                    </td>
                <td></td>
            </tr>      
            <tr id="tdLCChange">
                 <td style="font-size:15px;"   align="right" >Loanee Code&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>    </td>
                <td>              
                   
                    <asp:TextBox ID="txtLoaneeCode" autocomplete="off" Width="200px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>  
                    
                     <asp:TextBox ID="txthdnLoaneeCode" autocomplete="off"  Width="10px" style="text-transform:uppercase;display:none; "  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>                   
                    <asp:CheckBox ID="chckAllLoanee" autocomplete="off" ClientIDMode="Static" onclick="selectAll();" Font-Size="12px" Font-Bold="true" Text=" All" runat="server" />     
                     <asp:Button ID="cmdAdd" runat="server" autocomplete="off" Text="Add" Width="50" Height="20" />
                   <asp:DropDownList ID="ddladdLoanee" autocomplete="off" runat="server" Width="200px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                      </asp:DropDownList>
                    
             </td>
                
                <td></td>
            </tr>
            <tr id="tdAsOnChange">
                <td style="font-size:15px;" align="right"><asp:Label ID="lblAsOn" runat="server" Text="As On" ></asp:Label>  &nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>                  
                      <asp:TextBox ID="txtAsOn" autocomplete="off" placeholder="dd/mm/yyyy"  Width="200Px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>                   
                </td>                            
               <td></td>
            </tr>    
            <tr id="tdMIAchange">
                 <td  align="right" style="font-size:15px;">Minimum Installment Amount &nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>                  
                    <asp:TextBox ID="txtMinInsAmount" autocomplete="off" Width="200Px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>                 
                    </td>
                <td></td>
            </tr>  
            
             <tr id="tdDoc">
                 <td  align="right" style="font-size:15px;">Document Suffix &nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>                  
                    <asp:TextBox ID="txtDoc" autocomplete="off" Width="200Px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>                 
                    </td>
                <td></td>
            </tr>
             <tr id="TdEtc">
                 <td  align="right" style="font-size:15px;">Document Etc &nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>                  
                    <asp:TextBox ID="txtEtc" autocomplete="off" Width="200Px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>                 
                    </td>
                <td></td>
            </tr> 
            
                     
            <tr>      
                <td></td>         
                <td >
                       <asp:Button ID="btnGenerateReport" autocomplete="off" runat="server" Text="Report"  Width="100px" CssClass="save-button DefaultButton"/>
                       <asp:Button ID="btnCancel" autocomplete="off" runat="server" Text="Cancel "  Width="100px" CssClass="save-button"
                          OnClientClick='javascript: return checkBlunk_btnCopyData()' />
                </td> 
                <td></td>              
            </tr>        
        </table>

         <%--<div style="overflow-y: scroll; height: 300px; width: 40%; float: left">
             <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
                 AutoGenerateColumns="false" CellPadding="2" CellSpacing="2" AllowPaging="false">
                 <AlternatingRowStyle BackColor="Honeydew" />
                 <SelectedRowStyle BackColor="#87CEEB" />
                 <Columns>
                     <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Lonee Code" />
                     <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Delete" />
                 </Columns>
             </asp:GridView>
         </div>--%>

     </div>
</asp:Content>
