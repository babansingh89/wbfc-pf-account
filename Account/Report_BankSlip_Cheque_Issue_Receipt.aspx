﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Report_BankSlip_Cheque_Issue_Receipt.aspx.cs" Inherits="Report_BankSlip_Cheque_Issue_Receipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/Report_BankSlip_Cheque_Issue_Receipt.aspx.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
             $(".DefaultButton").click(function (event) {
                 event.preventDefault();
             });
        });

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
          
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Report Bank Slip-Cheque Issue-Cheque Receipt</td>

              
            </tr>
        </table>
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td align="right" style="width: 40%">
                    <asp:Label ID="Label1" runat="server" autocomplete="off" CssClass="testClass" Font-Bold="true" Font-Size="16px">Select Report Type :  </asp:Label>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlReportType" autocomplete="off" Width="60%" Height="25px" ClientIDMode="Static" runat="server" CssClass="textbox" AppendDataBoundItems="true"
                            DataTextField="Column1" DataValueField="UserID">
                            <asp:ListItem Text="---- Please Select ----" Value=""></asp:ListItem>
                            <asp:ListItem Text="Bank Deposit Slip" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Cheque Issue" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Cheque Receipt" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
            </tr>
        </table>

        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td> <asp:Label ID="Label2" runat="server" CssClass="testClass" Font-Bold="true" Font-Size="13px">Start Date :</asp:Label></td>
                <td> <asp:TextBox ID="txtStartDate" autocomplete="off" ClientIDMode="Static" Font-Bold="true" runat="server" CssClass="inputbox2" placeholder="DD/MM/YYYY" ReadOnly="true"></asp:TextBox>
                </td>
                <td><asp:Label ID="Label3" autocomplete="off" runat="server" CssClass="testClass" Font-Bold="true" Font-Size="13px">End Date :</asp:Label>

                </td>
                <td><asp:TextBox ID="txtEndDate" autocomplete="off" ClientIDMode="Static" Font-Bold="true" runat="server" CssClass="inputbox2" placeholder="DD/MM/YYYY" ReadOnly="true"></asp:TextBox>
                </td>
                <td> <asp:Label ID="LblBank" autocomplete="off" runat="server" Font-Bold="true" CssClass="testClass" Font-Size="13px">Bank Code:</asp:Label>
                </td>
                <td class="labelCaption">
                        <asp:TextBox ID="txtBank" autocomplete="off" PlaceHolder="---(Select Bank)---" Width="300px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestBank"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="float: right">
                    <asp:Label ID="Label4" autocomplete="off" runat="server" Font-Bold="true" CssClass="testClass" Font-Size="13px">Start Voucher No : </asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtVoucharNo" autocomplete="off" MaxLength="100" Font-Bold="true" ClientIDMode="Static" Text='' runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>
                <td colspan="0" style="float: right">
                    <asp:Label ID="Label5" runat="server" autocomplete="off" Font-Bold="true" CssClass="testClass" Font-Size="13px">End Voucher No :</asp:Label>
                </td>
                <td colspan="0">
                    <asp:TextBox ID="txtEndVoucharNO" autocomplete="off" Font-Bold="true" MaxLength="100" ClientIDMode="Static" Text='' runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>
                <td colspan="0" style="float: right">
                    <asp:Label ID="Label73" runat="server" autocomplete="off" Font-Bold="true" CssClass="testClass" Font-Size="13px">Search Type :</asp:Label>
                </td>
                <td>
                    <asp:RadioButton ID="rbCode" autocomplete="off" font-size="13px" Text="Bank Code" runat="server" GroupName="a" Checked="true" Font-Bold="true" class="labelCaption"/>
                   <asp:RadioButton ID="rbbank" autocomplete="off" font-size="13px" Text="Bank Name" runat="server" GroupName="a" Font-Bold="true" class="labelCaption"/>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td colspan="3" style="float: right">
                    <asp:Button ID="cmdPrint" runat="server" Text="Print" CommandName="Add" autocomplete="off"
                     OnClientClick='javascript: return BeforeSave()' Width="100px" Height="30px" CssClass="save-button DefaultButton" />
                </td>
                <td colspan="3"> 
                    <asp:Button ID="cmdRefrsh" runat="server" Text="Refresh" CommandName="Add" autocomplete="off"
                    Width="100px" Height="30px" CssClass="save-button" OnClick="cmdReset_Click" />
                </td>
            </tr>
        </table>
    </div>

</asp:Content>

