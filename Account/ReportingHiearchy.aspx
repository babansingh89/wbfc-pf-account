﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ReportingHiearchy.aspx.cs" Inherits="ReportingHiearchy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" src="js/EmployeeHiearchy.js"></script>

     <script type="text/javascript">
         function beforeSave()
         {
             alert("hi");
        $("#frmEcom").validate();
        $("#txtReportingNo").rules("add", { required: true, messages: { required: "Please Enter Your Old Password" } });
        }
   </script> 
     
     <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
        }

        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            background-color: #fff;
            font-size: 10pt !important;
            font-family: Arial !important;
        }

            table th {
                background-color: #B8DBFD;
                color: #333;
                font-weight: bold;
            }

            table, table table td {
                border: 0px solid #ccc;
            }
        /*table, th, td {
            cellpadding:1px;
            cellspacing:1px;
        }*/
        th {
            background: lightskyblue;
        }

        .selected_row {
            background-color: #A1DCF2 !important;
        }

        .Gridposition {
            margin-left: 10px;
            width: 300px;
        }

        .textposition {
            margin-left: 1px;
            width: 295px;
        }

        .GridFolioDes {
            margin-left: 1px;
            width: 570px;
        }

        .Foliotextposition {
            margin-left: 2px;
            width: 565px;
        }

        .TextSearchVendor {
            margin-left: 1px;
            width: 516px;
        }

        .GridVendor {
            margin-left: 1px;
            width: 520px;
        }

        .GridAllIndent {
            margin-left: 1px;
            width: 800px;
        }

        .input3 {
            margin-left: 1px;
            width: 200px;
        }

        /*=================================================*/
        .Grid {
            border: solid 1px #59bdcc;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

            .Grid td {
                padding: 2px;
                border: solid 1px #59bdcc;
                color: navy;
                background-color: #e6f3be;
            }

            .Grid th {
                padding: 4px 2px;
                background: #609eb3;
                font-size: 0.9em;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript">

         $(document).ready(function () {
             $(".DefaultButton").click(function (event) {
                 event.preventDefault();
             });
         });


    </script>

       <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>EMPLOYEE REPORTING MASTER</td>
            </tr>
        </table>
        <br />
           <div style="float:left; text-align:left; " >
               <asp:TreeView ID="treeViews" runat="server" ImageSet="Arrows" NodeWrap="true"
                   ExpandDepth="-1" NodeIndent="10"
                   PopulateNodesFromClient="False">
                   <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                   <NodeStyle Font-Names="Tahoma" Font-Size="11px" ForeColor="Black"
                       HorizontalPadding="3px" NodeSpacing="0px" VerticalPadding="0px" />
                   <ParentNodeStyle Font-Bold="False" />
                   <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD"
                       HorizontalPadding="0px" VerticalPadding="0px" />
               </asp:TreeView>
           </div>

           <div style="float: right;">
               <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
                   <tr>
                       <td>
                           <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                               OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                               <InsertItemTemplate>
                                   <table align="center" cellpadding="5" width="100%">
                                       <tr>
                                           <td class="labelCaption">Reporting Manager No. &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtReportingNo" autocomplete="off" MaxLength="5" Style="text-transform: uppercase;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                           <td class="labelCaption">Reporting Manager Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtReportingName" autocomplete="off" MaxLength="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td class="labelCaption">Employee No. &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtEmployeeNo" autocomplete="off" MaxLength="5" Style="text-transform:uppercase;" AutoPostBack="false" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                           <td class="labelCaption">Employee Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtEmpName" autocomplete="off" MaxLength="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td class="labelCaption">Rank &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtRank" autocomplete="off" MaxLength="5" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                       </tr>
                                   </table>
                               </InsertItemTemplate>
                               <EditItemTemplate>
                                   <table align="center" cellpadding="5" width="100%">
                                       <tr>
                                           <td class="labelCaption">Reporting Manager No. &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtReportingNo" autocomplete="off" MaxLength="5" Style="text-transform: uppercase;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                           <td class="labelCaption">Reporting Manager Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtReportingName" autocomplete="off" MaxLength="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                       </tr>
                                       <tr>
                                           <td class="labelCaption">Employee No. &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtEmployeeNo" autocomplete="off" MaxLength="5" Style="text-transform:uppercase;" AutoPostBack="false" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                           <td class="labelCaption">Employee Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtEmpName" autocomplete="off" MaxLength="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                       </tr>

                                       <tr>
                                           <td class="labelCaption">Rank &nbsp;&nbsp;<span class="require">*</span> </td>
                                           <td class="labelCaption">:</td>
                                           <td align="left">
                                               <asp:TextBox ID="txtRank" autocomplete="off" MaxLength="5" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                           </td>
                                       </tr>
                                   </table>
                               </EditItemTemplate>
                               <FooterTemplate>
                                   <table align="center" cellpadding="5" width="100%">
                                       <tr>
                                           <td colspan="4" class="labelCaption">
                                               <hr class="borderStyle" />
                                           </td>
                                       </tr>
                                       <tr>
                                           <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                           <td>&nbsp;</td>
                                           <td align="left">
                                               <div style="float: left; margin-left: 200px;">
                                                   <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" OnClick="cmdSave_Click" autocomplete="off"
                                                       Width="100px" CssClass="save-button"  />

                                                   <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                                       Width="100px" CssClass="save-button" />
                                               </div>
                                           </td>
                                       </tr>
                                   </table>
                               </FooterTemplate>
                           </asp:FormView>
                       </td>
                   </tr>
                   
                   
               </table>
           </div>

           <%-- <%-- ================ START Employee List POP UP GRIDVIEW =====================  --%>
           <div id="dialogEmployeeList" style="display: none">

               <asp:TextBox ID="txtSearchEmployee" autocomplete="off" runat="server" MaxLength="480" Style="text-transform:uppercase;" CssClass="Foliotextposition"></asp:TextBox>
               <asp:GridView ID="GridViewEmployeeList" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false"
                   PageSize="5" CssClass="GridFolioDes" AllowPaging="true">
                   <Columns>
                       <asp:BoundField DataField="EmpNo" HeaderText="EmpNo" ItemStyle-Width="50" />
                       <asp:BoundField DataField="EmpName" HeaderText="EmpName" ItemStyle-Width="200" />
                   </Columns>
               </asp:GridView>
           </div>
          <%--  ================ END Employee List POP UP GRIDVIEW =====================  --%>
    </div>
</asp:Content>

