﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="VoucherReport.aspx.cs" Inherits="VoucherReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/VoucherPrint.js?v=3"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            document.getElementById("A").checked = true;
            document.getElementById("rdGLCODE").checked = true;
            document.getElementById("rd10X6").checked = true;
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
            
            $('#v1').hide();    
            $('#v2').hide();
            $('#v3').hide();
            $('#v4').hide();
            $('#btnPrint').hide();
            $('#cmdSearch').show();
            $('#v6').show();
            $('#v7').hide();
            $("#trvchdate").show();
            $('#trVchAmt').show();
            $('#tdempname').show();
            $('#tdemp').show();
            $('#tdempnameDis').show();
            $('#trGLSL').show();
            
            $('#tdvchdate1').hide();
            $('#tdvchdate2').hide();
            $('#tdvchdate3').hide();
            $('#tdvchdate4').hide();
            $('#tdvchdate5').hide();
            $('#tdvchdate6').hide();
                       
            //$('#v7').hide();
            $('#btnPrint').hide();
            $('#txtYearmonth').focus();
            $("input[name='chkSearch']").click(function () {
                alert('s1');
                if ($('#A').is(":checked")) {
                    $('#v1').hide();
                    $('#v2').hide();
                    $('#v3').hide();
                    $('#v4').hide();
                    $('#btnPrint').hide();
                    $('#cmdSearch').show();
                    $('#v6').show();
                    $('#v7').hide();

                    $('#trVchAmt').show();
                    $('#tdempname').show();
                    $('#tdemp').show();
                    $('#tdempnameDis').show();
                    $('#trGLSL').show();
                    $("#trvchdate").show();

                    $('#tdvchdate1').hide();
                    $('#tdvchdate2').hide();
                    $('#tdvchdate3').hide();
                    $('#tdvchdate4').hide();
                    $('#tdvchdate5').hide();
                    $('#tdvchdate6').hide();
                    $('#txtYearmonth').focus();

                } else {
                    alert('sm');
                    $('#v1').show();
                    $('#v2').show();
                    $('#v3').show();
                    $('#v4').show();
                    $('#btnPrint').show();
                    $('#cmdSearch').hide();
                    $('#panVchFinal').hide();
                    $('#v6').hide();
                    $('#v7').show();

                    $('#trVchAmt').hide();
                    $('#tdempname').hide();
                    $('#tdemp').hide();
                    $('#tdempnameDis').hide();
                    $('#trGLSL').hide();
                    $("#trvchdate").hide();

                    $('#tdvchdate1').show();
                    $('#tdvchdate2').show();
                    $('#tdvchdate3').show();
                    $('#tdvchdate4').show();
                    $('#tdvchdate5').show();
                    $('#tdvchdate6').show();
                    $('#txtYearmonth').focus();
                }
            });

        });

        $(document).ready(function () {
            $('#txtFrmDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                changeYear: true,
                changeMonth: true,
                dateformat: 'dd/mm/yyyy'
            });

            $('#txtVchFromDate1').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                changeYear: true,
                changeMonth: true,
                dateformat: 'dd/mm/yyyy'
            });
        });
        
        $(document).ready(function () {
            $('#txtToDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            $('#txtVchToDate1').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            $('#txtVchFromDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            $('#txtVchToDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });
        });
        
        $(document).ready(function () {
            $('#txtYearmonth').attr({ maxLength: 6 });
            $('#txtYearmonth').keypress(function (evt) {

                if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                    alert("Please Enter a Numeric Value");
                    return false;
                }

            });
            $('#txtVchAmount').keypress(function (evt) {

                if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                    alert("Please Enter a Numeric Value");
                    return false;
                }

            });
        });

        function beforeSave() {
            $("#frmEcom").validate();
            $("#txtYearmonth").rules("add", { required: true, messages: { required: "Please enter Year & Month" } });
            //$("#txtVchNo").rules("add", { required: true, messages: { required: "Please enter Voucher No." } });
            $("#vchNoFromTo").rules("add", { required: true, messages: { required: "Please enter Voucher No to." } });
            $("#ddlVoucharType").rules("add", { required: true, messages: { required: "Please select Vouchar Type" } });

        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }

        //$(document).ready(function () {
        //    gridviewScroll();
        //});

        //function gridviewScroll() {
        //    $('#tbl').gridviewScroll({
        //        width: 420,
        //        startHorizontal: 0,
        //        barhovercolor: "#3399FF",
        //        barcolor: "#3399FF"
        //    });
        //}
        FunA = function () {
            $('#v1').hide();
            $('#v2').hide();
            $('#v3').hide();
            $('#v4').hide();
            $('#btnPrint').hide();
            $('#cmdSearch').show();
            $('#v6').show();
            $('#v7').hide();

            $('#trVchAmt').show();
            $('#tdempname').show();
            $('#tdemp').show();
            $('#tdempnameDis').show();
            $('#trGLSL').show();
            $("#trvchdate").show();

            $('#tdvchdate1').hide();
            $('#tdvchdate2').hide();
            $('#tdvchdate3').hide();
            $('#tdvchdate4').hide();
            $('#tdvchdate5').hide();
            $('#tdvchdate6').hide();
            $('#txtYearmonth').focus();
        };

        FunB = function () {
            $('#v1').show();
            $('#v2').show();
            $('#v3').show();
            $('#v4').show();
            $('#btnPrint').show();
            $('#cmdSearch').hide();
            $('#panVchFinal').hide();
            $('#v6').hide();
            $('#v7').show();

            $('#trVchAmt').hide();
            $('#tdempname').hide();
            $('#tdemp').hide();
            $('#tdempnameDis').hide();
            $('#trGLSL').hide();
            $("#trvchdate").hide();

            $('#tdvchdate1').show();
            $('#tdvchdate2').show();
            $('#tdvchdate3').show();
            $('#tdvchdate4').show();
            $('#tdvchdate5').show();
            $('#tdvchdate6').show();
            $('#txtYearmonth').focus();
        };

        $(document).ready(function () {

            $('[id$=chkHeader]').click(function () {
                $("[id$='chkApprove']").prop('checked', this.checked);
            });
        });

    </script>
    <style type="text/css">
        .hiddencol {
            display: none;
        }
    </style>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">

     <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>
    <%-- =================== Bound Records Display By Search Part =============--%>
                    
                    <div id="dialogSearchBound" runat="server" style="display: none">
						
                        <asp:GridView ID="tbl" autocomplete="off" Width="100%"  ClientIDMode="Static" style="border:2px solid #59bdcc"
                             runat="server" GridLines="Both" AutoGenerateColumns="false">
							<AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                 <asp:BoundField DataField="desc_dtl" HeaderText="desc_dtl" ItemStyle-Width="200" /> 
                                <asp:BoundField DataField="InstNo" HeaderText="InstNo" ItemStyle-Width="300" /> 
                                <asp:BoundField DataField="InstDate" HeaderText="InstDate" ItemStyle-Width="300" /> 
                                <asp:BoundField DataField="Amount" HeaderText="Amount" ItemStyle-Width="300" /> 
                                <asp:BoundField DataField="Bank" HeaderText="Bank" ItemStyle-Width="50" /> 
                               
                            </Columns>

						</asp:GridView>
							</div>

    <div >
        <table class="headingCaption" width="98%" >
            <tr>
                <td>Voucher Report</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td colspan="6">
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td style="height: 15px;" align="left">Voucher Report Search</td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    <asp:formview id="dv" runat="server" width="99%" cellpadding="4" autogeneraterows="False" autocomplete="off" 
                        defaultmode="Insert" horizontalalign="Center" gridlines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">

                                <tr>
                                    <td>
                                        <div id="searchYM" style="width: 100%;">
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td align="center" colspan="9">
                                                        <input type='radio' autocomplete="off" runat="server" clientidmode="Static" name='chkSearch' id='A' onclick="FunA();" /><font size="3px" color="navy"><b>Show Voucher</b></font>
                                                        <input type='radio' autocomplete="off" runat="server" clientidmode="Static" name='chkSearch' id='B' onclick="FunB();" /><font size="3px" color="navy"><b>Print Voucher</b></font>
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="9" class="labelCaption">
                                                        <hr class="borderStyle" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="labelCaption">Year & Month(YYYYMM) &nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtYearmonth" autocomplete="off" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                      
                                                    </td>

                                                    <td id="v6" class="labelCaption">Voucher No.&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td id="v7" style="display:none" class="labelCaption">Voucher No. From &nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtVchNo" autocomplete="off" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                    <td id="v1" style="display:none" class="labelCaption">Voucher No To &nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td id="v2" style="display:none" class="labelCaption">:</td>
                                                    <td id="v3" style="display:none" align="left">
                                                        <asp:TextBox ID="vchNoFromTo" autocomplete="off" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td class="labelCaption">Vouchar Type&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlVoucharType" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2" Width="160px" Height="25px" ForeColor="#18588a">
                                                            <asp:ListItem Text="--Select Voucher Type--" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="Normal" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Instalment" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Interest" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td id="tdempname" class="labelCaption">Employee Name</td>
                                                    <td id="tdemp" class="labelCaption">:</td>
                                                    <td id="tdempnameDis" align="left">
                                                        <asp:DropDownList ID="ddlEmp" autocomplete="off" DataSource='<%# drpload() %>'
                                                            DataValueField="UserID"
                                                            DataTextField="UserName" Width="160px" Height="25px" runat="server" ForeColor="#18588a"
                                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                                            <asp:ListItem Text="(--Select Employee--)" Value=""></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>

                                                    <td id="tdvchdate1" class="labelCaption">Vouchar From Date&nbsp;&nbsp;<span class="require"></span> </td>
                                                    <td id="tdvchdate2" class="labelCaption">:</td>
                                                    <td  id="tdvchdate3" align="left">
                                                        <asp:TextBox ID="txtVchFromDate1" autocomplete="off" AutoPostBack="false" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>

                                                     <td id="tdvchdate4" class="labelCaption">Vouchar To Date&nbsp;&nbsp;<span class="require"></span> </td>
                                                    <td id="tdvchdate5" class="labelCaption">:</td>
                                                    <td id="tdvchdate6" align="left">
                                                        <asp:TextBox ID="txtVchToDate1" autocomplete="off" AutoPostBack="false" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>

                                                    <%--<td id="v4" align="center" colspan="6">
                                                        <input type='radio' name='chkPrint' id='Radio1' checked="checked" /><font size="2px">Print Voucher</font>
                                                        <input type='radio' name='chkPrint' id='Radio2' /><font size="2px">Print Exchang Advice </font>
                                                    </td>--%>
                                                </tr>

                                                <tr id="trVchAmt">
                                                    <td class="labelCaption">Vouchar Amount&nbsp;&nbsp;<span class="require"></span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtVchAmount" autocomplete="off" MaxLength="15" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                    <td id="tdNarration" class="labelCaption">Narration</td>
                                                    <td id="tdNarration1" class="labelCaption">:</td>
                                                    <td id="tdNarration2" align="left">
                                                       <asp:TextBox ID="txtNarration" autocomplete="off" Width="150" MaxLength="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                   
                                                </tr>

                                                <tr id="trGLSL">
                                                     <td class="labelCaption">GL/SL&nbsp;&nbsp;<span class="require"></span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">

                                                        <asp:TextBox ID="txtGLSL" autocomplete="off" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestSL"></asp:TextBox>
                                                    </td>
                                                    <td id="td1" class="labelCaption">Search By</td>
                                                    <td id="td2" class="labelCaption">:</td>
                                                    <td id="td3" align="left">
                                                        <input type='radio' name='chkGLCode' autocomplete="off" runat="server" clientidmode="Static" id='rdGLCODE' value="C" /><font size="2px" color="navy"><b>GL/SL Code</b></font>
                                                        <input type='radio' name='chkGLCode' autocomplete="off" runat="server" clientidmode="Static" id='rdGLNAME' value="N" /><font size="2px" color="navy"><b>GL/SL Name</b></font>
                                                    </td>
                                                       
                                                </tr>

                                                <tr id="trvchdate">
                                                    <td class="labelCaption">Vouchar From Date&nbsp;&nbsp;<span class="require"></span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtVchFromDate" autocomplete="off" AutoPostBack="false" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>

                                                    <td class="labelCaption">Vouchar To Date&nbsp;&nbsp;<span class="require"></span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtVchToDate" autocomplete="off" AutoPostBack="false" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>

                                                </tr>
                                                <tr id="v4">
                                                    <td class="labelCaption">Report Name&nbsp;&nbsp;<span class="require"></span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlReportName" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2" Width="160px" Height="25px" ForeColor="#18588a">
                                                            <asp:ListItem Text="Print Voucher" Value="Print Voucher"></asp:ListItem>
                                                            <asp:ListItem Text="Print Exchang Advice" Value="Print Exchang Advice"></asp:ListItem>
                                                            <asp:ListItem Text="Date Wise Voucher Summary" Value="Date Wise Voucher Summary"></asp:ListItem>
                                                            <asp:ListItem Text="Date Wise Voucher Detail" Value="Date Wise Voucher Detail"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>

                                                    <td>
                                                        <input type='radio' name='chkPaper' autocomplete="off" runat="server" clientidmode="Static" id='rd10X6'/><font size="2px" color="navy"><b>Paper Size-10X6</b></font>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <input type='radio' name='chkPaper' autocomplete="off" runat="server" clientidmode="Static" id='rdA4' /><font size="2px" color="navy"><b>Paper Size-A4</b></font>

                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="6">

                                        <div style="float: right; margin-left: 200px;">
                                            <asp:Button ID="btnPrint" runat="server" Text="Print" CommandName="print" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" />
                                            <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="search" autocomplete="off"
                                                Width="100px" CssClass="save-button" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSearch_Click" />
                                            <asp:Button ID="cmdCancelSearch" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button" OnClick="cmdCancelSearch_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>

                    </asp:formview>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:panel id="panVchFinal" runat="server" width="100%" visible="false">
                        <div id="divVchFinal" style="width: 80%;">

                            <%--<table width="80%" align="center">
                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;">YearMonth :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (YearMonth = string.IsNullOrEmpty(YearMonth) ? "" : YearMonth)%></font>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;">Voucher Type :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (VchType = string.IsNullOrEmpty(VchType) ? "" : VchType)%> 
                                        </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;">Voucher Date :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (VchDate = string.IsNullOrEmpty(VchDate) ? "" : VchDate)%>
                                        </font>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;">Voucher No :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (VchNo = string.IsNullOrEmpty(VchNo) ? "" : VchNo)%>
                                        </font>

                                    </td>
                                </tr>
                            </table>--%>
                            <%--<br />--%>
                            <%= (str = string.IsNullOrEmpty(str) ? "" : str)%>
                        </div>
                    </asp:panel>
                </td>
            </tr>

            <tr>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>
