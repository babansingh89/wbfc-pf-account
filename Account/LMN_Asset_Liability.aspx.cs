﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Data.SqlClient;


public partial class LMN_Asset_Liability : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_Asset_Liability_BY_ID", ID);
                Session["ID"] = ID;

                dv.DataSource = dtResult;
                dv.DataBind();
                PopulateGrid();

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_Assets_Liability_BY_ID", ID);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtDescription = (TextBox)dv.FindControl("txtDescription");
                TextBox TxtGroup = (TextBox)dv.FindControl("TxtGroup");
                DropDownList DdlStyle = (DropDownList)dv.FindControl("DdlStyle");
                DropDownList DdlActive = (DropDownList)dv.FindControl("DdlActive");
                DBHandler.Execute("Insert_Assets_Liability", txtDescription.Text, TxtGroup.Text, DdlStyle.Text, DdlActive.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Inserted Successfully.')</script>");
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();

            }
            else if (cmdSave.CommandName == "Edit")
            {
                String ID = (String)Session["ID"];

                TextBox txtDescription = (TextBox)dv.FindControl("txtDescription");
                TextBox TxtGroup = (TextBox)dv.FindControl("TxtGroup");
                DropDownList DdlStyle = (DropDownList)dv.FindControl("DdlStyle");
                DropDownList DdlActive = (DropDownList)dv.FindControl("DdlActive");
                DBHandler.Execute("Update_asset_liability", ID, txtDescription.Text, TxtGroup.Text, DdlStyle.Text, DdlActive.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                cmdSave.Text = "Save";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                Session["ID"] = null;

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
        //
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Asset_Liability");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}