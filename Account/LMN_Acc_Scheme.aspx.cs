﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Globalization;


public partial class LMN_Acc_Scheme : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
                PopulateGLID();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGLID()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("OLD_GL_ID");
            dt.Columns.Add("GL_NAME");
            dt.Rows.Add();
            grdGL.DataSource = dt;
            grdGL.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                //DropDownList DdlType = (DropDownList)dv.FindControl("DdlType");
                //DdlType.Enabled.Equals(false);
                

                DataTable dtResult = DBHandler.GetResult("Get_Acc_Scheme_By_ID", ID);
                Session["ID"] = ID;

                dv.DataSource = dtResult;
                dv.DataBind();
                TextBox txtCode = (TextBox)dv.FindControl("txtCode");
                TextBox txtGLID = (TextBox)dv.FindControl("txtGLID");
                txtCode.Enabled = false;
                txtGLID.Enabled = false;

                PopulateGrid();

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_Acc_Scheme_By_ID", ID);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtCode = (TextBox)dv.FindControl("txtCode");
                TextBox txtName = (TextBox)dv.FindControl("txtName");
                TextBox txtGLID = (TextBox)dv.FindControl("txtGLID");
                DropDownList DdlType = (DropDownList)dv.FindControl("DdlType");
                TextBox txtTotalAmmount = (TextBox)dv.FindControl("txtTotalAmmount");

                TextBox txtFrom = (TextBox)dv.FindControl("txtFrom");
                DateTime dtFrom = DateTime.ParseExact(txtFrom.Text, "dd/mm/yyyy", CultureInfo.InvariantCulture);

                TextBox txtTo = (TextBox)dv.FindControl("txtTo");
                DateTime dtto = DateTime.ParseExact(txtTo.Text, "dd/mm/yyyy", CultureInfo.InvariantCulture);

                TextBox txtSingleUnitrate = (TextBox)dv.FindControl("txtSingleUnitrate");
                TextBox txtInterestRate = (TextBox)dv.FindControl("txtInterestRate");
                DropDownList ddlRepaymentmode = (DropDownList)dv.FindControl("ddlRepaymentmode");
                TextBox txtISIN_NO = (TextBox)dv.FindControl("txtISIN_NO");
                TextBox txtDPID_NO = (TextBox)dv.FindControl("txtDPID_NO");
                DropDownList DdlSlr = (DropDownList)dv.FindControl("DdlSlr");
                TextBox txtPeriodDays = (TextBox)dv.FindControl("txtPeriodDays");

                DBHandler.Execute("Insert_Acc_Scheme",
                txtCode.Text.Equals("") ? DBNull.Value : (object)txtCode.Text,
                 txtName.Text.Equals("") ? DBNull.Value : (object)txtName.Text,
                 DdlType.Text.Equals("") ? DBNull.Value : (object)DdlType.Text,
                 txtTotalAmmount.Text.Equals("") ? DBNull.Value : (object)txtTotalAmmount.Text,
                 dtFrom.ToShortDateString(),
                 dtto.ToShortTimeString(),
                 txtSingleUnitrate.Text.Equals("") ? DBNull.Value : (object)txtSingleUnitrate.Text,
                 txtInterestRate.Text.Equals("") ? DBNull.Value : (object)txtInterestRate.Text,
                 ddlRepaymentmode.Text.Equals("") ? DBNull.Value : (object)ddlRepaymentmode.Text,
                 txtISIN_NO.Text.Equals("") ? DBNull.Value : (object)txtISIN_NO.Text,
                 txtDPID_NO.Text.Equals("") ? DBNull.Value : (object)txtDPID_NO.Text,
                 DdlSlr.Text.Equals("") ? DBNull.Value : (object)DdlSlr.Text,
                 txtPeriodDays.Text.Equals("") ? DBNull.Value : (object)txtPeriodDays.Text,
                  Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]),
                 Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]),
                 txtGLID.Text.Equals("") ? DBNull.Value : (object)txtGLID.Text);

                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Inserted Successfully.')</script>");
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();

            }
            else if (cmdSave.CommandName == "Edit")
            {
                String ID = (String)Session["ID"];

                TextBox txtCode = (TextBox)dv.FindControl("txtCode");
                TextBox txtName = (TextBox)dv.FindControl("txtName");
                TextBox txtGLID = (TextBox)dv.FindControl("txtGLID");
                DropDownList DdlType = (DropDownList)dv.FindControl("DdlType");
                TextBox txtTotalAmmount = (TextBox)dv.FindControl("txtTotalAmmount");
                TextBox txtFrom = (TextBox)dv.FindControl("txtFrom");
                DateTime dtFrom = DateTime.ParseExact(txtFrom.Text, "dd/mm/yyyy", CultureInfo.InvariantCulture);

                TextBox txtTo = (TextBox)dv.FindControl("txtTo");
                DateTime dtto = DateTime.ParseExact(txtTo.Text, "dd/mm/yyyy", CultureInfo.InvariantCulture);

                TextBox txtSingleUnitrate = (TextBox)dv.FindControl("txtSingleUnitrate");
                TextBox txtInterestRate = (TextBox)dv.FindControl("txtInterestRate");
                DropDownList ddlRepaymentmode = (DropDownList)dv.FindControl("ddlRepaymentmode");
                TextBox txtISIN_NO = (TextBox)dv.FindControl("txtISIN_NO");
                TextBox txtDPID_NO = (TextBox)dv.FindControl("txtDPID_NO");
                DropDownList DdlSlr = (DropDownList)dv.FindControl("DdlSlr");
                TextBox txtPeriodDays = (TextBox)dv.FindControl("txtPeriodDays");

                DBHandler.Execute("UPdate_Acc_Scheme",
                 ID,
                 txtCode.Text.Equals("") ? DBNull.Value : (object)txtCode.Text,
                 txtName.Text.Equals("") ? DBNull.Value : (object)txtName.Text,
                 DdlType.Text.Equals("") ? DBNull.Value : (object)DdlType.Text,
                 txtTotalAmmount.Text.Equals("") ? DBNull.Value : (object)txtTotalAmmount.Text,
                 dtFrom.ToShortDateString(),
                 dtto.ToShortTimeString(),
                 txtSingleUnitrate.Text.Equals("") ? DBNull.Value : (object)txtSingleUnitrate.Text,
                 txtInterestRate.Text.Equals("") ? DBNull.Value : (object)txtInterestRate.Text,
                 ddlRepaymentmode.Text.Equals("") ? DBNull.Value : (object)ddlRepaymentmode.Text,
                 txtISIN_NO.Text.Equals("") ? DBNull.Value : (object)txtISIN_NO.Text,
                 txtDPID_NO.Text.Equals("") ? DBNull.Value : (object)txtDPID_NO.Text,
                 DdlSlr.Text.Equals("") ? DBNull.Value : (object)DdlSlr.Text,
                 txtPeriodDays.Text.Equals("") ? DBNull.Value : (object)txtPeriodDays.Text,
                  Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]),
                 Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]),
                 txtGLID.Text.Equals("") ? DBNull.Value : (object)txtGLID.Text);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                cmdSave.Text = "Save";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                Session["ID"] = null;
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Acc_Scheme");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //============================ Start Max Bond Scheme ID Coding Start here===================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_MaxSchemeID()
    {
        string UserRight = "";
        int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_MaxBondSchID");
        if (dtGetData.Rows.Count >  0)
        {
            UserRight = Convert.ToString(dtGetData.Rows[0]["OLD_SCHEME_ID"]);
        }
       
        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(UserRight);
    }
    //============================End Max Bond Scheme ID Coding Start here===================================//

    //============================ Start Duplicate Check Bond Scheme ID Coding Start here===================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DupChkSchemeID(string OLD_SCHEME_ID, string SCHEME_ID)
    {
        string UserRight = "";
        DataTable dtGetData = DBHandler.GetResult("Get_DupCheckBondSchID", OLD_SCHEME_ID, SCHEME_ID);
        if (dtGetData.Rows.Count > 0)
        {
            UserRight = Convert.ToString(dtGetData.Rows[0]["OLD_SCHEME_ID"]);
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(UserRight);
    }
    //============================End Duplicate Check Bond Scheme ID Coding Start here===================================//

    [WebMethod]
    public static GLAll[] Search_GlCode()
    {
        List<GLAll> Detail = new List<GLAll>();
        DataTable dtGetData = DBHandler.GetResult("Get_GLIDOnScheme");
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GLAll DataObj = new GLAll();

            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GLAll //Class for binding data
    {

        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
    }

}