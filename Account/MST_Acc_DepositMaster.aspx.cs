﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Configuration;
using System.Web.Services;
using System.Globalization;         
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;

  
//using System.Globalization;

public partial class MST_Acc_DepositMaster : System.Web.UI.Page
{

    static DataTable dtGLName;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Select Sector Before Continuing To Loan Sanction Page!');window.location.href='welcome.aspx';</script>");
              
            }
            else
            {
                if (!IsPostBack)
                {
                    GridBond();
                    GridIndentScheme();
                    GridIndentSector();
                    GridIndentTds();
                    GridIndentGLName();
                    HttpContext.Current.Session["GLName"] = null;
                    dtGLName = (DataTable)HttpContext.Current.Session["GLName"];
                }
            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void GridBond()
    {
        DataTable Bound = new DataTable();
        Bound.Columns.Add("OLD_SCHEME_ID");
        Bound.Columns.Add("ISSUE_DT");
        Bound.Columns.Add("QTY");
        Bound.Columns.Add("ISSU_AMOUNT");
        Bound.Columns.Add("APPLICANT_NAME");
        Bound.Columns.Add("ADDR");
        Bound.Columns.Add("MATURITY_DT");
        Bound.Columns.Add("DEPOSITSCHEME_ID");
        Bound.Columns.Add("DEPOSIT_ID");
        Bound.Columns.Add("SCHEME_ID");
        Bound.Columns.Add("BOND_ID");
        Bound.Columns.Add("SCHEME_TYPE");
        Bound.Rows.Add();
        tbl.DataSource = Bound;
        tbl.DataBind();


        



    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid(Convert.ToString(HttpContext.Current.Session[SiteConstants.StrFormName]));
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    public void UpdateDeleteRecord(int flag, string Scheme_ID,string Deposit_ID)
    {
        try
        {
            if (flag == 1)
            {                
                DataTable dtResult = DBHandler.GetResult("Acc_Deposit_ByID", Scheme_ID, Deposit_ID);
                Session["Scheme_ID"] = Scheme_ID;
                Session["Deposit_ID"] = Deposit_ID;      
                dv.DataSource = dtResult;
                dv.DataBind();
                CheckBox check15H = (CheckBox)dv.FindControl("CheckBox1");
                if (dtResult.Rows[0]["FORM15H"].ToString() == "Y")
                { check15H.Checked = true; }

                 DataTable dt = new DataTable();
                dt.Columns.Add("ChequeNo");
                dt.Columns.Add("Cheque_DT");
                dt.Columns.Add("DraweeBank");
                dt.Columns.Add("ArrangerName");
                dt.Columns.Add("DepositeAt");

                DataRow dtrow = dt.NewRow();
                dtrow["ChequeNo"] = dtResult.Rows[0]["CHQ_NO"].ToString();
                dtrow["Cheque_DT"] = dtResult.Rows[0]["CHQ_DT"].ToString();
                dtrow["DraweeBank"] = dtResult.Rows[0]["DRAWEE_BANK"].ToString();
                dtrow["ArrangerName"] = dtResult.Rows[0]["ARRANGER_NAME"].ToString();
                dtrow["DepositeAt"] = dtResult.Rows[0]["BANK_CD"].ToString();

                dt.Rows.Add(dtrow);
                HttpContext.Current.Session["GLName"] = dt;          
             

                txtChequeNo.Text = dtResult.Rows[0]["CHQ_NO"].ToString();
                txtCheque_DT.Text = dtResult.Rows[0]["CHQ_DT"].ToString();
                txtDraweeBank.Text = dtResult.Rows[0]["DRAWEE_BANK"].ToString();
                txtArrangerName.Text = dtResult.Rows[0]["ARRANGER_NAME"].ToString();
                txtDepositeAt.Text = dtResult.Rows[0]["BANK_CD"].ToString();

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_Acc_Deposit_ID", Scheme_ID, Deposit_ID);
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlUserSector = (DropDownList)Master.FindControl("ddlUserSector");
            string sec = ddlUserSector.SelectedValue.ToString();

            Button cmdSave = (Button)sender;

            TextBox txtSaveEdit = (TextBox)dv.FindControl("txtSaveEdit");
            TextBox txtScheme_ID = (TextBox)dv.FindControl("txtSchemeType");
            TextBox txtScheme_TYPE = (TextBox)dv.FindControl("txtScheme_ID1");
            TextBox txtBranchID = (TextBox)dv.FindControl("txtBranchID");
            TextBox txtDeposit_ID = (TextBox)dv.FindControl("txtDeposit_ID");
            TextBox txtTDSID = (TextBox)dv.FindControl("txtTDSID");
            TextBox txtAppl_No = (TextBox)dv.FindControl("txtAppl_No");

            TextBox txtAppl_DT = (TextBox)dv.FindControl("txtAppl_DT");
            DateTime dtAppl_DT = new DateTime();
            if (txtAppl_DT.Text.Trim() != "")
            {  dtAppl_DT = DateTime.ParseExact(txtAppl_DT.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture); }

            TextBox txtAppl_Amount = (TextBox)dv.FindControl("txtAppl_Amount");         

            TextBox txtIssue_DT = (TextBox)dv.FindControl("txtIssue_DT");
            DateTime dtIssue_DT = new DateTime();
            if (txtIssue_DT.Text.Trim() != "")
            {  dtIssue_DT = DateTime.ParseExact(txtIssue_DT.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture); }      

            TextBox txtIssue_Amount = (TextBox)dv.FindControl("txtIssue_Amount");     

            TextBox txtQty = (TextBox)dv.FindControl("txtQty");
            TextBox txtDistinctive_Start = (TextBox)dv.FindControl("txtDistinctive_Start");
            TextBox txtDistinctive_End = (TextBox)dv.FindControl("txtDistinctive_End");
            TextBox txtPeriod = (TextBox)dv.FindControl("txtPeriod");

            string pmonths = "";
            pmonths = txtPeriod.Text;

            TextBox txtMaturity_DT = (TextBox)dv.FindControl("txtMaturity_DT");
            DateTime dttxtMaturity_DT = new DateTime(); 
            if (txtMaturity_DT.Text.Trim() != "")
            {  dttxtMaturity_DT = DateTime.ParseExact(txtMaturity_DT.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture); }

            TextBox txtRepaid_DT = (TextBox)dv.FindControl("txtRepaid_DT");
            DateTime dttxtRepaid_DT = new DateTime();
            if (txtRepaid_DT.Text.Trim() != "")
            {  dttxtRepaid_DT = DateTime.ParseExact(txtRepaid_DT.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture); }

            TextBox txtName = (TextBox)dv.FindControl("txtName");
            TextBox txtAddr1 = (TextBox)dv.FindControl("txtAddr1");
            TextBox txtAddr2 = (TextBox)dv.FindControl("txtAddr2");
            TextBox txtAddr3 = (TextBox)dv.FindControl("txtAddr3");
            TextBox txtAddr4 = (TextBox)dv.FindControl("txtAddr4");
            TextBox txtPin = (TextBox)dv.FindControl("txtPin");
            TextBox txtPhone = (TextBox)dv.FindControl("txtPhone");
            DropDownList ddlpayment_Mode = (DropDownList)dv.FindControl("ddlpayment_Mode");
            TextBox txtRemarks = (TextBox)dv.FindControl("txtRemarks");
            DropDownList ddlNomination = (DropDownList)dv.FindControl("ddlNomination");
            TextBox txtDPID_No = (TextBox)dv.FindControl("txtDPID_No");
            TextBox txtDrn_No = (TextBox)dv.FindControl("txtDrn_No");
            TextBox txtISSIN_No = (TextBox)dv.FindControl("txtISSIN_No");

            TextBox txtAllotment_DT = (TextBox)dv.FindControl("txtAllotment_DT");
            DateTime dttxtAllotment_DT = new DateTime();
            if (txtAllotment_DT.Text.Trim() != "")
            {  dttxtAllotment_DT = DateTime.ParseExact(txtAllotment_DT.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture); }

            TextBox txtPan = (TextBox)dv.FindControl("txtPan");            

            string FOR15H = "";
            CheckBox chech15H = (CheckBox)dv.FindControl("CheckBox1");
            if (chech15H.Checked == true)
            { FOR15H = "Y"; }

            TextBox txtF15H_FOR_FY = (TextBox)dv.FindControl("txtF15H_FOR_FY");

            string Bank_Code = "";
            string ChequeNo = "";
            string Cheque_DT = "";
            string DraweeBank = "";
            string ArrangerName = "";
            DateTime dtCheque_DT = new DateTime();
            if (HttpContext.Current.Session["GLName"] != null)
            {
                DataTable dtGLName = (DataTable)HttpContext.Current.Session["GLName"];
                Bank_Code = dtGLName.Rows[0]["DepositeAt"].ToString();
                ChequeNo = dtGLName.Rows[0]["ChequeNo"].ToString();
                DraweeBank = dtGLName.Rows[0]["DraweeBank"].ToString();
                ArrangerName = dtGLName.Rows[0]["ArrangerName"].ToString();
                Cheque_DT = dtGLName.Rows[0]["Cheque_DT"].ToString();             
                if (Cheque_DT != "")
                { dtCheque_DT = DateTime.ParseExact(Cheque_DT, "dd/MM/yyyy", CultureInfo.InstalledUICulture); }
            }

            TextBox txtApplication_DT = (TextBox)dv.FindControl("txtApplication_DT");
            DateTime dttxtApplication_DT = new DateTime();
            if (txtApplication_DT.Text.Trim() != "")
            {  dttxtApplication_DT = DateTime.ParseExact(txtApplication_DT.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture); }          

            TextBox txtBankName = (TextBox)dv.FindControl("txtBankName");
            TextBox txtBranchName = (TextBox)dv.FindControl("txtBranchName");       
            TextBox txtAccNo = (TextBox)dv.FindControl("txtAccNo");
            TextBox txtRtgsIfsc = (TextBox)dv.FindControl("txtRtgsIfsc");

            if (txtSaveEdit.Text == "Add") //cmdSave.CommandName
            {
                DataTable dt = DBHandler.GetResult("Check_Duplicate_BondHolderID", txtDeposit_ID.Text, txtScheme_ID.Text, txtScheme_TYPE.Text, sec);
                //DataTable dt = DBHandler.GetResult("Check_Duplicate_BondHolderID", txtDeposit_ID, gblSchemeID, gblSchemeType, SectorID);
                if (dt.Rows.Count > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Bond Holder ID Already Exist');</script>");
                    txtDeposit_ID.Focus();
                }
                else
                {
                    DBHandler.Execute("Insert_Account_Deposit",
                               txtScheme_ID.Text,
                               txtScheme_TYPE.Text,
                               txtBranchID.Text,
                               txtDeposit_ID.Text,
                               txtTDSID.Text,
                               txtAppl_No.Text,
                               txtAppl_DT.Text.Equals("") ? DBNull.Value : (object)dtAppl_DT.ToShortDateString(),
                               txtAppl_Amount.Text.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(txtAppl_Amount.Text),
                                txtIssue_DT.Text.Equals("") ? DBNull.Value : (object)dtIssue_DT.ToShortDateString(),
                               txtIssue_Amount.Text.Trim().Equals("") ? Convert.ToDecimal(0) : (object)Convert.ToDecimal(txtIssue_Amount.Text),
                              Convert.ToInt32(txtQty.Text),
                               txtDistinctive_Start.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtDistinctive_Start.Text),
                               txtDistinctive_End.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtDistinctive_End.Text),
                               txtPeriod.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtPeriod.Text),
                               txtMaturity_DT.Text.Equals("") ? DBNull.Value : (object)dttxtMaturity_DT.ToShortDateString(),
                               txtRepaid_DT.Text.Equals("") ? DBNull.Value : (object)dttxtRepaid_DT.ToShortDateString(),
                               txtName.Text,
                               txtAddr1.Text,
                               txtAddr2.Text.Equals("") ? DBNull.Value : (object)txtAddr2.Text,
                               txtAddr3.Text.Equals("") ? DBNull.Value : (object)txtAddr3.Text,
                               txtAddr4.Text.Equals("") ? DBNull.Value : (object)txtAddr4.Text,
                               txtPin.Text,
                               txtPhone.Text.Equals("") ? DBNull.Value : (object)txtPhone.Text,
                               ddlpayment_Mode.SelectedValue,
                               txtRemarks.Text.Equals("") ? DBNull.Value : (object)txtRemarks.Text,
                               ddlNomination.SelectedValue,//BOOK_ID
                                txtDPID_No.Text.Equals("") ? DBNull.Value : (object)txtDPID_No.Text,
                                txtDrn_No.Text.Equals("") ? DBNull.Value : (object)txtDrn_No.Text,
                                txtISSIN_No.Text.Equals("") ? DBNull.Value : (object)txtISSIN_No.Text,
                                txtAllotment_DT.Text.Equals("") ? DBNull.Value : (object)dttxtAllotment_DT.ToShortDateString(),
                                txtPan.Text.Equals("") ? DBNull.Value : (object)txtPan.Text,
                                FOR15H.Equals("") ? DBNull.Value : (object)FOR15H,
                                txtF15H_FOR_FY.Text.Equals("") ? DBNull.Value : (object)txtF15H_FOR_FY.Text,
                                Bank_Code.Equals("") ? DBNull.Value : (object)Bank_Code,
                               ChequeNo.Equals("") ? DBNull.Value : (object)ChequeNo,
                               Cheque_DT.Equals("") ? DBNull.Value : (object)dtCheque_DT.ToShortDateString(),
                               txtApplication_DT.Text.Equals("") ? DBNull.Value : (object)dttxtApplication_DT.ToShortDateString(),
                               DraweeBank.Equals("") ? DBNull.Value : (object)DraweeBank,
                              ArrangerName.Equals("") ? DBNull.Value : (object)ArrangerName,
                              txtBankName.Text.Equals("") ? DBNull.Value : (object)txtBankName.Text,
                              txtBranchName.Text.Equals("") ? DBNull.Value : (object)txtBranchName.Text,
                             txtAccNo.Text.Equals("") ? DBNull.Value : (object)txtAccNo.Text,
                              txtRtgsIfsc.Text.Equals("") ? DBNull.Value : (object)txtRtgsIfsc.Text,

                               Session[SiteConstants.SSN_SECTOR_ID],
                               Session[SiteConstants.SSN_INT_USER_ID]);
                    cmdSave.Text = "Create";
                    dv.ChangeMode(FormViewMode.Insert);
                    //PopulateGrid();  
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
                }
            }
            else if (txtSaveEdit.Text == "Edit") //cmdSave.CommandName
            {
                //  string ID = (string)Session["ID"];
                string Scheme_ID = Session["Scheme_ID"].ToString();
                string Deposit_ID = Session["Deposit_ID"].ToString();
                DBHandler.Execute("Update_Account_Deposit", Scheme_ID, Deposit_ID,
                    //   txtScheme_ID.Text,
                    //    txtScheme_TYPE.Text,
                             txtBranchID.Text,
                    //     txtDeposit_ID.Text,
                             txtTDSID.Text,
                             txtAppl_No.Text,
                             txtAppl_DT.Text.Equals("") ? DBNull.Value : (object)dtAppl_DT.ToShortDateString(),
                             txtAppl_Amount.Text.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(txtAppl_Amount.Text),
                              txtIssue_DT.Text.Equals("") ? DBNull.Value : (object)dtIssue_DT.ToShortDateString(),
                             txtIssue_Amount.Text.Trim().Equals("") ? Convert.ToDecimal(0) : (object)Convert.ToDecimal(txtIssue_Amount.Text),
                            Convert.ToInt32(txtQty.Text),
                             txtDistinctive_Start.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtDistinctive_Start.Text),
                             txtDistinctive_End.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtDistinctive_End.Text),
                             txtPeriod.Text.Equals("") ? DBNull.Value : (object)Convert.ToInt32(txtPeriod.Text),
                             txtMaturity_DT.Text.Equals("") ? DBNull.Value : (object)dttxtMaturity_DT.ToShortDateString(),
                             txtRepaid_DT.Text.Equals("") ? DBNull.Value : (object)dttxtRepaid_DT.ToShortDateString(),
                             txtName.Text,
                             txtAddr1.Text,
                             txtAddr2.Text.Equals("") ? DBNull.Value : (object)txtAddr2.Text,
                             txtAddr3.Text.Equals("") ? DBNull.Value : (object)txtAddr3.Text,
                             txtAddr4.Text.Equals("") ? DBNull.Value : (object)txtAddr4.Text,
                             txtPin.Text,
                             txtPhone.Text.Equals("") ? DBNull.Value : (object)txtPhone.Text,
                    //  ddlpayment_Mode.SelectedValue,
                             txtRemarks.Text.Equals("") ? DBNull.Value : (object)txtRemarks.Text,
                    //  ddlNomination.SelectedValue,//BOOK_ID
                              txtDPID_No.Text.Equals("") ? DBNull.Value : (object)txtDPID_No.Text,
                              txtDrn_No.Text.Equals("") ? DBNull.Value : (object)txtDrn_No.Text,
                              txtISSIN_No.Text.Equals("") ? DBNull.Value : (object)txtISSIN_No.Text,
                              txtAllotment_DT.Text.Equals("") ? DBNull.Value : (object)dttxtAllotment_DT.ToShortDateString(),
                              txtPan.Text.Equals("") ? DBNull.Value : (object)txtPan.Text,
                              FOR15H.Equals("") ? DBNull.Value : (object)FOR15H,
                              txtF15H_FOR_FY.Text.Equals("") ? DBNull.Value : (object)txtF15H_FOR_FY.Text,
                              Bank_Code.Equals("") ? DBNull.Value : (object)Bank_Code,
                             ChequeNo.Equals("") ? DBNull.Value : (object)ChequeNo,
                             Cheque_DT.Equals("") ? DBNull.Value : (object)dtCheque_DT.ToShortDateString(),
                             txtApplication_DT.Text.Equals("") ? DBNull.Value : (object)dttxtApplication_DT.ToShortDateString(),
                             DraweeBank.Equals("") ? DBNull.Value : (object)DraweeBank,
                            ArrangerName.Equals("") ? DBNull.Value : (object)ArrangerName,
                            txtBankName.Text.Equals("") ? DBNull.Value : (object)txtBankName.Text,
                            txtBranchName.Text.Equals("") ? DBNull.Value : (object)txtBranchName.Text,
                           txtAccNo.Text.Equals("") ? DBNull.Value : (object)txtAccNo.Text,
                            txtRtgsIfsc.Text.Equals("") ? DBNull.Value : (object)txtRtgsIfsc.Text,

                            Session[SiteConstants.SSN_SECTOR_ID],
                             Session[SiteConstants.SSN_INT_USER_ID]);

                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                dv.DataBind();
                txtSaveEdit.Text = "Add";
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    protected void Edit(object sender, GridViewCommandEventArgs e)
    {
        using (GridViewRow row = (GridViewRow)((LinkButton)sender).Parent.Parent)
        {
            try
            {
                string[] cmar = e.CommandArgument.ToString().Split('/');
                if (e.CommandName == "Select")
                {
                    dv.ChangeMode(FormViewMode.Edit);
                    UpdateDeleteRecord(1, cmar[1].ToString(), cmar[0].ToString());
                    Button cmdSave = (Button)dv.FindControl("cmdSave");
                    cmdSave.CommandName = "Edit";
                    cmdSave.ToolTip = "Update";
                    cmdSave.Text = "Update";
                }
                else if (e.CommandName == "Del")
                {
                    UpdateDeleteRecord(2, cmar[1].ToString(), cmar[0].ToString());
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
            }
            //txtCustomerID.ReadOnly = true;
            //txtCustomerID.Text = row.Cells[0].Text;
            //txtContactName.Text = row.Cells[1].Text;
            //txtCompany.Text = row.Cells[2].Text;
            //popup.Show();
        }
    }


    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            string[] cmar=e.CommandArgument.ToString().Split('/');
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1,cmar[1].ToString(),cmar[0].ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, cmar[1].ToString(), cmar[0].ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void btnSearchCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid(string SchemeID)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Account_Deposit", Convert.ToInt64(SchemeID));
            if (dt.Rows.Count > 0)
            { 
            tbl.DataSource = dt;
            tbl.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    //==================================================================Voucher Window====================================================================//
    private void GridIndentGLName()
    {
        DataTable Indant = new DataTable();
        Indant.Columns.Add("SL_ID");
        Indant.Columns.Add("OLD_SL_ID");
        Indant.Columns.Add("GL_NAME");
        Indant.Rows.Add();
        GridViewGLName.DataSource = Indant;
        GridViewGLName.DataBind();
    }

    [WebMethod]
    public static UnitGLcode[] GET_GLName()
    {
        int UserID;
       
        List<UnitGLcode> Detail = new List<UnitGLcode>();
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_GL_Name", UserID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            UnitGLcode DataObj = new UnitGLcode();
            DataObj.SL_ID = dtRow["SL_ID"].ToString();
            DataObj.OLD_SL_ID = dtRow["OLD_SL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();

            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class UnitGLcode //Class for binding data
    {
        public string SL_ID { get; set; }
        public string OLD_SL_ID { get; set; }
        public string GL_NAME { get; set; }

    }
    //==================================================================================================Scheme======//
    private void GridIndentScheme()
    {
        DataTable Indant = new DataTable();
        Indant.Columns.Add("SCHEME_ID");
        Indant.Columns.Add("SCHEME_NAME");
        Indant.Columns.Add("OLD_SCHEME_ID");
        
        Indant.Columns.Add("SCHEME_FROM");
        Indant.Columns.Add("REPAYMENT_MODE");
        Indant.Columns.Add("SLR_NSLR");
        Indant.Columns.Add("PERIOD_DAYS");
        Indant.Columns.Add("SCHEME_TYPE");
        Indant.Columns.Add("ISIN_NO");
        Indant.Columns.Add("DPID_NO");
        Indant.Rows.Add();
        GridViewSchemeName.DataSource = Indant;
        GridViewSchemeName.DataBind();
    }
   
    [WebMethod]
    public static UnitSchemecode[] GET_Scheme()
    {
        int UserID;
        List<UnitSchemecode> Detail = new List<UnitSchemecode>();
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_BOND_SCHEME", UserID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            UnitSchemecode DataObj = new UnitSchemecode();
            DataObj.SCHEME_ID = dtRow["SCHEME_ID"].ToString();
            DataObj.OLD_SCHEME_ID = dtRow["OLD_SCHEME_ID"].ToString();
            DataObj.SCHEME_NAME = dtRow["SCHEME_NAME"].ToString();
            DataObj.SCHEME_FROM = dtRow["SCHEME_FROM"].ToString();
            DataObj.REPAYMENT_MODE = dtRow["REPAYMENT_MODE"].ToString();
            DataObj.SLR_NSLR = dtRow["SLR_NSLR"].ToString();
            DataObj.PERIOD_DAYS = dtRow["PERIOD_DAYS"].ToString();
            DataObj.SCHEME_TYPE = dtRow["SCHEME_TYPE"].ToString();
            DataObj.ISIN_NO = dtRow["ISIN_NO"].ToString();
            DataObj.DPID_NO = dtRow["DPID_NO"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }

    public class UnitSchemecode //Class for binding data
    {
        public string SCHEME_ID { get; set; }
        public string OLD_SCHEME_ID { get; set; }
        public string SCHEME_NAME { get; set; }
        public string SCHEME_FROM { get; set; }
        public string REPAYMENT_MODE { get; set; }
        public string SLR_NSLR { get; set; }
        public string PERIOD_DAYS { get; set; }
        public string SCHEME_TYPE { get; set; }
        public string ISIN_NO { get; set; }
        public string DPID_NO { get; set; }
    }

    [WebMethod]
    //==================================================================Branch/Sector====================================================================//
    private void GridIndentSector()
    {
        DataTable Indant = new DataTable();
        Indant.Columns.Add("SectorID");  
        Indant.Columns.Add("SectorCode");
        Indant.Columns.Add("SectorName");
        Indant.Rows.Add();
        GridViewSectorName.DataSource = Indant;
        GridViewSectorName.DataBind();
    }

    [WebMethod]
    public static UnitSectorcode[] GET_Sector()
    {
        int UserID;
        List<UnitSectorcode> Detail = new List<UnitSectorcode>();
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_Sector_Name", UserID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            UnitSectorcode DataObj = new UnitSectorcode();
            DataObj.SectorID = dtRow["SectorID"].ToString();
            DataObj.SectorCode = dtRow["SectorCode"].ToString();
            DataObj.SectorName = dtRow["SectorName"].ToString();

            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }

    public class UnitSectorcode //Class for binding data
    {
        public string SectorID { get; set; }
        public string SectorCode { get; set; }
        public string SectorName { get; set; }

    }
    //==================================================Voucher Window========================================================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SET_GLNameOnSession(string ChequeNo, string Cheque_DT, string DraweeBank, string ArrangerName, string DepositeAt)
    {     

        string JSONVal = "";
        try
        {
            if (HttpContext.Current.Session["GLName"] != null)
            {
                DataTable dtdetail = (DataTable)HttpContext.Current.Session["GLName"];
                if (dtdetail.Rows.Count > 0)
                {
                    //DataRow dtrow = dtdetail.NewRow();
                    //dtrow["ChequeNo"] = ChequeNo;
                    //dtrow["Cheque_DT"] = Cheque_DT;
                    //dtrow["DraweeBank"] = DraweeBank;
                    //dtrow["ArrangerName"] = ArrangerName;
                    //dtrow["DepositeAt"] = DepositeAt;
                    //dtdetail.Rows.Add(dtrow);
                    //HttpContext.Current.Session["IssueDetails"] = dtdetail;
                    dtdetail.Rows[0]["ChequeNo"]=ChequeNo;
                    dtdetail.Rows[0]["Cheque_DT"]=Cheque_DT;
                    dtdetail.Rows[0]["DraweeBank"]=DraweeBank;
                    dtdetail.Rows[0]["ArrangerName"]=ArrangerName;
                    dtdetail.Rows[0]["DepositeAt"] = DepositeAt;
                    HttpContext.Current.Session["GLName"] = dtdetail;
                    JSONVal = "Update";
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ChequeNo");
                dt.Columns.Add("Cheque_DT");
                dt.Columns.Add("DraweeBank");
                dt.Columns.Add("ArrangerName");
                dt.Columns.Add("DepositeAt");

                DataRow dtrow = dt.NewRow();
                dtrow["ChequeNo"] = ChequeNo;
                dtrow["Cheque_DT"] = Cheque_DT;
                dtrow["DraweeBank"] = DraweeBank;
                dtrow["ArrangerName"] = ArrangerName;
                dtrow["DepositeAt"] = DepositeAt;

                dt.Rows.Add(dtrow);
                HttpContext.Current.Session["GLName"] = dt;
                JSONVal = "insert";
            }
            //string result = "success";
            //JSONVal = result.ToJSON();
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }       
    //===========================================TDS===========================================================================//
    private void GridIndentTds()
    {
        DataTable Indant = new DataTable();
        Indant.Columns.Add("TDS_ID");
        Indant.Columns.Add("TDS_DESC");
        Indant.Rows.Add();
        GridViewTds.DataSource = Indant;
        GridViewTds.DataBind();
    }

    [WebMethod]
    public static UnitTdscode[] GET_TdsName()
    {
        int UserID;
        List<UnitTdscode> Detail = new List<UnitTdscode>();
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_BOND_TDS", UserID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            UnitTdscode DataObj = new UnitTdscode();
            DataObj.TDS_ID = dtRow["TDS_ID"].ToString();
            DataObj.TDS_DESC = dtRow["TDS_DESC"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }

    public class UnitTdscode //Class for binding data
    {
        public string TDS_ID { get; set; }
        public string TDS_DESC { get; set; }

    }

    [WebMethod]
    public static string GET_FinancialYear(string CurrentDate)
    {
        string finYear = "";
        try
        {        
            DateTime CurDate = DateTime.ParseExact(CurrentDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            
            DataTable dt = DBHandler.GetResult("Get_FIN_YEAR", CurDate.ToShortDateString());
             //DataTable dt = DBHandler.GetDirectResult(query);
            if (dt.Rows.Count > 0)
            {
                finYear = dt.Rows[0]["FinYear"].ToString();
            }          
        }
        catch(Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return finYear;
    }

        [WebMethod]
    public static string GET_Deposit_ID(string ValSchemeID,string ValSchemeType,string SectorID)
    {
        string Deposit_ID = "";
        try
        {             
            DataTable dt = DBHandler.GetResult("GET_Deposit_ID_BondMaster", ValSchemeID, ValSchemeType,Convert.ToInt32(SectorID));
            
            if (dt.Rows.Count > 0)
            {
                Deposit_ID = dt.Rows[0]["DepositID"].ToString();
            }          
        }
        catch(Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Deposit_ID;
    }


        [WebMethod]
        public static string[] GET_AutoComplete_SchemeID(string SchemeID)
        {
            List<string> Detail = new List<string>();
            DataTable dt = DBHandler.GetResult("Get_SEARCH_SCHEMEID", SchemeID);
            foreach (DataRow dtRow in dt.Rows)
            {
                Detail.Add(dtRow["OLD_SCHEME_ID"].ToString()  + "|" + dtRow["SCHEME_ID"].ToString());
            }
            return Detail.ToArray();
        }

        [WebMethod]
        public static string[] GET_AutoComplete_Name(string BoundName)
        {
            List<string> Detail = new List<string>();
            DataTable dt = DBHandler.GetResult("Get_SEARCH_BOUNDNAME", BoundName);
            foreach (DataRow dtRow in dt.Rows)
            {
                Detail.Add(dtRow["APPLICANT_NAME"].ToString() + "|" + dtRow["SCHEME_ID"].ToString());
            }
            return Detail.ToArray();
            //+ "|" + dtRow["OLD_SCHEME_ID"].ToString()
        }
            
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BondRecords[] GET_FillGrid(string SchemeID, string BondName)
        {
            int UserID;
            List<BondRecords> Detail = new List<BondRecords>();
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);

            DataTable dtGetData = DBHandler.GetResult("Get_Account_Deposit", Convert.ToString(SchemeID), Convert.ToString(BondName));

            foreach (DataRow dtRow in dtGetData.Rows)
            {  
                BondRecords DataObj = new BondRecords();
                DataObj.SchemeID = dtRow["OLD_SCHEME_ID"].ToString();
                DataObj.IssueDate = dtRow["ISSUE_DT"].ToString();
                DataObj.Qty = dtRow["QTY"].ToString();
                DataObj.IssueAmount = dtRow["ISSU_AMOUNT"].ToString();
                DataObj.Name = dtRow["APPLICANT_NAME"].ToString();
                DataObj.ADDR = dtRow["ADDR"].ToString();
                DataObj.MATURITY_DT = dtRow["MATURITY_DT"].ToString();
                DataObj.DEPOSITSCHEME_ID = dtRow["DEPOSITSCHEME_ID"].ToString();
                DataObj.DepositID = dtRow["DEPOSIT_ID"].ToString();
                DataObj.SchemeCode = dtRow["SCHEME_ID"].ToString();
                DataObj.SchemeType = dtRow["SCHEME_TYPE"].ToString();
                

                Detail.Add(DataObj);
            }

            return Detail.ToArray();
        }

        public class BondRecords //Class for binding data
        {
            public string SchemeID { get; set; }
            public string IssueDate { get; set; }
            public string Qty { get; set; }
            public string IssueAmount { get; set; }
            public string Name { get; set; }
            public string ADDR { get; set; }
            public string MATURITY_DT { get; set; }
            public string DEPOSITSCHEME_ID { get; set; }
            public string DepositID { get; set; }
            public string SchemeCode { get; set; }
            public string SchemeType { get; set; }
            
        }


        [WebMethod]
    
        public static BoundData[] GET_ShowBond(string DepositID, string SchemeID, string SchType)
        {
            List<BoundData> Detail = new List<BoundData>();
            DataTable dtResult = DBHandler.GetResult("Acc_Deposit_ByID", Convert.ToString(SchemeID), Convert.ToString(DepositID), Convert.ToString(SchType));

            foreach (DataRow dtRow in dtResult.Rows)
            {
                BoundData DataObj = new BoundData();
                DataObj.SCHEME_ID = dtRow["SCHEME_ID"].ToString();
                DataObj.SectorName = dtRow["SectorName"].ToString();
                DataObj.SCHEME_NAME = dtRow["SCHEME_NAME"].ToString();
                DataObj.OLD_SCHEME_ID = dtRow["OLD_SCHEME_ID"].ToString();
                DataObj.SCHEME_TYPE = dtRow["SCHEME_TYPE"].ToString();
                DataObj.BRANCH_ID = dtRow["BRANCH_ID"].ToString();
                DataObj.TDS_ID = dtRow["TDS_ID"].ToString();
                DataObj.TDS_DESC = dtRow["TDS_DESC"].ToString();
                DataObj.APPL_NO = dtRow["APPL_NO"].ToString();
                DataObj.APPL_DT = dtRow["APPL_DT"].ToString();
                DataObj.APPL_AMOUNT = dtRow["APPL_AMOUNT"].ToString();
                DataObj.ISSUE_DT = dtRow["ISSUE_DT"].ToString();
                DataObj.DEPOSIT_ID = dtRow["DEPOSIT_ID"].ToString();
                DataObj.ISSU_AMOUNT = dtRow["ISSU_AMOUNT"].ToString();
                DataObj.QTY = dtRow["QTY"].ToString();
                DataObj.DISTINCTIVE_START = dtRow["DISTINCTIVE_START"].ToString();
                DataObj.DISTINCTIVE_END = dtRow["DISTINCTIVE_END"].ToString();
                DataObj.PERIOD_DAYS = dtRow["PERIOD_DAYS"].ToString();
                DataObj.MATURITY_DT = dtRow["MATURITY_DT"].ToString();
                DataObj.REPAID_DT = dtRow["REPAID_DT"].ToString();
                DataObj.APPLICANT_NAME = dtRow["APPLICANT_NAME"].ToString();
                DataObj.ADDR1 = dtRow["ADDR1"].ToString();
                DataObj.ADDR2 = dtRow["ADDR2"].ToString(); 
                DataObj.ADDR3 = dtRow["ADDR3"].ToString();
                DataObj.ADDR4 = dtRow["ADDR4"].ToString();
                DataObj.PIN = dtRow["PIN"].ToString();
                DataObj.PHONE = dtRow["PHONE"].ToString();
                DataObj.PAYMENT_MODE = dtRow["PAYMENT_MODE"].ToString();
                DataObj.REMARKS = dtRow["REMARKS"].ToString();
                DataObj.NOMINATION = dtRow["NOMINATION"].ToString();
                DataObj.DPID_NO = dtRow["DPID_NO"].ToString();
                DataObj.DRN_NO = dtRow["DRN_NO"].ToString();
                DataObj.ISIN_NO = dtRow["ISIN_NO"].ToString();
                DataObj.PAN = dtRow["PAN"].ToString();
                DataObj.FORM15H = dtRow["FORM15H"].ToString();
                DataObj.F15H_FOR_FY = dtRow["F15H_FOR_FY"].ToString();
                DataObj.BANK_CD = dtRow["BANK_CD"].ToString();
                DataObj.CHQ_NO = dtRow["CHQ_NO"].ToString();
                DataObj.CHQ_DT = dtRow["CHQ_DT"].ToString();
                DataObj.ALLOTMENT_DT = dtRow["ALLOTMENT_DT"].ToString();
                DataObj.DRAWEE_BANK = dtRow["DRAWEE_BANK"].ToString();
                DataObj.ARRANGER_NAME = dtRow["ARRANGER_NAME"].ToString();
                DataObj.BRANCH_NAME = dtRow["BRANCH_NAME"].ToString();
                DataObj.BANK_NAME = dtRow["BANK_NAME"].ToString();
                DataObj.APPLICATION_DATE = dtRow["APPLICATION_DATE"].ToString();
                DataObj.BANK_ACC_NO = dtRow["BANK_ACC_NO"].ToString();
                DataObj.RTGS_IFSC_NO = dtRow["RTGS_IFSC_NO"].ToString();
                DataObj.SCHEME_FROM = dtRow["SCHEME_FROM"].ToString();

                System.Web.HttpContext.Current.Session["Scheme_ID"]= dtRow["SCHEME_ID"].ToString();
                 System.Web.HttpContext.Current.Session["Deposit_ID"]= dtRow["DEPOSIT_ID"].ToString();

                Detail.Add(DataObj);
            }

            return Detail.ToArray();
        }

        public class BoundData //Class for binding data
        {
            public string SCHEME_ID { get; set; }
            public string SectorName { get; set; }
            public string SCHEME_NAME { get; set; }
            public string OLD_SCHEME_ID { get; set; }
            public string SCHEME_TYPE { get; set; }
            public string BRANCH_ID { get; set; }
            public string TDS_ID { get; set; }
            public string TDS_DESC { get; set; }
            public string APPL_NO { get; set; }
            public string APPL_DT { get; set; }
            public string APPL_AMOUNT { get; set; }
            public string ISSUE_DT { get; set; }
            public string DEPOSIT_ID { get; set; }
            public string ISSU_AMOUNT { get; set; }
            public string QTY { get; set; }
            public string DISTINCTIVE_START { get; set; }
            public string DISTINCTIVE_END { get; set; }
            public string PERIOD_DAYS { get; set; }
            public string MATURITY_DT { get; set; }
            public string REPAID_DT { get; set; }
            public string APPLICANT_NAME { get; set; }
            public string ADDR1 { get; set; }
            public string ADDR2 { get; set; }
            public string ADDR3 { get; set; }
            public string ADDR4 { get; set; }
            public string PIN { get; set; }
            public string PHONE { get; set; }
	        public string PAYMENT_MODE { get; set; }
            public string REMARKS { get; set; }
            public string NOMINATION { get; set; }
            public string DPID_NO { get; set; }
            public string DRN_NO { get; set; }
            public string ISIN_NO { get; set; }
            public string PAN { get; set; }
            public string FORM15H { get; set; }
            public string F15H_FOR_FY { get; set; }
            public string BANK_CD { get; set; }
            public string CHQ_NO { get; set; }
            public string CHQ_DT { get; set; }
            public string ALLOTMENT_DT { get; set; }
            public string DRAWEE_BANK { get; set; }
            public string ARRANGER_NAME { get; set; }
            public string BRANCH_NAME { get; set; }
            public string BANK_NAME { get; set; }
            public string APPLICATION_DATE { get; set; }
            public string BANK_ACC_NO { get; set; }
            public string RTGS_IFSC_NO { get; set; }
            public string SCHEME_FROM { get; set; }
	       
        }


        
        [WebMethod]
        public static string[] GET_Delete_Bond(string DepositID, string SchemeID, string SchType)
        {
            List<string> Detail = new List<string>();
            DataTable dtResult = DBHandler.GetResult("Delete_Acc_Deposit_ID", Convert.ToString(SchemeID), Convert.ToString(DepositID), Convert.ToString(SchType));
            Detail.Add("Record Delete Successfully");
            return Detail.ToArray();
        }

        [WebMethod]
        public static string Check_Deposit_ID(string txtDeposit_ID, string gblSchemeID, string gblSchemeType, string SectorID)
        {
            string value = "";
            try
            {
                DataTable dt = DBHandler.GetResult("Check_Duplicate_BondHolderID", txtDeposit_ID, gblSchemeID, gblSchemeType, SectorID);
                if (dt.Rows.Count > 0)
                {
                    value = "0";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return value;
        }

}
