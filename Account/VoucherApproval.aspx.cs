﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Data.SqlClient;


public partial class VoucherApproval : System.Web.UI.Page
{
    public string result="";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            panGrd.Visible = false;
           
            if (!IsPostBack)
            {
                GetuserName();
                TextBox txtAprvdBy = (TextBox)dv.FindControl("txtAprvdBy");
                txtAprvdBy.Enabled = true;
                DataRow drRow = (DataRow)(Session[SiteConstants.SSN_DR_USER_DETAILS]);
                txtAprvdBy.Text = drRow["Name"].ToString();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void GetuserName()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_ServerDate_Pf");
            if (dt.Rows.Count > 0) {
                string Currentdate = dt.Rows[0]["ServerDate"].ToString();
                TextBox txtAprvdON = (TextBox)dv.FindControl("txtAprvdON");
                txtAprvdON.Text = Currentdate;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }


    //protected DataTable drpload()
    //{
    //    DataTable dt = DBHandler.GetResult("Get_SectorForGlSLTrial");
    //    return dt;
    //}

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                string[] arg = new string[4];
                arg = e.CommandArgument.ToString().Split(';');
                int VoucherNo = Convert.ToInt32(arg[0].ToString());
                string Yearmon = arg[1];
                int VoucherType = Convert.ToInt32(arg[2].ToString());
                int SectorID = Convert.ToInt32(arg[3].ToString());
                ShowVoucherDetail(VoucherNo, Yearmon, VoucherType, SectorID);
               
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void ShowVoucherDetail(int VCH_No, string Year_Mon, int VCH_Type, int Sector_ID)
    {
        try
        {
            DataTable dtResult = DBHandler.GetResult("Get_VoucherApprovalDetail_Pf", VCH_No, Year_Mon, VCH_Type, Sector_ID, HttpContext.Current.Session["Menu_For"].ToString());
            if (dtResult.Rows.Count > 0)
            {
                panGrd.Visible = true;
                grdVchDetail.DataSource = dtResult;
                grdVchDetail.DataBind();
                PopulateSummery(Year_Mon,VCH_Type.ToString());
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Enter Correct Year month(YYYYMM) and Voucher Type. No Voucher found for this Year month and Voucher Type')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateSummery(string yearmon, string VchType) 
    {
        try
        {
            TextBox frmdate = (TextBox)dv.FindControl("txtFrmDate");
            TextBox todate = (TextBox)dv.FindControl("txtToDate");

            DateTime dtFromDate = new DateTime();
            DateTime dtToDate = new DateTime();
            if (frmdate.Text != "")
            {
                dtFromDate = DateTime.ParseExact(frmdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (todate.Text != "")
            {
                dtToDate = DateTime.ParseExact(todate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }

            DataRow drSum = DBHandler.GetResult("Acc_Vch_cnt_Pf", yearmon, VchType, dtFromDate.ToShortDateString(),
                                                dtToDate.ToShortDateString(), Session[SiteConstants.SSN_SECTOR_ID].ToString(),
                                                HttpContext.Current.Session["Menu_For"].ToString()).Rows[0];
            result = drSum["VchSummery"].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGrid(string yearmon, string VchType, string frmdate, string todate)
    {
        try
        {
            DateTime dtFromDate = new DateTime();
            DateTime dtToDate = new DateTime();
            if (frmdate != "")
            {
                dtFromDate = DateTime.ParseExact(frmdate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            if (todate != "")
            {
                dtToDate = DateTime.ParseExact(todate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
                   

            DataTable dt = DBHandler.GetResult("Get_VoucherApprovalByVCHNo_Pf",
                                            yearmon.Equals("") ? DBNull.Value : (object)yearmon,
                                            VchType.Equals("") ? DBNull.Value : (object)Convert.ToInt32(VchType),
                                            frmdate.Equals("") ? DBNull.Value : (object)dtFromDate.ToShortDateString(),
                                            todate.Equals("") ? DBNull.Value : (object)dtToDate.ToShortDateString(),
                                            Session[SiteConstants.SSN_SECTOR_ID].ToString(),
                                            HttpContext.Current.Session["Menu_For"].ToString());

            if (dt.Rows.Count > 0)
            {
                panGrd.Visible = true;
                tbl.DataSource = dt;
                tbl.DataBind();
                PopulateSummery(yearmon, VchType);
            }
            else
            {
                panGrd.Visible = false;
                clearAll();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Enter Correct Year month(YYYYMM) and Voucher Type. No Voucher found for this Year month and Voucher Type.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "search")
            {

                TextBox txtYearmonth = (TextBox)dv.FindControl("txtYearmonth");
                DropDownList ddlVoucharType = (DropDownList)dv.FindControl("ddlVoucharType");

                TextBox txtFrmDate = (TextBox)dv.FindControl("txtFrmDate");
                TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
                

                PopulateGrid( txtYearmonth.Text, ddlVoucharType.SelectedValue, txtFrmDate.Text, txtToDate.Text);
                grdVchDetail.DataSource = null;
                grdVchDetail.DataBind();
            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdPrint_Click(object sender,EventArgs evt)
    {
        try
        {

            
            String Msg = "";
            string StrFormula = "";
            string FormName = "", ReportName = "", ReportType = "";
            FormName="VoucherApproval.aspx";
            ReportName="mst_acc_voucher_rpt";
            ReportType="Voucher Approval";
            int UserID;
            int SectorID;
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;

            TextBox txtYearmonth = (TextBox)dv.FindControl("txtYearmonth");
            DropDownList ddlVoucharType = (DropDownList)dv.FindControl("ddlVoucharType");

            TextBox txtFrmDate = (TextBox)dv.FindControl("txtFrmDate");
            TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");

            StrFormula = "";

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, SectorID, txtYearmonth.Text, ddlVoucharType.SelectedValue,Convert.ToDateTime(txtFrmDate.Text).ToString("yyyy-MM-dd"),Convert.ToDateTime(txtToDate.Text).ToString("yyyy-MM-dd"), "", "", "", "", "");

            //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('ReportView.aspx','_newtab');", true);
            Context.Response.Write("<script language='javascript'>window.open('ReportView.aspx','_newtab');</script>");

        }
            catch(Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    

    protected void cmdApprove_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdApprove = (Button)sender;
            if (cmdApprove.CommandName == "Approve")
            {
                int cnt = 0;
                if (tbl.Rows.Count > 0)
                {
                    for (int j = 0; j < tbl.Rows.Count; j++)
                    {
                        CheckBox chkApprove = (tbl.Rows[j].Cells[6].FindControl("chkApprove") as CheckBox);
                        if (chkApprove.Checked)
                        {
                            cnt++;
                        }
                    }

                    if (cnt == 0)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please check at least one Voucher for approval.')</script>");
                        return;
                    }

                    cnt = 0;

                    for (int i = 0; i < tbl.Rows.Count; i++)
                    {
                        string yearmon = "";
                        string VoucherNo = "";
                        string VoucherType = "";
                        string SectorID = "";
                        string VoucherDate = "";
                        string chk = "N";
                        
                        CheckBox chkApprove = (tbl.Rows[i].Cells[6].FindControl("chkApprove") as CheckBox);
                        if (chkApprove.Checked)
                        {
                            chk = "Y";
                        }
                             yearmon = tbl.DataKeys[i].Values[0].ToString();
                             VoucherNo = tbl.DataKeys[i].Values[1].ToString();
                             VoucherType = tbl.DataKeys[i].Values[2].ToString();
                             SectorID = tbl.DataKeys[i].Values[3].ToString();
                             VoucherDate = tbl.Rows[i].Cells[3].Text;


                             DateTime dtVoucherDate = DateTime.ParseExact(VoucherDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                             DBHandler.Execute("Update_VoucherApproval_Pf", yearmon, Convert.ToInt32(VoucherType), Convert.ToInt32(VoucherNo), dtVoucherDate.ToShortDateString(),
                             Convert.ToInt32(SectorID), chk.Equals("Y") ? 'N' : 'Y', Session[SiteConstants.SSN_INT_USER_ID].ToString());
                        //chk = "N";
                    }
                }  
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No record found.')</script>");
                }

                clearAll();
                
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Voucher Approved Successfully.')</script>");
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void clearAll()
    {
        try
        {
            TextBox txtYearmonth = (TextBox)dv.FindControl("txtYearmonth");
            TextBox txtFrmDate = (TextBox)dv.FindControl("txtFrmDate");
            TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
            DropDownList ddlVoucharType = (DropDownList)dv.FindControl("ddlVoucharType");
            tbl.DataSource = null;
            tbl.DataBind();
            grdVchDetail.DataSource = null;
            grdVchDetail.DataBind();
            panGrd.Visible = false;   
            //txtYearmonth.Text = "";
            //ddlVoucharType.SelectedValue = "";
            //txtFrmDate.Text = "";
            //txtToDate.Text = "";


        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdCancelSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_YearMonth(int SecID)
    {
        string JSONVal = string.Empty;
        DataTable dtDetails = (DataTable)HttpContext.Current.Session["AccGlDetail"];
            try
            {
                DataTable dtLoaneeData = DBHandler.GetResult("GET_MaxYearMonth_Pf", SecID, HttpContext.Current.Session["Menu_For"].ToString());
                foreach (DataRow dtRow in dtLoaneeData.Rows)
                {
                    JSONVal = dtRow["YEAR_MONTH"].ToString();
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session["AccGlDetail"] = null;
            }
        
        return JSONVal;
    }

}


