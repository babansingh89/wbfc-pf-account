﻿using System;
using Common;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Configuration;
using System.Web.Services;

public partial class ReportingHiearchy : System.Web.UI.Page
{
    DataTable dtMenu;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            populateTreeView();
            GridEmployee();
        }
    }

    private void GridEmployee()
    {
        DataTable Employee = new DataTable();
        Employee.Columns.Add("EmpNo");
        Employee.Columns.Add("EmpName");
        Employee.Rows.Add();
        GridViewEmployeeList.DataSource = Employee;
        GridViewEmployeeList.DataBind();
    }
    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                //PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox txtEmployeeNo = (TextBox)dv.FindControl("txtEmployeeNo");
            TextBox txtEmpName = (TextBox)dv.FindControl("txtEmpName");
            TextBox txtReportingNo = (TextBox)dv.FindControl("txtReportingNo");
            TextBox txtReportingName = (TextBox)dv.FindControl("txtReportingName");
            TextBox txtRank = (TextBox)dv.FindControl("txtRank");

            if (txtReportingNo.Text != "")
            {
                if (txtEmployeeNo.Text != "")
                {
                    DataTable dt = DBHandler.GetResult("Insert_EmployeeReporting", txtEmployeeNo.Text, txtReportingNo.Text, txtRank.Text, HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                    if (dt.Rows.Count > 0)
                    {
                        string result = dt.Rows[0]["Result"].ToString();
                        if (result == "Inserted")
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record is inserted Successfully.')</script>");
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Sorry ! This Employee is already available in this Reporting Employee.')</script>");
                        }
                    }
                   
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Insert Employee No.')</script>");
                    txtEmployeeNo.Focus();
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Insert Reporting Employee No.')</script>");
                txtReportingNo.Focus();
            }


        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void populateTreeView()
    {

        dtMenu = DBHandler.GetResult("Get_ParentReportingManager");
        foreach (DataRow dr in dtMenu.Rows)
        {
            TreeNode node = new TreeNode();
            node.Text = dr["EmpName"].ToString();
            node.Value = dr["ParentEmployeeID"].ToString();
            //node.NavigateUrl = dr["PageName"].ToString();
            AddNodesModel(node);
            treeViews.Nodes.Add(node);
        }
    }
    protected void AddNodesModel(TreeNode node)
    {
        DataTable dt = new DataTable();
        dt = DBHandler.GetResult("Get_EmployeeUnderReportingManager", Convert.ToInt32(node.Value));
        foreach (DataRow row in dt.Rows)
        {
            TreeNode nNode = new TreeNode();
            nNode.Value = row["EmployeeID"].ToString();
            nNode.Text = row["EmpName"].ToString();
            //nNode.NavigateUrl = row["PageName"].ToString();
            //AddNodesGroup(nNode, node);
            node.ChildNodes.Add(nNode);
        }
    }


    [WebMethod]
    public static ClsEmployee[] GET_EmployeeList(string EmpNo)
    {
        int UserID;
        List<ClsEmployee> Detail = new List<ClsEmployee>();
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("GET_ShowEmployee", EmpNo);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ClsEmployee DataObj = new ClsEmployee();
            DataObj.EmpNo = dtRow["EmpNo"].ToString();
            DataObj.EmpName = dtRow["EmpName"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class ClsEmployee //Class for binding data
    {
        public string EmpNo { get; set; }
        public string EmpName { get; set; }
    }

    [WebMethod]
    public static string GET_EmployeeRank(string EmpNo)
    {
        string JSONVal = "";
        try
        {
            DataTable dt = DBHandler.GetResult("Get_EmployeeRank", EmpNo);
            if (dt.Rows.Count > 0)
            {
                JSONVal = dt.Rows[0]["Count"].ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }
}