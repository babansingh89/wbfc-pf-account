﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="PaymentReceived.aspx.cs" Inherits="PaymentReceived" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/PaymentReceived.js?v=2"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#txtDatePay').datepicker().datepicker('setDate', '<%= (pay_current_Date = string.IsNullOrEmpty(pay_current_Date) ? "" : pay_current_Date)%>');
            $('#txtDatePay').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            $("#imgDate").click(function () {
                $('#txtDatePay').datepicker("show");
                return false;
            });


        });

        //function beforeSave() {
        //    $("#frmEcom").validate();
        //    $("#txtLoaneeSearch").rules("add", { required: true, messages: { required: "Please select a Loanee." } });
        //    $("#txtBank").rules("add", { required: true, messages: { required: "Please select a Bank." } });
        //    $("#txtAmount").rules("add", { required: true, messages: { required: "Please enter total Amount." } });
        //    $("#txtDatePay").rules("add", { required: true, messages: { required: "Select a Date" } });
        //    $("#txtDraweeBank").rules("add", { required: true, messages: { required: "Please enter a Drawee Bank." } });
        //    $("#ddlInstrumentType").rules("add", { required: true, messages: { required: "Please select a Instrument Type." } });
        //    $("#txtNo").rules("add", { required: true, messages: { required: "Select enter a No." } });

        //    $("#ddlRepaymentTerm").rules("add", { required: true, messages: { required: "Settlement Type can not be blank." } });
        //    $("#txtHealthCode").rules("add", { required: true, messages: { required: "Health Code can not be blank." } });

        //    if ($("#hdnLoaneeID").val() == "") {
        //        alert("Please select a correct Loanee from dropdown.");
        //        $("#txtLoaneeSearch").val('');
        //    }
        //    if ($("#hdnBankID").val() == "") {
        //        alert("Please select a correct Bank from dropdown.");
        //        $("#txtBank").val('');
        //    }
        //}

    </script>
    <style type="text/css">
        .rightAlign {
            text-align: right;
        }

        .hiddencol {
            display: none;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .vs  {
          pointer-events: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Payment Received</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td align="left">Payment Received Search</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="panSearch" runat="server">
                        <table width="98%" align="center">
                            <tr>
                                <td class="labelCaption">Year & Month(YYYYMM) &nbsp;&nbsp;<span class="require">*</span> </td>
                                <td class="labelCaption">:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtYearmonth" autocomplete="off" Width="45" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    <asp:TextBox ID="txtText" autocomplete="off" Width="800" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestSearch"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td colspan="9">
                                    <div style="padding-left: 850px">
                                        <asp:Button ID="cmdShow" runat="server" Text="Search" CommandName="Add" Height="30" autocomplete="off"
                                            Width="70px" CssClass="save-button DefaultButton"
                                            OnClick="cmdshow_Click" />
                                        <asp:Button ID="cmdShowCancel" runat="server" Text="Refresh" Height="30" autocomplete="off"
                                            Width="70px" CssClass="save-button DefaultButton" OnClick="cmdShowCancel_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="10" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td>
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td align="left">Payment Received Entry</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Loanee Code &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtLoaneeSearch" autocomplete="off" PlaceHolder="---(Select Loanee Code)---" Width="370px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestLoaneeCode"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Settlement Type  &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlRepaymentTerm" autocomplete="off" Width="170px" Height="25px" CssClass="inputbox2, vs" AutoPostBack="false" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="(Select Settlement Type)" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Standerd" Value="S"></asp:ListItem>
                                            <asp:ListItem Text="NPA" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="OTS" Value="O"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Health Code &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtHealthCode" autocomplete="off" Width="100px" ClientIDMode="Static" runat="server" CssClass="inputbox2, vs"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Bank &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtBank" autocomplete="off" PlaceHolder="---(Select Bank)---" Width="370px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestBank"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Total Amount &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtAmount" autocomplete="off" Width="120px" ClientIDMode="Static" OnTextChanged="txtAmountTextChanged" AutoPostBack="true" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Date &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtDatePay" autocomplete="off" Width="70px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                    <%--<td>
                                        <asp:Button ID="btnCreate" runat="server" Text="Create" CommandName="create"
                                            Width="90px" Height="40px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                            OnClick="cmdCreate_Click" />
                                        <asp:Button ID="cmdRefreshSearch" runat="server" Text="Refresh"
                                            Width="90px" Height="40px" CssClass="save-button DefaultButton" OnClick="cmdRefreshSearch_Click" OnClientClick='javascript: return unvalidate()' />
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Drawee Bank &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtDraweeBank" autocomplete="off" Width="370px" ClientIDMode="Static" runat="server" CssClass="inputbox2, uppercase"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Instrument Type  &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="4" align="left">
                                        <asp:DropDownList ID="ddlInstrumentType" autocomplete="off" DataSource='<%# drpload() %>'
                                            DataValueField="INST_TYPE" DataTextField="INST_TYPE_DESC" AppendDataBoundItems="true"
                                            Width="170px" Height="25px" CssClass="inputbox2" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="(Select Instrument Type)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="lblInstrumentLabel" autocomplete="off" CssClass="labelCaption" runat="server">No. &nbsp;&nbsp;<span class="require">*</span> </asp:Label>
                                        <asp:TextBox ID="txtNo" Width="70px" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <%--<td class="labelCaption">No</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtInstrumentNo" Width="60px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <div style="padding-left: 850px">
                                            <asp:Button ID="cmdSearch" runat="server" Text="Show" CommandName="Add" Height="30" autocomplete="off"
                                                Width="70px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSearch_Click" />
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" Height="30" autocomplete="off"
                                                Width="70px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="10" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>

                            </table>
                        </InsertItemTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="panGrd" runat="server" Width="100%" Visible="true">
                        <div style="overflow-y: scroll; height: auto; width: 100%; float: left">
                            <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both" ShowFooter="true"
                                AutoGenerateColumns="false" DataKeyNames="SUB_ID" OnRowDataBound="tbl_OnRowDataBound" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <SelectedRowStyle BackColor="#87CEEB" />
                                <Columns>
                                    <asp:BoundField DataField="SUB_ID" ItemStyle-CssClass="labelCaption, hiddencol" HeaderStyle-CssClass="TableHeader hiddencol" HeaderText="SUB_ID" FooterStyle-CssClass="hiddencol" />
                                    <asp:BoundField DataField="SUB_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sub Head" />
                                    <asp:BoundField DataField="DUE_AMT" ItemStyle-CssClass="labelCaptionright" DataFormatString="{0:0.00}" HeaderStyle-CssClass="TableHeader" HeaderText="Due Amount" />
                                    <%--<asp:BoundField DataField="DUE_DATE" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Due Date" />--%>
                                    <%--<asp:BoundField DataField="ACTUAL_AMT" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Actual Amount" />--%>
                                    <asp:TemplateField HeaderText="Actual Amount">
                                        <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:TextBox autocomplete="off" ID="txtActAmount" Width="100px" Height="20px" ClientIDMode="Static" ForeColor="Navy" Text='<%# string.Format("{0:f2}",Eval("ACTUAL_AMT"))%>' runat="server" CssClass="inputbox2, rightAlign" onkeyup="calculateGrandTotal();"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DUE_DATE" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Due Date" />
                                    <asp:TemplateField HeaderText="Remarks">
                                        <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:TextBox autocomplete="off" ID="txtRemarks" Width="600px" Height="20px" ForeColor="Navy" Text='<%# Eval("REMARKS")%>' ClientIDMode="Static" runat="server" CssClass="inputbox2, uppercase"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="labelCaption, rightAlign" BackColor="#99ccff" Font-Size="10" Font-Bold="true" ForeColor="Navy" />
                            </asp:GridView>
                        </div>
                        <table align="center" cellpadding="5" width="100%">
                            <tr>
                                <td colspan="4" class="labelCaption">
                                    <hr class="borderStyle" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10">
                                    <div style="padding-left: 850px">
                                        <asp:Button ID="cmdUpdate" autocomplete="off" runat="server" Text="Cerate" CommandName="Add" Height="30"
                                            Width="70px" CssClass="save-button DefaultButton" OnClick="cmdUpdate_Click" />
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </asp:Panel>

                    <asp:Panel ID="panVch" runat="server" Width="100%" Visible="false">
                        <div style="margin-left: 300px">
                            <asp:Label ID="lblCaption" autocomplete="off" Font-Size="20px" ForeColor="Navy" Font-Bold="true" runat="server">Last Voucher No. </asp:Label>
                            <asp:Label ID="lblVchNo" autocomplete="off" Font-Size="20px" ForeColor="Navy" Font-Bold="true" runat="server"></asp:Label>
                            <br />
                            <br />

                            <asp:Button ID="cmdDone" runat="server" Text="Done" Height="30" autocomplete="off"
                                Width="70px" CssClass="save-button DefaultButton" OnClick="cmdDone_Click" />
                            <asp:Button ID="btnPrint" runat="server" Text="Print" CommandName="print" Height="30" Width="70px" autocomplete="off"
                                CssClass="save-button Default" />
                        </div>

                    </asp:Panel>

                </td>
            </tr>

            <tr>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>
