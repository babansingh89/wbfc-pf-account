﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;   
using System.Data.SqlClient;
using System.Configuration;

public partial class BondAllotment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Select Sector Before Continuing To Loan Sanction Page!');window.location.href='welcome.aspx';</script>");
            }
            else
            {
                if (!IsPostBack)
                {
                    //PopulateGrid();
                    GridScheme();
                    GridBondAllot();

                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void GridBondAllot()
    {
        DataTable DtTerms = new DataTable();
        DtTerms.Columns.Add("DEPOSIT_ID");
        DtTerms.Columns.Add("APPL_NO");
        DtTerms.Columns.Add("APPL_DT");
        DtTerms.Columns.Add("APPL_AMOUNT");
        DtTerms.Columns.Add("APPLICANT_NAME");
        DtTerms.Columns.Add("ISSU_AMOUNT");
        DtTerms.Rows.Add();
        gvDetails.DataSource = DtTerms;
        gvDetails.DataBind();
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                //PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_InstrumentTypeByID", ID);
                Session["ID"] = ID;
                dv.DataSource = dtResult;
                dv.DataBind();
                
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_InstrumentTypeByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                //PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }     

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GET_SchemeIDDisplay(string Scheme_OLD_ID)
    {
        List<string> Detail = new List<string>();
      int  Sectorid = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
      DataTable dt = DBHandler.GetResult("Get_SchemeID_BondAllotment", Scheme_OLD_ID, Sectorid);
        foreach (DataRow dtRow in dt.Rows)
        {
            Detail.Add(dtRow["SCHEME_ID"].ToString() + "|" + dtRow["OLD_SCHEME_ID"].ToString() + "|" + dtRow["SCHEME_NAME"].ToString() + "|" + dtRow["AMOUNT"].ToString());
        }
        return Detail.ToArray();      
    }
    public class SchemeIDPopup 
    {
        public string OLD_SCHEME_ID { get; set; }
        public string SCHEME_NAME { get; set; }       
    }

    private void GridScheme()
    {
        DataTable DtTerms = new DataTable();
        DtTerms.Columns.Add("OLD_SCHEME_ID");
        DtTerms.Columns.Add("SCHEME_NAME");
        DtTerms.Rows.Add();
        GridViewSchemeID.DataSource = DtTerms;
        GridViewSchemeID.DataBind();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ClsSchemename[] GET_SchemeNameAmount(int StrSchemeID)
    {
       

        List<ClsSchemename> Detail = new List<ClsSchemename>();


        DataTable dtGetData = DBHandler.GetResult("Get_SchemeNameAmount", StrSchemeID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ClsSchemename DataObj = new ClsSchemename();
            DataObj.SCHEME_NAME = dtRow["SCHEME_NAME"].ToString();
            DataObj.AMOUNT = dtRow["AMOUNT"].ToString();
            Detail.Add(DataObj);
        }

        return Detail.ToArray();
    }
    public class ClsSchemename 
    {
        public string SCHEME_NAME { get; set; }
        public string AMOUNT { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ClsBondAllot[] GET_BondAllotGridpopu(string SchemeID,string Status)
    {
        List<ClsBondAllot> Detail = new List<ClsBondAllot>();

        DataTable dtGetData = DBHandler.GetResult("Get_BondAllotmentPopu", SchemeID, Status);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ClsBondAllot DataObj = new ClsBondAllot();
            DataObj.DEPOSIT_ID = dtRow["DEPOSIT_ID"].ToString();
            DataObj.APPL_NO = dtRow["APPL_NO"].ToString();
            DataObj.APPLICATION_DATE = dtRow["APPLICATION_DATE"].ToString();
            DataObj.APPL_DT = dtRow["APPL_DT"].ToString();
            DataObj.APPL_AMOUNT = dtRow["APPL_AMOUNT"].ToString();
            DataObj.APPLICANT_NAME = dtRow["APPLICANT_NAME"].ToString();
            DataObj.ISSU_AMOUNT = dtRow["ISSU_AMOUNT"].ToString();
            Detail.Add(DataObj);
        }

        return Detail.ToArray();
    }  
    public class ClsBondAllot 
    {
        public string DEPOSIT_ID { get; set; }
        public string APPL_NO { get; set; }
        public string APPLICATION_DATE { get; set; }        
        public string APPL_DT { get; set; }
        public string APPL_AMOUNT { get; set; }
        public string APPLICANT_NAME { get; set; }
        public string ISSU_AMOUNT { get; set; }

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_BondAllotmentProceed(string SchemeID, string DepositID, string IssueAmount)
    {
        string JSONVal = "";
        try
        {           
            int ID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            DataTable dtGetData = DBHandler.GetResult("Update_BondAllotment", SchemeID, DepositID, Convert.ToDecimal(IssueAmount), ID);
            
            string result = "success";
            JSONVal = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

}