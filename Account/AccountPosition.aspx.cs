﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class AccountPosition : System.Web.UI.Page
{
    static string StrFormula = "";
    protected string YearMonth = "";
    protected string VchType = "";
    protected string VchDate = "";
    protected string VchNo = "";
    protected string str = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }

            if (!IsPostBack)
            {
                //PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    class Array1s
    {       
        public string LoaneeCode { get; set; }
    }

    [WebMethod(EnableSession = true)]    
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string YearMonth, string LoaneeUni_ID, string AsOnDate, string MinInsAmount,string F,string Doc,string Etc)
    {
        string JSONVal = "";       
        try
        {
            //string Lonee = ""; string LoneeSplit = "";
            //if (LoaneeUni_ID != "0")
            //{
            //    var deta = new JavaScriptSerializer();
            //    List<Array1s> subChild = deta.Deserialize<List<Array1s>>(LoaneeUni_ID);

            //    for (var i = 0; i < subChild.Count; i++)
            //    {
            //        Lonee = Lonee + subChild[i].LoaneeCode + ",";
            //    }
            //    LoneeSplit = Lonee.Remove(Lonee.Length - 1);
            //}

            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            String Msg = "";
            StrFormula = "";
            int UserID;
            int SectorID;           
            string StrPaperSize = "";     
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            StrFormula = "";                
            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize); 
            if (F == "AP")
            { DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, LoaneeUni_ID, SectorID, "", "", "", "", "", "", ""); }            
            if (F == "MInRA")
            { DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, LoaneeUni_ID, SectorID, "", "", "", "", "", "", ""); }
            if (F == "MRS")
            { DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, SectorID, "", "", "", "", "", "", "", ""); }
            if (F == "IRA")
            { DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, LoaneeUni_ID, AsOnDate, MinInsAmount, SectorID, Doc, Etc, "", "", ""); }

            if (F == "APLD")
            {
                DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, SectorID, LoaneeUni_ID, 1, "", "", "", "", "", ""); 
            }

            if (F == "APVD")
            {
                DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, SectorID, LoaneeUni_ID, 2, "", "", "", "", "", ""); 
            }


            //if (F == "DIS") //F == "DIS" || F == "SANC"
            //{
            //    DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, Doc, Etc, SectorID, "", "", "", "", "", "", ""); 
            //}

            if (F == "DIS" || F == "SANC")
            {
                DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, Doc, Etc, SectorID, LoaneeUni_ID, "", "", "", "", "", "");
            }

            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    [WebMethod]
    public static string[] GET_AutoComplete_StartCode(string STcode, int SecID)
    {
        List<string> Detail = new List<string>();
        DataTable dt = DBHandler.GetResult("GET_MST_AccLoan_LoaneeCodeName", STcode, SecID);
        foreach (DataRow dtRow in dt.Rows)
        {
            Detail.Add(dtRow["LOANEE_CODE"].ToString() + "|" + dtRow["LOANEE_NAME"].ToString() + "|" + dtRow["LOANEE_UNQ_ID"].ToString());
        }
        return Detail.ToArray();   
    }

    //public void PopulateGrid()
    //{
    //    DataTable dt = new DataTable();
    //    dt.Rows.Add();

    //    tbl.DataSource = dt;
    //    tbl.DataBind();
    //}

}