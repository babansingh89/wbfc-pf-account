﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="GlSlTrial.aspx.cs" Inherits="GlSlTrial" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/GL_SLTrial.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
             
            $("input[name='GLSLtrial']").click(function () {
                if ($('#A').is(":checked")) {
                    $('#panGlID').hide();

                } else {
                    $('#panGlID').show();
                }
            });
            
        });


        $(document).ready(function () {
            $('#txtFrmDate').datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop', 
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'


            });
            $("#txtFrmDate").focus();
        });

        $(document).ready(function () {
            $('#txtToDate').datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'


            });
        });

        function beforeSave() {
            $("#frmEcom").validate();
            $("#txtFrmDate").rules("add", { required: true, messages: { required: "Please select From date" } });
            $("#txtToDate").rules("add", { required: true, messages: { required: "Please select To date" } });
            $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please Select a Unit" } });
            // $("#txtGLCode").rules("add", { required: true, messages: { required: "Select a GL ID" } });
        }

        function PrintDiv() {
            var divToPrint = $('#divGL').html().trim();

            if (divToPrint != "") {
                var popupWin = window.open('', '_blank', '');
                popupWin.document.open();
                //popupWin.document.write('<html><head><title>Print</title>');
                popupWin.document.write('<div style="page-break-after:always;"><center>');
                popupWin.document.write('<label>' + $("#txtOrgName").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>' + $("#txtOrgAddress").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>TRIAL BALANCE FROM  ' + $("#txtFrmDate").val() + ' TO ' + $("#txtToDate").val() + '</label>');
               
                
                popupWin.document.write("<link href='css/wbfdc-style1.css' rel='stylesheet' type='text/css' />");
                popupWin.document.write('</head><body onload="window.print()"><center>' + divToPrint + '</center></body></html>');
                popupWin.document.close();
            } else {
                alert('No Data To Print');
            }
        }
        function PrintDivSL() {
            var divToPrint = $('#divSL').html().trim();

            if (divToPrint != "") {
                var popupWin = window.open('', '_blank', '');
                popupWin.document.open();
                //popupWin.document.write('<html><head><title>Print</title>');
                popupWin.document.write('<div style="page-break-after:always;"><center>');
                popupWin.document.write('<label>' + $("#txtOrgName").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>' + $("#txtOrgAddress").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>SUB TRIAL BALANCE FROM  ' + $("#txtFrmDate").val() + ' TO ' + $("#txtToDate").val() + '</label>');
                popupWin.document.write("<link href='css/wbfdc-style1.css' rel='stylesheet' type='text/css' />");
                popupWin.document.write('</head><body onload="window.print()"><center>' + divToPrint + '</center></body></html>');
                popupWin.document.close();
            } else {
                alert('No Data To Print');
            }
        }

        function PrintDivVoucherMon() {
            var divToPrint = $('#divVoucherMon').html().trim();
            if (divToPrint != "") {
                var popupWin = window.open('', '_blank', ''); //sm
                popupWin.document.open();
                //popupWin.document.write('<html><head><title>Print</title>');
                popupWin.document.write('<div style="page-break-after:always;"><center>');
                popupWin.document.write('<label>' + $("#txtOrgName").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>' + $("#txtOrgAddress").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>VOUCHER FROM  ' + $("#txtFrmDate").val() + ' TO ' + $("#txtToDate").val() + '</label>');

                popupWin.document.write("<link href='css/wbfdc-style1.css' rel='stylesheet' type='text/css' />");
                popupWin.document.write('</head><body onload="window.print()"><center>' + divToPrint + '</center></body></html>');
                popupWin.document.close();
            } else {
                alert('No Data To Print');
            }
        }

        function PrintdivVchFinal() {
            var divToPrint = $('#divVchFinal').html().trim(); 
            if (divToPrint != "") {
                var popupWin = window.open('', '_blank', '');
                popupWin.document.open();
                //popupWin.document.write('<html><head><title>Print</title>');
                //popupWin.document.write("<link href='css/wbfdc-style1.css' rel='stylesheet' type='text/css' />");
                popupWin.document.write('<div style="page-break-after:always;"><center>');
                popupWin.document.write('<label>' + $("#txtOrgName").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>' + $("#txtOrgAddress").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>VOUCHER FROM  ' + $("#txtFrmDate").val() + ' TO ' + $("#txtToDate").val() + '</label>');
                popupWin.document.write("<link href='css/wbfdc-style1.css' rel='stylesheet' type='text/css' />");
                popupWin.document.write('</head><body onload="window.print()"><center>' + divToPrint + '</center></body></html>');
                popupWin.document.close();
            } else {
                alert('No Data To Print');
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">

       <div align="center" class="borderStyle" style="width: 100%">
        <table class="headingCaption" width="100%" align="center">
            <tr>
                 <td>GL SL Trial</td>
                <asp:TextBox ID="txtOrgName" autocomplete="off" runat="server" ClientIDMode="Static" style="display:none" ></asp:TextBox>
                <asp:TextBox ID="txtOrgAddress" autocomplete="off" runat="server" ClientIDMode="Static" style="display:none"></asp:TextBox>
            </tr>
        </table>  

     
        <div id="dialogGL" style="display: none">
            <asp:TextBox ID="txtSearchGL" autocomplete="off" runat="server" Width="516px" CssClass="TextSearchVendor" MaxLength="150"></asp:TextBox>

            <asp:GridView ID="grdGL" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc; width: 520px;" AutoGenerateColumns="false" CssClass="GridVendor" PageSize="5" AllowPaging="true">
                <Columns>
                    <asp:BoundField DataField="OLD_GL_ID" HeaderText="Group ID" ItemStyle-Width="150" />
                    <asp:BoundField DataField="GL_NAME" HeaderText="Group Name" ItemStyle-Width="150" />
                </Columns>
            </asp:GridView>

        </div>
        <table style="width:100%;"  align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="100%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table style="width:100%" align="center" >
                                <tr >
                                    <%--<td class="labelCaption"> Search Type </td>
                                    <td class="labelCaption">:</td>--%>
                                    <td align="center" colspan="0" style="font-family: Segoe UI; font-size: 12px; color: Navy;font-weight:bold;font-size:14px;width:100px;">Search Type :- 
                                       </td>
                                    <td align="center" style="width:50px;">
                                        <input type='radio' runat="server" autocomplete="off" clientidmode="Static" name='GLSLtrial' id='A' /><font size="3" color="navy" style="font-weight:bold;">GL</font>
                                        </td>
                                    <td align="center" style="width:50px;">
                                        <input type='radio' runat="server" autocomplete="off" clientidmode="Static" name='GLSLtrial' id='B' /><font size="3" color="navy" style="font-weight:bold;">SL</font>
                                        <%--<asp:RadioButton ID="rdGL" Checked="true" class="inputbox2" runat="server" Font-Size="small" GroupName="GLSLtrial" Text="GL" />
                                        <asp:RadioButton ID="rdSL" class="inputbox2" runat="server" Font-Size="small" GroupName="GLSLtrial" Text="SL" />--%>
                                    </td>
                                    <td align="right"></td>
                                   
                                </tr>
                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                </table>
                                 <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td style="padding: 5px;font-weight:bold;" class="labelCaption">From Date &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="txtFrmDate" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td style="padding: 5px;font-weight:bold;" class="labelCaption">To Date &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="txtToDate" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                     <td>
                                        <asp:CheckBox ID="chkZero" autocomplete="off" CssClass="textbox" Width="250px"  runat="server" Text=" Zero" Font-Bold="true" ForeColor="Navy" Font-Size="13px"
                                                AppendDataBoundItems="true">
                                            </asp:CheckBox>
                                    </td>
                                </tr>
                                      </table>
                                 <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td style="font-weight:bold;width:115px;" class="labelCaption">Sector</td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td style="padding: 5px;" align="left" >
                                        <div style="height: 100%; width: 100%; overflow-y: scroll;">
                                            <asp:CheckBoxList ID="ddlSector" autocomplete="off" DataSource='<%# drpload() %>' RepeatColumns="6" Font-Bold="true" ForeColor="Navy" Font-Size="12px"
                                                RepeatDirection="Vertical"
                                                DataValueField="SectorID" CssClass="textbox" Width="100%"
                                                DataTextField="SectorName" runat="server"
                                                AppendDataBoundItems="true">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                               <%-- <tr>
                                    <td style="padding: 5px;" class="labelCaption">Zero</td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td style="padding: 5px;" align="left" colspan="4">
                                        
                                            <asp:CheckBox ID="chkZero" CssClass="textbox" Width="250px"  runat="server" Text="Zero" 
                                                AppendDataBoundItems="true">
                                            </asp:CheckBox>
                                        
                                    </td>
                                </tr>--%>
                                      </table>
                                 <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="6">
                                        <div id="panGlID" style="width: 100%; display: none;">
                                            <table>
                                                <tr>
                                                    <td class="labelCaption" style="font-size:12px;font-weight:bold;width:115px;">GL Code &nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtGLCode" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                        <asp:TextBox ID="txtGLName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>

                                    </td>
                                </tr>
                                     
                            </table>
                            
                        </InsertItemTemplate>
                        <FooterTemplate>
                           
                            <table align="center" width="100%">
                               <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSearch" runat="server" Text="Show" CommandName="Add" Height="40px" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSearch_Click" />
                                            
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" Height="40px" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <%--<tr>
                <td colspan="6" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>--%>
            <%-- <tr><td><a onclick="javascript:PrintDiv();" style="font-size:12px;color:Black; font-family:Segoe UI;font-weight:bold;" href="javascript:void()"><b>Print</b></a></td></tr>--%>
            <table style="width:100%;">
            <tr>
                <td>
                    <asp:Panel ID="pan_tbl" runat="server" Visible="false">
                        <%--Print GL--%>
                        <div style="float: left">
                            <asp:Button ID="Button1" runat="server" Text="Print" Height="40px" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClientClick="javascript:PrintDiv();" />
                             <asp:Button ID="btnExcel" runat="server" Text="Excel" Height="40px" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClick="btnExcel_Click" />
                            <%--<a onclick="javascript:PrintDiv();" style="font-size: 12px; color: Black; font-family: Segoe UI; font-weight: bold;" href="javascript:void()"><b>Print GL</b></a>--%>
                        </div>
                        <div id="divGL" runat="server" style="overflow-y: scroll; height: auto; width: 100%">
                            <asp:GridView ID="tbl" runat="server" ClientIDMode="Static" autocomplete="off" Width="100%" align="center" GridLines="Both" OnSelectedIndexChanged="tbl_OnSelectedIndexChanged" ShowFooter="true"
                                OnRowDataBound="tbl_OnRowDataBound" AutoGenerateColumns="false" DataKeyNames="GL_ID, GL_MASTER, OLD_GL_ID, OLD_SL_ID" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <SelectedRowStyle BackColor="#87CEEB" />
                                <Columns>
                                    <asp:BoundField DataField="OLD_GL_ID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="GL Code" />
                                    <asp:BoundField DataField="GL_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="GL Name" />
                                    <asp:BoundField DataField="OP_DR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Opening Debit" />
                                    <asp:BoundField DataField="OP_CR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Opening Credit" />
                                    <asp:BoundField DataField="TR_DR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Transaction Ddebit" />
                                    <asp:BoundField DataField="TR_CR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Transaction Credit" />
                                    <asp:BoundField DataField="CL_DR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Closing Debit" />
                                    <asp:BoundField DataField="CL_CR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Closing Credit" />
                                </Columns>
                                <FooterStyle CssClass="labelCaption" BackColor="#99ccff" />
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pan_tblSL" runat="server" Visible="false">
                        <%--Print SL--%>
                        <div style="float: left">
                            <asp:Button ID="Button2" runat="server" Text="Print" Height="40px" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClientClick="javascript:PrintDivSL();" />

                            <%--<a onclick="javascript:PrintDivSL();" style="font-size: 12px; color: Black; font-family: Segoe UI; font-weight: bold;" href="javascript:void()"><b>Print SL</b></a>--%>
                        </div>

                        <div id="divSL" style="overflow-y: scroll; height: auto; width: 100%">
                            <table class="headingCaption" width="100%" align="center">
                                <tr>
                                    <td>GL Code and Name:- <%= (GL_Name = string.IsNullOrEmpty(GL_Name) ? "" : GL_Name)%></td>
                                </tr>
                            </table>
                            <asp:GridView ID="tblSL" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both" OnSelectedIndexChanged="tblSL_OnSelectedIndexChanged" ShowFooter="true"
                                OnRowDataBound="tblSL_OnRowDataBound" AutoGenerateColumns="false" DataKeyNames="GL_ID, SL_ID, OLD_GL_ID, OLD_SL_ID" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <SelectedRowStyle BackColor="#87CEEB" />
                                <Columns>
                                    <asp:BoundField DataField="OLD_SL_ID" Visible="true" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="SL Code" />
                                    <asp:BoundField DataField="GL_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="SL Name" />
                                    <asp:BoundField DataField="OP_DR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Opening Debit" />
                                    <asp:BoundField DataField="OP_CR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Opening Credit" />
                                    <asp:BoundField DataField="TR_DR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Transaction Ddebit" />
                                    <asp:BoundField DataField="TR_CR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Transaction Credit" />
                                    <asp:BoundField DataField="CL_DR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Closing Debit" />
                                    <asp:BoundField DataField="CL_CR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Closing Credit" />
                                </Columns>
                                <FooterStyle CssClass="labelCaption" BackColor="#99ccff" />
                            </asp:GridView>
                        </div>
                        <div style="float: right;">
                            <asp:Button ID="cmdBack" runat="server" Text="GL" CommandName="Add1" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                OnClick="cmdBack_Click" />
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="panVoucherMon" runat="server" Visible="false">
                        <%--Print Voucher--%>
                        <div style="float: left">
                            <asp:Button ID="Button3" runat="server" Text="Print" Height="40px" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClientClick="javascript:PrintDivVoucherMon();" />
                            <%--<a onclick="javascript:PrintDivVoucherMon();" style="font-size: 12px; color: Black; font-family: Segoe UI; font-weight: bold;" href="javascript:void()"><b>Print Voucher</b></a>--%>
                        </div>

                        <div id="divVoucherMon" style="overflow-y: scroll; height: auto; width: 100%">
                            <table class="headingCaption" width="100%" align="center">
                                <tr>
                                    <%--<td>SL Code and Name:- <%= (SL_Name = string.IsNullOrEmpty(SL_Name) ? "" : SL_Name)%></td>--%>
                                </tr>
                            </table>
                            <br />
                            <table width="80%" align="center">
                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; text-align:center;">Name :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (Sectors = string.IsNullOrEmpty(Sectors) ? "" : Sectors)%></font>
                                    </td>
                                </tr>
                                <%--                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;">Address :</td>
                                </tr>--%>
                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; text-align:center;">Month Wise Detail :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">From  <%= (From_Date = string.IsNullOrEmpty(From_Date) ? "" : From_Date)%>  to  <%= (To_Date = string.IsNullOrEmpty(To_Date) ? "" : To_Date)%>
                                        </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy;  text-align:center;">GL/SL Name :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (SL_Name = string.IsNullOrEmpty(SL_Name) ? "" : SL_Name)%>
                                        </font>

                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:GridView ID="tblVoucherMon" autocomplete="off" runat="server" Width="80%" align="center" GridLines="Both" OnSelectedIndexChanged="tblVoucherMon_OnSelectedIndexChanged" ShowFooter="true"
                                OnRowDataBound="tblVoucherMon_OnRowDataBound" AutoGenerateColumns="false" DataKeyNames="GL_ID, SL_ID, OLD_GL_ID, OLD_SL_ID" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <SelectedRowStyle BackColor="#87CEEB" />
                                <Columns>
                                    <asp:BoundField DataField="MONTH_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Month" />
                                    <asp:BoundField DataField="OP_DRCR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Opening" />
                                    <asp:BoundField DataField="TR_DR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Transaction Ddebit" />
                                    <asp:BoundField DataField="TR_CR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Transaction Credit" />
                                    <asp:BoundField DataField="CL_DRCR" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Closing" />
                                </Columns>
                                <FooterStyle CssClass="labelCaption" BackColor="#99ccff" />
                            </asp:GridView>
                             </div>
                            <div style="padding-left: 100px;" id="DivchartGLSL">
                                <asp:Chart ID="chartGLSL" autocomplete="off" runat="server" Width="800px" Height="200px" BorderlineWidth="0">
                                    <Series>
                                        <asp:Series Name="Series1" LegendText="Year" ChartType="Column" ChartArea="ChartArea1" Color="#3399ff" XValueMember="MONTH_NAME"
                                            YValueMembers="CL_DRCR" BackGradientStyle="DiagonalLeft" YValueType="Double">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" BackColor="#ffffcc" BackSecondaryColor="#99ccff" BorderColor="#ff0066" AlignmentStyle="AxesView">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                       
                        <div style="float: right;">
                            <asp:Button ID="cmdSl" runat="server" Text="SL" CommandName="Add2" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                OnClick="cmdSLBack_Click" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panVchMonthDtl" runat="server" Visible="false">
                        <%--<div style="float: left">
                            <a onclick="javascript:PrintDivVchMonth();" style="font-size: 12px; color: Black; font-family: Segoe UI; font-weight: bold;" href="javascript:void()"><b>Print SL</b></a>
                        </div>--%>

                        <div id="divVchMonthDtl" style="overflow-y: scroll; height: auto; width: 100%">
                            <table class="headingCaption" width="100%" align="center">
                                <tr>
                                    <td>SL Name:- <%= (SL_NameMonth = string.IsNullOrEmpty(SL_NameMonth) ? "" : SL_NameMonth)%></td>
                                </tr>
                            </table>
                            <asp:GridView ID="tblVchMonthDtl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                                OnRowDataBound="tblVchMonthDtl_OnRowDataBound" OnSelectedIndexChanged="tblVchMonthDtl_OnSelectedIndexChanged"
                                AutoGenerateColumns="false" DataKeyNames="GL_ID, SL_ID, VCH_NO, VCH_TYPE, SectorID" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <SelectedRowStyle BackColor="#87CEEB" />
                                <Columns> <%--VCH_NO--%>
                                    <asp:BoundField DataField="YEAR_MONTH" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Year Month" />
                                    <asp:BoundField DataField="VCH_DATE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Date" />
                                    <asp:BoundField DataField="VoucherNo" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher No" />
                                    <asp:BoundField DataField="VCH_AMOUNT" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Amount" />
                                    <asp:BoundField DataField="NARRATION" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Narration" />

                                </Columns>
                            </asp:GridView>
                        </div>
                        <div style="float: right;">
                            <asp:Button ID="cmbVchMonBack" runat="server" Text="Back" CommandName="Add3" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                OnClick="cmbVchMonBack_Click" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panVchFinal" runat="server" Visible="false">
                        <%--Print Voucher--%>
                        <div style="float: left">
                             <asp:Button ID="Button4" runat="server" Text="Print" Height="40px" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClientClick="javascript:PrintdivVchFinal();" />

                            <%--<a onclick="javascript:PrintdivVchFinal();" style="font-size: 12px; color: Black; font-family: Segoe UI; font-weight: bold;" href="javascript:void()"><b>Print Voucher</b></a>--%>
                        </div><br /><br />
                        <div id="divVchFinal" style="width: 100%; text-align: center;" runat="server" clientidmode="Static">

                            <table style="width: 100%; text-align: center;">
                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; text-align: center">YearMonth :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (YearMonth = string.IsNullOrEmpty(YearMonth) ? "" : YearMonth)%></font>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; text-align: center;">Voucher Type :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (VchType = string.IsNullOrEmpty(VchType) ? "" : VchType)%> 
                                        </font>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; text-align: center;">Voucher Date :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (VchDate = string.IsNullOrEmpty(VchDate) ? "" : VchDate)%>
                                        </font>

                                    </td>
                                </tr>
                                <tr></tr>

                                <tr>
                                    <td style="font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; text-align: center;">Voucher No :
                                        <font style="font-family: Segoe UI; font-size: 12px; color: Navy;">
                                            <%= (VchNo = string.IsNullOrEmpty(VchNo) ? "" : VchNo)%>
                                        </font>

                                    </td>
                                </tr>

                                <%= (str = string.IsNullOrEmpty(str) ? "" : str)%>
                            </table>
                        </div>
                        <div style="float: right;">
                          
                            <asp:Button ID="btnVchReport" runat="server" Text="Back" CommandName="Add4" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton"
                                OnClick="cmbVchReport_Click" />
                        </div>
                    </asp:Panel>

                </td>
            </tr>
                </table>
        </table>
    </div>
</asp:Content>
