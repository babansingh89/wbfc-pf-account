﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Services;
using System.Data.SqlClient;
using System.Threading;
using System.Web.Script.Services;
using System.Text;
using System.Web.Script;
using System.Web.Script.Serialization;

public partial class InsuranceMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }

            if (!IsPostBack)
            {         
                Blank_Grid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    public void Blank_Grid()
    {
        DataTable dt = new DataTable();
        dt.Rows.Add();
        gvDetails.DataSource = dt;
        gvDetails.DataBind();
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
   
    [WebMethod]
    public static string SAVE_deatails(string INS_ID,string INS_CODE, string INS_NAME, string INS_ADD1, string INS_ADD2, string INS_ADD3)
    {
        string json = "";
        try
        {
            string result = "";     

            int SECTORID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
            int INSERTED_BY = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (INS_ID == "")
            {
                DataTable dtGetData = DBHandler.GetResult("Get_DupCheckInsuCode", "0", INS_CODE);
                if (dtGetData.Rows.Count > 0)
                {
                    //ValInsuID = Convert.ToString(dtGetData.Rows[0]["INS_CODE"]);
                    result = "Insurance Code Already Exists!";
                }
                else
                {
                    DBHandler.Execute("Insert_MSTAccLoanInsu_InsMST", INS_CODE, INS_NAME,
                        INS_ADD1.Equals("") ? DBNull.Value : (object)INS_ADD1,
                          INS_ADD2.Equals("") ? DBNull.Value : (object)INS_ADD2,
                           INS_ADD3.Equals("") ? DBNull.Value : (object)INS_ADD3, SECTORID, INSERTED_BY);

                    result = "Data Save Successfully.";
                }
            }
            else
            {
                DBHandler.Execute("Update_MSTAccLoanInsu_InsMST", Convert.ToInt32(INS_ID), INS_CODE,INS_NAME,
                     INS_ADD1.Equals("") ? DBNull.Value : (object)INS_ADD1,
                      INS_ADD2.Equals("") ? DBNull.Value : (object)INS_ADD2,
                       INS_ADD3.Equals("") ? DBNull.Value : (object)INS_ADD3, SECTORID, INSERTED_BY );
                result = "Record Update successfully.";
            }
            json = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return json;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ClsInsMST[] Bind_GRID(string InsuCode)
    {
        List<ClsInsMST> Detail = new List<ClsInsMST>();
        int SECTORID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtGetData = DBHandler.GetResult("GET_DetlBySecID_MSTAccLoanInsu_InsMST",Convert.ToString(InsuCode), Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ClsInsMST DataObj = new ClsInsMST();
            DataObj.INS_ID = dtRow["INS_ID"].ToString();
            DataObj.INS_CODE = dtRow["INS_CODE"].ToString();
            DataObj.INS_NAME = dtRow["INS_NAME"].ToString();
            DataObj.INS_ADD1 = dtRow["INS_ADD1"].ToString();
            DataObj.INS_ADD2 = dtRow["INS_ADD2"].ToString();
            DataObj.INS_ADD3 = dtRow["INS_ADD3"].ToString();
           
            Detail.Add(DataObj);
        }

        return Detail.ToArray();
    }
    public class ClsInsMST
    {
        public string INS_ID { get; set; }
        public string INS_CODE { get; set; }
        public string INS_NAME { get; set; }
        public string INS_ADD1 { get; set; }
        public string INS_ADD2 { get; set; }
        public string INS_ADD3 { get; set; }       

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ClsEdit[] Edit_GRID(string INS_ID)
    {
        List<ClsEdit> Detail = new List<ClsEdit>();
        int SECTORID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtGetData = DBHandler.GetResult("GET_DetlByInsID_MSTAccLoanInsu_InsMST", Convert.ToInt32(INS_ID), SECTORID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ClsEdit DataObj = new ClsEdit();
            DataObj.INS_ID = dtRow["INS_ID"].ToString();
            DataObj.INS_CODE = dtRow["INS_CODE"].ToString();
            DataObj.INS_NAME = dtRow["INS_NAME"].ToString();
            DataObj.INS_ADD1 = dtRow["INS_ADD1"].ToString();
            DataObj.INS_ADD2 = dtRow["INS_ADD2"].ToString();
            DataObj.INS_ADD3 = dtRow["INS_ADD3"].ToString();
            Detail.Add(DataObj);
        }

        return Detail.ToArray();
    }
    public class ClsEdit
    {
        public string INS_ID { get; set; }
        public string INS_CODE { get; set; }
        public string INS_NAME { get; set; }
        public string INS_ADD1 { get; set; }
        public string INS_ADD2 { get; set; }
        public string INS_ADD3 { get; set; }  
    }  

    [WebMethod]
    public static string Delete_Data(string INS_ID)
    {
        string json = "";
        string result = "";
        try
        {
            DataTable dtGetData = DBHandler.GetResult("Get_CheckInsCodeOnInsDet", INS_ID);
            if (dtGetData.Rows.Count > 0)
            {
                result = "Can Not Delete Record ! Insurance Code Used On Insurance Detail!..";
            }
            else
            {
                int SECTORID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
                DBHandler.Execute("Delete_MSTAccLoanInsu_InsMST", Convert.ToInt32(INS_ID), SECTORID);
                result = "Delete successfull.";
            }
            json = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return json;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Get_INS_ID(string InsuCode)
    {
        string Result = "";
        int SECTORID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtGetData = DBHandler.GetResult("GET_DetlBySecID_MSTAccLoanInsu_InsMST", Convert.ToString(InsuCode), Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            Result = dtRow["INS_ID"].ToString();
        }

        return Result;
    }

    //============================ Start Duplicate Check Group ID Coding Start here===================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DupChkInsuCode(string InsuCode, string InsuID)
    {
        string ValInsuID = "";
        DataTable dtGetData = DBHandler.GetResult("Get_DupCheckInsuCode", InsuID, InsuCode);
        if (dtGetData.Rows.Count > 0)
        {
            ValInsuID = Convert.ToString(dtGetData.Rows[0]["INS_CODE"]);
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(ValInsuID);
    }
    //============================End Duplicate Check Group ID Coding Start here===================================//

    [WebMethod]
    public static string[] Get_InsuranceCode(string InsuCode)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_InsuIDByCode", InsuCode, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {

            result.Add(string.Format("{0}|{1}", dtRow["INS_CODE"].ToString(), dtRow["INS_CODE"].ToString()));
        }
        return result.ToArray();
    }

}