﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Fixed_Asset_Location.aspx.cs" Inherits="Fixed_Asset_Location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

        });

        $(document).ready(function () {
            $("#txtLDesc").validate().trim();
        });

        $(document).ready(function () {
            if ("#txtLDesc" == LOC_ID) {
                alert("The Description already Exists");
                return false;
            }
        });

        function beforeSave() {
            $("#frmEcom").validate();
            $("#txtLDesc").rules("add", { required: true, messages: { required: "Please enter Location Name"} });
        }

        function Delete() {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            }
            else {
                return false;
            }
        }

        function unvalidate() {
            $('formEcom').validate().currentForm = '';
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>
                    Fixed Asset Location
                </td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="bordeStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" Width="99%" runat="server" CellPadding="4" OnModeChanging="dv_ModeChanging"
                        DefaultMode="Insert" GridLines="None" AutoGenerateRows="False" HorizontalAlign="Center">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">
                                        Location Description&nbsp;&nbsp;
                                    </td>
                                    <td class="labelCaption">
                                        :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLDesc" autocomplete="off" runat="server" MaxLength="100" Width="200px" ClientIDMode="Static" CssClass="inputbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">
                                        Main Location&nbsp;&nbsp;
                                    </td>
                                    <td class="labelCaption">
                                        :
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlMLoc" autocomplete="off" runat="server" MaxLength="100" Width="205px" ClientIDMode="Static"
                                            CssClass="inputbox" DataSource="<%# Mainloc() %>" DataTextField="LOC_DESC" DataValueField="LOC_ID"
                                            AppendDataBoundItems="true">
                                            <asp:ListItem Text="SELECT" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">
                                        Location Description&nbsp;&nbsp;
                                    </td>
                                    <td class="labelCaption">
                                        :
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLDesc" autocomplete="off" runat="server" MaxLength="100" Text='<%# Eval("LOC_DESC") %>'
                                            ClientIDMode="Static" CssClass="inputbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">
                                        Main Location&nbsp;&nbsp;
                                    </td>
                                    <td class="labelCaption">
                                        :
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlMLoc" autocomplete="off" runat="server" MaxLength="100" ClientIDMode="Static"
                                            CssClass="inputbox" DataSource="<%# Mainloc() %>" DataTextField="LOC_DESC" DataValueField="LOC_ID"
                                            AppendDataBoundItems="true">
                                            <asp:ListItem Text="SELECT" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="3" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">
                                        <span class="require">*</span> indicates Mandatory Field
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" Width="100px" autocomplete="off"
                                            CssClass="save-button DefaultButton" OnClick="cmdSave_Click" OnClientClick='javascript: return beforeSave()' />
                                        
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CommandName="Del" Width="100px" autocomplete="off"
                                            OnClick="cmdCancel_Click" CssClass="save-button DefaultButton" OnClientClick='javascript: return unvalidate()' />
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="gv" runat="server" AutoGenerateColumns="false" Width="100%" align="center" autocomplete="off"
                            DataKeyNames="LOC_ID" OnRowCommand="gv_RowCommand" CellPadding="2" CellSpacing="2"
                            GridLines="Both" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName="edt" ImageUrl="images/Edit.jpg" runat="server" ID="btnEdit" autocomplete="off"
                                            OnClientClick='javascript: return unvalidate()' CommandArgument='<%# Eval("LOC_ID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName="del" runat="server" ImageUrl="images/Delete.gif" CommandArgument='<%# Eval("LOC_ID") %>'
                                            autocomplete="off" ID="btnDelete" OnClientClick='javascript: return Delete()' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="LOC_DESC" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" 
                                    HeaderText="Location Description" />
                                <asp:BoundField DataField="P_LOC_Desc" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader"
                                    HeaderText="Main Location" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
