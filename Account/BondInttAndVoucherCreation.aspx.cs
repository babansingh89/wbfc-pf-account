﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Globalization;

public partial class BondInttAndVoucherCreation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }

            if (!IsPostBack)
            {

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert(" + ex.Message.Replace("'", "") + ")</script>");
        }
    }
    protected void btnCreate_click(object sender, EventArgs e)
    {
        try {
          //  txtAllotmentDate.Text.Equals("") ? DBNull.Value : (object)DateTime.ParseExact(txtAllotmentDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string allotment = "N";
            string Recalc = "Y";
           
            if (chckNewAllotment.Checked == true)
            {
                allotment = "Y";
                //allotmentDate = DateTime.ParseExact(txtAllotmentDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString();
            }
           // string allotdate = txtAllotmentDate.Text;

            if (chckNewAllotment.Checked == true)
            {
                DBHandler.Execute("Acc_Bond_Intt_Calc", txtForPeriod.Text, txthdnBank.Text, allotment, Convert.ToInt32(txthdnSchemeID.Text),
                   txtAllotmentDate.Text.Equals("") ? DBNull.Value : (object)DateTime.ParseExact(txtAllotmentDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                     Recalc, Session[SiteConstants.SSN_SECTOR_ID], Session[SiteConstants.SSN_INT_USER_ID]);
            }

            if (chckNewAllotment.Checked == false)
            {
                DBHandler.Execute("Acc_Bond_Intt_Calc", txtForPeriod.Text, txthdnBank.Text, allotment, null,
                   null,Recalc, Session[SiteConstants.SSN_SECTOR_ID], Session[SiteConstants.SSN_INT_USER_ID]);
            }

            ClientScript.RegisterStartupScript(this.GetType(), "Success", "<script>alert('Data Recorded Successfilly')</script>");
        }
        catch (Exception ex) { ClientScript.RegisterStartupScript(this.GetType(), "Error", "<Script>alert('" + ex.Message.Replace("'", "") + "')</script>"); }
     }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    [WebMethod]
    public static string[] GET_AutoComplete_Bank(string Bnk)
    {
        List<string> Detail = new List<string>();
        DataTable dt = DBHandler.GetResult("GET_MST_Acc_GL_BondInttVoucherCreation", Bnk);
        foreach (DataRow dtRow in dt.Rows)
        {
            Detail.Add(dtRow["BankID"].ToString() + "|" + dtRow["GL_ID"].ToString() + "|" + dtRow["SL_ID"].ToString());
        }
        return Detail.ToArray();
    }
    [WebMethod]
    public static string[] GET_AutoComplete_SchemeID(string scmID)
    {
        List<string> Detail = new List<string>();
        DataTable dt = DBHandler.GetResult("GET_MST_Acc_Scheme_BondInttVoucherCreation", scmID);
        foreach (DataRow dtRow in dt.Rows)
        {
            Detail.Add(dtRow["SchemeID"].ToString() + "|" + dtRow["SLR_NSLR"].ToString() + "|" + dtRow["GL_ID"].ToString() + "|" + dtRow["SCHEME_ID"].ToString());
        }
        return Detail.ToArray();
    }
}