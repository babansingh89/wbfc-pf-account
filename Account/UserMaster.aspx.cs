﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Web.Script.Services;


public partial class UserMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                GridLocksUser();
                
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void GridLocksUser()
    {
        DataTable User = new DataTable();
        User.Rows.Add();
        GridLockUser.DataSource = User;
        GridLockUser.DataBind();

        Button cmdResetPass = (Button)dv.FindControl("cmdResetPass");
        int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);

        DataTable dtMstUser = DBHandler.GetResult("GET_UserApproval", UserID);
        if (dtMstUser.Rows.Count > 0)
        {
            if (dtMstUser.Rows[0]["AdminFlag"].ToString() == "0")
            {
                  cmdResetPass.Style.Add("display", "none"); 
            }
        }
        else { cmdResetPass.Style.Add("display", "none"); }
    }

    [WebMethod]
    public static string GetUserType()
    {
        UserType_BLL typeUser = new UserType_BLL();

        List<Typ_User> Units = typeUser.GetAllTypeUser();
        return Units.ToJSON();
    }

    [WebMethod]
    public static string GetSector()
    {
        Mst_Sector_BLL sector = new Mst_Sector_BLL();

        List<Mst_Sector> Sectors = sector.GetAllSector();
        return Sectors.ToJSON();
    }

    [WebMethod]
    public static string GetAllUser()
    {
        Mst_User_BLL user = new Mst_User_BLL();

        List<Mst_User> MstUser = user.GetAll();
        return MstUser.ToJSON();
    }

    [WebMethod]
    public static string GetUserByUserID(string UserID)
    {
        Mst_User_BLL user = new Mst_User_BLL();

        List<Mst_User> MstUser = user.GetUserByID(UserID);
        return MstUser.ToJSON();
    }

    [WebMethod]
    public static string InsertUpdateUserMaster(string gUserID, string TypeUser, string UserNamae, string Password, string Name, string Sectors)
    {  
        CheckStatus cs = new CheckStatus();
        try
        {
            Mst_User_BLL userInsert = new Mst_User_BLL();
            Mst_User user=null;
            if (gUserID == "")
            {
                //Mst_User_BLL userInsert = new Mst_User_BLL();
                user = userInsert.InsertUser(new Mst_User()
                {
                    TypeCode = Convert.ToInt32(TypeUser),
                    UserName = UserNamae,
                    Name = Name,
                    ListSector = Sectors.Split(',').Select(t => new Mst_Sector()
                    {
                        SectorID = Convert.ToInt32(t)
                    }).ToList()
                }, Password);
            }
            else if (gUserID != "")
            {
                user = userInsert.UpdateUser(new Mst_User()
                {
                    UserID = Convert.ToInt32(gUserID),
                    TypeCode = Convert.ToInt32(TypeUser),
                    UserName = UserNamae,
                    Name = Name,
                    ListSector = Sectors.Split(',').Select(t => new Mst_Sector()
                    {
                        SectorID = Convert.ToInt32(t)
                    }).ToList()
                });
            }
            

            if (user != null)
            {
                cs.Message = "User " + user.UserName + " Submitted Successfully";
                cs.Status = "Y";
                cs.Data = user;
            }
            else
            {
                throw new Exception( " ERROR...... User " + user.UserName + " not created");                
            }
            //return cs.ToJSON();
        }
        catch (Exception ex)
        {
            cs.Message = ex.Message;
            cs.Status = "N";         
        }
        return cs.ToJSON();
    }

    [WebMethod]
    public static string DeleteUserByUserID(string UserID)
    {
        CheckStatus cs = new CheckStatus();
        try
        {
            string strDel="";
            Mst_User_BLL userDelete = new Mst_User_BLL();
            strDel = userDelete.DeleteUser(UserID);
            if (strDel == "Y")
            {
                cs.Message = "User Deleted Successfully";
                cs.Status = "Y";
                //cs.Data = userMaster;
            }
            else if (strDel == "N")
            {
                cs.Message = "Can not delete current loged user... ";
                cs.Status = "N";
            }
            else
            {
                throw new Exception(" ERROR...... User not deleted");
            }
        }
        catch (Exception ex)
        {
            cs.Message = ex.Message;
            cs.Status = "N";
        }
        
        return cs.ToJSON();
    }



    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    [WebMethod]
    public static string GET_UserReset(int UserID,string UserPass, string UserLocked)
    {
        string JSonVal = "";
        try
        {
            DataTable DTModify = DBHandler.GetResult("Get_ResetPassAndLocked", UserID, UserPass, UserLocked);
            JSonVal = "Records Save Successfully...!";
            
        }
        catch (Exception ex)
        {
            JSonVal = ex.Message;
           
        }

        return JSonVal;
    }

    [WebMethod]
    public static string UserCompleteData(string UserName)
    {
        UserMaster FrmUser = new UserMaster();
        List<Mst_User> users = FrmUser.userCompleteData(UserName);

        return users.ToJSON();
    }

    public List<Mst_User> userCompleteData(string username)
    {
        List<Mst_User> ListUsers = new List<Mst_User>();
        DataTable dtUsers = DBHandler.GetResult("GET_UsersAutoComplete", username);
        foreach (DataRow dtRow in dtUsers.Rows)
        {
            Mst_User user = new Mst_User();
            user.UserID = Convert.ToInt32(dtRow["UserID"].ToString());
            user.Name = dtRow["Name1"].ToString();

            ListUsers.Add(user);
        }
        return ListUsers;
    }

    [WebMethod]
    public static string Get_LockData()
    {
        string xml = "";
        
        DataSet ds = DBHandler.GetResults("GET_LockedUser");
        if (ds.Tables.Count > 0)
        {
            xml = ds.GetXml();
        }
        return xml;
    }

    [WebMethod]
    public static string Get_CheckedAdmnFlag()
    {  
        string JSonVal = "";
        try
        {
            int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);

            DataTable dtMstUser = DBHandler.GetResult("GET_UserApproval", UserID);
            if (dtMstUser.Rows.Count > 0)
            {
                JSonVal = dtMstUser.Rows[0]["AdminFlag"].ToString();
            }
            else { JSonVal="0"; }
        }
        catch (Exception ex)
        {
            JSonVal = ex.Message;

        }
        return JSonVal;
    }

}