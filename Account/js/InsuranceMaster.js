﻿$(document).ready(function () {

    $("input").focus(function () {
        $(this).css("background-color", "#cccccc");
    });
    $("input").blur(function () {
        $(this).css("background-color", "#ffffff");
    });

    $("select").focus(function () {
        $(this).css("background-color", "#cccccc");
    });

    $("select").blur(function () {
        $(this).css("background-color", "#ffffff");
    });

    BindGridView("");
    SearchLoaneeCode();
    $("#txtCode").focus();
    
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
});


$(document).ready(function () {
    $('#txtCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            if ($('#txtCode').val().trim() == '') {
                alert('Please Type Insurance Code');
                $('#txtCode').focus();
                return false;
            }
            else {
                DuplicateCheckInsuCode();
                return false;
            }
        }
    });


    $('#txtCode').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 9) {
            if ($('#txtCode').val().trim() == '') {
                alert('Please Type Insurance Code');
                $('#txtCode').focus();
                return false;
            }
            else {
                DuplicateCheckInsuCode();
                return false;
            }
        }
    });


    $('#txtName').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            if ($('#txtName').val().trim() == '') {
                alert('Please Type Insurance Name');
                $('#txtName').focus();
                return false;
            }
            else {
                $('#txtAdd1').focus();
                return false;
            }
        }
    });


    $('#txtAdd1').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtAdd2').focus();
            return false;
        }
    });

    $('#txtAdd2').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtAdd3').focus();
            return false;
        }
    });

    $('#txtAdd3').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#btnSave').focus();
            return false;
        }
    });




});

function DuplicateCheckInsuCode() {
    var InsuID = '';
    if ($("#txtCode").val().trim() == '') {
        return false;
    }
    $(".loading-overlay").show();
    if (hdINS_ID == '') { //$("#hdINS_ID").val()
        InsuID = '0';
    }
    else { InsuID = hdINS_ID; } //$("#hdINS_ID").val();
    var W = "{InsuCode:'" + $("#txtCode").val() + "',InsuID:'" + InsuID + "'}";
    $.ajax({
        type: "POST",
        url: "InsuranceMaster.aspx/GET_DupChkInsuCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            var GroupID = jQuery.parseJSON(data.d);
            if (GroupID != '') {
                alert('Insurance Code Already Exists!');
                $(".loading-overlay").hide();
                $("#txtCode").val('');
                $("#txtCode").focus();
                return false;
            }
            else { $(".loading-overlay").hide(); $('#txtName').focus(); return false; }

        },
    });
}


function BindGridView(ValInsuCode) {
    $(".loading-overlay").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "InsuranceMaster.aspx/Bind_GRID",
        data: "{InsuCode:'" + ValInsuCode + "'}",
        dataType: "json",
        success: function (data) { 
            $("#gvDetails").empty();
            $("#gvDetails").append("<tr><th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Edit</th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Delete</th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Code</th>" +
                                    "<TH style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Insurance Name </th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Address 1 </th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Address 2</th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Address 3 </th></tr>");
            
            for (var i = 0; i < data.d.length; i++) {
                $("#gvDetails").append("<tr><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;cursor:pointer'><div onclick='Edit(" + data.d[i].INS_ID + ")'><img src='images/edit.png' id='Edit" + data.d[i].INS_ID + "'/></div></td>" +
                                           "<td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;cursor:pointer'><div onclick='Delete(" + data.d[i].INS_ID + ")'><img src='images/delete.gif' id='Delete" + data.d[i].INS_ID + "'/></div></td>" +
                                          "<td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                    data.d[i].INS_CODE + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                  data.d[i].INS_NAME + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                  data.d[i].INS_ADD1 + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                  data.d[i].INS_ADD2 + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                  data.d[i].INS_ADD3 + "</td></tr>");
            }
            $(".loading-overlay").hide();
        },
        error: function (result) {
            alert("Error");
        }
    });
}
var hdINS_ID = '';
function Edit(INS_ID) {
    //  alert(TransID);
    hdINS_ID = INS_ID;//$("#hdINS_ID").val(INS_ID);
    document.getElementById('txtCode').disabled = true;
    document.getElementById('btnSearch').disabled = true;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "InsuranceMaster.aspx/Edit_GRID",
        data: "{INS_ID:'" + INS_ID + "'}",
        dataType: "json",
        success: function (data) { 
            //string INS_ID,string INS_CODE, string INS_NAME, string INS_ADD1, string INS_ADD2, string INS_ADD3
            hdINS_ID = data.d[0].INS_ID;//$("#hdINS_ID").val(data.d[0].INS_ID);
            $("#txtCode").val(data.d[0].INS_CODE);            
            $("#txtName").val(data.d[0].INS_NAME);
            $("#txtAdd1").val(data.d[0].INS_ADD1);                
            $("#txtAdd2").val(data.d[0].INS_ADD2);           
            $("#txtAdd3").val(data.d[0].INS_ADD3);        

            $("#btnSave").val('Update');
            $("#txtName").focus();
        },
        error: function (result) {
            alert("Error");  
        }
    });
}

function Delete(INS_ID) {
    var conf = confirm("Are you sure whant to delete this data??");
    if (conf == true) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "InsuranceMaster.aspx/Delete_data",
            data: "{INS_ID:'" + INS_ID + "'}",
            dataType: "json",
            success: function (data) {
                alert(data.d);
                BindGridView("");
            },
            error: function (result) {
                alert("Error");
            }
        });
    } else { return false; }
    //alert(conf);
}



$(document).ready(function () {
    $('#btnSave').click(function (event) {       //string INS_ID,string INS_CODE, string INS_NAME, string INS_ADD1, string INS_ADD2, string INS_ADD3 
       
        if ($("#txtCode").val().trim() == '')
        { alert("Please enter a Insurance Code !!"); $("#txtCode").focus(); return false; }
       
        if ($("#txtName").val().trim() == '')
        { alert("Enter A Insurance Name !!"); $("#txtName").focus(); return false; }        
      

        var H = "{INS_ID:'" + hdINS_ID + "',INS_CODE:'" + $("#txtCode").val().trim() + "',INS_NAME:'" + $("#txtName").val().trim() + "',INS_ADD1:'" + $("#txtAdd1").val() + "',INS_ADD2:'" + $("#txtAdd2").val() + "',INS_ADD3:'" + $("#txtAdd3").val().trim() + "'}"
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "InsuranceMaster.aspx/SAVE_deatails",
            data: H,
            dataType: "json",
            success: function (data) {
                if (data.d == '"Insurance Code Already Exists!"')
                {
                    alert(data.d);
                    $("#txtCode").focus();
                    return false;
                }
                else
                {
                    alert(data.d);
                    $("#btnSave").val("Save");

                    hdINS_ID = '';//$("#hdINS_ID").val('');

                    $("#tblMain").find('input:text').val('');

                    document.getElementById('txtCode').disabled = false;
                    document.getElementById('btnSearch').disabled = false;
                    BindGridView("");
                    $("#txtCode").focus();
                    return false;
                }
            },
            error: function (result) {
                alert("Error");
            }
        });      
    });
});


function SearchLoaneeCode() {
    $(".autosuggestLoaneeCode").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "InsuranceMaster.aspx/Get_InsuranceCode",
                data: "{'InsuCode':'" + document.getElementById('txtCode').value + "'}",

                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        };
                    }));
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            FunInsID(i.item.val);
            //$("#txtLoaneeIDSearch").val(i.item.val);
            //$("#hdnLoaneeID").val(i.item.val);
        }
    });
}

function FunInsID(InsuCode) {
    var W = "{InsuCode:'" + InsuCode + "'}";
    $.ajax({
        type: "POST",
        url: "InsuranceMaster.aspx/Get_INS_ID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            alert(data.d);
            hdINS_ID = data.d;
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            hdINS_ID = '';
            return false;
        }
    });
}

$(document).ready(function () {
    $('#btnSearch').click(function (event) {
        //if ($('#txtCode').val().trim() == '')
        //{
        //    alert('Please Select Insurance Code');
        //    $('#txtCode').focus();
        //    return false;
        //}
        BindGridView($('#txtCode').val());
        return false;
    });
});