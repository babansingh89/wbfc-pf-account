﻿var gUserID = "";
var EditStatus = "N";
var hdnUserID = '';
$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    populateModule();

    $('#txtUserName').prop('disabled', true);     
    $('#ddlModule').on('change', function () {
        $('#txtUserName').val('');
        hdnUserID='';
        populateMenu();
        $('#txtUserName').prop('disabled', false);        
    });
    autocompleteUserMaster();
});

function populateModule()
{
        $.ajax({
            type: "POST",
            url: "MenuAccess.aspx/GetModule",
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $('#ddlModule').empty().append('<option selected="selected" value="0">Please select</option>');
                $.each(JSON.parse(data.d), function (key, value) {
                    $('#ddlModule').append($("<option></option>").val(value.ModuleCode).html(value.ModuleName));
                });
            }
        });
}

function autocompleteUserMaster() {
    $("#txtUserName").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "MenuAccess.aspx/UserCompleteData",
                data: "{'UserName':'" + $("#txtUserName").val() + "'}",
                dataType: "json",
                success: function (data) {
                    var userData = JSON.parse(data.d);
                    if ($(userData).length > 0) {
                        var userDataAutoComplete = [];
                        $.each(userData, function (index, item) {
                            userDataAutoComplete.push({
                                label: item.Name,
                                Userval: item.UserID
                            });
                        });
                        response(userDataAutoComplete);
                    }
                    else {
                        alert('No data found.');
                        $('#txtUserName').val('');
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            hdnUserID = i.item.Userval;
            checkedMenu();
        }
    });
}

function checkedMenu() {
    var userID = hdnUserID;
    var moduleCode = $('#ddlModule').val();
    var chkMenuContainer = $("#chkMenuContainer");

    $.ajax({
        type: "POST",
        url: "MenuAccess.aspx/GetCheckedMenu",
        data: "{UserID:'" + userID + "', moduleCode:'" + moduleCode + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (data) {
            var userMenu = JSON.parse(data.d);

            var str = $("#chkMenuContainer").find("input[type='checkbox'][data-parentID!='0 ']").prop("checked", false);
         
            $.each(userMenu, function (index, value) {
                var checkedMenu = value.MenuCode;               
                var chk = $('#chkMenuContainer input[type="checkbox"][value=' + checkedMenu + ']');
                if ($(chk).length > 0) {
                    $(chk).prop("checked", true);
                }
            });          
        }
    });
}

function populateMenu() {
    var chkMenuContainer = $("#chkMenuContainer");
    var Module = $('#ddlModule').val();
   
    chkMenuContainer.empty();
    $.ajax({
        type: "POST",
        url: "MenuAccess.aspx/GetMenu",
        data: "{Module:'" + Module + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (data) {
            var sectors = JSON.parse(data.d);
            $.each(sectors, function (index, value) {
                //console.log(value);
                if ($.trim(value.ParentMenuCode) == 0) {
                    chkMenuContainer.append("<input type='checkbox' data-parentID='" + $.trim(value.ParentMenuCode) + " ' class='chkSector' value='" + value.MenuCode + "'/><span style='font-size:13px; font-weight:bold; color:#18588a'>" + value.Menuname + "<br/>");
                }
                else {
                    chkMenuContainer.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' data-parentID='" + $.trim(value.ParentMenuCode) + " ' class='chkSector' value='" + value.MenuCode + "'/><span style='font-size:11px; font-weight:bold; color:#18588a'>" + value.Menuname + "<br/>");
                }
                
            });
            var chk = $('#chkMenuContainer input[type="checkbox"]')
            $(chk).each(function () {
                //console.log($(this).attr("data-parentID"));
                if ($(this).attr("data-parentID") == 0) {
                    $(this).prop('checked', true);
                   // $(this).prop('disabled', true);
                }
            })          
        }
    });  
}

$(document).ready(function () {
    $('#cmdSave').click(function (event) {
        
        var userID = hdnUserID;
        var ModuleCode = $('#ddlModule').val();
        var chkMenus = "";
        var cnt = 0;        

        if (hdnUserID == '')
        { alert("Please select a User!!"); $("#txtUserName").focus(); return false; }
        if ($("#ddlModule").val() == '0')
        { alert("Please select a Module!!"); $("#ddlModule").focus(); return false; }

        var checkedCheckBoxes = $("#chkMenuContainer").find(".chkSector:checked");
        $.each(checkedCheckBoxes, function (index, value) {
            chkMenus = chkMenus + ($(value).val() + ",");
            cnt = cnt + 1;
        });
        chkMenus = chkMenus.substring(0, chkMenus.length - 1)
        if (cnt == 0)
        { alert("Please select at least one sector !!"); return false; }
        //alert(chkSector);

        $.ajax({
            type: "POST",
            url: "MenuAccess.aspx/InsertUserMenu",
            data: "{UserID:'" + userID + "', Menus:'" + chkMenus + "', ModuleCode:'" + ModuleCode + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var status = JSON.parse(data.d)
                if (status.Status == "Y") {
                    alert(status.Message);
                }
                else if (status.Status == "N") {
                    alert(status.Message);
                }

                //$("#chkMenuContainer").find("input[type='checkbox'][data-parentID!='0 ']").prop("checked", false);
                $("#chkMenuContainer").empty();
                $('#txtUserName').val('');
                $('#txtUserName').prop('disabled', true);
                hdnUserID='';
                $('#ddlModule').val('');
            },
            error: function (result) {
                alert("Error");
            }
        });
    });
});



