﻿
var InsertMode = 'Add';
var hdnLoaneeID = '';
var txtLoneeID = '';
var hdnGLID = '';
var hdnLoaneeUnqID = '';
var hdbPhoneID = '';
$.validator.addMethod("validEmail", function (value, element) {
    return this.optional(element) || value == value.match(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);//  /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/
    // --                                    or leave a space here ^^
}, "Not a Valid Email Address");

$.validator.addMethod("validNumeric", function (value, element) {
    //return this.optional(element) || value == value.match(/^[0-9]{0,3}[\.]{1}[0-9]{0,2}$/g);
    if (value != "") {
        //var IntRate = ($("#txtInterestRate").val());
        var IntRate = value;
        var IntRateLen = IntRate.indexOf('.');
        
        if (IntRateLen == -1 && IntRate.length > 3) {           
          return false;
        }
        else if ((IntRateLen > -1 && IntRateLen > 3) || (IntRateLen > -1 && (parseInt(IntRate.length) - parseInt(IntRateLen)) > 3)) {
            return false;
        }
    }
    return true;
}, "Not a Valid Numeric");

$.validator.addMethod("checkMobNo", function (value, element) {
    if (value != "") {
        var ExpMob = ($("#txtMobileNo1").val());
        if (parseInt(ExpMob).toString().length < 10) {
            return false;
        }
    }
    return true;
}, "Mobile No must be 10 Digit No.");

$(document).ready(function () {
    document.getElementById("A").checked = true;
    $('#btnRepaySchedule').hide();

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });

    $('#txtApplicationDt').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtApplicationDt').focus();
        }
    });
      
    $('#txtSancandt').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtSancandt').focus();
        }
    });

    $('#txtSanctionDT').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtSanctionDT').focus();
        }
    });

    $('#txtRepStaartDt').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtRepStaartDt').focus();
        }
    });

    $('#txtProjImpleDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtProjImpleDate').focus();
        }
    });

    $('#txtActualImpleDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtActualImpleDate').focus();
        }
    });

    $('#txtInspectionDT').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtInspectionDT').focus();
        }
    });

    $('#txtFirstDisbdt').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtFirstDisbdt').focus();
        }
    });

    $('#txtInstallDuedt').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtInstallDuedt').focus();
        }
    });

    $('#txtReStructureDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtReStructureDate').focus();
        }
    });

    $('#dtCommOperation').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#dtCommOperation').focus();
        }
    });

    $('#txtPrePayDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            $('#txtPrePayDate').focus();
        }
    });

    $("#imgApplicationDt").click(function () {
        $('#txtApplicationDt').datepicker("show");
        return false;
    });

   
    $("#imgSanctionDT").click(function () {
        $('#txtSanctionDT').datepicker("show");
        return false;
    });

    $("#imgSancandt").click(function () {
        $('#txtSancandt').datepicker("show");
        return false;
    });

    $("#imgRepStaartDt").click(function () {
        $('#txtRepStaartDt').datepicker("show");
        return false;
    });

    $("#imgProjImpleDate").click(function () {
        $('#txtProjImpleDate').datepicker("show");
        return false;
    });

    $("#imgActualImpleDate").click(function () {
        $('#txtActualImpleDate').datepicker("show");
        return false;
    });

    $("#imgInspectionDT").click(function () {
        $('#txtInspectionDT').datepicker("show");
        return false;
    });

    $("#imgFirstDisbdt").click(function () {
        $('#txtFirstDisbdt').datepicker("show");
        return false;
    });

    $("#imgInstallDuedt").click(function () {
        $('#txtInstallDuedt').datepicker("show");
        return false;
    });

    $("#imgReStructureDate").click(function () {
        $('#txtReStructureDate').datepicker("show");
        return false;
    });

    $("#imgCommOperation").click(function () {
        $('#dtCommOperation').datepicker("show");
        return false;
    });
});

// ======================================================== End of Date time picker ======================================================

$(document).ready(function () {                          /// Save button Click
    $("#cmdSave").click(function (event) {
        if (InsertMode == 'Add') {

            //alert($('#tblMailPhone tr').length);  
            if (hdnLoaneeUnqID == '') {  
                if ($('#tblMailPhone tr').length == 1) {
                    alert("Please Enter A Mobile Number and Email Address !");
                    $("#txtMobileNo1").focus();
                    return false;
                }
            }

            var xml = "<PhoneMail>";

            $("#tblMailPhone").find("tr").each(function (index, value) {
                if (index != 0) {
                    xml += "<Details>";
                    xml += "<Phone>";
                    xml += $(this).find("td").eq(1).text();
                    xml += "</Phone>";

                    xml += "<Mail>";
                    xml += $(this).find("td").eq(2).text();
                    xml += "</Mail>";

                    xml += "<Row_ID>";
                    xml += $(this).find("td").eq(3).text();
                    xml += "</Row_ID>";

                    xml += "</Details>";
                }
            });
            xml += "</PhoneMail>";



            $("#frmEcom").validate();
            $("#txtEmail1").rules("add", { validEmail: true, messages: { required: "Not a Valid Email Address" } });
            $("#txtMobileNo1").rules("add", { checkMobNo: true, messages: { required: "Mobile No must be 12 character" } });
            $("#txtInterestRate").rules("add", { validNumeric: true, messages: { required: "Not a Valid Numeric" } });
            $("#txtrebaterate").rules("add", { validNumeric: true, messages: { required: "Not a Valid Numeric" } });
            $("#txtPenaltyRate").rules("add", { validNumeric: true, messages: { required: "Not a Valid Numeric" } });

            $("#txtMobileNo1").valid();
            $("#txtEmail1").valid();
            $("#txtInterestRate").valid();
            $("#txtrebaterate").valid();
            $("#txtPenaltyRate").valid();
            if (!$("#frmEcom").valid()) {
                return false;
            }

        if ($('#ddlUserSector').val().trim() == '') {
            alert("Please select Unit");
            $("#ddlUserSector").css({ "background-color": "#ff9999" });
            $('#ddlUserSector').focus();
            return false;
        }
        else {
            $("#txtLoanee").css({ "background-color": "" });
        }

        if ($('#txtLoanee').val().trim() == '') {
            alert("Enter Lonee Code");
            $("#txtLoanee").css({ "background-color": "#ff9999" });
            $('#txtLoanee').focus();
            return false;
        }
        else {
            $("#txtLoanee").css({ "background-color": "" });
        }

        if ($('#txtLoaneeName').val().trim() == '') {
            alert("Enter Lonee Name");
            $("#txtLoaneeName").css({ "background-color": "#ff9999" });
            $('#txtLoaneeName').focus();
            return false;
        }
        else {
            $("#txtLoaneeName").css({ "background-color": "" });
        }

        if ($('#ddlLoanType').val().trim() == '') {
            alert("Select Loan Type");
            $("#ddlLoanType").css({ "background-color": "#ff9999" });
            $('#ddlLoanType').focus();
            return false;
        }
        else {
            $("#ddlLoanType").css({ "background-color": "" });
        }

        if ($('#txtOrginalCode').val().trim() == '') {
            alert("Enter Original Lonee Code");
            $("#txtOrginalCode").css({ "background-color": "#ff9999" });
            $('#txtOrginalCode').focus();
            return false;
        }
        else {
            $("#txtOrginalCode").css({ "background-color": "" });
        }

        //if ($('#txtOffAddress').val().trim() == '') {
        //    alert("Enter Office Address");
        //    $("#txtOffAddress").css({ "background-color": "#ff9999" });
        //    $('#txtOffAddress').focus();
        //    return false;
        //}
        //else {
        //    $("#txtOffAddress").css({ "background-color": "" });
        //}

        //if ($('#txtOffPin').val().trim() == '') {
        //    alert("Enter Office Pin Code");
        //    $("#txtOffPin").css({ "background-color": "#ff9999" });
        //    $('#txtOffPin').focus();
        //    return false;
        //}
        //else {
        //    $("#txtOffPin").css({ "background-color": "" });
        //}

        //if ($('#txtOffTel').val().trim() == '') {
        //    alert("Enter Office Telephone No.");
        //    $("#txtOffTel").css({ "background-color": "#ff9999" });
        //    $('#txtOffTel').focus();
        //    return false;
        //}
        //else {
        //    $("#txtOffTel").css({ "background-color": "" });
        //}

        if ($('#ddlRepaymentTerm').val().trim() == '') {
            alert("Select a term of Repayment");
            $("#ddlRepaymentTerm").css({ "background-color": "#ff9999" });
            $('#ddlRepaymentTerm').focus();
            return false;
        }
        else {
            $("#ddlRepaymentTerm").css({ "background-color": "" });
        }

            /// =========== Check there has any row in detail Gridview =========
        ////////var totalRowCount = $("[id*=grdLoanDetail] tr").length;
        //////////var rowCount = $("[id*=grdLoanDetail] td").closest("tr").length;     1
        //////////var message = "Total Row Count: " + totalRowCount;                   2
        //////////message += "\nRow Count: " + rowCount;                               3
        //////////alert(message);                                                      4
        ////////if (totalRowCount <= 2) {
        ////////    alert("Please add at least one Loan Detail for this Loanee.");
        ////////    $('#txtProcYearMon').focus();
        ////////    return false;
        ////////}
        $('.loading-overlay').show();
        // =================================== End of checking whether mandatory Text Box are blank or not before save ====================================================================================

        var LoaneeUnqID = '';
        var LooneeID = '';                   ///  declare Loan Master variable declaretion -----------------------
        var GLID = '';
        var LooneeCode = '';
        var LooneeName = '';
        var LoanType = '';
        var LooneeOriCode = '';

        var OfficeAdd = '';
        var OfficeAdd2 = '';
        var OfficeAdd3 = '';
        var OfficePin = '';
        var OfficeTeleNo = '';
        var FactoryAdd = '';
        var FactoryAdd2 = '';
        var FactoryAdd3 = '';
        var FactoryPin = '';
        var FactoryTeleNo = '';

        var ImplementaiotnStatus = '';
        var CommOpDate = '';
        var District = '';
        var Industry = '';
        var Constitution = '';
        var Area = '';
        var Sector = '';
        var Purpose = '';

        var ApplicationDate = '';
        var ApplicationAmount = '';
        var ProjectCost = '';
        var SanctionDate = '';
        var SanctionAmount = '';
        var SanctionCancelDate = '';
        var SanctionCancelAmount = '';
        var RepaymentTerm = '';
        var RepStartDate = '';

        var Pri_Mortgaged = '';
        var Pri_Hypothctd = '';
        var Pri_Pledged = '';
        var CoL_Mortgaged = '';
        var CoL_Hypothctd = '';
        var CoL_Pledged = '';


        var ChiefPromoter = '';
        var PwrStatus = '';
        var Product = '';
        var Consultant = '';
        var ProjectEmployment = '';
        var ActualEmployment = '';
        var ProjectedImplementionDate = '';
        var ActualImplementionDate = '';
        var SanctionBy = '';

        var MobileNo = '';
        var EmailID = '';
        var InspectionDate = '';
        var ProcessingOff = '';
        var DisburseOff = '';
        var RecoOff = '';
        var PrePayDate = '';
        var PrePayAmount = '';
        var PanNo = '';
        var GstNo = '';
  // ===========================================  Initialize variable ===============

        
            LoaneeUnqID = hdnLoaneeUnqID;
            LooneeID = txtLoneeID;
            GLID = hdnGLID;

            if ($('#txtLoanee').val().trim() == '') {
                LooneeCode = '';
            }
            else {
                LooneeCode = $('#txtLoanee').val().trim();
            }

            if ($('#txtLoaneeName').val().trim() == '') {
                LooneeName = '';
            }
            else {
                LooneeName = $('#txtLoaneeName').val().trim();
            }

            if ($('#ddlLoanType').val().trim() == '') {
                LoanType = '';
            }
            else {
                LoanType = $('#ddlLoanType').val().trim();
            }

            if ($('#txtOrginalCode').val().trim() == '') {
                LooneeOriCode = '';
            }
            else {
                LooneeOriCode = $('#txtOrginalCode').val().trim();
            }
            // ----------------------------------------------------------------------
            if ($('#txtOffAddress').val().trim() == '') {
                OfficeAdd = '';
            }
            else {
                OfficeAdd = $('#txtOffAddress').val().trim();
            }

            if ($('#txtOffAddress2').val().trim() == '') {
                OfficeAdd2 = '';
            }
            else {
                OfficeAdd2 = $('#txtOffAddress2').val().trim();
            }

            if ($('#txtOffAddress3').val().trim() == '') {
                OfficeAdd3 = '';
            }
            else {
                OfficeAdd3 = $('#txtOffAddress3').val().trim();
            }

            if ($('#txtOffPin').val().trim() == '') {
                OfficePin = '';
            }
            else {
                OfficePin = $('#txtOffPin').val().trim();
            }

            if ($('#txtOffTel').val().trim() == '') {
                OfficeTeleNo = '';
            }
            else {
                OfficeTeleNo = $('#txtOffTel').val().trim();
            }

            if ($('#txtFactoryAddr').val().trim() == '') {
                FactoryAdd = '';
            }
            else {
                FactoryAdd = $('#txtFactoryAddr').val().trim();
            }

            if ($('#txtFactoryAddr2').val().trim() == '') {
                FactoryAdd2 = '';
            }
            else {
                FactoryAdd2 = $('#txtFactoryAddr2').val().trim();
            }

            if ($('#txtFactoryAddr3').val().trim() == '') {
                FactoryAdd3 = '';
            }
            else {
                FactoryAdd3 = $('#txtFactoryAddr3').val().trim();
            }

            if ($('#txtFactoryPin').val().trim() == '') {
                FactoryPin = '';
            }
            else {
                FactoryPin = $('#txtFactoryPin').val().trim();
            }

            if ($('#txtFactoryTel').val().trim() == '') {
                FactoryTeleNo = '';
            }
            else {
                FactoryTeleNo = $('#txtFactoryTel').val().trim();
            }
            // -------------------------------------------------------------------------
            if ($('#ddlImplement').val().trim() == '') {
                ImplementaiotnStatus = '';
            }
            else {
                ImplementaiotnStatus = $('#ddlImplement').val().trim();
            }
            if ($('#dtCommOperation').val().trim() == '') {
                CommOpDate = '';
            }
            else {
                CommOpDate = $('#dtCommOperation').val().trim();
            }
            if ($('#ddlDistrict').val().trim() == '') {
                District = '';
            }
            else {
                District = $('#ddlDistrict').val().trim();
            }

            if ($('#ddlIndustry').val().trim() == '') {
                Industry = '';
            }
            else {
                Industry = $('#ddlIndustry').val().trim();
            }

            if ($('#ddlConstitution').val().trim() == '') {
                Constitution = '';
            }
            else {
                Constitution = $('#ddlConstitution').val().trim();
            }

            if ($('#ddlArea').val().trim() == '') {
                Area = '';
            }
            else {
                Area = $('#ddlArea').val().trim();
            }

            if ($('#ddlSector').val().trim() == '') {
                Sector = '';
            }
            else {
                Sector = $('#ddlSector').val().trim();
            }

            if ($('#ddlPurpose').val().trim() == '') {
                Purpose = '';
            }
            else {
                Purpose = $('#ddlPurpose').val().trim();
            }
            // ---------------------------------------------------------------------

            if ($('#txtApplicationDt').val().trim() == '') {
                ApplicationDate = '';
            }
            else {
                ApplicationDate = $('#txtApplicationDt').val().trim();
            }

            if ($('#txtApplicationAmt').val().trim() == '') {
                ApplicationAmount = '';
            }
            else {
                ApplicationAmount = $('#txtApplicationAmt').val().trim();
            }

            if ($('#txtProjCost').val().trim() == '') {
                ProjectCost = '';
            }
            else {
                ProjectCost = $('#txtProjCost').val().trim();
            }

            if ($('#txtSanctionDT').val().trim() == '') {
                SanctionDate = '';
            }
            else {
                SanctionDate = $('#txtSanctionDT').val().trim();
            }

            if ($('#txtSanctioAmt').val().trim() == '') {
                SanctionAmount = '';
            }
            else {
                SanctionAmount = $('#txtSanctioAmt').val().trim();
            }

            if ($('#txtSancandt').val().trim() == '') {
                SanctionCancelDate = '';
            }
            else {
                SanctionCancelDate = $('#txtSancandt').val().trim();
            }

            if ($('#txtSancanAmt').val().trim() == '') {
                SanctionCancelAmount = '';
            }
            else {
                SanctionCancelAmount = $('#txtSancanAmt').val().trim();
            }

            if ($('#ddlRepaymentTerm').val().trim() == '') {
                RepaymentTerm = '';
            }
            else {
                RepaymentTerm = $('#ddlRepaymentTerm').val().trim();
            }

            if ($('#txtRepStaartDt').val().trim() == '') {
                RepStartDate = '';
            }
            else {
                RepStartDate = $('#txtRepStaartDt').val().trim();
            }
            // -----------------------------------------------------------------
            if ($('#txtMortPri').val().trim() == '') {
                Pri_Mortgaged = '';
            }
            else {
                Pri_Mortgaged = $('#txtMortPri').val().trim();
            }

            if ($('#txtHypoPri').val().trim() == '') {
                Pri_Hypothctd = '';
            }
            else {
                Pri_Hypothctd = $('#txtHypoPri').val().trim();
            }

            if ($('#txtPlePri').val().trim() == '') {
                Pri_Pledged = '';
            }
            else {
                Pri_Pledged = $('#txtPlePri').val().trim();
            }

            if ($('#txtMortCol').val().trim() == '') {
                CoL_Mortgaged = '';
            }
            else {
                CoL_Mortgaged = $('#txtMortCol').val().trim();
            }

            if ($('#txtHypoCol').val().trim() == '') {
                CoL_Hypothctd = '';
            }
            else {
                CoL_Hypothctd = $('#txtHypoCol').val().trim();
            }

            if ($('#txtPleCol').val().trim() == '') {
                CoL_Pledged = '';
            }
            else {
                CoL_Pledged = $('#txtPleCol').val().trim();
            }

            // --------------------------------------------------------------------

            if ($('#txtChiefPromoter').val().trim() == '') {
                ChiefPromoter = '';
            }
            else {
                ChiefPromoter = $('#txtChiefPromoter').val().trim();
            }

            if ($('#ddlPwrStatus').val().trim() == '') {
                PwrStatus = '';
            }
            else {
                PwrStatus = $('#ddlPwrStatus').val().trim();
            }

            if ($('#txtProduct').val().trim() == '') {
                Product = '';
            }
            else {
                Product = $('#txtProduct').val().trim();
            }

            if ($('#txtConsultant').val().trim() == '') {
                Consultant = '';
            }
            else {
                Consultant = $('#txtConsultant').val().trim();
            }

            if ($('#txtProjectEmployment').val().trim() == '') {
                ProjectEmployment = '';
            }
            else {
                ProjectEmployment = $('#txtProjectEmployment').val().trim();
            }

            if ($('#txtActualEmployment').val().trim() == '') {
                ActualEmployment = '';
            }
            else {
                ActualEmployment = $('#txtActualEmployment').val().trim();
            }

            if ($('#txtProjImpleDate').val().trim() == '') {
                ProjectedImplementionDate = '';
            }
            else {
                ProjectedImplementionDate = $('#txtProjImpleDate').val().trim();
            }

            if ($('#txtActualImpleDate').val().trim() == '') {
                ActualImplementionDate = '';
            }
            else {
                ActualImplementionDate = $('#txtActualImpleDate').val().trim();
            }

            if ($('#ddlEmpSanction').val().trim() == '') {
                SanctionBy = '';
            }
            else {
                SanctionBy = $('#ddlEmpSanction').val().trim();
            }
            //----------------------------------------------------------------------------------------------------------

            if ($('#txtMobileNo1').val().trim() == '') {
                MobileNo = '';
            }
            else {
                MobileNo = $('#txtMobileNo1').val().trim();
            }

            if ($('#txtEmail1').val().trim() == '') {
                EmailID = '';
            }
            else {
                EmailID = $('#txtEmail1').val().trim();
            }

            if ($('#txtInspectionDT').val().trim() == '') {
                InspectionDate = '';
            }
            else {
                InspectionDate = $('#txtInspectionDT').val().trim();
            }

            if ($('#ddlProcessinfOFF').val().trim() == '') {
                ProcessingOff = '';
            }
            else {
                ProcessingOff = $('#ddlProcessinfOFF').val().trim();
            }

            if ($('#ddDisburseOff').val().trim() == '') {
                DisburseOff = '';
            }
            else {
                DisburseOff = $('#ddDisburseOff').val().trim();
            }

            if ($('#ddRecoOff').val().trim() == '') {
                RecoOff = '';
            }
            else {
                RecoOff = $('#ddRecoOff').val().trim();
            }
            
            PrePayDate = $('#txtPrePayDate').val();
            PrePayAmount = $('#txtPrePayAmount').val();
            if (PrePayAmount == '') { PrePayAmount = 0; }
            PanNo = $('#txtPanNo').val();
            GstNo = $('#txtGstNo').val();
            //======================================================== END ==================================================================== 

            var W = "{LoaneeUnqID:'" + LoaneeUnqID + "', LooneeID:'" + LooneeID + "', GLID:'" + GLID + "', LooneeCode:'" + LooneeCode + "', LooneeName:'" + LooneeName + "', LoanType:'" + LoanType + "', LooneeOriCode:'" + LooneeOriCode + "',      " +
                    " OfficeAdd:'" + OfficeAdd + "', OfficeAdd2:'" + OfficeAdd2 + "', OfficeAdd3:'" + OfficeAdd3 + "', OfficePin:'" + OfficePin + "', OfficeTeleNo:'" + OfficeTeleNo + "', FactoryAdd:'" + FactoryAdd + "', FactoryAdd2:'" + FactoryAdd2 + "', FactoryAdd3:'" + FactoryAdd3 + "', FactoryPin:'" + FactoryPin + "', FactoryTeleNo:'" + FactoryTeleNo + "',    " +
                    " ImplementaiotnStatus:'" + ImplementaiotnStatus + "' ,CommOpDate:'" + CommOpDate + "' ,District:'" + District + "', Industry:'" + Industry + "', Constitution:'" + Constitution + "', Area:'" + Area + "', Sector:'" + Sector + "', Purpose:'" + Purpose + "',      " +
                    " ApplicationDate:'" + ApplicationDate + "', ApplicationAmount:'" + ApplicationAmount + "', ProjectCost:'" + ProjectCost + "', SanctionDate:'" + SanctionDate + "', SanctionAmount:'" + SanctionAmount + "', SanctionCancelDate:'" + SanctionCancelDate + "', SanctionCancelAmount:'" + SanctionCancelAmount + "', RepaymentTerm:'" + RepaymentTerm + "', RepStartDate:'" + RepStartDate + "',   " +
                    " Pri_Mortgaged:'" + Pri_Mortgaged + "', Pri_Hypothctd:'" + Pri_Hypothctd + "', Pri_Pledged:'" + Pri_Pledged + "', CoL_Mortgaged:'" + CoL_Mortgaged + "', CoL_Hypothctd:'" + CoL_Hypothctd + "', CoL_Pledged:'" + CoL_Pledged + "',   " +
                    " ChiefPromoter:'" + ChiefPromoter + "', PwrStatus:'" + PwrStatus + "', Product:'" + Product + "', Consultant:'" + Consultant + "', ProjectEmployment:'" + ProjectEmployment + "', ActualEmployment:'" + ActualEmployment + "', ProjectedImplementionDate:'" + ProjectedImplementionDate + "', ActualImplementionDate:'" + ActualImplementionDate + "', SanctionBy:'" + SanctionBy + "' , " +
                    " MobileEmailXml:'" + xml + "', InspectionDate:'" + InspectionDate + "', ProcessingOff:'" + ProcessingOff + "', DisburseOff:'" + DisburseOff + "', RecoOff:'" + RecoOff + "', PrePayDate:'" + PrePayDate + "', PrePayAmount:'" + PrePayAmount + "', PanNo:'" + PanNo + "', GstNo:'" + GstNo + "'} ";
            //alert(W);
            $.ajax({
                type: "POST",
                url: "MSTAccLoan.aspx/Save_MSTAccLoan",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    if (LoaneeUnqID == '') {
                        var msg = jQuery.parseJSON(data.d);
                        if (msg == 'Fail') {
                            $('.loading-overlay').hide();
                            alert("Records not save successfully");
                            ClearLoanMaster();
                            ClearLoanDetail();
                            return false;
                        }
                        else {
                            alert("Records Save Successfully");
                            $('#btnRepaySchedule').hide();
                            $('.loading-overlay').hide();
                            ClearLoanMaster();
                            ClearLoanDetail();
                            $("#tblMailPhone tbody tr").each(function (index, value) {
                                if (index != 0) { $(this).remove(); }
                            });
                            return false;
                        }                   
                    }
                    else {
                        $("#ddlUserSector").removeAttr("disabled");
                        $("#txtLoanee").removeAttr("disabled");
                       // $("#txtLoaneeName").removeAttr("disabled");
                        $("#ddlLoanType").removeAttr("disabled");
                        $("#txtOrginalCode").removeAttr("disabled");

                        ClearLoanMaster();
                        ClearLoanDetail();
                        $('#txtLoaneeSearch').val('');
                        $('#txtLoaneeNameSearch').val('');

                        $('#txtLoanee').focus();
                        $('.loading-overlay').hide();
                        alert("Records Updated Successfully");
                        $('#btnRepaySchedule').hide();
                        $("#tblMailPhone tbody tr").each(function (index, value) {
                            if (index != 0) { $(this).remove(); }
                        });


                    }

                },
                error: function (response) {
                    ClearLoanMaster();
                    ClearLoanDetail();
                    $('.loading-overlay').hide();
                    alert("Records Not Saved!");
                    //$("#txtPODate").focus();
                    return false;
                }
            });
            return false;

           
        }
        else {
            alert("Record is in Modification Mode. Please Add details first...");
            return false;
        }

    });
});

// ================================================================  Loan Detail =============================================================================

$(document).ready(function () {

   

    var GridgrdAccGLDtl = document.getElementById("grdLoanDetail");   //main Grid Initialization!
    GridgrdAccGLDtl.deleteRow(1);
    $('#grdLoanDetail tr:last').after('<tr> ' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' + '</tr>');
    $("[id*=grdLoanDetail] tr:has(td):last").hide();

    $("#cmdAddDtl").click(function (event) {

        InsertMode = "Add";

        $("#frmEcom").validate();      
        $("#txtInterestRate").rules("add", { validNumeric: true, messages: { required: "Not a Valid Numeric" } });
        $("#txtrebaterate").rules("add", { validNumeric: true, messages: { required: "Not a Valid Numeric" } });
        $("#txtPenaltyRate").rules("add", { validNumeric: true, messages: { required: "Not a Valid Numeric" } });

        
        $("#txtInterestRate").valid();
        $("#txtrebaterate").valid();
        $("#txtPenaltyRate").valid();
        if (!$("#frmEcom").valid()) {
            return false;
        }

        var ProYear = '';                          /// Loan Detail variable declaretion ...
        var InstallmentNo1 = '';
        var InstallmentAmount1 = '';
        var FirstDisbDate = '';

        var InterestRate = '';
        var InstallmentNo2 = '';
        var InstallmentAmount2 = '';
        var InstallDueDate = '';

        var RebateRate = '';
        var InstallmentNo3 = '';
        var InstallmentAmount3 = '';
        var DisbAmount = '';

        var PenaltyRate = '';
        var InstallmentNo4 = '';
        var InstallmentAmount4 = '';
        var HealthCode = '';

        var CumlDebitAmount = '';
        var CumlCreditAmount = '';
        var TotalOS = '';
        var ServiceCharge = '';

        var PrincipleDefault = '';
        var InterestDefault = '';
        var OtherDefault = '';
        var DefaultStatusCode = '';

        var AccountStatusCode = '';
        var Status = '';
        var ReStructureDate = '';

        if ($('#txtProcYearMon').val().trim() == '') {                                /// Assign Value into variable and check whether fields are blank or not......  
            alert("Enter Year Month (YYYYMM)");
            $("#txtProcYearMon").css({ "background-color": "#ff9999" });
            $('#txtProcYearMon').focus();
            ProYear = '';
            return false;
        }
        else {
            $("#txtProcYearMon").css({ "background-color": "" });
            ProYear = $('#txtProcYearMon').val().trim();
        }

        //if ($('#txtInstallNo1').val().trim() == '') {
        //    alert("Enter Installmment No 1");
        //    $("#txtInstallNo1").css({ "background-color": "#ff9999" });
        //    $('#txtInstallNo1').focus();
        //    InstallmentNo1 = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallNo1").css({ "background-color": "" });
        //    InstallmentNo1 = $('#txtInstallNo1').val().trim();
        //}
        InstallmentNo1 = $('#txtInstallNo1').val().trim();

        //if ($('#txtInstallAmt1').val().trim() == '') {
        //    alert("Enter Installment Amount 1");
        //    $("#txtInstallAmt1").css({ "background-color": "#ff9999" });
        //    $('#txtInstallAmt1').focus();
        //    InstallmentAmount1 = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallAmt1").css({ "background-color": "" });
        //    InstallmentAmount1 = $('#txtInstallAmt1').val().trim();
        //}
        InstallmentAmount1 = $('#txtInstallAmt1').val().trim();

        //if ($('#txtFirstDisbdt').val().trim() == '') {
        //    alert("Select Disburse Date");
        //    $("#txtFirstDisbdt").css({ "background-color": "#ff9999" });
        //    $('#txtFirstDisbdt').focus();
        //    FirstDisbDate = '';
        //    return false;
        //}
        //else {
        //    $("#txtFirstDisbdt").css({ "background-color": "" });
        //    FirstDisbDate = $('#txtFirstDisbdt').val().trim();
        //}
        FirstDisbDate = $('#txtFirstDisbdt').val().trim();

        //if ($('#txtInterestRate').val().trim() == '') {
        //    alert("Enter Interest Rate");
        //    $("#txtInterestRate").css({ "background-color": "#ff9999" });
        //    $('#txtInterestRate').focus();
        //    InterestRate = '';
        //    return false;
        //}
        //else {
        //    $("#txtInterestRate").css({ "background-color": "" });
        //    InterestRate = $('#txtInterestRate').val().trim();
        //}
        InterestRate = $('#txtInterestRate').val().trim();
        

        //if ($('#txtInstallNo2').val().trim() == '') {
        //    alert("Enter Installment No 2");
        //    $("#txtInstallNo2").css({ "background-color": "#ff9999" });
        //    $('#txtInstallNo2').focus();
        //    InstallmentNo2 = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallNo2").css({ "background-color": "" });
        //    InstallmentNo2 = $('#txtInstallNo2').val().trim();
        //}
        InstallmentNo2 = $('#txtInstallNo2').val().trim();

        //if ($('#txtInstallAmt2').val().trim() == '') {
        //    alert("Enter Installment Amount 2");
        //    $("#txtInstallAmt2").css({ "background-color": "#ff9999" });
        //    $('#txtInstallAmt2').focus();
        //    InstallmentAmount2 = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallAmt2").css({ "background-color": "" });
        //    InstallmentAmount2 = $('#txtInstallAmt2').val().trim();
        //}
        InstallmentAmount2 = $('#txtInstallAmt2').val().trim();

        //if ($('#txtInstallDuedt').val().trim() == '') {
        //    alert("Select Installment Due Date");
        //    $("#txtInstallDuedt").css({ "background-color": "#ff9999" });
        //    $('#txtInstallDuedt').focus();
        //    InstallDueDate = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallDuedt").css({ "background-color": "" });
        //    InstallDueDate = $('#txtInstallDuedt').val().trim();
        //}
        InstallDueDate = $('#txtInstallDuedt').val().trim();

        //if ($('#txtrebaterate').val().trim() == '') {
        //    alert("Enter Rebate Rate");
        //    $("#txtrebaterate").css({ "background-color": "#ff9999" });
        //    $('#txtrebaterate').focus();
        //    RebateRate = '';
        //    return false;
        //}
        //else {
        //    $("#txtrebaterate").css({ "background-color": "" });
        //    RebateRate = $('#txtrebaterate').val().trim();
        //}
        RebateRate = $('#txtrebaterate').val().trim();

        //if ($('#txtInstallNo3').val().trim() == '') {
        //    alert("Enter Installament No 3");
        //    $("#txtInstallNo3").css({ "background-color": "#ff9999" });
        //    $('#txtInstallNo3').focus();
        //    InstallmentNo3 = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallNo3").css({ "background-color": "" });
        //    InstallmentNo3 = $('#txtInstallNo3').val().trim();
        //}
        InstallmentNo3 = $('#txtInstallNo3').val().trim();

        //if ($('#txtInstallAmt3').val().trim() == '') {
        //    alert("Enter Installament Amount 3");
        //    $("#txtInstallAmt3").css({ "background-color": "#ff9999" });
        //    $('#txtInstallAmt3').focus();
        //    InstallmentAmount3 = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallAmt3").css({ "background-color": "" });
        //    InstallmentAmount3 = $('#txtInstallAmt3').val().trim();
        //}
        InstallmentAmount3 = $('#txtInstallAmt3').val().trim();

        //if ($('#txtDisbAmt').val().trim() == '') {
        //    alert("Enter Disburse Amount");
        //    $("#txtDisbAmt").css({ "background-color": "#ff9999" });
        //    $('#txtDisbAmt').focus();
        //    DisbAmount = '';
        //    return false;
        //}
        //else {
        //    $("#txtDisbAmt").css({ "background-color": "" });
        //    DisbAmount = $('#txtDisbAmt').val().trim();
        //}
        DisbAmount = $('#txtDisbAmt').val().trim();

        //if ($('#txtPenaltyRate').val().trim() == '') {
        //    alert("Enter Penlaty Rate");
        //    $("#txtPenaltyRate").css({ "background-color": "#ff9999" });
        //    $('#txtPenaltyRate').focus();
        //    PenaltyRate = '';
        //    return false;
        //}
        //else {
        //    $("#txtPenaltyRate").css({ "background-color": "" });
        //    PenaltyRate = $('#txtPenaltyRate').val().trim();
        //}
        PenaltyRate = $('#txtPenaltyRate').val().trim();

        //if ($('#txtInstallNo4').val().trim() == '') {
        //    alert("Enter Installment No 4");
        //    $("#txtInstallNo4").css({ "background-color": "#ff9999" });
        //    $('#txtInstallNo4').focus();
        //    InstallmentNo4 = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallNo4").css({ "background-color": "" });
        //    InstallmentNo4 = $('#txtInstallNo4').val().trim();
        //}
        InstallmentNo4 = $('#txtInstallNo4').val().trim();

        //if ($('#txtInstallAmt4').val().trim() == '') {
        //    alert("Enter Installment Amount 4");
        //    $("#txtInstallAmt4").css({ "background-color": "#ff9999" });
        //    $('#txtInstallAmt4').focus();
        //    InstallmentAmount4 = '';
        //    return false;
        //}
        //else {
        //    $("#txtInstallAmt4").css({ "background-color": "" });
        //    InstallmentAmount4 = $('#txtInstallAmt4').val().trim();
        //}
        InstallmentAmount4 = $('#txtInstallAmt4').val().trim();

        //if ($('#txtHealthCode').val().trim() == '') {
        //    alert("Enter Health Code");
        //    $("#txtHealthCode").css({ "background-color": "#ff9999" });
        //    $('#txtHealthCode').focus();
        //    HealthCode = '';
        //    return false;
        //}
        //else {
        //    $("#txtHealthCode").css({ "background-color": "" });
        //    HealthCode = $('#txtHealthCode').val().trim();
        //}
        HealthCode = $('#txtHealthCode').val().trim();

        //if ($('#txtCumlDrAmt').val().trim() == '') {
        //    alert("Enter Cuml Debit Amount");
        //    $("#txtCumlDrAmt").css({ "background-color": "#ff9999" });
        //    $('#txtCumlDrAmt').focus();
        //    CumlDebitAmount = '';
        //    return false;
        //}
        //else {
        //    $("#txtCumlDrAmt").css({ "background-color": "" });
        //    CumlDebitAmount = $('#txtCumlDrAmt').val().trim();
        //}
        CumlDebitAmount = $('#txtCumlDrAmt').val().trim();

        //if ($('#txtCumlCrAmt').val().trim() == '') {
        //    alert("Enter Cuml Credit Amount");
        //    $("#txtCumlCrAmt").css({ "background-color": "#ff9999" });
        //    $('#txtCumlCrAmt').focus();
        //    CumlCreditAmount = '';
        //    return false;
        //}
        //else {
        //    $("#txtCumlCrAmt").css({ "background-color": "" });
        //    CumlCreditAmount = $('#txtCumlCrAmt').val().trim();
        //}
        CumlCreditAmount = $('#txtCumlCrAmt').val().trim();

        //if ($('#txtTotalOS').val().trim() == '') {
        //    alert("Enter Total O/S");
        //    $("#txtTotalOS").css({ "background-color": "#ff9999" });
        //    $('#txtTotalOS').focus();
        //    TotalOS = '';
        //    return false;
        //}
        //else {
        //    $("#txtTotalOS").css({ "background-color": "" });
        //    TotalOS = $('#txtTotalOS').val().trim();
        //}

        //if ($('#ddlServiceCharge').val().trim() == '') {
        //    alert("Enter Select Service Charge");
        //    $("#ddlServiceCharge").css({ "background-color": "#ff9999" });
        //    $('#ddlServiceCharge').focus();
        //    ServiceCharge = '';
        //    return false;
        //}
        //else {
        //    $("#ddlServiceCharge").css({ "background-color": "" });
        //    ServiceCharge = $('#ddlServiceCharge').val().trim();
        //}
        ServiceCharge = $('#ddlServiceCharge').val().trim();

        //if ($('#txtPrincipleDefault').val().trim() == '') {
        //    alert("Enter Principle Default");
        //    $("#txtPrincipleDefault").css({ "background-color": "#ff9999" });
        //    $('#txtPrincipleDefault').focus();
        //    PrincipleDefault = '';
        //    return false;
        //}
        //else {
        //    $("#txtPrincipleDefault").css({ "background-color": "" });
        //    PrincipleDefault = $('#txtPrincipleDefault').val();
        //}
        PrincipleDefault = $('#txtPrincipleDefault').val();

        //if ($('#txtInterestDef').val().trim() == '') {
        //    alert("Enter Interest Default");
        //    $("#txtInterestDef").css({ "background-color": "#ff9999" });
        //    $('#txtInterestDef').focus();
        //    InterestDefault = '';
        //    return false;
        //}
        //else {
        //    $("#txtInterestDef").css({ "background-color": "" });
        //    InterestDefault = $('#txtInterestDef').val().trim();
        //}
        InterestDefault = $('#txtInterestDef').val().trim();

        //if ($('#txtOtherDef').val().trim() == '') {
        //    alert("Enter Other Default");
        //    $("#txtOtherDef").css({ "background-color": "#ff9999" });
        //    $('#txtOtherDef').focus();
        //    OtherDefault = '';
        //    return false;
        //}
        //else {
        //    $("#txtOtherDef").css({ "background-color": "" });
        //    OtherDefault = $('#txtOtherDef').val().trim();
        //}
        OtherDefault = $('#txtOtherDef').val().trim();

        //if ($('#txtDefStatuCode').val().trim() == '') {
        //    alert("Enter Default Status Code");
        //    $("#txtDefStatuCode").css({ "background-color": "#ff9999" });
        //    $('#txtDefStatuCode').focus();
        //    DefaultStatusCode = '';
        //    return false;
        //}
        //else {
        //    $("#txtDefStatuCode").css({ "background-color": "" });
        //    DefaultStatusCode = $('#txtDefStatuCode').val().trim();
        //}
        DefaultStatusCode = $('#txtDefStatuCode').val().trim();

        //if ($('#txtAccStatuCode').val().trim() == '') {
        //    alert("Enter Account Status Code");
        //    $("#txtAccStatuCode").css({ "background-color": "#ff9999" });
        //    $('#txtAccStatuCode').focus();
        //    AccountStatusCode = '';
        //    return false;
        //}
        //else {
        //    $("#txtAccStatuCode").css({ "background-color": "" });
        //    AccountStatusCode = $('#txtAccStatuCode').val().trim();
        //}
        AccountStatusCode = $('#txtAccStatuCode').val().trim();

        //if ($('#txtStatus').val().trim() == '') {
        //    alert("Enter Status");
        //    $("#txtStatus").css({ "background-color": "#ff9999" });
        //    $('#txtStatus').focus();
        //    Status = '';
        //    return false;
        //}
        //else {
        //    $("#txtStatus").css({ "background-color": "" });
        //    Status = $('#txtStatus').val().trim();
        //}
        Status = $('#txtStatus').val().trim();

        //if ($('#txtReStructureDate').val().trim() == '') {
        //    alert("Select Re-Structure Date");
        //    $("#txtReStructureDate").css({ "background-color": "#ff9999" });
        //    $('#txtReStructureDate').focus();
        //    ReStructureDate = '';
        //    return false;
        //}
        //else {
        //    $("#txtReStructureDate").css({ "background-color": "" });
        //    ReStructureDate = $('#txtReStructureDate').val().trim();
        //}
        ReStructureDate = $('#txtReStructureDate').val().trim();

        if (InsertMode == 'Add') {

            var grdLoanDetail = document.getElementById("grdLoanDetail");
            var rowCount = '';
            for (var row = 1; row < grdLoanDetail.rows.length; row++) {
                //  rowCount = row;
                var GridIAccGl_Cell = grdLoanDetail.rows[row].cells[2];
                var YearMonth = GridIAccGl_Cell.textContent.toString();
                //alert(YearMonth);
                if (YearMonth == $("#txtProcYearMon").val()) {
                    alert("Year Month Already Exists for this Loanee.");
                    ClearLoanDetail();
                    return false;
                    break;
                }
            }

        }
        
        if (InsertMode == 'Add') {

            

            if ($('#txtProcYearMon').val() != '') {

                var InstAmt1 = ($('#txtInstallAmt1').val() == '') ? 0 : $('#txtInstallAmt1').val();
                var CumlDr = ($('#txtCumlDrAmt').val() == '') ? 0 : $('#txtCumlDrAmt').val();
                var CumlCr = ($('#txtCumlCrAmt').val() == '') ? 0 : $('#txtCumlCrAmt').val();
                var TotOS = ($('#txtTotalOS').val() == '') ? 0 : $('#txtTotalOS').val();
                var PriDefault = ($('#txtPrincipleDefault').val() == '') ? 0 : $('#txtPrincipleDefault').val();
                var InstDefault = ($('#txtInterestDef').val() == '') ? 0 : $('#txtInterestDef').val();
                var OthDefault = ($('#txtOtherDef').val() == '') ? 0 : $('#txtOtherDef').val();
               
                $('#grdLoanDetail tr:first').after('<tr style="background-color:White;">' +
                        '<td align="center">' + "<div onClick='EditMstAccLoanDetail_change(this);'><img src='images/edit.png' id='EditAccGlDetail_" + $('#txtProcYearMon').val() + "'/>" + '</td>' +
                        //'<td>' + "<div onClick='CancelAccGlDetail_change(" + $('#txtProcYearMon').val() + ");'><img src='images/close.png' id='CancelAccGlDetail_" + $('#txtProcYearMon').val() + "'/>" + '</td>' +
                        '<td align="center">' + "<div onClick='DeleteMstAccLoanDetail_change(this);'><img src='images/delete.png' id='DeleteAccGlDetail_" + $('#txtProcYearMon').val() + "'/>" + '</td>' +
                        '<td Class="labelCaption" style="text-align:center">' + $('#txtProcYearMon').val() + '</td>' +                        
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(InstAmt1).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:center">' + $('#txtHealthCode').val() + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(CumlDr).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(CumlCr).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(TotOS).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(PriDefault).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(InstDefault).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(OthDefault).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:center">' + $('#txtStatus').val() + '</td>' + '</tr>');

                var InstAmt1 = '';
                var CumlDr = '';
                var CumlCr = '';
                var TotOS = '';
                var PriDefault = '';
                var InstDefault = '';
                var OthDefault = '';

                GetLoanDetailValueToSession(ProYear, InstallmentNo1, InstallmentAmount1, FirstDisbDate,
                                            InterestRate, InstallmentNo2, InstallmentAmount2, InstallDueDate,
                                            RebateRate, InstallmentNo3, InstallmentAmount3, DisbAmount,
                                            PenaltyRate, InstallmentNo4, InstallmentAmount4, HealthCode,
                                            CumlDebitAmount, CumlCreditAmount, ServiceCharge,
                                            PrincipleDefault, InterestDefault, OtherDefault, DefaultStatusCode,
                                            AccountStatusCode, Status, ReStructureDate,
                                            QTD_PRODUCT, QTD_PRINCIPAL, QTD_INTEREST, QTD_OTHERS, INTT_CAL_MODE);
            }
            else {

                alert("Record is in Modification Mode. Please Add details first...");
            }
        }
    });
});

var QTD_PRODUCT = '0';
var QTD_PRINCIPAL = '0';
var QTD_INTEREST = '0';
var QTD_OTHERS = '0';
var INTT_CAL_MODE = '';

function EditMstAccLoanDetail_change(evt) {

    if (InsertMode == "Add") {
        var rowTR = evt.closest('tr');                // select clicked row
        var YearMonth = $(rowTR).find("td:eq(2)").text();   // get value from particular cell of selected row.
        //  var inst1 = $(rowTR).find("td:eq(3)").text();

        var W = "{YearMonth:'" + YearMonth + "'}";  /// delete row from session also.
        $.ajax({
            type: "POST",
            url: "MSTAccLoan.aspx/FetchValueFromSession",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {

                        $("#txtProcYearMon").val(data.d[i].ProYear);
                        $("#txtInstallNo1").val(data.d[i].InstallmentNo1);
                        $("#txtInstallAmt1").val((data.d[0].InstallmentAmount1) == '' ? 0 : parseFloat(data.d[i].InstallmentAmount1).toFixed(2));
                        $("#txtFirstDisbdt").val(data.d[i].FirstDisbDate);

                        $("#txtInterestRate").val((data.d[0].InterestRate) == '' ? 0 : parseFloat(data.d[i].InterestRate).toFixed(2));
                        $("#txtInstallNo2").val(data.d[i].InstallmentNo2);
                        $("#txtInstallAmt2").val((data.d[0].InstallmentAmount2) == '' ? 0 : parseFloat(data.d[i].InstallmentAmount2).toFixed(2));
                        $("#txtInstallDuedt").val(data.d[i].InstallDueDate);

                        $("#txtrebaterate").val((data.d[0].RebateRate) == '' ? 0 : parseFloat(data.d[i].RebateRate).toFixed(2));
                        $("#txtInstallNo3").val(data.d[i].InstallmentNo3);
                        $("#txtInstallAmt3").val((data.d[0].InstallmentAmount3) == '' ? 0 : parseFloat(data.d[i].InstallmentAmount3).toFixed(2));
                        $("#txtDisbAmt").val((data.d[0].DisbAmount) == '' ? 0 : parseFloat(data.d[i].DisbAmount).toFixed(2));

                        $("#txtPenaltyRate").val((data.d[0].PenaltyRate) == '' ? 0 : parseFloat(data.d[i].PenaltyRate).toFixed(2));
                        $("#txtInstallNo4").val(data.d[i].InstallmentNo4);
                        $("#txtInstallAmt4").val((data.d[0].InstallmentAmount4) == '' ? 0 : parseFloat(data.d[i].InstallmentAmount4).toFixed(2));
                        $("#txtHealthCode").val(data.d[i].HealthCode);

                        $("#txtCumlDrAmt").val((data.d[0].CumlDebitAmount) == '' ? 0 : parseFloat(data.d[i].CumlDebitAmount).toFixed(2));
                        $("#txtCumlCrAmt").val((data.d[0].CumlCreditAmount) == '' ? 0 : parseFloat(data.d[i].CumlCreditAmount).toFixed(2));
                        //$("#txtTotalOS").val(data.d[i].TotalOS);
                        $("#ddlServiceCharge").val(data.d[i].ServiceCharge);  

                        $("#txtPrincipleDefault").val((data.d[0].PrincipleDefault) == '' ? 0 : parseFloat(data.d[i].PrincipleDefault).toFixed(2));
                        $("#txtInterestDef").val((data.d[0].InterestDefault) == '' ? 0 : parseFloat(data.d[i].InterestDefault).toFixed(2));
                        $("#txtOtherDef").val((data.d[0].OtherDefault) == '' ? 0 : parseFloat(data.d[i].OtherDefault).toFixed(2));
                        $("#txtDefStatuCode").val(data.d[i].DefaultStatusCode);

                        $("#txtAccStatuCode").val(data.d[i].AccountStatusCode);
                        $("#txtStatus").val(data.d[i].Status);
                        $("#txtReStructureDate").val(data.d[i].ReStructureDate);

                        $("#txtTotalOS").val((parseFloat($("#txtCumlDrAmt").val()) - parseFloat($("#txtCumlCrAmt").val())).toFixed(2));

                        QTD_PRODUCT = data.d[i].QTD_PRODUCT;
                        QTD_PRINCIPAL = data.d[i].QTD_PRINCIPAL;
                        QTD_INTEREST = data.d[i].QTD_INTEREST;
                        QTD_OTHERS = data.d[i].QTD_OTHERS;
                        INTT_CAL_MODE = data.d[i].INTT_CAL_MODE;
                    }
                }
                evt.closest('tr').remove();
                DeleteLoanDetailValueToSession(YearMonth);
                InsertMode = "Modify";

                //alert("Record deleted successfully for this Year-Month - " + YearMonth);
            },
            error: function (result) {
                alert("Error Records Data");
            }
        });
    }
    else {
        alert("Sorry! Unable to Edit, as This record is in MODIFICATION! Please Add details first...");
    }
    
}

function DeleteMstAccLoanDetail_change(evt) {

    $('<div></div>').appendTo('body')
    .html('<div><h4>Are you sure ?</h4></div>')
    .dialog({
        modal: true,
        title: 'Delete message',
        zIndex: 10000,
        autoOpen: true,
        width: '400px',
        resizable: false,
        buttons: {
            Yes: function () {
                if (InsertMode == "Add") {

                    var rowTR = evt.closest('tr');                // select clicked row
                    var YearMonth = $(rowTR).find("td:eq(2)").text();   // get value from particular cell of selected row.
                   // var inst1 = $(rowTR).find("td:eq(3)").text();
                    //alert(YearMonth + ',' + inst1);
                    evt.closest('tr').remove();   // Remove selected row
                    DeleteLoanDetailValueToSession(YearMonth);
                    alert("Record deleted successfully for this Year-Month - " + YearMonth);
                }
                else {
                    alert("Sorry! Unable to Delete, as This record is in MODIFICATION!");
                    return false;
                }

                $(this).dialog("close");
            },
            No: function () {
                $(this).dialog("close");
            }
        },
        close: function (event, ui) {
            $(this).remove();
        }
    });
}


//$(document).ready(function(){
//    $("#grdLoanDetail tr:even").css("background-color", "#dedede");
//    $("#grdLoanDetail tr:odd").css("background-color", "#ffffff");
 
//});

function DeleteLoanDetailValueToSession(YearMonth) {

    var W = "{YearMonth:" + YearMonth + "}";  /// delete row from session also.
    $.ajax({
        type: "POST",
        url: "MSTAccLoan.aspx/DeleteSessionLoanByYearMonth",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            
        },
        error: function (result) {
            alert("Error Records Data");


        }
    });
}

function GetLoanDetailValueToSession(ProYear, InstallmentNo1, InstallmentAmount1, FirstDisbDate,
                                            InterestRate, InstallmentNo2, InstallmentAmount2, InstallDueDate,
                                            RebateRate, InstallmentNo3, InstallmentAmount3, DisbAmount,
                                            PenaltyRate, InstallmentNo4, InstallmentAmount4, HealthCode,
                                            CumlDebitAmount, CumlCreditAmount, ServiceCharge,
                                            PrincipleDefault, InterestDefault, OtherDefault, DefaultStatusCode,
                                            AccountStatusCode, Status, ReStructureDate,
                                            QTD_PRODUCT, QTD_PRINCIPAL, QTD_INTEREST, QTD_OTHERS, INTT_CAL_MODE)
{
    var CheckStatus;
    if (hdnLoaneeUnqID == "") { CheckStatus = ""; } else {
        CheckStatus = 1;
    }
    var W = "{ProYear:'" + ProYear + "', InstallmentNo1:'" + InstallmentNo1 + "', InstallmentAmount1:'" + InstallmentAmount1 + "', FirstDisbDate:'" + FirstDisbDate + "', " +
             " InterestRate:'" + InterestRate + "',InstallmentNo2:'" + InstallmentNo2 + "', InstallmentAmount2:'" + InstallmentAmount2 + "',InstallDueDate:'" + InstallDueDate + "',"+
              "RebateRate:'" + RebateRate + "',InstallmentNo3:'" + InstallmentNo3 + "',InstallmentAmount3:'" + InstallmentAmount3 + "',DisbAmount:'" + DisbAmount + "',"+
              "PenaltyRate:'" + PenaltyRate + "',InstallmentNo4:'" + InstallmentNo4 + "',InstallmentAmount4:'" + InstallmentAmount4 + "',HealthCode:'" + HealthCode + "',"+
              "CumlDebitAmount:'" + CumlDebitAmount + "',CumlCreditAmount:'" + CumlCreditAmount + "', ServiceCharge:'" + ServiceCharge + "',"+
              "PrincipleDefault:'" + PrincipleDefault + "',InterestDefault:'" + InterestDefault + "',OtherDefault:'" + OtherDefault + "',DefaultStatusCode:'" + DefaultStatusCode + "',"+
              "AccountStatusCode:'" + AccountStatusCode + "',Status:'" + Status + "',ReStructureDate:'" + ReStructureDate + "',CheckStatus:'" + CheckStatus + "',"+
              "QTD_PRODUCT:'" + QTD_PRODUCT + "',QTD_PRINCIPAL:'" + QTD_PRINCIPAL + "',QTD_INTEREST:'" + QTD_INTEREST + "',QTD_OTHERS:'" + QTD_OTHERS + "',INTT_CAL_MODE:'" + INTT_CAL_MODE + "'}";

    //alert(W);
    $.ajax({
        type: "POST",
        url: "MSTAccLoan.aspx/GET_LoanDetailsOnSession",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (JSONVal) {

            InsertMode = "Add";           
            ClearLoanDetail();

            $('#txtProcYearMon').focus();
            return false;
        },
        error: function (JSONVal) {
        }
    });
}

// =============================== Clear All Form Loan Dertail ==================================================

function ClearLoanDetail() {

    $('#txtProcYearMon').val('');
    $('#txtInstallNo1').val('');
    $('#txtInstallAmt1').val('');
    $('#txtFirstDisbdt').val('');

    $('#txtInterestRate').val('');
    $('#txtInstallNo2').val('');
    $('#txtInstallAmt2').val('');
    $('#txtInstallDuedt').val('');

    $('#txtrebaterate').val('');
    $('#txtInstallNo3').val('');
    $('#txtInstallAmt3').val('');
    $('#txtDisbAmt').val('');

    $('#txtPenaltyRate').val('');
    $('#txtInstallNo4').val('');
    $('#txtInstallAmt4').val('');
    $('#txtHealthCode').val('');

    $('#txtCumlDrAmt').val('');
    $('#txtCumlCrAmt').val('');
    $('#txtTotalOS').val('');
    $('#ddlServiceCharge').val('');

    $('#txtPrincipleDefault').val('');
    $('#txtInterestDef').val('');
    $('#txtOtherDef').val('');
    $('#txtDefStatuCode').val('');

    $('#txtAccStatuCode').val('');
    $('#txtStatus').val('');
    $('#txtReStructureDate').val('');
    $('#txtProcYearMon').focus();

    QTD_PRODUCT = '0';
    QTD_PRINCIPAL = '0';
    QTD_INTEREST = '0';
    QTD_OTHERS = '0';
    INTT_CAL_MODE = '';

}

//====================================================================================================================

// =============================== Clear All Form Loan Master ==================================================

function ClearLoanMaster() {

    hdnGLID = '';
    hdnLoaneeUnqID='';

    txtLoneeID = '';
    $('#txtLoanee').val('');
    $('#txtLoaneeName').val('');
    $('#ddlLoanType').val('');
    $('#txtOrginalCode').val('');

    $('#txtOffAddress').val('');
    $('#txtOffAddress2').val('');
    $('#txtOffAddress3').val('');
    $('#txtOffPin').val('');
    $('#txtOffTel').val('');
    $('#txtFactoryAddr').val('');
    $('#txtFactoryAddr2').val('');
    $('#txtFactoryAddr3').val('');
    $('#txtFactoryPin').val('');
    $('#txtFactoryTel').val('');

    $('#ddlDistrict').val('');
    $('#ddlIndustry').val('');
    $('#ddlConstitution').val('');
    $('#ddlArea').val('');
    $('#ddlSector').val('');
    $('#ddlPurpose').val('');

    $('#txtApplicationDt').val('');
    $('#txtApplicationAmt').val('');
    $('#txtProjCost').val('');
    $('#txtSanctionDT').val('');
    $('#txtSanctioAmt').val('');
    $('#txtSancandt').val('');
    $('#txtSancanAmt').val('');
    $('#txtRepaymentTerm').val('');
    $('#txtRepStaartDt').val('');

    $('#txtMortPri').val('');
    $('#txtHypoPri').val('');
    $('#txtPlePri').val('');
    $('#txtMortCol').val('');
    $('#txtHypoCol').val('');
    $('#txtPleCol').val('');

    $('#txtChiefPromoter').val('');
    $('#ddlPwrStatus').val('');
    $('#txtProduct').val('');
    $('#txtConsultant').val('');
    $('#txtProjectEmployment').val('');
    $('#txtActualEmployment').val('');
    $('#txtProjImpleDate').val('');
    $('#txtActualImpleDate').val('');
    $('#ddlEmpSanction').val('');

    $('#txtMobileNo1').val('');
    $('#txtEmail1').val('');
    $('#txtInspectionDT').val('');
    $('#ddlProcessinfOFF').val('');
    $('#ddDisburseOff').val('');
    $('#ddRecoOff').val('');
    $('#txtPrePayDate').val('');
    $('#txtPrePayAmount').val('');
    $('#txtPanNo').val('');
    $('#txtGstNo').val('');

    var GridAccGLDtl = document.getElementById("grdLoanDetail");

    for (var row = GridAccGLDtl.rows.length - 1; row <= GridAccGLDtl.rows.length; row--) {

        if (row == 0) {
            $('#grdLoanDetail tr:last').after('<tr> ' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' + '</tr>');
            break;
        }
        else {
            GridAccGLDtl.deleteRow(row);
        }

    }
    $("[id*=grdLoanDetail] tr:has(td):last").hide();
    return false;
}

//====================================================================================================================

// =============================================================== Focus on Text Box in Loan Detail =====================================================

$(document).ready(function () {
    $('#txtProcYearMon').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallNo1').focus();
            return false;
        }

    });

    $('#txtInstallNo1').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallAmt1').focus();
            return false;
        }

    });

    $('#txtInstallAmt1').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtFirstDisbdt').focus();
            return false;
        }

    });

    $('#txtFirstDisbdt').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInterestRate').focus();
            return false;
        }

    });

    $('#txtInterestRate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallNo2').focus();
            return false;
        }

    });

    $('#txtInstallNo2').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallAmt2').focus();
            return false;
        }

    });

    $('#txtInstallAmt2').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallDuedt').focus();
            return false;
        }

    });

    $('#txtInstallDuedt').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtrebaterate').focus();
            return false;
        }

    });

    $('#txtrebaterate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallNo3').focus();
            return false;
        }

    });

    $('#txtInstallNo3').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallAmt3').focus();
            return false;
        }

    });

    $('#txtInstallAmt3').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtDisbAmt').focus();
            return false;
        }

    });

    $('#txtDisbAmt').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtPenaltyRate').focus();
            return false;
        }

    });

    $('#txtPenaltyRate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallNo4').focus();
            return false;
        }

    });

    $('#txtInstallNo4').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInstallAmt4').focus();
            return false;
        }

    });

    $('#txtInstallAmt4').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtHealthCode').focus();
            return false;
        }

    });

    $('#txtHealthCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtCumlDrAmt').focus();
            return false;
        }

    });

    $('#txtCumlDrAmt').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtCumlCrAmt').focus();
            return false;
        }

    });

    $('#txtCumlCrAmt').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#ddlServiceCharge').focus();
            return false;
        }

    });

    $('#txtTotalOS').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#ddlServiceCharge').focus();
            return false;
        }

    });

    $('#ddlServiceCharge').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtPrincipleDefault').focus();
            return false;
        }

    });

    $('#txtPrincipleDefault').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInterestDef').focus();
            return false;
        }

    });

    $('#txtInterestDef').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtOtherDef').focus();
            return false;
        }

    });

    $('#txtOtherDef').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtDefStatuCode').focus();
            return false;
        }

    });

    $('#txtDefStatuCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtAccStatuCode').focus();
            return false;
        }

    });

    $('#txtAccStatuCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtStatus').focus();
            return false;
        }

    });

    $('#txtStatus').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtReStructureDate').focus();
            return false;
        }

    });

    $('#txtReStructureDate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#cmdAddDtl').focus();
            return false;
        }

    });
    //=================================================================END OF VALIDATTION=================================================================
});

// =================================================================================================================================

// ===================================== Loan Master All field validation ================================================================
$(document).ready(function () {

    $('#txtLoanee').attr({ maxLength: 8 });
    $('#txtLoaneeName').attr({ maxLength: 40 });
    $('#txtOrginalCode').attr({ maxLength: 8 });
    $('#txtOffAddress, #txtOffAddress2, #txtOffAddress3').attr({ maxLength: 100 });
    $('#txtFactoryAddr, #txtFactoryAddr2, #txtFactoryAddr3').attr({ maxLength: 100 });
    $('#txtOffPin').attr({ maxLength: 6 });
    $('#txtOffTel').attr({ maxLength: 20 });
    $('#txtFactoryPin').attr({ maxLength: 6 });
    $('#txtFactoryTel').attr({ maxLength: 20 });
    $('#txtChiefPromoter').attr({ maxLength: 30 });
    $('#txtProduct').attr({ maxLength: 30 });
    $('#txtConsultant').attr({ maxLength: 4 });
    $('#txtEmail1').attr({ maxLength: 50 });
    $('#txtMobileNo1').attr({ maxLength: 12 });
    //$('#txtMobileNo1').attr({ minLength: 10 });

    

    $('#txtProjCost, #txtApplicationAmt, #txtSanctioAmt, #txtSancanAmt, #txtMortPri, #txtHypoPri, #txtPlePri, #txtMortCol, #txtHypoCol, #txtPleCol').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $("#txtOffPin, #txtFactoryPin, #txtOffTel, #txtFactoryTel, #ddlProcessinfOFF, #ddDisburseOff, #ddRecoOff, #txtMobileNo1").keypress(function (event) {
        if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            event.preventDefault();
        }
    });
    $('#txtProjectEmployment, #txtActualEmployment').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $("#txtLoanee, #txtLoaneeName").keypress(function (event) {   // prevent |  in Loanee code and loanee name.
        if (event.charCode == 124) {
            event.preventDefault();
        }
    });

    


    // ------------------------------------------------------  Loan Detail Validation --------------------------------------------


    $('#txtProcYearMon').attr({ maxLength: 6 });
    $('#txtHealthCode').attr({ maxLength: 3 });
    $('#txtDefStatuCode').attr({ maxLength: 1 });
    $('#txtStatus').attr({ maxLength: 3 });
    $('#txtAccStatuCode').attr({ maxLength: 2 });

    $('#txtInstallAmt1, #txtInterestRate, #txtInstallAmt2, #txtrebaterate, #txtInstallAmt3, #txtDisbAmt, #txtPenaltyRate, #txtInstallAmt4, #txtCumlDrAmt, #txtCumlCrAmt, #txtPrincipleDefault, #txtInterestDef, #txtOtherDef').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $("#txtProcYearMon, #txtInstallNo1, #txtInstallNo2, #txtInstallNo3, #txtInstallNo4").keypress(function (event) {
        if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            event.preventDefault();
        }
    });

    $('input[type=text]').bind('copy paste cut', function (e) {
        e.preventDefault();
    });

    $('input[type=text]').keypress(function (event) {
        if (event.charCode == 34 || event.charCode == 39) {
            event.preventDefault();
        }
    });

    $('#txtOffAddress, #txtFactoryAddr').keypress(function (event) {
        if (event.charCode == 34 || event.charCode == 39) {
            event.preventDefault();
        }
    });
    SumOfDebitCredit();

    //$('#txtInstallAmt1, #txtInterestRate').css('text-align', 'right');
    

});

// ---------------------------- mobile no min length checking -----------------------------------------
function checkLength(el) {
    if (el.value.length < 10) {
        alert("Mobile Number / Contact Number must be exactly 10 digit")
        $('#txtMobileNo1').focus();
    }
}

function checkEmail(el) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (re.test(el.value)) {

        // $(this).css("background-color", "green");

    } else {
        alert("Enter a correct E-Mail Address.");
        $(this).css("background-color", "#ff9999");
        $('#txtEmail1').focus();
    }
}

//$('#txtEmail1').change(function () {
//    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

//    if (re.test($(this).val())) {

//        // $(this).css("background-color", "green");

//    } else {
//        alert("Enter a correct E-Mail Address.");
//        $(this).css("background-color", "#ff9999");
//        $('#txtEmail1').focus();
//    }
//});
// ---------------------------------------------------------------------------------------


function SumOfDebitCredit() {
    
    $('#txtCumlDrAmt').bind('input', function () {
        var debit = parseFloat(0);
        var credit = parseFloat(0);
        var total = parseFloat(0);

        if ($('#txtCumlDrAmt').val() != '') {
            debit = parseFloat($('#txtCumlDrAmt').val());

        }
        if ($('#txtCumlCrAmt').val() != '') {
            credit = parseFloat($('#txtCumlCrAmt').val());
        }
        total = parseFloat(debit) + parseFloat(credit);

        $('#txtTotalOS').val(parseFloat(total).toFixed(2));

    });

    $('#txtCumlCrAmt').bind('input', function () {
        var debit = parseFloat(0);
        var credit = parseFloat(0);
        var total = parseFloat(0);

        if ($('#txtCumlDrAmt').val() != '') {
            debit = parseFloat($('#txtCumlDrAmt').val());

        }
        if ($('#txtCumlCrAmt').val() != '') {
            credit = parseFloat($('#txtCumlCrAmt').val());
        }
        total = parseFloat(debit) + parseFloat(credit);

        $('#txtTotalOS').val(parseFloat(total).toFixed(2));

    });

    
}


// --------------------------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------- Loanee Search ---------------------------------------------------------------------------

$(document).ready(function () {
    SearchLoaneeCode();
    SearchLoaneeName();
    //SearchLoanType();
});

function SearchLoaneeCode() {
    $('#txtLoaneeSearch').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "MSTAccLoan.aspx/LoaneeAutoCompleteData",
                data: "{'LoaneeCode':'" + document.getElementById('txtLoaneeSearch').value + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            LOANEE_NAME: item.LOANEE_NAME,
                            LOANEE_UNQ_ID: item.LOANEE_UNQ_ID,
                            SiteAddress: item.SiteAddress,
                            json: item
                        }
                    }))
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        },
        focus: function (event, ui) {
            $('#txtLoaneeSearch').val(ui.item.LOANEE_NAME);

            return false;
        },
        select: function (event, ui) {
            hdnLoaneeUnqID = ui.item.LOANEE_UNQ_ID;
            return false;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li style='border-bottom:0.1px solid gray;width:405px'>")
        .append("<a style='padding-left:40px;height:auto" +
        "background-repeat:no-repeat;background-position:left center;' >" + item.LOANEE_NAME + "</a>").appendTo(ul);
    };
}



function SearchLoaneeName() {
    $(".autosuggestLoaneeName").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "MSTAccLoan.aspx/LoaneeNameAutoCompleteData",
                data: "{'LoaneeName':'" + document.getElementById('txtLoaneeNameSearch').value + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            hdnLoaneeUnqID = i.item.val;
        }
    });
}

//function SearchLoanType() {
//    $(".autosuggestLoanType").autocomplete({
//        source: function (request, response) {
//            $.ajax({
//                type: "POST",
//                contentType: "application/json; charset=utf-8",
//                url: "MSTAccLoan.aspx/LoanTypeAutoCompleteData",
//                data: "{'LoanType':'" + document.getElementById('txtLoanTypeSearch').value + "'}",
//                dataType: "json",
//                success: function (data) {
//                    //response(data.d);
//                    response($.map(data.d, function (item) {
//                        return {
//                            label: item.split('|')[0],
//                            val: item.split('|')[1]
//                        }
//                    }))
//                },
//                error: function (result) {
//                    alert("Error");
//                }
//            });
//        },
//        select: function (e, i) {
//            //$("#txtLoaneeIDSearch").val(i.item.val);
//            $("#hdnLoanTypeCode").val(i.item.val);
//        }
//    });
//}
//----------------- In the search panel Loanee id (hidden filed) will be blank for the below condition ---------------------------
$(document).ready(function () {                               
    $('#txtLoaneeSearch').keydown(function (evt) {                /// for Backspace
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            //$('#txtLoaneeIDSearch').val('');
            //$('#hdnLoaneeUnqID').val('');
        }
    });
    $('#txtLoaneeSearch').keyup(function (evt) {                  /// fro Delete     
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            //$('#hdnLoaneeUnqID').val('');
        }
    });
    //$("#txtLoaneeSearch").change(function () {                   /// for all select
    //    if ($(this).select()) {
    //       // $('#hdnLoaneeUnqID').val('');
    //        //alert('changed');
    //    }
    //});
    //-------------------------------- loanee name ----------------------------------------------
    $('#txtLoaneeNameSearch').keydown(function (evt) {                /// for Backspace
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode == 8 || iKeyCode === 46) {
            //$('#txtLoaneeIDSearch').val('');
            hdnLoaneeUnqID='';
        }
    });
    $('#txtLoaneeNameSearch').keyup(function (evt) {                  /// fro Delete     
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            hdnLoaneeUnqID='';
        }
    });
    //$("#txtLoaneeNameSearch").change(function () {                   /// for all select
    //    if ($(this).select()) {
    //        $('#hdnLoaneeUnqID').val('');
    //        //alert('changed');
    //    }
    //});
    //-------------------------------------Loan Type-------------------------------------------------
    //    $('#txtLoanTypeSearch').keydown(function (evt) {              /// for Backspace
    //        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //        if (iKeyCode == 8) {
    //            $('#hdnLoanTypeCode').val('');
    //        }
    //    });
    //    $('#txtLoanTypeSearch').keyup(function (evt) {                  /// fro Delete     
    //        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //        if (iKeyCode === 46) {
    //            $('#hdnLoanTypeCode').val('');
    //        }
    //    });
    //    $("#txtLoanTypeSearch").change(function () {                   /// fro all select
    //        if ($(this).select()) {
    //            $('#hdnLoanTypeCode').val('');
    //            //alert('changed');
    //        }
    //    });
    });
    //--------------------------------------------------------------------------------------------------------------------------------
    $(document).ready(function () {

        $("#btnLoanSearch").click(function (event) {
            if (InsertMode == "Add") {
                $("#grdLoanDetail").find("tr:not(:nth-child(1)):not(:nth-child(2))").remove();   // remove row of gridview..
                //UsertMode = "Search";
                $("#ddlUserSector").attr("disabled", "disabled");

                if ($('#ddlUserSector').val().trim() == '') {
                    alert("Please select Unit");
                    $("#ddlUserSector").css({ "background-color": "#ff9999" });
                    $('#ddlUserSector').focus();
                    return false;
                }
                else {
                    $("#txtLoanee").css({ "background-color": "" });
                }

                var loaneeUnqID = '';
                //var loaneeID = '';
                //var LoanTypeID = '';

                if ($('#txtLoaneeSearch').val() == '' && $('#txtLoaneeNameSearch').val() == '') {

                    alert("Please select a Loanee Code  before search.");
                    $("#txtLoaneeSearch").css({ "background-color": "#ff9999" });
                    $('#txtLoaneeSearch').focus();

                    //ClearLoanMaster();
                    //ClearLoanDetail();
                    return false;
                
                }
                else {
                    $("#txtLoaneeSearch").css({ "background-color": "" });
                    if (hdnLoaneeUnqID == '') {
                        alert("Please select a correct Loanee Code.");
                        $("#txtLoaneeSearch").css({ "background-color": "#ff9999" });
                        $('#txtLoaneeSearch').focus();
                        $("#txtLoaneeNameSearch").css({ "background-color": "#ff9999" });
                        $('#txtLoaneeNameSearch').val('');
                        $('#txtLoaneeSearch').val('');
                        hdnLoaneeUnqID='';
                    }
                    loaneeUnqID = hdnLoaneeUnqID;

                }

                //if ($('#txtLoanTypeSearch').val() == '') {

                //    alert("Please select Loan Type before search.");
                //    $("#txtLoanTypeSearch").css({ "background-color": "#ff9999" });
                //    $('#txtLoanTypeSearch').focus();

                //    //ClearLoanMaster();
                //    //ClearLoanDetail();
                //    return false;
                //}
                //else {
                //    $("#txtLoanTypeSearch").css({ "background-color": "" });

                //    if ($('#hdnLoanTypeCode').val() == '') {
                //        alert("Please select a correct Loan Type.");
                //        $("#txtLoanTypeSearch").css({ "background-color": "#ff9999" });
                //        $('#txtLoanTypeSearch').focus();
                //        $('#txtLoanTypeSearch').val('');
                //        $('#hdnLoanTypeCode').val('');

                //        //ClearLoanMaster();
                //        //ClearLoanDetail();
                //    }

                //    // alert($('#hdnLoanTypeCode').val());
                //    LoanTypeID = $('#hdnLoanTypeCode').val();

                //}
                if (hdnLoaneeUnqID != '') {
                    loaneeMasterSearch(loaneeUnqID);
                    loaneeSearch(loaneeUnqID);
                }
               

            }
            else {

                alert("Sorry! as This record is in MODIFICATION!");
                return false;
            }

        
        });

    });

    function loaneeMasterSearch(loaneeUnqID) {
        if (InsertMode == "Add") {
            ClearLoanMaster();
           
            $("#txtLoanee").attr("disabled", "disabled");
            //$("#txtLoaneeName").attr("disabled", "disabled");
            //$("#ddlLoanType").attr("disabled", "disabled");
            //$("#txtOrginalCode").attr("disabled", "disabled");
            var W = "{loaneeUnqID:'" + loaneeUnqID + "'}";  
            $.ajax({
                type: "POST",
                url: "MSTAccLoan.aspx/LoaneeMasterSearch",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {

                        hdnLoaneeUnqID=data.d[0].LoaneeUnqID;
                        txtLoneeID = data.d[0].LooneeID;
                        hdnGLID = data.d[0].GLID;

                        $("#txtLoanee").val(data.d[0].LooneeCode);
                        $("#txtLoaneeName").val(data.d[0].LooneeName);
                        $("#ddlLoanType").val(data.d[0].LoanType);
                        $("#txtOrginalCode").val(data.d[0].LooneeOriCode);

                        $("#txtOffAddress").val(data.d[0].OfficeAdd);
                        $("#txtOffAddress2").val(data.d[0].OfficeAdd2);
                        $("#txtOffAddress3").val(data.d[0].OfficeAdd3);
                        $("#txtOffPin").val(data.d[0].OfficePin);
                        $("#txtOffTel").val(data.d[0].OfficeTeleNo);
                        $("#txtFactoryAddr").val(data.d[0].FactoryAdd);
                        $("#txtFactoryAddr2").val(data.d[0].FactoryAdd2);
                        $("#txtFactoryAddr3").val(data.d[0].FactoryAdd3);
                        $("#txtFactoryPin").val(data.d[0].FactoryPin);
                        $("#txtFactoryTel").val(data.d[0].FactoryTeleNo);

                        $("#ddlImplement").val(data.d[0].ImpleStatus);
                        $("#dtCommOperation").val(data.d[0].CommOpDate);

                        $("#ddlDistrict").val(data.d[0].District);
                        $("#ddlIndustry").val(data.d[0].Industry);
                        $("#ddlConstitution").val(data.d[0].Constitution);   
                        $("#ddlArea").val(data.d[0].Area);
                        $("#ddlSector").val(data.d[0].Sector);
                        $("#ddlPurpose").val(data.d[0].Purpose);

                        $("#txtApplicationDt").val(data.d[0].ApplicationDate);
                        $("#txtApplicationAmt").val((data.d[0].ApplicationAmount) == '' ? 0 : parseFloat(data.d[0].ApplicationAmount).toFixed(2));
                        $("#txtProjCost").val((data.d[0].ProjectCost) == '' ? 0 : parseFloat(data.d[0].ProjectCost).toFixed(2));
                        $("#txtSanctionDT").val(data.d[0].SanctionDate);
                        $("#txtSanctioAmt").val((data.d[0].SanctionAmount) == '' ? 0 : parseFloat(data.d[0].SanctionAmount).toFixed(2));
                        $("#txtSancandt").val(data.d[0].SanctionCancelDate);
                        $("#txtSancanAmt").val((data.d[0].SanctionCancelAmount) == '' ? 0 : parseFloat(data.d[0].SanctionCancelAmount).toFixed(2));
                        $("#ddlRepaymentTerm").val(data.d[0].RepaymentTerm);
                        $("#txtRepStaartDt").val(data.d[0].RepStartDate);

                        $("#txtMortPri").val((data.d[0].Pri_Mortgaged) == '' ? 0 : parseFloat(data.d[0].Pri_Mortgaged).toFixed(2));
                        $("#txtHypoPri").val((data.d[0].Pri_Hypothctd) == '' ? 0 : parseFloat(data.d[0].Pri_Hypothctd).toFixed(2));
                        $("#txtPlePri").val((data.d[0].Pri_Pledged) == '' ? 0 : parseFloat(data.d[0].Pri_Pledged).toFixed(2));
                        $("#txtMortCol").val((data.d[0].CoL_Mortgaged) == '' ? 0 : parseFloat(data.d[0].CoL_Mortgaged).toFixed(2));
                        $("#txtHypoCol").val((data.d[0].CoL_Hypothctd) == '' ? 0 : parseFloat(data.d[0].CoL_Hypothctd).toFixed(2));
                        $("#txtPleCol").val((data.d[0].CoL_Pledged) == '' ? 0 : parseFloat(data.d[0].CoL_Pledged).toFixed(2));

                        $("#txtChiefPromoter").val(data.d[0].ChiefPromoter);
                        $("#ddlPwrStatus").val(data.d[0].PwrStatus);    
                        $("#txtProduct").val(data.d[0].Product);
                        $("#txtConsultant").val(data.d[0].Consultant);
                        $("#txtProjectEmployment").val((data.d[0].ProjectEmployment) == '' ? 0 : parseFloat(data.d[0].ProjectEmployment).toFixed(0));
                        $("#txtActualEmployment").val((data.d[0].ActualEmployment) == '' ? 0 : parseFloat(data.d[0].ActualEmployment).toFixed(0));
                        $("#txtProjImpleDate").val(data.d[0].ProjectedImplementionDate);
                        $("#txtActualImpleDate").val(data.d[0].ActualImplementionDate);
                        $("#ddlEmpSanction").val(data.d[0].SanctionBy);

                        //$("#txtMobileNo1").val(data.d[0].MobileNo);
                        //$("#txtEmail1").val(data.d[0].EmailID);
                            
                        $("#txtInspectionDT").val(data.d[0].InspectionDate);
                        $("#ddlProcessinfOFF").val((data.d[0].ProcessingOff) == '' ? 0 : parseInt(data.d[0].ProcessingOff));
                        $("#ddDisburseOff").val((data.d[0].DisburseOff) == '' ? 0 : parseInt(data.d[0].DisburseOff));
                        $("#ddRecoOff").val((data.d[0].RecoOff) == '' ? 0 : parseInt(data.d[0].RecoOff));
                        $("#txtPrePayDate").val(data.d[0].PrePayDate);
                        $("#txtPrePayAmount").val((data.d[0].PrePayAmount) == '' ? 0 : parseFloat(data.d[0].PrePayAmount));
                        $("#txtPanNo").val(data.d[0].PAN_NO);
                        $("#txtGstNo").val(data.d[0].GSTNo);

                        var xmlString = $(data.d[0].MobileNo);
                        $("#tblMailPhone tbody tr").each(function (index, value) {
                            if (index != 0) { $(this).remove(); }
                        });
                        $(xmlString).find('Loanee').each(function () {
                            var ID = Math.floor(Math.random() * 1000) + Date.now();
                            $('#tblMailPhone').append('<tr><td style="display:none" class="UserID">' + ID
                                          + '</td><td style="text-align:center">' + $(this).find('MOBILE_NO').text()
                                          + '</td><td style="text-align:center">' + $(this).find('EMAIL_ID').text()
                                          + '</td><td style="display:none">' + $(this).find('Row_ID').text()
                                          + '<td style="text-align:center"><input type="button" style="height:25px;width:90%;font-size:small" class="myButton1" value="Edit" onclick="Edit(this)"></td><td style="text-align:center"><input type="button" style="height:25px;width:90%;font-size:small" class="myButton1" value="Delete" onclick="Delete(this)"></td></tr>');
                        });

                    }
                    else {
                        alert("No record found for this Loanee Code and Loan Type. Select correct Loanee Code and Loan Type.");
                    }
                
                },
                error: function (result) {
                    alert("Error Records Data");
                }
            });
        }
    }

    function loaneeSearch(loaneeUnqID) {

        if (InsertMode == "Add") {
            $('.loading-overlay').show();
            var W = "{loaneeUnqID:'" + loaneeUnqID + "'}";  
            $.ajax({
                type: "POST",
                url: "MSTAccLoan.aspx/LoaneeSeaarch",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    if (hdnLoaneeUnqID != '') {
                        $('#btnRepaySchedule').show();
                    }
                    if (data.d.length > 0) {
                        $("#grdLoanDetail").find("tr:not(:nth-child(1)):not(:nth-child(2))").remove();
                        //   $("#grdLoanDetail").append("<tr><th>Edit</th><th>Delete</th><th>Year Month</th><th>Installment No 1</th><th>Installment Amount 1</th><th>Installment No 2</th><th>Installment Amount 2</th></tr>");                 
                        for (var i = 0; i < data.d.length; i++) {
                            $("#grdLoanDetail").append("<tr>" +
                          '<td align="center">' + "<div onClick='EditMstAccLoanDetail_change(this);'><img src='images/edit.png' id='EditAccGlDetail_" + $('#txtProcYearMon').val() + "'/>" + '</td>' +
                          '<td align="center">' + "<div onClick='DeleteMstAccLoanDetail_change(this);'><img src='images/delete.png' id='DeleteAccGlDetail_" + $('#txtProcYearMon').val() + "'/>" + '</td>' +
                          "<td Class='labelCaption' style='text-align:center'>" + data.d[i].ProYear + "</td> " +
                         '<td Class="labelCaption" style="text-align:right">' + parseFloat(data.d[i].InstallmentAmount1).toFixed(2) + '</td>' +
                         "<td Class='labelCaption' style='text-align:center'>" + data.d[i].H_code + "</td> " +
                         '<td Class="labelCaption" style="text-align:right">' + parseFloat(data.d[i].dr).toFixed(2) + '</td>' +
                         '<td Class="labelCaption" style="text-align:right">' + parseFloat(data.d[i].cr).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(data.d[i].tot).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(data.d[i].Pri_Default).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(data.d[i].Inst_Default).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:right">' + parseFloat(data.d[i].Oth_Default).toFixed(2) + '</td>' +
                        '<td Class="labelCaption" style="text-align:center">' + data.d[i].Status + '</td>' +
                          "</tr>");
                        }
                        
                    }
                    //alert("Record deleted successfully for this Year-Month - " + YearMonth);
                    $('.loading-overlay').hide();
                },
                error: function (result) {
                    alert("Error Records Data");
                }
            });
        }
    }



//--------------------------------------------------------------------------------------------------------------------------------------------------//
//================================== Check Repayment Schedule==== Date:30-12-2016===================================================================//
    $(document).ready(function () {
        $('#btnRepaySchedule').click(function (e) {
            $('.loading-overlay').show();
            e.preventDefault();
            var D = "{loaneeUnqID:'" + hdnLoaneeUnqID + "',SectorID:'" + $('#ddlUserSector').val() + "'}";
            $.ajax({
                type: "POST",
                url: "MSTAccLoan.aspx/GET_CheckRepaymentSchedule",
                contentType: "application/json;charset=utf-8",
                data: D,
                dataType: "json",
                success: function (D) {
                    var xmlDoc = $.parseXML(D.d);
                    var xml = $(xmlDoc);
                    var table = $(xml).find('Table');
                    if ($(table).find('LOANEE_UNQ_ID').text() != '') {
                        var i = 1;
                        $("#tblRepayDetails tbody tr").each(function (index, value) {
                            if (index != 0) { $(this).remove(); }
                        });
                        $(xml).find('Table').each(function () {
                            $('#tblRepayDetails').append('<tr><td>' + i++ + '</td><td>' + $(this).find('LOANEE_NAME').text() + '</td><td style="text-align:center"><input type="button" style="height:25px;width:120px" class="myButton1" value="Show Details" onclick="ShowDetails(' + $(this).find('ScheduleID').text() + ',' + $(this).find('LOANEE_UNQ_ID').text() + ')"></td></tr>');
                            $("#tblRepayDetails").show();
                        });
                    }
                    else {
                        $("#tblRepayDetails").hide();
                        $('#tableDiv').empty();
                        $('#tableDiv').append('<h1 style="text-align:left;font-size: 13px;">Sorry ! No Existing Records .Click Up Button To Create New Repayment Schedule...!</h1>');
                    }
                    $('.loading-overlay').hide();
                },
            });
            $("#dialog").dialog({
                modal: true,
                width: 700,
                resizable: false,
                position: ['middle', 60],
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        });
        $('#btnNewSchedule').click(function (e) {
            e.preventDefault();
            $("#dialog").dialog("close");
            ShowDetails('', hdnLoaneeUnqID);
        });
    });
   

    function ShowDetails(ScheduleID,LoaneeID) {
        var StrLoaneeID = '';
        var StrSanctionAmt = '';
        var StrRepaymentTerm = '';
        var StrRepayStartDate = '';
        var Data = "";
        if (ScheduleID != '') {
             Data = "{StrLoaneeID:'" + LoaneeID + "',ScheduleID:'" + ScheduleID + "',StrSanctionAmt:'" + $('#txtSanctioAmt').val() + "',StrRepaymentTerm:'" + $('#ddlRepaymentTerm').val() + "',StrRepayStartDate:'" + $('#txtRepStaartDt').val() + "'}";
        }
        else {
            Data = "{StrLoaneeID:'" + LoaneeID + "',ScheduleID:'" + ScheduleID + "',StrSanctionAmt:'" + $('#txtSanctioAmt').val() + "',StrRepaymentTerm:'" + $('#ddlRepaymentTerm').val() + "',StrRepayStartDate:'" + $('#txtRepStaartDt').val() + "'}";
        }
        $.ajax({
            type: "POST",
            url: "MSTAccLoan.aspx/Post_DataForSession",
            contentType: "application/json;charset=utf-8",
            data: Data,
            dataType: "json",
            success: function (D) {
                window.open('RePayment_Schedule.aspx');
                return false;
            },
        });
    }
//
$(document).ready(function () {
    $('#btnPrint').on('click', function (e) {
        e.preventDefault();
        if (hdnLoaneeUnqID == '') { alert('Please Select Loanee And Search Click..!'); $('#txtLoaneeSearch').focus(); return false; }
        if ($('#txtProcYearMon').val() == '') { alert('Please Select Loanee Detail And Click On Edit..!'); $('#txtProcYearMon').focus(); return false; }
        ShowReport();
    });
        $('#btnMailPhone').on('click', function (e) {
            var t = true;
            e.preventDefault();
            if ($('#txtMobileNo1').val() == '') {
                alert("This Field Is Required..!");
                $('#txtMobileNo1').focus();
                return false;
            }
            if ($('#txtMobileNo1').val().length < 10) {
                alert("Mobile Number must be exactly 10 digit")
                $('#txtMobileNo1').focus();
                return false;
            }
            if ($('#txtEmail1').val() == '') {
                alert("This Field Is Required..!");
                $('#txtEmail1').focus();
                return false;
            }

            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if (!emailReg.test($('#txtEmail1').val())) {
                alert("Please enter valid email id");
                $('#txtEmail1').focus();
                return false;
            }
            if ($(this).val() == 'Update') {
                $('#tblMailPhone').find('tr').each(function () {
                    if (parseInt($(this).find('td:eq(0)').text()) == parseInt(hdbPhoneID)) {
                        $(this).find('td:eq(1)').text($('#txtMobileNo1').val());
                        $(this).find('td:eq(2)').text($('#txtEmail1').val());
                        hdbPhoneID='';
                        $('#btnMailPhone').val('Add');
                        t = false;
                        $('.clr').val('');
                        return false;
                    }
                });

                if (!t) { return false; }
            }
            else {
                $('#tblMailPhone tr').each(function () {
                    if ($(this).find('td:eq(1)').text() == $('#txtMobileNo1').val() || $(this).find('td:eq(2)').text() == $('#txtMobileNo1').val()) {
                        alert('This  Data Alredy Exits..!');
                        return false;
                        t = false;
                    }
                });
                if (!t) { return false; } else {
                    var ID = Math.floor(Math.random() * 1000) + Date.now();
                    $('#tblMailPhone').append('<tr><td style="display:none" class="UserID">' + ID
                                  + '</td><td style="text-align:center">' + $('#txtMobileNo1').val()
                                  + '</td><td style="text-align:center">' + $('#txtEmail1').val()
                                 //  + '</td><td style="text-align:center">' + $('#txtEmail1').val()
                                  + '<td style="text-align:center"><input type="button" style="height:25px;width:90%;font-size:small" class="myButton1" value="Edit" onclick="Edit(this)"></td><td style="text-align:center"><input type="button" style="height:25px;width:90%;font-size:small" class="myButton1" value="Delete" onclick="Delete(this)"></td></tr>');
                    $('.clr').val('');
                }
            }
        });
    });

    //===========================Start Edit Inventory/Stock On Gridview  =================================//
    function Edit(a) {
        var b = $(a).closest('tr');
        hdbPhoneID=$(b).closest('tr').find('td:eq(0)').text();
        $('#txtMobileNo1').val($(b).closest('tr').find('td:eq(1)').text());
        $('#txtEmail1').val($(b).closest('tr').find('td:eq(2)').text());
        $('#btnMailPhone').val('Update');
    }
    //===========================End Edit Inventory/Stock On Gridview  =================================//
    function Delete(a) {
        var b = $(a).closest('tr');
        b.remove();
        $('.clr').val('');
        $('#btnMailPhone').val('Add');
    }
    //============================End coding for Inventory/Stock On Gridview =========================================//

//================================ Start Code For Loan Print =======================================================//
function ShowReport() {
    var FormName = '';
    var ReportName = '';
    var ReportType = '';
    var LoaneeID = '';
    var YearMonth = '';   
    var SectorID = '';
    FormName = "MSTAccLoan.aspx";
    ReportName = "LoanMaster";
    ReportType = "Loan Master";
    LoaneeID = hdnLoaneeUnqID;
    YearMonth = $('#txtProcYearMon').val();
    SectorID = $("#ddlUserSector").val();
    var E = '';
    E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  LoaneeID:" + LoaneeID + ",  YearMonth:'" + YearMonth + "',SectorID:" + SectorID + "}";
    //alert(E);
    $.ajax({
        type: "POST",
        url: pageUrl + '/SetReportValue',
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var show = response.d;
            if (show == "OK") {
                window.open("ReportView.aspx?E=Y");
            }

        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

//================================ End Code For Loan Print =======================================================//