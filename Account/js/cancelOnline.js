﻿
var checkBoxCount = 0;
$(document).ready(function () {
    $(".loading-overlay").show();
    $(".loading-overlay").hide();
    
    window.cancelPage = new WBFDC.CancelPage();
    window.cancelPage.init();
    $(".DefaultButton").click(function (event) {        
        event.preventDefault();
    });
    if ($.trim($("#txtPermitID").val()) != "") {
        window.cancelPage.cancelInfo.SearchedClicked();
    }

});
WBFDC.CancelInfo = function () {
    var A = this;
    this.init = function () {
        $("#cmdSearch").click(A.SearchedClicked);
        $("#button-cancel-details").click(A.CancelClicked);
    };
    this.SearchedClicked = function () {

        if ($("#txtPermitID").val() != "") {//&& $("#txtPermitID").val().trim().length() == 11
            window.cancelPage.SearchDetails();
        } else {
            alert("Invalid Permit ID.");
        }
    };
    this.CancelClicked = function () {
        var checkValue = window.cancelPage.GetCheckBoxStatus();
        if (checkValue) {
            window.cancelPage.UpdateOtherInfo();
        } else {
            alert("Select Date to cancel !! ");
            return false;
        }
    };
};
WBFDC.CancelPage = function () {
    var A = this;
    this.cancelForm;
    this.init = function () {
        this.cancelInfo = new WBFDC.CancelInfo();
        this.cancelInfo.init();
    };
    this.SearchDetails = function () {
        $(".loading-overlay").show();
        var FDCID = "FDC/" + $("#txtPermitID").val();
        var E = "{FDCID :'" + FDCID + "',CancelType:'" + $("#cancelType").val() + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/GetCancellationDetails',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnSearchDetailsProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.OnSearchDetailsProcessed = function (response) {
        A.cancelForm = JSON.parse(response.d);
        //alert(JSON.stringify(A.cancelForm));
        A.showBookingCancelInfo();
        $(".loading-overlay").hide();
    };

    this.showBookingCancelInfo = function () {
        var html = '';
        var B = A.cancelForm.CancelMaster;
        var cancelType = $("#cancelType").val();
        if (A.cancelForm.AdminIDSuccess == true) {
            if (A.cancelForm.FullCancelInfo == true) {
                html += "<hr style='border:solid 1px #6d760b' /><br/>";
                html += "<table class='headingCaption' Width='100%' align='center'><tr><td>Booking Cancellation</td></tr></table> "
                        + "<br />";
                html += "<table width='100%' cellpadding='5' style='border:solid 1px #6d760b;' cellspacing='0'><tr><td>";
                html += "<table align='center' id='detailTable'  cellpadding='3' width='100%'>"
                        + "<tr>"
                        + "<td class='labelCaption'>Permit No</td> "
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.FDCID + " </td>"
                        + " <td class='labelCaption' >Date Of Booking</td>"
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.BookingDate + " </td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td class='labelCaption'>Name</td> "
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'colspan='4' > " + B.Name + " </td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td class='labelCaption'>Address</td> "
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'colspan='4' > " + B.Address + " </td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td class='labelCaption'>Phone</td> "
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.Mobile + " </td>"
                        + " <td class='labelCaption' >Email</td>"
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.Email + " </td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td class='labelCaption'>Adult(s)</td> "
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.NOA + " </td>"
                        + " <td class='labelCaption' >Children</td>"
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.NOC + " </td>"
                        + "</tr>"
                        + "<tr>"
                        + "<td class='labelCaption'>Location</td> "
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.LocationName + " </td>"
                        + " <td class='labelCaption' >Accomodation</td>"
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.AccomodationName + " </td>"
                        + "</tr>"
                         + "<td class='labelCaption'>Date of Journey</td> "
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.DOJ + " </td>";
                if (B.Tax > 0) {
                    html += " <td class='labelCaption' >Luxury Tax</td>"
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + B.Tax + " </td>";
                }
                html += "</tr>";
                html += "<tr>"
                        + "<td class='labelCaption'>Extra Cot + Charge</td> "
                        + "<td class='labelCaption'>:</td>"
                        + "<td align='left' class='tr'>" + (B.ExtraCot).toString() + " ( Rs " + (B.CotValue).toString() + " /- extra) " + " </td>"
                        + " <td class='labelCaption' >Total Price</td>"
                        + "<td class='labelCaption'>:</td>"
                // + "<td align='left' class='tr'>" + (B.Charges).toString() + " + " + (B.CancelFlag == "N" ? (B.CotValue).toString() : (0).toString()) + " + " + (B.Tax).toString() + " = " + (B.TotalAmount + B.Tax) + "/- </td>"
                        + "<td align='left' class='tr'>" + (B.Charges).toString() + " + " + (B.CotValue).toString() + " + " + (B.Tax).toString() + " = " + (B.TotalAmount + B.Tax) + "/- </td>"
                        + "</tr>";
                html += "<tr><td colspan='6'>";

                var bookedDates = WBFDC.Utils.fixArray(A.cancelForm.BookedDates);
                if (bookedDates != null && bookedDates.length > 0) {

                    html += "<hr style='border:solid 1px #6d760b' /><br/>";
                    html += "<table class='headingCaption' Width='100%' align='center'><tr><td>Booking Details<span style='float:right;'>Part cancellation (dates to cancel)</span></td></tr></table> "
                        + "<br />";
                    html += "<div style='float:left;width:33%'>"
                    html += "<table width='99%' align='center' id='bookingDateTable' cellpadding='5' style='border:solid 1px #6d760b;-moz-box-shadow: inset 0.9px 1px 3px #a7b322;	-webkit-box-shadow: inset 0.9px 1px 3px #a7b322;box-shadow: inset 0.9px 1px 3px #a7b322;' cellspacing='0'>"
                            + "<thead>"
                            + "<tr>"
                            + "<td class='th'>Booking Date</td>"
                            + "<td class='th'><input type='checkbox' id='chkFull' onclick='window.cancelPage.CheckUncheckAll(this);' name='chkFull' " + (cancelType == "G" ? (B.DateDiff < 10 ? "disabled" : "") : "") + " /> <label for='chkFull'  >Full Cancel</label> </td>"
                            + "</tr>"
                            + "</thead>";
                    html += "<tr><td colspan='2' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                    html += "<tbody>";
                    checkBoxCount = 0;
                    //A.cancelForm.NewDateRange = bookedDates;
                    for (var k in bookedDates) {
                        html += "<tr>"
                                + "<td class='tr'>" + bookedDates[k].BookedDate + "</td>"
                                + "<td class='tr'><input type='checkbox' onclick='window.cancelPage.checkuncheck(this)' value='" + bookedDates[k].BookedDate + "' id='chkDates_" + (k) + "' name='chkDates_" + (k) + "' " + (cancelType == "G" ? (B.DateDiff < 10 ? "disabled" : "") : "") + " />  </td>"
                                + "</tr>";
                        checkBoxCount++;
                    }
                    html += "</tbody>";
                    html += "</table>";
                    html += "</div>"
                    html += "<div style='float:left;width:67%' >";
                    html += "<table width='99%' align='center' cellpadding='5' style='border:solid 1px #6d760b;-moz-box-shadow: inset 0.9px 1px 3px #a7b322;	-webkit-box-shadow: inset 0.9px 1px 3px #a7b322;box-shadow: inset 0.9px 1px 3px #a7b322;' cellspacing='0'>"
                            + "<tr>"
                            + "<td class='labelCaption'>Total Amount</td> "
                            + "<td class='labelCaption'>:</td>"
                            + "<td align='left' ><input type='text' Class='inputbox2' disabled style='width:70px;text-align:right;' value='" + B.TotalAmount + "' id='txtTotalAmount' /> </td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td class='labelCaption'>Deduction Rate</td> "
                            + "<td class='labelCaption'>:</td>"
                            + "<td align='left' class='labelCaption' ><input type='text' Class='inputbox2' "
                            + "style='width:40px;text-align:right;' value='" + B.DeductionRate + "' " + (cancelType != 'S' ? "readonly" : "") + " id='txtDeductionRate' /> "
                            + "&nbsp; % of Cancelled Amount &nbsp;"
                            + "<input type='text' Class='inputbox2' style='width:60px;text-align:right;' value='0'  " + (cancelType != 'S' ? "readonly" : "") + "  id='txtCancelledAmount' /> "
                            + "</td>"
                            + "</tr>"
                            + "<tr>"
                            + "<td class='labelCaption'>Retained </td> "
                            + "<td class='labelCaption'>:</td>"
                            + "<td  class='labelCaption' align='left'><input type='text' Class='inputbox2' "
                            + " style='width:40px;text-align:right;' value='0' " + (cancelType != 'S' ? "readonly" : "") + " id='txtRetainedAmount' /> "
                            + "&nbsp; Refund : &nbsp;"
                            + "<input type='text' Class='inputbox2' style='width:40px;text-align:right;' value='0'  " + (cancelType != 'S' ? "readonly" : "") + "  id='txtRefundAmount' /> "
                            + "<input type='text' Class='inputbox2' style='width:40px;text-align:right;' value='0'  " + (cancelType != 'S' ? "readonly" : "") + "  id='txtRefundTax' /> "
                            + "&nbsp; = &nbsp;"
                            + "<input type='text' Class='inputbox2' style='width:40px;text-align:right;' value='0'  " + (cancelType != 'S' ? "readonly" : "") + "  id='txtRefundTotal' /> "
                            + "</td>"
                            + "</tr>"
                            ;
                    html += "</table>";
                    html += "</div>"
                    $("#button-cancel-details").css("display", "block");
                } else {
                    html += "<hr style='border:solid 1px lightblue' /><br/>";
                    html += "<table class='headingCaption' Width='100%' align='center'><tr><td>Booking Details<span style='float:right;'>Part cancellation (dates to cancel)</span></td></tr></table> "
                        + "<br />";
                    html += "<div style='float:left;width:100%'>"
                    html += "<table width='99%' align='center' id='bookingDateTable' cellpadding='5' style='border:solid 1px lightblue;-moz-box-shadow: inset 0.9px 1px 3px #a2c7d3;	-webkit-box-shadow: inset 0.9px 1px 3px #a2c7d3;box-shadow: inset 0.9px 1px 3px #a2c7d3;' cellspacing='0'>"
                            + "<thead>"
                            + "<tr>"
                            + "<td>Booking Dates Not Avaliable For Cancellation.</td>"
                            + "</tr>"
                            + "</thead>"
                            + "</table>"
                            +"</div>";
                }
                html += "</td></tr>";
                html += "</table>";
                html += "</td></tr></table>";


                $("#innerTable").empty().html(html);

            } else {
                alert(A.cancelForm.FullCancelMessage);
            }
        } else {
            alert(A.cancelForm.AdminSuccessMessage);
        }
    };

    this.GetCheckBoxStatus = function () {
        var flag = false;
        $("#bookingDateTable tbody tr td:last-child input:checkbox").each(function () {
            if (this.checked) {
                flag = true;
                return;
            }
        });
        return flag;
    };
    this.UpdateOtherInfo = function () {
        var B = window.cancelPage.cancelForm;
        var fullCancel = $("#chkFull").is(":checked");
        B.FullCancelFlag = fullCancel;
        B.NewDateRange = [];
        B.OldDateRange = [];
        $("#bookingDateTable tbody tr td:last-child input:checkbox").each(function () {
            var chkValue = this.value;
            if (this.checked) {
                B.OldDateRange.push({
                    BookedDate: chkValue
                });
            } else {
                B.NewDateRange.push({
                    BookedDate: chkValue
                });
            }
        });

        var arr = WBFDC.Utils.fixArray(B.NewDateRange);
        var newdatestring = "", olddatestring = "";
        for (var j in arr) {
            newdatestring += arr[j].BookedDate + ",";
        }
        if (newdatestring.length > 0) {
            newdatestring = newdatestring.substring(0, newdatestring.length - 1);
        }
        arr = WBFDC.Utils.fixArray(B.OldDateRange);
        for (var j in arr) {
            olddatestring += arr[j].BookedDate + ",";
        }
        if (olddatestring.length > 0) {
            olddatestring = olddatestring.substring(0, olddatestring.length - 1);
        }

        B.CancelMaster.NewDateRange = newdatestring;
        B.CancelMaster.OldDateRange = olddatestring;
        //alert(JSON.stringify(window.cancelPage.cancelForm));
        A.PostCancelData();

    };

    this.PostCancelData = function () {
        $(".loading-overlay").show();
        var B = A.cancelForm;
        var cancelType = $("#cancelType").val();
        var E = "{\'CancelForm\':\'" + JSON.stringify(B) + "\',CancelType:'" + cancelType + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SubmitCancellationDetails',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnSubmitCancelProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.OnSubmitCancelProcessed = function (response) {
        A.cancelForm = JSON.parse(response.d);
        //alert(JSON.stringify(A.cancelForm));
        $(".loading-overlay").hide();
        A.setPageRedirection();
    };
    this.setPageRedirection = function () {
        var B = A.cancelForm;
        if (B.CancelID > 0) {
            //window.location.href = "CancellationComplete.aspx?FDCID=" + B.CancelMaster.FDCID + "&CancelID=" + B.CancelID + "&FullCancelFlag=" + (B.FullCancelFlag ? "Y" : "N");
            alert(B.OnlineResponseMessage);
            window.location.href = B.RedirectionURL;
        }
    };

    this.PercentChange = function () {
        var B = window.cancelPage.cancelForm;
        var cancelType = $("#cancelType").val();
        var CancelledAmount = eval($("#txtCancelledAmount").val());
        var DeductionRate = eval($("#txtDeductionRate").val());
        var DeductionAmt = 0, RefundAmt = 0, RefundTaxAmt = 0, TotalRefund = 0;
        DeductionAmt = (eval(CancelledAmount) * eval(DeductionRate)) / 100;
        RefundAmt = CancelledAmount - DeductionAmt;
        TotalRefund = RefundAmt + RefundTaxAmt;
        $("#txtRetainedAmount").val(DeductionAmt);
        $("#txtRefundAmount").val(RefundAmt);
        $("#txtRefundTax").val(RefundTaxAmt);
        $("#txtRefundTotal").val(TotalRefund);
        B.CancelMaster.CancelledAmount = CancelledAmount;
        B.CancelMaster.RetainedAmount = DeductionAmt;
        B.CancelMaster.RefundAmount = RefundAmt;
        B.CancelMaster.RefundTax = RefundTaxAmt;
        B.CancelMaster.RefundTotal = TotalRefund;

    };
    this.checkuncheck = function (chk) {
        var $this = $(chk);
        var checkedStatus = $this.is(":checked");
        var B = window.cancelPage.cancelForm;

        var chkValue = $this.val();
        //alert(checkedStatus + " " + chkValue);

        var noofdays = B.CancelMaster.NoOfDays;
        var AccoCharges = B.CancelMaster.Charges;
        var CotAmount = B.CancelMaster.CotValue; //(B.CancelMaster.CancelFlag == "Y" ? 0 : B.CancelMaster.CotValue);
        var AccoChargesPerDay = (AccoCharges / noofdays);
        var CotAmountPerDay = (CotAmount == 0 ? 0 : (CotAmount / noofdays));

        var CancelledAmount = eval($("#txtCancelledAmount").val());

        if (checkedStatus) {


            //            B.OldDateRange.push({
            //                BookedDate: chkValue
            //            });
            //            A.findAndRemove2(B.NewDateRange, "BookedDate", chkValue);
            B.CancelMaster.NoOfCancelledDays = B.CancelMaster.NoOfCancelledDays + 1;
            $("#txtCancelledAmount").val(eval(CancelledAmount + AccoChargesPerDay + CotAmountPerDay));
            A.PercentChange();
        } else {
            //            B.NewDateRange.push({
            //                BookedDate: chkValue
            //            });
            //            A.findAndRemove2(B.OldDateRange, "BookedDate", chkValue);
            B.CancelMaster.NoOfCancelledDays = B.CancelMaster.NoOfCancelledDays - 1;
            $("#txtCancelledAmount").val(eval(CancelledAmount - (AccoChargesPerDay + CotAmountPerDay)));
            A.PercentChange();
        }
        //alert(B.CancelMaster.NoOfCancelledDays + '  ' + checkBoxCount);
        if (parseInt(B.CancelMaster.NoOfCancelledDays) == parseInt(checkBoxCount)) {
            $("#chkFull").prop("checked", true);
        } else {
            $("#chkFull").prop("checked", false);
        }

    };
    this.CheckUncheckAll = function (chk) {
        var $this = $(chk);
        var checked_status = $this.is(":checked");
        //alert($this.parents);
        //        var ck = $this.parents('table:eq(0)').find(':checkbox');
        //        ck.attr('checked', checked_status);
        //        alert(ck.val());
        //        if (checked_status == true) {
        //        }
        //        alert(checked_status);
        var B = window.cancelPage.cancelForm;

        //$this.closest('tbody').find('input:checkbox').each(function () {
        if (checked_status) {
            B.CancelMaster.NoOfCancelledDays = 0;
        }
        $("#bookingDateTable tbody tr td:last-child input:checkbox").each(function () {
            this.checked = checked_status;
            //alert(this.value + " " + this.value.length);
            if (checked_status == true) {
                if (this.value.length > 2) {
                    //                    B.OldDateRange.push({
                    //                        BookedDate: this.value
                    //                    });
                    //                    A.findAndRemove2(B.NewDateRange, "BookedDate", this.value);
                    B.CancelMaster.NoOfCancelledDays = B.CancelMaster.NoOfCancelledDays + 1;
                }
            } else {
                if (this.value.length > 2) {
                    //                    B.NewDateRange.push({
                    //                        BookedDate: this.value
                    //                    });
                    //                    A.findAndRemove2(B.OldDateRange, "BookedDate", this.value);
                    B.CancelMaster.NoOfCancelledDays = B.CancelMaster.NoOfCancelledDays - 1;
                }
            }
        });

        if (checked_status) {
            $("#txtCancelledAmount").val(eval($("#txtTotalAmount").val()));
            A.PercentChange();
        } else {
            $("#txtCancelledAmount").val(eval(0));
            A.PercentChange();
        }

        //alert(JSON.stringify(window.cancelPage.cancelForm));

    };
    this.findAndRemove2 = function (array, property, value) {
        $.each(array, function (index, result) {
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    this.findData = function (array, property, value) {
        var found = false;
        $.each(array, function (index, result) {
            if (result[property] == value) {
                //Remove from array
                found = true;
                return;
            }
        });
        return found;
    };
};