﻿$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    $("#txtDate").keypress(function (event) {
        event.preventDefault();
    });
    $("#txtMonthYear").keypress(function (event) {
        event.preventDefault();
    });
    $('#txtDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });

    $('#txtMonthYear').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy'
    }).focus(function () {
        var thisCalendar = $(this);
        $('.ui-datepicker-calendar').detach();
        $('.ui-datepicker-close').click(function () {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            thisCalendar.datepicker('setDate', new Date(year, month, 1));
        });
    });
});


$(document).ready(function () {
    $("#ddlReportName").change(function (event) {
        if ($("#ddlReportName").val() == '2') {
            $("#divdmy").hide();
            $("#divtxtdmy").hide();
            $("#divBndType").hide();
            $("#divddlBond").hide();
            $("#divGridSummary").hide();
            $("#txtDate").val('');
            $("#ddlBondType").val('0');

            $("#btnPrint").attr('disabled', false);

            $("#divmY").show();
            //$("#divXYZ").show();
            $("#divtxtSecID").show();
            $("#divtxtmy").show(); 
        } else {
            $("#divdmy").show();
            $("#divtxtdmy").show();
            $("#divBndType").show();
            $("#divddlBond").show();
            $("#divGridSummary").show();

            $("#btnPrint").attr('disabled', true);

            $("#divmY").hide();
            $("#divXYZ").hide();
            $("#divtxtSecID").hide();
            $("#divtxtmy").hide();
            $("#txtMonthYear").val('');
            $("#txtSecID").val('');

        }
    });
});

$(document).ready(function () {
    $("#cmdSearch").click(function (event) {
        if ($("#ddlReportName").val() == '1')
            {
        var msg = '';
        if ($("#txtDate").val().trim() == '')
        { alert("Please enter AS on Date !!"); return false; }
        if ($("#ddlUserSector").val() == "")
        { alert("Please select a Unit name from list!!");return false;}
       
        if ($("#ddlBondType").val() == "0")
        { alert("please select a Bond Type !!!"); return false; }
        //if (msg != '')
        //{ alert(msg); return false; }
        //else { 
        GridSummary_Populate($("#txtDate").val().trim(), $("#ddlBondType").val()); 
    }
    });
});

function GridSummary_Populate(ASonDATE, BondType) {
    var T = "{ASonDATE:'" + ASonDATE + "',BondType:'" + BondType + "'}";

    $.ajax({
        type: "POST",
        url: "BondTypeSummaryReport.aspx/Grid_Bind",
        contentType: "application/json;charset=utf-8",
        data: T,
        dataType: "json",
        success: function (data) {
            $("#GridSummary").empty();
            $("#GridSummary").append("<tr><th style='height:20px;background-color:#0066ff;'>Sl. No. </th><th style='height:20px;background-color:#0066ff;'> Bond Type </th><th style='height:20px;background-color:#0066ff;'> ISIN No. </th><th style='height:20px;background-color:#0066ff;'> Amount </th></tr>");
            if (data.d.length > 0) {
                var slNo = '';
                for (var i = 0; i < data.d.length; i++) {
                    slNo = i + 1;
                    $("#GridSummary").append("<tr><td style='height:20px;background-color:honeydew;color:black;'>" +
                                         slNo   + "<td style='height:20px;background-color:honeydew;color:black;'>" +
                           data.d[i].SCHEME_Name + "</td> <td style='height:20px;background-color:honeydew;color:black;'>" +
                           data.d[i].ISIN_NO + "</td> <td style='height:20px;background-color:honeydew;color:black;'>" +
                           data.d[i].Amount + "</td></tr>");
                }
                $("#btnPrint").attr('disabled', false);
            }
        }
    });
}

var From = ''; var FromMonth = ''; var FromYear = ''; var MMYYYY = '';

function monthyear() {
    From = $('#txtMonthYear').val().trim().split(' ');
    
    FromMonth = From[0];    
    FromYear = From[1];      

    switch (FromMonth) {
        case 'January':
            FromMonth = '01';
            break;
        case 'February':
            FromMonth = '02';
            break;
        case 'March':
            FromMonth = '03';
            break;
        case 'April':
            FromMonth = '04';
            break;
        case 'May':
            FromMonth = '05';
            break;
        case 'June':
            FromMonth = '06';
            break;
        case 'July':
            FromMonth = '07';
            break;
        case 'Aughust':
            FromMonth = '08';
            break;
        case 'September':
            FromMonth = '09';
            break;
        case 'October':
            FromMonth = '10';
            break;
        case 'November':
            FromMonth = '11';
            break;
        case 'December':
            FromMonth = '12';
            break;
    }   
    MMYYYY = FromYear + FromMonth;
}

$(document).ready(function () {
    $("#btnPrint").click(function (event) {
        //PrintDivVoucherMon();
        if ($("#ddlReportName").val() == '1') {
            PrintRecord();
        }
        else {
            if ($("#txtMonthYear").val().trim() == '')
            {
                alert("Please Select/Enter Month year !!");
                $("#txtMonthYear").focus();
                return false;
            }
            //if ($("#txtSecID").val().trim() == '' )
            //{
            //    alert("Please Enter Sector ID !!");
            //    $("#txtSecID").focus();
            //    return false;
            //}
                monthyear();
          //  alert(MMYYYY);
                PrintBondIntestAnnx(MMYYYY, 'Bont',0);
        
        }
    });
});

function PrintDivVoucherMon() {
    var divToPrint = $('#divGridSummary').html().trim();
    if (divToPrint != "") {
        var popupWin = window.open('', '', 'left=0,top=0,width=1000,height=600,status=0');
        popupWin.document.open();
        popupWin.document.write('<html><head><title>Print</title>');
        popupWin.document.write("<style> @media print{ table { page-break-inside:auto } tr { page-break-inside:avoid; page-break-after:auto } thead { display:table-header-group } tfoot { display:table-footer-group } }</style>");
        popupWin.document.write('</head><body onload="window.print()">');
        popupWin.document.write('<div style="page-break-after:always;"><center>' + divToPrint + '</center></div>');
        popupWin.document.write('</body></html>');
        popupWin.document.close();
    } else {
        alert('No Data To Print');
    }
}

function PrintRecord() {

    
    var W = "{}";
    $.ajax({
        type: "POST",
        url: "BondTypeSummaryReport.aspx/GET_SectorAddress",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (Data) {
            //var GridDetails = document.getElementById("divGridSummary");
            //  var GRID = $('#divGridSummary').html().trim();
            var GRID = '';            
            if ($("#divGridSummary tr").length > 1) {
                GRID = $("#divGridSummary").html().trim();
            } else { alert("No Data found for print!!"); return false;}
            //var i = 0;
            //var TableLength = TableData.d.length;
            //var DivLength = TableLength / 2;
            var popupWin = window.open('', '', 'left=0,top=0,width=1000,height=600,status=0');
            popupWin.document.open();
            popupWin.document.write('<html><head><title>Print</title>');
            popupWin.document.write('<style> @media print{ table { page-break-inside:auto } tr { page-break-inside:avoid; page-break-after:auto } thead { display:table-header-group } tfoot { display:table-footer-group } }</style>');
            popupWin.document.write('</head><body onload="window.print()">');

          //  for (i = 0; i < DivLength; i++) {
            popupWin.document.write('<div style="page-break-after:always;"><center>');
            popupWin.document.write('<label>'+ Data.d +'</label>');
                //popupWin.document.write('<label>WEST BENGAL FINANCIAL CORPORATION</label>');
                popupWin.document.write('<br/>');
                //popupWin.document.write('<label>12A, N.S. ROAD, CALCUTTA 700001.</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>Bond type wise summary report as on  '+ $("#txtDate").val()+'</label>');
                popupWin.document.write('<br/>');
                //popupWin.document.write('<label>' + $('#lblDuration').text() + '</label>');
                //popupWin.document.write('<br/>');
                //popupWin.document.write(TableData.d[i + DivLength]);
                //popupWin.document.write('<br/>');
                popupWin.document.write('<label>===============================================================================================================</label>');
                popupWin.document.write(GRID);
                //popupWin.document.write('<table width="98%"><tr><td>DATE</td><td>DOC. TYPE</td><td>PARTICULARS</td><td>CODE</td><td align="right">DEBIT</td><td align="right">CREDIT</td></tr>');
                //popupWin.document.write('<tr><td colspan="6"><label>===============================================================================================================</label></td></tr>');
                //popupWin.document.write(TableData.d[i]);
                //popupWin.document.write('</table>');
                popupWin.document.write('</center></div>');
        //    }
            popupWin.document.write('</body></html>');
            popupWin.document.close();
        },
        //error: function (result) {
        //    $(".loading-overlay").hide();
        //    alert("Error Records Data");

        //}
    });
}


//=============== Start Indent View Record's Print Coding Here =========================//
function PrintBondIntestAnnx(yearmonth, xyz, secId) {
    var C = "{'yearmonth':'" + yearmonth + "','xyz':'" + xyz + "','secId':'" + secId + "'}";
    $.ajax({
        type: "POST",
        url: 'BondTypeSummaryReport.aspx/PrintBondInterestAnnx',
        data: C,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            window.open("ReportView.aspx?");
        },
        error: function (response) {
            alert('');
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

//=============== END Indent View Record's Print Coding Here =========================//