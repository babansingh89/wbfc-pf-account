﻿$(document).ready(function () {
    document.getElementById("RDStanderd").checked = true;
    $("#txtYearmonth").focus();
    $("#btnPrint").click(function (event) {
        if ($('#txtYearmonth').val().trim() === '') {
            alert("Please select Year Month");
            $("#txtYearmonth").css({ "background-color": "#ff9999" });
            $('#txtYearmonth').focus();
            return false;
        }
        else {
           
            ShowReport();
        }

    }); 
});

function ShowReport() {
    $(".loading-overlay").show();
    var SecotrId = '';
    var Index = '';
    document.getElementById("ddlSector").checked = true;
   
    var checkList1 = document.getElementById('ddlSector');
    var checkBoxList1 = checkList1.getElementsByTagName("input");
    var checkBoxSelectedItems1 = new Array();
    Index = 1;
    for (var i = 0; i < checkBoxList1.length; i++) {
        if (checkBoxList1[i].checked) {
            if (SecotrId === '') {
                //var text = checkBoxes[i].parentNode.getElementsByTagName("LABEL")[0].innerHTML;
                SecotrId =  Index;
            }
            else {
                SecotrId = SecotrId + ',' + Index;
            }

            Index = parseInt(Index) + 1;
        }
    }

    if (SecotrId === '') {
        alert('Please check at least one Sector for Print!');
        checkBoxList1[0].focus();
        return false;
    }

    var E = '';
        var FormName = '';
        var ReportName = '';
        var ReportType = '';
        var StandNPA = '';
        if (document.getElementById("RDStanderd").checked === true)
        {
            StandNPA = ' Standerd';
        }
        if (document.getElementById("RDNPA").checked === true) {
            StandNPA = 'NPA';
        }
        FormName = "LoanInterestReport.aspx";
        ReportName = "Loan Interest";
        ReportType = "Loan Interest Report";
        E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  YearMonth:'" + $('#txtYearmonth').val() + "',SecotrId:'" + SecotrId + "',StandNPA:'" + StandNPA + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SetReportValue',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",            
            success: function (response) {
                var show = response.d;

                if (show === "OK") {
                    window.open("ReportView.aspx?E=Y");
                    $("#txtYearmonth").focus();
                }
                
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
}


