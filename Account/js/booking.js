﻿

var otherText = "";
var otherValue = 0.00;
var maxdate;
var maxStartDays;

var maxdt = new Date(-100000000 * 86400000);
var mindt = new Date(100000000 * 86400000);

function compare(key, value) {
    if (key == "BookingDate") {
        var arr = value.split("/");
        var dt = new Date();
        dt.setFullYear(arr[2], (arr[1] - 1), arr[0]);
        //alert("arrdt" + dt.getTime() + " min date" + mindt.getTime());
        //alert("   " + dt.getTime() < mindt.getTime());
        dt.setHours(0, 0, 0, 0);
        if (dt.getTime() < mindt.getTime())
            mindt = dt;
    }
}

function traverse(obj, fun) {
    for (prop in obj) {
        fun.apply(this, [prop, obj[prop]]);
        if (typeof (obj[prop]) == "object") {
            traverse(obj[prop], fun);
        }
    }
}


$(document).ready(function () {
    $(".loading-overlay").show();
    $(".loading-overlay").hide();
    $('#overlay-box').fadeOut('fast');

    var BookingStartDays = $("#BookingStartDays").val();
    var maxopen = $("#MaxOpenDays").val();
    maxdate = new Date();
    maxStartDays = new Date();
    var cdt = $("#curdt").val();
    var curdate = new Date(cdt);
    if (maxopen != null) {
        maxdate.setTime(curdate.getTime() + maxopen*24*60*60*1000);
    };
    if (BookingStartDays != null) {
        maxStartDays.setTime(curdate.getTime() + BookingStartDays * 24 * 60 * 60 * 1000);
    }
    //alert("curdate " +curdate+ "   ===   add days "+ maxopen + " =====   added days    "+ maxdate );
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    $('#txtChequeDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow'

    });
    $(".calendar").click(function () {
        $('#txtChequeDate').datepicker("show");
        return false;
    });


    $('#txtBookingDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        showOn: 'both',
        buttonImage: 'images/date.png',
        buttonImageOnly: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        minDate: 0,
        maxDate: maxStartDays
        //        onSelect: function (selectedDate) {
        //            $("#txtBookingDateTo").datepicker("option", "minDate", selectedDate);
        //        }

    });

    $('#txtBookingDateTo').datepicker({
        showOtherMonths: true,
        showOn: 'both',
        buttonImage: 'images/date.png',
        buttonImageOnly: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        minDate: 0,
        maxDate: maxdate
        //        onSelect: function (selectedDate) {
        //            $("#txtBookingDate").datepicker("option", "maxDate", selectedDate);
        //        }

    });

//    $("#btnDatePicker").click(function () {
//        $('#txtBookingDate').datepicker("show");
//        return false;
//    });

//    $("#btnDatePickerTo").click(function () {
//        $('#txtBookingDateTo').datepicker("show");
//        return false;
//    });

    window.bookingPage = new WBFDC.BookingPage();
    window.bookingPage.init();
    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });

    if ($.trim($("#txtTokenNo").val()) != "") {
        window.bookingPage.bookingInfo.BookedClicked();
    }
});
function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}
function dateDiff() {
    var frmdate = null, toDate = null, totalDays = "";
    if ($.trim($("#txtBookingDate").val())!='') {
        frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
    }
    if ($.trim($("#txtBookingDateTo").val()) != '') {
        toDate = new Date($("#txtBookingDateTo").datepicker('getDate'));
    }

    if ((frmdate != null && toDate != null))
        totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;
    //alert(frmdate+'   '+ toDate+'  '+ totalDays);
    return totalDays;
}
function check(ch) {
    if (ch.id == "txtBookingDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtBookingDateTo").datepicker("option", "minDate", '-100Y');
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtBookingDateTo").datepicker("option", "minDate", testm);
        }
    } else if (ch.id == "txtBookingDateTo") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);
        }
    }
}

function openOffersDialog() {
    //$('#overlay-box').css('display', 'block');
    $('#overlay-box').fadeIn('fast', function () {
        $('#boxpopup').css('display', 'block');
        $('#boxpopup').animate({ 'left': '30%' }, 500);
    });
}


function closeOffersDialog(prospectElementID) {
    $(function ($) {
        $(document).ready(function () {
            $('#' + prospectElementID).css('position', 'fixed');
            $('#' + prospectElementID).animate({ 'left': '-100%' }, 500, function () {
                $('#' + prospectElementID).css('position', 'fixed');
                $('#' + prospectElementID).css('left', '100%');
                $('#overlay-box').fadeOut('fast');
            });
        });
    });
}


WBFDC.BookingInfo = function () {
//WBFDCHotels.BookingInfo = function () {
    var A = this;
    this.validator;
    this.init = function () {
        $('#booking-info').show();
        $("#cmdBook").click(A.BookedClicked);
        $("#cmdUnlock").click(A.UnlockClicked);
        $("#button-customer-details").click(A.clickCustomerDetails);
        $("#btnRoomAdd").click(A.RoomAddClicked);
        $("#btnNamesAdd").click(A.NamesAddClicked);
    };
    
    this.RoomAddClicked = function () {
        window.bookingPage.newAddDetails();
    };
    this.NamesAddClicked = function () {
        window.bookingPage.newNamesAddDetails();
    };
    this.UnlockClicked = function () {
        //alert("Booked Clicked");
        if ($('#txtTokenNo').val() != "") {
            window.bookingPage.UnlockBooking();
        } else {
            alert("Enter Guest Enquiry ID");
        }
    };

    this.BookedClicked = function () {
        //alert("Booked Clicked");
        if ($('#txtTokenNo').val() != "") {
            window.bookingPage.CheckBookingAdminID();
        } else {
            alert("Enter Guest Enquiry ID");
        }
    };
    this.clickCustomerDetails = function () {
        if (A.check()) {
            //alert('CustomerDetails');
            A.CheckDateSelected();
            //$("#booking-info").hide();
            //$(".booking-info-tab").removeClass("tab-active");
            //$(".booking-info-tab").addClass("tab-visited");
            //$(".booking-info-tab").click(A.tabClick);
            //window.bookingPage.customerDetails.transition();
        } else {
            alert('No Data to Proceed.');
        }
    };

    this.CheckDateSelected = function () {
        $(".loading-overlay").show();
        var B = window.bookingPage.bookingForm;
        var E = "{\'BookingForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/CheckDateSelected',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnCheckSelectedDatesProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    }

    this.OnCheckSelectedDatesProcessed = function (response) {
        var result = JSON.parse(response.d);
        //alert(JSON.stringify(result));
        var resdt = new Date(result.MinDate);
        //alert(resdt);
        //alert(+resdt + "  maxstartdays  " + +maxStartDays);
        if (+resdt > +maxStartDays) {
            alert("Your selected dates are not in the range. Start date must be within " + $("#BookingStartDays").val() + " days. Please check the booking rules and regulation before moving forward.");
        }
        else {
            $("#booking-info").hide();
            $(".booking-info-tab").removeClass("tab-active");
            $(".booking-info-tab").addClass("tab-visited");
            $(".booking-info-tab").click(A.tabClick);
            window.bookingPage.customerDetails.transition();
        }
        $(".loading-overlay").hide();
    };


    this.check = function () {
        var B = window.bookingPage.bookingForm;
        if (B != null) {
            if (B.enquiryMaster != null) {
                if (B.enquiryDetail != null) {
                    if (B.enquiryDetail.length > 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    };
    this.tabClick = function () {
        window.bookingPage.setTabActive(".booking-info-tab");
        $(".tabcontent").hide();
        $("#booking-info").show();

    };
};
WBFDC.CustomerDetails = function () {
    var A = this;
    var validator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#button-payment-details").click(A.clickPaymentDetails);
        $("#txtNOC").change(A.NOCChanged);
    };
    this.NOCChanged = function () {
        var nocValue = $.trim($("#txtNOC").val());
        if (parseInt(nocValue) > 1) {
            alert("Maximum No of Children Allowed is 1(one).");
            $("#txtNOC").val("1");
        }
    };
    this.getValidator = function () {
        //        return $("#customer-details-form").validate({ 
        //            rules: {
        //            txtName: { required: true },
        //            txtCity: { required: true },
        //            txtState: { required: true },
        //            txtEmail: { required: true, email: true },
        //            txtMobile: { required: true, number: true, minlength: 10 },
        //            $("#ddlIDProofType"): { required: true },
        //            txtIDProofNo: { required: true },
        //            txtNOA: { required: true, number: true },
        //            txtNOC: { number: true }
        //        },
        //            messages: {
        //                txtName: "Please enter your name",
        //                txtCity: "Please enter your city",
        //                txtState: "Please select your state",
        //                email: "Please enter a valid email address",
        //                mobile: {
        //                    required: "Please provide a valid mobile number",
        //                    number: "Please provide a valid mobile number",
        //                    minlength: "Please provide a valid mobile number"
        //                },
        //                ddlIDProofType: {
        //                    required: "Please select a valid ID proof"
        //                },
        //                txtIDProofNo: "Please enter a valid ID proof no",
        //                txtNOA: {
        //                    required: "Please provide a valid no of adult",
        //                    number: "Please provide a valid no of adult"
        //                },
        //                txtNOC: {
        //                    number: "Number Required"
        //                }
        //            }
        //        });
        //$("#frmEcom").validate().currentForm = '';
        //        $("#customer-details-form").validate();
        $("#frmEcom").validate();
        $("#txtName").rules("add", { required: true, messages: { required: "Please enter your name"} });
        $("#txtAddress").rules("add", { required: true, messages: { required: "Please enter your valid address"} });
        $("#txtCity").rules("add", { required: true, messages: { required: "Please enter your city"} });
        $("#txtState").rules("add", { required: true, messages: { required: "Please select your state"} });
        $("#txtCountry").rules("add", { required: true, messages: { required: "Please select your Country"} });
        $("#txtEmail").rules("add", { email: true, messages: { email: "Please enter a valid email address"} });
        $("#txtMobile").rules("add", { required: true, number: true, minlength: 10, messages: { required: "Please provide a valid mobile number", number: "Please provide a valid mobile number", minlength: "Please provide a valid mobile number"} });
        //$("#ddlIDProofType").rules("add", { required: true, messages: { required: "Please select a valid ID proof"} });
        //$("#txtIDProofNo").rules("add", { required: true, messages: { required: "Please enter a valid ID proof no"} });
        $("#txtNOA").rules("add", { required: true, number: true, messages: { required: "Please provide a valid no of adult", number: "Number Required"} });
        $("#txtNOC").rules("add", { number: true, messages: { number: "Number Required"} });
        return $("#frmEcom").validate();
//        return $("#customer-details-form").validate();

    };
    this.clickPaymentDetails = function () {
        if (A.validator.form()) {
            A.setFormValues();
            //alert('PaymentDetails');
            $("#customer-details").hide();
            $(".customer-info-tab").removeClass("tab-active");
            $(".customer-info-tab").addClass("tab-visited");
            $(".customer-info-tab").click(A.tabClick);
            window.bookingPage.paymentDetails.transition();
        }
    };
    this.setFormValues = function () {
        var B = window.bookingPage.bookingForm;

        B.enquiryMaster.Name = $("#txtName").val();
        B.enquiryMaster.Address = $("#txtAddress").val();
        B.enquiryMaster.City = $("#txtCity").val();
        B.enquiryMaster.State = $("#txtState").val();
        B.enquiryMaster.Country = $("#txtCountry").val();
        B.enquiryMaster.Mobile = $("#txtMobile").val();
        B.enquiryMaster.Email = $("#txtEmail").val();
        B.enquiryMaster.IDProofTypeID = $("#ddlIDProofType").val();
        B.enquiryMaster.IDProofNo = $("#txtIDProofNo").val();
        B.enquiryMaster.NOA = $("#txtNOA").val();
        B.enquiryMaster.NOC = $("#txtNOC").val();
        B.enquiryMaster.Comment = $("#txtComment").val();
    };
    this.transition = function () {
        window.bookingPage.UpdateDates();
        
    };
    this.tabClick = function () {
        window.bookingPage.setTabActive(".customer-info-tab");
        $(".tabcontent").hide();
        $("#customer-details").show();

    };
};
WBFDC.PaymentDetails = function () {
    var A = this;
    this.init = function () {
        $('#button-complete').attr("disabled", false);
        $("#button-complete").click(A.SubmitClick);
    };
    this.SubmitClick = function () {
        $('#button-complete').attr("disabled", true);
        
        if (A.check()) {
            //alert("clicked");
            A.setFormValues();
        } else {
            $('#button-complete').attr("disabled", false);
            alert("Total Charges should be greater than zero(0).");
        }
    };
    this.setFormValues = function () {
        var B = window.bookingPage.bookingForm;
        B.paymentInfo.GrandTotal = $("#txtTotalCharges").val();

        B.paymentInfo.PaidStatus = ($("#rd1").is(":checked") ? $("#rd1").val() : $("#rd2").val());
        //alert($("#rd1").val() + " - - " + $("#rd2").val() + " -- - - " + B.paymentInfo.PaidStatus + " - - - - " + $("#rd1").is(":checked"));
        B.paymentInfo.PayMode = ($("#rdCash").is(":checked") ? $("#rdCash").val() : $("#rdCheque").is(":checked") ? $("#rdCheque").val() : $("#rdCredit").is(":checked") ? $("#rdCredit").val() : $("#rdSpot").is(":checked") ? $("#rdSpot").val() : $("#rdCard").val());
        B.paymentInfo.Bank = $("#txtBank").val();
        B.paymentInfo.Branch = $("#txtBranch").val();
        B.paymentInfo.ChequeNo = $("#txtChequeNo").val();
        B.paymentInfo.ChequeDate = $("#txtChequeDate").val();
        window.bookingPage.SubmitForm();
    };
    this.check = function () {
        var rvip = $("#rdVIP").is(":checked");

        if (rvip) {
            if ($.trim($("#txtTotalCharges").val()) >= 0) {

                return true;
            }
        } else {
            if ($.trim($("#txtTotalCharges").val()) > 0) {
                return true;
            }
        }
        return false;
    };
    this.transition = function () {
        $("#payment-details").show();
        window.bookingPage.showPaymentData(window.bookingPage.bookingForm.paymentInfo);
        $(".payment-info-tab").addClass("tab-active").removeClass("tab-disabled");
    };
};
WBFDC.BookingPage = function () {
    var A = this;
    this.bookingForm;
    this.init = function () {
        //this.bookingForm = JSON.parse($("#bookingJSON").val());
        //alert(JSON.stringify(this.enquiryForm));
        this.bookingInfo = new WBFDC.BookingInfo();
        this.bookingInfo.init();
        this.customerDetails = new WBFDC.CustomerDetails();
        this.customerDetails.init();
        this.paymentDetails = new WBFDC.PaymentDetails();
        this.paymentDetails.init();
        $("#ddlLocation").change(A.PopulateProperty);
        //$("#txtBookingDate").change(A.PopulateTariff);
        $("#txtBookingDate").change(A.BookingDateChanged);
        $("#txtBookingDateTo").change(A.BookingDateChangedTo);
        $("#ddlProperty").change(A.PopulateTariff);

    };

    this.BookingDateChanged = function () {
        //alert('changed');
        A.PopulateTariff();
        check(this);
        var tDays = dateDiff();
        if (tDays != "")
            $("#spanNOD").html(tDays + " Days ");
        else
            $("#spanNOD").html("");
    };
    this.BookingDateChangedTo = function () {
        //alert('changed');
        //A.PopulateTariff();
        check(this);
        var tDays = dateDiff();
        if (tDays != "")
            $("#spanNOD").html(tDays + " Days ");
        else
            $("#spanNOD").html("");
    };
    this.setTabActive = function (C) {
        var D = parseInt($(C).attr("id").substring(4));
        for (var B = D; B < 5; B++) {
            $("#step" + B).unbind("click").addClass("tab-disabled").removeClass("tab-visited").removeClass("tab-active")
        } $(C).addClass("tab-active").removeClass("tab-disabled")
    };
    this.UpdateDates = function () {
        $(".loading-overlay").show();
        var B = window.bookingPage.bookingForm;
        var E = "{\'BookingForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/UpdateDates',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnUpdateDatesProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.OnUpdateDatesProcessed = function (response) {
        var r = jQuery.parseJSON(response.d);
        if (r.Result == "Success") {
            //alert(r.GrandTotal);
            A.bookingForm.paymentInfo.GrandTotal = r.GrandTotal;
            //alert(A.bookingForm.paymentInfo.GrandTotal);
            $("#customer-details").show();
            window.bookingPage.showMasterData(window.bookingPage.bookingForm.enquiryMaster);
            $(".customer-info-tab").addClass("tab-active").removeClass("tab-disabled");
            $(".loading-overlay").hide();
        }
    };
    this.SubmitForm = function () {
        $(".loading-overlay").show();
        var B = window.bookingPage.bookingForm;
        var E = "{\'BookingForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SubmitBooking',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnSubmitProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $('#button-complete').attr("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $('#button-complete').attr("disabled", false);
                $(".loading-overlay").hide();
            }
        });
    };
    this.OnSubmitProcessed = function (response) {
        var bid = jQuery.parseJSON(response.d);
        var bookingid = bid.BookingID;
        if (bookingid != null || bookingid != "") {
            //alert(bookingid);
            $('#button-complete').attr("disabled", false);
            $(".loading-overlay").hide();
            window.location.href = "BookingComplete.aspx?BookingID=" + bookingid;
        } else {
            $(".loading-overlay").hide();
            $('#button-complete').attr("disabled", false);
        }


    };
    this.UnlockBooking = function () {
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/UnlockBooking',
            data: "{EnquiryTokenID: " + $('#txtTokenNo').val() + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnUnlockProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };

    this.OnUnlockProcessed = function (response) {
        var result = JSON.parse(response.d);
        if (result.result == "SUCCESS") {
            alert("Unlocking of Booked Room(s) in this Guest Enquiry ID '" + $('#txtTokenNo').val() + "' has been Successfuly Done.");
            //window.location = 'booking.aspx';
            $('#txtTokenNo').val('');
            //location.reload();
            window.location.href = "Welcome.aspx";
        }
        $(".loading-overlay").hide();
    };

    this.UnlockBookingForce = function () {
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/UnlockBooking',
            data: "{EnquiryTokenID: " + $('#txtTokenNo').val() + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnUnlockProcessedForce,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };

    this.OnUnlockProcessedForce = function (response) {
        var result = JSON.parse(response.d);
        if (result.result == "SUCCESS") {
            alert("Unlocking of Booked Room(s) in this Guest Enquiry ID '" + $('#txtTokenNo').val() + "' has been Successfuly Done.");
            //window.location = 'booking.aspx';
            $('#txtTokenNo').val('');
            location.reload();
            
        }
        $(".loading-overlay").hide();
    };
    this.CheckBookingAdminID = function () {
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            url: pageUrl + '/CheckBookingAdminID',
            data: "{EnquiryTokenID: " + $('#txtTokenNo').val() + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnCheckedProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.OnCheckedProcessed = function (response) {
        var result = JSON.parse(response.d);
        if (result.result == "SUCCESS") {
            $(".loading-overlay").hide();
            A.ProcessBooking();
        } else {
            var message = "You have not completed your previous booking against Guest Enquiry ID '" + result.EnquiryTokenIDPrevious
            + "'. Either Complete that booking or unlock those blocked days before processing any other Guest Enquiry ID.\n\nDo you want to unlock the days in Guest Enquiry ID '" + result.EnquiryTokenIDPrevious
            + "' now?"
            var ans = confirm(message);
            if (ans) {
                $(".loading-overlay").hide();
                $("#txtTokenNo").val(result.EnquiryTokenIDPrevious);
                A.UnlockBookingForce();
                //$("#txtTokenNo").val('');
            } else {
                $(".loading-overlay").hide();
                window.location.href = 'booking.aspx?TID=' + $("#txtTokenNo").val();
            }
        }
    };
    this.ProcessBooking = function () {
        $(".loading-overlay").show();
        var bookingtype = ($("#rdGeneral").is(":checked") ? $("#rdGeneral").val() : $("#rdVIP").is(":checked") ? $("#rdVIP").val() : $("#rdWild").val());
        var bookingVIPType = (bookingtype == "V" ? $("#rdVipSpot").is(":checked") ? $("#rdVipSpot").val() : $("#rdVipCom").val() : '');
        $.ajax({
            type: "POST",
            url: pageUrl + '/StartBooking',
            data: "{EnquiryTokenID: " + $('#txtTokenNo').val() + ",BookingType:'" + bookingtype + "',BookingVIPType:'" + bookingVIPType + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: A.OnBookedProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.OnBookedProcessed = function (response) {
        A.bookingForm = JSON.parse(response.d);
        //alert(JSON.stringify(A.bookingForm.enquiryName));
        A.showDetails(A.bookingForm.enquiryDetail);
        A.showMasterData(A.bookingForm.enquiryMaster);
        A.showPaymentData(A.bookingForm.paymentInfo);
        $("#innerTable table[id='detailTable']").hide();
        $("#innerTable table[id='detailTable']").show("slow");
        $(".loading-overlay").hide();
    };
    this.showDetails = function (enquiryDetail) {
        var html = '';
        var rwild = $("#rdWild").is(":checked");
        var rvip = $("#rdVIP").is(":checked");
        if (A.bookingForm.enquiryMaster != null && A.bookingForm.enquiryMaster.EnquiryTokenID != 0) {
            if (enquiryDetail != "undefined" && enquiryDetail != null) {
                if (enquiryDetail.length > 0) {

                    html += "<hr style='border:solid 1px lightblue' /><br/>";
                    html += "<table align='center' id='detailTable' class='divTable' cellpadding='3' width='100%'>" + "\n"
                    + "<tr>"
                    + "<td class='th'></td>"
                    + "<td class='th'>Booking Date</td>"
                    + "<td class='th'>Location</td>"
                    + "<td class='th'>Property</td>"
                    //+ "<td class='th'>Tariff Range</td>"
                    //+ "<td class='th'>No of Rooms</td></tr>"
                    + "<td class='th' style='text-align:center;'>Booked Details</td></tr>"
                    ;

                    html += "<tr><td colspan='5' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";


                    var enqDetails = WBFDC.Utils.fixArray(enquiryDetail);
                    for (var i in enqDetails) {
                        html += "<tr id='EnquiryTokenDetID_" + enqDetails[i].EnquiryTokenDetID + "'>"
                                + "<td class='tr' id='tddeleteEnquiryTokenDetID_" + enqDetails[i].EnquiryTokenDetID + "'>"
                                + " <div><a href='" + pageUrl + '/DeleteEnquiryAccomodationDetail' +
                                "' onClick='return window.bookingPage.removeEnquiryDetail(" + enqDetails[i].EnquiryTokenDetID +
                                ",0)'><img src='images/delete.png' style='border:none;' id='deleteEnquiryTokenDetID_" + enqDetails[i].EnquiryTokenDetID + "' /></a></div></td>"
                                + "<td  class='tr' >" + enqDetails[i].BookingDate + "</td>"
                                + "<td  class='tr' >" + enqDetails[i].LocationName + "</td>"
                                + "<td  class='tr' >" + enqDetails[i].PropertyName + "</td>"
                                + "<td  class='tr' >";

                        var bookDetails = WBFDC.Utils.fixArray(enqDetails[i].bookedDetails);
                        if (bookDetails != "undefined") {
                            if (bookDetails.length > 0) {
                                html += "<table align='center' id='detailTableInner_" + enqDetails[i].EnquiryTokenDetID + "' class='divTable'  cellpadding='5' width='100%'>" + "\n"
                            + "<tr>"
                            + "<td class='th'>Room</td>"
                            + "<td class='th' align='right'>Rates</td>"
                            + "<td class='th' align='right'>LT</td>"
                            //+ "<td class='th' align='right'>" + enqDetails[i].luxuryTaxRate + "% LT</td>"
                            //+ "<td class='th'  style='width:50px;' align='right'>" + enqDetails[i].serviceTaxRate + "% ST <hr style='border:solid 1px #cbc7c7' /> " + enqDetails[i].ecessRate + "% EC <hr style='border:solid 1px #cbc7c7' /> " + enqDetails[i].hecessRate + "% H.EC</td>"
                            + "<td class='th' align='right'>ST</td>"
                                
                             + "<td class='th' style='width:50px;' align='right'>Additional Cot /<br/> Charge</td>";
                                if (rwild) {
                                    html += "<td class='th' >Other Charges</td>";
                                }

                                html += "<td class='th' align='right'>Sub Total</td>"
                            + "<td class='th'></td>"
                                //+ "<td class='th'>Status</td>"
                            + "</tr>";
                                html += "<tr><td colspan='9' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                                var totrates = 0;
                                var totserviceTax = 0;
                                var totecess = 0;
                                var totextracharge = 0;
                                var tothcess = 0;
                                var gtot = 0;

                                for (var j in bookDetails) {
                                    var rates = 0;
                                    var serviceTax = 0;
                                    var ecess = 0;
                                    var hcess = 0;
                                    var extracharge = 0;
                                    var tot = 0;
                                    html += "<tr id='BookedAccomodationID_" + enqDetails[i].EnquiryTokenDetID + "_" + bookDetails[j].Accomodation_Details_ID + "'>"
                                    + "<td class='tr'>" + bookDetails[j].AccomodationName + "<br/>(Guest Name: " + bookDetails[j].enquiryNames.GuestName + ")</td>"
                                    + "<td class='tr'   align='right'><input type='text' id='txtRates_"
                                    + enqDetails[i].EnquiryTokenDetID + "_" + bookDetails[j].Accomodation_Details_ID
                                    + "' " + bookDetails[j].Rates + " " + (rvip ? "" : "disabled='disabled'") + " onchange='return window.bookingPage.ChangeRateCharge("
                                        + enqDetails[i].EnquiryTokenDetID + "," + bookDetails[j].Accomodation_Details_ID +
                                        ")' class='trinput' value='" + bookDetails[j].Rates + "' /></td>";
                                    // Changed as on 15/05/2013 as per ARNAB by Ranadeep
                                    ////html += "<td class='tr'   align='right'>" + bookDetails[j].LuxuryTaxValue + "</td>"
                                    ////    + "<td class='tr'   align='right'>" + bookDetails[j].serviceTaxValue +
                                    ////    " <hr style='border:solid 1px #cbc7c7' /> " + bookDetails[j].ecessValue +
                                    ////    " <hr style='border:solid 1px #cbc7c7' /> " + bookDetails[j].hcessValue + "</td>";
                                    ///
                                    //Changed as on 16/07/2013 for VIP Booking by Ranadeep
                                    //html += "<td class='tr'   align='right'>" + bookDetails[j].LuxuryTaxValue + "</td>";
                                    //html += "<td class='tr'   align='right'>" + (parseFloat(bookDetails[j].serviceTaxValue) + parseFloat(bookDetails[j].ecessValue) + parseFloat(bookDetails[j].hcessValue));

                                    html += "<td class='tr'   align='right'><input type='text' id='txtLT_"
                                    + enqDetails[i].EnquiryTokenDetID + "_" + bookDetails[j].Accomodation_Details_ID
                                    + "'  " + (rvip ? "" : "disabled='disabled'") + " onchange='return window.bookingPage.ChangeLTCharge("
                                        + enqDetails[i].EnquiryTokenDetID + "," + bookDetails[j].Accomodation_Details_ID +
                                        ")' class='trinput' style='width:50px;' value='" + bookDetails[j].LuxuryTaxValue + "' /></td>";

                                    html += "<td class='tr'   align='right'><input type='text' id='txtST_"
                                    + enqDetails[i].EnquiryTokenDetID + "_" + bookDetails[j].Accomodation_Details_ID
                                    + "'  " + (rvip ? "" : "disabled='disabled'") + " onchange='return window.bookingPage.ChangeSTCharge("
                                        + enqDetails[i].EnquiryTokenDetID + "," + bookDetails[j].Accomodation_Details_ID +
                                        ")' class='trinput' style='width:50px;' value='" + (parseFloat(bookDetails[j].serviceTaxValue) + parseFloat(bookDetails[j].ecessValue) + parseFloat(bookDetails[j].hcessValue)) + "' /></td>";


                                    
                                     html+= "<td class='tr' align='right'><input type='text' id='txtCotNo_" + enqDetails[i].EnquiryTokenDetID +
                                        "_" + bookDetails[j].Accomodation_Details_ID + "' onblur='if($(this).val()==\"\") {$(this).val(0)};'  onkeyup='return window.bookingPage.ChangeCotCharge("
                                        + enqDetails[i].EnquiryTokenDetID + "," + bookDetails[j].Accomodation_Details_ID +
                                        ",\"" + bookDetails[j].ACNONACTYPE + "\"," + bookDetails[j].ACNONAC_Charge
                                        + ")' class='trinput' value='" + bookDetails[j].ExtraCot + "'/><br/>"
                                        + "<input type='text' id='txtCotChrg_" + enqDetails[i].EnquiryTokenDetID +
                                        "_" + bookDetails[j].Accomodation_Details_ID + "' disabled='disabled' class='trinput' value='" + bookDetails[j].ACNONAC_Charge + "'/>"
                                        + "</td>";

                                    if (rwild) {
                                        //alert(bookDetails[j].Other);
                                        html += "<td class='th' ><textarea  class='trinputarea' name='txtOther_" + enqDetails[i].EnquiryTokenDetID +
                                        "_" + bookDetails[j].Accomodation_Details_ID + "' id='txtOther_" + enqDetails[i].EnquiryTokenDetID +
                                        "_" + bookDetails[j].Accomodation_Details_ID + "'  >" + bookDetails[j].Other + "</textarea><br/><input type='button' value='...' id='btnSelOther_" + enqDetails[i].EnquiryTokenDetID +
                                        "_" + bookDetails[j].Accomodation_Details_ID +
                                        "' onclick='return window.bookingPage.SelectOtherValue(" + enqDetails[i].LocationID +
                                        "," + enqDetails[i].EnquiryTokenDetID + "," + bookDetails[j].Accomodation_Details_ID + ");' title='Select'/>" +
                                        "<br/><input type='text' class='trinput' style='width:50px;' id='txtOtherValue_" + enqDetails[i].EnquiryTokenDetID +
                                        "_" + bookDetails[j].Accomodation_Details_ID + "' value='" + bookDetails[j].OtherValue + "' /></td>";
                                    }


                                    html += "<td class='tr' id='tdsubTotal_" + enqDetails[i].EnquiryTokenDetID +
                                        "_" + bookDetails[j].Accomodation_Details_ID + "' align='right'>" + bookDetails[j].subTotal + "</td>";
                                    //html += "<td class='tr'>Booked</td>";
                                    html += "<td class='tr' id='tddeleteAccomodationID_" + enqDetails[i].EnquiryTokenDetID + "_" + bookDetails[j].Accomodation_Details_ID
                                + "'><div><a href='" + pageUrl + '/DeleteEnquiryAccomodationDetail' +
                                "' onClick='return window.bookingPage.removeEnquiryDetail(" + enqDetails[i].EnquiryTokenDetID +
                                "," + bookDetails[j].Accomodation_Details_ID + ")'><img src='images/delete.png' style='border:none;' id='deleteAccomodationID_" + enqDetails[i].EnquiryTokenDetID
                                + "_" + bookDetails[j].Accomodation_Details_ID + "' /></a></div></td>"
                                    html += "</tr>";
                                }
                                html += "<tr><td colspan='9' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                                html += "<tr><td colspan='" + (rwild ? 6 : 5) + "' class='tr' align='right'>Total</td>"
                                    + "<td class='tr' id='tdtotsubtotal_" + enqDetails[i].EnquiryTokenDetID + "' align='right'>" + enqDetails[i].totsubTotal + "</td>"
                                    + "</tr>";
                                html += "</table>";
                            } else {
                                html += "<table align='center' id='detailTableInner_" + enqDetails[i].EnquiryTokenDetID + "' class='divTable'  cellpadding='5' width='100%'>" + "\n"
                            + "<tr>"
                            + "<td align='center' style='fon-size:12px;color:red;font-weight:bold;'>Room Not Avaliable in your selected Tariff Range.</td>"
                            + "</tr>"
                            + "<tr><td><button id='changeRoomRange'  "
                            + " class='proceed-button checkout-continue-button DefaultButton' "
                            + "style='width:173px;' onclick='return window.bookingPage.showContent(" + A.bookingForm.enquiryMaster.EnquiryTokenID
                            + "," + enqDetails[i].EnquiryTokenDetID + ")'>"
                            + "<span class='button-text'>Change Tariff Range</span></button></td></tr>"
                            + "</table>";

                            }
                        }
                        html += "</td>";
                        html += "</tr>";
                        html += "<tr><td colspan='5' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                    }
                    html += "</table>";


                }
            }
            var B = A.bookingForm;
            var totBooking = $("#TotalBookingNo").val();
            var totAdded = B.enquiryMaster.TotalNoOfRooms;
            if (totAdded < totBooking) {
                html += "<hr style='border:solid 1px lightblue' /><br/>";
                html += "<button id='newRoomRange'  "
                            + " class='proceed-button checkout-continue-button DefaultButton' "
                            + "style='width:160px;float:left;' onclick='return window.bookingPage.showContent(" + A.bookingForm.enquiryMaster.EnquiryTokenID
                            + ",null)'>"
                            + "<span class='button-text' >Continue booking</span></button>";
                            //+ "<span class='button-text' >New Dates</span></button>";
            }

        } else {
            html += "<hr style='border:solid 1px lightblue' /><br/>";
            html += "<table align='center' id='detailTable' class='divTable' cellpadding='3' width='100%'>"
                    + "<tr><td><div>No Guest Form with this Guest Enquiry ID is registered.</div></td></tr>"
                    + "</table>";
        }
        //alert(html);
        $("#innerTable").empty().html(html);
    };
    this.SelectOtherValue = function (LocationID, EnquiryTokenDetID, AccomodationID) {
        if (LocationID != null && EnquiryTokenDetID != null && AccomodationID != null) {
            $(".loading-overlay").show();
            var C = "{LocationID:" + LocationID + "}"
            $.ajax({
                type: "POST",
                url: pageUrl + '/WildLifeService',
                data: C,
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    //window.enquiryPage.enquiryForm = JSON.parse(D.d);
                    //alert(JSON.parse(D.d));
                    $("#content").empty();
                    $("#content").html(JSON.parse(D.d));
                    var button = "<input type='button' Value='Set' class='proceed-button checkout-continue-button' onclick='return window.bookingPage.SetOtherValue(" + EnquiryTokenDetID + "," + AccomodationID + ")'/>";
                    $("#butarea").html(button);
                    $(".loading-overlay").hide();

                    openOffersDialog();

                },
                error: function (response) {
                    alert('Some Error Occur');
                    $(".loading-overlay").hide();
                },
                failure: function (response) {
                    alert(response.d);
                    $(".loading-overlay").hide();
                }
            });
        }
    };
    //    this.ServiceCheked = function (ch) {
    //        var $this = $(ch);
    //        if ($this.is(":checked")) {
    //            
    //        }

    //    };
    this.SetOtherValue = function (EnquiryTokenDetID, AccomodationID) {
        otherText = "";
        otherValue = 0.00;
        var sTableTr = $("#seviceTable tr");
        sTableTr.each(function () {
            var $chk = $(this).find('input:checkbox');
            if ($chk.attr("checked")) {
                otherText += $chk.parents('tr:first').find('th:first').text() + ",";
                var value = ($chk.parents('tr:first').find('th:eq(1) #chvalue').text());
                otherValue += parseFloat(value);
            }
        });
        otherText = otherText.substring(0, (otherText.length) - 1);
        //alert("text  ---  " + otherText + "  val --- " + otherValue);
        $("#txtOther_" + EnquiryTokenDetID + "_" + AccomodationID).val(otherText);
        $("#txtOtherValue_" + EnquiryTokenDetID + "_" + AccomodationID).val(otherValue);


        var enqdetails = A.getEnquiryDetail(this.bookingForm.enquiryDetail, EnquiryTokenDetID);

        if (enqdetails != "undefined") {
            var bookdetails = A.getBookingDetail(enqdetails.bookedDetails, AccomodationID);
            if (bookdetails != "undefined") {
                bookdetails.Other = otherText;
                bookdetails.OtherValue = otherValue;
                //                        var subtot = (bookdetails.Rates + bookdetails.LuxuryTaxValue + bookdetails.serviceTaxValue + bookdetails.ecessValue + bookdetails.hcessValue + extraCotvalue);
                //                        subtot = roundNumber(subtot, 0);
                //                        bookdetails.subTotal = subtot
                //                        var tdsubtot = "#tdsubTotal_" + EnquiryTokenDetID + "_" + AccomodationID;
                //                        $(tdsubtot).html(subtot);
                //                        //alert(JSON.stringify(bookdetails));
            }
        }

        A.updateBookedDetails();

        closeOffersDialog('boxpopup');
    };
    this.showContent = function (EnquiryTokenID, EnquiryTokenDetID) {
        //alert('in room range' + EnquiryTokenID + '   ' + EnquiryTokenDetID);
        if (EnquiryTokenDetID != null) {
            var ed = A.getEnquiryDetail(this.bookingForm.enquiryDetail, EnquiryTokenDetID);
            //alert(ed.EnquiryTokenDetID);
            if (ed != "undefined") {
                $("#EnquiryTokenDetID").val(ed.EnquiryTokenDetID);
                $("#txtBookingDate").val(ed.BookingDate).attr("disabled", true);
//                $("#btnDatePicker").attr("disabled", true);
                $("#txtBookingDateTo").val(ed.BookingDate).attr("disabled", true);
//                $("#btnDatePickerTo").attr("disabled", true);
                $("#ddlLocation").val(ed.LocationID).attr("disabled", true);
                A.PopulateProperty();
                $("#ddlProperty").val(ed.PropertyID).attr("disabled", true);
                A.PopulateTariff();
                if (ed.RaRangeOrderPreference == "Asc") {
                    $("#rdAsc").attr("checked", true);
                } else {
                    $("#rdDesc").attr("checked", true);
                }
                $("#ddlAC").val(ed.ACNonAC);
                $("#chkAC").attr("checked", (ed.ACNonACCompulsory == "Y" ? true : false));
                $("#chkPart").attr("checked", (ed.PartBooking == "Y" ? true : false));

                $("#txtNOR").val(ed.NOR);
                $("#ddlAlternate").val(ed.AlternateLocation);
                //$("#btnAdd").click(A.AddEnquiryTokenDetails());
                $("#loadBookingDet").show();
            }
        } else {
            $("#EnquiryTokenDetID").val(0);
            $("#txtBookingDate").val('').attr("disabled", false);
            $("#txtBookingDate").datepicker("option", "minDate", 0);
            $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);
//            $("#btnDatePicker").attr("disabled", false);
            $("#txtBookingDateTo").val('').attr("disabled", false);
            $("#txtBookingDateTo").datepicker("option", "minDate", 0);
            $("#txtBookingDateTo").datepicker("option", "maxDate", maxdate);
//            $("#btnDatePickerTo").attr("disabled", false);
            $("#ddlLocation").val("").attr("disabled", false);
            A.PopulateProperty();
            $("#ddlProperty").val("").attr("disabled", false);
            A.PopulateTariff();
            $("#rdAsc").attr("checked", true);
            $("#ddlAC").val("");
            $("#chkAC").attr("checked", false);
            $("#chkPart").attr("checked", false);
            $("#txtNOR").val("");
            $("#ddlAlternate").val(0);
            //$("#btnAdd").click(A.AddEnquiryTokenDetails());
            $("#loadBookingDet").show();
        }
        return false;
    };
    this.CloseContent = function () {
        $("#loadBookingDet").hide();
        return false;
    };
    this.CloseRoomContent = function () {
        $("#loadRoomDet").hide();
        window.bookingPage.bookingForm.dateRoomID = [];
        return false;
    };
    this.AddEnquiryTokenDetails = function () {
        var EnquiryTokenDetID = $("#EnquiryTokenDetID").val();
        $("#frmEcom").validate();
        if ((EnquiryTokenDetID != null || EnquiryTokenDetID != "") && $("#frmEcom").valid()) {

            A.checkGuestExistence(EnquiryTokenDetID);
        }
    };
    this.showRoomAvailablity = function (tot) {
        $(".loading-overlay").show();
        var C = "{dateFrom:'" + $("#txtBookingDate").val() + "',dateTo:'" + $("#txtBookingDateTo").val() +
                "',propertyID:" + $("#ddlProperty").val() + ",locationID:" + $("#ddlLocation").val() +
                ",RangeFrom:" + $("#ddlFrom").val() + ",RangeTo:" + $("#ddlTo").val() + ",RangeOrderPreference:'" +
                (($("#rdAsc").is(":checked")) ? $("#rdAsc").val() : $("#rdDesc").val()) + "',TotalRoomAllowed:" + $("#TotalRoomsAllowed").val() + "}"
        $.ajax({
            type: "POST",
            url: pageUrl + '/ShowAvaliablity',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {

                $("#divroomTable").html(JSON.parse(D.d));
                $(".loading-overlay").hide();

                A.getOtherValue(tot);
                $("#loadRoomDet").show();

            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.getOtherValue = function (tot) {
        var otherHtml = '';
        ////        var B = window.enquiryPage.enquiryForm;
        ////        var totBooking = $("#TotalBookingNo").val();
        ////        var totAdded = B.enquiryMaster.TotalNoOfRooms;
        ////        var tot = (parseInt(totBooking)) - parseInt(totAdded);
        otherHtml += " Select Prefered Room(s) of " + $.trim($("#ddlLocation option:selected").text());
        //otherHtml += "<div style='font-size:11px;color:maroon;float:right;margin-right:20px;'>Maximum <span id='allowed' >" + tot + "</span> rooms.</div>"
        $("#divCaption").html(otherHtml);
    };
    this.RoomBookedCheked = function (ch) {
        var $this = $(ch);

        var LocID = parseInt($("#ddlLocation").val());
        var PropID = parseInt($("#ddlProperty").val());


        //var allowed = $("#allowed").html();
        //alert($this.attr("id") + "  allowed " + allowed);
        var idtext = $this.attr("id").split("_");
        if ($this.is(":checked")) {
            var dt = idtext[2];
            dt = dt.replace("/", "");
            dt = dt.replace("/", "");
            var $i = $("#lr_" + dt);
            var allowed = $i.html();
            //alert(allowed);
            if (parseInt(allowed) > 0) {
                var name = $.trim($this.parents('tr:first').find('th:first').text());
                //alert(idtext[0] + "  " + idtext[1] + "  " + idtext[2]+"   name   "+name);
                //allowed = parseInt(allowed) - 1;
                //$("#allowed").html(allowed);


                allowed = parseInt(allowed) - 1;
                $i.html(allowed);



                var B = window.bookingPage.bookingForm;
                var C = WBFDC.Utils.fixArray(B.enquiryLocationRoom);
                var D = WBFDC.Utils.fixArray(B.tempLocationRoom);
                //alert(D);
                var found = false;
                var foundD = false;
                if (C == "" && D == "") {
                    C.push({
                        LocationID: LocID,
                        PropertyID: PropID,
                        BookingDate: idtext[2],
                        TotalRoomsBooked: 1
                    });
                    D.push({
                        LocationID: LocID,
                        PropertyID: PropID,
                        BookingDate: idtext[2],
                        TotalRoomsBooked: 1
                    });
                } else {
                    for (var i in C) {
                        if (C[i].LocationID == LocID && C[i].PropertyID == PropID && C[i].BookingDate == idtext[2]) {
                            C[i].TotalRoomsBooked += 1;
                            found = true;
                        }
                    }
                    for (var j in D) {
                        if (D[j].LocationID == LocID && D[j].PropertyID == PropID && D[j].BookingDate == idtext[2]) {
                            D[j].TotalRoomsBooked += 1;
                            foundD = true;
                        }
                    }
                    if (!found) {
                        C.push({
                            LocationID: LocID,
                            PropertyID: PropID,
                            BookingDate: idtext[2],
                            TotalRoomsBooked: 1
                        });
                    }
                    if (!foundD) {
                        D.push({
                            LocationID: LocID,
                            PropertyID: PropID,
                            BookingDate: idtext[2],
                            TotalRoomsBooked: 1
                        });
                    }
                }
                B.dateRoomID.push({
                    BookingDate: idtext[2],
                    AccomodationDetailID: parseInt(idtext[1]),
                    AccomodationName: name
                });
                //alert(JSON.stringify(B));

            } else {
                alert("You have reached your maximum room selection.");
                $this.attr("checked", false);
            }
        } else {
            var dt = idtext[2];
            dt = dt.replace("/", "");
            dt = dt.replace("/", "");
            var $i = $("#lr_" + dt);
            var allowed = $i.html();
            allowed = parseInt(allowed) + 1;
            $i.html(allowed);

            var B = window.bookingPage.bookingForm;
            A.findAndRemove2(B.dateRoomID, "BookingDate", idtext[2], "AccomodationDetailID", parseInt(idtext[1]));
            var C = WBFDC.Utils.fixArray(B.enquiryLocationRoom);
            var D = WBFDC.Utils.fixArray(B.tempLocationRoom);
            for (var i in C) {
                if (C[i].LocationID == LocID && C[i].PropertyID == PropID && C[i].BookingDate == idtext[2]) {
                    C[i].TotalRoomsBooked -= 1;
                    //alert(C[i].TotalRoomsBooked);
                    if (C[i].TotalRoomsBooked == 0) {
                        C.splice(i, 1);
                        break;
                    }
                }
            }
            for (var j in D) {
                if (D[j].LocationID == LocID && D[j].PropertyID == PropID && D[j].BookingDate == idtext[2]) {
                    D[j].TotalRoomsBooked -= 1;
                    //alert(C[i].TotalRoomsBooked);
                    if (D[j].TotalRoomsBooked == 0) {
                        D.splice(j, 1);
                        break;
                    }
                }
            }


            //alert(JSON.stringify(B));
            //allowed = parseInt(allowed) + 1;
            //$("#allowed").html(allowed);
        }
    };

    //this.RoomBookedCheked = function (ch) {
    //    var $this = $(ch);

    //    var allowed = $("#allowed").html();
    //    //alert($this.attr("id") + "  allowed " + allowed);
    //    var idtext = $this.attr("id").split("_");
    //    if ($this.is(":checked")) {
    //        if (parseInt(allowed) > 0) {
    //            var name = $this.parents('tr:first').find('th:first').text();
    //            //alert(idtext[0] + "  " + idtext[1] + "  " + idtext[2]+"   name   "+name);
    //            allowed = parseInt(allowed) - 1;
    //            $("#allowed").html(allowed);
    //            var B = window.bookingPage.bookingForm;

    //            B.dateRoomID.push({
    //                BookingDate: idtext[2],
    //                AccomodationDetailID: parseInt(idtext[1]),
    //                AccomodationName: name
    //            });
    //            //alert(JSON.stringify(B));

    //        } else {
    //            alert("You have reached your maximum room selection.");
    //            $this.attr("checked", false);
    //        }
    //    } else {
    //        var B = window.bookingPage.bookingForm;
    //        A.findAndRemove2(B.dateRoomID, "BookingDate", idtext[2], "AccomodationDetailID", parseInt(idtext[1]));
    //        //alert(JSON.stringify(B));
    //        allowed = parseInt(allowed) + 1;
    //        $("#allowed").html(allowed);
    //    }
    //};
    this.findAndRemove2 = function (array, property, value, property2, value2) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    this.newAddDetails = function () {
        var B = window.bookingPage.bookingForm;
        var dateRoom = WBFDC.Utils.fixArray(B.dateRoomID);
        if (dateRoom == "") {
            alert('Select Prefered Rooms before adding ');
            return false;
        }


        //alert(JSON.stringify(dateRoom));
        mindt = new Date(100000000 * 86400000);
        mindt.setHours(0, 0, 0, 0);
        //mindt = new Date();
        //alert(mindt);
        traverse(dateRoom, compare);
        mindt.setHours(0, 0, 0, 0);
        //alert("mindate " + mindt + " max start date  " + maxStartDays);

        //alert("mindate " + mindt + " max start date  " + maxStartDays);
        //alert(+maxStartDays + "    " + +mindt);
        //alert(+mindt > +maxStartDays);
        if (+mindt > +maxStartDays) {
            alert("Start date must be within " + $("#BookingStartDays").val() + " days;")
            return false;
        }


        //var B = window.bookingPage.bookingForm;
        var arr = WBFDC.Utils.fixArray(B.tempLocationRoom);

        arr = $.map(arr, function (o) { return o.TotalRoomsBooked; });
        var highest = Math.max.apply(this, arr);
        //alert(highest);

        var enqName = WBFDC.Utils.fixArray(B.enquiryName);
        var enlength = enqName.length;

        var html = "";
        // html+="<form id='ms'></form> <form id='nameform' >";

        //$("#nameform").validate();
        html += "<table align='center' id='nameTable' class='divTable' cellpadding='5' width='80%'>" + "\n"
                    + "<tr>"
                    + "<td class='th' width='30%'>Guest Name</td>"
                    + "<td class='th' width='20%'>Age</td>"
                    + "<td class='th' width='20%'>Sex</td>"
                    + "<td class='th' width='20%'></td>"
                    + "</tr>";

        html += "<tr><td colspan='4' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
        //var s = $("<select id=\"" + total + "\" name=\"" + total + "\" />");            // 4-
        //for (var val in data) {
        //    $("<option />", { value: val, text: data[val] }).appendTo(s);
        //}
        if (enqName.length <= 0) {
            for (var i = 0; i < highest; i++) {
                html += "<tr id='tr_" + i + "'>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                    + "<td></td>"
                    + "</tr>";
                //$("#n_" + i).rules("add", { required: true, messages: { required: "Please Guest Name" } });
                //$("#a_" + i).rules("add", { required: true, messages: { required: "Please Guest Age" } });
                //$("#s_" + i).rules("add", { required: true, messages: { required: "Please Guest Sex" } });
            }
        } else {

            for (var i = 0; i < enlength; i++) {
                html += "<tr id='tr_" + i + "'>";
                //alert(i);
                //if (i <= highest - 1) {
                //////html += "<td class='tr' align='left'>" + enqName[i].GuestName + "</<td>"
                //////    + "<td class='tr' align='left'>" + enqName[i].Age + "</<td>"
                //////    + "<td class='tr' align='left'>" + (enqName[i].Sex == "M" ? "Male" : "Female") + "</<td>"
                //////    + "<td><input type='checkbox' id='chkN_" + i + "' "+(i<=highest-1?"checked='checked'":"")+" /></td>";

                html += "<td class='tr' align='left'><input type='text' disabled='disabled' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value='" + enqName[i].GuestName + "'/></<td>"
                    + "<td class='tr' align='left'><input type='text' disabled='disabled' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value='" + enqName[i].Age + "'/></<td>"
                    + "<td class='tr' align='left'><select type='text' disabled='disabled' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M' " + (enqName[i].Sex == 'M' ? "selected='selected'" : "") + ">Male</option><option value='F' " + (enqName[i].Sex == 'F' ? "selected='selected'" : "") + ">Female</option></select></<td>"
                    + "<td><input type='checkbox' id='chkN_" + i + "' name='chkN_" + i + "' " + (i <= highest - 1 ? "checked='checked'" : "") + "  /></td>";
                ///////onclick = 'return window.enquiryPage.NamesCheckedClick(this);'
                //}else{
                //    html +=  "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                //    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                //    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                //    + "<td></td>"
                //}
                html += "</tr>";
            }

            if (enlength < highest) {
                var remain = highest - enlength;
                for (var j = 0; j < remain; j++) {
                    html += "<tr id='trj_" + j + "'>";
                    html += "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='nj_" + j + "' name='nj_" + j + "' value=''/></<td>"
                        + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='aj_" + j + "' name='aj_" + j + "' value=''/></<td>"
                        + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='sj_" + j + "' name='sj_" + j + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                        + "<td></td>";
                    html += "</tr>";
                }
            }

        }

        html += "</table>";
        //html += "</form>";

        if (enqName.length > 0) {
            html += "<table align='center' id='nameNewTable' style='display:none;' class='divTable' cellpadding='5' width='80%'>" + "\n"
                        + "<tr>"
                        + "<td class='th' width='30%'>Guest Name</td>"
                        + "<td class='th' width='20%'>Age</td>"
                        + "<td class='th' width='20%'>Sex</td>"
                        + "<td class='th' width='20%'></td>"
                        + "</tr>";

            html += "<tr><td colspan='4' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
            for (var i = 0; i < highest; i++) {
                html += "<tr id='tr_" + i + "'>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                    + "<td></td>"
                    + "</tr>";

            }
            html += "</table>";
        }


        $("#divNameTable").empty().html(html);

        var otherHtml = '';

        otherHtml += " Enter Guest Details of Room(s) of " + $.trim($("#ddlLocation option:selected").text());
        var OptionHtml = '';
        OptionHtml += "<div style='float:right;' >"
            + "<label for='rdOld'  style='padding-right:5px;font-size:11px;padding-left:5px'>Select Old Names</label><input type='radio' checked='checked' id='rdOld' name='rdOld' value='O' onclick='return window.bookingPage.RadionSelected(this)' />"
            + "<label for='rdNew'  style='padding-right:5px;font-size:11px;padding-left:5px'>Enter New Names</label><input type='radio'  id='rdNew' name='rdOld' value='N' onclick='return window.bookingPage.RadionSelected(this)'  />"
            + "</div>";

        if (enqName.length > 0) {
            otherHtml += OptionHtml;
        }
        $("#NamesCaption").html(otherHtml);

        $("#loadRoomDet").hide();

        $("#LoadNamesDet").show();
        //return $("#nameform").validate();
        
    };

    this.RadionSelected = function (ch) {
        var $this = $(ch);
        var optionValue = $this.val();
        if (optionValue == 'O') {
            $("#nameTable").show();
            $("#nameNewTable").hide();
        } else {
            $("#nameTable").hide();
            $("#nameNewTable").show();
        }
    };

    this.newNamesAddDetails = function () {
        //var v = A.ValidateNameForm();
        //alert(v);
        $("#frmEcom").validate();
        //alert('form  validated');
        //alert($("#enquiry").valid());
        if ($("#frmEcom").valid()) {
            //A.addAfterValidate();
            A.SaveTempNames();
            A.addAfterValidate();
            //alert('validated');
        } else {

        }

        return false;

    };
    this.SaveTempNames = function () {

        var B = window.bookingPage.bookingForm;
        var dateRoom = WBFDC.Utils.fixArray(B.dateRoomID);
        if (dateRoom == "") {
            alert('Select Prefered Rooms before adding ');
            return false;
        }

        var arr = WBFDC.Utils.fixArray(B.tempLocationRoom);

        arr = $.map(arr, function (o) { return o.TotalRoomsBooked; });
        var highest = Math.max.apply(this, arr);
        var enqName = WBFDC.Utils.fixArray(B.enquiryName);
        var tempname = [];//WBFDC.Utils.fixArray(B.tempNames);
        if (enqName == "") {
            for (var i = 0; i < highest; i++) {

                var ele1 = $("#nameTable input[name=n_" + i + "]").val();
                var ele2 = $("#nameTable input[name=a_" + i + "]").val();
                var ele3 = $("#nameTable select[name=s_" + i + "]").val();
                //alert(ele1 + " " + ele2 + " " + ele3);
                //alert(tempname);
                tempname.push({
                    GuestName: ele1,
                    Age: parseInt($.trim(ele2)),
                    Sex: ele3
                });


                //alert(JSON.stringify(tempname));
                if (enqName == "") {
                    enqName.push({
                        GuestName: ele1,
                        Age: parseInt($.trim(ele2)),
                        Sex: ele3
                    });
                } else {
                    var found = false;
                    for (var k in enqName) {
                        if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                            found = true;
                        }
                    }
                    if (!found) {
                        enqName.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });
                    }
                }
            }

        } else {
            if ($("#rdOld").is(":checked")) {
                for (var i = 0; i < enqName.length; i++) {

                    var $chk = $("#nameTable input[name=chkN_" + i + "]");
                    if ($chk.is(":checked")) {
                        var ele1 = $("#nameTable input[name=n_" + i + "]").val();
                        var ele2 = $("#nameTable input[name=a_" + i + "]").val();
                        var ele3 = $("#nameTable select[name=s_" + i + "]").val();
                        //alert(ele1 + " " + ele2 + " " + ele3);
                        //alert(tempname);
                        tempname.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });


                        //alert(JSON.stringify(tempname));
                        if (enqName == "") {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        } else {
                            var found = false;
                            for (var k in enqName) {
                                if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                    found = true;
                                }
                            }
                            if (!found) {
                                enqName.push({
                                    GuestName: ele1,
                                    Age: parseInt($.trim(ele2)),
                                    Sex: ele3
                                });
                            }
                        }
                    }
                }
                var enlength = enqName.length;
                if (enlength < highest) {
                    var remain = highest - enlength;
                    for (var j = 0; j < remain; j++) {
                        ////html += "<tr id='trj_" + j + "'>";
                        ////html += "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='nj_" + j + "' name='nj_" + j + "' value=''/></<td>"
                        ////    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='aj_" + j + "' name='aj_" + j + "' value=''/></<td>"
                        ////    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='sj_" + j + "' name='sj_" + j + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                        ////    + "<td></td>";
                        ////html += "</tr>";
                        ///
                        var ele1 = $("#nameTable input[name=nj_" + j + "]").val();
                        var ele2 = $("#nameTable input[name=aj_" + j + "]").val();
                        var ele3 = $("#nameTable select[name=sj_" + j + "]").val();
                        //alert(ele1 + " " + ele2 + " " + ele3);
                        //alert(tempname);
                        tempname.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });


                        //alert(JSON.stringify(tempname));
                        if (enqName == "") {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        } else {
                            var found = false;
                            for (var k in enqName) {
                                if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                    found = true;
                                }
                            }
                            if (!found) {
                                enqName.push({
                                    GuestName: ele1,
                                    Age: parseInt($.trim(ele2)),
                                    Sex: ele3
                                });
                            }
                        }
                    }
                }


            } else if ($("#rdNew").is(":checked")) {
                for (var i = 0; i < highest; i++) {

                    var ele1 = $("#nameNewTable input[name=n_" + i + "]").val();
                    var ele2 = $("#nameNewTable input[name=a_" + i + "]").val();
                    var ele3 = $("#nameNewTable select[name=s_" + i + "]").val();
                    //alert(ele1 + " " + ele2 + " " + ele3);
                    //alert(tempname);
                    tempname.push({
                        GuestName: ele1,
                        Age: parseInt($.trim(ele2)),
                        Sex: ele3
                    });


                    //alert(JSON.stringify(tempname));
                    if (enqName == "") {
                        enqName.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });
                    } else {
                        var found = false;
                        for (var k in enqName) {
                            if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                found = true;
                            }
                        }
                        if (!found) {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        }
                    }
                }
            }
        }
        B.tempNames = tempname;
        //alert(JSON.stringify(B.tempNames));
    };
    this.sortJSON = function (data, key) {
        return data.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    };
    this.addAfterValidate = function () {

        //////////////var people = [ 
        //////////////{ 'myKey': 'A', 'status': 0 },
        //////////////{ 'myKey': 'D', 'status': 3 },
        //////////////{ 'myKey': 'E', 'status': 3 },
        //////////////{ 'myKey': 'F', 'status': 2 },
        //////////////{ 'myKey': 'G', 'status': 7 }      
        //////////////];
        //////////////var people2 = A.sortJSON(people, 'status');
        //////////////alert("1. After processing (0 to x): " + JSON.stringify(people2));

        var B = A.bookingForm;
        var arr = WBFDC.Utils.fixArray(B.tempLocationRoom);
        arr = $.map(arr, function (o) { return o.TotalRoomsBooked; });
        var highest = Math.max.apply(this, arr);
        //alert(highest);

        var tempname = WBFDC.Utils.fixArray(B.tempNames);
        if (tempname == "") {
            alert('Guest Names Details Not Added');
            return false;
        } else if (tempname.length != highest) {
            alert('Guest Names Details Not Added');
            return false;
        }

        var frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
        var toDate = new Date($("#txtBookingDateTo").datepicker('getDate'));
        var totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;

        var EnquiryTokenDetID = $("#EnquiryTokenDetID").val();
        var totBooking = $("#TotalBookingNo").val();
        var nor = ($("#txtNOR").val());
        var totAdded = B.enquiryMaster.TotalNoOfRooms;
        var tot = (parseInt(nor) * parseInt(totalDays)) + parseInt(totAdded);
        //alert('totalDays -->  ' + totalDays);
        var C = [];
        if (tot <= totBooking) {
            for (var i = 0; i < totalDays; i++) {
                var frmdate1 = new Date(frmdate.getTime() + i * 24 * 60 * 60 * 1000);
                var curr_date = frmdate1.getDate();
                var curr_month = frmdate1.getMonth();
                curr_month = curr_month + 1;
                var curr_year = frmdate1.getFullYear();
                var cdate = curr_date + '/' + curr_month + '/' + curr_year;
                var dateRoom = WBFDC.Utils.fixArray(A.bookingForm.dateRoomID);
                var enqName1 = WBFDC.Utils.fixArray(A.bookingForm.tempNames);
                var enqName = A.sortJSON(enqName1, "Age");
                ////alert("EnqName : =>  " + JSON.stringify(enqName));
                var enquiryRoomDet = [];
                var TotalRoomsSelected = 0;
                var k = 0;
                
                if (dateRoom.length > 0) {
                    //alert(dateRoom.length);

                    for (var j in dateRoom) {
                        var spldt = (dateRoom[j].BookingDate).split('/');
                        var bookingdt = new Date();
                        bookingdt.setFullYear(spldt[2], (spldt[1] - 1), spldt[0]);
                        var chkdate = new Date();
                        chkdate.setFullYear(curr_year, (curr_month - 1), curr_date);
                        //alert(" bookingdt----> " + bookingdt.getTime() + " ----- frmdate1----> " + chkdate.getTime());
                        if (chkdate.getTime() == bookingdt.getTime()) {
                            //alert('matched cdate=' + cdate + ' roomid=' + dateRoom[j].AccomodationDetailID + '  i=' + i + " j=" + j + " k=" + k);
                            //var enquirName=B.enquiryDetail.enquiryRoomDetail.enquiryName ;
                            var enquirName = {};
                            //.push({
                            enquirName.GuestName = enqName[k].GuestName;
                            enquirName.Age = enqName[k].Age;
                            enquirName.Sex = enqName[k].Sex;
                            //});
                            k += 1;
                            enquiryRoomDet.push({
                                BookingDate: cdate,
                                AccomodationDetailID: dateRoom[j].AccomodationDetailID,
                                AccomodationName: dateRoom[j].AccomodationName,
                                enquiryName: enquirName
                            });
                            //alert(JSON.stringify(enquiryRoomDet));
                        }
                    }
                    if (k > 0) {
                        ////B.enquiryDetail.push({
                        ////    BookingDate: cdate,
                        ////    LocationID: $("#enquiry").field("ddlLocation"),
                        ////    LocationName: $("#ddlLocation option:selected").text().trim(),
                        ////    PropertyID: $("#enquiry").field("ddlProperty"),
                        ////    PropertyName: $("#ddlProperty option:selected").text().trim(),
                        ////    RangeFrom: $("#enquiry").field("ddlFrom"),
                        ////    RangeTo: $("#enquiry").field("ddlTo"),
                        ////    RangeOrderPreference: ($("#enquiry").field("range")),
                        ////    ACNonAC: ($("#enquiry").field("ddlAC")),
                        ////    ACNonACCompulsory: ($("#chkAC").is(':checked') ? 'Y' : 'N'),
                        ////    //NOR: ($("#enquiry").field("txtNOR")),
                        ////    NOR: k,
                        ////    AlternateLocation: ($("#enquiry").field("ddlAlternate")),
                        ////    PartBooking: ($("#chkPart").is(':checked') ? 'Y' : 'N'),
                        ////    enquiryRoomDetail: enquiryRoomDet
                        ////});
                        C.push({
                            BookingDate: cdate,
                            EnquiryTokenDetID: EnquiryTokenDetID,
                            LocationID: $("#ddlLocation").val(),
                            LocationName: $.trim($("#ddlLocation option:selected").text()),
                            PropertyID: $("#ddlProperty").val(),
                            PropertyName: $.trim($("#ddlProperty option:selected").text()),
                            RangeFrom: $("#ddlFrom").val(),
                            RangeTo: $("#ddlTo").val(),
                            RangeOrderPreference: ($("#rdAsc").is(":checked") ? $("#rdAsc").val() : $("#rdDesc").val()),
                            ACNonAC: ($("#ddlAC").val()),
                            ACNonACCompulsory: ($("#chkAC").is(':checked') ? 'Y' : 'N'),
                            //NOR: ($("#txtNOR").val()),
                            NOR: k,
                            AlternateLocation: ($("#ddlAlternate").val()),
                            PartBooking: ($("#chkPart").is(':checked') ? 'Y' : 'N'),
                            enquiryRoomDetail: enquiryRoomDet
                        });
                    }
                }
            }

            var K = {};
            K.enquiryDetail = C;
            B.enquiryMaster.TotalNoOfRooms = tot;

            B.dateRoomID = [];
            B.tempLocationRoom = [];
            B.tempNames = [];
            //alert(JSON.stringify(K));
            
            $("#LoadNamesDet").hide();
            A.captureDetails(K);
            

        } else {
            alert("Maximum Allowed Booking is " + totBooking + ". Either " + totBooking + " days OR " + totBooking + " No of Rooms. You have booked " + totAdded + " room(s)/day(s). You can book " + (totBooking - totAdded) + " total no of rooms/days");
            $(".loading-overlay").hide();
        }
    };

    //this.addAfterValidate = function () {
    //    var frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
    //    var toDate = new Date($("#txtBookingDateTo").datepicker('getDate'));
    //    var totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;
    //    var EnquiryTokenDetID = $("#EnquiryTokenDetID").val();
    //    var B = window.bookingPage.bookingForm;
    //    var totBooking = $("#TotalBookingNo").val();
    //    var nor = ($("#txtNOR").val());

    //    var totAdded = B.enquiryMaster.TotalNoOfRooms;
    //    var tot = parseInt(nor) + parseInt(totAdded);
    //    var C = [];
    //    if (tot <= totBooking) {
            
    //        for (var i = 0; i < totalDays; i++) {
    //            var frmdate1 = new Date(frmdate.getTime() + i * 24 * 60 * 60 * 1000);
    //            var curr_date = frmdate1.getDate();
    //            var curr_month = frmdate1.getMonth();
    //            curr_month = curr_month + 1;
    //            var curr_year = frmdate1.getFullYear();
    //            var cdate = curr_date + '/' + curr_month + '/' + curr_year;
    //            var dateRoom = WBFDC.Utils.fixArray(A.bookingForm.dateRoomID);;
    //            var enquiryRoomDet = [];
    //            if (dateRoom.length > 0) {
    //                //alert(dateRoom.length);
    //                for (var j in dateRoom) {
    //                    var spldt = (dateRoom[j].BookingDate).split('/');
    //                    var bookingdt = new Date();
    //                    bookingdt.setFullYear(spldt[2], (spldt[1] - 1), spldt[0]);
    //                    var chkdate = new Date();
    //                    chkdate.setFullYear(curr_year, (curr_month - 1), curr_date);
    //                    //alert(" bookingdt----> "+bookingdt.getTime() +" ----- frmdate1----> "+chkdate.getTime());
    //                    if (chkdate.getTime() == bookingdt.getTime()) {
    //                        //alert('matched');
    //                        enquiryRoomDet.push({
    //                            BookingDate: cdate,
    //                            AccomodationDetailID: dateRoom[j].AccomodationDetailID,
    //                            AccomodationName: dateRoom[j].AccomodationName
    //                        });
    //                    }
    //                }
    //            }

    //            C.push({
    //                BookingDate: cdate,
    //                EnquiryTokenDetID: EnquiryTokenDetID,
    //                LocationID: $("#ddlLocation").val(),
    //                LocationName: $.trim($("#ddlLocation option:selected").text()),
    //                PropertyID: $("#ddlProperty").val(),
    //                PropertyName: $.trim($("#ddlProperty option:selected").text()),
    //                RangeFrom: $("#ddlFrom").val(),
    //                RangeTo: $("#ddlTo").val(),
    //                RangeOrderPreference: ($("#rdAsc").is(":checked") ? $("#rdAsc").val() : $("#rdDesc").val()),
    //                ACNonAC: ($("#ddlAC").val()),
    //                ACNonACCompulsory: ($("#chkAC").is(':checked') ? 'Y' : 'N'),
    //                NOR: ($("#txtNOR").val()),
    //                AlternateLocation: ($("#ddlAlternate").val()),
    //                PartBooking: ($("#chkPart").is(':checked') ? 'Y' : 'N'),
    //                enquiryRoomDetail: enquiryRoomDet
    //            });
    //        }
    //        var K = {};
    //        K.enquiryDetail = C;

    //        B.enquiryMaster.TotalNoOfRooms = tot;

    //        B.dateRoomID = [];
    //        //alert(JSON.stringify(K));
    //        $("#loadRoomDet").hide();
    //        window.bookingPage.captureDetails(K);
    //        //window.enquiryPage.showEnquiryDetail();

    //    } else {
    //        alert("Maximum Allowed Booking is " + totBooking + ". Either " + totBooking + " days OR " + totBooking + " No of Rooms. You have booked " + totAdded + " room(s)/day(s). You can book " + (totBooking - totAdded) + " total no of rooms/days");
    //        $(".loading-overlay").hide();
    //    }
    //};

    this.CloseNamesContent = function () {
        $("#LoadNamesDet").hide();
        A.bookingForm.dateRoomID = [];
        A.bookingForm.tempLocationRoom = [];
        A.bookingForm.tempNames = [];
        return false;
    };
    this.checkGuestExistence = function (EnquiryTokenDetID) {
        $("#loadBookingDet").hide();
        $(".loading-overlay").show();
        var B = window.bookingPage.bookingForm;
        var E = "{mobileno:" + B.enquiryMaster.Mobile + ",EnquiryTokenDetID:" + EnquiryTokenDetID + "}";
        //var C = JSON.stringify(E);
        //alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/CheckGuest',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = jQuery.parseJSON(D.d);
                window.bookingPage.bookingForm.enquiryMaster.TotalNoOfRooms = t.TotalNoofRooms;
                //alert(t.TotalNoofRooms);

                var frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
                var toDate = new Date($("#txtBookingDateTo").datepicker('getDate'));
                var totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;

                var B = window.bookingPage.bookingForm;
                var totBooking = $("#TotalBookingNo").val();
                var nor = ($("#txtNOR").val());
                var totAdded = B.enquiryMaster.TotalNoOfRooms;
                //var tot = parseInt(nor) + parseInt(totAdded);
                var tot = (parseInt(nor) * parseInt(totalDays)) + parseInt(totAdded);
                //alert("totBooking " + totBooking + "  nor  " + nor + " totAdded " + totAdded + " tot  " + tot);
                var C = {};

                if (tot <= totBooking) {
                    A.showRoomAvailablity(totBooking - parseInt(totAdded));
                    //////////////                    C.EnquiryTokenDetID = EnquiryTokenDetID;
                    //////////////                    C.BookingDate = $("#txtBookingDate").val();
                    //////////////                    C.LocationID = $("#ddlLocation").val();
                    //////////////                    C.LocationName = $("#ddlLocation option:selected").text().trim();
                    //////////////                    C.PropertyID = $("#ddlProperty").val();
                    //////////////                    C.PropertyName = $("#ddlProperty option:selected").text().trim();
                    //////////////                    C.RangeFrom = $("#ddlFrom").val();
                    //////////////                    C.RangeTo = $("#ddlTo").val();
                    //////////////                    C.RangeOrderPreference = ($("#rdAsc").is(":checked") ? $("#rdAsc").val() : $("#rdDesc").val());
                    //////////////                    C.ACNonAC = ($("#ddlAC").val());
                    //////////////                    C.ACNonACCompulsory = ($("#chkAC").is(':checked') ? 'Y' : 'N');
                    //////////////                    C.NOR = ($("#txtNOR").val());
                    //////////////                    C.AlternateLocation = ($("#ddlAlternate").val());
                    //////////////                    C.PartBooking = ($("#chkPart").is(':checked') ? 'Y' : 'N');
                    //////////////                    //B.enquiryDetail = enquiryDetail;

                    //////////////                    B.enquiryMaster.TotalNoOfRooms = tot;
                    //////////////                    
                    //////////////                    A.captureDetails(C);


                } else {
                    ////                    alert("You have booked " + totAdded + " room(s)/day(s). Maximum Allowed Booking is " + totBooking + ". Either " + totBooking + " days OR " + totBooking + " No of Rooms.");
                    ////                    $(".loading-overlay").hide();
                    alert("Maximum Allowed Booking is " + totBooking + ". Either " + totBooking + " days OR " + totBooking + " No of Rooms. You have booked " + totAdded + " room(s)/day(s). You can book " + (totBooking - totAdded) + " total no of rooms/days");
                    $(".loading-overlay").hide();
                    $("#loadBookingDet").show();
                }

            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });


    };
    this.PayModeRadioClicked = function (D) {
        var val = $(D).val();
        if (val == "B" || val == "D" || val == "S") {
            $("#txtBank").attr("disabled", false);
            $("#txtBranch").attr("disabled", false);
            $("#txtChequeNo").attr("disabled", false);
            $("#txtChequeDate").attr("disabled", false);
            $("#btnDatePickerCheque").attr("disabled", false);
        } else {
            $("#txtBank").attr("disabled", true);
            $("#txtBranch").attr("disabled", true);
            $("#txtChequeNo").attr("disabled", true);
            $("#txtChequeDate").attr("disabled", true);
            $("#btnDatePickerCheque").attr("disabled", true);
        }
    };
    this.RadioClicked = function (C) {

        if ($(C).val() == "V") {
            if ($(C).is(":checked")) {
                $("#vipType").show();
            } else {
                $("#vipType").hide();
            }
        } else {
            $("#vipType").hide();
        }

    };
    this.captureDetails = function (C) {
        $(".loading-overlay").show();
        var B = {};
        //alert('b empty');
        B = C;
        //alert('b added');
        var E = "{\'enquiryDetail\':\'" + JSON.stringify(B) + "\',EnquiryTokenID:" + A.bookingForm.enquiryMaster.EnquiryTokenID
        + ",BookingType:'" + A.bookingForm.enquiryMaster.BookingType + "',BookingVIPType:'" + A.bookingForm.enquiryMaster.BookingVIPType + "'}";
        //var C = JSON.stringify(E);
        //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/AddEnquiryDetails',
            data: E,
            contentType: "application/json; charset=utf-8",
            //            success: function (D) {
            //                A.bookingForm = JSON.parse(D.d);
            //                alert(JSON.stringify(D.d));
            //                A.showDetails(A.bookingForm.enquiryDetail);
            //                A.showMasterData(A.bookingForm.enquiryMaster);
            //                A.showPaymentData(A.bookingForm.paymentInfo);

            //                $(".loading-overlay").hide();
            //            },
            success: A.OnBookedProcessed,
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.ChangeRateCharge = function (EnquiryTokenDetID, AccomodationID) {
        var elemRates = "#txtRates_" + EnquiryTokenDetID + "_" + AccomodationID;
        var RateValue = $(elemRates).val();

        var enqdetails = A.getEnquiryDetail(this.bookingForm.enquiryDetail, EnquiryTokenDetID);

        if (enqdetails != "undefined") {
            var bookdetails = A.getBookingDetail(enqdetails.bookedDetails, AccomodationID);
            if (bookdetails != "undefined") {
                bookdetails.Rates = parseInt(RateValue);
                var discountValue = (bookdetails.OriginalRates - bookdetails.Rates);
                //alert(discountValue + "  " + RateValue);
                bookdetails.discount = parseInt(discountValue);

            }
        }
        A.updateBookedDetails();


    };

    this.ChangeLTCharge = function (EnquiryTokenDetID, AccomodationID) {
        var elemRates = "#txtLT_" + EnquiryTokenDetID + "_" + AccomodationID;
        var LTValue = $(elemRates).val();

        var enqdetails = A.getEnquiryDetail(this.bookingForm.enquiryDetail, EnquiryTokenDetID);

        if (enqdetails != "undefined") {
            var bookdetails = A.getBookingDetail(enqdetails.bookedDetails, AccomodationID);
            if (bookdetails != "undefined") {
                bookdetails.LuxuryTaxValue = parseInt(LTValue);
                //var discountValue = (bookdetails.OriginalRates - bookdetails.Rates);
                //alert(discountValue + "  " + RateValue);
                //bookdetails.discount = parseInt(discountValue);

            }
        }
        A.updateBookedDetails();


    };

    this.ChangeSTCharge = function (EnquiryTokenDetID, AccomodationID) {
        var elemRates = "#txtST_" + EnquiryTokenDetID + "_" + AccomodationID;
        var STValue = $(elemRates).val();

        var enqdetails = A.getEnquiryDetail(this.bookingForm.enquiryDetail, EnquiryTokenDetID);

        if (enqdetails != "undefined") {
            var bookdetails = A.getBookingDetail(enqdetails.bookedDetails, AccomodationID);
            if (bookdetails != "undefined") {
                if (parseInt(STValue) == 0) {
                    bookdetails.serviceTaxValue = parseInt(0);
                    bookdetails.ecessValue = parseInt(0);
                    bookdetails.hcessValue = parseInt(0);
                    bookdetails.extraCharge = parseInt(0);
                    //var discountValue = (bookdetails.OriginalRates - bookdetails.Rates);
                    //alert(discountValue + "  " + RateValue);
                    //bookdetails.discount = parseInt(discountValue);
                }
            }
        }
        A.updateBookedDetails();


    };

    this.ChangeCotCharge = function (EnquiryTokenDetID, AccomodationID, ACNONACTYPE, ACNONAC_Charge) {
        var elemcotno = "#txtCotNo_" + EnquiryTokenDetID + "_" + AccomodationID;
        var cotno = $(elemcotno).val();
        if (cotno == "") {
            cotno = 0;
            $(elemcotno).val(cotno);
        } else if (cotno > 1) {
            alert("Maximum one(1) extra cot allowed per room.");
            cotno = 1;
            $(elemcotno).val(cotno);

        }
        //var cotchg = "#txtCotChrg_" + EnquiryTokenDetID + "_" + AccomodationID;


        //        if ($(cotno).val() != 0) {
        //            $(cotchg).val(ACNONAC_Charge);
        //        } else {
        //            $(cotchg).val(0);
        //        }

        var enqdetails = A.getEnquiryDetail(this.bookingForm.enquiryDetail, EnquiryTokenDetID);

        if (enqdetails != "undefined") {
            var bookdetails = A.getBookingDetail(enqdetails.bookedDetails, AccomodationID);
            if (bookdetails != "undefined") {
                bookdetails.ExtraCot = cotno;
                var extraCotvalue = (cotno * ACNONAC_Charge);
                bookdetails.ExtraCotValue = extraCotvalue;
                //                        var subtot = (bookdetails.Rates + bookdetails.LuxuryTaxValue + bookdetails.serviceTaxValue + bookdetails.ecessValue + bookdetails.hcessValue + extraCotvalue);
                //                        subtot = roundNumber(subtot, 0);
                //                        bookdetails.subTotal = subtot
                //                        var tdsubtot = "#tdsubTotal_" + EnquiryTokenDetID + "_" + AccomodationID;
                //                        $(tdsubtot).html(subtot);
                //                        //alert(JSON.stringify(bookdetails));
            }
        }
        A.updateBookedDetails();


    };

    this.updateBookedDetails = function () {
        var gt = 0;


        var enquiryDetail = this.bookingForm.enquiryDetail;
        if (enquiryDetail != "undefined") {
            if (enquiryDetail.length > 0) {
                var enqDetails = WBFDC.Utils.fixArray(enquiryDetail);
                for (var i in enqDetails) {
                    var st = 0;
                    var bookDetails = WBFDC.Utils.fixArray(enqDetails[i].bookedDetails);
                    if (bookDetails != "undefined") {
                        if (bookDetails.length > 0) {
                            for (var j in bookDetails) {
                                //if (bookDetails[j].Accomodation_Details_ID == AccomodationID) {
                                //bookDetails[j].ExtraCot = cotno;
                                //var extraCotvalue = (cotno * ACNONAC_Charge);
                                //bookDetails[j].ExtraCotValue = extraCotvalue;
                                var subtot = (bookDetails[j].Rates + bookDetails[j].LuxuryTaxValue + bookDetails[j].serviceTaxValue +
                                                bookDetails[j].ecessValue + bookDetails[j].hcessValue + bookDetails[j].ExtraCotValue + bookDetails[j].OtherValue);
                                subtot = roundNumber(subtot, 0);
                                bookDetails[j].subTotal = subtot
                                var tdsubtot = "#tdsubTotal_" + enquiryDetail[i].EnquiryTokenDetID + "_" + bookDetails[j].Accomodation_Details_ID;
                                $(tdsubtot).html(subtot);

                                //}
                                var t = 0;
                                t = bookDetails[j].subTotal;
                                st += t;
                            }
                        }
                    }

                    //if (enquiryDetail[i].EnquiryTokenDetID == EnquiryTokenDetID) {
                    var tdtotsubtot = "#tdtotsubtotal_" + enquiryDetail[i].EnquiryTokenDetID;
                    $(tdtotsubtot).html(st);
                    //}
                    enquiryDetail[i].totsubTotal = st;
                    gt += st;
                }
            }
        }
        A.bookingForm.paymentInfo.GrandTotal = gt;
        window.bookingPage.showPaymentData(window.bookingPage.bookingForm.paymentInfo);
        A.OnChangeUpdateDates();
        //alert(JSON.stringify(this.bookingForm));
    };

    this.OnChangeUpdateDates = function () {
        //$(".loading-overlay").show();
        var B = window.bookingPage.bookingForm;
        var E = "{\'BookingForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/UpdateDates',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var r = jQuery.parseJSON(response.d);
                if (r.Result == "Success") {
                    //alert(r.GrandTotal);
                    A.bookingForm.paymentInfo.GrandTotal = r.GrandTotal;
                    //alert(A.bookingForm.paymentInfo.GrandTotal);
                    
                    $(".loading-overlay").hide();
                }
               // $(".loading-overlay").hide();
            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };

    this.showMasterData = function (enquiryMaster) {
        if (enquiryMaster != "undefined" && enquiryMaster != null) {
            $('#txtName').val(enquiryMaster.Name);
            $('#txtAddress').val(enquiryMaster.Address);
            $('#txtCity').val(enquiryMaster.City);
            $('#txtState').val(enquiryMaster.State);
            $('#txtCountry').val(enquiryMaster.Country);
            $('#txtEmail').val(enquiryMaster.Email);
            $('#txtMobile').val(enquiryMaster.Mobile);
            $('#ddlIDProofType').val(enquiryMaster.IDProofTypeID);
            $('#txtIDProofNo').val(enquiryMaster.IDProofNo);
            $('#txtNOA').val(enquiryMaster.NOA);
            $('#txtNOC').val(enquiryMaster.NOC);
        }
    };
    this.showPaymentData = function (paymentInfo) {
        if (paymentInfo != "undefined" && paymentInfo != null) {
            $('#txtTotalCharges').val(paymentInfo.GrandTotal);
            $('#txtBank').val(paymentInfo.Bank);
            $('#txtBranch').val(paymentInfo.Branch);
            $('#txtChequeNo').val(paymentInfo.ChequeNo);
            //if ($('#txtChequeDate').val() != "") {
            //    $('#txtChequeDate').val(paymentInfo.ChequeDate);
            //}
            if (paymentInfo.PaidStatus != null) {
                ////            if (paymentInfo.PaidStatus == "P") {
                ////                $("#rd1").attr("checked", true);
                ////                //$("#rd2").attr("checked", false);
                ////            } else {
                ////                $("#rd2").attr("checked", true);
                ////                //$("#rd1").attr("checked", false);
                ////            }
//////////                if (paymentInfo.PaidStatus == "N") {
//////////                    $("#rd2").attr("checked", true);
//////////                } else {
//////////                    $("#rd1").attr("checked", true);
//////////                }

            }
        }
    };

    this.getBookingDetail = function (arr, code) {
        var ar = WBFDC.Utils.fixArray(arr);
        for (var i = 0; i < ar.length; i++) {
            if (ar[i].Accomodation_Details_ID == code) {
                return ar[i];
            }
        }
        return null;
    };

    this.getEnquiryDetail = function (arr, code) {
        var ar = WBFDC.Utils.fixArray(arr);
        for (var i = 0; i < ar.length; i++) {
            if (ar[i].EnquiryTokenDetID == code) {
                return ar[i];
            }
        }
        return null;
    };
    this.removeEnquiryDetail = function (EnquiryTokenDetID, AccomodationID) {
        //alert(EnquiryTokenDetID + ' - - ' + AccomodationID);
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';

        var removeElemTr = (AccomodationID == 0 ? "#tddeleteEnquiryTokenDetID_" + EnquiryTokenDetID : "#tddeleteAccomodationID_" + EnquiryTokenDetID + "_" + AccomodationID);
        $(removeElemTr).append(ajaxLoader);
        var ajaxUrl = pageUrl + '/DeleteEnquiryAccomodationDetail';

        var B = {};
        //alert('b empty');
        B = window.bookingPage.bookingForm;
        //A.findAndRemove(B.enquiryForm.enquiryDetail, 'EnquiryTokenDetID', EnquiryTokenDetID);

        //alert('b added');
        var E = "{EnquiryTokenID:" + B.enquiryMaster.EnquiryTokenID + ",EnquiryTokenDetID:" + EnquiryTokenDetID + ",AccomodationID:" + AccomodationID + ",BookingType:'" + B.enquiryMaster.BookingType + "'}";
        //var C = JSON.stringify(E);
        //alert(E);
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.bookingPage.bookingForm = JSON.parse(D.d);

                //alert(JSON.stringify(window.enquiryPage.enquiryForm));
                $("#innerTable table[id='detailTable']").remove();
                A.showDetails(window.bookingPage.bookingForm.enquiryDetail);
                $("#innerTable table[id='prodTab']").hide();
                $("#innerTable table[id='prodTab']").show("slow");

            }
        });
        return false;
    };
    this.PopulateControl = function (list, control) {
        //alert(JSON.stringify(list));
        if (list.length > 0) {
            control.removeAttr("disabled");
            control.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(list, function () {
                control.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
        else {
            control.empty().append('<option selected="selected" value="-1">Not available<option>');
        }
    };
    this.PopulateProperty = function () {
        //alert('in prop');
        $("#ddlProperty").attr("disabled", "disabled");
        $("#ddlFrom").attr("disabled", "disabled");
        $("#ddlTo").attr("disabled", "disabled");
        if ($('#ddlLocation').val() == "") {
            $('#ddlProperty').empty().append('<option selected="selected" value="">Please select</option>');
            $('#ddlFrom').empty().append('<option selected="selected" value="">Please select</option>');
            $('#ddlTo').empty().append('<option selected="selected" value="">Please select</option>');
        }
        else {
            $('#ddlProperty').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                url: pageUrl + '/populateProperty',
                data: '{LocationID: ' + $('#ddlLocation').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: A.OnPropertyPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };

    this.PopulateTariff = function () {
        if ($.trim($("#txtBookingDate").val()) != "") {
            $("#ddlFrom").attr("disabled", "disabled");
            $("#ddlTo").attr("disabled", "disabled");
            if ($('#ddlProperty').val() == "" || $('#ddlProperty').val() == "0") {
                $('#ddlFrom').empty().append('<option selected="selected" value="">Please select</option>');
                $('#ddlTo').empty().append('<option selected="selected" value="">Please select</option>');

            }
            else {
                $('#ddlFrom').empty().append('<option selected="selected" value="">Loading...</option>');
                $('#ddlTo').empty().append('<option selected="selected" value="">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/populateTariffRange',
                    data: "{BookingDate: '" + $('#txtBookingDate').val() + "',LocationID: " + $('#ddlLocation').val() + ",PropertyID: " + $('#ddlProperty').val() + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: A.OnTariffPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }
    };

    this.OnTariffPopulated = function (response) {
        //alert('in success');
        A.PopulateControl(response.d, $("#ddlFrom"));
        A.PopulateControl(response.d, $("#ddlTo"));
    };

    this.OnPropertyPopulated = function (response) {
        A.PopulateControl(response.d, $("#ddlProperty"));
    };
};