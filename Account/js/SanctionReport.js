﻿
$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });

    $('#txtFrmDate, #txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });

    $("#btnGenerateReport").click(function (event) {
        if ($("#ddlReportName").val() == "0") {
            alert("Select a Report Name!");
            $("#ddlReportName").focus();
            return false;
        }
        if ($("#txtFrmDate").val() == "") {
            alert("Please Select Sanction From Date!");
            $("#txtFrmDate").focus();
            return false;
        }
        if ($("#txtToDate").val() == "") {
            alert("Please Select Sanction To Date!");
            $("#txtToDate").focus();
            return false;
        }
        ShowReport();
        return false;
    });
    $("#ddlReportName").focus();
    $("#ddlReportName").val('SR');
});

function ShowReport() {
    var FormName = '';
    var ReportName = '';
    var ReportType = '';
    var SacFromDate = '';
    var SacToDate = '';
    var ReportID = '';
    var RptShortBy = '';
    var TotProjCost = "0";
    var SecID = 0;
    SecID = $("#ddlUserSector").val();
    var chkAllSector = document.getElementById("chkAllSector");
    if (chkAllSector.checked == true) { SecID = 0; }
    if ($("#ddlReportName").val() == "SR") {
        FormName = "SanctionReport.aspx";
        ReportName = "Sectionreport";
        ReportType = "Section Report";
        ReportID = $("#ddlReportName").val();
    }
    RptShortBy = $('#ddlRptShortBy').val();
    if ($('#txtFrmDate').val() != '') {
        var fromDate = $("#txtFrmDate").val().split("/");
        SacFromDate = fromDate[2] + "-" + fromDate[1] + "-" + fromDate[0];
    }
    if ($('#txtToDate').val() != '') {
        var ToDate = $("#txtToDate").val().split("/");
        SacToDate = ToDate[2] + "-" + ToDate[1] + "-" + ToDate[0];
    }
    if (SacFromDate > SacToDate)
    {
        alert('Sanction To Date Should Be Greater Than Sanction From Date');
        $("#txtToDate").focus();
        return false;
    } 
    
    if (document.getElementById("chkTotProjCost").checked == true) { TotProjCost = '1'; }

    var E = '';  
    E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  SacFromDate:'" + SacFromDate + "',  SacToDate:'" + SacToDate + "',ReportID:'" + ReportID + "',RptShortBy:'" + RptShortBy + "',TotProjCost:'" + TotProjCost + "',SecID:" + SecID +"}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/SetReportValue',
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var show = response.d;
            if (show == "OK") {
                window.open("ReportView.aspx?E=Y");            
            }

        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


