﻿

var vchslr = 0;
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

lpad = function (padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

function leftpad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

$(document).ready(function () {
    $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });

  
    $("#txtVoucherDate").change(function () {
        var yearMonth = $("#txtYearMonth").val();
        var get_Month = parseInt(yearMonth.substring(4, 6));
        var get_Year = parseInt(yearMonth.substring(0, 4));
        var dateVal = $("#txtVoucherDate").val();
        var month = parseInt(dateVal.substring(3, 5));
        var year = parseInt(dateVal.substring(6, 10));
        if ((get_Month != month && get_Year == year) || (get_Month == month && get_Year != year) || (get_Month != month && get_Year != year)) {
            $("#txtVoucherDate").val('01/' + yearMonth.substring(4, 6) + '/' + yearMonth.substring(0, 4));
            //alert(dateVal);
            alert(" Can not change. Select a date between given YearMonth (" + yearMonth+")");
            return false;
        }

        //alert(get_Month + '--' + get_Year + '--' + month + '--' + year + " Check");
    });
    
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    $('#txtVoucherDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        maxDate: 'now'

    });
    $('#txtInstDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            //this.focus();
            $("#txtInstAmount").focus();
        }

    });
    $('#txtInstDate').mask("99/99/9999", { placeholder: "_" });
    $('#txtVoucherDate').mask("99/99/9999", { placeholder: "_" });
    $("#btnDatePicker").click(function () {
        $('#txtVoucherDate').datepicker("show");
        return false;
    });
    window.voucherInfo = new WBFC.VoucherInfo();
    window.voucherInfo.init();

    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });

});
$.validator.addMethod("numeric", function (value, element, param) {
    if (param) {
        return this.optional(element) || value == value.match(/^[0-9]+$/);
    }
    return true;
    // --                                    or leave a space here ^^
}, "No wild card characters like - ,*,?,! is allowed. Entry should be numeric only , i.e within 0-9");
WBFC.VoucherMaster = function () {
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {
        $("#txtDebit_0").prop("disabled",true);
        $("#txtCredit_0").prop("disabled",true);

        this.validator = A.getValidator();

        $("#txtYearMonth").focus();
        $("#txtYearMonth").blur(A.YearMonthChanged);
        $("#txtYearMonth").keypress(A.YearMonKeyPress);
        $("#ddlVchType").keypress(A.VchTypeKeyPress);
        $("#txtVoucherDate").keypress(A.VchDateKeyPress);
        $("#lblAccDesc_0").keypress(A.GLSLAccKeyPress);

        $("#txtGLSLDescSearch").keyup(A.TxtGlSlKeyUP);
        $("#txtGLSLDescSearch").keypress(A.TxtGlSlKeyPress);


        $("#txtSUBDescSearch").keyup(A.TxtSubKeyUP);
        $("#txtSUBDescSearch").keypress(A.TxtSubKeyPress);

        $("#lblSubAccDesc_0").keypress(A.SUBAccKeyPress);

        $("#ddlDRCR").change(A.DRCRChanged);
        $("#ddlDRCR").keypress(A.DRCRKeyDown);
        $("#txtDebit_0").keypress(A.DebitKeyDown);
        $("#txtCredit_0").keypress(A.DebitKeyDown);
        $("#ddlInstType").change(A.CallInstypeBlur);
        $("#ddlInstType").keypress(A.InstTypeKeyPress);
        $("#btnInstAdd").click(A.InstrumentAdd);
        A.SetTabIndexInstType();
        A.SetTabIndexFA();
        
        //$("#txtSlipNo").keypress(A.SlipNoKeyPress);
        //$("#txtInstNo").keypress(A.InstNoKeyPress);
        //$("#txtInstDate").keypress(A.InstDateKeyPress);
        $("#txtInstAmount").keypress(A.InstAmountKeyPress);
        //$("#txtDBankBR").keypress(A.DBankBRKeyPress);
        $("#txtDBankBR").keydown(A.DBankBRKeyDown);
        //$("#txtGLSLDescSearch").keypress(A.TxtGlSlKeyPress);

        $("#txtFAValue").keydown(A.FAValueKeyDown);

        $("#cmdSave").click(A.SaveClicked);
        $("#cmdRefresh").click(A.RefreshClicked);
        $("#ddlVchType").val(0);
        A.SetYearMonth();

    };
    this.DRCRKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            A.DRCRChanged();
        }
    }
    this.ShowModalPopupFA = function () {
        $("#dialogFA").dialog({
            title: "Fixed Asset Entry",
            width: 980,

            buttons: {
                Ok: function () {
                    //A.InstrumentAdd();
                    // A.CallculateInstAmount();
                }
            },
            modal: true
        });
    };

    this.FAValueKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            A.FAAdd();
            return false;

        } else {
            return true;
        }
        return false;

    };
    this.FAAdd = function () {

        if ($.trim($("#txtFAD").val()) == "") {
            alert("Please enter Fixed Asset Description");
            $("#txtFAD").focus();
            return false;
        }

        if ($.trim($("#ddlLocation").val()) == "") {
            alert("Please Select Location ");
            $("#ddlLocation").focus();
            return false;
        }

        if ($.trim($("#ddlSupplier").val()) == "") {
            alert("Please Select Supplier ");
            $("#ddlSupplier").focus();
            return false;
        }

        if ($.trim($("#txtDRate").val()) == "") {
            alert("Please enter depreciation rate ");
            $("#txtDRate").focus();
            return false;
        }

        if ($.trim($("#txtFAValue").val()) == "" || parseFloat($.trim($("#txtFAValue").val())) <= 0) {
            alert("Please enter asset value ");
            $("#txtFAValue").focus();
            return false;
        }

        var B = window.voucherInfo.voucherForm;
        var SectorID = B.VoucherMast.SectorID;
        var C = WBFC.Utils.fixArray(B.tmpVchFA);

        var faid = C.length + 1;
        C.push({
            FA_ID: faid,
            GL_ID: $("#lblGlID").val(),
            FA_DESC: $("#txtFAD").val(),
            VENDOR_CODE: $("#ddlSupplier").val(),
            VENDOR_NAME: $("#ddlSupplier option:selected").text(),
            DEP_RATE: $("#txtDRate").val(),
            DEP_METHOD: 'S',
            PUR_VALUE: ($("#ddlDRCR").val() == "D" ? $("#txtFAValue").val() : 0),
            PUR_VCH_NO: 0,
            PUR_VCH_DATE: ($("#ddlDRCR").val() == "D" ? $("#txtVoucherDate").val() : ""),
            VCH_SECTORID: SectorID,//$("#ddlLocation").val(),            
            SOL_VALUE: ($("#ddlDRCR").val() == "C" ? $("#txtFAValue").val() : 0),
            SOL_VCH_NO: 0,
            SOL_VCH_DATE: ($("#ddlDRCR").val() == "C" ? $("#txtVoucherDate").val() : ""),            
            SectorID: $("#ddlLocation").val(),
            SECTORNAME: $("#ddlLocation option:selected").text(),
        });

        //alert(JSON.stringify(C));
        A.ShowFADetail(C);
        A.ClearFAText();
        A.CallModalConfirmFA();
    };
    this.ShowFADetail = function (C) {
        var html = ""
        if (C.length > 0) {

            for (var i in C) {

                html += "<tr id='trFA_" + C[i].FA_ID + "'>"
                    + "<td id='tdFA_" + C[i].FA_ID + "'><div><a href='" + pageUrl + '/DeleteFADetail' + "' onClick='return window.voucherInfo.masterInfo.removeFADetail(" + C[i].FA_ID + ");'><img src='images/delete.png' id='deleteFAID_" + C[i].FA_ID + "' /></a></div></td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + C[i].FA_DESC + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + C[i].SECTORNAME + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].VENDOR_NAME) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].DEP_RATE) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;text-align:right;'>" + (($("#ddlDRCR").val() == "D" ? C[i].PUR_VALUE : C[i].SOL_VALUE)) + "</td>";
                   

                html + "</tr>";
            }
        }


        $(".tblFA").find("tr:gt(1)").remove();
        $(".tblFA").append(html);
    };
    this.removeFADetail = function (FA_ID) {
        //alert("in fa delete");
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#trFA_" + FA_ID;
        $(removeElemTr).append(ajaxLoader);
        var B = {};
        B = window.voucherInfo.voucherForm;

        window.voucherInfo.findAndRemove(B.tmpVchFA, 'FA_ID', FA_ID);
        A.ShowFADetail(B.tmpVchFA);
        $("#txtFAD").focus();
        return false;
    };
    this.ClearFAText = function () {
        $("#txtFAD").val("");
        $("#ddlLocation").val("");
        $("#ddlSupplier").val("");
        $("#txtDRate").val("")
        $("#txtFAValue").val("")
        

    };

    this.CallModalConfirmFA = function () {
        $("#spanTxt").html("Do you want to add any more fixed asset?");
        $("#dialog-confirm").dialog({
            title: "Add More Fixed Asset?",
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                "Yes": function () {

                    $(this).dialog("close");
                    $("#txtFAD").focus();
                },
                "No": function () {

                    $(this).dialog("close");
                    A.CallculateFAAmount();

                }
            },
            open: function() {
            $(this).siblings('.ui-dialog-buttonpane').find('button:eq(1)').focus(); 
        }
        });
    };
    this.CallculateFAAmount = function () {
        var B = window.voucherInfo.voucherForm;
        var SectorID = B.VoucherMast.SectorID;
        var C = WBFC.Utils.fixArray(B.tmpVchFA);
        var FAAmount = 0;
        for (var i in C) {
            FAAmount = parseFloat(FAAmount) + parseFloat(C[i].PUR_VALUE) + parseFloat(C[i].SOL_VALUE);
        }
        $("#dialogFA").dialog('close');
        $("#txtCredit_0").val("");
        $("#txtDebit_0").val("");
        if ($("#ddlDRCR").val() == "D") {

            $("#txtCredit_0").prop("disabled", true);
            $("#txtDebit_0").prop("disabled", false).val(FAAmount);
            $("#txtDebit_0").focus();
        } else if ($("#ddlDRCR").val() == "C") {

            $("#txtDebit_0").prop("disabled", true);
            $("#txtCredit_0").prop("disabled", false).val(FAAmount);
            $("#txtCredit_0").focus();
        }
    };

    this.RefreshClicked = function () {
        location.href = "ACC_VCHMaster.aspx";
    };
    this.SetTabIndexInstType = function () {
        $(".hori").keypress(function (event) {
            if (event.keyCode == 13) {
                textboxes = $("input.hori");
                debugger;
                currentBoxNumber = textboxes.index(this);
                if (textboxes[currentBoxNumber + 1] != null) {
                    nextBox = textboxes[currentBoxNumber + 1]
                    nextBox.focus();
                    nextBox.select();
                    event.preventDefault();
                    return false
                }
            }
        });
    };
    this.SetTabIndexFA = function () {
        $(".horiFA").keypress(function (event) {
            if (event.keyCode == 13) {
                textboxes = $("input.horiFA,select.horiFA");
                debugger;
                currentBoxNumber = textboxes.index(this);
                if (textboxes[currentBoxNumber + 1] != null) {
                    nextBox = textboxes[currentBoxNumber + 1]
                    nextBox.focus();
                    nextBox.select();
                    event.preventDefault();
                    return false
                }
            }
        });
    };
    this.SlipNoKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
            //alert($(this).next('input').attr("id"));
            $(this).next('input').focus();
        }
        return false;
    };
    this.InstNoKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {

            $(this).next('input').focus();
        }
        return false;
    };
    this.InstDateKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {

            $(this).next('input').focus();
        }
        return false;
    };
    this.InstAmountKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (!isNumberKey(evt)) {
            alert("Please enter number key");
            return false;
        } else {
            return true;
        }
        
    };
    this.DBankBRKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {

            //$(this).next('input').focus();
        }
        return false;
    };
    this.YearMonKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            if ($("#txtYearMonth").val() != "") {
                $("#ddlVchType").focus();
                return false;
            } else {
                alert("Please enter year month");
                $("#txtYearMonth").focus();
                return false;
            }
            
        }
    };
    this.VchTypeKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            if ($("#ddlVchType").val() != "") {
                $("#txtVoucherDate").focus();
                return false;
            } else {
                alert("Please Select Voucher Type");
                $("#ddlVchType").focus();
                return false;
            }

        }
    };
    this.VchDateKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            if ($("#txtVoucherDate").val() != "") {
                $("#lblAccDesc_0").focus();
                return false;
            } else {
                alert("Please Enter Voucher Date");
                $("#txtVoucherDate").focus();
                return false;
            }

        }
    };
    this.DBankBRKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            A.InstrumentAdd();
            return false;

        }else{
            return true;
        }
        return false;
    };
    this.InstrumentAdd = function () {

        if ($("#ddlInstType").val() == "") {
            alert("Please Select Instrument Type");
            $("#ddlInstType").focus();
            return false;
        }

        if ($("#txtInstNo").val() == "" && ($("#lblGlType").val() == 'A' || $("#lblGlType").val() == 'B')) {
            alert("Please enter instrument number");
            $("#txtInstNo").focus();
            return false;
        }

        if ($("#txtDBankBR").val() == "" && ($("#lblGlType").val() == 'A' || $("#lblGlType").val() == 'B')) {
            alert("Please enter drawee bank or branch");
            $("#txtDBankBR").focus();
            return false;
        }

        var B = window.voucherInfo.voucherForm;
        var SectorID = B.VoucherMast.SectorID;
        var C = WBFC.Utils.fixArray(B.tmpVchInstType);

        var instsrno = C.length + 1;
        C.push({
            InstSrNo: instsrno,
            YearMonth: $("#txtYearMonth").val(),
            VoucherType: $("#ddlVchType").val(),
            VCHNo: 0,
            GLID: $("#lblGlID").val(),
            SLID: $("#lblSlID").val(),
            DRCR: $("#ddlDRCR").val(),
            SlipNo: $("#txtSlipNo").val(),
            InstType: $("#ddlInstType").val(),
            InstTypeName: $("#ddlInstType option:selected").text(),
            InstNo: $("#txtInstNo").val(),
            InstDT: $("#txtInstDate").val(),
            Amount: $("#txtInstAmount").val(),
            ActualAmount: $("#txtInstAmount").val(),
            Drawee: $("#txtDBankBR").val().toString().toUpperCase(),
            SectorID: SectorID
        });
        //alert(JSON.stringify(C));
        A.ShowInstDetail(C);
        A.ClearInstText();
        A.CallModalConfirm();
    }
    this.CallModalConfirm = function () {
        $("#spanTxt").html("Do you want to add any more instrument type?");
        $("#dialog-confirm").dialog({
            title: "Add More Instrument?",
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                "Yes": function () {
                    
                    $(this).dialog("close");
                    $("#ddlInstType").focus();
                },
                "No": function () {
                   
                    $(this).dialog("close");
                    A.CallculateInstAmount();

                }
            },
            open: function() {
            $(this).siblings('.ui-dialog-buttonpane').find('button:eq(1)').focus(); 
        }
        });
    };
    this.ClearInstText = function () {
        $("#txtSlipNo").val("");
        $("#ddlInstType").val("");        
        $("#txtInstNo").val("");
        $("#txtInstDate").val("")
        $("#txtInstAmount").val("")        
        $("#txtDBankBR").val("");

    };
    this.DebitKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            if ($("#txtDebit_0").val() != "" || $("#txtCredit_0").val() != "") {
                if ($("#ddlDRCR").val() == "D" && parseFloat($.trim($("#txtDebit_0").val())) == 0) {
                    alert("Amount Must be greater than 0");
                    return false;
                }

                if ($("#ddlDRCR").val() == "C" && parseFloat($.trim($("#txtCredit_0").val())) == 0) {
                    alert("Amount Must be greater than 0");
                    return false;
                }

                var B = window.voucherInfo.voucherForm;
                var SectorID =B.VoucherMast.SectorID;
                var C = WBFC.Utils.fixArray(B.VoucherDetail);
                var D = WBFC.Utils.fixArray(B.tmpVchInstType);
                var E = WBFC.Utils.fixArray(B.tmpVchFA);
                if (!A.CheckValidDRCR(C, $("#ddlDRCR").val())) {
                    alert("Multiple Debit Against multple credit not allowed and vice versa");
                    return false;
                }
                if ($("#lblGlType").val() == "A" || $("#lblGlType").val() == "B" || $("#lblGlType").val() == "C") {
                    if (D == "") {
                        alert("Instrument is required when Bank/Branch/ Cash is Selected");
                        A.ShowModalPopupInstType();
                        return false;
                    }
                    var AMOUNT = ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val());
                    var AMOUNTType = ($("#ddlDRCR").val() == "D" ? "Debit Amount" : "Credit Amount");
                   var instamt = parseFloat(0);
                    for (var k in D) {
                        instamt += parseFloat(D[k].Amount);
                    }
                    //alert(AMOUNT);
                    //alert(instamt);
                    if (parseFloat(AMOUNT) != parseFloat(instamt)) {
                        alert("Total Amount of Instrument and " + AMOUNTType + " mus be equal.");
                        A.ShowModalPopupInstType();
                        return;
                    }
                }
                if ($("#lblGlType").val() == "F") {
                    if (E == "") {
                        alert("Fixed Asset Entry is required when Account Code Type if Fixed Asset");
                        A.ShowModalPopupFA();
                        return false;
                    }
                }
                var found = false;
                vchslr = C.length + 1;
                if (C == "" ) {
                    C.push({
                        YearMonth: $("#txtYearMonth").val(),
                        VoucherType: $("#ddlVchType").val(),
                        VCHNo: 0,
                        OLDSLID: $("#lblAccOldSlCode").val(),
                        GLName: $("#lblAccDesc_0").val(),
                        VchSrl: vchslr,
                        GlType:$("#lblGlType").val(),
                        GLID: $("#lblGlID").val(),
                        SLID: $("#lblSlID").val(),
                        SubID: ($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val()),
                        OLDSUBID: ($("#lblOldSubCode").val() == "" ? '' : $("#lblOldSubCode").val()),
                        SUBDesc: ($("#lblSubAccDesc_0").val() == "" ? '' : $("#lblSubAccDesc_0").val()),
                        DRCR: $("#ddlDRCR").val(),
                        AMOUNT: ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val()),
                        SectorID: SectorID,
                        VchInstType: D,
                        VchFA:E
                    });
                } else {
                    for (var i in C) {
                        if (C[i].SLID == $("#lblSlID").val() && C[i].GLID == $("#lblGlID").val() && C[i].SubID == ($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val())) {
                            alert("Account Code Already Entered. Select Another Account Code.");
                            found = true;
                        }
                    }
                    if (!found) {
                        C.push({
                            YearMonth: $("#txtYearMonth").val(),
                            VoucherType: $("#ddlVchType").val(),
                            VCHNo: 0,
                            OLDSLID: $("#lblAccOldSlCode").val(),
                            GLName: $("#lblAccDesc_0").val(),
                            VchSrl: vchslr,
                            GlType:$("#lblGlType").val(),
                            GLID: $("#lblGlID").val(),
                            SLID: $("#lblSlID").val(),
                            SubID: ($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val()),
                            OLDSUBID: ($("#lblOldSubCode").val() == "" ? '' : $("#lblOldSubCode").val()),
                            SUBDesc: ($("#lblSubAccDesc_0").val() == "" ? '' : $("#lblSubAccDesc_0").val()),
                            DRCR: $("#ddlDRCR").val(),
                            AMOUNT: ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val()),
                            SectorID: SectorID,
                            VchInstType: D,
                            VchFA: E
                        });
                    }
                }
                D = [];
                E = [];
                B.tmpVchFA = [];
                B.tmpVchInstType = [];
                //alert(JSON.stringify(E));
                //alert(JSON.stringify(B.tmpVchFA));
                //alert(JSON.stringify(D));
                //alert(JSON.stringify(B.tmpVchInstType));
                //alert(JSON.stringify(C));
                A.ClearText();
                A.ShowVoucherDetail(C);
                return false;
            } else {
                alert("Pleas enter " + ($("#ddlDRCR").val()=="D"?"Debit":"Credit") + " amount.");
                return false;
            }
        }else{
            return true;
        }
        return false;
    }
    this.CheckValidDRCR = function (C, DRCR) {
        var checkstatus = true;
        var countDR = 0;
        var countCR = 0;
        //alert(JSON.stringify(C));
        //alert(DRCR);
        for (var i in C) {
            if (C[i].DRCR == "D") {
                countDR++;
            }
            if (C[i].DRCR == "C") {
                countCR++;
            }
        }

        //alert("DRP " + countDR + " CRP " + countCR);
        if (DRCR == "D") {
            countDR++;
        } else if (DRCR == "C") {
            countCR++;
        }

        //alert("DR " + countDR + " CR " + countCR);
        if ((countDR == 1 && countCR == 1) || (countDR == 0 && countCR >= 0) || (countDR >= 0 && countCR == 0)) {
            checkstatus = true;
        }
        if (countDR > 1 && countCR > 1) {
            checkstatus = false;
        }

        return checkstatus;
    };
    this.ClearText = function () {
        
        $("#lblAccOldSlCode").val("");
        $("#lblAccDesc_0").val("");        
        $("#lblGlID").val("");
        $("#lblSlID").val("");
        $("#lblSubID").val("");
        $("#lblOldSubCode").val("");
        $("#lblSubAccDesc_0").val("");
        $("#ddlDRCR").val("");
        $("#txtDebit_0").val("");
        $("#txtCredit_0").val("");
        $("#lblGlType").val("");
        $(".tblInst").find("tr:gt(1)").remove();
        
        $("#lblAccDesc_0").focus();
    }
    this.ShowVoucherDetail = function (C) {
        var html = ""
        if (C.length > 0) {
            
            for (var i in C) {

                html += "<tr id='tr_" + C[i].VchSrl + "'>"
                    + "<td id='td_" + C[i].VchSrl + "'><div><a href='" + pageUrl + '/DeleteVoucherDetail' + "' onClick='return window.voucherInfo.masterInfo.removeVoucherDetail(" + C[i].VchSrl + ");'><img src='images/delete.png' id='deleteVchSID_" + C[i].VchSrl + "' /></a></div></td>"
                    + "<td style='width: 20%; border-bottom: solid 1px lightyellow;'>" + C[i].OLDSLID + "<br/>" + C[i].OLDSUBID + "</td>"
                    + "<td style='width: 60%; border-bottom: solid 1px lightyellow;'>" + C[i].GLName + "<br/>" + C[i].SUBDesc + "</td>"
                    + "<td style='width: 10%; border-bottom: solid 1px lightyellow;'>" + (C[i].DRCR == "D" ? "DR" : "CR") + "</td>"
                    + "<td style='width: 10%; border-bottom: solid 1px lightyellow;text-align:right;'>" + (C[i].DRCR == "D" ? C[i].AMOUNT : "") + "</td>"
                    + "<td style='width: 10%; border-bottom: solid 1px lightyellow;text-align:right;'>" + (C[i].DRCR == "C" ? C[i].AMOUNT : "") + "</td>"

                html + "</tr>";
            }            
        }

        A.CalCulateDebitCredit(C);
        $(".test").find("tr:gt(1)").remove();
        $(".test").append(html);
    };
    this.ShowInstDetail = function (C) {
        var html = ""
        if (C.length > 0) {

            for (var i in C) {

                html += "<tr id='trInst_" + C[i].InstSrNo + "'>"
                    + "<td id='tdInst_" + C[i].InstSrNo + "'><div><a href='" + pageUrl + '/DeleteInstDetail' + "' onClick='return window.voucherInfo.masterInfo.removeInstDetail(" + C[i].InstSrNo + ");'><img src='images/delete.png' id='deleteInstID_" + C[i].InstSrNo + "' /></a></div></td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + C[i].InstTypeName  + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + C[i].SlipNo + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].InstNo) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].InstDT) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;text-align:right;'>" + (C[i].Amount) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].Drawee) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'></td>"

                html + "</tr>";
            }
        }

        
        $(".tblInst").find("tr:gt(1)").remove();
        $(".tblInst").append(html);
    };
    this.CalCulateDebitCredit = function (C) {
        var TotalDebit = 0;
        var TotalCredit = 0;
        for (var i in C) {
            if (C[i].DRCR == "D") {
                TotalDebit = parseFloat(TotalDebit) +  parseFloat(C[i].AMOUNT);
            } else if (C[i].DRCR == "C") {
                TotalCredit = parseFloat(TotalCredit) + parseFloat(C[i].AMOUNT);
            }

        }
        $("#txtTotalDebit").val(TotalDebit);
        $("#txtTotalCredit").val(TotalCredit);

        if (TotalCredit>0 && TotalDebit>0 && TotalCredit == TotalDebit) {
            A.ShowConfirmSaveDialog();
        } 

    };
    this.ShowConfirmSaveDialog = function () {
        $("#spanTxt").html("Total Debit is equal to Total Credit. Do you want to save the voucher?");
        $("#dialog-confirm").dialog({
            title:"Confirm Saving?",
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                "Yes": function () {

                    $(this).dialog("close");
                    //A.CallSaveVoucher();
                    A.ShowNarationDialog();
                },
                "No": function () {

                    $(this).dialog("close");
                    

                }
            },
            open: function () {
                $(this).siblings('.ui-dialog-buttonpane').find('button:eq(0)').focus();
            }

        });
    };
    this.removeVoucherDetail = function (VchSRL) {

        //alert("in delete");
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#tr_" + VchSRL;
        $(removeElemTr).append(ajaxLoader);
        var B = {};
        B = window.voucherInfo.voucherForm;

        window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', VchSRL);
        A.ShowVoucherDetail(B.VoucherDetail);
        $("#lblAccDesc_0").focus();
        return false;
    }
    this.removeInstDetail = function (InstSrno) {
        //alert("in inst delete");
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#trInst_" + InstSrno;
        $(removeElemTr).append(ajaxLoader);
        var B = {};
        B = window.voucherInfo.voucherForm;

        window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', InstSrno);
        A.ShowInstDetail(B.tmpVchInstType);
        $("#ddlInstType").focus();
        return false;
    };
    this.DRCRChanged = function () {
        if ($("#ddlDRCR").val() != "") {
            $("#txtDebit_0").removeAttr("disabled");
            $("#txtCredit_0").removeAttr("disabled");
            $("#txtCredit_0").val("");
            $("#txtDebit_0").val("");
            var ValGLType = $("#lblGlType").val();
            if (ValGLType == "A" || ValGLType == "B" || ValGLType == "C") {
                if ($("#ddlDRCR").val() == "D") {

                    $("#txtCredit_0").prop("disabled", true);
                    $("#txtDebit_0").prop("disabled", false);
                    //$("#txtDebit_0").focus();
                    A.ShowModalPopupInstType();

                } else if ($("#ddlDRCR").val() == "C") {
                    $("#txtDebit_0").prop("disabled", true);
                    $("#txtCredit_0").prop("disabled", false);
                    //$("#txtCredit_0").focus();
                    A.ShowModalPopupInstType();
                }
            } else if (ValGLType == "F") {
                if ($("#ddlDRCR").val() == "D") {

                    $("#txtCredit_0").prop("disabled", true);
                    $("#txtDebit_0").prop("disabled", false);
                    //$("#txtDebit_0").focus();
                    A.ShowModalPopupFA();

                } else if ($("#ddlDRCR").val() == "C") {
                    $("#txtDebit_0").prop("disabled", true);
                    $("#txtCredit_0").prop("disabled", false);
                    //$("#txtCredit_0").focus();
                    A.ShowModalPopupFA();
                }
            } else {
                if ($("#ddlDRCR").val() == "D") {

                    $("#txtCredit_0").prop("disabled", true);
                    $("#txtDebit_0").prop("disabled", false);
                    $("#txtDebit_0").focus();
                } else if ($("#ddlDRCR").val() == "C") {
                    $("#txtDebit_0").prop("disabled", true);
                    $("#txtCredit_0").prop("disabled", false);
                    $("#txtCredit_0").focus();
                }
            }
            
        } else {
            //$("#txtDebit_0").removeAttr("disabled");
            //$("#txtCredit_0").removeAttr("disabled");
            $("#txtCredit_0").val("");
            $("#txtDebit_0").val("");
        }

        


    };

    

    this.GLSLAccKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
            if (!A.validator.form()) {
                alert("Required Field not entered");
                A.validator.focusInvalid();
                return false;
            }

            if ($('#lblAccDesc_0').val() != '') {
                $('#txtGLSLDescSearch').val($('#lblAccDesc_0').val());
                A.BindGridGLSL($('#txtGLSLDescSearch').val());
                $('#txtGLSLDescSearch').focus();
                return false;
            }
            else {
                $('#txtGLSLDescSearch').val('');
                A.BindGridGLSL($('#txtGLSLDescSearch').val());
                $('#txtGLSLDescSearch').focus();
                return false;
            }
        }
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57))
            alert("Please Enter Key Press For Display Indent Number");
        //$('#lblAccDesc_0').focus();
        if ($('#lblAccDesc_0').val() != '') {
            $('#txtGLSLDescSearch').val($('#lblAccDesc_0').val());
        }
        else {
            $('#txtGLSLDescSearch').val('');
        }
        A.BindGridGLSL($('#txtGLSLDescSearch').val());
        return false;

    };
    this.SUBAccKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
            if ($('#lblSubAccDesc_0').val() != '') {
                $('#txtSUBDescSearch').val($('#lblSubAccDesc_0').val());
                A.BindGridSub($('#txtSUBDescSearch').val());
                $('#txtSUBDescSearch').focus();
                return false;
            }
            else {
                $('#txtSUBDescSearch').val('');
                A.BindGridSub($('#txtSUBDescSearch').val());
                $('#txtSUBDescSearch').focus();
                return false;
            }
        }
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57))
            alert("Please Enter Key Press For Display Sub Description");
        //$('#lblAccDesc_0').focus();
        if ($('#lblSubAccDesc_0').val() != '') {
            $('#txtSUBDescSearch').val($('#lblSubAccDesc_0').val());
        }
        else {
            $('#txtSUBDescSearch').val('');
        }
        A.BindGridSub($('#txtSUBDescSearch').val());
        return false;

    };
    this.BindGridGLSL = function (GlSlDesc) {
        var StrIndentStatus = '';
        var StrFromIndentDate = '';
        var StrIndentName = '';
        $("#spNoData").html('');

        $(".loading-overlay").show();
        var W = "{GlSlDesc:'" + GlSlDesc + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/GET_DisGlSl',
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                $("#grdGLSL").empty();
                //alert(JSON.stringify(data.d));
                if (data.d.length > 0) {
                    $("#grdGLSL").append("<tr><th>OLD SL ID</th><th>GL NAME</th><th style='display:none;'>GL TYPE</th><th style='display:none;'>GL ID</th><th style='display:none;'>SL ID</th></tr>");
                    A.ShowModalPopupGLSL();
                    for (var i = 0; i < data.d.length; i++) {
                        $("#grdGLSL").append("<tr><td>" +
                                data.d[i].OLD_SL_ID + "</td> <td>" +
                                data.d[i].GL_NAME + "</td> <td style='display:none;'>" +
                                data.d[i].GL_TYPE + "</td> <td style='display:none;'>" +
                                data.d[i].GL_ID + "</td> <td style='display:none;'>" +
                                data.d[i].SL_ID + "</td></tr>");
                    }
                    //===================== Start PO Number Search is not Blank Then PO Number Record Display in GridView ===========================//
                    //if ($("#txtGLSLDescSearch").val() != '') {
                    //    var rows;
                    //    var coldata;

                    //    $("#grdGLSL").find('tr:gt(0)').hide();
                    //    var data = $('#txtGLSLDescSearch').val();
                    //    var len = data.length;
                    //    if (len > 0) {
                    //        $("#grdGLSL").find('tbody tr').each(function () {
                    //            coldata = $(this).children().eq(1);
                    //            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                    //            if (temp === 0) {
                    //                $(this).show();
                    //            }

                    //        });
                    //    } else {
                    //        $("#grdGLSL").find('tr:gt(0)').show();

                    //    }
                    //}
                    //===================== End PO Number Search is not Blank Then PO Number Record Display in GridView ===========================//

                    $(".loading-overlay").hide();
                    $(function () {
                        $("[id*=grdGLSL] td").bind("click", function () {
                            var row = $(this).parent();
                            $("[id*=grdGLSL] tr").each(function (rowindex) {
                                if ($(this)[0] != row[0]) {
                                    $("td", this).removeClass("selected_row");
                                }
                            });

                            $("td", row).each(function () {
                                if (!$(this).hasClass("selected_row")) {
                                    $(this).addClass("selected_row");
                                } else {
                                    $(this).removeClass("selected_row");
                                }
                            });
                        });
                    });
                    // Paging Function Calling
                    //GirdViewPaging();
                    //===================== Start Selected Gridview PO Number Value Display in Text Box ===========================//bind
                    $(document).ready(function () {
                        $("[id*=grdGLSL] tbody tr").click("click", function () {
                            var ValGLSL = $(this).children("td:eq(1)").text();
                            $("#txtGLSLDescSearch").val(ValGLSL);
                            $("#txtGLSLDescSearch").focus();
                        });
                    });
                    //===================== End Selected Gridview PO Number Value Display in Text Box ===========================//

                }
                else {
                    //alert("No Records Data");
                    $("#spNoData").html("No Records Found");
                    $(".loading-overlay").hide();
                    $('#lblAccDesc_0').focus();
                }
            },
            error: function (result) {
                alert("Error Records Data");
                $(".loading-overlay").hide();

            }
        });
    };
    this.BindGridSub = function (SubDesc) {
        var StrIndentStatus = '';
        var StrFromIndentDate = '';
        var StrIndentName = '';
        $("#spSUBNoData").html('');

        $(".loading-overlay").show();
        var W = "{SubDesc:'" + SubDesc + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/GET_DisSub',
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                $("#grdSUB").empty();
                //alert(JSON.stringify(data.d));
                if (data.d.length > 0) {
                    $("#grdSUB").append("<tr><th>OLD SUB ID</th><th>SUB NAME</th><th style='display:none;'>SUB ID</th><th style='display:none;'>SUB TYPE</th></tr>");
                    A.ShowModalPopupSub();
                    for (var i = 0; i < data.d.length; i++) {
                        $("#grdSUB").append("<tr><td>" +
                                data.d[i].OLD_SUB_ID + "</td> <td>" +
                                data.d[i].SUB_NAME + "</td> <td style='display:none;'>" +
                                data.d[i].SUB_ID + "</td> <td  style='display:none;'>" +
                                data.d[i].SUB_TYPE + "</td> </tr>");
                    }
                    //===================== Start PO Number Search is not Blank Then PO Number Record Display in GridView ===========================//
                    //if ($("#txtSUBDescSearch").val() != '') {
                    //    var rows;
                    //    var coldata;

                    //    $("#grdSUB").find('tr:gt(0)').hide();
                    //    var data = $('#txtSUBDescSearch').val();
                    //    var len = data.length;
                    //    if (len > 0) {
                    //        $("#grdSUB").find('tbody tr').each(function () {
                    //            coldata = $(this).children().eq(1);
                    //            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                    //            if (temp === 0) {
                    //                $(this).show();
                    //            }

                    //        });
                    //    } else {
                    //        $("#grdSUB").find('tr:gt(0)').show();

                    //    }
                    //}
                    //===================== End PO Number Search is not Blank Then PO Number Record Display in GridView ===========================//

                    $(".loading-overlay").hide();
                    $(function () {
                        $("[id*=grdSUB] td").bind("click", function () {
                            var row = $(this).parent();
                            $("[id*=grdSUB] tr").each(function (rowindex) {
                                if ($(this)[0] != row[0]) {
                                    $("td", this).removeClass("selected_row");
                                }
                            });

                            $("td", row).each(function () {
                                if (!$(this).hasClass("selected_row")) {
                                    $(this).addClass("selected_row");
                                } else {
                                    $(this).removeClass("selected_row");
                                }
                            });
                        });
                    });
                    // Paging Function Calling
                    //GirdViewPaging();
                    //===================== Start Selected Gridview PO Number Value Display in Text Box ===========================//bind
                    $(document).ready(function () {
                        $("[id*=grdSUB] tbody tr").click("click", function () {
                            var ValGLSL = $(this).children("td:eq(1)").text();
                            $("#txtSUBDescSearch").val(ValGLSL);
                            $("#txtSUBDescSearch").focus();
                        });
                    });
                    //===================== End Selected Gridview PO Number Value Display in Text Box ===========================//

                }
                else {
                    //alert("No Records Data");
                    $("#spSUBNoData").html('No records found');
                    $(".loading-overlay").hide();
                    $('#lblSubAccDesc_0').focus();
                }
            },
            error: function (result) {
                alert("Error Records Data");
                $(".loading-overlay").hide();

            }
        });
    };
    this.InstTypeKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
            A.CallInstypeBlur();
        }
        return false;
    };
    this.CallInstypeBlur = function () {
        if ($("#ddlInstType").val() != "") {
            var InstType = $("#ddlInstType").val();
            var B = window.voucherInfo.voucherForm;
            var D = WBFC.Utils.fixArray(B.tmpVchInstType);

            if (InstType == "O") {
                var i = D.length + 1;
                var instSrl=leftpad(i.toString(),4,'0');
                alert(instSrl);
                var InstNo = ($("#txtYearMonth").val() + $("#lblAccOldSlCode").val() + instSrl);
                $("#txtInstNo").val(InstNo);
                $("#txtInstDate").val($("#txtVoucherDate").val());
                $("#txtInstAmount").focus();
                return false;
            } else {
                $("#txtInstNo").val("");
                $("#txtInstDate").val("");                
                $("#txtSlipNo").focus();
                return false;
            }
        } else {
            alert("Please instrument type");
            return false;
        }
    }
    this.ShowModalPopupInstType = function () {
        $("#dialogInstrumentType").dialog({
            title: "Instrument Type",
            width: 980,

            buttons: {
                Ok: function () {
                    //A.InstrumentAdd();
                    A.CallculateInstAmount();
                }
            },
            modal: true
        });
    };
    this.CallculateInstAmount = function () {
        var B = window.voucherInfo.voucherForm;
        var SectorID = B.VoucherMast.SectorID;
        var C = WBFC.Utils.fixArray(B.tmpVchInstType);
        var InstAmount = 0;
        for (var i in C) {
            InstAmount = parseFloat(InstAmount) + parseFloat(C[i].Amount);
        }
        $("#dialogInstrumentType").dialog('close');
        $("#txtCredit_0").val("");
        $("#txtDebit_0").val("");
        if ($("#ddlDRCR").val() == "D") {

            $("#txtCredit_0").prop("disabled", true);
            $("#txtDebit_0").prop("disabled", false).val(InstAmount);
            $("#txtDebit_0").focus();
        } else if ($("#ddlDRCR").val() == "C") {

            $("#txtDebit_0").prop("disabled", true);
            $("#txtCredit_0").prop("disabled", false).val(InstAmount);
            $("#txtCredit_0").focus();
        }
    };
    this.ShowModalPopupGLSL = function () {
        $("#dialogSearchGLSL").dialog({
            title: "GL Search",
            width: 580,

            buttons: {
                Ok: function () {
                    A.GetGridGLSlValue();
                }
            },
            modal: true
        });
    };
    this.GetGridGLSlValue = function () {
        var GridGLSL = document.getElementById("grdGLSL");
        var ValGlName = '';
        var ValOLDSlID = '';
        var ValGLType = '';
        var ValSLID = '';
        var ValGLID = '';
        if ($('#txtGLSLDescSearch').val().trim() == '') {
            alert("Please Select Old Sl ID");
            $('#txtGLSLDescSearch').val('');
            $("#txtGLSLDescSearch").focus();
            return false;
        }
        if ($("#txtGLSLDescSearch").val() != '') {
            for (var row = 1; row < GridGLSL.rows.length; row++) {
                var GridGLName_Cell = GridGLSL.rows[row].cells[1];
                var valueGlSlName = GridGLName_Cell.textContent.toString();
                if (valueGlSlName == $("#txtGLSLDescSearch").val()) {
                    ValOLDSlID = GridGLSL.rows[row].cells[0].textContent.toString();
                    ValGlName = valueGlSlName;
                    ValGLType = GridGLSL.rows[row].cells[2].textContent.toString();
                    ValGLID = GridGLSL.rows[row].cells[3].textContent.toString();
                    ValSLID = GridGLSL.rows[row].cells[4].textContent.toString();


                    break;
                }
            }
            if (ValGlName != $("#txtGLSLDescSearch").val()) {
                alert("Invalid OLD SL ID (OLD SL ID is not in List)");
                $("#txtGLSLDescSearch").focus();
                return false;
            }

            else {
                $("#lblAccDesc_0").val($("#txtGLSLDescSearch").val());
                $("#lblGlType").val(ValGLType);
                $("#lblSlID").val(ValSLID);
                $("#lblGlID").val(ValGLID);
                $("#lblAccOldSlCode").val(ValOLDSlID);
                //A.BindGridGLSL($("#lblAccDesc_0").val());
                //$("#lblAccDesc_0").focus();

                $("#dialogSearchGLSL").dialog('close');
                if (ValGLType != "L") {
                    //alert(ValGLType);
                    $("#ddlDRCR").focus();
                } else {
                    $("#lblSubAccDesc_0").focus();
                }

                return false;
            }
        }
    };
    this.ShowModalPopupSub = function () {
        $("#dialogSearchSub").dialog({
            title: "Sub Search",
            width: 580,

            buttons: {
                Ok: function () {
                    A.GetGridSubValue();
                }
            },
            modal: true
        });
    };
    this.GetGridSubValue = function () {
        var GridSub = document.getElementById("grdSUB");
        var ValSubName = '';
        var ValOLDSubID = '';
       
        var ValSUBID = '';
       
        if ($('#txtSUBDescSearch').val().trim() == '') {
            alert("Please Select Old Sub Desc");
            $('#txtSUBDescSearch').val('');
            $("#txtSUBDescSearch").focus();
            return false;
        }
        if ($("#txtSUBDescSearch").val() != '') {
            for (var row = 1; row < GridSub.rows.length; row++) {
                var GridSubName_Cell = GridSub.rows[row].cells[1];
                var valueSubName = GridSubName_Cell.textContent.toString();
                if (valueSubName == $("#txtSUBDescSearch").val()) {
                    ValOLDSubID = GridSub.rows[row].cells[0].textContent.toString();
                    ValSubName = valueSubName;                    
                    ValSUBID = GridSub.rows[row].cells[2].textContent.toString();


                    break;
                }
            }
            if (ValSubName != $("#txtSUBDescSearch").val()) {
                alert("Invalid OLD SUB ID (OLD SUB ID is not in List)");
                $("#txtSUBDescSearch").focus();
                return false;
            }

            else {
                $("#lblSubAccDesc_0").val($("#txtSUBDescSearch").val());                
                $("#lblSubID").val(ValSUBID);               
                $("#lblOldSubCode").val(ValOLDSubID);
                //A.BindGridGLSL($("#lblAccDesc_0").val());
                //$("#lblAccDesc_0").focus();

                $("#dialogSearchSub").dialog('close');
                $("#ddlDRCR").focus();

                return false;
            }
        }
    };
    this.TxtSubKeyUP = function (evt) {
        //$("#grdSUB").find('tr:gt(0)').hide();
        //var data = $('#txtSUBDescSearch').val();
        //var len = data.length;
        //if (len > 0) {
        //    $("#grdSUB").find('tbody tr').each(function () {
        //        coldata = $(this).children().eq(1);
        //        var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
        //        if (temp === 0) {
        //            $(this).show();
        //        }
        //    });
        //} else {
        //    $("#grdSUB").find('tr:gt(0)').show();
        //}

        A.BindGridSub($('#txtSUBDescSearch').val());

        //var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        //if ($('#txtGLSLDescSearch').val() != "" && iKeyCode == 13) {
        //    alert($('#grdGLSL tr:visible').length);
        //    A.GetGridGLSlValue();
        //}

    };
    
    this.TxtSubKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            A.GetGridSubValue();
        }
    }

    this.TxtGlSlKeyUP = function (evt) {
        //$("#grdGLSL").find('tr:gt(0)').hide();
        //var data = $('#txtGLSLDescSearch').val();
        //var len = data.length;
        //if (len > 0) {
        //    $("#grdGLSL").find('tbody tr').each(function () {
        //        coldata = $(this).children().eq(1);
        //        var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
        //        if (temp === 0) {
        //            $(this).show();
        //        }
        //    });
        //} else {
        //    $("#grdGLSL").find('tr:gt(0)').show();
        //}
        A.BindGridGLSL($('#txtGLSLDescSearch').val());
        

    };
    this.TxtGlSlKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            A.GetGridGLSlValue();
        }
    }
    this.getValidator = function () {
        $("#frmEcom").validate();
        $("#txtYearMonth").rules("add", { required: true,minlength:6,maxlength:6,number:true,numeric:true, messages: { required: "Please enter year month" } });
        $("#ddlVchType").rules("add", { required: true, messages: { required: "Please select voucher type" } });
        $("#txtVoucherDate").rules("add", { required: true, dpDate: true, messages: { required: "Please enter voucher date" } });
        //$("#txtYearMonth").valid();
        //$("#ddlVchType").valid();
        //$("#txtVoucherDate").valid();
        return $("#frmEcom").validate();
    };
    this.YearMonthChanged = function () {
        if ($("#txtYearMonth").val() == "") {
            return false;
        }
        $(".loading-overlay").show();
        var B = window.voucherInfo.voucherForm.VoucherMast;
        var E = "{YearMonth:'" + $.trim($("#txtYearMonth").val()) + "',SectorID:" + B.SectorID + "}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/GetVchDate',
            data: E,
            //async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ret = JSON.parse(response.d);
                if (parseInt(ret.Status) == 1) {
                    $("#txtVoucherDate").val(ret.VchDate);
                    $("#ddlVchType").focus();

                } else if (parseInt(ret.Status) == 0) {
                    $("#txtVoucherDate").val('');
                    alert(ret.Info);
                    $("#txtYearMonth").focus();
                }


                $(".loading-overlay").hide();
                //return false;
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    };
    this.SetMasterFormValues = function () {
        var B = window.voucherInfo.voucherForm;
        var C = B.VoucherMast;
        C.NARRATION = $.trim($("#txtNarration").val()).toString().toUpperCase();
        C.YearMonth = $.trim($("#txtYearMonth").val());
        C.VoucherType = $.trim($("#ddlVchType").val());
        C.VCHDATE = $.trim($("#txtVoucherDate").val());
        C.STATUS = 'D';
        C.VCHAMOUNT = $.trim($("#txtTotalDebit").val());
        //alert(JSON.stringify(B));
        A.CallSaveVoucher();
    };
    this.CallSaveVoucher = function () {
        $(".loading-overlay").show();
        var B = {};
        //alert('b empty');
        B = window.voucherInfo.voucherForm;
        //alert('b added');
        var E = "{\'VoucherForm\':\'" + JSON.stringify(B) + "\'}";
        //var C = JSON.stringify(E);
        //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/SaveVoucher',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.voucherInfo.voucherForm = JSON.parse(D.d);
                var B = window.voucherInfo.voucherForm.VoucherMast;
                
                $("#txtVoucherNo").val(B.VoucherNo);
                alert("Voucher Saved Successfully. Voucher No is '" + B.VoucherNo + "'.");
                location.href = "ACC_VCHMaster.aspx";
                //A.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);
                //A.ClearBookingDetails();
                //if (A.checkMasterDetail()) {
                //    $("#cmdSave").removeAttr("disabled");
                //} else {
                //    $("#cmdSave").attr("disabled", "disabled");
                //}
                $(".loading-overlay").hide();
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.ShowNarationDialog = function () {
        $("#dialogNaration").dialog({
            title: "Enter Narration",
            resizable: false,
            height: 240,
            
            modal: true,
            buttons: {
                Ok: function () {
                    if ($.trim($("#txtNarration").val()) != "") {
                        $("#cmdSave").removeAttr("disabled");
                        A.SetMasterFormValues();
                        $("#dialogNaration").dialog('close');
                    } else {
                        alert("Enter Narration Before Saving.");
                        return false;
                    }
                    
                }
            },
        });
    };

    this.SaveClicked = function () {
        if (A.validator.form()) {
            var B = window.voucherInfo.voucherForm;
            var SectorID = B.VoucherMast.SectorID;
            var C = WBFC.Utils.fixArray(B.VoucherDetail);
            var D = WBFC.Utils.fixArray(B.tmpVchInstType);
            var E = WBFC.Utils.fixArray(B.tmpVchFA);
            if (C == "") {
                alert("Please add voucher detail before saving.");
                $("#lblAccDesc_0").focus();
                return false;
            }
            var TotalDebit = 0;
            var TotalCredit = 0;
            for (var i in C) {
                if (C[i].DRCR == "D") {
                    TotalDebit = parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT);
                } else if (C[i].DRCR == "C") {
                    TotalCredit = parseFloat(TotalCredit) + parseFloat(C[i].AMOUNT);
                }

            }
            $("#txtTotalDebit").val(TotalDebit);
            $("#txtTotalCredit").val(TotalCredit);

            if (TotalCredit > 0 && TotalDebit > 0 && TotalCredit == TotalDebit) {
                A.ShowConfirmSaveDialog();
            } else {
                alert("Total Debit must be equal to Total Credit and Greater than zero(0). Cannot Save the Voucher");
                return false;

            }
        } else {
            alert("Required Field not entered");
            A.validator.focusInvalid();
            return false;
        }
        
    };

    this.SetYearMonth = function () {
        
        var B = window.voucherInfo.voucherForm.VoucherMast;
        var E = "{}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SetYearMon',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ret = JSON.parse(response.d);
                
                $("#txtYearMonth").val(ret.YEAR_MONTH);
                A.YearMonthChanged();          
                $(".loading-overlay").hide();
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    };
};

WBFC.VoucherInfo = function () {
    var A = this;
    this.voucherForm;
    this.init = function () {
        this.voucherForm = JSON.parse($("#voucherJSON").val());
       // alert(JSON.stringify(this.voucherForm));
        this.masterInfo = new WBFC.VoucherMaster();
        this.masterInfo.init();
    };

    this.findAndRemove = function (array, property, value) {
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;

            }
        });
    };

    this.findAndRemove3 = function (array, property, value, property2, value2, property3, value3) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2 && result[property3] == value3) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
};