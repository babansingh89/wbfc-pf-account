﻿var txtHdnSlCodeSearch = '';
$(document).ready(function () {
    $("#btnPrint").click(function (event) {

        if ($('#txtYearmonth').val().trim() == '') {
            alert("Please select Year Month");
            $("#txtYearmonth").css({ "background-color": "#ff9999" });
            $('#txtYearmonth').focus();
            return false;
        }
        else {
            $("#txtYearmonth").css({ "background-color": "" });
        }
        
        if ($("#ddlReportName").val() == 'Print Voucher' || $("#ddlReportName").val() == 'Print Exchang Advice') {
            if ($('#txtVchNo').val().trim() == '') {
                alert("Please enter voucher no from.");
                $("#txtVchNo").css({ "background-color": "#ff9999" });
                $('#txtVchNo').focus();
                return false;
            }
            else {
                $("#txtVchNo").css({ "background-color": "" });
            }

            if ($('#vchNoFromTo').val().trim() == '') {
                alert("Please enter voucher no to.");
                $("#vchNoFromTo").css({ "background-color": "#ff9999" });
                $('#vchNoFromTo').focus();
                return false;
            }
            else {
                $("#vchNoFromTo").css({ "background-color": "" });
            }

        }
        if ($("#ddlReportName").val() == 'Print Voucher' || $("#ddlReportName").val() == 'Print Exchang Advice') {
            if ($('#ddlVoucharType').val().trim() == '') {
                alert("Please select voucher type.");
                $("#ddlVoucharType").css({ "background-color": "#ff9999" });
                $('#ddlVoucharType').focus();
                return false;
            }
            else {
                $("#ddlVoucharType").css({ "background-color": "" });
            }
        }
        var d = new Date();
        var fromdate = '';
        var todate = '';
        fromdate = $('#txtVchNo').val();
        todate = $('#vchNoFromTo').val();
        //if (fromdate == '') { fromdate = d.getDate() + '-' + month_name[d.getMonth()] + '-' + d.getFullYear(); alert(fromdate); }
        //if (todate == '') { todate = d.getDate() + '-' + month_name[d.getMonth()] + '-' + d.getFullYear(); }
       
        ShowReport();
       
        //if ($('#txtYearmonth').val().trim() != '' && $('#txtVchNo').val().trim() != '' && fromdate != '' && todate != '') {
        //    ShowReport();
        //}
    });
});
     

function ShowReport() {
      
        $(".loading-overlay").show();
        var FormName = '';     
        var ReportName = '';
        var ReportType = '';
        if ($("#ddlReportName").val() == 'Print Voucher') {
            FormName = "VoucherReport.aspx";
            if ($("#rd10X6").is(":checked")) {
                ReportName = "VOUCHER 8x6";
            }
            else {
                ReportName = "VOUCHER";
            }
           
            ReportType = "Voucher Report";
        }
        
        if ($("#ddlReportName").val() == 'Print Exchang Advice') {
            FormName = "VoucherReport.aspx";
            ReportName = "eadvice";
            ReportType = "Exchange Advise";
        }
        if ($("#ddlReportName").val() == 'Date Wise Voucher Summary') {
            FormName = "VoucherReport.aspx";
            ReportName = "Voucher Type";
            ReportType = "Date Wise Voucher Summary";
        }
        if ($("#ddlReportName").val() == 'Date Wise Voucher Detail') {
            FormName = "VoucherReport.aspx";
            ReportName = "Voucher Type Detail";
            ReportType = "Date Wise Voucher Detail";
        }
        var d = new Date();
        var fromdate = '';
        var todate = '';
        fromdate = $('#txtVchNo').val();
        todate = $('#vchNoFromTo').val();
        var VchFromDate = '';
        var VchToDate = '';
        if ($('#txtVchFromDate1').val() != '') {
            var fromDate = $("#txtVchFromDate1").val().split("/");
            VchFromDate = fromDate[2] + "-" + fromDate[1] + "-" + fromDate[0];
        }
        if ($('#txtVchToDate1').val() != '') {
            var ToDate = $("#txtVchToDate1").val().split("/");
            VchToDate = ToDate[2] + "-" + ToDate[1] + "-" + ToDate[0];
        }
        //if (fromdate == '') { fromdate = d.getDate() + '-' + month_name[d.getMonth()] + '-' + d.getFullYear(); }
        //if (todate == '') { todate = d.getDate() + '-' + month_name[d.getMonth()] + '-' + d.getFullYear(); }
        var E = '';
        E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  YearMonth:'" + $('#txtYearmonth').val() + "', VchType:'" + $('#ddlVoucharType').val() + "',  VchNoFrom:'" + fromdate + "', " +
            "  VchNoTo:'" + todate + "',VchFromDate:'" + VchFromDate + "',VchToDate:'" + VchToDate + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SetReportValue',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",            
            success: function (response) {
                var show = response.d;

                if (show == "OK") {
                    window.open("ReportView.aspx?E=Y");
                    $(".loading-overlay").hide();
                }
                     
            }, 
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
}

function FetchData(evt) {
    //$('#lbltext').html('');
    var ChkVal='  Click For Details'
    var x = evt.parentElement.parentElement.parentElement;
    var y = x.querySelector("#summary");
    var z = y.querySelector("span");
    z.innerHTML = '';

    var rowTR = evt.closest('tr');
    var GL_TYPE = $(rowTR).find("td:eq(0)").text(); // get value from particular cell of selected row.
    var GL_ID = $(rowTR).find("td:eq(1)").text();
    var SL_ID = $(rowTR).find("td:eq(2)").text();
    var YEAR_MONTH = $(rowTR).find("td:eq(3)").text();
    var VCH_NO = $(rowTR).find("td:eq(4)").text();
    var SectorID = $(rowTR).find("td:eq(5)").text();

    //alert(GL_TYPE + GL_ID + SL_ID + YEAR_MONTH + VCH_NO + SectorID);
    $(".loading-overlay").show();
    var W = "{GL_TYPE:'" + GL_TYPE + "', GL_ID:'" + GL_ID + "', SL_ID:'" + SL_ID + "', YEAR_MONTH:'" + YEAR_MONTH + "', VCH_NO:'" + VCH_NO + "', SectorID:'" + SectorID + "'}";
    //alert(W);
    $.ajax({
        type: "POST",
        url: "VoucherReport.aspx/FetchDetailValue",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                //alert(data.d[0].DetailTest);
                if (GL_TYPE == 'A' || GL_TYPE == 'B') {
                    z.innerHTML = '<input type="checkbox" style="text-align: center;" id="checkDet" name="checkDet" onclick="ChkShow();" ><label for="checkDet">' + ChkVal + '</label>';
                    $("#tbl").empty();
                    $("#tbl").append("<tr><th style='width:80px'>Instrument Type</th><th style='width:80px'>Instrument No</th><th style='width:80px'>Instrument Date </th><th style='width:100px'>Amount </th><th style='width:200px'>Drawee Bank</th></tr>");
                   
                    for (var i = 0; i < data.d.length; i++) {
                        $("#tbl").append("<tr><td style='text-align:center'>" +
                            data.d[i].DetailTest.replace(/\n\r?/g, '<br />') + "</td> <td style='text-align:left'>" +
                            data.d[i].DetailTest1.replace(/\n\r?/g, '<br />') + "</td> <td style='text-align:center'>" +
                            data.d[i].DetailTest2.replace(/\n\r?/g, '<br />') + "</td> <td style='text-align:right'>" +
                            data.d[i].DetailTest3.replace(/\n\r?/g, '<br />') + "</td> <td style='text-align:left'>" +
                            data.d[i].DetailTest4.replace(/\n\r?/g, '<br />') + "</td></tr>");

                    }
                    $(".loading-overlay").hide();
                    BoundRecordsShowModalPopup();  
                }
                else {  
                    z.innerHTML = data.d[0].DetailTest; //replace('e', '\n');
                    z.innerHTML = z.innerHTML.replace(/\n\r?/g, '<br />');
                    $(".loading-overlay").hide();
                    //alert(z.innerHTML);
                    //$('#lbltext').html(data.d[0].DetailTest);
                }
            }
            else {
                //$('#lbltext').val('');
                z.innerHTML = '';
                $(".loading-overlay").hide();
            }

        },
        error: function (result) {
            $(".loading-overlay").hide();
            alert("Error Records Data");
        }
    });

}

function ChkShow() {
    BoundRecordsShowModalPopup();
    $("#checkDet").checked = false;
    

}

function BoundRecordsShowModalPopup() {
    $("#dialogSearchBound").dialog({
        title: "Records Display",
        width: 600,

        buttons: {
            Ok: function () {
                $("#dialogSearchBound").dialog('close');
                // alert("ok");

            }
        },
        modal: true
    });
}

$(document).ready(function () {

    $('#txtGLSL').keydown(function (evt) {                /// for Backspace
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            txtHdnSlCodeSearch = '';
            SetSlCodeSearch(0, txtHdnSlCodeSearch);

        }
    });
    $('#txtGLSL').keyup(function (evt) {                  /// fro Delete     
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            txtHdnSlCodeSearch = '';
            SetSlCodeSearch(0, txtHdnSlCodeSearch);
        }
        SearchGLSL($('#txtGLSL').val());
    });
    $("#txtGLSL").change(function () {                   /// for all select
        if ($(this).select()) {
            //$('#txtHdnSlCodeSearch').val('');
            //alert('changed');
        }
    });

    SearchGLSL($('#txtGLSL').val());
});

function SearchGLSL(GlSlDesc) {
    var GLType = '';
    if ($("#rdGLCODE").is(":checked")) {
        GLType = $("#rdGLCODE").val();
    } else {
        GLType = $("#rdGLNAME").val();
    }
       
    $(".autosuggestSL").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "VoucherReport.aspx/AccGLSlAutoCompleteData",
                data: "{GlSlDesc:'" + GlSlDesc + "',Type:'" + GLType + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            txtHdnSlCodeSearch = i.item.val;
            SetSlCodeSearch(1, txtHdnSlCodeSearch);
        }
    });
}
function SetSlCodeSearch(type, txtHdnSlCodeSearch) {
    var W = "{type:" + type + ",SlCodeSearch:'" + SlCodeSearch + "'}";
    $.ajax({
        type: "POST",
        url: "VoucherReport.aspx/GET_SetSLCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            return false;
        }
    });
}