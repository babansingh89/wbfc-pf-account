﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;

public partial class Acc_Sl : System.Web.UI.Page
{
    static DataTable dtAccSl;
    static int AccGlID;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            if (!IsPostBack)
            {
                PopulateGrid();
                PopulateGrid1();
                PopulateGridgrdAccGLDtl();
                HttpContext.Current.Session["AccSlDetail"] = null;
                dtAccSl = (DataTable)HttpContext.Current.Session["AccSlDetail"];
            }
             CheckBox chkUnitOnly = (CheckBox)dv.FindControl("chkUnitOnly");
            
               
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }



    // ------------------------------- Search SL Code Part --------------------------------------------------------------------
    [WebMethod]
    public static string[] AccSlAutoCompleteData(string OLD_SL_ID)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_AccSl", OLD_SL_ID, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["GL_NAME1"].ToString(), dtRow["OLD_SL_ID"].ToString()));
        }
        return result.ToArray();
    }
    

    // ------------------------------- End Of Search SL Code Part -------------------------------------------------------------


    [WebMethod]
    public static GL[] GET_GLNameByGLID(string StrGLCode)
    {
        //int UserID;
        List<GL> Detail = new List<GL>();
        //UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_GL_NameByID", StrGLCode);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GL DataObj = new GL();
            DataObj.GL_ID = dtRow["GL_ID"].ToString();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GL //Class for binding data
    {
        public string GL_ID { get; set; }
        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
    }

    [WebMethod]
    public static GLAll[] Search_GlCode()
    {
        List<GLAll> Detail = new List<GLAll>();
        DataTable dtGetData = DBHandler.GetResult("Get_GlCodeAll_M");
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GLAll DataObj = new GLAll();

            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GLAll //Class for binding data
    {

        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
    }

    [WebMethod]
    public static CheckGlCode[] Chk_GLCode(string StrGLCode)
    {
        //int UserID;
        List<CheckGlCode> Detail = new List<CheckGlCode>();
        //UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("CkhGlCode", StrGLCode);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            CheckGlCode DataObj = new CheckGlCode();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class CheckGlCode //Class for binding data
    {
        public string OLD_GL_ID { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_AccGlDetailsOnSession(string YearMonth, string OpenDebit, string OpenCredit, string TotalDebit, string TotalCredit)
    {
        string JSONVal = "";
        try
        {

            if (HttpContext.Current.Session["AccSlDetail"] != null)
            {
                DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccSlDetail"];
                if (dtdetail.Rows.Count > 0)
                {
                    DataRow dtrow = dtdetail.NewRow();
                    dtrow["YearMonth"] = YearMonth;
                    dtrow["OpenDebit"] = OpenDebit;
                    dtrow["OpenCredit"] = OpenCredit;
                    dtrow["TotalDebit"] = TotalDebit;
                    dtrow["TotalCredit"] = TotalCredit;
                    dtdetail.Rows.Add(dtrow);
                    HttpContext.Current.Session["AccSlDetail"] = dtdetail;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("YearMonth");
                dt.Columns.Add("OpenDebit");
                dt.Columns.Add("OpenCredit");
                dt.Columns.Add("TotalDebit");
                dt.Columns.Add("TotalCredit");

                DataRow dtrow = dt.NewRow();
                dtrow["YearMonth"] = YearMonth;
                dtrow["OpenDebit"] = OpenDebit;
                dtrow["OpenCredit"] = OpenCredit;
                dtrow["TotalDebit"] = TotalDebit;
                dtrow["TotalCredit"] = TotalCredit;

                dt.Rows.Add(dtrow);
                HttpContext.Current.Session["AccSlDetail"] = dt;
            }

            string result = "success";
            JSONVal = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_AccGlRecord(string SLID, string GlID, string OldGlID, string OldSlID, string GlName)
    {
        int UserID, SectorID;

        UserID   = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        string JSonVal = "";
        string ConString = "";
        string SL_ID = "";
   //     ConString = DataAccess.DBHandler.GetConnectionString();   
        try
        {
            if (SLID == "")
            {

                DataTable dtResult = DBHandler.GetResult("Insert_AccSl", GlID, OldGlID, OldSlID, GlName, SectorID, UserID);
                if (dtResult.Rows.Count > 0)
                {
                    SL_ID = dtResult.Rows[0]["SL_ID"].ToString();
                }

                DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccSlDetail"];
                if (dtdetail.Rows.Count > 0)
                {
                    foreach (DataRow objDR in dtdetail.Rows)
                    {
                        DBHandler.Execute("Insert_AccSlOpning",GlID, SL_ID, objDR["YearMonth"], Convert.ToDecimal(objDR["OpenDebit"]), Convert.ToDecimal(objDR["OpenCredit"]), Convert.ToDecimal(objDR["TotalDebit"]), Convert.ToDecimal(objDR["TotalCredit"]), SectorID, UserID);
                        //cmd = new SqlCommand("Exec Save_IssueDetail " + Convert.ToInt32(IntIssueId) + ",'" + IssueNo + "','" + IssueDate + "'," + Convert.ToInt16(SrNo) + "," + Convert.ToInt16(ReqID) + "," + Convert.ToInt32(objDR["FolioID"]) + ",'" + Convert.ToDecimal(objDR["IssuedQuantity"]) + "'," + Convert.ToInt16(UNIT_COMBO_CODE) + ", " + Convert.ToInt16(UserID) + "", db, transaction);
                        //cmd.ExecuteNonQuery();
                        //SrNo++;
                    }
                }
                HttpContext.Current.Session["AccSlDetail"] = null;
                dtAccSl = (DataTable)HttpContext.Current.Session["AccSlDetail"];
            }
            else
            {
                DBHandler.Execute("Update_AccSl", GlID, SLID, GlName, SectorID, UserID);

                //////////////////////DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                //////////////////////if (dtdetail.Rows.Count > 0)
                //////////////////////{
                //////////////////////    foreach (DataRow objDR in dtdetail.Rows)
                //////////////////////    {
                //////////////////////        DBHandler.Execute("Insert_AccGlOpning", GlID, objDR["YearMonth"], Convert.ToDecimal(objDR["OpenDebit"]), Convert.ToDecimal(objDR["OpenCredit"]), Convert.ToDecimal(objDR["TotalDebit"]), Convert.ToDecimal(objDR["TotalCredit"]), SectorID, UserID);
                //////////////////////        //cmd = new SqlCommand("Exec Save_IssueDetail " + Convert.ToInt32(IntIssueId) + ",'" + IssueNo + "','" + IssueDate + "'," + Convert.ToInt16(SrNo) + "," + Convert.ToInt16(ReqID) + "," + Convert.ToInt32(objDR["FolioID"]) + ",'" + Convert.ToDecimal(objDR["IssuedQuantity"]) + "'," + Convert.ToInt16(UNIT_COMBO_CODE) + ", " + Convert.ToInt16(UserID) + "", db, transaction);
                //////////////////////        //cmd.ExecuteNonQuery();
                //////////////////////        //SrNo++;
                //////////////////////    }
                //////////////////////}
                HttpContext.Current.Session["AccSlDetail"] = null;
                dtAccSl = (DataTable)HttpContext.Current.Session["AccSlDetail"];

            }
            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
     

        return JSonVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DeleteSessionAccGl(string ValYearMonth)
    {
        StringBuilder objSB = new StringBuilder();
        string JSONVal = string.Empty;
        string strYearMon = "";
        DataTable dtDetails = (DataTable)HttpContext.Current.Session["AccSlDetail"];
        if (HttpContext.Current.Session["AccSlDetail"] != null)
        {
            strYearMon = ValYearMonth;
            try
            {
                if (dtDetails.Rows.Count == 1)
                {
                    HttpContext.Current.Session["IssueDetails"] = null;
                    dtAccSl = (DataTable)HttpContext.Current.Session["AccSlDetail"];
                }
                else
                {
                    dtDetails = dtDetails.AsEnumerable().Where(x => x.Field<String>("FolioID") != strYearMon).CopyToDataTable();
                    HttpContext.Current.Session["AccSlDetail"] = dtDetails;
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session["AccSlDetail"] = null;
            }
        }
        return JSONVal;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static SLMaster[] SLMasterSearch(string old_sl_id)
    {

        List<SLMaster> Detail = new List<SLMaster>();
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtSlMaster = DBHandler.GetResult("Search_SLMaster", old_sl_id, SectorID);

        if (dtSlMaster.Rows.Count > 0)
        {

            for (int i = 0; i < dtSlMaster.Rows.Count; i++)
            {
                SLMaster DataObj = new SLMaster();
                DataObj.OLD_GL_ID = dtSlMaster.Rows[i]["OLD_GL_ID"].ToString();
                DataObj.GL_NAME = dtSlMaster.Rows[i]["GL_NAME"].ToString();
                DataObj.GL_ID = dtSlMaster.Rows[i]["GL_ID"].ToString();
                DataObj.OLD_SL_ID = dtSlMaster.Rows[i]["OLD_SL_ID"].ToString();
                DataObj.SL_NAME = dtSlMaster.Rows[i]["SL_NAME"].ToString();
                DataObj.SL_ID = dtSlMaster.Rows[i]["SL_ID"].ToString();

                Detail.Add(DataObj);
            }
        }
        return Detail.ToArray();
    }

    public class SLMaster
    {
        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
        public string GL_ID { get; set; }
        public string OLD_SL_ID { get; set; }
        public string SL_NAME { get; set; }
        public string SL_ID { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static SLDetail[] SLDetailSearch(string old_sl_id)
    {
        HttpContext.Current.Session["AccSlDetail"] = null;
        List<SLDetail> Detail = new List<SLDetail>();
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtSlDetail = DBHandler.GetResult("Search_SLDetail", old_sl_id, SectorID);

        if (dtSlDetail.Rows.Count > 0)
        {
            HttpContext.Current.Session["AccSlDetail"] = dtSlDetail;
            for (int i = 0; i < dtSlDetail.Rows.Count; i++)
            {
                SLDetail DataObj = new SLDetail();
                DataObj.YEAR_MONTH = dtSlDetail.Rows[i]["YEAR_MONTH"].ToString();
                DataObj.GL_ID = dtSlDetail.Rows[i]["GL_ID"].ToString();
                DataObj.OPBAL = dtSlDetail.Rows[i]["OPBAL"].ToString();
                DataObj.CLBAL = dtSlDetail.Rows[i]["CLBAL"].ToString();
                DataObj.DEBIT = dtSlDetail.Rows[i]["DEBIT"].ToString();
                DataObj.CREDIT = dtSlDetail.Rows[i]["CREDIT"].ToString();
                Detail.Add(DataObj);
            }
        }
        return Detail.ToArray();
    }

    public class SLDetail
    {
        public string YEAR_MONTH { get; set; }
        public string GL_ID { get; set; }
        public string OPBAL { get; set; }
        public string CLBAL { get; set; }
        public string DEBIT { get; set; }
        public string CREDIT { get; set; }
    }
   



    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {

            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            //////DataTable dt = DBHandler.GetResult("Get_LMN_LoanType");
            //////tbl.DataSource = dt;
            //////tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

 

    private void PopulateGrid1()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("OLD_GL_ID");
            dt.Columns.Add("GL_NAME");
            dt.Rows.Add();
            grdGL.DataSource = dt;
            grdGL.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGridgrdAccGLDtl()
    {
        try
        {
            GridView grdAccGLDtl = (GridView)dv.FindControl("grdAccGLDtl");
            DataTable dt = new DataTable();
            dt.Columns.Add("ItemID");
            dt.Columns.Add("YearMonth");
            dt.Columns.Add("OpeningDebit");
            dt.Columns.Add("OpeningCredit");
            dt.Columns.Add("TotalDebit");
            dt.Columns.Add("TotalCredit");
            dt.Columns.Add("ClosingDebit");
            dt.Columns.Add("ClosingCredit");
            dt.Rows.Add();
            grdAccGLDtl.DataSource = dt;
            grdAccGLDtl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //protected DataTable drpload()
    //{
    //    try
    //    {
    //        DataTable dt = DBHandler.GetResult("LoadCountry");
    //        return dt;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}
}