﻿var hdnBankID = '';
$(document).ready(function () {


        $("#txtStartDate,#txtEndDate")
            .datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                changeYear: true,
                changeMonth: true,
                maxDate: 'today',
                duration: 'slow'

            });

        $("#ddlReportType") 
            .change(function() {
                if ($("#ddlReportType :selected").val() == 1 || $("#ddlReportType :selected").val() == 2) {
                    $("#txtBank").attr('disabled', 'disabled');
                    $("#txtVoucharNo").attr('disabled', 'disabled');
                    $("#txtEndVoucharNO").attr('disabled', 'disabled');
                    $("#txtBank").val('');
                    $("#txtVoucharNo").val('');
                    $("#txtEndVoucharNO").val('');
                } else {
                    $("#txtBank").prop("disabled", false);
                    $("#txtVoucharNo").prop("disabled", false);
                    $("#txtEndVoucharNO").prop("disabled", false);
                    
                }
            });
        SearchBank();
        $("#txtEndDate,#txtStartDate").change(function () {
            Check_FromAndToDate();
            });

    $("#rbbank").change(function () {
        if (document.getElementById("rbbank").checked == true) {
            $("#LblBank").text("Bank Name");
            hdnBankID='';
            $('#txtBank').val('');
            $('#txtBank').focus();
        }
    });
    $("#rbCode").change(function () {
        if (document.getElementById("rbCode").checked == true) {
            $("#LblBank").text("Bank Code");
            hdnBankID = '';
            $('#txtBank').val('');
            $('#txtBank').focus();
        }
    });
    $("#ddlReportType").focus();
        //PrinReport();
        //ShowReport();
    });

//function PrinReport() {
//    $("#cmdPrint").click(function (e) {
//        e.preventDefault();
//        if(BeforeSave()==false){return}
//        var FormName = '';
//        var ReportName = '';
//        var ReportType = '';
       
//        FormName = "Report_BankSlip_Cheque_Issue_Receipt.aspx";
//        if ($("#ddlReportType :selected").val() == 0) { ReportName = "BankDepositslip"; }
//        if ($("#ddlReportType :selected").val() == 1) { ReportName = "ChequeIssue"; }
//        if ($("#ddlReportType :selected").val() == 2) { ReportName = "Chequereceipt"; }
        
//        if ($("#ddlReportType :selected").val() == 0) { ReportType = "Bank Slip"; }
//        if ($("#ddlReportType :selected").val() == 1) { ReportType = "Cheque Issue"; }
//        if ($("#ddlReportType :selected").val() == 2) { ReportType = "Cheque Receipt"; }
//        var W = "{FormName :'" + FormName + "',ReportName :'" + ReportName + "',ReportType :'" + ReportType + "',StartDate :'" + $("#txtStartDate").val() + "',EndDate :'" + $("#txtEndDate").val() + "'," +
//            "Bank :'" + $("#hdnBankID").val() + "',StratVoucharNo :'" + $("#txtVoucharNo").val() + "',EndVoucharNo :'" + $("#txtEndVoucharNO").val() + "'}";
//        $.ajax({
//            type: "POST",
//            url: "Report_BankSlip_Cheque_Issue_Receipt.aspx/Print_ReportTypeWise",
//            data: W,
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (msg) {
//                jsmsg = JSON.parse(msg.d);
//                $(".loading-overlay").hide();
//                var left = ($(window).width() / 2) - (900 / 2),
//                        top = ($(window).height() / 2) - (600 / 2),
//                        popup = window.open("ReportView.aspx", "popup", "width=900, height=600, top=" + top + ", left=" + left);
//                popup.focus();
//                window.location.href = "Report_BankSlip_Cheque_Issue_Receipt.aspx";
//            }
//        });


//    });
//}
function BeforeSave() {
    if ($("#ddlReportType :selected").val() == "") {
        alert('Please Select Report Type');
        $("#ddlReportType").focus();
        return false;
    }
    if ($("#ddlUserSector").val == "") {
        alert('Please Select Sector');
        $("#ddlUserSector").focus();
        return false;
    }
    if ($("#ddlReportType :selected").val() == 1 || $("#ddlReportType :selected").val() == 2) {
        if ($("#txtStartDate").val() == "") {
            alert('Please Select Start Date');
            $("#txtStartDate").focus();
            return false;

        }
        if ($("#txtEndDate").val() == "") {
            alert('Please Select End Date');
            $("#txtEndDate").focus();
            return false;

        }
    } else {
        if ($("#txtStartDate").val() == "") {
            alert('Please Select Start Date');
            $("#txtStartDate").focus();
            return false;
        }
        if ($("#txtEndDate").val() == "") {
            alert('Please Select End Date');
            $("#txtEndDate").focus();
            return false;
        }
        if ($("#txtBank").val() == "") {
            alert('Please Select Bank Name');
            $("#txtBank").focus();
            return false;
        }
        if ($("#txtVoucharNo").val() == "") {
            alert('Please Enter Strat Voucher No');
            $("#txtVoucharNo").focus();
            return false;
        }
        if ($("#txtEndVoucharNO").val() == "") {
            alert('Please Enter End Voucher No');
            $("#txtEndVoucharNO").focus();
            return false;
        }
    }
    ShowReport();
}

function SearchBank() {
    $(".autosuggestBank").autocomplete({
        source: function (request, response) {
            var bank = "";
            if ($('#txtBank').val() != '') {
                bank = $('#txtBank').val();
            }
            var con = 'code';
            if (document.getElementById("rbCode").checked == true) {
                con = 'Code';
            }
            if (document.getElementById("rbbank").checked == true) {
                con = 'Name';
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Report_BankSlip_Cheque_Issue_Receipt.aspx/BankAutoCompleteData",
                data: "{'Bank':'" + bank + "','con':'" + con + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            }
                        }))
                    }
                    else {
                        alert('No data found.');
                        $('#txtBank').val('');
                    }

                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            hdnBankID = i.item.val;
        }
    });
}

function Check_FromAndToDate() {
    var f = $('#txtStartDate').val();
    var t = $('#txtEndDate').val();
    if (f != "" && t != "") {
        var fdate = f.substring(0, 2);
        var fmonth = f.substring(3, 5);
        var fyear = f.substring(6, 10);
        var myfDate = new Date(fyear, fmonth - 1, fdate);

        var tdate = t.substring(0, 2);
        var tmonth = t.substring(3, 5);
        var tyear = t.substring(6, 10);
        var mytDate = new Date(tyear, tmonth - 1, tdate);

        if (mytDate < myfDate) {
            alert('Sorry ! To Date should  not be  less than From Date.');
            $('#txtEndDate').val('');
        }
        
    }
}
function ShowReport() {
    //$("#cmdPrint").click(function (e) {
        //e.preventDefault();
        //if (BeforeSave() == false) { return }
            $(".loading-overlay").show();
            var FormName = '';
            var ReportName = '';
            var ReportType = '';

            FormName = "Report_BankSlip_Cheque_Issue_Receipt.aspx";
            if ($("#ddlReportType :selected").val() == 0) {
                ReportName = "BankDepositslip";
            }
            if ($("#ddlReportType :selected").val() == 1) {
                ReportName = "ChequeIssue";
            }
            if ($("#ddlReportType :selected").val() == 2) {
                ReportName = "Chequereceipt";
            }

            if ($("#ddlReportType :selected").val() == 0) {
                ReportType = "Bank Slip";
            }
            if ($("#ddlReportType :selected").val() == 1) {
                ReportType = "Cheque Issue";
            }
            if ($("#ddlReportType :selected").val() == 2) {
                ReportType = "Cheque Receipt";
            }
            var W = "{FormName :'" + FormName + "',ReportName :'" +ReportName +"',ReportType :'" +ReportType +"',StartDate :'" + $("#txtStartDate").val() +"',EndDate :'" +$("#txtEndDate").val() +"'," +
                "Bank :'" + hdnBankID + "',StratVoucharNo :'" + $("#txtVoucharNo").val() + "',EndVoucharNo :'" + $("#txtEndVoucharNO").val() + "'}";
            $.ajax({
                type: "POST",
                url: pageUrl + '/Print_ReportTypeWise',
                data: W,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    var show = response.d;
                    if (show == "OK") {
                        window.open("ReportView.aspx?E=Y");
                    }
                    $(".loading-overlay").hide();
                    return false;
                    //window.location.href = "Report_BankSlip_Cheque_Issue_Receipt.aspx";
                },
                error: function(response) {
                    var responseText;
                    responseText = JSON.parse(response.responseText);
                    alert("Error : " + responseText.Message);
                    $(".loading-overlay").hide();
                },
                failure: function(response) {
                    alert(response.d);
                    $(".loading-overlay").hide();
                }
            });
    //});






}