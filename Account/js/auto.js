﻿
/// <reference path="jquery-1.8.1.js" />
/// <reference path="utility.js" />
/// <reference path="json2.js" />
/// <reference path="countries.min.js" />
/// <reference path="jquery-ui-1.8.23.custom.min.js" />
var countries = [];
var state = [];
$(document).ready(function () {
    var newcountries = WBFDC.Utils.fixArray(countries_and_states);

    for (var i in newcountries) {
        countries.push(newcountries[i].name);
        var newunits = WBFDC.Utils.fixArray(newcountries[i].units);
        if (newunits != "undefined" && newunits != null) {
            for (var k in newunits) {
                state.push(newunits[k]);
            }
        }
    }
    //alert(state);
    $("#txtState").autocomplete({
        minLength: 0,
        source: state,
        height: 200
    });
    $("#txtCountry").autocomplete({
        minLength: 0,
        source: countries,
        height: 200
    });
});
