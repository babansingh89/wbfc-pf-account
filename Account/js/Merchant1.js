﻿

$(document).ready(function () {

    window.merchantPage1 = new WBFDC.MerchantPage1();
    window.merchantPage1.init();
    $(".DefaultButton1").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });
});

WBFDC.RuleInfo = function () {
    var A = this;
    var validator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#btnRuleInfoAdd").click(A.AddClicked);
    };

    this.AddClicked = function () {
        A.getValidator();
        if (A.validator.form()) {
            A.setFormValues();
        }
    };

    this.setFormValues = function () {
        var P = window.merchantPage1.merchantForm1;
        var Q = WBFDC.Utils.fixArray(P.MerchantRule);
        var RuleDesc = $("#txtRuleDesc").val().toString();
        Q.push({
            RuleDescription: RuleDesc
        });
        //        alert(JSON.stringify(Q));
        //        alert(JSON.stringify(P));
        window.merchantPage1.newAddDetails();
        A.clearTxt();
    };

    this.getValidator = function () {
        $("#txtRuleDesc").removeClass("ignore");

        $("#frmEcom").validate();
        $("#txtRuleDesc").rules("add", { required: true, messages: { required: "Please enter Rule"} });

        return $("#frmEcom").validate();
    };
    this.clearTxt = function () {
        ($("#txtRuleDesc").val(" "));
    }
};

WBFDC.MerchantPage1 = function () {
    var A = this;
    this.merchantForm1;
    this.init = function () {
        this.merchantForm1 = JSON.parse($("#merchantJSON1").val());
        //alert(JSON.stringify(this.merchantForm1));
        this.ruleInfo = new WBFDC.RuleInfo();
        this.ruleInfo.init();
        var P = A.merchantForm1;
        var Q = P.MerchantRule;
        A.showMerchantDetail(Q);
    };

    this.newAddDetails = function (tot) {
        $(".loading-overlay").show();
        var P = {};
        P.merchantForm1 = window.merchantPage1.merchantForm1;

        var E = "{\'merchantForm1\':\'" + JSON.stringify(P.merchantForm1) + "\'}";
        //alert(E);
        //var Q = "{FromDays:" + $("#txtFromDays").val() + ",ToDays:" + $("#txtToDays").val() +
        //      ",Percentage:" + $("#txtPercentage").val() + "}"
        //var Q = "{RuleID:'" + $("#hdnID").val() + "}"
        $.ajax({
            type: "POST",
            url: pageUrl + '/AddDetails1',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                A.merchantForm1 = JSON.parse(D.d);
                //alert(JSON.stringify(A.merchantForm1));
                var P = A.merchantForm1;
                var Q = P.MerchantRule;
                A.showMerchantDetail(Q);
                $(".loading-overlay").hide();
            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };

    this.showMerchantDetail = function (ruleInfo) {
        var html = '';

        if (ruleInfo != "undefined") {
            if (ruleInfo.length > 0) {

                html += " <table align='center' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td colspan='7' class='labelCaption'><hr style='border:solid 1px lightblue' /></td></tr>"
                    + "</table>";
                html += "<table align='center' id='detailTable1' class='divTable1' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td class='th'></td>"
                    + "<td class='th'>Rule Description</td>"
                    + "</tr>";

                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";

                var enqDetails1 = WBFDC.Utils.fixArray(ruleInfo);
                for (var i in enqDetails1) {
                    html += "<tr id='RuleID_" + enqDetails1[i].RuleID + "'>"
                                    + "<td class='tr'><div><a href='" + pageUrl + '/DeleteMerchantDetail1' + "' onClick='return window.merchantPage1.removeMerchantDetail(" + enqDetails1[i].RuleID + ");'><img src='images/delete.png' id='deleteCancelInfoID_" + enqDetails1[i].RuleID + "' /></a></div></td>"
                                + "<td  class='tr' >" + enqDetails1[i].RuleDescription + "</td>";
                }
                html += "</table>";
            }
        }
        //alert(html);
        $("#divTable1").empty().html(html);
    };

    this.removeMerchantDetail = function (RuleID) {
        //alert('in remove ' + EnquiryTokenDetID);
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#RuleID_" + RuleID;
        $(removeElemTr).append(ajaxLoader);
        var ajaxUrl = pageUrl + '/DeleteMerchantDetail1';

        var P = {};
        //alert('b empty');
        P.merchantForm1 = A.merchantForm1;

        A.findAndRemove(P.merchantForm1.MerchantRule, 'RuleID', RuleID);
        //alert(JSON.stringify(P.enquiryForm));
        //alert('b added');
        var E = "{\'merchantForm1\':\'" + JSON.stringify(P.merchantForm1) + "\',\'RuleID\':\'" + RuleID + "\'}";
        //var Q = JSON.stringify(E);
        //alert(Q);
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                A.merchantForm1 = JSON.parse(D.d);
                //alert(JSON.stringify(A.merchantForm1));
                var P = A.merchantForm1;
                var Q = P.MerchantRule;
                A.showMerchantDetail(Q);
                //                window.enquiryPage.enquiryForm = JSON.parse(D.d);

                //                //alert(JSON.stringify(window.enquiryPage.enquiryForm));
                //                $("#divTable1 table[id='detailTable1']").remove();
                //                A.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);
                //                $("#divTable1 table[id='prodTab']").hide();
                //                $("#divTable1 table[id='prodTab']").show("slow");
            }
        });
        return false;
    };

    this.findAndRemove = function (array, property, value) {
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };

    this.sortJSON = function (data, key) {
        return data.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    };
};