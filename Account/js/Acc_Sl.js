﻿
//=======================================Strat Search for AccGL============================================

var txtHdnSlCodeSearch = '';
var hdnGLID = '';
var hdnSlID = '';
$(document).ready(function () {

    $('#txtSlCodeSearch').keydown(function (evt) {                /// for Backspace
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            //$('#txtLoaneeIDSearch').val('');
            //$('#txtHdnSlCodeSearch').val('');
            txtHdnSlCodeSearch = '';
        }
    });
    $('#txtSlCodeSearch').keyup(function (evt) {                  /// fro Delete     
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            //$('#txtHdnSlCodeSearch').val('');
            txtHdnSlCodeSearch = '';
        }
    });
    $("#txtSlCodeSearch").change(function () {                   /// for all select
        if ($(this).select()) {
            //$('#txtHdnSlCodeSearch').val('');
            //alert('changed');
        }
    });


    SearchSL();
});

function SearchSL() {
    $(".autosuggestSL").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Acc_Sl.aspx/AccSlAutoCompleteData",
                data: "{'OLD_SL_ID':'" + document.getElementById('txtSlCodeSearch').value + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            //$("#txtLoaneeIDSearch").val(i.item.val);
            //$("#txtHdnSlCodeSearch").val(i.item.val);
            txtHdnSlCodeSearch = i.item.val;
        }
    });
}
         

//=======================================End ==============================================================

$(document).ready(function () {
    $('#txtGLCode').keydown(function (evt)
    {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 9) {
           
            if ($('#txtGLCode').val() == '') {
                $('#txtGLCode').focus();
                BindGridGLCode();
                return false;
            }
            else {
                BindGlName($('#txtGLCode').val());
                return false;
            }
            
        }
    });
});

$(document).ready(function () {
    $('#txtGLCode').keypress(function (evt)
    {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        

        if (iKeyCode == 13)
        {
            if ($("#txtGLCode").val().trim() != '')
            {
                if ($("#txtGLName").val().trim() != '')
                {
                    //BindGridGLCode();
                    $('#txtSearchGL').val($('#txtGLCode').val());
                }
                $('#txtGLCode').focus();
                BindGlName($('#txtGLCode').val());
                
                
                return false;
            }
            else
            {
                //$('#hdnGLID').val('');
                hdnGLID = '';
                $('#txtGLCode').val('');
                $('#txtGLName').val('');
                BindGridGLCode();
                $('#txtSearchGL').focus();
                return false;
            }           

        }

    });
});
//========== End Unit Code Keypress 

$(document).ready(function () {
    $('#txtGLCode').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            $('#txtGLName').val('');
            //$('#hdnGLID').val('');
            hdnGLID = '';
        }
        if (iKeyCode == 46) {
            if ($('#txtGLCode').val().length < 3) {
                $('#txtGLName').val('');
                //$('#hdnGLID').val('');
                hdnGLID = '';
            }
        }
    });
});

// ====== [ GetUnit() Removed ]

//============= Start Display Group Name
function BindGlName(GLCode)
{
    var StrGLCode = '';
    StrGLCode = GLCode;
    $(".loading-overlay").show();
    var W = "{StrGLCode:'" + StrGLCode + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Sl.aspx/GET_GLNameByGLID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data)
        {
            if (data.d.length > 0)
            {
                var strGLID = data.d[0].GL_ID;
                var strOldGlID = data.d[0].OLD_GL_ID;
                var strGlName = data.d[0].GL_NAME;

                //$('#hdnGLID').val('');
                //$('#hdnGLID').val(strGLID);
                hdnGLID = strGLID;
                $('#txtGLCode').val('');
                $('#txtGLCode').val(strOldGlID);
                $('#txtGLName').val('');
                $('#txtGLName').val(strGlName);
                $('#txtSlCode').focus();
              $(".loading-overlay").hide();
            }
            else
            {
                $('#txtGLCode').focus();
                alert("No GL Records Data");
                //$('#hdnGLID').val('');
                strGLID = '';
                $('#txtGLCode').val('');
                $('#txtGLName').val('');
                $(".loading-overlay").hide();
                BindGridGLCode();
                
            }
        },
        error: function (data)
        {
            alert("Error Records Data...");
            $(".loading-overlay").hide();

        }
    });
}
//============= End Display Unit Name

//==============Start Unit Popup

function BindGridGLCode()
{
  //  $(".loading-overlay").show();
    var W = "{}";

    $.ajax({
        type: "POST",
        url: "Acc_Sl.aspx/Search_GlCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data)
        {
            $("#grdGL").empty();
            if (data.d.length > 0)
            {
                $("#grdGL").append("<tr><th>Gl Code</th><th>Gl Name</th></tr>");
                
                for (var i = 0; i < data.d.length; i++)
                {
                    $("#grdGL").append("<tr><td>" +
                    data.d[i].OLD_GL_ID + "</td> <td>" +
                    data.d[i].GL_NAME + "</td></tr>");
                }
                GlPopup();
                //===================== START Search is not Blank Record Display in GridView
                if ($('#txtSearchGL').val() != '')
                {
                    var rows;
                    var coldata;
                    $('#grdGL').find('tr:gt(0)').hide();
                    var data = $('#txtSearchGL').val();
                    var len = data.length;
                    if (len > 0) {
                        $('#grdGL').find('tbody tr').each(function ()
                        {
                            coldata = $(this).children().eq(0);
                            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                            if (temp === 0)
                            {
                                $(this).show();
                            }
                        });
                    }
                    else
                    {
                        $('#grdGL').find('tr:gt(0)').show();

                    }
                }

                $(document).ready(function ()
                {
                    $("[id*=grdGL] tbody tr").click("click", function ()
                    {
                        var strVal = $(this).children("td:eq(0)").text();
                        var valUnitName = $(this).children("td:eq(1)").text();
                        $("#txtSearchGL").val(strVal);
                        $("#txtGLName").val(valUnitName);
                        $("#txtSearchGL").focus();
                    });
                });
                //===================== END Selected Gridview in Text Box

            }
            else
            {
                alert("No Records Data");
           //     $(".loading-overlay").hide();
                $('#txtGrpCode').focus();
            }
        },
        error: function (result)
        {
            alert("Error Records Data");
       //     $(".loading-overlay").hide();

        }
    });
}
///======================= START UNIT keypress
$(document).ready(function ()
{
    $("#txtSearchGL").keypress(function (e)
    {
        if (e.keyCode == 13)
        {
            var GridGroup = document.getElementById("grdGL");
            var strVal = '';
            if ($('#txtSearchGL').val().trim() == '') {
                alert("Select a Gl");
                $('#txtSearchGL').val('');
                $("#txtSearchGL").focus();
                return false;
            }
            if ($('#txtSearchGL').val() != '') {
                for (var row = 1; row < GridGroup.rows.length; row++) {
                    var GridGroup_Cell = GridGroup.rows[row].cells[0];
                    var valueGrdCell = GridGroup_Cell.textContent.toString();
                    if (valueGrdCell == $('#txtSearchGL').val()) {
                        strVal = valueGrdCell;
                        break;
                    }
                }

                if (strVal != $('#txtSearchGL').val()) {
                    alert("Invalid GL ID (GL ID is not in List)");
                    $("#txtSearchGL").focus();
                    return false;
                }
                else {
                    $("#txtGLCode").val($("#txtSearchGL").val());
                    BindGlName($('#txtGLCode').val());
                    $("#txtGLCode").focus();
                    $("#dialogGL").dialog('close');
                    return false;
                }
            }
        }
    });
});

//==============End  Unit Popup

function GlPopup()
{
    $("#dialogGL").dialog({
        title: "GL Search",
        width: 550,

        buttons:
        {
            Ok: function ()
            {

                var GridGroup = document.getElementById("grdGL");
                var strVal = '';
                if ($('#txtSearchGL').val().trim() == '')
                {
                    alert("Select a Gl");
                    $('#txtSearchGL').val('');
                    $("#txtSearchGL").focus();
                    return false;
                }
                if ($('#txtSearchGL').val() != '')
                {
                    for (var row = 1; row < GridGroup.rows.length; row++)
                    {
                        var GridGroup_Cell = GridGroup.rows[row].cells[0];
                        var valueGrdCell = GridGroup_Cell.textContent.toString();
                        if (valueGrdCell == $('#txtSearchGL').val())
                        {
                            strVal = valueGrdCell;
                            break;
                        }
                    }

                    if (strVal != $('#txtSearchGL').val())
                    {
                        alert("Invalid GL ID (GL ID is not in List)");
                        $("#txtSearchGL").focus();
                        return false;
                    }
                    else
                    {
                        $("#txtGLCode").val($("#txtSearchGL").val());
                        BindGlName($('#txtGLCode').val());
                        $("#txtGLCode").focus();
                        $("#dialogGL").dialog('close');
                        return false;
                    }
                }
            }
        },
        modal: true
    });
}

$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtSearchGL').keyup(function ()
    {
        $('#grdGL').find('tr:gt(0)').hide();
        var data = $('#txtSearchGL').val();
        var len = data.length;
        if (len > 0) {
            $('#grdGL').find('tbody tr').each(function () {
                coldata = $(this).children().eq(0);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        } else {
            $('#grdGL').find('tr:gt(0)').show();
        }

    });
});


//$(document).ready(function () {

//    $('#txtGlCode').keyup(function (evt) {
//        //   $(".loading-overlay").show();
//        var StrGLCode = '';
//        StrGLCode = $('#txtGlCode').val().trim();
//        //  alert(StrGLCode);
//        var W = "{StrGLCode:'" + StrGLCode + "'}";
//        $.ajax({
//            type: "POST",
//            url: "Acc_Sl.aspx/Chk_GLCode",
//            contentType: "application/json;charset=utf-8",
//            data: W,
//            dataType: "json",
//            success: function (data) {
//                if (data.d.length > 0) {
//                    var strGl_Code = data.d[0].OLD_GL_ID;

//                    if (strGl_Code != '') {
//                        $('#txtGlCode').val('');
//                        alert("This Gl Code already Exist...");
//                        return false;
//                    }

//                }

//            },
//            error: function (result) {
//                alert("Error Records Data...");
//                //   $(".loading-overlay").hide();

//            }
//        });
//    });
    
//    $('#txtGlCode').change(function (evt) {     
//        //   $(".loading-overlay").show();
//        var StrGLCode = '';
//        StrGLCode = $('#txtGlCode').val().trim();
//      //  alert(StrGLCode);
//        var W = "{StrGLCode:'" + StrGLCode + "'}";
//        $.ajax({
//            type: "POST",
//            url: "Acc_Sl.aspx/Chk_GLCode",
//            contentType: "application/json;charset=utf-8",
//            data: W,
//            dataType: "json",
//            success: function (data) {
//                if (data.d.length > 0) {
//                    var strGl_Code = data.d[0].OLD_GL_ID;
                    
//                    if (strGl_Code != '') {
//                        $('#txtGlCode').val('');
//                        alert("This Gl Code already Exist...");
//                        return false;
//                    }
                    
//                }
                
//            },
//            error: function (result) {
//                alert("Error Records Data...");
//                //   $(".loading-overlay").hide();

//            }
//        });
//    });
//});

///====================== Set Test box Length ==================
$(document).ready(function () {
    $('#txtSlCode').attr({ maxLength: 10 });
    $('#txtSlNmae').attr({ maxLength: 50 });
    //$('#txtSchedule').attr({ maxLength: 10 });
    $('#txtYearMonth').attr({ maxLength: 6 });

    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });
    var d = new Date();
    var month = (d.getMonth()+1).toString();
    var year = (d.getFullYear()).toString();
    var yearmon = '';
    if (month.length > 1) {
        yearmon = year + month;
    }
    else {
        yearmon = year + '0' + month;
    }
   // alert(yearmon);
    //$('#txtYearMonth').val(yearmon);
    $('#txtYearMonth').val($('#hdnMaxYearMonth').val());

});

////`4$(document).ready(function ()
////{
////    $('#txtYearMonth').datepicker({
////        showOtherMonths: true,
////        selectOtherMonths: true,
////        closeText: 'X',
////        showAnim: 'drop',
////        showButtonPanel: true,
////        duration: 'slow',
////        dateformat: 'dd/mm/yyyy'
        

////    });
////});

$(document).ready(function ()
{
    var flag1 = 0;
    var flag2 = 0;
    var flag3 = 0;
    var flag4 = 0;
    $("#txtYearMonth").keypress(function (evt) {

        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
            alert("Please Enter a Numeric Value");
            return false;
        }

        if (evt.keyCode == 13) {
            $("#txtOpenDebit").focus();
            return false;
        }
    });

    $("#txtOpenDebit").keypress(function (evt)
    {  
        if (flag1 == 0)
        {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else
        {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) ) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag1 = 1;
        }

        
        
        if (evt.keyCode == 13)
        {
            if ($("#txtOpenDebit").val() != '') {
                //$("#txtOpenCredit").attr('disabled', 'disabled');
                $("#txtOpenCredit").val('0');
                $("#txtTotalDebit").val('0');
                $("#txtTotalCredit").val('0');
                $("#txtCloseDebit").val('0');
                $("#txtCloseCredit").val('0');
                $("#txtOpenCredit").focus();
            }
            else {
                $("#txtOpenCredit").focus();
            }
            

        //    $("#txtOpenCredit").focus();
            return false;
        }
    });

    $("#txtOpenDebit").keydown(function (evt) {
        if (flag1 == 0) {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag1 = 1;
        }



        if (evt.keyCode == 9) {
            if ($("#txtOpenDebit").val() != '') {
                //$("#txtOpenCredit").attr('disabled', 'disabled');
                $("#txtOpenCredit").val('0');
                $("#txtTotalDebit").val('0');
                $("#txtTotalCredit").val('0');
                $("#txtCloseDebit").val('0');
                $("#txtCloseCredit").val('0');
                $("#txtOpenCredit").focus();
            }
            else {
                $("#txtOpenCredit").focus();
            }


            //    $("#txtOpenCredit").focus();
            return false;
        }
    });

    $("#txtOpenCredit").keypress(function (evt)
    {
        if (flag2 == 0) {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag2 = 1;
        }

        if (evt.keyCode == 13) {
            $("#txtTotalDebit").focus();
            return false;
        }
    });

    $("#txtTotalDebit").keypress(function (evt)
    {
        if (flag3 == 0) {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag3 = 1;
        }
        if (evt.keyCode == 13) {
            $("#txtTotalCredit").focus();
            return false;
        }
    });

    $("#txtTotalCredit").keypress(function (evt)
    {
        if (flag4 == 0) {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag4 = 1;
        }
        if (evt.keyCode == 13) {
            $("#txtCloseDebit").focus();
            return false;
        }
    });

    $("#txtCloseDebit").keypress(function (e) {
        if (e.keyCode == 13) {
            $("#txtCloseCredit").focus();
            return false;
        }
    });
});

//$(document).ready(function ()
//{
//    var OpenBal;
//    var CloseBal;
//    var OpenDebit;
//    var ClaoseDebit;

//    $('#txtOpenCredit').keyup(function ()
//    {
//        if ($('#txtOpenDebit').val() != '' && $('#txtOpenCredit').val() != '') {
//            OpenBal = parseFloat($('#txtOpenDebit').val()) - parseFloat($('#txtOpenCredit').val());
//        }
//        else
//        {
//            OpenBal = 0;
//        }

//  //      calculation();
//        //alert(OpenBal);
//        return false;

//    });
//    $('#txtOpenDebit').keyup(function ()
//    {
//        if ($('#txtOpenDebit').val() != '' && $('#txtOpenCredit').val() != '')
//        {
//            OpenBal = parseFloat($('#txtOpenDebit').val()) - parseFloat($('#txtOpenCredit').val());
//        }
//        else {
//            OpenBal = 0;
//        }
//   //     calculation();
//       // alert(OpenBal);
//        return false;

//    });

//    $('#txtTotalDebit').keyup(function ()
//    {
//        calculation();

//        return false;

//    });

//    $('#txtTotalCredit').keyup(function () {
        
//    //    calculation();
//        return false;

//    });
//});

//function calculation() {
//    if ($('#txtOpenDebit').val() != '' && $('#txtOpenCredit').val() != '' && $('#txtTotalDebit').val() != '' && $('#txtTotalCredit').val() != '') {
//        CloseBal = (parseFloat($('#txtOpenDebit').val()) - parseFloat($('#txtOpenCredit').val())) + (parseFloat($('#txtTotalDebit').val()) - parseFloat($('#txtTotalCredit').val()))
//    }
//    else {
//        CloseBal = 0;
//    }

//    if (CloseBal < 0) {
//        $('#txtCloseCredit').val(CloseBal);
//        $('#txtCloseDebssit').val('0');
//    }
//    else {
//        $('#txtCloseDebit').val(CloseBal);
//        $('#txtCloseCredit').val('0');
//    }
//}


$(document).ready(function () {
    $("input").focus(function () {
        $(this).css("background-color", "#cccccc");
    });
    $("input").blur(function () {
        $(this).css("background-color", "#ffffff");
    });

    $("select").focus(function () {
        $(this).css("background-color", "#cccccc");
    });

    $("select").blur(function () {
        $(this).css("background-color", "#ffffff");
    });

});

$(document).ready(function () {
$("#txtSlCode").keypress(function (e) {
    //if ($('#txtGLCode').val() != '' && $('#txtGLName').val() != '') {
    if (e.keyCode == 13) {
        if ($('#txtGLCode').val().trim() == '' && $('#txtGLName').val().trim() == '') {
            $('#txtGLCode').focus();
            return true;
        }
        else
        {
        ChekDuplicateSLCode();
        return true;
        }
    }
});
});


$(document).ready(function () {
    $("#txtSlCode").keydown(function (e) {
        if (e.keyCode == 9) {
            if ($('#txtGLCode').val().trim() == '' && $('#txtGLName').val().trim() == '') {
                $('#txtGLCode').focus();
                return false;
            }
            else
            {
            ChekDuplicateSLCode();
            return true;
            }
        }
    });
});


function ChekDuplicateSLCode() {
       
    //alert($('#hdnGlID').val());
    var SLId = '';
    var GLId = '';

    if ($('#txtGLCode').val() == '')
    {
        $('#txtGLCode').focus();
        alert('Please Select GL Code');
        return false;

    }
    SLId = hdnSlID;//$('#hdnSlID').val();
    GLId = strGLID;//$('#hdnGLID').val();
    
    if (SLId == '') {
        SLId = '0';

    }
    //if ($('#txtSlCode').val().trim() == '') {
    //    alert('Please First Select Group Code');
    //    $('#txtGrpCode').focus();     
    //    $('#txtGlCode').val('');
    //    return false;
    //}
    var StrSLCode = '';
    StrSLCode = $('#txtSlCode').val().trim();
    $(".loading-overlay").show();
    var W = "{StrSLCode:'" + StrSLCode + "',StrSLID:'" + SLId + "',StrGLID:'" + GLId + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Sl.aspx/Chk_SLCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var strSl_Code = data.d[0].OLD_SL_ID;

                if (strSl_Code != '') {
                    $('#txtSlCode').val('');
                    alert("This Sl Code already Exist...");
                    $('#txtSlCode').focus();
                    $(".loading-overlay").hide();
                    return false;
                }

            }
            $(".loading-overlay").hide();

        },
        error: function (result) {
            alert("Error Records Data...");
               $(".loading-overlay").hide();

        }
    });
}
