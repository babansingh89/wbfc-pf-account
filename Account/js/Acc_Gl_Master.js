﻿

var InsertMode = 'Add';
var ModifyrowCount = '0';

$(document).ready(function () {

    var GridgrdAccGLDtl = document.getElementById("grdAccGLDtl");   //main Grid Initialization!
    GridgrdAccGLDtl.deleteRow(1);
    $('#grdAccGLDtl tr:last').after('<tr><td style="width:7%;">' + '' + '</td>' +
                           '<td style="width:7%;">' + '' + '</td>' +
                           '<td style="width:7%;">' + '' + '</td>' +
                           '<td style="width:12%;">' + '' + '</td>' +
                           '<td style="width:12%;">' + '' + '</td>' +
                           '<td style="width:12%;">' + '' + '</td>' +
                           '<td style="width:12%;">' + '' + '</td>' +
                           '<td style="width:12%;">' + '' + '</td>' +
                           '<td style="width:12%;">' + '' + '</td>' +
                           '<td style="width:12%;">' + '' + '</td>' + '</tr>');
    $("[id*=grdAccGLDtl] tr:has(td):last").hide();

    $('#txtOpenCredit').keypress(function (evt)
    {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {

            //=========================Start Form Validation For Blank File Code Here

            if ($('#txtYearMonth').val().trim() == '')
            {
                alert("Enter Year Month(YYYYMM)");
                $("#txtYearMonth").focus();
                return false;
            }


            if ($('#txtOpenDebit').val().trim() == '') {
                alert("Enter Opening Debit");
                $('#txtOpenDebit').focus();
                return false;
            }


            if ($('#txtOpenCredit').val().trim() == '') {
                alert("Enter Opening Credit");
                $('#txtOpenCredit').focus();
                return false;
            }

            if ($('#txtTotalDebit').val().trim() == '') {
                alert("Enter Total Debit");
                $('#txtTotalDebit').focus();
                return false;
            }

            if ($('#txtTotalCredit').val().trim() == '') {
                alert("Enter Total Credit");
                $('#txtTotalCredit').focus();
                return false;
            }

            if (InsertMode == "Add") {
                var GridIAccGl = document.getElementById("grdAccGLDtl");
                var rowCount = '';
                for (var row = 1; row < GridIAccGl.rows.length; row++) {
                  //  rowCount = row;
                    var GridIAccGl_Cell = GridIAccGl.rows[row].cells[3];
                    var YearMonth = GridIAccGl_Cell.textContent.toString();
                    if (YearMonth == $("#txtYearMonth").val()) {
                        alert("Year Month Already Exists");
                        $('#txtYearMonth').val('');
                        $('#txtOpenDebit').val('');
                        $('#txtOpenCredit').val('');
                        $('#txtTotalDebit').val('');
                        $('#txtTotalCredit').val('');
                        $('#txtCloseDebit').val('');
                        $('#txtCloseCredit').val('');

                        //if (document.getElementById("chkFolio").checked == true) {
                        //    BindGridFolioItem($('#txtRequisitionId').val());
                        //}
                        return false;
                    }
                }
            }

            //if (ModifyrowCount != '0')
            //{
            //    rowCount = rowCount + 1;
            //}


            //============ ADD TEXTBOX VALUES TO THE GRIDVIEW

            if (InsertMode == 'Add')
            {
                if ($('#txtTotalCredit').val() != '')
                {
                   
                    $('#grdAccGLDtl tr:last').before('<tr >' +
                        '<td Style="cursor:pointer;">' + "<div onClick='EditAccGlDetail_change(" + $('#txtYearMonth').val() + " Style='text-align:center;');><img src='images/Edit.png' Style='text-align:center;' id='EditAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' +
                        '<td Style="cursor:pointer">' + "<div onClick='CancelAccGlDetail_change(" + $('#txtYearMonth').val() + ");' Style='text-align:center;'><img src='images/close.png' Style='text-align:center;' id='CancelAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' +
                        '<td Style="cursor:pointer">' + "<div onClick='DeleteAccGlDetail_change(" + $('#txtYearMonth').val() + ");' Style='text-align:center;'><img src='images/delete.png' Style='text-align:center;' id='DeleteAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' +
                        '<td Style="text-align:center;">' + $('#txtYearMonth').val() + '</td>' +
                      '<td>' + $('#txtOpenDebit').val() + '</td>' +
                      '<td>' + $('#txtOpenCredit').val() + '</td>' +
                      '<td>' + $('#txtTotalDebit').val() + '</td>' +
                      '<td>' + $('#txtTotalCredit').val() + '</td>' +
                      '<td>' + $('#txtCloseDebit').val() + '</td>' +
                      '<td>' + $('#txtCloseCredit').val() + '</td>' + '</tr>');

                        //======== START Coding For Function to Add Item in SESSION

                        AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());

                        //======== END   Coding For Function to Add Item in SESSION
                    
                    ////////if (ModifyrowCount != '0') {

                    ////////    $('#GridViewAddIssueDetails tr:last').before('<tr>' +
                    ////////  '<td>' + "<div onClick='EditNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/Edit.png' id='EditNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + "<div onClick='CancelNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/close.png' id='CancelNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + "<div onClick='DeleteNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/delete.png' id='DeleteNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + rowCount + '</td>' +
                    ////////  '<td>' + $('#txtFolioNo').val() + '</td>' +
                    ////////  '<td>' + $('#txtItemDesc').val() + '</td>' +
                    ////////  '<td>' + $('#txtUOM').val() + '</td>' +
                    ////////  '<td>' + $('#txtReqQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtUsedQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtPendingQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtIssuedQty').val() + '</td>' + '</tr>');

                    ////////    //======== START Coding For Function to Add Item in SESSION
                    ////////    AddIssueItemDetailsOnSession($('#txtFolioID').val(), $('#txtIssuedQty').val());
                    ////////    //======== END   Coding For Function to Add Item in SESSION

                  /////////  }
                }
                else
                    alert('Invalid Entry!');
            }
            else
            {

                //====== In Edit Mode Value Add in Exixts Records code here ====================//
                var GridAccGL = document.getElementById("grdAccGLDtl");

                for (var i = 1; i < GridAccGL.rows.length; i++)
                {
                    var GridIAccGl_Cell1 = GridAccGL.rows[i].cells[3];
                    var YearMonths = GridIAccGl_Cell1.textContent.toString();

                    if (YearMonths == $("#txtYearMonth").val())
                    {
                        GridAccGL.rows[i].cells[3].textContent = $('#txtYearMonth').val();
                        GridAccGL.rows[i].cells[4].textContent = $('#txtOpenDebit').val();
                        GridAccGL.rows[i].cells[5].textContent = $('#txtOpenCredit').val();
                        GridAccGL.rows[i].cells[6].textContent = $('#txtTotalDebit').val();
                        GridAccGL.rows[i].cells[7].textContent = $('#txtTotalCredit').val();
                        GridAccGL.rows[i].cells[8].textContent = $('#txtCloseDebit').val();
                        GridAccGL.rows[i].cells[9].textContent = $('#txtCloseCredit').val();

                        AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());
                    }
                }
            }

            //================ START Selected Grid Row Color Change coding Here
            $(function () {
                $("[id*=GridViewAddIssueDetails] td").bind("click", function () {
                    if (InsertMode == "Add") {
                        var row = $(this).parent();

                        $("[id*=GridViewAddIssueDetails] tr").each(function (rowindex) {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });

                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    }// if close
                });

            });
            return false;
        } //======Keypree Enter=13 close

        //================ END Selected Grid Row Color Change coding Here

    });
    

});

function AddAccGlDetailsOnSession(YearMonth, OpenDebit, OpenCredit, TotalDebit, TotalCredit)
{
   // alert("resr");
    var W = "{YearMonth:'" + YearMonth + "', OpenDebit:'" + OpenDebit + "', OpenCredit:'" + OpenCredit + "', TotalDebit:'" + TotalDebit + "', TotalCredit:'" + TotalCredit + "'}";
  //  alert(W);
    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/GET_AccGlDetailsOnSession",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (JSONVal)
        {
            
            //=========Start function calling To Add Total Count
            
            //=========End   function calling To Add Total Count

            InsertMode = "Add";           // CLEAR ALL TEXTBOXES
            $('#txtYearMonth').val('');
            $('#txtOpenDebit').val('');
            $('#txtOpenCredit').val('');
            $('#txtTotalDebit').val('');
            $('#txtTotalCredit').val('');
            $('#txtCloseDebit').val('');
            $('#txtCloseCredit').val('');
                        
            var d = new Date();
            var month = (d.getMonth() + 1).toString();
            var year = (d.getFullYear()).toString();
            var yearmon = '';
            if (month.length > 1) {
                yearmon = year + month;
            }
            else {
                yearmon = year + '0' + month;
            }
            // alert(yearmon);
            //$('#txtYearMonth').val(yearmon);
            $('#txtYearMonth').val($('#hdnMaxYearMonth').val());
            $('#txtYearMonth').focus();
            document.getElementById("txtYearMonth").disabled = false;
        //    document.getElementById("txtRequisitionNo").disabled = true;
            ////ValItemID = '';
            ////ValReqQty = '';
            ////$('#TxtSearchFolioDetails').val('');
           
            ////if (document.getElementById("chkFolio").checked) {
            ////    BindGridFolioItem($('#txtRequisitionId').val());
            ////}
            //$('#txtDetailIndentNo').focus();
            return false;
        },
        error: function (JSONVal)
        {
        }
    });
}


function EditAccGlDetail_change(YearMonth) {
    if ($('#txtHdnGlCodeSearch').val() != '') { alert('Permission Not Allowed For Edit ? Only For New Entry Allow'); return false }
    if (InsertMode == "Modify") { return false; }
    InsertMode = 'Add';
    //$('#txtIndentItemID').val(ItemID);
    var GridItemDetail = document.getElementById("grdAccGLDtl");
    var tbody = GridItemDetail.getElementsByTagName("tbody")[0];

    tbody.onclick = function (e) {
        if (InsertMode == "Add") {
            e = e || window.event;
            var data = [];
            var target = e.srcElement || e.target;
            while (target && target.nodeName !== "TR") {
                target = target.parentNode;
            }
        }
        if (InsertMode == "Add") {
            deleteGLDetailSession(YearMonth);
            if (target) {
                var cells = target.getElementsByTagName("td");
                for (var i = 0; i < cells.length; i++) {
                    //data.push(cells[i].textContent);
                    if (i == 3) {
                        $('#txtYearMonth').val(cells[i].textContent);
                        document.getElementById("txtYearMonth").disabled = true;
                    }
                    if (i == 4) {
                        $('#txtOpenDebit').val(cells[i].textContent);
                    }
                    if (i == 5) {
                        $('#txtOpenCredit').val(cells[i].textContent);
                    }
                    if (i == 6) {
                        $('#txtTotalDebit').val(cells[i].textContent);
                    }
                    if (i == 7) {
                        $('#txtTotalCredit').val(cells[i].textContent);
                    }
                    if (i == 8) {
                        $('#txtCloseDebit').val(cells[i].textContent);
                    }
                    if (i == 9) {
                        $('#txtCloseCredit').val(cells[i].textContent);
                    }

                }
            }
            //alert(data);
            $('#txtOpenDebit').focus();
            InsertMode = "Modify";
            return false;
        }
        else if (InsertMode == "Modify") {
            alert("This Record is Already in Modify Mode");
            InsertMode = "Modify";
            $("#txtOpenDebit").focus();
            return false;
        }
    };
}


function deleteGLDetailSession(ValYearMonth) {
    //$(".loading-overlay").show();
    var W = "{YearMonth:" + ValYearMonth + "}";
    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/GET_DeleteSessionGLDetail",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            //$(".loading-overlay").hide();
        },
        error: function (result) {
            alert("Error Records Data");
            //$(".loading-overlay").hide();

        }
    });
}


//=============== Start GL Detail Cancel For Exists Item From DataBase Coding Here =========================//
function CancelAccGlDetail_change(YearMonth) {
    if (InsertMode == 'Modify') {
        //======== Start Coding For Function calling for Add Item in Session=========//
        AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());
        return false;
        //======== End Coding For Function calling for Add Item in Session=========//
    }
    else { return false; }
}

//=============== END GL Detail Cancel For Exists Item From DataBase Coding Here =========================//


////=============== Start GL Detail Delete For Add New Item in Coding Here =========================//
function DeleteAccGlDetail_change(YearMonth) {
    if ($('#txtHdnGlCodeSearch').val() != '') { alert('Permission Not Allowed For Edit ? Only For New Entry Allow'); return false }
    var StrYearMonth = '';
    if (InsertMode == "Modify") { return false; }
    InsertMode = 'Add';
    var GridItemDetail = document.getElementById("grdAccGLDtl");
    var tbody = GridItemDetail.getElementsByTagName("tbody")[0];
    var confirm_value = document.createElement("INPUT");
    confirm_value.type = "hidden";
    confirm_value.name = "confirm_value";
    if (confirm("Do you want to Delete GL Record?")) {
        confirm_value.value = "Yes";

    } else {
        confirm_value.value = "No";
        InsertMode = '';
    }

    if (confirm_value.value == "Yes") {
        tbody.onclick = function (e) {
            if (InsertMode == "Add") {
                e = e || window.event;
                var data = [];
                var target = e.srcElement || e.target;
                while (target && target.nodeName !== "TR") {
                    target = target.parentNode;
                }
            }

            if (InsertMode == "Add") {

                
                $(".loading-overlay").show();
                var GlID = '';
                GlID = $('#hdnGlID').val();
                if (GlID == '') { GlID ='0'}
                var W = "{GL_ID:'" + GlID + "'}";
                $.ajax({
                    type: "POST",
                    url: "Acc_Gl.aspx/GET_GlNoOnVoucher",
                    contentType: "application/json;charset=utf-8",
                    data: W,
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length > 0)
                        {
                            var objPartNo = data.d[0].GL_ID;
                            $(".loading-overlay").hide();
                            alert('Can Not Delete Record!This GL Code Already Used On Voucher');
                            return false;
                        }

                        else {

                            if (target) {
                                var cells = target.getElementsByTagName("td");
                                for (var i = 0; i < cells.length; i++) {
                                    if (i == 3) {
                                        StrYearMonth = cells[i].textContent;
                                    }
                                }
                            }

                            var GridRowId = '';
                            for (var row = 1; row < GridItemDetail.rows.length; row++) {
                                var GridYearMonth_Cell = GridItemDetail.rows[row].cells[3];
                                var valueYearMonth = GridYearMonth_Cell.textContent.toString();

                                if (valueYearMonth == StrYearMonth) {
                                    GridRowId = row;
                                    //return false;
                                    break;
                                }
                            }

                            GridItemDetail.deleteRow(GridRowId);
                            deleteGLDetailSession(YearMonth);
                            InsertMode = 'Add';
                            $(".loading-overlay").hide();
                            alert("Record Delete");
                            $("#txtYearMonth").focus();
                            return false;

                        }
                        
                        
                        
                    },
                    error: function (result) {
                        alert("Error Records Data");
                        return false;
                    }
                });

                
            }

            else if (InsertMode == "Modify") {
                alert("Sorry! Unable to Delete, as This record is in MODIFICATION!");
                $("#txtYearMonth").focus();
                return false;
            }

        };
    }
}

//=============== END GL Detail Delete For Add New Item in Coding Here =========================//




$(document).ready(function () {
    $("#cmdSave").click(function (event) {
        var Group_ID = '';
        var OldGlID = '';
        var OldSlID = '';
        var GlName = '';
        var GlMaster = '';
        var GlType = '';
        var Schedule = '';
        var chkSectorID = '';
        var GlID = '';
        var GridRowCount = '';



        if ($('#txtGrpCode').val().trim() == '') {
            alert("Select Group Code");
            $("#txtGrpCode").focus();
            return false;
        }

        if ($('#txtGrpName').val().trim() == '') {
            alert("Select Group Name");
            $("#txtGrpCode").focus();
            return false;
        }
        if ($('#txtGlCode').val().trim() == '') {
            alert("Select Gl Code");
            $("#txtGlCode").focus();
            return false;
        }
        if ($('#txtGlName').val().trim() == '') {
            alert("Select Gl Name");
            $("#txtGlName").focus();
            return false;
        }

        var GridAccGLDtl = document.getElementById("grdAccGLDtl");
        if ($('#hdnGlID').val() == '') {
            GridRowCount = 2;
        }
        else {
            GridRowCount = 1;
        }
        if (GridAccGLDtl.rows.length <= parseInt(GridRowCount)) {

            alert("Please Insert Year Month(YYYYMM) / Item in Grid");
            $("#txtYearMonth").focus();
            return false;
        }
        ////if ($('#txtYearMonth').val().trim() == '') {
        ////    alert("Enter Year Month(YYYYMM)");
        ////    $("#txtYearMonth").focus();
        ////    return false;
        ////}
        ////if ($('#txtOpenDebit').val().trim() == '') {
        ////    alert("Enter Opning Debit");
        ////    $("#txtOpenDebit").focus();
        ////    return false;
        ////}
        ////if ($('#txtOpenCredit').val().trim() == '') {
        ////    alert("Enter Opning Credit");
        ////    $("#txtOpenCredit").focus();
        ////    return false;
        ////}
        ////if ($('#txtTotalDebit').val().trim() == '') {
        ////    alert("Enter Total Debit");
        ////    $("#txtTotalDebit").focus();
        ////    return false;
        ////}
        ////if ($('#txtTotalCredit').val().trim() == '') {
        ////    alert("Enter Total Credit");
        ////    $("#txtTotalCredit").focus();
        ////    return false;
        ////}
   
        //=========================START Fetch TextField Values and CONDITIONS     
        if (InsertMode == 'Add')
        {
            GlID = $('#hdnGlID').val();

            if ($('#hdnGroupID').val().trim() == '')
            {
                Group_ID = '';
            }
            else
            {
                Group_ID = $('#hdnGroupID').val();
            }

            if ($('#txtGlCode').val().trim() == '')
            {
                OldGlID = '';
                OldSlID = '';
            }
            else {
                OldGlID = $('#txtGlCode').val();
                OldSlID = $('#txtGlCode').val();
            }

            if ($('#txtGlName').val().trim() == '')
            {
                GlName = '';
            }
            else {
                GlName = $('#txtGlName').val().trim();
            }

           

            if (document.getElementById('rdNormal').checked)
            {
                GlMaster = 'N';
            }
            if (document.getElementById('rdMaster').checked)
            {
                GlMaster = 'M';
            }

            if (document.getElementById('chkUnitOnly').checked) {
                chkSectorID = 'Y';
            }
            else {
                chkSectorID = 'N';
            }

            GlType = $('#ddlGlType').val();


            if ($('#txtSchedule').val().trim() == '')
            {
                Schedule = '';
            }        
            else
            {
                Schedule = $('#txtSchedule').val();
            }

            
           
            //=========================END   Fetch TExtField Values and CONDITIONS

            $(".loading-overlay").show();

            var W = "{GlID:'" + GlID + "', Group_ID:'" + Group_ID + "', OldGlID:'" + OldGlID + "', OldSlID:'" + OldSlID + "', GlName:'" + GlName + "', GlMaster:'" + GlMaster + "',GlType:'" + GlType + "',Schedule:'" + Schedule + "', chkSectorID:'" + chkSectorID + "'}";
            $.ajax({
                type: "POST",
                url: "Acc_Gl.aspx/Save_AccGlRecord",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data)
                {
                    if (GlID == '')
                    {
                        alert("Records Save Successfully");
                        $(".loading-overlay").hide();
                        ClearAllField();
                        return false;
                      //  var ValIssueNo = jQuery.parseJSON(data.d);

                        //var ValReqNo = strReqNo[0]["ReqNumber"];
                     //   $('#txtGenerateIssueNo').val(ValIssueNo);

                  //      $(".loading-overlay").hide();
                   //     PopupModelIssueNoShow();
                       
                   //     BindGetUnitNameDis();

                    }
                    else
                    {
                        ClearAllField();
                        $(".loading-overlay").hide();
                        alert("Records Updated Successfully");
                        ////CurrentDate();
                        //$("#txtRequisitionDate").focus();

                    }

                },
                error: function (response) {
                    $(".loading-overlay").hide();
                    alert("Records Not Saved!");
                    //$("#txtPODate").focus();
                    return false;
                }
            });
            return false;
        }
        else
        {
            alert("Record In Modification Mode!");
            return false;
        }

    });
});

$(document).ready(function () {
    $("#cmdView").click(function (event) {
        
     //   InsertMode == "Modify"
        if ($('#txtGlCodeSearch').val().trim() == '' && $('#txtHdnGlCodeSearch').val().trim() == '') {

            alert('Please select GL Code');
            return false;
        }
        ClearAllField();
        var GL_ID = $('#txtHdnGlCodeSearch').val().trim();
      //  var OLD_GL_ID = $('#txtGlCodeSearch').val().trim();
          $(".loading-overlay").show();
        
        var W = "{GL_ID:'" + GL_ID + "'}";
        
            $.ajax({
                type: "POST",
                url: "Acc_Gl.aspx/View_AccGL",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data){
                    if (GL_ID != '')
                    {
                        $('#txtGrpCode').val(data.d[0].group_code);
                        $('#txtGrpName').val(data.d[0].group_name);
                        $('#hdnGlID').val(data.d[0].GL_ID);
                        $('#txtGlCode').val(data.d[0].OLD_GL_ID);
                        $('#txtGlName').val(data.d[0].GL_NAME);
                        $('#ddlGlType').val(data.d[0].GL_TYPE);
                        $('#txtSchedule').val(data.d[0].SCHEDULE);
                        $('#hdnGroupID').val(data.d[0].GROUP_ID);
                        
                        if ((data.d[0].GL_MASTER) == 'N') {
                            $("#rdNormal").prop("checked", true);
                            $("#rdMaster").prop("checked", false);
                        }
                        else {
                            $("#rdMaster").prop("checked", true);
                            $("#rdNormal").prop("checked", false);
                        }

                        if (data.d[0].SectorID != '') {
                            $("#chkUnitOnly").prop("checked", true);
                            //  document.getElementById("chkUnitOnly").Checked = true;
                        }

                        document.getElementById("txtGrpCode").disabled = true;
                        document.getElementById("txtGlCode").disabled = true;
                        fetchAccGlDetail(GL_ID);

                        return false;

                    }
                    else
                    {
                        ClearAllField();
                        $(".loading-overlay").hide();
                        alert("Select a correct GL Code");
                        ////CurrentDate();
                        $("#txtGlCodeSearch").focus();

                    }

                },

                error: function (response) {
                    $(".loading-overlay").hide();
                    alert("Records Not Saved!");
                    //$("#txtPODate").focus();
                    return false;
                }
            });
            return false;
        
        

    });
});

$(document).ready(function () {
    $("#cmdDelete").click(function (event) {
        
        if ($('#txtGlCodeSearch').val().trim() == '')
        {
            alert('Please Select GL Code');
            $('#txtGlCodeSearch').focus();
            return false;
        }

        if ($('#txtGrpCode').val().trim() == '') {
            alert('Please Click View After Delete!');
            $('#cmdView').focus();
            return false;
        }

                  

        $(".loading-overlay").show();

        var GlID = '';
        GlID = $('#hdnGlID').val();
        if (GlID == '') { GlID = '0' }
        var W = "{GL_ID:'" + GlID + "'}";
        $.ajax({
            type: "POST",
            url: "Acc_Gl.aspx/GET_GlNoOnVoucher",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    var strmsg = data.d;
                    $(".loading-overlay").hide();
                    alert(strmsg);
                    return false;
                }

                else {
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    if (confirm("Do you want to Delete GL Record?")) {
                        confirm_value.value = "Yes";

                    } else {
                        confirm_value.value = "No";
                        InsertMode = 'Add';
                    }

                    if (confirm_value.value == "Yes") {
                        DeleteGLRecord(GlID)
                        //$(".loading-overlay").hide();
                    }
                    else { $('#txtGlName').val(); $(".loading-overlay").hide(); }
                    //GridItemDetail.deleteRow(GridRowId);
                    //deleteGLDetailSession(YearMonth);
                    //InsertMode = 'Add';
                    //$(".loading-overlay").hide();
                    //alert("Record Delete");
                    //$("#txtYearMonth").focus();
                    //return false;

                }



            },
            error: function (result) {
                $(".loading-overlay").hide();
                alert("Error Records Data");
                return false;
            }
        });

    });
});


function DeleteGLRecord(GLID) {
    $(".loading-overlay").show();
    var W = "{GL_ID:'" + GLID + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/GET_DelGLRecord",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            InsertMode = 'Add';
            $(".loading-overlay").hide();
            alert("Record Delete");
            ClearAllField();
            $("#txtGlCodeSearch").val('');
            $("#txtHdnGlCodeSearch").val('');
            $(".loading-overlay").hide();
            return false;
        },
        error: function (result) {
            $(".loading-overlay").hide();
            alert("Error Records Data");
            return false;
        }
    });

}





















function fetchAccGlDetail(GL_ID) {

    var W = "{GL_ID:'" + GL_ID + "'}";

    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/View_AccGLOpening",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.d.length; i++) {

                var OpenDebit = 0;
                var OpenCredit = 0;
                var CloseDebit = 0;
                var CloseCredit = 0;
                //alert(data.d[i].OPBAL);
                //alert(data.d[i].CLBAL);
                
                if (data.d[i].OPBAL >= 0) {

                    OpenDebit = data.d[i].OPBAL;
                    OpenCredit = 0;
                }
                else {
                    OpenDebit = 0;
                    OpenCredit = data.d[i].OPBAL;
                }

                if (data.d[i].CLBAL >= 0)
                {
                    CloseDebit = data.d[i].CLBAL;
                    CloseCredit = 0;
                }
                else
                {
                    CloseDebit = 0;
                    CloseCredit = data.d[i].CLBAL;;
                }
                //alert(OpenCredit);
                //alert(OpenDebit);

                $("#grdAccGLDtl").append("<tr><td Style='cursor:pointer' >" +
                    "<div onClick='EditAccGlDetail_change(" + data.d[i].YEAR_MONTH + ");' Style='text-align:center;'><img src='images/Edit.png' Style='text-align:center;' id='EditAccGlDetail_" + data.d[i].YEAR_MONTH + "'/>" + "</td> <td>" +
                    "<div onClick='CancelAccGlDetail_change(" + data.d[i].YEAR_MONTH + ");' Style='text-align:center;'><img src='images/close.png' Style='text-align:center;' id='CancelAccGlDetail_" + data.d[i].YEAR_MONTH + "'/>" + "</td> <td>" +
                    "<div onClick='DeleteAccGlDetail_change(" + data.d[i].YEAR_MONTH + ");' Style='text-align:center;'><img src='images/delete.png' Style='text-align:center;' id='DeleteAccGlDetail_" + data.d[i].YEAR_MONTH + "'/>" + "</td> <td Style='text-align:center;'>" +
                        data.d[i].YEAR_MONTH + "</td> <td align='right'>" +
                        parseFloat(OpenDebit).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(OpenCredit).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(data.d[i].DEBIT).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(data.d[i].CREDIT).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(CloseDebit).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(CloseCredit).toFixed(2) + "</td>" + "</tr>");

                AddAccGlDetailsOnSession(data.d[i].YEAR_MONTH, OpenDebit, OpenCredit, data.d[i].DEBIT, data.d[i].CREDIT);

                
            }
            $(".loading-overlay").hide();
        },
        
        error: function (response) {
            $(".loading-overlay").hide();
            alert("Records Not Saved!");
            return false;
        }
    });
}

function ClearAllField()
{
   
    InsertMode = 'Add';
   
    $('#txtGrpCode').val('');
    $('#txtGrpName').val('');
    $('#hdnGroupID').val('');
    $('#hdnGlID').val('');
    $('#txtGlCode').val('');
    $('#txtGlName').val('');
    $('#ddlGlType').val('');
    $('#txtSchedule').val('');
    document.getElementById("txtGrpCode").disabled = false;
    document.getElementById("txtGlCode").disabled = false;
    //$('#txtUserUnitCode').val('');
    //$('#txtUserUnitName').val('');

    document.getElementById("rdNormal").Checked = true;
    var GridAccGLDtl = document.getElementById("grdAccGLDtl");

    for (var row = GridAccGLDtl.rows.length - 1; row <= GridAccGLDtl.rows.length; row--) {

        if (row == 0) {
            $('#grdAccGLDtl tr:last').after('<tr><td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' + '</tr>');
            break;
        }
        else {
            GridAccGLDtl.deleteRow(row);
        }

    }
    $("[id*=grdAccGLDtl] tr:has(td):last").hide();
    return false;
}

function EditAccGlDetail(YearMonth)
{

    $('#txtYearMonth').val(YearMonth);
    var GridAccGLDtl = document.getElementById("grdAccGLDtl");
    var tbody = GridAccGLDtl.getElementsByTagName("tbody")[0];

    tbody.onclick = function (e)
    {
        if (InsertMode == "Add")
        {
            e = e || window.event;
            var data = [];
            var target = e.srcElement || e.target;
            while (target && target.nodeName !== "TR")
            {
                target = target.parentNode;
            }
        }
        if (InsertMode == "Add") {
            if (target)
            {
                var cells = target.getElementsByTagName("td");
                for (var i = 0; i < cells.length; i++)
                {
                    data.push(cells[i].textContent);

                    if (i == 3)
                    {
                        $('#txtYearMonth').val(cells[i].textContent);
                    }
                    if (i == 4)
                    {
                        $('#txtOpenDebit').val(cells[i].textContent);
                        ValSrNo = cells[i].textContent;
                    }
                    if (i == 5) {
                        $('#txtOpenCredit').val(cells[i].textContent);
                    }
                    if (i == 6) {
                        $('#txtTotalDebit').val(cells[i].textContent);
                    }
                    if (i == 7) {
                        $('#txtTotalCredit').val(cells[i].textContent);
                    }
                    if (i == 8) {
                        $('#txtCloseDebit').val(cells[i].textContent);
                    }
                    if (i == 9) {
                        $('#txtCloseCredit').val(cells[i].textContent);
                    }
                }
            }

            $('#txtYearMonth').focus();
            DeleteAccGlDetailSession(YearMonth);
            $('#txtYearMonth').focus();
            InsertMode = "Modify";
            return false;
        } //If close

        else
        {
            alert("This Record is Already in Modification Mode");
            InsertMode = "Modify";
            $("#txtYearMonth").focus();
            return false;
        }
    };
}

function DeleteAccGlDetailSession(ValYearMonth)
{
    $(".loading-overlay").show();
    var W = "{ValYearMonth:" + ValYearMonth + "}";
    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/GET_DeleteSessionAccGl",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            $(".loading-overlay").hide();
        },
        error: function (result) {
            alert("Error Records Data");
            $(".loading-overlay").hide();

        }
    });
}

function DeleteAccGlDetail(YearMon) {

    if (confirm("Are You sure you want to delete?"))
    {
        var StrFolioNo = '';
        var GridRowCount = '';
        //$("#GridViewAddIndentDetail").find("tr:not(:nth-child(1)):not(:nth-child(2))").remove();
        var GridAccGlDetail = document.getElementById("grdAccGLDtl");
        var tbody = GridAccGlDetail.getElementsByTagName("tbody")[0];

        tbody.onclick = function (e)
        {
            if (InsertMode == "Add")
            {
                e = e || window.event;

                var data = [];
                var target = e.srcElement || e.target;
                while (target && target.nodeName !== "TR")
                {
                    target = target.parentNode;
                }
            }
            if (InsertMode == "Add")
            {
                if (target)
                {
                    var cells = target.getElementsByTagName("td");
                    for (var i = 0; i < cells.length; i++)
                    {
                        if (i == 3) {
                            StrFolioNo = cells[i].textContent;
                            break;
                        }
                    }
                }

                for (var row = 1; row < GridAccGlDetail.rows.length; row++)
                {
                    var GridFolioNo_Cell = GridAccGlDetail.rows[row].cells[3];
                    var valueFolioNo = GridFolioNo_Cell.textContent.toString();
                    if (StrFolioNo == valueFolioNo)
                    {
                        GridAccGlDetail.deleteRow(row);
                        alert("Test");
                        DeleteAccGlDetailSession(YearMon);

                        alert("Record Deleted");
                        ////if (GridAccGlDetail.rows.length - 1 <= parseInt(GridRowCount))
                        ////{
                        ////    document.getElementById("txtRequisitionNo").disabled = false;

                        ////}
                        $("#txtYearMonth").focus();
                        return false;
                        break;
                    }
                }
            }
            else
            {
                alert("Sorry! Unable to Delete, as This record is in MODIFICATION!");
                $("#txtFolioNo").focus();
                return false;
            }
        };
    }
    else {     
        return false;
    }

}

function CancelAccGlDetail(YearMonth) {

    if (InsertMode == 'Modify') {
        var ValYearMonth = parseInt(YearMonth);
        //======== Start Coding For Function calling for Add Item in Session=========//
        ////AddIssueItemDetailsOnSession(ValFolioID, $('#txtIssuedQty').val());
        AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val()); AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());
        return false;
    }
    else { return false; }
}
