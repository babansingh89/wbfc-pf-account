﻿
var hdnRcvID = '';
var hdnLoaneeID = '';
var hdnBankID = '';
var hdnDraweeBank = '';
var vchNo = '';
var vchType = '';
var vchYearMonth = '';
$(document).ready(function () {
    $(".Default").click(function (event) {
        event.preventDefault();
    });

    SearchLoaneeCode();
    calculateGrandTotal();
    SearchBank();
    GetSearchValue();
});
function SearchLoaneeCode() {
    $(".autosuggestLoaneeCode").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "PaymentReceived.aspx/LoaneeAutoCompleteData",
                data: "{'LoaneeCode':'" + document.getElementById('txtLoaneeSearch').value + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            }
                        }))
                    }
                    else {
                        alert('No data found.');
                        $('#txtLoaneeSearch').val('');
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            hdnLoaneeID = i.item.val;
            SetLoaneeID(1, hdnLoaneeID);
            LoaneeSattlTypeHeealthCode(i.item.val);
        }
    });
}

function SetLoaneeID(type, hdnLoaneeID) {
    var W = "{type:" + type + ",StrLoaneeID:'" + hdnLoaneeID + "'}";
    $.ajax({
        type: "POST",
        url: "PaymentReceived.aspx/GET_SetLoaneeID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            return false;
        }
    });
}

function LoaneeSattlTypeHeealthCode(LoaneeUNQID) {

    var getdate = $('#txtDatePay').val();
        var year = getdate.substring(6, 10);
        var month = parseInt(getdate.substring(3, 5)) - 1;  // pervious month

        if (month.toString().length === 1) {
            month = '0' + month.toString();
        }
       var yearmonth = year + month;

    //alert(yearmonth);

    var LoaneeName = '';
    var W = "{LoaneeUNQID:'" + LoaneeUNQID + "', YearMonth:'" + yearmonth + "'}";
    $.ajax({
        type: "POST",
        url: "PaymentReceived.aspx/GET_LoaneeSattlTypeHeealthCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var LoaneeName = data.d[0].Loanee_Name;
                $('#ddlRepaymentTerm').val('');
                $('#txtHealthCode').val('');
                $('#ddlRepaymentTerm').val(data.d[0].Loanee_SettlementType);
                $('#txtHealthCode').val(data.d[0].Loanee_HealthCode);;
            }
        },
        error: function (result) {
            alert("Error Records Data...");
        }
    });
}

function SearchBank() {
    $(".autosuggestBank").autocomplete({
        source: function (request, response) {
            var bank = "";
            if ($('#txtBank').val() !== '') {
                bank = $('#txtBank').val();
            }
            //if ($('#txtDraweeBank').val() != '') {
            //    bank = $('#txtDraweeBank').val();
            //}
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "PaymentReceived.aspx/BankAutoCompleteData",
                data: "{'Bank':'" + bank + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            };
                        }));
                    }
                    else {
                        alert('No data found.');
                        $('#txtBank').val('');
                    }
                    
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            //$("#txtLoaneeIDSearch").val(i.item.val);
            hdnBankID = i.item.val;
            SetBankID(1, hdnBankID);
        }
    });
}

function SetBankID(type, hdnBankID) {
    var W = "{type:" + type + ",StrBankID:'" + hdnBankID + "'}";
    $.ajax({
        type: "POST",
        url: "PaymentReceived.aspx/GET_SetBankID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            return false;
        }
    });
}


function GetSearchValue() {
    $(".autosuggestSearch").autocomplete({
        source: function (request, response) {
            var LoaneeID = "";
            var Yearmonth = "";
            if ($('#txtYearmonth').val() !== '') {
                Yearmonth = $('#txtYearmonth').val();
            }
            else {
                alert('Please enter a correct Year Month(YYYYMM)');
                $('#txtYearmonth').val('');
                $('#txtYearmonth').focus();
                $('#txtText').val('');
                hdnRcvID = '';
                SetRcvID(0, hdnRcvID);
                return false;
            }
            if ($('#txtText').val() !== '') {
                LoaneeID = $('#txtText').val();
            }
            
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "PaymentReceived.aspx/SearchAutoCompleteData",
                data: "{'LoaneeID':'" + LoaneeID + "','Yearmonth':'" + Yearmonth + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    if (data.d.length > 0) {

                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            };
                        }));
                    }
                    else {
                        alert('No data found.');
                        $('#txtText').val('');
                        hdnRcvID = '';
                        SetRcvID(0, hdnRcvID);
                    }
                    
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            hdnRcvID = i.item.val;
            SetRcvID(1, hdnRcvID);
        }
    });
}

function SetRcvID(type, hdnRcvID) {
    var W = "{type:" + type + ",StrhdnRcvID:'" + hdnRcvID + "'}";
    $.ajax({
        type: "POST",
        url: "PaymentReceived.aspx/GET_SetRcvID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            return false;
        }
    });
}

function calculateGrandTotal() {

    //var rowTR = evt.closest('tr');
    //var OrgName = $(rowTR).find("td:eq(0)").text().trim();

    var Amount = 0.00;
    var amt = $('#txtAmount').val();
    $("#tbl input[name*='txtActAmount']").each(function (index) {
        //Check if number is not empty 
        
        
        if ($.trim($(this).val()) !== "")
            //Check if number is a valid integer
            if (!isNaN($(this).val())) {               
                    Amount = parseFloat(Amount) + parseFloat($(this).val());
            }       
    });
    $("#tbl span[id*='lblTotalAmount']").text(Amount);    
};


$(document).ready(function () {

    $('#txtLoaneeSearch').keydown(function (evt) {                /// for Backspace
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 8) {
            hdnLoaneeID = '';
            SetLoaneeID(0, hdnLoaneeID);
        }
    });
    $('#txtLoaneeSearch').keyup(function (evt) {                  /// fro Delete     
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            hdnLoaneeID = '';
            SetLoaneeID(0, hdnLoaneeID);
        }
    });
    $("#txtLoaneeSearch").change(function () {                   /// for all select
        if ($(this).select()) {
            hdnLoaneeID = '';
            SetLoaneeID(0, hdnLoaneeID);
        }
    });



    $('#txtBank').keydown(function (evt) {               /// for Backspace
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 8) {
            hdnBankID = '';
            SetBankID(0, hdnBankID);
        }
    });
    $('#txtBank').keyup(function (evt) {                  /// for Delete     
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            hdnBankID = '';
            SetBankID(0, hdnBankID);
        }
    });

    $("#txtBank").change(function () {
        if ($(this).select()) {
            hdnBankID = '';
            SetBankID(0, hdnBankID);
        }
    });


    $('#txtAmount, #txtActAmount').keypress(function (event) {
        if ((event.which !== 46 || $(this).val().indexOf('.') !== -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $("#txtNo, #txtYearmonth").keypress(function (event) {
        if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            event.preventDefault();
        }
    });


    $('#txtYearmonth').attr({ maxLength: 6 });
    $('#txtRemarks').attr({ maxLength: 50 });
});


$(document).ready(function () {
    $('#txtLoaneeSearch').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode === 13) {
            $('#txtBank').focus();
            return false;
        }

    });

    //$('#ddlRepaymentTerm').keypress(function (evt) {
    //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //    if (iKeyCode == 13) {
    //        $('#txtBank').focus();
    //        return false;
    //    }

    //});

    $('#txtBank').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode === 13) {
            $('#txtAmount').focus();
            return false;
        }

    });

    $('#txtAmount').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode === 13) {
            $('#txtDatePay').focus();
            return false;
        }

    });

    $('#txtDatePay').change(function () {
        if (hdnLoaneeID !== '') {
            LoaneeSattlTypeHeealthCode(hdnLoaneeID);
        }
    });

    $('#txtDatePay').keypress(function (evt) {
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 13) {
            $('#txtDraweeBank').focus();
            return false;
        }
    });

    $('#txtDraweeBank').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode === 13) {
            $('#ddlInstrumentType').focus();
            return false;
        }

    });

    $('#ddlInstrumentType').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode === 13) {
            $('#txtNo').focus();
            return false;
        }

    });

    $('#txtNo').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode === 13) {
            $('#cmdSearch').focus();
            return false;
        }

    });
});



$(document).ready(function () {
    $("#cmdShow").click(function (event) {

        var RcvID = '';
        $("#tbl").find("tr:not(:nth-child(1)):not(:nth-child(2))").remove();   // remove row of gridview..

        if ($('#txtYearmonth').val().trim() === '') {
            alert("Please enter a correct Yearmonth(YYYYMM)");
            $("#txtYearmonth").css({ "background-color": "#ff9999" });
            $('#txtYearmonth').val('');
            $('#txtYearmonth').focus();
            return false;
        }
        else {
            $("#txtYearmonth").css({ "background-color": "" });
        }

        if ($('#txtText').val() === '') {
            alert("Please select a Loanee before search.");
            $("#txtText").css({ "background-color": "#ff9999" });
            $('#txtText').val('');
            $('#txtText').focus();
            return false;
        }
        else {
            $("#txtText").css({ "background-color": "" });

            if (hdnRcvID === '') {
                alert("Please select a correct Loanee.");
                $("#txtText").css({ "background-color": "#ff9999" });
                $('#txtText').focus();
                $('#txtText').val('');
                hdnRcvID='';
            }

            RcvID = hdnRcvID;
            SetRcvID(1, RcvID);
            //alert(RcvID);
        }

    });
});

$(document).ready(function () {
    $("#btnPrint").click(function (event) {
        ShowReport();
    });
});
function ShowReport() {

    $(".loading-overlay").show();
    var FormName = '';
    var ReportName = '';
    var ReportType = '';
    
    FormName = "PaymentReceived.aspx";
        ReportName = "VOUCHER";
        ReportType = "Voucher Report";
        
    var E = '';
    E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  YearMonth:'" + vchYearMonth + "',  VchNoFrom:'" + vchNo + "', " +
        "  VchNoTo:'" + vchNo + "', VchType:'" + vchType + "'}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/SetReportValue',
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var show = response.d;

            if (show === "OK") {
                window.open("ReportView.aspx?E=Y");
            }

        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
            $(".loading-overlay").hide();
        },
        failure: function (response) {
            alert(response.d);

            $(".loading-overlay").hide();
        }
    });
}

function beforeSave() {
    alert('Yes');
    $("#frmEcom").validate();
    $("#txtLoaneeSearch").rules("add", { required: true, messages: { required: "Please select a Loanee." } });
    $("#txtBank").rules("add", { required: true, messages: { required: "Please select a Bank." } });
    $("#txtAmount").rules("add", { required: true, messages: { required: "Please enter total Amount." } });
    $("#txtDatePay").rules("add", { required: true, messages: { required: "Select a Date" } });
    $("#txtDraweeBank").rules("add", { required: true, messages: { required: "Please enter a Drawee Bank." } });
    $("#ddlInstrumentType").rules("add", { required: true, messages: { required: "Please select a Instrument Type." } });
    $("#txtNo").rules("add", { required: true, messages: { required: "Select enter a No." } });

    $("#ddlRepaymentTerm").rules("add", { required: true, messages: { required: "Settlement Type can not be blank." } });
    $("#txtHealthCode").rules("add", { required: true, messages: { required: "Health Code can not be blank." } });

    if (hdnLoaneeID === "") {
        alert("Please select a correct Loanee from dropdown.");
        $("#txtLoaneeSearch").val('');
    }
    if (hdnBankID === "") {
        alert("Please select a correct Bank from dropdown.");
        $("#txtBank").val('');
    }
}

function SetDraweeBank(type, hdnDraweeBank) {
    var W = "{type:" + type + ",StrDraweeBank:'" + hdnDraweeBank + "'}";
    $.ajax({
        type: "POST",
        url: "PaymentReceived.aspx/GET_DraweeBank",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            return false;
        }
    });
}