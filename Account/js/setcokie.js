﻿
function getCookie(NameOfCookie) {
    if (document.cookie.length > 0) {
        begin = document.cookie.indexOf(" " + NameOfCookie + "=");
        if (begin != -1) {
            begin += NameOfCookie.length + 2;
            end = document.cookie.indexOf(";", begin);
            if (end == -1)
                end = document.cookie.length;
            return unescape(document.cookie.substring(begin, end));
        } else {
            begin = document.cookie.indexOf(NameOfCookie + "=");
            if (begin != -1) {
                begin += NameOfCookie.length + 1;
                end = document.cookie.indexOf(";", begin);
                if (end == -1)
                    end = document.cookie.length;
                return unescape(document.cookie.substring(begin, end));
            }
        }
    }
    return null;
}

function setCookie(NameOfCookie, value, expiredays, path, domain) {
    var ExpireDate = new Date();
    ExpireDate.setTime(ExpireDate.getTime() + (expiredays * 24 * 3600 * 1000));
    document.cookie = NameOfCookie
						+ "="
						+ escape(value)
						+ ((expiredays == null) ? "" : "; expires="
								+ ExpireDate.toGMTString())
						+ ((path) ? ";path=" + path : "")
						+ ((domain) ? ";domain=" + domain : "");
}

function deleteCookie(cookie_name) {
    var cookie_date = new Date();  // current date & time
    cookie_date.setTime(cookie_date.getTime() - 1);
    document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}