﻿
$(document).ready(function () {

    $('#txtProcName').keydown(function (evt) {                /// For Proc Name
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8 || iKeyCode === 46) {
            $('#txtProcOrder').val('');
            $('#txtProcType').val('');
        }
    });
    $("#txtProcName").focus(function () {
        if ($(this).select()) {
            $('#txtProcName').keypress(function (evt) {
                $('#txtProcOrder').val('');
                $('#txtProcType').val('');
            });
        }
    });

    $('#txtDr_GLId').keydown(function (evt) {                /// For Debit GL
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8 || iKeyCode === 46) {
            $('#hdnDr_GLId').val('');
        }
    });
    $("#txtDr_GLId").focus(function () {
        if ($(this).select()) {
            $('#txtDr_GLId').keypress(function (evt) {
                $('#hdnDr_GLId').val('');
            });
        }
    });

    $('#txtDr_SLId').keydown(function (evt) {                /// For Debit SL
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8 || iKeyCode === 46) {
            $('#hdnDr_SLId').val('');
        }
    });
    $("#txtDr_SLId").focus(function () {
        if ($(this).select()) {
            $('#txtDr_SLId').keypress(function (evt) {
                $('#hdnDr_SLId').val('');
            });
        }
    });

    $('#txtDr_SubId').keydown(function (evt) {                /// For Debit SUB
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8 || iKeyCode === 46) {
            $('#hdnDr_SubId').val('');
        }
    });
    $("#txtDr_SubId").focus(function () {
        if ($(this).select()) {
            $('#txtDr_SubId').keypress(function (evt) {
                $('#hdnDr_SubId').val('');
            });
        }
    });

    $('#txtCr_GLId').keydown(function (evt) {                /// For Credit GL
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8 || iKeyCode === 46) {
            $('#hdnCr_GLId').val('');
        }
    });
    $("#txtCr_GLId").focus(function () {
        if ($(this).select()) {
            $('#txtCr_GLId').keypress(function (evt) {
                $('#hdnCr_GLId').val('');
            });
        }
    });

    $('#txtCr_SLId').keydown(function (evt) {                /// For Credit SL
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8 || iKeyCode === 46) {
            $('#hdnCr_SLId').val('');
        }
    });
    $("#txtCr_SLId").focus(function () {
        if ($(this).select()) {
            $('#txtCr_SLId').keypress(function (evt) {
                $('#hdnCr_SLId').val('');
            });
        }
    });

    $('#txtCr_SubId').keydown(function (evt) {                /// For Debit SUB
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8 || iKeyCode === 46) {
            $('#hdnCr_SubId').val('');
        }
    });
    $("#txtCr_SubId").focus(function () {
        if ($(this).select()) {
            $('#txtCr_SubId').keypress(function (evt) {
                $('#hdnCr_SubId').val('');
            });
        }
    });

    Search_ProcName();
    Search_Debit_GL_ID();
    Search_Debit_SL_ID();
    Search_Debit_SUB_ID();

    Search_Credit_GL_ID();
    Search_Credit_SL_ID();
    Search_Credit_SUB_ID();
});


function Search_ProcName() {
    $("#txtProcName").autocomplete({
        source: function (request, response) {
            var ProcName = $("#txtProcName").val().trim();
            var W = "{'ProcName':'" + ProcName + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AccountsVoucherConfig.aspx/GET_AutoCompleteProcNameDetails",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item,
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            var arr = i.item.val.split("/");
            $("#txtProcType").val(arr[1]);
            $("#txtProcOrder").val(arr[2]);
            $("#txtProcName").val(arr[0]);
            $("#txtProcType").prop('disabled', true);
            $("#txtProcOrder").prop('disabled', true);
            return false;
        }
    });
}


function Search_Debit_GL_ID() {
    $("#txtDr_GLId").autocomplete({
        source: function (request, response) {
            var ID = $("#txtDr_GLId").val().trim();
            var Type = "G";
            var W = "{'ID':'" + ID + "','Type':'" + Type + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AccountsVoucherConfig.aspx/GET_AutoCompleteDebitCreditDetails",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("/")[0],
                            val: item.split("/")[1],
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            $("#hdnDr_GLId").val(i.item.val);
        }
    });
}

function Search_Debit_SL_ID() {
    $("#txtDr_SLId").autocomplete({
        source: function (request, response) {
            var ID = $("#txtDr_SLId").val().trim();
            var Type = "S";
            var W = "{'ID':'" + ID + "','Type':'" + Type + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AccountsVoucherConfig.aspx/GET_AutoCompleteDebitCreditDetails",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("/")[0],
                            val: item.split("/")[1],
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            $("#hdnDr_SLId").val(i.item.val);
        }
    });
}

function Search_Debit_SUB_ID() {
    $("#txtDr_SubId").autocomplete({
        source: function (request, response) {
            var ID = $("#txtDr_SubId").val().trim();
            var Type = "T";
            var W = "{'ID':'" + ID + "','Type':'" + Type + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AccountsVoucherConfig.aspx/GET_AutoCompleteDebitCreditDetails",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("/")[0],
                            val: item.split("/")[1],
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            $("#hdnDr_SubId").val(i.item.val);
        }
    });
}

function Search_Credit_GL_ID() {
    $("#txtCr_GLId").autocomplete({
        source: function (request, response) {
            var ID = $("#txtCr_GLId").val().trim();
            var Type = "G";
            var W = "{'ID':'" + ID + "','Type':'" + Type + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AccountsVoucherConfig.aspx/GET_AutoCompleteDebitCreditDetails",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("/")[0],
                            val: item.split("/")[1],
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            $("#hdnCr_GLId").val(i.item.val);
        }
    });
}


function Search_Credit_SL_ID() {
    $("#txtCr_SLId").autocomplete({
        source: function (request, response) {
            var ID = $("#txtCr_SLId").val().trim();
            var Type = "S";
            var W = "{'ID':'" + ID + "','Type':'" + Type + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AccountsVoucherConfig.aspx/GET_AutoCompleteDebitCreditDetails",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("/")[0],
                            val: item.split("/")[1],
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            $("#hdnCr_SLId").val(i.item.val);
        }
    });
}


function Search_Credit_SUB_ID() {
    $("#txtCr_SubId").autocomplete({
        source: function (request, response) {
            var ID = $("#txtCr_SubId").val().trim();
            var Type = "T";
            var W = "{'ID':'" + ID + "','Type':'" + Type + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AccountsVoucherConfig.aspx/GET_AutoCompleteDebitCreditDetails",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("/")[0],
                            val: item.split("/")[1],
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            $("#hdnCr_SubId").val(i.item.val);
        }
    });
}

$(document).ready(function () {
    $('#cmdSave').click(function (e) {

        if ($("#txtProcName").val().trim() == "" || $("#txtProcType").val().trim() == "" || $("#txtProcOrder").val().trim() == "") {
            alert("Partial Or Incorrect Code! Please ReEnter Proc Name Using AutoComplete!");
            $("#txtProcName").val("");
            $("#txtProcType").val("");
            $("#txtProcOrder").val("");
            $("#txtProcName").focus();
            e.preventDefault();
            return false;
        }

        if ($("#hdnDr_GLId").val().trim() == "") {
            alert("Partial Or Incorrect Code! Please ReEnter GL Code Using AutoComplete!");
            $("#txtDr_GLId").val("");
            $("#txtDr_GLId").focus();
            e.preventDefault();
            return false;
        }

        if ($("#hdnDr_SLId").val().trim() == "") {
            alert("Partial Or Incorrect Code! Please ReEnter SL Code Using AutoComplete!");
            $("#txtDr_SLId").val("");
            $("#txtDr_SLId").focus();
            return false;
        }

        if ($("#hdnDr_SubId").val().trim() == "") {
            alert("Partial Or Incorrect Code! Please ReEnter SUB Code Using AutoComplete!");
            $("#txtDr_SubId").val("");
            $("#txtDr_SubId").focus();
            return false;
        }

        if ($("#hdnCr_GLId").val().trim() == "") {
            alert("Partial Or Incorrect Code! Please ReEnter GL Code Using AutoComplete!");
            $("#txtCr_GLId").val("");
            $("#txtCr_GLId").focus();
            return false;
        }

        if ($("#hdnCr_SLId").val().trim() == "") {
            alert("Partial Or Incorrect Code! Please ReEnter SL Code Using AutoComplete!");
            $("#txtCr_SLId").val("");
            $("#txtCr_SLId").focus();
            return false;
        }

        if ($("#hdnCr_SubId").val().trim() == "") {
            alert("Partial Or Incorrect Code! Please ReEnter SUB Code Using AutoComplete!");
            $("#txtCr_SubId").val("");
            $("#txtCr_SubId").focus();
            return false;
        }

        $("#txtProcType").prop('disabled', false);
        $("#txtProcOrder").prop('disabled', false);
    });
});