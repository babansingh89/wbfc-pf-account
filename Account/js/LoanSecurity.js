﻿$(document).ready(function () {

    $('input[type=text]').bind('copy paste', function (e) {
        e.preventDefault();
    });

    $('#txtSecurityDate, #txtMaturityDate, #txtPledgeDate, #txtReleaseDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'

    });

    $("#imgSecurityDate").click(function () {
        $('#txtSecurityDate').datepicker("show");
        return false;
    });
    $("#imgMaturityDate").click(function () {
        $('#txtMaturityDate').datepicker("show");
        return false;
    });
    $("#imgPledgeDate").click(function () {
        $('#txtPledgeDate').datepicker("show");
        return false;
    });
    $("#imgReleaseDate").click(function () {
        $('#txtReleaseDate').datepicker("show");
        return false;
    });

    $('#txtSecurityType').attr({ maxLength: 10 });
    $('#txtSecurityNumber').attr({ maxLength: 20 });
    $('#txtBankName').attr({ maxLength: 50 });
    $('#txtBranchName').attr({ maxLength: 50 });
    $('#txtHolderName').attr({ maxLength: 50 });
    $('#txtFolderName').attr({ maxLength: 25 });
    $('#txtRemarks').attr({ maxLength: 100 });

    $('#txtFaceValue, #txtMaturityValue').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});

$(document).ready(function () {

    SearchLoaneeCode();

    $('#txtLoaneeSearch').keyup(function (evt) {                  /// Prevent delete keypress     
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            //$('#hdnLoaneeID').val('');
            SetLoaneeID(0, hdnLoaneeID);
        }
    });

    $('#txtLoaneeSearch').keydown(function (evt) {
        var iKeyCode = evt.which ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            //$('#hdnLoaneeID').val('');
            SetLoaneeID(0, hdnLoaneeID);
        }
    });

    $("#txtLoaneeSearch").change(function () {
        if ($(this).select()) {
            //$('#hdnLoaneeID').val('');

            //alert('changed');
        }
    });
});

function SearchLoaneeCode() {
    $(".autosuggestLoaneeCode").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AccLoanSecurity.aspx/LoaneeAutoCompleteData",
                data: "{'LoaneeCode':'" + document.getElementById('txtLoaneeSearch').value + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        };
                    }));
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            //$("#txtLoaneeIDSearch").val(i.item.val);
            //$("#hdnLoaneeID").val(i.item.val);
            SetLoaneeID(1, i.item.val); 
        }
    });
}
var hdnLoaneeID = '';
function SetLoaneeID(type, LoaneeID) {
    var W = "{type:" + type + ",LoaneeID:'" + LoaneeID + "'}";
    $.ajax({
        type: "POST",
        url: "AccLoanSecurity.aspx/GET_LoaneeID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            return false;
        }
    });
}
$(document).ready(function () {

    $('#txtLoaneeSearch').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtSecurityType').focus();
            return false;
        }
    });
    $('#txtSecurityType').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtSecurityNumber').focus();
            return false;
        }
    });
    $('#txtSecurityNumber').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtSecurityDate').focus();
            return false;
        }
    });
    $('#txtSecurityDate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtBankName').focus();
            return false;
        }
    });
    $('#txtBankName').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtBranchName').focus();
            return false;
        }
    });
    $('#txtBranchName').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtFaceValue').focus();
            return false;
        }
    });
    $('#txtFaceValue').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtMaturityValue').focus();
            return false;
        }
    });
    $('#txtMaturityValue').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtMaturityDate').focus();
            return false;
        }
    });
    $('#txtMaturityDate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtPledgeDate').focus();
            return false;
        }
    });
    $('#txtPledgeDate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtHolderName').focus();
            return false;
        }
    });
    $('#txtHolderName').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtReleaseDate').focus();
            return false;
        }
    });
    $('#txtReleaseDate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtFolderName').focus();
            return false;
        }
    });
    $('#txtFolderName').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtRemarks').focus();
            return false;
        }
    });

    $('#txtRemarks').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#cmdSave').focus();
            return false;
        }
    });
});