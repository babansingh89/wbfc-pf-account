﻿
$(document).ready(function () {
    $("#btnGenerateReport").click(function (event) {
       // alert("");
        if ($("#ddlReportName").val() == "0") 
        {
            alert("Select a Report Name!");
            return false;    
        }
        else{ ShowReport(); }
    });
    $("#cmdAdd").hide();
    $("#ddladdLoanee").hide();
});     
  
$(document).ready(function () {
    $("#txtYearMonth").blur(function () {
        var mm = $("#txtYearMonth").val();
        var ddmmyy = '30' + '/' + mm.slice(4, 6) + '/' + mm.slice(0, 4);
        //Date date1 = '01' + '/' + mm.slice(4, 6) + '/' + mm.slice(0, 4);
        //var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        //alert(lastDay);
    if ($("#ddlReportName").val() == "IRA") {
        $("#txtAsOn").val(ddmmyy);
    }
});
});

function ShowReport() {
    //var arr = [];
    var FormName = '';
    var ReportName = '';
    var ReportType = '';
    var F = '';
    var LoaneeCode = '';
    var Doc = "0";
    var Etc = "0";
    var txtAsOn = '';
    if ($("#ddlReportName").val() != "DIS" && $("#ddlReportName").val() != "SANC") {
        if ($("#txtYearMonth").val().trim() == "") {
            alert("Enter Year Month(YYYYMM)!");
            return false;
        }
    }
    if ($("#ddlReportName").val() == "AP") {
        FormName = "AccountPosition.aspx";
        ReportName = "accpos";
        ReportType = "Account Position";
        F = 'AP';

        if (document.getElementById("chckAllLoanee").checked == false) {
            if ($("#txtLoaneeCode").val().trim() == "") {
                alert("Enter Loanee Code!");
                $("#txtLoaneeCode").focus();
                return false;
            } else if ($("#txthdnLoaneeCode").val().trim() == "") {
                alert("select Loanee code from suggested code!"); $("#txtLoaneeCode").focus(); return false;
            }
            LoaneeCode = $('#txthdnLoaneeCode').val();

            //if ($('#tbl tr').length > 1) {
            //    $.each($('#tbl tr.trclass'), function () { 
            //        var ID = $(this).find('td:eq(1)').text().trim();
            //        arr.push({ 'LoaneeCode': ID });
            //    });
            //}
            //else {
            //    alert("Please select Lonee Code !"); return false;
            //}
        }
        else {
            LoaneeCode  ='0';
        }
    }   
    if ($("#ddlReportName").val() == "MInRA") {      
        FormName = "AccountPosition.aspx";
        ReportName = "instadv";
        ReportType = "Monthly Installment and Repayment Advise";
        F = 'MInRA';
        if (document.getElementById("chckAllLoanee").checked == false) {
        if ($("#txtLoaneeCode").val().trim() == "") {
            alert("Enter Loanee Code!");
            $("#txtLoaneeCode").focus();
            return false;
        } else if ($("#txthdnLoaneeCode").val().trim() == "") {
            alert("select Loanee code from suggested code!"); $("#txtLoaneeCode").focus(); return false;
        }
            LoaneeCode = $('#txthdnLoaneeCode').val();

            //if ($('#tbl tr').length > 1) {
            //    $.each($('#tbl tr.trclass'), function () {
            //        var ID = $(this).find('td:eq(1)').text().trim();
            //        arr.push({ 'LoaneeCode': ID });
            //    });
            //}
            //else {
            //    alert("Please select Lonee Code !"); return false;
            //}
        }
        else {
            LoaneeCode  ='0';
        }
    }
    if ($("#ddlReportName").val() == "MRS") {
        FormName = "AccountPosition.aspx";
        ReportName = "PRIN_REPAY";
        ReportType = "Monthly Repayment Statement";
        F = 'MRS';
    }
    if ($("#ddlReportName").val() == "IRA") {
        FormName = "AccountPosition.aspx";
        ReportName = "Quarterly Interest Advice";
        ReportType = "Account Position";
        F = 'IRA';
        Doc = $("#txtDoc").val();        
        Etc = $("#txtEtc").val();

        var fromDate = $("#txtAsOn").val().split("/");
        txtAsOn = fromDate[2] + "-" + fromDate[1] + "-" + fromDate[0];

        if (Doc == '') { Doc = '0'; }
        if (Etc == '') { Etc = '0'; }
        if (document.getElementById("chckAllLoanee").checked == false) {
            if ($("#txtLoaneeCode").val().trim() == "") {
                alert("Enter Loanee Code!");
                $("#txtLoaneeCode").focus();
                return false;
            } else if ($("#txthdnLoaneeCode").val().trim() == "") {
                alert("select Loanee code from suggested code!"); $("#txtLoaneeCode").focus(); return false;
            } else


            //if ($('#tbl tr').length > 1) {
            //    $.each($('#tbl tr.trclass'), function () {
            //        var ID = $(this).find('td:eq(1)').text().trim();
            //        arr.push({ 'LoaneeCode': ID });
            //    });
            //}
            //else {
            //    alert("Please select Lonee Code !"); return false;
            //    }


            if ($("#txtAsOn").val().trim() == "") {
                alert("Select a date!");
                return false;
            } else if ($("#txtMinInsAmount").val().trim() == "")
            { alert("Enter Minimum Installment Amount!"); $("#txtMinInsAmount").focus(); return false; }
            LoaneeCode = $('#txthdnLoaneeCode').val();
        }

        else {
            LoaneeCode = '0';
        }

    }





    if ($("#ddlReportName").val() == "APLD") {
        FormName = "AccountPosition.aspx";
        ReportName = "Account Position Loan Detail";
        ReportType = "Account Position Loan Detail";
        F = 'APLD';
        if (document.getElementById("chckAllLoanee").checked == false) {
            //if ($('#tbl tr').length > 1) {
            //    $.each($('#tbl tr.trclass'), function () {
            //        var ID = $(this).find('td:eq(1)').text().trim();
            //        arr.push({ 'LoaneeCode': ID });
            //    });
            //}
            //else {
            //    alert("Please select Lonee Code !"); return false;
            //}
            if ($("#txtLoaneeCode").val().trim() == "") {
                alert("Enter Loanee Code!");
                $("#txtLoaneeCode").focus();
                return false;
            } else if ($("#txthdnLoaneeCode").val().trim() == "") {
                alert("select Loanee code from suggested code!"); $("#txtLoaneeCode").focus(); return false;
            } else if ($("#txtAsOn").val().trim() == "") {
                alert("Select a date!");
                return false;
            }// else if ($("#txtMinInsAmount").val().trim() == "")
            //{ alert("Enter Minimum Installment Amount!"); $("#txtMinInsAmount").focus(); return false; }
            LoaneeCode = $('#txthdnLoaneeCode').val();


        }

        else {
            LoaneeCode = '0';
        }
    }

    if ($("#ddlReportName").val() == "APVD") {
        FormName = "AccountPosition.aspx";
        ReportName = "Account Position Voucher Detail";
        ReportType = "Account Position Voucher Detail";
        F = 'APVD';

        if (document.getElementById("chckAllLoanee").checked == false) {
            //if ($('#tbl tr').length > 1) {
            //    $.each($('#tbl tr.trclass'), function () {
            //        var ID = $(this).find('td:eq(1)').text().trim();
            //        arr.push({ 'LoaneeCode': ID });
            //    });
            //}
            //else {
            //    alert("Please select Lonee Code !"); return false;
            //}

            if ($("#txtLoaneeCode").val().trim() == "") {
                alert("Enter Loanee Code!");
                $("#txtLoaneeCode").focus();
                return false;
            } else if ($("#txthdnLoaneeCode").val().trim() == "") {
                alert("select Loanee code from suggested code!"); $("#txtLoaneeCode").focus(); return false;
            } else if ($("#txtAsOn").val().trim() == "") {
                alert("Select a date!");
                return false;
            }// else if ($("#txtMinInsAmount").val().trim() == "")
            //{ alert("Enter Minimum Installment Amount!"); $("#txtMinInsAmount").focus(); return false; }
            LoaneeCode = $('#txthdnLoaneeCode').val();
        }

        else {
            LoaneeCode = '0';
        }
    }

    if ($("#ddlReportName").val() == "DIS" || $("#ddlReportName").val() == "SANC") {
        if ($("#txtFrmDate").val() != '' && $("#txtToDate").val() != '') {
            if ($("#ddlReportName").val() == "SANC") {
                ReportName = "Acc_StatementOfSec";
                ReportType = "Acc_StatementOfSec";
            }
            else {
                ReportName = "Acc_StatementOfDisbursement_Rep";
                ReportType = "Acc_StatementOfDisbursement_Rep";
            }
            FormName = "AccountPosition.aspx";
            F = $("#ddlReportName").val();
            LoaneeCode = '0';

            var from = $("#txtFrmDate").val().split("/");
            Doc = from[2] + "-" + from[1] + "-" + from[0];

            var To = $("#txtToDate").val().split("/");
            Etc = To[2] + "-" + To[1] + "-" + To[0];

            //if (document.getElementById("chckAllLoanee").checked == false) {
            //      if ($("#txtLoaneeCode").val().trim() == "") {
            //        alert("Enter Loanee Code!");
            //        $("#txtLoaneeCode").focus();
            //        return false;
            //    }
            //}
            //LoaneeCode = $('#txthdnLoaneeCode').val();
            LoaneeCode = LoaneeID;
            if (LoaneeCode == '') { LoaneeCode = 0; }

        } else { alert("Please Select Date!"); return false;}
    }
    //if (LoaneeCode!='0')
    //LoaneeCode = JSON.stringify(arr);
    
    var E = '';  
    E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  YearMonth:'" + $('#txtYearMonth').val()
        + "',  LoaneeUni_ID:'" + LoaneeCode + "', AsOnDate:'" + txtAsOn + "', MinInsAmount:'" + $('#txtMinInsAmount').val() + "', F:'" + F + "', Doc:'" + Doc + "', Etc:'" + Etc + "'}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/SetReportValue',
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var show = response.d;

            if (show == "OK") {
                window.open("ReportView.aspx?E=Y"); //querystring E not equal to null (E=Y)..               
            }

        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
          //  $(".loading-overlay").hide();
        },
        failure: function (response) {
            alert(response.d);
          //  $(".loading-overlay").hide();
        }
    });
}

$(document).ready(function () {
    //$('[id*=txtYearMonth]').keypress(function (e) {
    //    var regex = new RegExp("^[a-zA-Z]+$");
    //    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    //    if (regex.test(str)) {
    //        return false;
    //    }
    //    return true;
    //});
    $('#txtYearMonth').keypress(function (evt) {
        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
            alert("Please Enter a Numeric Value");
            return false;
        }
    });
    $('#txtMinInsAmount').keypress(function (evt) {
        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
            alert("Please Enter a Numeric Value");
            return false;
        }
    });
    //$('[id*=txtMinInsAmount]').keypress(function (e) {
    //    var regex = new RegExp("^[a-zA-Z]+$");
    //    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    //    if (regex.test(str)) {
    //        return false;
    //    }
    //    return true;
    //});
    $('#txtAsOn, #txtFrmDate, #txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });
    $("#tdMIAchange").hide();
    $("#tdAsOnChange").hide();
    $("#tdDoc").val("");
    $("#tdDoc").hide();
    $("#TdEtc").val("");
    $("#TdEtc").hide();

    
    $("#ddlReportName").change(function () {
        if ($("#ddlReportName").val() == "DIS" || $("#ddlReportName").val() == "SANC") {
            $("#tdStatement").show();
            $("#ym").hide();
            $("#txtYearMonth").val("");
            $("#txtAsOn").val("");
            $("#tdAsOnChange").hide();
            $("#txtLoaneeCode").val("");
            $("#txthdnLoaneeCode").val("");
            //$("#tdLCChange").hide();
            $("#tdDoc").val("");
            $("#tdDoc").hide();
            $("#TdEtc").val("");
            $("#TdEtc").hide();
            $("#divloanee").show();
            $("#ddladdLoanee").show();

            $("#cmdAdd").show();
            $("#ddladdLoanee").show();

        } else { $("#ym").show(); $("#tdLCChange").show(); $("#tdStatement").hide(); $("#txtFrmDate").val(""); $("#txtToDate").val(""); }

        if ($("#ddlReportName").val() == "AP") {
            $("#txtAsOn").val("");
            document.getElementById('txtYearMonth').disabled = false;
            $("#tdAsOnChange").hide();
            $("#tdDoc").val("");
            $("#tdDoc").hide();
            $("#TdEtc").val("");
            $("#TdEtc").hide();

            $("#cmdAdd").hide();
            $("#ddladdLoanee").hide();
        }        
        if ($("#ddlReportName").val() == "MInRA") {          
            $("#tdLCChange").show();
            document.getElementById('txtYearMonth').disabled = false;
            $("#txtAsOn").val("");
            $("#tdAsOnChange").hide();
            $("#tdDoc").val("");
            $("#tdDoc").hide();
            $("#TdEtc").val("");
            $("#TdEtc").hide();
            $("#cmdAdd").hide();
            $("#ddladdLoanee").hide();
        }
        if ($("#ddlReportName").val() == "MRS") {
            $("#txtAsOn").val("");
            $("#tdAsOnChange").hide();
            document.getElementById('txtYearMonth').disabled = false;
            $("#txtLoaneeCode").val("");
            $("#txthdnLoaneeCode").val("");
            $("#tdLCChange").hide();
            $("#tdDoc").val("");
            $("#tdDoc").hide();
            $("#TdEtc").val("");
            $("#TdEtc").hide();
            $("#cmdAdd").hide();
            $("#ddladdLoanee").hide();
        }
        //else { $("#tdLCChange").show(); }
        if ($("#ddlReportName").val() == "IRA") {
            $("#tdAsOnChange").show();
            $("#lblAsOn").text('Payment Date');
            document.getElementById('txtYearMonth').disabled = false;
            $("#tdMIAchange").show();
            $("#tdDoc").val("");
            $("#tdDoc").show();
            $("#TdEtc").val("");
            $("#TdEtc").show();
            $("#cmdAdd").hide();
            $("#ddladdLoanee").hide();
        }
        else {
            $("#txtMinInsAmount").val("");          
            $("#tdMIAchange").hide();
            $("#lblAsOn").text('As On');
            //$("#cmdAdd").hide();
            //$("#ddladdLoanee").hide();
        }
    });
});

function selectAll() {
    
    if (document.getElementById("chckAllLoanee").checked == true) {
        document.getElementById("txtLoaneeCode").disabled = true;
        $('#txthdnLoaneeCode').val('');
        $('#txthdnLoaneeCode').val('');
    }
    else {
        document.getElementById("txtLoaneeCode").disabled = false;
        $("#txtLoaneeCode").focus();
    }
}

var LoaneeID = 0;
$(document).ready(function () {
    autocompleteLoaneeCode();
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });

    $("#cmdAdd").click(function (event) {
        event.preventDefault();
        if ($("#txtLoaneeCode").val() == '') { alert('Please select Loanee Code'); $("#txtLoaneeCode").focus(); return false;}
        var ddl = document.getElementById("ddladdLoanee");
        var option = document.createElement("OPTION");
        option.innerHTML = document.getElementById("txtLoaneeCode").value;
        option.value = document.getElementById("txthdnLoaneeCode").value;
        ddl.options.add(option);
        $("#txtLoaneeCode").val('');
        $("#txthdnLoaneeCode").val('');
        $("#txtLoaneeCode").focus();
        if (LoaneeID == 0) { LoaneeID = option.value; } else { LoaneeID = LoaneeID + "," + option.value;}
        //alert(LoaneeID);
    });
});

function autocompleteLoaneeCode() {
    $("#txtLoaneeCode").autocomplete({
        source: function (request, response) {
            var STcode = $("#txtLoaneeCode").val();
            var SecID = $("#ddlUserSector").val();;
            var W = "{'STcode':'" + STcode + "',SecID:" + SecID +"}";
            $.ajax({
                type: "POST", 
                contentType: "application/json; charset=utf-8",
                url: "AccountPosition.aspx/GET_AutoComplete_StartCode",
                data: W,
                dataType: "json",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            //label: '<input type="checkbox" id="check' + item.split("|")[1] + '" name="check' + item.split("|")[1] + '"><label for="check' + item.split("|")[1] + '">' + item.split("|")[0] + '</label>' + item.split("|")[0] + '<>' + item.split("|")[1],
                            label: item.split("|")[0] + '<>' + item.split("|")[1],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        minLength: 0,
        select: function (e, i) {
         
            var arr = i.item.val.split("|");
            $("#txthdnLoaneeCode").val(arr[2]);
            //var lonee = arr[0] + '<>' + arr[1];
            //Selectedlonee(lonee, arr[2]);
            //$("#txtLoaneeCode").val("");
        }
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
}


//$(document).ready(function () {
//    var grid = document.getElementById('tbl');
//    grid.deleteRow(1);
//});
//function Selectedlonee(name, ID) {
//    $("#tbl").append("<tr class='trclass'><td>" +
//           name + "</td> <td style='display:none'>" +
//           ID + "</td> <td>" +
//           "<div onClick='deleteLonee(this);' style='text-align:center;'><img src='images/Delete.gif'/>" + "</td></tr>");
//    $("#txtLoaneeCode").val("");
//    $("#txthdnLoaneeCode").val("");
//}
//function deleteLonee(t) {
//    var a = $(t).closest('tr');
//    a.remove();
//}