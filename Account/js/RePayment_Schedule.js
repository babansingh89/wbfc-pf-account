﻿var check = true;
var hdnLoaneeUniqueId = '';
var hndID1 = '';
$(document).ready(function () {
    $("#txtFirstDisburseDate,#txtRepStaartDt,#txtEffectiveFrom").attr("placeholder", "dd/mm/yyyy");
    $("#txtFirstDisburseDate,#txtRepStaartDt,#txtEffectiveFrom").attr("MaxLength", "10");
    $("#txtFirstDisburseDate,#txtRepStaartDt").on("keyup", function () {
        $('#txtRepStaartDt').datepicker({
            minDate: $('#txtFirstDisburseDate').val(),
            onSelect: function (dateText, inst) {
                var date1 = $('#txtFirstDisburseDate').val();
                var date2 = $('#txtRepStaartDt').val();
            }
        });
        if ($("#txtFirstDisburseDate").val().length == 10 && $("#txtRepStaartDt").val().length==10) {
            calcualteMonthYr();
        }
    });
    
    SetLoaneeUniqueId();

    $('.loading-overlay').show();
    $.ajax({
        type: "POST",
        url: "RePayment_Schedule.aspx/SetExistData",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (D) {
            if (D.d != 0) {
                var xmlDoc = $.parseXML(D.d);
                var xml = $(xmlDoc);
                var MainData = xml.find("Table");

                $('#txtSanctionAmount').val($(MainData).find('SanctionAmt').text());
                $('#ddlRepaymentTerm').val($(MainData).find('RepayTerm').text());
                $('#txtRepStaartDt').val($(MainData).find('RepayStartDate').text());
                $('#txtFirstDisburseDate').val($(MainData).find('FirstDisburseDate').text());
                $('#txtMoratoriumPeriod').val($(MainData).find('MoratPeriod').text());
                $('#txtInterestRate').val($(MainData).find('InterestRate').text());
                $('#txtRebateRate').val($(MainData).find('RebateRate').text());
                $('#txtPenaltyRate').val($(MainData).find('PenaltyRate').text());
                $('#txtEffectiveFrom').val($(MainData).find('EffectiveFrom').text());
                // var i = 1;

                $('#txtRepStaartDt').datepicker({
                    minDate: $('#txtFirstDisburseDate').val(),
                    onSelect: function (dateText, inst) {
                        //var date1 = $('#txtFirstDisburseDate').val();
                        //var date2 = $('#txtRepStaartDt').val();
                        calcualteMonthYr();

                    }
                });
                $(xml).find('Table1').each(function () {
                    $('#tblRepayment').append('<tr><td class="idSrNo">' + $(this).find('InstSrl').text()
                  + '</td><td align="center">' + $(this).find('NoOfInst').text()
                    + '</td><td align="right">' + $(this).find('InstAmt').text()
                     + '</td><td align="right">' +(parseFloat( $(this).find('NoOfInst').text())*parseFloat( $(this).find('InstAmt').text()))
                      + '<td style="text-align:center"><input type="button" style="height:25px;width:60px" class="myButton1" value="Edit" onclick="Edit1(this)"></td><td style="text-align:center"><input type="button" style="height:25px;width:60px" class="myButton1" value="Delete" onclick="Delete1(this)"></td></tr>');
                });
                $('.loading-overlay').hide();
            }
            $('.loading-overlay').hide();
        },
    });
    //AddToGrid();
    $('.datepicker1').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        maxDate: '0',
        dateformat: 'dd/mm/yyyy'
    });
    //$('#txtRepStaartDt').keydown(function () { return false;});

    $('.datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        maxDate: '0',
        onSelect: function (dateText, inst) {
            var date = $(this).val();
            if ($('#txtFirstDisburseDate').val() != '') {
                $("#txtRepStaartDt").removeAttr('disabled');
                $("#txtRepStaartDt").datepicker("destroy");
                $('#txtRepStaartDt').val('');
                $('#txtMoratoriumPeriod').val('');
                $("#txtRepStaartDt").datepicker("destroy");
                $('#txtRepStaartDt').datepicker({
                    minDate: $('#txtFirstDisburseDate').val(),
                    onSelect: function (dateText, inst) {
                        var date1 = $('#txtFirstDisburseDate').val();
                        var date2 = $('#txtRepStaartDt').val();
                        calcualteMonthYr();

                    }
                });
            }
        },
        dateformat: 'dd/mm/yyyy'
    });

    function calcualteMonthYr() {
        var To = $("#txtRepStaartDt").val().split("/");

       


        var from = $("#txtFirstDisburseDate").val().split("/");
        var fromDate = new Date(from[2], from[1] - 1, from[0]);
        var toDate = new Date(To[2], To[1] - 1, To[0]);


        //var fromDate = new Date($('#txtRepStaartDt').val()); // Date picker (text fields)
        //var toDate = new Date($('#txtFirstDisburseDate').val());
        var months = 0;
        months = (toDate.getFullYear() - fromDate.getFullYear()) * 12;
        months -= fromDate.getMonth();
        months += toDate.getMonth();
        if (toDate.getDate() < fromDate.getDate()) {
            months--;
        }
        $('#txtMoratoriumPeriod').val(months); // result
    }


    //txtRepStaartDt
    $(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
});



//===========================Start Add Data Shed & Other Civil Construction =================================//
$(function () {
    $('#btnAdd').on('click', function (e) {
        if ($('#NoOfsInst').val() == '') {
            //alert("Please Enter Number Of Interest..!");
            $('#NoOfsInst').focus();
            return false;
        }
        if ($('#InstAmt').val() == '') {
            //alert("Please Enter Interest Amount..!");
            $('#InstAmt').focus();
            return false;
        }

        e.preventDefault();
        if ($(this).val() == 'Update') {
            $('#tblRepayment tr').each(function () {
                if ($(this).find('td:eq(0)').text() == hndID1) {
                    //$(this).find('td:eq(1)').text($('#InstSrl').val());
                    $(this).find('td:eq(1)').text($('#NoOfsInst').val());
                    $(this).find('td:eq(2)').text($('#InstAmt').val());
                    $(this).find('td:eq(3)').text(parseFloat($('#NoOfsInst').val()) * parseFloat($('#InstAmt').val()));
                    $('.clr').val('');
                    $('#btnAdd').val('Add');
                    hndID1='';
                    return false;
                }
            });
        }
        else {
            var max = 0;
            var high = Math.max.apply(Math, $('.idSrNo').map(function () {
                max = $(this).text();
            }));

            $('#tblRepayment').append('<tr><td class="idSrNo">' + ++max
                   + '</td><td align="center">' + $('#NoOfsInst').val()
                     + '</td><td align="right">' + $('#InstAmt').val()
                       + '</td><td align="right">' + (parseFloat($('#NoOfsInst').val()) * parseFloat($('#InstAmt').val()))
                       + '<td style="text-align:center"><input type="button" style="height:25px;width:60px" class="myButton1" value="Edit" onclick="Edit1(this)"></td><td style="text-align:center"><input type="button" style="height:25px;width:60px" class="myButton1" value="Delete" onclick="Delete1(this)"></td></tr>');
            $('.clr').val('');
            $("#txtAsPerScheme").removeAttr("disabled");
        }

    });
});
//===========================End Add Data Gridview=================================//
//===========================Start Edit   =================================//
function Edit1(a) {
    var b = $(a).closest('tr');
    hndID1=$(b).closest('tr').find('td:eq(0)').text();
    //$('#InstSrl').val($(b).closest('tr').find('td:eq(1)').text());
    $('#NoOfsInst').val($(b).closest('tr').find('td:eq(1)').text());
    $('#InstAmt').val($(b).closest('tr').find('td:eq(2)').text());
    $('#btnAdd').val('Update');
}
//=========================== End Edit  =================================//
function Delete1(a) {
    var b = $(a).closest('tr');
    b.remove();
    $('#btnAdd1').val('Add');
    $('.clr').val('');
    var i = 1;
    $.each($(".idSrNo"), function (i, el) {
        $(this).text(i + 1);
    });
}


$(function () {
    $('#btnSave').click(function (e) {
        validate();
        if (!check) { return false;}
        $('.loading-overlay').show();
        e.preventDefault();
        var mst = [];
        var obj = {};
        obj.LoaneeID = hdnLoaneeUniqueId;
        obj.SanctionAmt = $('#txtSanctionAmount').val();
        obj.RepayTerm = $('#ddlRepaymentTerm').val();
        //===============================================================
        var Date = $('#txtRepStaartDt').val().split('/');
        var StrRepayStartDate = Date[2] + '-' + Date[1] + '-' + Date[0];
        obj.RepayStartDate = StrRepayStartDate;
        //===============================================================
        var Date1 = $('#txtFirstDisburseDate').val().split('/');
        var StrDisburseDate = Date1[2] + '-' + Date1[1] + '-' + Date1[0];
        obj.FirstDisburseDate = StrDisburseDate;
        //===============================================================
        obj.MoratPeriod = $('#txtMoratoriumPeriod').val() == '' ? 0 : $('#txtMoratoriumPeriod').val();
        obj.InterestRate = $('#txtInterestRate').val() == '' ? 0 : $('#txtInterestRate').val();
        obj.RebateRate = $('#txtRebateRate').val() == '' ? 0 : $('#txtRebateRate').val();
        obj.PenaltyRate = $('#txtPenaltyRate').val() == '' ? 0 : $('#txtPenaltyRate').val();
        //===============================================================
        var Date2 = $('#txtEffectiveFrom').val().split('/');
        var StrEffectiveFrom = Date2[2] + '-' + Date2[1] + '-' + Date2[0];
        obj.EffectiveFrom = StrEffectiveFrom;
        //===============================================================
        mst.push(obj);
        var Details = [];
        $('#tblRepayment tr').each(function (index, value) {
            if (index != 0) {
                var obj1 = {};
                obj1.SerialNo = $(this).find('td:eq(0)').text();
                obj1.NoOfInst = $(this).find('td:eq(1)').text();
                obj1.InterAmt = $(this).find('td:eq(2)').text();
                Details.push(obj1);
            }
        });
        var W = "{'StrMaster':" + JSON.stringify(mst) + ",'StrDetails':" + JSON.stringify(Details) + "}";
        $.ajax({
            type: "POST",
            url: "RePayment_Schedule.aspx/SaveData",
            data: W,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (d) {
                if (d.d != 0) {
                    alert(d.d);
                    window.location.href = "RePayment_Schedule.aspx";
                    return false;
                }
                else {
                    $('.loading-overlay').hide();
                    alert("Record Not Save Successfully...!");
                }
            },
        });
        
    });
});

//=================================Validation For All===========================//
function validate() {
    
    $('.Required').each(function () {
        if ($(this).val() == '') {
            alert("Plese Enter Value...!"); $(this).focus();
            check = false;
            return false;
        }
        else {
            check = true;
        }
       
    });
}

function SetLoaneeUniqueId() {
    var W = "{}";
    $.ajax({
        type: "POST",
        url: "RePayment_Schedule.aspx/GET_LoaneeUniqueId",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            hdnLoaneeUniqueId = data.d;
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            return false;
        }
    });
}