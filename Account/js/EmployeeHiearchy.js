﻿

var mode = '';
// ==================== Start Scheme Name keypress Type Not Allowed Coding Here =========================//       check1
$(document).ready(function () {
    $('#txtEmployeeNo').keypress(function (evt) {
        var ValEmpNo = '';
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {

            if ($("#txtReportingNo").val() != "") {

                mode = '1';
                if ($('#txtEmployeeNo').val() != '') {
                    EmployeeList(ValEmpNo);
                    $('#txtSearchEmployee').val($('#txtEmployeeNo').val());
                    return false;
                }
                else {
                    $('#txtSearchEmployee').val('');
                    EmployeeList(ValEmpNo);
                    return false;
                }

                if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
                    alert("Please Select Employee Number");
                    EmployeeList();
                    return false;
                }
            }
            else {

                alert("First Select Reporting Manager Employee No.");
                $("#txtReportingNo").focus();
                return true;
            }
        }
   });
});
// ==================== End Scheme Name keypress Type Not Allowed Coding Here =========================//

// ==================== Start Scheme Name keypress Type Not Allowed Coding Here =========================//       check1
$(document).ready(function () {
    $('#txtReportingNo').keypress(function (evt) {
        var ValEmpNo = '';
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        ValEmpNo = $('#txtEmployeeNo').val();
        if (iKeyCode == 13) {
            mode = '2';
           
            if ($('#txtReportingNo').val() != '') {
                EmployeeList(ValEmpNo);
                $('#txtSearchEmployee').val($('#txtReportingNo').val());
                return false;
            }
            else {
                $('#txtSearchEmployee').val('');
                EmployeeList(ValEmpNo);
                return false;
            }
        }
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
            alert("Please Select Employee Number");
            EmployeeList();
            return false;
        }
    });
});
// ==================== End Scheme Name keypress Type Not Allowed Coding Here =========================//
//==============Start Scheme Name Popup coding here=====================================//

function EmployeeList(ValEmpNo) {
    var EmpNo = '';
    EmpNo = ValEmpNo;
    $(".loading-overlay").show();
    var W = "{EmpNo:'" + EmpNo + "'}";
    $.ajax({
        type: "POST",
        url: "ReportingHiearchy.aspx/GET_EmployeeList",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            $("#GridViewEmployeeList").empty();
            if (data.d.length > 0) {
                $("#GridViewEmployeeList").append("<tr><th>Employee No</th><th>Employee Name</th></tr>");
                EmployeeListShowModalPopup();
                                for (var i = 0; i < data.d.length; i++) {
                                    $("#GridViewEmployeeList").append("<tr><td>" +
                                    data.d[i].EmpNo + "</td> <td>" +
                                    data.d[i].EmpName + "</td></tr>");
                                    
                                }
                    $(".loading-overlay").hide();
                //===================== Start Folio Number Search is not Blank Then Foloi Number Record Display in GridView ===========================
                    if ($('#txtSearchEmployee').val() != '') {
                    var rows;
                    var coldata;
                    $('#GridViewEmployeeList').find('tr:gt(0)').hide();
                    var data = $('#txtSearchEmployee').val();
                    var len = data.length;
                    if (len > 0) {
                        $('#GridViewEmployeeList').find('tbody tr').each(function () {
                            coldata = $(this).children().eq(0);
                            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                            if (temp === 0) {
                                $(this).show();
                            }

                        });
                    } else {
                        $('#GridViewEmployeeList').find('tr:gt(0)').show();

                    }
                }


                //===================== End Foloi Number Search is not Blank Then Foloi Number Record Display in GridView ===========================

               
                    $("[id*=GridViewEmployeeList] td").bind("click", function () {
                    var row = $(this).parent();
                    $("[id*=GridViewEmployeeList] tr").each(function () {
                        if ($(this)[0] != row[0]) {
                            $("td", this).removeClass("selected_row");
                        }
                    });

                    $("td", row).each(function () {
                        if (!$(this).hasClass("selected_row")) {
                            $(this).addClass("selected_row");
                        } else {
                            $(this).removeClass("selected_row");
                        }
                    });
                });


                //===================== Start Selected Gridview Value Display in Text Box ===========================//bind
                $(document).ready(function () {
                    $("[id*=GridViewEmployeeList] tbody tr").click("click", function () {

                        var ValEmpNo = $(this).children("td:eq(0)").text();
                        $("#txtSearchEmployee").val(ValEmpNo);
                        $("#txtSearchEmployee").focus();
                    });
                });
                //===================== End Selected Gridview Value Display in Text Box ===========================//
            }
            else {
                alert("No Records Data");
                $(".loading-overlay").hide();
            }
        },
        error: function (result) {
            alert("Error Records Data");
            $(".loading-overlay").hide();

        }

    });
}

//============= Start From Indent Search Coding Here ======================================//
$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtSearchEmployee').keyup(function () {
        $("#GridViewEmployeeList").find('tr:gt(0)').hide();
        var data = $('#txtSearchEmployee').val();
        var len = data.length;
        if (len > 0) {
            $("#GridViewEmployeeList").find('tbody tr').each(function () {
                coldata = $(this).children().eq(0);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        } else {
            $("#GridViewEmployeeList").find('tr:gt(0)').show();
        }

    });
});
//============= Start From Indent Search Coding Here ======================================//

//============= Start Key Enter On From Indent Search Coding Here ======================================//
$(document).ready(function () {
    $("#txtSearchEmployee").keypress(function (e) {
        if (e.keyCode == 13) {
            var GridViewEmp = document.getElementById("GridViewEmployeeList");
            var ValEmpNo = '';
            var ValEmpName = '';
            if ($('#txtSearchEmployee').val().trim() == '') {
                alert("Please Select Employee Number");
                $('#txtSearchEmployee').val('');
                $("#txtSearchEmployee").focus();
                return false;
            }
            if ($('#txtSearchEmployee').val() != '') {
                for (var row = 1; row < GridViewEmp.rows.length; row++) {
                    var GridEmpNo_Cell = GridViewEmp.rows[row].cells[0];
                    var valueEmpNo = GridEmpNo_Cell.textContent.toString();
                   
                    var GridEmpName_Cell = GridViewEmp.rows[row].cells[1];
                    var valueEmpName = GridEmpName_Cell.textContent.toString();
                   
                    if (valueEmpNo == $("#txtSearchEmployee").val().toUpperCase()) {
                        ValEmpNo = valueEmpNo;
                        ValEmpName = valueEmpName;
                        break;
                    }
                }

                if (ValEmpNo != $('#txtSearchEmployee').val().toUpperCase()) {
                    alert("Invalid Employee Number (Employee Number is Not in List)");
                    $("#txtSearchEmployee").focus();
                    return false;
                }

                else {
                    if (mode == '1') {
                        $("#txtEmployeeNo").val($("#txtSearchEmployee").val());
                        $("#txtEmpName").val(ValEmpName);
                        $("#txtEmployeeNo").focus();
                        ShowRank();
                    }
                    if (mode == '2') {
                        $("#txtReportingNo").val($("#txtSearchEmployee").val()); 
                        $("#txtReportingName").val(ValEmpName);
                        $("#txtReportingNo").focus();
                       
                    }
                    $("#txtSearchEmployee").val('');
                    $("#dialogEmployeeList").dialog('close');
                    return false;
                }
            }
        }

    });
});

function EmployeeListShowModalPopup() {
    $("#dialogEmployeeList").dialog({
        title: "Employee  Search",
        width: 600,

        buttons: {
            Ok: function () {
                var GridViewEmp = document.getElementById("GridViewEmployeeList");
                var ValEmpNo = '';
                var ValEmpName = '';
                if ($('#txtSearchEmployee').val().trim() == '') {
                    alert("Please Select Employee Number");
                    $('#txtSearchEmployee').val('');
                    $("#txtSearchEmployee").focus();
                    return false;
                }
                if ($('#txtSearchEmployee').val() != '') {
                    for (var row = 1; row < GridViewEmp.rows.length; row++) {
                        var GridEmpNo_Cell = GridViewEmp.rows[row].cells[0];
                        var valueEmpNo = GridEmpNo_Cell.textContent.toString();

                        var GridEmpName_Cell = GridViewEmp.rows[row].cells[1];
                        var valueEmpName = GridEmpName_Cell.textContent.toString();
                        if (valueEmpNo == $('#txtSearchEmployee').val()) {
                            ValEmpNo = valueEmpNo;
                            ValEmpName = valueEmpName;
                            break;
                        }
                    }

                    if (ValEmpNo != $('#txtSearchEmployee').val()) {
                        alert("Invalid Employee Number (Employee Number is Not in List)");
                        $("#txtSearchEmployee").focus();
                        return false;
                    }

                    else {
                        if (mode == '1')
                        {
                            $("#txtEmployeeNo").val($("#txtSearchEmployee").val());
                            $("#txtEmpName").val(ValEmpName);
                            $("#txtEmployeeNo").focus();
                            ShowRank();
                        }
                        if (mode == '2') {
                            $("#txtReportingNo").val($("#txtSearchEmployee").val());
                            $("#txtReportingName").val(ValEmpName);
                            $("#txtReportingNo").focus();
                        }
                        $("#txtSearchEmployee").val('');
                        $("#dialogEmployeeList").dialog('close');
                        return false;
                    }
                }
            }
        },
        modal: true
    });
}

function ShowRank()
{
   
        var W = "{EmpNo:'" + $("#txtReportingNo").val() + "'}";
        $.ajax({
            type: "POST",
            url: "ReportingHiearchy.aspx/GET_EmployeeRank",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                var t = JSON.parse(data.d);
                $("#txtRank").val(parseInt(t) + 1);
            }
        });
    

}
