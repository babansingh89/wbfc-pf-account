﻿/// <reference path="jquery-1.8.1.js" />
/// <reference path="utility.js" />
/// <reference path="json2.js" />
/// 
var maxdate;
var maxStartDays;
$(document).ready(function () {
    $(".loading-overlay").show();
    $(".loading-overlay").hide();

    var BookingStartDays = $("#BookingStartDays").val();
    var maxopen = $("#MaxOpenDays").val();

    maxStartDays = new Date();
    maxdate = new Date();
    var cdt = $("#curdt").val();
    var curdate = new Date(cdt);
    if (maxopen != null) {
        maxdate.setTime(curdate.getTime() + (maxopen) * 24 * 60 * 60 * 1000);
    };
    if (BookingStartDays != null) {
        maxStartDays.setTime(curdate.getTime() + BookingStartDays * 24 * 60 * 60 * 1000);
    }
    //alert("cdt  ===  " + cdt + "  ===  curdate " + curdate + "   ===   add days " + maxopen + " =====   added days    " + maxdate + "    " + maxStartDays);
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    $('#txtBookingDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        minDate: 0,
        maxDate: maxStartDays
        //        onSelect: function (selectedDate) {
        //            $("#txtBookingDateTo").datepicker("option", "minDate", selectedDate);
        //        }

    });

    $('#txtBookingDateTo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        minDate: 0,
        maxDate: maxdate
        //        onSelect: function (selectedDate) {
        //            $("#txtBookingDate").datepicker("option", "maxDate", selectedDate);
        //        }

    });

    $("#btnDatePicker").click(function () {
        $('#txtBookingDate').datepicker("show");
        return false;
    });

    $("#btnDatePickerTo").click(function () {
        $('#txtBookingDateTo').datepicker("show");
        return false;
    });

    //    $("#txtBookingDateTo").change(function () {

    //        check(this);
    //        

    //    });


    window.checkPage = new WBFDC.CheckPage();
    window.checkPage.init();
    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });

});

function lpad(originalstr, length, strToPad) {
    while (originalstr.length < length)
        originalstr = strToPad + originalstr;
    return originalstr;
}

function rpad(originalstr, length, strToPad) {
    while (originalstr.length < length)
        originalstr = originalstr + strToPad;
    return originalstr;
}
function dateDiff() {
    var frmdate = null, toDate = null, totalDays = "";
    if ($("#txtBookingDate").val().trim()) {
        frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
    }
    if ($("#txtBookingDateTo").val().trim()) {
        toDate = new Date($("#txtBookingDateTo").datepicker('getDate'));
    }

    if ((frmdate != null && toDate != null))
        totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;
    //alert(frmdate+'   '+ toDate+'  '+ totalDays);
    return totalDays;
}
function check(ch) {
    if (ch.id == "txtBookingDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtBookingDateTo").datepicker("option", "minDate", '-100Y');
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtBookingDateTo").datepicker("option", "minDate", testm);
        }
    } else if (ch.id == "txtBookingDateTo") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);
        }
    }
}
WBFDC.MasterInfo = function () {
    var A = this;
    this.validator;
    
    this.init = function () {

        this.validator = A.getValidator();
        
        $("#ddlLocation").change(A.PopulateProperty);
        $("#txtBookingDate").change(A.BookingDateChanged);
        $("#txtBookingDateTo").change(A.BookingDateChangedTo);
        $("#ddlProperty").change(A.PopulateTariff);

        $("#btnShow").click(A.ShowClicked);
    };
    
   
    this.getValidator = function () {
        $("#available").validate();
        $("#txtBookingDate").rules("add",  { required: true ,messages: {required: "Please provide a valid booking date"}});
        $("#ddlLocation").rules("add",  { required: true ,messages: {required: "Please provide a valid location"}});
        $("#ddlProperty").rules("add",  { required: true ,messages: {required: "Please provide a valid property"}});
        $("#ddlFrom").rules("add",  { required: true ,messages: {required: "Please provide a valid range from"}});
        $("#ddlTo").rules("add",  { required: true ,messages: {required: "Please provide a valid range to"}});
        $("#txtNOR").rules("add",  { required: true, number: true ,messages: {required: "Please provide a valid no of rooms", number: "Please provide a valid no of rooms"}});
        return $("#available").validate();       
    };




    this.ShowClicked = function () {

        //alert('addclicked');
        if (A.validator.form()) {
            A.setFormValues();
        }
    };
   

    this.BookingDateChanged = function () {
        //alert('changed');
        A.PopulateTariff();
        check(this);
        var tDays = dateDiff();
        if (tDays != "")
            $("#spanNOD").html(tDays + " Days ");
        else
            $("#spanNOD").html("");
    };
    this.BookingDateChangedTo = function () {
        //alert('changed');
        //A.PopulateTariff();
        check(this);
        var tDays = dateDiff();
        if (tDays != "")
            $("#spanNOD").html(tDays + " Days ");
        else
            $("#spanNOD").html("");
    };
    this.setFormValues = function () {
        window.checkPage.showRoomAvailablity();

    };
    this.PopulateProperty = function () {
        //alert('in prop');
        $("#ddlProperty").attr("disabled", "disabled");
        $("#ddlFrom").attr("disabled", "disabled");
        $("#ddlTo").attr("disabled", "disabled");
        if ($('#ddlLocation').val() == "") {
            $('#ddlProperty').empty().append('<option selected="selected" value="0">Please select</option>');
            $('#ddlFrom').empty().append('<option selected="selected" value="0">Please select</option>');
            $('#ddlTo').empty().append('<option selected="selected" value="0">Please select</option>');
        }
        else {
            $('#ddlProperty').empty().append('<option selected="selected" value="0">Loading...</option>');
            $.ajax({
                type: "POST",
                url: pageUrl + '/populateProperty',
                data: '{LocationID: ' + $('#ddlLocation').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnPropertyPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };

    this.PopulateTariff = function () {
        //alert($("#txtBookingDate").val().trim());
        if ($("#txtBookingDate").val().trim() != "") {
            //alert('in tariff');
            $("#ddlFrom").attr("disabled", "disabled");
            $("#ddlTo").attr("disabled", "disabled");
            if ($('#ddlProperty').val() == "") {
                $('#ddlFrom').empty().append('<option selected="selected" value="0">Please select</option>');
                $('#ddlTo').empty().append('<option selected="selected" value="0">Please select</option>');

            }
            else {
                $('#ddlFrom').empty().append('<option selected="selected" value="0">Loading...</option>');
                $('#ddlTo').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/populateTariffRange',
                    data: "{BookingDate: '" + $('#txtBookingDate').val() + "',LocationID: " + $('#ddlLocation').val() + ",PropertyID: " + $('#ddlProperty').val() + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: A.OnTariffPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }
    };

    this.OnTariffPopulated = function (response) {
        //alert('in success');
        window.checkPage.PopulateControl(response.d, $("#ddlFrom"));
        window.checkPage.PopulateControl(response.d, $("#ddlTo"));
    };

    this.OnPropertyPopulated = function (response) {
        window.checkPage.PopulateControl(response.d, $("#ddlProperty"));
    };
};
WBFDC.CheckPage = function () {
    var A = this;
    this.enquiryForm;
    this.init = function () {
        //this.enquiryForm = JSON.parse($("#enquiryJSON").val());
        //alert(JSON.stringify(this.enquiryForm));
        this.masterInfo = new WBFDC.MasterInfo();
        this.masterInfo.init();
    };
    this.showRoomAvailablity = function (tot) {
        $(".loading-overlay").show();
        var C = "{dateFrom:'" + $("#txtBookingDate").val() + "',dateTo:'" + $("#txtBookingDateTo").val() +
                "',propertyID:" + $("#ddlProperty").val() + ",locationID:" + $("#ddlLocation").val() +
                ",RangeFrom:" + $("#ddlFrom").val() + ",RangeTo:" + $("#ddlTo").val() + ",RangeOrderPreference:'" +
                (($("#rdAsc").is(":checked")) ? $("#rdAsc").val() : $("#rdDesc").val()) + "'}"
        $.ajax({
            type: "POST",
            url: pageUrl + '/ShowAvaliablity',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                //window.checkPage.enquiryForm = JSON.parse(D.d);
                //alert(JSON.parse(D.d));
                $("#divroomTable").html(JSON.parse(D.d));
                $(".loading-overlay").hide();
                //callfixtable();
                A.getOtherValue(tot);
                $("#loadBookingDet").show();
                //A.PageRedirection();
            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.getOtherValue = function (tot) {
        var otherHtml = '';
        ////        var B = window.checkPage.enquiryForm;
        ////        var totBooking = $("#TotalBookingNo").val();
        ////        var totAdded = B.enquiryMaster.TotalNoOfRooms;
        ////        var tot = (parseInt(totBooking)) - parseInt(totAdded);
        otherHtml += " Showing Available Room(s) of " + $("#ddlLocation option:selected").text().trim();
        otherHtml += "<div style='font-size:11px;color:maroon;float:right;margin-right:20px;'><img src='images/check.png' /> &nbsp;Available &nbsp;<img src='images/cross.png' />&nbsp;Booked  </div>"
        $("#divCaption").html(otherHtml);
    };
    
    

//    this.PageRedirection = function () {
//        var B = window.checkPage.enquiryForm;
//        if (B.SMSResult == "SUCCESS") {
//            //alert("Success");
//            window.location.href = "EnquiryComplete.aspx?EnquiryTokenID=" + B.enquiryMaster.EnquiryTokenID;
//        }
//    };
    this.PopulateControl = function (list, control) {
        //alert(JSON.stringify(list));
        if (list.length > 0) {
            control.removeAttr("disabled");
            control.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(list, function () {
                control.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
        else {
            control.empty().append('<option selected="selected" value="-1">Not available<option>');
        }
    };
    

    this.findAndRemove = function (array, property, value) {
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;

            }
        });
    };

    this.findAndRemove2 = function (array, property, value, property2, value2) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    this.CloseContent = function () {
        $("#loadBookingDet").hide();        
        return false;
    };
    
    
    
};