﻿

/// <reference path="jquery-1.9.0.js" />

/// <reference path="utility.js" />
/// <reference path="json2.js" />
/// <reference path="jquery.maskedinput.js" />
var vchslr = 0;
var curcellGL;
var curCellSUB;
var Modifyflg = 'Add';
var ModifyflgInst = 'Add';
var glvrno = '';

var lblGlID = '';
var lblGlType = '';
var lblSlID = '';
var lblSubID = '';

$(function () {
    /*
     * this swallows backspace keys on any non-input element.
     * stops backspace -> back
     */
    var rx = /INPUT|SELECT|TEXTAREA/i;

    $(document).bind("keydown keypress", function (e) {
        if (e.which == 8) { // 8 == backspace
            if (!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly) {
                e.preventDefault();
            }
        }
    });
});
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

lpad = function (padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

function leftpad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
function CheckVoucherDate() {
    var yearMonth = $("#txtYearMonth").val();
    //var get_Month = parseInt(yearMonth.substring(4, 6));
    //var get_Year = parseInt(yearMonth.substring(0, 4));
    var dateVal = $("#txtVoucherDate").val();
    var month = (dateVal.substring(3, 5));
    var year = (dateVal.substring(6, 10));
    ///alert(month);
    //alert(year);
    var yearmon = (year + month).toString();
    //if ((get_Month != month && get_Year == year) || (get_Month == month && get_Year != year) || (get_Month != month && get_Year != year)) {
    if (yearMonth != yearmon) {
        //$("#txtVoucherDate").val('01/' + yearMonth.substring(4, 6) + '/' + yearMonth.substring(0, 4));
        //alert(dateVal);
        //alert(" Can not change. Select a date between given YearMonth (" + yearMonth + ")");
        return false;
    }
    return true;
}


$(document).ready(function () {

    //$("#txtInstDate").keydown(function (evt) {
    //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
    //        //alert("Please Enter Key Press Or Tab Key Press For Display GL ID");
    //        return false;
    //    }
    //});

    $("form").bind("keypress", function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });
    
  
    //$("#txtVoucherDate").change(function () {
    //    var yearMonth = $("#txtYearMonth").val();
    //    var get_Month = parseInt(yearMonth.substring(4, 6));
    //    var get_Year = parseInt(yearMonth.substring(0, 4));
    //    var dateVal = $("#txtVoucherDate").val();
    //    var month = parseInt(dateVal.substring(3, 5));
    //    var year = parseInt(dateVal.substring(6, 10));
    //    if ((get_Month != month && get_Year == year) || (get_Month == month && get_Year != year) || (get_Month != month && get_Year != year)) {
    //        $("#txtVoucherDate").val('01/' + yearMonth.substring(4, 6) + '/' + yearMonth.substring(0, 4));
    //        //alert(dateVal);
    //        alert(" Can not change. Select a date between given YearMonth (" + yearMonth+")");
    //        return false;
    //    }

    //    //alert(get_Month + '--' + get_Year + '--' + month + '--' + year + " Check");
    //});
    
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    var currentTime = new Date();
    // First Date Of the month 
    var startDateFrom = new Date(currentTime.getFullYear(), currentTime.getMonth(), 1);
    // Last Date Of the Month 
    var startDateTo = new Date(currentTime.getFullYear(), currentTime.getMonth() + 1, 0);
   
    //$('#txtVoucherDate').datepicker({
    //    autoOpen: false,
    //    showOtherMonths: true,
    //    selectOtherMonths: true,
    //    closeText: 'X',
    //    showAnim: 'drop',
    //    showButtonPanel: true,
    //    duration: 'slow',
    //    dateFormat: 'dd/mm/yy',
    //    //maxDate: 'now',
    //    minDate: startDateFrom,
    //    maxDate: startDateTo
    //    , onSelect: function (event) {
    //        //this.focus();
    //        //$("#txtInstDate").datepicker("option", "maxDate", event);
    //        if ($('#txtVoucherDate').valid()) {
    //            $('#txtVoucherDate').datepicker("hide");
    //            $("#lblAccDesc_0").focus();
    //        } else {
    //            $('#txtVoucherDate').focus();
    //        }
            
            
    //    }
         

    //}); //Code Hide Date 01/09/2017 by shrawan 


    //$("#txtVoucherDate").on("blur", function (e) { $(this).off("focus").datepicker("hide"); });
    
    //$("#txtVoucherDate ~ .ui-datepicker").hide();

    //start Coding Hide
    //$('#txtInstDate').datepicker({
    //    autoOpen: false,
    //    showOtherMonths: true,
    //    selectOtherMonths: true,
    //    closeText: 'X',
    //    showAnim: 'drop',
    //    showButtonPanel: true,
    //    duration: 'slow',
    //    dateFormat: 'dd/mm/yy',
        
    //    onSelect: function (event) {
    //        $('#txtInstDate').datepicker("hide");
    //        $("#txtInstAmount").focus();
    //        $("#txtInstAmount").select();
    //    }

    //});
    //End Coding Hide

    $('#txtInstDate').mask("99/99/9999", { placeholder: "_" });
    $('#txtVoucherDate').mask("99/99/9999", { placeholder: "_" });
    $("#btnDatePicker").click(function () {
        $('#txtVoucherDate').datepicker("show");
        return false;
    });
   

    window.voucherInfo = new WBFC.VoucherInfo();
    window.voucherInfo.init();

    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });

});
$.validator.addMethod("numeric", function (value, element, param) {
    if (param) {
        return this.optional(element) || value == value.match(/^[0-9]+$/);
    }
    return true;
    // --                                    or leave a space here ^^
}, "No wild card characters like - ,*,?,! is allowed. Entry should be numeric only , i.e within 0-9");
$.validator.addMethod("vchcheck", function (value, element) {
    //var $min = $(param);

    //if (this.settings.onfocusout) {
    //    $min.off(".validate-vchcheck").on("blur.validate-vchcheck", function () {
    //        $(element).valid();
    //    });
    //}
    var validator = this;
    //alert(parseInt($min.val()) + 2);
    if (value != "") {
        
        return CheckVoucherDate();
        
    }
   
    return true;
}, "Voucher Date is invalid");

WBFC.VoucherMaster = function () {
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {
        $("#txtDebit_0").prop("disabled",true);
        $("#txtCredit_0").prop("disabled",true);

        this.validator = A.getValidator();
        $("#txtYearMonth").val('');
        
        $("#txtYearMonth").focus();
        $("#txtYearMonth").blur(A.YearMonthChanged());
        $("#txtYearMonth").keypress(A.YearMonKeyPress);
        $("#ddlVchType").keypress(A.VchTypeKeyPress);
                
        $("#txtVoucherDate").keypress(A.VchDateKeyPress);
        $("#lblAccDesc_0").keypress(A.GLSLAccKeyPress);

        $("#txtVoucherNo").keypress(A.VoucherKeyPress);

        $("#txtGLSLDescSearch").keyup(A.TxtGlSlKeyUP);
        $("#txtGLSLDescSearch").keypress(A.TxtGlSlKeyPress);
        $("#txtGLSLDescSearch").keydown(A.TxtGlSlKeyDown);
        $("[id*=grdGLSL]").bind("keydown", A.GrdGLSLKeyDown);
        $("[id*=grdGLSL]").bind("keypress", A.TxtGlSlKeyPress);
        $("#rdName").click(A.GlRadioClicked);
        $("#rdCode").click(A.GlRadioClicked);
        $("#txtSUBDescSearch").keyup(A.TxtSubKeyUP);
        $("#txtSUBDescSearch").keypress(A.TxtSubKeyPress);
        $("#txtSUBDescSearch").keydown(A.TxtSubKeyDown);

        $("[id*=grdSUB]").bind("keydown", A.GrdSubKeyDown);
        $("[id*=grdSUB]").bind("keypress", A.TxtSubKeyPress);
        $("#rdSName").click(A.SubRadioClicked);
        $("#rdSCode").click(A.SubRadioClicked);
        $("#lblSubAccDesc_0").keypress(A.SUBAccKeyPress);

        $("#ddlDRCR").change(A.DRCRChanged);
        
        $("#ddlDRCR").keypress(A.DRCRKeyDown);
        $("#txtDebit_0").keypress(A.DebitKeyDown);
        $("#txtCredit_0").keypress(A.DebitKeyDown);

        $("#ddlInstType").change(A.CallInstypeBlur);
        $("#ddlInstType").keypress(A.InstTypeKeyPress);

        $("#ddlInstType").keypress(A.ddlInstTypeKeyPress);
        $("#ddlInstType").keydown(A.ddlInstTypekeydown);
        
        $("#txtInstDate").keypress(A.KeypressSetInstDate);
        $("#txtInstDate").keydown(A.KeyDownSetInstDate);
        //$("#txtInstDate").click(A.SetInstDate);
        
        
        $("#btnInstAdd").click(A.InstrumentAdd);
        A.SetTabIndexInstType();
        A.SetTabIndexFA();
        

        
        //$("#txtSlipNo").keypress(A.SlipNoKeyPress);
        //$("#txtInstNo").keypress(A.InstNoKeyPress);
        //$("#txtInstDate").keypress(A.InstDateKeyPress);
        $("#txtInstAmount").keypress(A.InstAmountKeyPress);
        //$("#txtDBankBR").keypress(A.DBankBRKeyPress);
        $("#txtDBankBR").keydown(A.DBankBRKeyDown);
        

        $("#txtFAValue").keydown(A.FAValueKeyDown);

        $("#cmdSave").click(A.SaveClicked);
        $("#btnSearchVch").click(A.GetSearchResult);
        $("#btnSearchVch").keydown(A.SearchKeyDown);
        $("#cmdDelete").click(A.GetDeleteVoucher);
        $("#cmdRefresh").click(A.RefreshClicked);
        $("#cmdUpdate").click(A.UpdateClicked);
        $("#ddlVchType").val(0);
        $("#btnRep").click(A.ShowReport);
        $("#btnShowReport").click(A.CallModalConfirmReport);
        //$('div#dialog-report').on('dialogclose', A.ClosePopReport);
        A.SetYearMonth();

    };

    //$("#txtInstDate").keydown(function (evt) {
    //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
    //        alert("Type Not Allow ! Please Select Date");
    //        return false;
    //    }
    //});

    function SetVoucherDate()
    { 
        $('#txtInstDate').datepicker({
            autoOpen: false,
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            showButtonPanel: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy',

            onSelect: function (event) {
                $('#txtInstDate').datepicker("hide");
                checkInstDtless90days();
                return false;
            }

        });
        $("#txtInstDate").datepicker("option", "maxDate", $('#txtVoucherDate').val())
    }

    this.GlRadioClicked = function () {
        if ($(this).val() == "N") {
            $("#GLSpan").html("Search By Description");
            $("#txtGLSLDescSearch").val("");
            A.BindGridGLSL($('#txtGLSLDescSearch').val());
        } else if ($(this).val() == "C") {
            $("#GLSpan").html("Search By ID");
            $("#txtGLSLDescSearch").val("");
            A.BindGridGLSL($('#txtGLSLDescSearch').val());
        }
        
    };
    this.SubRadioClicked = function () {
        if ($(this).val() == "N") {
            $("#SubSpan").html("Search By Description");
            $("#txtSUBDescSearch").val("");
            A.BindGridSub($('#txtSUBDescSearch').val());
        } else if ($(this).val() == "C") {
            $("#SubSpan").html("Search By ID");
            $("#txtSUBDescSearch").val("");
            A.BindGridSub($('#txtSUBDescSearch').val());
        }

    };

    this.DRCRKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            A.DRCRChanged();
        }
    }
    this.ShowModalPopupFA = function () {
        $("#dialogFA").dialog({
            title: "Fixed Asset Entry",
            width: 980,

            buttons: {
                Ok: function () {
                    //A.InstrumentAdd();
                    // A.CallculateInstAmount();
                }
            },
            modal: true
        });
    };

    this.FAValueKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            A.FAAdd();
            return false;

        } else {
            return true;
        }
        return false;

    };
    this.FAAdd = function () {

        if ($.trim($("#txtFAD").val()) == "") {
            alert("Please enter Fixed Asset Description");
            $("#txtFAD").focus();
            return false;
        }

        if ($.trim($("#ddlLocation").val()) == "") {
            alert("Please Select Location ");
            $("#ddlLocation").focus();
            return false;
        }

        if ($.trim($("#ddlSupplier").val()) == "") {
            alert("Please Select Supplier ");
            $("#ddlSupplier").focus();
            return false;
        }

        if ($.trim($("#txtDRate").val()) == "") {
            alert("Please enter depreciation rate ");
            $("#txtDRate").focus();
            return false;
        }

        if ($.trim($("#txtFAValue").val()) == "" || parseFloat($.trim($("#txtFAValue").val())) <= 0) {
            alert("Please enter asset value ");
            $("#txtFAValue").focus();
            return false;
        }

        var B = window.voucherInfo.voucherForm;
        var SectorID = B.VoucherMast.SectorID;
        var C = WBFC.Utils.fixArray(B.tmpVchFA);

        var faid = C.length + 1;
        C.push({
            FA_ID: faid,
            GL_ID: lblGlID,//$("#lblGlID").val(),
            FA_DESC: $("#txtFAD").val(),
            VENDOR_CODE: $("#ddlSupplier").val(),
            VENDOR_NAME: $("#ddlSupplier option:selected").text(),
            DEP_RATE: $("#txtDRate").val(),
            DEP_METHOD: 'S',
            PUR_VALUE: ($("#ddlDRCR").val() == "D" ? $("#txtFAValue").val() : 0),
            PUR_VCH_NO: 0,
            PUR_VCH_DATE: ($("#ddlDRCR").val() == "D" ? $("#txtVoucherDate").val() : ""),
            VCH_SECTORID: SectorID,//$("#ddlLocation").val(),            
            SOL_VALUE: ($("#ddlDRCR").val() == "C" ? $("#txtFAValue").val() : 0),
            SOL_VCH_NO: 0,
            SOL_VCH_DATE: ($("#ddlDRCR").val() == "C" ? $("#txtVoucherDate").val() : ""),            
            SectorID: $("#ddlLocation").val(),
            SECTORNAME: $("#ddlLocation option:selected").text(),
        });

        //alert(JSON.stringify(C));
        A.ShowFADetail(C);
        A.ClearFAText();
        A.CallModalConfirmFA();
    };
    this.ShowFADetail = function (C) {
        var html = ""
        if (C.length > 0) {

            for (var i in C) {

                html += "<tr id='trFA_" + C[i].FA_ID + "'>"
                    + "<td id='tdFA_" + C[i].FA_ID + "'><div><a href='" + pageUrl + '/DeleteFADetail' + "' onClick='return window.voucherInfo.masterInfo.removeFADetail(" + C[i].FA_ID + ");'><img src='images/delete.png' id='deleteFAID_" + C[i].FA_ID + "' /></a></div></td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + C[i].FA_DESC + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + C[i].SECTORNAME + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].VENDOR_NAME) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].DEP_RATE) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;text-align:right;'>" + (($("#ddlDRCR").val() == "D" ? C[i].PUR_VALUE : C[i].SOL_VALUE)) + "</td>";
                   

                html + "</tr>";
            }
        }


        $(".tblFA").find("tr:gt(1)").remove();
        $(".tblFA").append(html);
    };
    this.removeFADetail = function (FA_ID) {
        //alert("in fa delete");
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#trFA_" + FA_ID;
        $(removeElemTr).append(ajaxLoader);
        var B = {};
        B = window.voucherInfo.voucherForm;

        window.voucherInfo.findAndRemove(B.tmpVchFA, 'FA_ID', FA_ID);
        A.ShowFADetail(B.tmpVchFA);
        $("#txtFAD").focus();
        return false;
    };
    this.ClearFAText = function () {
        $("#txtFAD").val("");
        $("#ddlLocation").val("");
        $("#ddlSupplier").val("");
        $("#txtDRate").val("")
        $("#txtFAValue").val("")
        

    };

    this.CallModalConfirmFA = function () {
        $("#spanTxt").html("Do you want to add any more fixed asset?");
        $("#dialog-confirm").dialog({
            title: "Add More Fixed Asset?",
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                "Yes": function () {

                    $(this).dialog("close");
                    $("#txtFAD").focus();
                },
                "No": function () {

                    $(this).dialog("close");
                    A.CallculateFAAmount();

                }
            },
            open: function() {
            $(this).siblings('.ui-dialog-buttonpane').find('button:eq(1)').focus(); 
        }
        });
    };
    this.CallculateFAAmount = function () {
        var B = window.voucherInfo.voucherForm;
        var SectorID = B.VoucherMast.SectorID;
        var C = WBFC.Utils.fixArray(B.tmpVchFA);
        var FAAmount = 0;
        for (var i in C) {
            FAAmount = parseFloat(FAAmount) + parseFloat(C[i].PUR_VALUE) + parseFloat(C[i].SOL_VALUE).toFixed(2);
        }
        $("#dialogFA").dialog('close');
        $("#txtCredit_0").val("");
        $("#txtDebit_0").val("");
        if ($("#ddlDRCR").val() == "D") {

            $("#txtCredit_0").prop("disabled", true);
            //$("#txtDebit_0").prop("disabled", false).val(FAAmount);
            $("#txtDebit_0").prop("disabled", false).val(FAAmount);
            $("#txtDebit_0").focus();
            $("#txtDebit_0").select();
        } else if ($("#ddlDRCR").val() == "C") {

            $("#txtDebit_0").prop("disabled", true);
            $("#txtCredit_0").prop("disabled", false).val(FAAmount);
            $("#txtCredit_0").focus();
        }
    };

    this.RefreshClicked = function () {
        location.href = "ACC_VCHMaster.aspx";
    };
    this.SetTabIndexInstType = function () {
        $(".hori").keypress(function (event) {
            if (event.keyCode == 13) {
                textboxes = $("input.hori");
                debugger;
                currentBoxNumber = textboxes.index(this);
                if (textboxes[currentBoxNumber + 1] != null) {
                    nextBox = textboxes[currentBoxNumber + 1]
                    nextBox.focus();
                    nextBox.select();
                    event.preventDefault();
                    return false
                }
            }
        });
    };
    this.SetTabIndexFA = function () {
        $(".horiFA").keypress(function (event) {
            if (event.keyCode == 13) {
                textboxes = $("input.horiFA,select.horiFA");
                debugger;
                currentBoxNumber = textboxes.index(this);
                if (textboxes[currentBoxNumber + 1] != null) {
                    nextBox = textboxes[currentBoxNumber + 1]
                    nextBox.focus();
                    nextBox.select();
                    event.preventDefault();
                    return false
                }
            }
        });
    };
    this.SlipNoKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
            //alert($(this).next('input').attr("id"));
            $(this).next('input').focus();
        }
        return false;
    };
    this.InstNoKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {

            $(this).next('input').focus();
        }
        return false;
    };
    this.InstDateKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {

            $(this).next('input').focus();
        }
        return false;
    };
    this.InstAmountKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (!isNumberKey(evt)) {
            alert("Please enter number key");
            return false;
        } else {
            return true;
        }
        
    };
    this.DBankBRKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {

            //$(this).next('input').focus();
        }
        return false;
    };
    this.YearMonKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            if ($("#txtYearMonth").val() != "") {
                $("#ddlVchType").focus();
                return false;
            } else {
                alert("Please enter year month");
                $("#txtYearMonth").focus();
                return false;
            }
            
        }
    };
    this.VchTypeKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            if ($("#ddlVchType").val() != "") {
                $("#txtVoucherDate").focus();
                GetMaxSchemeID();
                return false;
            } else {
                alert("Please Select Voucher Type");
                $("#ddlVchType").focus();
                return false;
            }

        }
    };
    this.VchDateKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            if ($("#txtVoucherDate").val() != "") {
                if (CheckVoucherDate()) {
                    $("#lblAccDesc_0").focus();
                    return false;
                } else {
                    return false;
                }
                
            } else {
                alert("Please Enter Voucher Date");
                $("#txtVoucherDate").focus();
                return false;
            }

        }
        if (iKeyCode == 9) {
            if ($("#txtVoucherDate").val() != "") {
                if (CheckVoucherDate()) {
                    $("#lblAccDesc_0").focus();
                    return false;
                } else {
                    return false;
                }
            } else {
                alert("Please Enter Voucher Date");
                $("#txtVoucherDate").focus();
                return false;
            }

        }
    };
    this.VoucherKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            if ($("#txtVoucherNo").val() != "") {
                $("#btnSearchVch").focus();
                return false;
            } else {
                alert("Please enter voucher no");
                $("#txtVoucherNo").focus();
                return false;
            }

        }
    };
    
    this.DBankBRKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            A.InstrumentAdd();
            return false;

        }else{
            return true;
        }
        //return false;
    };

    this.InstrumentAdd = function () {
       
        if ($("#ddlInstType").val() == "") {
            alert("Please Select Instrument Type");
            $("#ddlInstType").focus();
            return false;
        }

        //if ($("#txtInstNo").val() == "" && ($("#lblGlType").val() == 'A' || $("#lblGlType").val() == 'B') && ($("#ddlInstType").val() != "N")) {
        if ($("#txtInstNo").val() == "" && (lblGlType == 'A' || lblGlType == 'B') && ($("#ddlInstType").val() != "N")) {
            //alert($("#lblGlType").val());
            alert(lblGlType);
            alert("Please enter instrument number");
            $("#txtInstNo").focus();
            return false;
        }

        if ($("#txtInstAmount").val() == "" || $("#txtInstAmount").val() <= 0) {
            alert("Please Enter Amount!");
            $("#txtInstAmount").focus();
            return false;
        }

        //if ($("#txtDBankBR").val() == "" && ($("#lblGlType").val() == 'A' || $("#lblGlType").val() == 'B')) {
        if ($("#txtDBankBR").val() == "" && (lblGlType == 'A' || lblGlType == 'B')) {
            alert("Please enter drawee bank or branch");
            $("#txtDBankBR").focus();
            return false;
        }

        // ========= Start Code For Less 90 Days check instrument Date from voucher Date ========

        var VDate = $('#txtVoucherDate').val().split("/");
        var StrVchDate = VDate[2] + "-" + VDate[1] + "-" + VDate[0];
        var NewVchDate = new Date(StrVchDate);
        var LessInstdate = NewVchDate.setDate(NewVchDate.getDate() + parseInt(-90));
        var DtLessInstdate = new Date(LessInstdate); //compare les 90 days
        var SetInstDate = $('#txtInstDate').val().split("/");
        var CurrInstDate = SetInstDate[2] + "-" + SetInstDate[1] + "-" + SetInstDate[0];
        var DtCurrInstDate = new Date(CurrInstDate); //Compare Current Inst Date
        if (DtCurrInstDate < DtLessInstdate) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Instrument Date Exced 90 Days's! Will You Proceed Further (Y/N)?")) {
                confirm_value.value = "Yes";

            } else {
                confirm_value.value = "No";
            }

            if (confirm_value.value == "Yes") {
                $('#txtInstAmount').focus();
                $('#txtInstAmount').select();
            }
            if (confirm_value.value == "No") {
                $('#txtInstDate').val('');
                $('#txtInstDate').focus();
                return false;
            }
        } // Main If Close

        // ========= End Code For Less 90 Days check instrument Date from voucher Date ========



        var B = window.voucherInfo.voucherForm;
        var SectorID = B.VoucherMast.SectorID;
        var C = WBFC.Utils.fixArray(B.tmpVchInstType);

        var instsrno = C.length + 1;
        C.push({
            InstSrNo: instsrno,
            YearMonth: $("#txtYearMonth").val(),
            VoucherType: $("#ddlVchType").val(),
            VCHNo: 0,
            GLID: lblGlID,//$("#lblGlID").val(),
            SLID: lblSlID,//$("#lblSlID").val(),
            DRCR: $("#ddlDRCR").val(),
            SlipNo: $("#txtSlipNo").val(),
            InstType: $("#ddlInstType").val(),
            InstTypeName: $("#ddlInstType option:selected").text(),
            InstNo: $("#txtInstNo").val(),
            InstDT: $("#txtInstDate").val(),
            Amount: $("#txtInstAmount").val(),
            ActualAmount: $("#txtInstAmount").val(),
            Drawee: $("#txtDBankBR").val().toString().toUpperCase(),
            SectorID: SectorID
        });

        //alert(JSON.stringify(C));

        A.ShowInstDetail(C);
        A.ClearInstText();
        A.CallModalConfirm();
        ModifyflgInst = 'Add';
    }
    this.CallModalConfirm = function () {
        $("#spanTxt").html("Do you want to add any more instrument type?");
        $("#dialog-confirm").dialog({
            title: "Add More Instrument?",
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                "Yes": function () {
                    
                    $(this).dialog("close");
                    $("#ddlInstType").focus();
                    $("#txtInstAmount").val($("#txtInstAmount").val()); 
                   
                    if ($("#ddlDRCR").val() == "D") {
                        var B = window.voucherInfo.voucherForm;
                        var C = WBFC.Utils.fixArray(B.tmpVchInstType);
                        var InstAmount = 0;
                        for (var i in C) {
                            InstAmount = (parseFloat(InstAmount) + parseFloat(C[i].Amount)).toFixed(2);
                        }
                        var TotalDebt = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //yes
                        var TotalCret = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
                        var BalanceAmount = TotalCret - TotalDebt - InstAmount;
                        if (BalanceAmount <= 0) {
                            $("#txtInstAmount").val(0);
                        }
                        else {
                            $("#txtInstAmount").val(BalanceAmount);
                        }
                       
                    } else if ($("#ddlDRCR").val() == "C") {

                        var B = window.voucherInfo.voucherForm;
                        var C = WBFC.Utils.fixArray(B.tmpVchInstType);
                        var InstAmount = 0;
                        for (var i in C) {
                            InstAmount = (parseFloat(InstAmount) + parseFloat(C[i].Amount)).toFixed(2);
                        }
                        
                        var TotalDebt1 = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //sm
                        var TotalCret1 = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
                        var BalanceAmount1 = TotalDebt1 - TotalCret1 - InstAmount;
                        if (BalanceAmount1 <= 0) {
                            $("#txtInstAmount").val(0);
                        }
                        else {
                            $("#txtInstAmount").val(BalanceAmount1);
                        }
                    }
                    
                },
                "No": function () {
                   
                    $(this).dialog("close");
                    A.CallculateInstAmount();

                }
            },
            open: function() {
            $(this).siblings('.ui-dialog-buttonpane').find('button:eq(1)').focus(); 
        }
        });
    };
    this.ClearInstText = function () {
        $("#txtSlipNo").val("");
        $("#ddlInstType").val("");        
        $("#txtInstNo").val("");
        $("#txtInstDate").val("")
        $("#txtInstAmount").val("")        
        $("#txtDBankBR").val("");

    };
    this.DebitKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            if ($("#ddlDRCR").val() == "") {
                alert("Please Select Debit or Credit First");
                return false;
            }
            if ($("#txtDebit_0").val() != "" || $("#txtCredit_0").val() != "") {
                if ($("#ddlDRCR").val() == "D" && parseFloat($.trim($("#txtDebit_0").val())) == 0) {
                    alert("Amount Must be greater than 0");
                    return false;
                }

                if ($("#ddlDRCR").val() == "C" && parseFloat($.trim($("#txtCredit_0").val())) == 0) {
                    alert("Amount Must be greater than 0");
                    return false;
                }

                var B = window.voucherInfo.voucherForm;
                var SectorID =B.VoucherMast.SectorID;
                var C = WBFC.Utils.fixArray(B.VoucherDetail);
                var D = WBFC.Utils.fixArray(B.tmpVchInstType);
                var E = WBFC.Utils.fixArray(B.tmpVchFA);
                if (!A.CheckValidDRCR(C, $("#ddlDRCR").val())) {
                    alert("Multiple Debit Against multple credit not allowed and vice versa");
                    return false;
                }
                //if ($("#lblGlType").val() == "A" || $("#lblGlType").val() == "B") { //|| $("#lblGlType").val() == "C" code hide by Asish & Mantu sir 25-08-17
                if (lblGlType == "A" || lblGlType == "B") { //|| $("#lblGlType").val() == "C" code hide by Asish & Mantu sir 25-08-17
                    if (D == "") {
                        alert("Instrument is required when Bank/Branch"); //alert("Instrument is required when Bank/Branch/ Cash is Selected");
                        A.ShowModalPopupInstType();
                        //alert($("#ddlInstType").val());
                        $("#ddlInstType").val("C");
                        return false;
                    }   
                    var AMOUNT = ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val());
                    var AMOUNTType = ($("#ddlDRCR").val() == "D" ? "Debit Amount" : "Credit Amount");
                   var instamt = parseFloat(0);
                    for (var k in D) {
                        instamt += parseFloat(D[k].Amount);
                    }
                    //alert(AMOUNT);
                    //alert(instamt);
                    if (parseFloat(AMOUNT) != parseFloat(instamt.toFixed(2))) {
                        alert("Total Amount of Instrument and " + AMOUNTType + " mus be equal.");
                        A.ShowModalPopupInstType();
                        alert($("#ddlInstType").val());
                        $("#ddlInstType").val("C");
                        return;
                    }
                }
                if (lblGlType == "F") {//$("#lblGlType").val()
                    if (E == "") {
                        alert("Fixed Asset Entry is required when Account Code Type if Fixed Asset");
                        A.ShowModalPopupFA();
                        return false;
                    }  
                }
                var found = false;
               
                if (Modifyflg == 'Add') {
                    vchslr = C.length + 1;
                }
                else {
                    vchslr = glvrno;

                }
                 

                if (C == "" ) {
                    C.push({
                        YearMonth: $("#txtYearMonth").val(),
                        VoucherType: $("#ddlVchType").val(),
                        VCHNo: 0,
                        OLDSLID: $("#lblAccOldSlCode").val(),
                        GLName: $("#lblAccDesc_0").val(),
                        VchSrl: vchslr,
                        GlType:lblGlType,//$("#lblGlType").val(),
                        GLID: lblGlID,//$("#lblGlID").val(),
                        SLID: lblSlID,//$("#lblSlID").val(),
                        SubID: lblSubID == "" ? '0000000000' : lblSubID, //($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val()),
                        OLDSUBID: ($("#lblOldSubCode").val() == "" ? '' : $("#lblOldSubCode").val()),
                        SUBDesc: ($("#lblSubAccDesc_0").val() == "" ? '' : $("#lblSubAccDesc_0").val()),
                        DRCR: $("#ddlDRCR").val(),
                        AMOUNT: ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val()),
                        SectorID: SectorID,
                        VchInstType: D,
                        VchFA:E
                    });
                } else {
                    for (var i in C) { //$("#lblGlID").val() //$("#lblSlID").val()
                        //if (C[i].SLID == lblSlID && C[i].GLID == lblGlID && C[i].SubID == ($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val())) {
                        if (C[i].SLID == lblSlID && C[i].GLID == lblGlID && C[i].SubID == (lblSubID == "" ? '0000000000' : lblSubID)) {
                            alert("Account Code Already Entered. Select Another Account Code.");
                            found = true;
                        }
                    }
                    if (!found) {
                        C.push({
                            YearMonth: $("#txtYearMonth").val(),
                            VoucherType: $("#ddlVchType").val(),
                            VCHNo: 0,
                            OLDSLID: $("#lblAccOldSlCode").val(),
                            GLName: $("#lblAccDesc_0").val(),
                            VchSrl: vchslr,
                            GlType:lblGlType,//$("#lblGlType").val(),
                            GLID: lblGlID,//$("#lblGlID").val(),
                            SLID: lblSlID,//$("#lblSlID").val(),
                            SubID: lblSubID == "" ? '0000000000' : lblSubID, //($("#lblSubID").val() == "" ? '0000000000' : $("#lblSubID").val()),
                            OLDSUBID: ($("#lblOldSubCode").val() == "" ? '' : $("#lblOldSubCode").val()),
                            SUBDesc: ($("#lblSubAccDesc_0").val() == "" ? '' : $("#lblSubAccDesc_0").val()),
                            DRCR: $("#ddlDRCR").val(),
                            AMOUNT: ($("#ddlDRCR").val() == "D" ? $("#txtDebit_0").val() : $("#txtCredit_0").val()),
                            SectorID: SectorID,
                            VchInstType: D,
                            VchFA: E
                        });
                    }
                }
                D = [];
                E = [];
                B.tmpVchFA = [];
                B.tmpVchInstType = [];
                //alert(JSON.stringify(E));
                //alert(JSON.stringify(B.tmpVchFA));
                //alert(JSON.stringify(D));
                //alert(JSON.stringify(B.tmpVchInstType));
                //alert(JSON.stringify(C));
                A.ClearText();
                A.ShowVoucherDetail(C, "I");
                Modifyflg = 'Add';
                return false;
            } else {
                alert("Please enter " + ($("#ddlDRCR").val()=="D"?"Debit":"Credit") + " amount.");
                return false;
            }
        }else{
            return true; Modifyflg = 'Add';
        }
        Modifyflg = 'Add';
        return false;
    }
    this.CheckValidDRCR = function (C, DRCR) {
        var checkstatus = true;
        var countDR = 0;
        var countCR = 0;
        //alert(JSON.stringify(C));
        //alert(DRCR);
        for (var i in C) {
            if (C[i].DRCR == "D") {
                countDR++;
            }
            if (C[i].DRCR == "C") {
                countCR++;
            }
        }

        //alert("DRP " + countDR + " CRP " + countCR);
        if (DRCR == "D") {
            countDR++;
        } else if (DRCR == "C") {
            countCR++;
        }

        //alert("DR " + countDR + " CR " + countCR);
        if ((countDR == 1 && countCR == 1) || (countDR == 0 && countCR >= 0) || (countDR >= 0 && countCR == 0)) {
            checkstatus = true;
        }
        if (countDR > 1 && countCR > 1) {
            checkstatus = false;
        }

        return checkstatus;
    };
    this.ClearText = function () {
        
        $("#lblAccOldSlCode").val("");
        $("#lblAccDesc_0").val("");        
        //$("#lblGlID").val("");
        lblGlID = '';
        //$("#lblSlID").val("");
        lblSlID = '';
        //$("#lblSubID").val("");
        lblSubID = '';
        $("#lblOldSubCode").val("");
        $("#lblSubAccDesc_0").val("");
        $("#ddlDRCR").val("");
        $("#txtDebit_0").val("");
        $("#txtCredit_0").val("");
        //$("#lblGlType").val("");
        lblGlType = '';
        $(".tblInst").find("tr:gt(1)").remove();
        
        $("#lblAccDesc_0").focus();
    }
    this.ShowVoucherDetail = function (C,Type) {
        var html = ""
        if (C.length > 0) {
           //alert(JSON.stringify(C));
            for (var i in C) {
                //instrument Date Exceed 90-Day's! Will you Proceed Further (Y/N):
                html += "<tr id='tr_" + C[i].VchSrl + "'>"
                    + "<td id='td_" + C[i].VchSrl + "'><div>"
                    + "<a href='" + pageUrl + '/DeleteVoucherDetail' + "' onClick='return window.voucherInfo.masterInfo.removeVoucherDetail(" + C[i].VchSrl + ");'><img src='images/cancelNew.png' alt='Delete Voucher' style='width:32px;height:20px;' title='Delete Voucher' id='deleteVchSID_" + C[i].VchSrl + "' /></a>"
                    //+ "<a href='" + pageUrl + '/EditVoucherDetail' + "' onClick='return window.voucherInfo.masterInfo.editVoucherDetail(this, \"" + C[i].VchSrl + "\");'><img src='images/editNew.png' style='width:32px;height:20px;'  title='Edit Voucher' alt='Edit Voucher' id='editVchSID_" + C[i].VchSrl + "' /></a>"
                    //+ "<a href='" + pageUrl + '/CanEditVoucherDetail' + "' onClick='return window.voucherInfo.masterInfo.caneditVoucherDetail(" + C[i].VchSrl + ");'><img src='images/canedit.png' style='width:32px;height:32px;display:none;'  title='Cancel Edit' alt='Cancel Edit' id='caneditVchSID_" + C[i].VchSrl + "' /></a>"
                    + "</div></td>"
                    + "<td style='width: 15%; border-bottom: solid 1px lightyellow;'>" + C[i].OLDSLID + "<br/>" + (C[i].OLDSUBID=="O"?"":C[i].OLDSUBID) + "</td>"
                    + "<td style='width: 40%; border-bottom: solid 1px lightyellow;'>" + C[i].GLName + "<br/>" + (C[i].OLDSUBID=="O"?"":C[i].SUBDesc) + "</td>"
                    + "<td style='width: 10%; border-bottom: solid 1px lightyellow;'>" + (C[i].DRCR == "D" ? "DR" : "CR") + "</td>"
                    + "<td style='width: 10%; border-bottom: solid 1px lightyellow;text-align:right;'>" + (C[i].DRCR == "D" ? C[i].AMOUNT : "") + "</td>"
                    + "<td style='width: 10%; border-bottom: solid 1px lightyellow;text-align:right;'>" + (C[i].DRCR == "C" ? C[i].AMOUNT : "") + "</td>"

                 + "<td id='td_" + C[i].VchSrl + "'><div>"
                    + "<a href='" + pageUrl + '/EditVoucherDetail' + "' onClick='return window.voucherInfo.masterInfo.editVoucherDetail(this, \"" + C[i].VchSrl + "\");'><img src='images/editNew.png' style='width:30px;height:20px;'  title='Edit Voucher' alt='Edit Voucher' id='editVchSID_" + C[i].VchSrl + "' /></a>"
                    + "</div></td>"

                html + "</tr>"; //EditVoucherDetail
            }            
        }

        A.CalCulateDebitCredit(C,Type);
        $(".test").find("tr:gt(1)").remove();
        $(".test").append(html);
    };
    this.ShowInstDetail = function (C) {
        var html = ""
        //alert(C.length);
        if (C.length > 0) {
           
            for (var i in C) {
               
                html += "<tr id='trInst_" + C[i].InstSrNo + "'>"
                    + "<td id='tdInst_" + C[i].InstSrNo + "' class='labelCaption' style='border:solid 1px lightblue;'><div><a href='" + pageUrl + '/DeleteInstDetail' + "' onClick='return window.voucherInfo.masterInfo.removeInstDetail(" + C[i].InstSrNo + ");'><img src='images/delete.png' id='deleteInstID_" + C[i].InstSrNo + "' /></a></div></td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + C[i].InstTypeName  + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + C[i].SlipNo + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].InstNo) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].InstDT) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;text-align:right;'>" + (C[i].Amount) + "</td>"
                    + "<td class='labelCaption' style='border:solid 1px lightblue;'>" + (C[i].Drawee) + "</td>"
                    //+ "<td class='labelCaption' style='border:solid 1px lightblue;'></td>"
                + "<td id='tdInst_" + C[i].InstSrNo + "' class='labelCaption' style='border:solid 1px lightblue;'><div><a href='" + pageUrl + '/EditInstDetail' + "' onClick='return window.voucherInfo.masterInfo.EditInstDetail(this, \"" + C[i].InstSrNo + "\");'><img src='images/Modify1.png' id='EditInstID_" + C[i].InstSrNo + "' /></a></div></td>"
                html + "</tr>";
            }
        }

        
        $(".tblInst").find("tr:gt(1)").remove();
        $(".tblInst").append(html);
    };
    this.CalCulateDebitCredit = function (C, Type) {
       
            var TotalDebit = 0;
            var TotalCredit = 0;
            for (var i in C) {
                if (C[i].DRCR == "D") {
                    TotalDebit = (parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT)).toFixed(2);
                } else if (C[i].DRCR == "C") {
                    TotalCredit = (parseFloat(TotalCredit) + parseFloat(C[i].AMOUNT)).toFixed(2);
                }

            }
        

        $("#txtTotalDebit").val(TotalDebit);
        $("#txtTotalCredit").val(TotalCredit);
        //var B = window.voucherInfo.voucherForm;
        //var EntryType = B.EntryType;
        if (TotalCredit > 0 && TotalDebit > 0 && TotalCredit == TotalDebit && Type=="I") {
            A.ShowConfirmSaveDialog();
        } 

    };
    this.ShowConfirmSaveDialog = function () {
        $("#spanTxt").html("Total Debit is equal to Total Credit. Do you want to save the voucher?");
        $("#dialog-confirm").dialog({
            title:"Confirm Saving?",
            resizable: false,
            height: 140,
            modal: true,
            buttons: {
                "Yes": function () {

                    $(this).dialog("close");
                    //A.CallSaveVoucher();
                    $("#txtNarration").val($("#txtNarrationval").val());
                    A.ShowNarationDialog();
                },
                "No": function () {

                    $(this).dialog("close");
                    

                }
            },
            open: function () {
                $(this).siblings('.ui-dialog-buttonpane').find('button:eq(0)').focus();
            }

        });
    };
    this.removeVoucherDetail = function (VchSRL) {
        if (GlobalSTATUS == 'Complete') {
            alert('This Voucher already have been approved! Can Not Delete?');
            return false;
        }
        //alert("in delete");
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#tr_" + VchSRL;
        $(removeElemTr).append(ajaxLoader);
        var B = {};
        B = window.voucherInfo.voucherForm;

        window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', VchSRL);
        A.ShowVoucherDetail(B.VoucherDetail,"I");
        $("#lblAccDesc_0").focus();
        return false;
    }
    this.removeInstDetail = function (InstSrno) {
        //alert("in inst delete");
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#trInst_" + InstSrno;
        $(removeElemTr).append(ajaxLoader);
        var B = {};
        B = window.voucherInfo.voucherForm;

        window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', InstSrno);
        A.ShowInstDetail(B.tmpVchInstType);
        $("#ddlInstType").focus();
        return false;
    };
    
   
    this.EditInstDetail = function (h, InstSrno) {
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#trInst_" + InstSrno;
        $(removeElemTr).append(ajaxLoader);
        var B = {};
        B = window.voucherInfo.voucherForm;

        var ValType = '';
        var ValInstType = '';
        var ValSlipNo = '';
        var ValNumber = '';
        var ValDate = '';
        var ValAmount = '';
        var ValBank = '';
        ValType = $(h).closest('tr').find('td:eq(1)').text();
        ValSlipNo = $(h).closest('tr').find('td:eq(2)').text();
        ValNumber = $(h).closest('tr').find('td:eq(3)').text();
        ValDate = $(h).closest('tr').find('td:eq(4)').text();
        ValAmount = $(h).closest('tr').find('td:eq(5)').text();
        ValBank = $(h).closest('tr').find('td:eq(6)').text();

        if(ValType=='Cheque'){ValInstType='C';}
        if(ValType=='Draft'){ValInstType='D';}
        if(ValType=='Cash'){ValInstType='H';}
        if(ValType=='Initiating Advice'){ValInstType='I';}
        if(ValType=='NEFT/RTGS'){ValInstType='N';}
        if(ValType=='Others'){ValInstType='O';}
        if(ValType=='Pay Order'){ValInstType='P';}
        if (ValType == 'Responding Advice') { ValInstType = 'R'; }
        $("#ddlInstType").val(ValInstType);
        $("#txtSlipNo").val(ValSlipNo);
        $("#txtInstNo").val(ValNumber);
        $("#txtInstDate").val(ValDate);
        $("#txtInstAmount").val(ValAmount);
        $("#txtDBankBR").val(ValBank);
        
        window.voucherInfo.findAndRemove(B.tmpVchInstType, 'InstSrNo', InstSrno);
        A.ShowInstDetail(B.tmpVchInstType);
        $("#ddlInstType").focus();
        ModifyflgInst = 'Modify';
        return false;

    };

    this.GetDebitCreditBal = function () {
        //if (Modifyflg == 'Add') {
            var BalAmount = '';
            var TotalDeb = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //yes
            var TotalCre = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
            if ($("#ddlDRCR").val() == "C")
            {
                 BalAmount = TotalDeb - TotalCre;
            }
            else if ($("#ddlDRCR").val() == "D")
            {
                BalAmount = TotalCre - TotalDeb;
            }
            if (BalAmount <= 0) { BalAmount = 0; }
            return BalAmount;
        //}
    }

    this.GetCreditDebitBal = function () {
        if (Modifyflg == 'Add') {
            var TotalDeb = ($("#txtTotalDebit").val() == "" ? 0 : parseFloat($("#txtTotalDebit").val()));  //yes
            var TotalCre = ($("#txtTotalCredit").val() == "" ? 0 : parseFloat($("#txtTotalCredit").val()));
            var BalAmount = TotalCre - TotalDeb;
            return BalAmount;
        }
    };

    this.DRCRChanged = function () {
        if ($("#ddlDRCR").val() != "") {
            $("#txtDebit_0").removeAttr("disabled");
            $("#txtCredit_0").removeAttr("disabled");
            $("#txtCredit_0").val("");
            $("#txtDebit_0").val("");
            var ValGLType = lblGlType;//$("#lblGlType").val();
            //alert(ValGLType);
            if (ValGLType == "A" || ValGLType == "B" || ValGLType == "C") { //&& $("#lblAccOldSlCode").val() != 'PCAS'
                if ($("#ddlDRCR").val() == "D") {

                    $("#txtCredit_0").prop("disabled", true);
                    $("#txtDebit_0").prop("disabled", false);
                    //$("#txtDebit_0").focus();
                    $("#txtInstAmount").val(A.GetDebitCreditBal); //A.GetDebitCreditBal() $("#txtTotalCredit").val()GetCreditDebitBal
                    $("#txtInstDate").val($("#txtVoucherDate").val());
                    A.ShowModalPopupInstType();

                } else if ($("#ddlDRCR").val() == "C") {
                    $("#txtDebit_0").prop("disabled", true);
                    
                    $("#txtCredit_0").prop("disabled", false);
                    $("#txtInstAmount").val(A.GetDebitCreditBal());
                    //$("#txtCredit_0").focus();
                    $("#txtInstDate").val($("#txtVoucherDate").val());
                    A.ShowModalPopupInstType();
                }


            } else if (ValGLType == "F") {
                if ($("#ddlDRCR").val() == "D") {

                    $("#txtCredit_0").prop("disabled", true);
                    $("#txtDebit_0").prop("disabled", false);
                    //$("#txtDebit_0").focus();
                    A.ShowModalPopupFA();

                } else if ($("#ddlDRCR").val() == "C") {
                    $("#txtDebit_0").prop("disabled", true);
                    $("#txtCredit_0").prop("disabled", false).val(parseFloat(A.GetDebitCreditBal()).toFixed(2));
                    //$("#txtCredit_0").focus();
                    A.ShowModalPopupFA();
                }   
            } else {
                if ($("#ddlDRCR").val() == "D") {

                    $("#txtCredit_0").prop("disabled", true);
                    $("#txtDebit_0").prop("disabled", false);
                    $("#txtDebit_0").prop("disabled", false).val(parseFloat(A.GetDebitCreditBal()).toFixed(2));
                    $("#txtDebit_0").focus();
                    $("#txtDebit_0").select();
                } else if ($("#ddlDRCR").val() == "C") {
                    $("#txtDebit_0").prop("disabled", true);
                    $("#txtCredit_0").prop("disabled", false).val(parseFloat(A.GetDebitCreditBal()).toFixed(2));
                    $("#txtCredit_0").focus();
                    $("#txtCredit_0").select();
                }
            }
            
        } else {
            
            $("#txtCredit_0").val("");
            $("#txtDebit_0").val("");
            $("#txtDebit_0").prop("disabled", true);
            $("#txtCredit_0").prop("disabled", true);
        }
    };

    this.editVoucherDetail = function (h, VchSRL) {
        if (GlobalSTATUS == 'Complete')
        {
            alert('This Voucher already have been approved! Can Not Edit?');
            return false;

        }
        
        //$("#editVchSID_" + VchSRL).hide();
        //$("#caneditVchSID_" + VchSRL).show();
        var B = {};
        //B = window.voucherInfo.voucherForm; alert(JSON.stringify(B));
        B = window.voucherInfo.voucherForm;
        //alert(JSON.stringify(B));
        var ValAccOldSlCode = '';

        var VchMstData = {};
        var VchDetData = {};

        VchMstData = B.VoucherMast;
        //alert(JSON.stringify(VchMstData));
        VchDetData = B.VoucherDetail;
        //alert(JSON.stringify(VchDetData));
        //alert(Modifyflg);
        //alert(JSON.stringify(VchDetData[VchSRL].inst));

        var VallblGlID = '';
        var VallblGlType = '';
        var VallblSlID = '';
        var VallblSubID = '';
        var ValOldSlCode = '';
        var ValAccDesc = '';
        var ValueDRCR = '';
        var ValueAmount = '';

        var SubID = '';
        var SubName = '';
       
        for (var i = 0 ; i < VchDetData.length; i++) {
            if (VchSRL == VchDetData[i].VchSrl) {
                //alert(VchDetData[i].VchSrl);
                 VallblGlID = VchDetData[i].GLID;
                 VallblGlType = VchDetData[i].GlType;
                 VallblSlID = VchDetData[i].SLID;
                 ValOldSlCode = VchDetData[i].OLDSLID;
                 ValAccDesc = VchDetData[i].GLName;
                 ValueDRCR = VchDetData[i].DRCR;
                 ValueAmount = VchDetData[i].AMOUNT;

                 VallblSubID = VchDetData[i].SubID;
                 SubID = VchDetData[i].OLDSUBID;
                 SubName = VchDetData[i].SUBDesc;
             }
        }



        lblGlID = VallblGlID;//$("#lblGlID").val(VallblGlID);
        lblGlType = VallblGlType;//$("#lblGlType").val(VallblGlType);
        lblSlID = VallblSlID;//$("#lblSlID").val(VallblSlID);
        lblSubID = VallblSubID;//$("#lblSubID").val(VallblSubID);

        $("#lblAccOldSlCode").val(ValOldSlCode);
        $("#lblAccDesc_0").val(ValAccDesc);
        if (ValueDRCR == 'D') {
            $("#ddlDRCR").val(ValueDRCR);
            $("#txtDebit_0").val(ValueAmount);
            $("#txtCredit_0").val('');
        }
        else {
            $("#ddlDRCR").val(ValueDRCR);
            $("#txtCredit_0").val(ValueAmount);
            $("#txtDebit_0").val('');
        }

        if (VallblSubID == '0000000000') {
            $("#lblOldSubCode").val('');
            $("#lblSubAccDesc_0").val('');
        }
        else {
                $("#lblOldSubCode").val(SubID);
                $("#lblSubAccDesc_0").val(SubName);
            }
        
        B.tmpVchInstType = [];
       
        glvrno = VchSRL;
        var SectorID = B.VoucherMast.SectorID;
        var C = WBFC.Utils.fixArray(B.tmpVchInstType);
        var rowid = 1;
        $.each(VchDetData, function (index, result) {
            if (result['VchSrl'] == glvrno) {
                //alert(JSON.stringify(result));
                $.each(result, function (index1, DataInst) {
                    //alert(result1.length);
                    for (var k in DataInst) {
                        //$("#lblGlID").val() //$("#lblSlID").val()
                        if (lblGlID == DataInst[k].GLID && lblSlID == DataInst[k].SLID) {
                            C.push({
                                InstSrNo: rowid,//DataInst[k].InstSrNo, skm
                                YearMonth: $("#txtYearMonth").val(),
                                VoucherType: $("#ddlVchType").val(),
                                VCHNo: DataInst[k].VCHNo,
                                GLID: lblGlID,//$("#lblGlID").val(),
                                SLID: lblSlID,//$("#lblSlID").val(),
                                DRCR: $("#ddlDRCR").val(),
                                SlipNo: DataInst[k].SlipNo,
                                InstType: DataInst[k].InstType,
                                InstTypeName: DataInst[k].InstTypeName,
                                InstNo: DataInst[k].InstNo,
                                InstDT: DataInst[k].InstDT,
                                Amount: DataInst[k].Amount,
                                ActualAmount: DataInst[k].Amount,
                                Drawee: DataInst[k].Drawee,
                                SectorID: DataInst[k].SectorID

                            });
                            rowid = rowid + 1;
                            A.ShowInstDetail(C);
                        }
                    }
                });
            }

        });
        
        
            window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', VchSRL);
            A.ShowVoucherDetail(B.VoucherDetail, "I");

            if (C.length > 0) {
                A.ShowModalPopupInstType();
            }

            else {
                if ($("#ddlDRCR").val() == 'D') {
                    $("#txtCredit_0").prop("disabled", true);
                    $("#txtDebit_0").prop("disabled", false).val();

                    $("#txtDebit_0").focus();
                    $("#txtDebit_0").select();
                }
                else {
                    $("#txtDebit_0").prop("disabled", true);
                    $("#txtCredit_0").prop("disabled", false).val();
                    $("#txtCredit_0").focus();
                    $("#txtCredit_0").select();
                }

            }
            ModifyflgInst = 'Add';
            Modifyflg = 'Modify';
        return false;
    }

   

    this.caneditVoucherDetail = function (VchSRL) {

        //alert("in delete");
        //var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        //var removeElemTr = "#tr_" + VchSRL;
        //$(removeElemTr).append(ajaxLoader);
        $("#editVchSID_" + VchSRL).show();
        $("#caneditVchSID_" + VchSRL).hide();
        var B = {};
        B = window.voucherInfo.voucherForm;

        //window.voucherInfo.findAndRemove(B.VoucherDetail, 'VchSrl', VchSRL);
        A.ShowVoucherDetail(B.VoucherDetail,"I");
        $("#lblAccDesc_0").focus();
        return false;
    }

    this.GLSLAccKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
            if (!A.validator.form()) {
                alert("Required Field not entered");
                A.validator.focusInvalid();
                return false;
            }

            if ($('#lblAccDesc_0').val() != '') {
                $('#txtGLSLDescSearch').val($('#lblAccDesc_0').val());
                A.BindGridGLSL($('#txtGLSLDescSearch').val());
                $('#txtGLSLDescSearch').focus();
                return false;
            }
            else {
                $('#txtGLSLDescSearch').val('');
                A.BindGridGLSL($('#txtGLSLDescSearch').val());
                $('#txtGLSLDescSearch').focus();
                return false;
            }
        }
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57))
            alert("Please Type Enter Key For List Of Value");
        //$('#lblAccDesc_0').focus();
        if ($('#lblAccDesc_0').val() != '') {
            $('#txtGLSLDescSearch').val($('#lblAccDesc_0').val());
            $('#txtGLSLDescSearch').focus();
        }
        else {
            $('#txtGLSLDescSearch').val('');
            $('#txtGLSLDescSearch').focus();
        }
        A.BindGridGLSL($('#txtGLSLDescSearch').val());
        
        return false;

    };
    this.SUBAccKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
            if ($('#lblSubAccDesc_0').val() != '') {
                $('#txtSUBDescSearch').val($('#lblSubAccDesc_0').val());
                A.BindGridSub($('#txtSUBDescSearch').val());
                $('#txtSUBDescSearch').focus();
                return false;
            }
            else {
                $('#txtSUBDescSearch').val('');
                A.BindGridSub($('#txtSUBDescSearch').val());
                $('#txtSUBDescSearch').focus();
                return false;
            }
        }
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57))
            alert("Please Enter Key Press For Display Sub Description");
        //$('#lblAccDesc_0').focus();
        if ($('#lblSubAccDesc_0').val() != '') {
            $('#txtSUBDescSearch').val($('#lblSubAccDesc_0').val());
            $('#txtSUBDescSearch').focus();
        }
        else {
            $('#txtSUBDescSearch').val('');
            $('#txtSUBDescSearch').focus();
        }
        A.BindGridSub($('#txtSUBDescSearch').val());
        return false;

    };
    this.BindGridGLSL = function (GlSlDesc) {
        var StrIndentStatus = '';
        var StrFromIndentDate = '';
        var StrIndentName = '';
        $("#spNoData").html('');

        $(".loading-overlay").show();
        var W = "{GlSlDesc:'" + GlSlDesc + "',Type:'" + ($("#rdName").is(":checked") ? $("#rdName").val() : $("#rdCode").val()) + "'}";
        //alert(W);
        $.ajax({
            type: "POST",
            url: pageUrl + '/GET_DisGlSl',
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                $("#grdGLSL").empty();
                //alert(JSON.stringify(data.d));
                if (data.d.length > 0) {
                    $("#grdGLSL").append("<tr><th>OLD SL ID</th><th>GL NAME</th><th style='display:none;'>GL TYPE</th><th style='display:none;'>GL ID</th><th style='display:none;'>SL ID</th></tr>");
                    
                    for (var i = 0; i < data.d.length; i++) {
                        $("#grdGLSL").append("<tr><td ><input type='text' value='" +
                                data.d[i].OLD_SL_ID + "' style='width:50px;' class='glnameenter' readonly  /></td> <td><input type='text' value='" +
                                data.d[i].GL_NAME + "' style='width:500px;' class='glnameenter' readonly  /></td> <td style='display:none;'>" + //disabled='true'
                                data.d[i].GL_TYPE + "</td> <td style='display:none;'>" +
                                data.d[i].GL_ID + "</td> <td style='display:none;'>" +
                                data.d[i].SL_ID + "</td></tr>");
                    }
                    //===================== Start PO Number Search is not Blank Then PO Number Record Display in GridView ===========================//
                    //if ($("#txtGLSLDescSearch").val() != '') {
                    //    var rows;
                    //    var coldata;

                    //    $("#grdGLSL").find('tr:gt(0)').hide();
                    //    var data = $('#txtGLSLDescSearch').val();
                    //    var len = data.length;
                    //    if (len > 0) {
                    //        $("#grdGLSL").find('tbody tr').each(function () {
                    //            coldata = $(this).children().eq(1);
                    //            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                    //            if (temp === 0) {
                    //                $(this).show();
                    //            }

                    //        });
                    //    } else {
                    //        $("#grdGLSL").find('tr:gt(0)').show();

                    //    }
                    //}
                    //===================== End PO Number Search is not Blank Then PO Number Record Display in GridView ===========================//

                    $(".loading-overlay").hide();
                    $(function () {
                        //$("[id*=grdGLSL]").bind("keydown", A.GrdGLSLKeyDown);
                        $("[id*=grdGLSL] td").bind("click", function () {
                            var row = $(this).parent();
                            $("[id*=grdGLSL] tr").each(function (rowindex) {
                                if ($(this)[0] != row[0]) {
                                    $("td", this).removeClass("selected_row");
                                    $("td", this).children('input').removeClass("selected_row");
                                }
                            });

                            $("td", row).each(function () {
                                if (!$(this).hasClass("selected_row")) {
                                    $(this).addClass("selected_row");
                                    $(this).children('input').addClass("selected_row");                                   
                                } else {
                                    $(this).removeClass("selected_row");
                                    $(this).children('input').removeClass("selected_row");                                    
                                }
                            });
                        });
                    });
                    // Paging Function Calling
                    //GirdViewPaging();
                    //===================== Start Selected Gridview PO Number Value Display in Text Box ===========================//bind
                    $(document).ready(function () {
                        $("[id*=grdGLSL] tbody tr").click("click", function () {
                            //var ValGLSL = $(this).children("td:eq(1)").text();
                            //var ValGLSL = $(this).children("td:eq(1)").children('input').val();
                            var ValGLSL = ($("#rdName").is(":checked") ? $(this).children("td:eq(1)").children('input').val() : $("#rdCode").is(":checked") ? $(this).children("td:eq(0)").children('input').val() : "");
                            curcellGL = ($("#rdName").is(":checked") ? $(this).children("td:eq(1)") : $("#rdCode").is(":checked") ? $(this).children("td:eq(0)") : $(this).children("td:eq(1)")); //$(this).children("td:eq(0)");
                            $("#txtGLSLDescSearch").val(ValGLSL);
                            //$("#txtGLSLDescSearch").focus();
                            curcellGL.children('input').focus();
                        });
                    });
                    //===================== End Selected Gridview PO Number Value Display in Text Box ===========================//
                    
                }

                else {
                    //alert("No Records Data");
                    $("#spNoData").html("No Records Found");
                    $(".loading-overlay").hide();
                    //$('#lblAccDesc_0').focus();
                    $("#txtGLSLDescSearch").focus();
                }
                A.ShowModalPopupGLSL();
                $("#txtGLSLDescSearch").focus();
            },
            error: function (result) {
                alert("Error Records Data");
                $(".loading-overlay").hide();

            }
        });
    };
    this.BindGridSub = function (SubDesc) {
        var StrIndentStatus = '';
        var StrFromIndentDate = '';
        var StrIndentName = '';
        $("#spSUBNoData").html('');

        $(".loading-overlay").show();
        var W = "{SubDesc:'" + SubDesc + "',Type:'" + ($("#rdSName").is(":checked") ? $("#rdSName").val() : $("#rdSCode").val()) + "'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/GET_DisSub',
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                $("#grdSUB").empty();
                //alert(JSON.stringify(data.d));
                if (data.d.length > 0) {
                    $("#grdSUB").append("<tr><th>OLD SUB ID</th><th>SUB NAME</th><th style='display:none;'>SUB ID</th><th style='display:none;'>SUB TYPE</th></tr>");
                   
                    for (var i = 0; i < data.d.length; i++) {
                        $("#grdSUB").append("<tr><td><input type='text' value='" +
                                data.d[i].OLD_SUB_ID + "' style='width:50px;' class='subnameenter' readonly  /> </td> <td><input type='text' value='" +
                                data.d[i].SUB_NAME + "' style='width:500px;' class='subnameenter' readonly  /></td> <td style='display:none;'>" +
                                data.d[i].SUB_ID + "</td> <td  style='display:none;'>" +
                                data.d[i].SUB_TYPE + "</td> </tr>");
                    }
                    //===================== Start PO Number Search is not Blank Then PO Number Record Display in GridView ===========================//
                    //if ($("#txtSUBDescSearch").val() != '') {
                    //    var rows;
                    //    var coldata;

                    //    $("#grdSUB").find('tr:gt(0)').hide();
                    //    var data = $('#txtSUBDescSearch').val();
                    //    var len = data.length;
                    //    if (len > 0) {
                    //        $("#grdSUB").find('tbody tr').each(function () {
                    //            coldata = $(this).children().eq(1);
                    //            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                    //            if (temp === 0) {
                    //                $(this).show();
                    //            }

                    //        });
                    //    } else {
                    //        $("#grdSUB").find('tr:gt(0)').show();

                    //    }
                    //}
                    //===================== End PO Number Search is not Blank Then PO Number Record Display in GridView ===========================//

                    $(".loading-overlay").hide();
                    $(function () {
                        $("[id*=grdSUB] td").bind("click", function () {
                            var row = $(this).parent();
                            $("[id*=grdSUB] tr").each(function (rowindex) {
                                if ($(this)[0] != row[0]) {
                                    $("td", this).removeClass("selected_row");
                                    $("td", this).children('input').removeClass("selected_row");
                                }
                            });

                            $("td", row).each(function () {
                                if (!$(this).hasClass("selected_row")) {
                                    $(this).addClass("selected_row");
                                    $(this).children('input').addClass("selected_row");
                                } else {
                                    $(this).removeClass("selected_row");
                                    $(this).children('input').removeClass("selected_row");
                                }
                            });
                        });
                    });
                    // Paging Function Calling
                    //GirdViewPaging();
                    //===================== Start Selected Gridview PO Number Value Display in Text Box ===========================//bind
                    $(document).ready(function () {
                        $("[id*=grdSUB] tbody tr").click("click", function () {
                            //var ValGLSL = $(this).children("td:eq(1)").text();
                            //$("#txtSUBDescSearch").val(ValGLSL);
                            //$("#txtSUBDescSearch").focus();

                            var ValGLSL = ($("#rdSName").is(":checked") ? $(this).children("td:eq(1)").children('input').val() : $("#rdSCode").is(":checked") ? $(this).children("td:eq(0)").children('input').val() : "");
                            curCellSUB = ($("#rdSName").is(":checked") ? $(this).children("td:eq(1)") : $("#rdSCode").is(":checked") ? $(this).children("td:eq(0)") : $(this).children("td:eq(1)")); //$(this).children("td:eq(0)");
                            $("#txtSUBDescSearch").val(ValGLSL);
                            //$("#txtGLSLDescSearch").focus();
                            curCellSUB.children('input').focus();
                        });
                    });
                    //===================== End Selected Gridview PO Number Value Display in Text Box ===========================//
                    
                }
                else {
                    //alert("No Records Data");
                    $("#spSUBNoData").html('No records found');
                    $(".loading-overlay").hide();
                    //$('#lblSubAccDesc_0').focus();
                    $("#txtSUBDescSearch").focus();
                }
                A.ShowModalPopupSub();
                $("#txtSUBDescSearch").focus();
            },
            error: function (result) {
                alert("Error Records Data");
                $(".loading-overlay").hide();

            }
        });
    };
    
    this.KeypressSetInstDate = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
            checkInstDtless90days();
        }
        return false;
    };
    
    this.KeyDownSetInstDate = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 9) {
            checkInstDtless90days();
        }
        return false;
    };

    function checkInstDtless90days() {
        var VDate = $('#txtVoucherDate').val().split("/");
        var StrVchDate = VDate[2] + "-" + VDate[1] + "-" + VDate[0];
        var NewVchDate = new Date(StrVchDate);
        var LessInstdate = NewVchDate.setDate(NewVchDate.getDate() + parseInt(-90));
        var DtLessInstdate = new Date(LessInstdate); //compare les 90 days
        var SetInstDate = $('#txtInstDate').val().split("/");
        var CurrInstDate = SetInstDate[2] + "-" + SetInstDate[1] + "-" + SetInstDate[0];
        var DtCurrInstDate = new Date(CurrInstDate); //Compare Current Inst Date
        if (DtCurrInstDate < DtLessInstdate) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Instrument Date Exced 90 Days's! Will You Proceed Further (Y/N)?")) {
                confirm_value.value = "Yes";

            } else {
                confirm_value.value = "No";
            }

            if (confirm_value.value == "Yes") {
                $('#txtInstAmount').focus();
                $('#txtInstAmount').select();
            }
            if (confirm_value.value == "No") {
                $('#txtInstDate').val('');
                $('#txtInstDate').focus();
                return false;
            }
        } // Main If Close
        else {
            $('#txtInstAmount').focus();
            $('#txtInstAmount').select();
            return false;

        }
    }

    this.InstTypeKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 13) {
           
            //SetVoucherDate();
            A.CallInstypeBlur();
        }
        return false;
    };

    this.ddlInstTypeKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        var InstType = $("#ddlInstType").val();
        //var B = window.voucherInfo.voucherForm;
        //var D = WBFC.Utils.fixArray(B.tmpVchInstType);
        if (iKeyCode == 13 || iKeyCode == 9) {
            if (InstType == "O" || InstType == "H" || InstType == "R" || InstType == "E") {
                $("#txtInstAmount").focus();
                $("#txtInstAmount").select();
                return false;
            } else if (InstType == "P") {   //else if (InstType == "P" || InstType == "H") {
                $("#txtInstAmount").focus();
                $("#txtInstAmount").select();
            } else {
                $("#txtSlipNo").focus();
                $("#txtSlipNo").select();
                return false;
            }
        }
        //return false;
    };

    this.ddlInstTypekeydown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        var InstType = $("#ddlInstType").val();
        if (iKeyCode == 9) {
            if (InstType == "O" || InstType == "H" || InstType == "R") {
                $("#txtInstAmount").focus();
                $("#txtInstAmount").select();
                //SetVoucherDate();
                return false;
            } else if (InstType == "P") {   //else if (InstType == "P" || InstType == "H") {
                $("#txtInstAmount").focus();
                $("#txtInstAmount").select();
                //SetVoucherDate();
                return false;
            } else {
                $("#txtSlipNo").focus();
                $("#txtSlipNo").select();
                //SetVoucherDate();
                return false;
            }
        }
        
        if (iKeyCode == 67) {
            if ($("#ddlInstType").val() == 'C') {
                $("#ddlInstType").val('H');
                A.CallInstypeBlur();
            }
            else { $("#ddlInstType").val('C'); A.CallInstypeBlur(); }
        }

        if (iKeyCode == 68) {
            $("#ddlInstType").val('D');
            A.CallInstypeBlur();
        }

        if (iKeyCode == 69) {
            $("#ddlInstType").val('E');
            A.CallInstypeBlur();
        }

        if (iKeyCode ==72) {
            $("#ddlInstType").val('H');
            A.CallInstypeBlur();
        }

        if (iKeyCode == 73) {
            $("#ddlInstType").val('I');
            A.CallInstypeBlur();
        }

        if (iKeyCode == 78) {
            $("#ddlInstType").val('N');
            A.CallInstypeBlur();
        }

        if (iKeyCode == 79) {
            $("#ddlInstType").val('O');
            A.CallInstypeBlur();
        }
        if (iKeyCode == 80) {
            $("#ddlInstType").val('P');
            A.CallInstypeBlur();
        }

        if (iKeyCode == 82) {
            $("#ddlInstType").val('R');
            A.CallInstypeBlur();
        }
        
    };

   
    this.CallInstypeBlur = function () {
        if ($("#ddlInstType").val() != "") {
            var InstType = $("#ddlInstType").val();
            var B = window.voucherInfo.voucherForm;
            var D = WBFC.Utils.fixArray(B.tmpVchInstType);
            //Shrawan
            if (InstType == "O" || InstType == "H" || InstType == "R" || InstType == "E") {
                if (ModifyflgInst == 'Add') {
                    var i = D.length + 1;
                    var instSrl = leftpad(i.toString(), 4, '0');
                    var InstNo = ($("#txtYearMonth").val() + $("#lblAccOldSlCode").val() + instSrl);
                    $("#txtInstNo").val(InstNo);
                    
                    $("#txtInstDate").val($("#txtVoucherDate").val());
                    //$("#txtInstAmount").focus();
                    return false;
                }
            } else if (InstType == "P") {   //else if (InstType == "P" || InstType == "H") {
                if (ModifyflgInst == 'Add') {
                    $("#txtInstNo").val("");
                    $("#txtInstDate").val($("#txtVoucherDate").val());
                    return false;
                    //$("#txtInstAmount").focus();
                }
            } else {
                if (ModifyflgInst == 'Add') {
                    $("#txtInstNo").val("");
                    $("#txtInstDate").val($("#txtVoucherDate").val());
                    //$("#txtSlipNo").focus();
                    return false;
                }
            }
        } else {
            alert("Please instrument type");
            return false;
        }
    }
    this.ShowModalPopupInstType = function () {
        SetVoucherDate();
        $("#dialogInstrumentType").dialog({
            title: "Instrument Type",
            width: 980,
           
            buttons: {
                Ok: function () {
                    //A.InstrumentAdd();
                    A.CallculateInstAmount();
                }
            },
            modal: true
        });
    };
    this.CallculateInstAmount = function () {
        //Modifyflg = 'Add';
        var B = window.voucherInfo.voucherForm;
        //alert(JSON.stringify(B));
        var SectorID = B.VoucherMast.SectorID;
        //alert(JSON.stringify(B.tmpVchInstType));
        var C = WBFC.Utils.fixArray(B.tmpVchInstType);
        //alert(JSON.stringify(C));
        var InstAmount = 0;
        for (var i in C) {
            InstAmount = (parseFloat(InstAmount) + parseFloat(C[i].Amount)).toFixed(2);
        }
        $("#dialogInstrumentType").dialog('close');
        $("#txtCredit_0").val("");
        $("#txtDebit_0").val("");
        if ($("#ddlDRCR").val() == "D") {

            $("#txtCredit_0").prop("disabled", true);
            //$("#txtDebit_0").prop("disabled", false).val(parseFloat(InstAmount).toFixed(2));
            $("#txtDebit_0").prop("disabled", false).val(InstAmount);
            $("#txtDebit_0").focus();
            $("#txtDebit_0").select();
        } else if ($("#ddlDRCR").val() == "C") {

            $("#txtDebit_0").prop("disabled", true);
            //$("#txtCredit_0").prop("disabled", false).val(parseFloat(InstAmount).toFixed(2));
            $("#txtCredit_0").prop("disabled", false).val(InstAmount);
            $("#txtCredit_0").focus();
            $("#txtCredit_0").select();
        }
    };
    this.ShowModalPopupGLSL = function () {
        $("#dialogSearchGLSL").dialog({
            title: "GL Search",
            width: 590,
            resizable: false,
            position: ['middle', 60],
            buttons: {
                Ok: function () {
                    A.GetGridGLSlValue();
                }
            },
            modal: true
        });
    };
    this.GetGridGLSlValue = function () {
        var GridGLSL = document.getElementById("grdGLSL");
        var ValGlName = '';
        var ValOLDSlID = '';
        var ValGLType = '';
        var ValSLID = '';
        var ValGLID = '';
        if ($('#txtGLSLDescSearch').val().trim() == '') {
            alert("Please Select Old Sl ID");
            $('#txtGLSLDescSearch').val('');
            $("#txtGLSLDescSearch").focus();
            return false;
        }
        if ($("#txtGLSLDescSearch").val() != '') {
            if ($("#rdName").is(":checked")) {
                for (var row = 1; row < GridGLSL.rows.length; row++) {
                    //var GridGLName_Cell = GridGLSL.rows[row].cells[1];
                    //var valueGlSlName = GridGLName_Cell.textContent.toString();
                    var GridGLName_Cell = GridGLSL.rows[row].cells[1].childNodes[0];
                    var valueGlSlName = GridGLName_Cell.value.toString();
                    if (valueGlSlName == $("#txtGLSLDescSearch").val()) {
                        ValOLDSlID = GridGLSL.rows[row].cells[0].childNodes[0].value.toString();
                        ValGlName = valueGlSlName;
                        ValGLType = GridGLSL.rows[row].cells[2].textContent.toString();
                        ValGLID = GridGLSL.rows[row].cells[3].textContent.toString();
                        ValSLID = GridGLSL.rows[row].cells[4].textContent.toString();
                        break;
                    }
                }
            } else if ($("#rdCode").is(":checked")) {
                for (var row = 1; row < GridGLSL.rows.length; row++) {
                    //var GridGLName_Cell = GridGLSL.rows[row].cells[1];
                    //var valueGlSlName = GridGLName_Cell.textContent.toString();
                    var GridGLName_Cell = GridGLSL.rows[row].cells[0].childNodes[0];
                    var valueGlSlName = GridGLName_Cell.value.toString();
                    if (valueGlSlName == $("#txtGLSLDescSearch").val()) {
                        ValOLDSlID = valueGlSlName; 
                        ValGlName = GridGLSL.rows[row].cells[1].childNodes[0].value.toString();
                        ValGLType = GridGLSL.rows[row].cells[2].textContent.toString();
                        ValGLID = GridGLSL.rows[row].cells[3].textContent.toString();
                        ValSLID = GridGLSL.rows[row].cells[4].textContent.toString();
                        break;
                    }
                }
            }

            //$("[id*=grdGLSL] tr").each(function (rowindex) {
            //    //var ValGLSL = $(this).children("td:eq(1)").text();
            //    //var ValGLSL = $(this).children("td:eq(1)").children('input').val();          
                

            //    var GridGLName_Cell = $(this).children("td:eq(1)").children('input');
            //        var valueGlSlName = GridGLName_Cell.val().toString();
            //        if (valueGlSlName == $("#txtGLSLDescSearch").val()) {
            //            ValOLDSlID = $(this).children("td:eq(0)").text().toString();
            //            ValGlName = valueGlSlName;
            //            ValGLType = $(this).children("td:eq(2)").text().toString();
            //            ValGLID = $(this).children("td:eq(3)").text().toString();
            //            ValSLID = $(this).children("td:eq(4)").text().toString();

            //            //break;
            //        }
            //});

            if (($("#rdName").is(":checked") && ValGlName != $("#txtGLSLDescSearch").val()) || ($("#rdCode").is(":checked") && ValOLDSlID != $("#txtGLSLDescSearch").val())) {
                alert("Invalid SL ID (SL ID is not in List)");
                $("#txtGLSLDescSearch").focus();
                return false;
            }

            else {
                if ($("#rdName").is(":checked")) {
                    //$("#lblAccDesc_0").val($("#txtGLSLDescSearch").val());
                    $("#lblAccDesc_0").val(ValGlName);
                    lblGlType = ValGLType;//$("#lblGlType").val(ValGLType);
                    lblSlID = ValSLID;//$("#lblSlID").val(ValSLID);
                    lblGlID = ValGLID;//$("#lblGlID").val(ValGLID);
                    $("#lblAccOldSlCode").val(ValOLDSlID);
                    //A.BindGridGLSL($("#lblAccDesc_0").val());
                    //$("#lblAccDesc_0").focus();
                } else if ($("#rdCode").is(":checked")) {
                    $("#lblAccDesc_0").val(ValGlName);
                    lblGlType = ValGLType;//$("#lblGlType").val(ValGLType);
                    lblSlID = ValSLID;//$("#lblSlID").val(ValSLID);
                    lblGlID = ValGLID;//$("#lblGlID").val(ValGLID);
                    $("#lblAccOldSlCode").val(ValOLDSlID);
                }
                $("#dialogSearchGLSL").dialog('close');
                $("#dvLoaneeOut").html("");
                if (ValGLType != "L") {
                    //alert(ValGLType);
                    
                    $("#ddlDRCR").val("D");
                    $("#ddlDRCR").focus();
                } else {
                    A.GetLoaneeOutDet();
                    $("#lblSubAccDesc_0").focus();
                }

                return false;
            }
        }
    };
    this.GetLoaneeOutDet = function () {
        $(".loading-overlay").show();
        var valueOS = '';
        var B = {};
        //alert('b empty');
        B = window.voucherInfo.voucherForm;
        //alert('b added'); //$("#lblGlID").val() //$("#lblSlID").val()
        var E = "{GLID:'" + lblGlID + "',SLID:'" + lblSlID + "',YRMN:'" + $.trim($("#txtYearMonth").val()) + "',SectorID:" + B.VoucherMast.SectorID + "}";
        //alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/GetLoaneeOutDet',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ret = JSON.parse(response.d);
                //alert(ret.Message.substr(4, 5).split("-"));
                //if (ret.Message.substr(4, 5).split(":") <= 0) {
                if (ret.Message !="") {
                    $("#dvLoaneeOut").html(ret.Message);
                    $("#dvLoaneeOut").html($("#dvLoaneeOut").html().replace(/\n\r?/g, '<br />'));
                    valueOS = ret.Message.substr(4, 5).split(":");
                    
                } else if (ret.Message == "") {
                    $("#dvLoaneeOut").html("");   
                }
                chekOutstanding(valueOS);
                $(".loading-overlay").hide();
                
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
        
    };

    function chekOutstanding(valOS)
    {
        if (valOS <= 0) {

            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Zero Balance For Requested Loanee! Continue with Voucher Entry?")) {
                confirm_value.value = "Yes";

            } else {
                confirm_value.value = "No";
            }
            if (confirm_value.value == "No") {

                lblGlID = '';
                //$("#lblGlID").val('');
                //$("#lblGlType").val('');
                lblGlType = '';
                //$("#lblSlID").val('');
                lblSlID = '';
                //$("#lblSubID").val('');
                lblSubID = '';
                $("#lblAccOldSlCode").val('');
                $("#lblOldSubCode").val('');
                $("#lblAccDesc_0").val('');
                $("#dvLoaneeOut").html("");
                $("#lblAccDesc_0").focus();
                $("#ddlDRCR").val('Select');
                return false;
            }


        }

    }

    this.ShowModalPopupSub = function () {
        $("#dialogSearchSub").dialog({
            title: "Sub Search",
            width: 590,

            buttons: {
                Ok: function () {
                    A.GetGridSubValue();
                }
            },
            modal: true
        });
    };
    this.GetGridSubValue = function () {
        var GridSub = document.getElementById("grdSUB");
        var ValSubName = '';
        var ValOLDSubID = '';
       
        var ValSUBID = '';
       
        if ($('#txtSUBDescSearch').val().trim() == '') {
            alert("Please Select Old Sub Desc");
            $('#txtSUBDescSearch').val('');
            $("#txtSUBDescSearch").focus();
            return false;
        }
        if ($("#txtSUBDescSearch").val() != '') {
            if ($("#rdSName").is(":checked")) {
                for (var row = 1; row < GridSub.rows.length; row++) {
                    //var GridSubName_Cell = GridSub.rows[row].cells[1];
                    //var valueSubName = GridSubName_Cell.textContent.toString();
                    var GridSubName_Cell = GridSub.rows[row].cells[1].childNodes[0];
                    var valueSubName = GridSubName_Cell.value.toString();
                    if (valueSubName == $("#txtSUBDescSearch").val()) {
                        //ValOLDSubID = GridSub.rows[row].cells[0].textContent.toString();
                        ValOLDSubID = GridSub.rows[row].cells[0].childNodes[0].value.toString();
                        ValSubName = valueSubName;
                        ValSUBID = GridSub.rows[row].cells[2].textContent.toString();
                        break;
                    }
                }
            } else if ($("#rdSCode").is(":checked")) {
                for (var row = 1; row < GridSub.rows.length; row++) {
                    var GridSubName_Cell = GridSub.rows[row].cells[0].childNodes[0];
                    var valueSubName = GridSubName_Cell.value.toString();
                    if (valueSubName == $("#txtSUBDescSearch").val()) {
                        ValOLDSubID = valueSubName;;
                        ValSubName = GridSub.rows[row].cells[1].childNodes[0].value.toString();
                        ValSUBID = GridSub.rows[row].cells[2].textContent.toString();
                        break;
                    }
                }
            }
            
            //if (ValSubName != $("#txtSUBDescSearch").val()) {
            if (($("#rdSName").is(":checked") && ValSubName != $("#txtSUBDescSearch").val()) || ($("#rdSCode").is(":checked") && ValOLDSubID != $("#txtSUBDescSearch").val())) {
                alert("Invalid SUB ID (SUB ID is not in List)");
                $("#txtSUBDescSearch").focus();
                return false;
            }

            else {
                if ($("#rdSName").is(":checked")) {
                    $("#lblSubAccDesc_0").val(ValSubName);
                    lblSubID = ValSUBID;//$("#lblSubID").val(ValSUBID);
                    $("#lblOldSubCode").val(ValOLDSubID);
                    //A.BindGridGLSL($("#lblAccDesc_0").val());
                    //$("#lblAccDesc_0").focus();
                } else {
                    $("#lblSubAccDesc_0").val(ValSubName);
                    lblSubID = ValSUBID;//$("#lblSubID").val(ValSUBID);
                    $("#lblOldSubCode").val(ValOLDSubID);
                }
                $("#dialogSearchSub").dialog('close');
                $("#ddlDRCR").val("D");
                $("#ddlDRCR").focus();

                return false;
            }
        }
    };
    this.TxtSubKeyUP = function (evt) {
        //$("#grdSUB").find('tr:gt(0)').hide();
        //var data = $('#txtSUBDescSearch').val();
        //var len = data.length;
        //if (len > 0) {
        //    $("#grdSUB").find('tbody tr').each(function () {
        //        coldata = $(this).children().eq(1);
        //        var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
        //        if (temp === 0) {
        //            $(this).show();
        //        }
        //    });
        //} else {
        //    $("#grdSUB").find('tr:gt(0)').show();
        //}

        A.BindGridSub($('#txtSUBDescSearch').val());

        //var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        //if ($('#txtGLSLDescSearch').val() != "" && iKeyCode == 13) {
        //    alert($('#grdGLSL tr:visible').length);
        //    A.GetGridGLSlValue();
        //}

    };
    
    this.TxtSubKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            A.GetGridSubValue();
        }
    }
    this.TxtSubKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 40) {
            if ($("#rdSName").is(":checked")) {
                curCellSUB = $("#grdSUB td:eq(1)");//.first();
                var row = $(curCellSUB).parent();
                A.SelectSetSubRowValue(row);
                //alert(curcellGL.attr("class"));
                curCellSUB.children('input').focus();

            } else if ($("#rdSCode").is(":checked")) {
                curCellSUB = $("#grdSUB td:eq(0)");//.first();
                var row = $(curCellSUB).parent();
                A.SelectSetSubRowValue(row);
                //alert(curcellGL.attr("class"));
                curCellSUB.children('input').focus();

            }



        }
    };

    this.SelectSetSubRowValue = function (row) {
        $("[id*=grdSUB] tr").each(function (rowindex) {
            if ($(this)[0] != row[0]) {
                $("td", this).removeClass("selected_row");
                $("td", this).children('input').removeClass("selected_row");
            }
        });

        $("td", row).each(function () {
            if (!$(this).hasClass("selected_row")) {
                $(this).addClass("selected_row");
                $(this).children('input').addClass("selected_row");
                //$("input", row).each(function () {
                //    $(this).addClass("selected_row");
                //});

            } else {
                $(this).removeClass("selected_row");
                $(this).children('input').removeClass("selected_row");
                //$("input", row).each(function () {
                //    $(this).removeClass("selected_row");
                //});
            }
        });

        //var ValGLSL = row.children("td:eq(1)").text();
        var ValGLSL = ($("#rdSName").is(":checked") ? row.children("td:eq(1)").children('input').val() : $("#rdSCode").is(":checked") ? row.children("td:eq(0)").children('input').val() : "");
        $("#txtSUBDescSearch").val(ValGLSL);
    };
    this.GrdSubKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        var c = "";

        if (iKeyCode == 38) {
            if ($("#rdSName").is(":checked")) {
                c = curCellSUB.closest('tr').prev().find('td:eq(1)');
            } else if ($("#rdSCode").is(":checked")) {
                c = curCellSUB.closest('tr').prev().find('td:eq(0)');
            }


        } else if (iKeyCode == 40) {
            // Down Arrow
            if ($("#rdSName").is(":checked")) {
                c = curCellSUB.closest('tr').next().find('td:eq(1)');
            } else if ($("#rdSCode").is(":checked")) {
                c = curCellSUB.closest('tr').next().find('td:eq(0)');
            }
            //c = curcellGL.closest('tr').next().find('td:eq(1)');
        }

        if (c.length > 0) {
            curCellSUB = c;
            var row = $(curCellSUB).parent();
            A.SelectSetSubRowValue(row);
            curCellSUB.focus();
        }

    };


    this.TxtGlSlKeyUP = function (evt) {
        //$("#grdGLSL").find('tr:gt(0)').hide();
        //var data = $('#txtGLSLDescSearch').val();
        //var len = data.length;
        //if (len > 0) {
        //    $("#grdGLSL").find('tbody tr').each(function () {
        //        coldata = $(this).children().eq(1);
        //        var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
        //        if (temp === 0) {
        //            $(this).show();
        //        }
        //    });
        //} else {
        //    $("#grdGLSL").find('tr:gt(0)').show();
        //}
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode!=40 )
            A.BindGridGLSL($('#txtGLSLDescSearch').val());
        

    };
    this.TxtGlSlKeyPress = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            A.GetGridGLSlValue();
        }
    };
    this.TxtGlSlKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 40) {
            if ($("#rdName").is(":checked")) {
                curcellGL = $("#grdGLSL td:eq(1)");//.first();
                var row = $(curcellGL).parent();
                A.SelectSetGLRowValue(row);
                //alert(curcellGL.attr("class"));
                curcellGL.children('input').focus();
                
            } else if ($("#rdCode").is(":checked")) {
                curcellGL = $("#grdGLSL td:eq(0)");//.first();
                var row = $(curcellGL).parent();
                A.SelectSetGLRowValue(row);
                //alert(curcellGL.attr("class"));
                curcellGL.children('input').focus();
                
            }
           
            
            
        }
    };

    this.SelectSetGLRowValue = function (row) {
        $("[id*=grdGLSL] tr").each(function (rowindex) {
            if ($(this)[0] != row[0]) {
                $("td", this).removeClass("selected_row");
                $("td", this).children('input').removeClass("selected_row");
            }
        });

        $("td", row).each(function () {
            if (!$(this).hasClass("selected_row")) {
                $(this).addClass("selected_row");
                $(this).children('input').addClass("selected_row");
                //$("input", row).each(function () {
                //    $(this).addClass("selected_row");
                //});
                
            } else {
                $(this).removeClass("selected_row");
                $(this).children('input').removeClass("selected_row");
                //$("input", row).each(function () {
                //    $(this).removeClass("selected_row");
                //});
            }
        });

        //var ValGLSL = row.children("td:eq(1)").text();
        var ValGLSL = ($("#rdName").is(":checked") ? row.children("td:eq(1)").children('input').val() : $("#rdCode").is(":checked")?row.children("td:eq(0)").children('input').val():"");
        $("#txtGLSLDescSearch").val(ValGLSL);
    };
    this.GrdGLSLKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        var c = "";
        
        if (iKeyCode == 38) {
            if ($("#rdName").is(":checked")) {
                c = curcellGL.closest('tr').prev().find('td:eq(1)');
            } else if ($("#rdCode").is(":checked")) {
                c = curcellGL.closest('tr').prev().find('td:eq(0)');
            }
             

        } else if (iKeyCode == 40) {
            // Down Arrow
            if ($("#rdName").is(":checked")) {
                c = curcellGL.closest('tr').next().find('td:eq(1)');
            } else if ($("#rdCode").is(":checked")) {
                c = curcellGL.closest('tr').next().find('td:eq(0)');
            }
            //c = curcellGL.closest('tr').next().find('td:eq(1)');
        }
       
         if (c.length > 0) {
             curcellGL = c;
             var row = $(curcellGL).parent();
             A.SelectSetGLRowValue(row);
             curcellGL.focus();
         }

       


    };

    this.getValidator = function () {
        $("#frmEcom").validate();
        $("#txtYearMonth").rules("add", { required: true,minlength:6,maxlength:6,number:true,numeric:true, messages: { required: "Please enter year month" } });
        $("#ddlVchType").rules("add", { required: true, messages: { required: "Please select voucher type" } });
        $("#txtVoucherDate").rules("add", { required: true, dpDate: true,vchcheck:true, messages: { required: "Please enter voucher date" } });
        //$("#txtYearMonth").valid();
        //$("#ddlVchType").valid();
        //$("#txtVoucherDate").valid();
        return $("#frmEcom").validate();
    };
    
    this.YearMonthChanged = function () { 
        if ($("#txtYearMonth").val() == "") {
            return false;
        }
        $(".loading-overlay").show(); //B.SectorID
        var B = window.voucherInfo.voucherForm.VoucherMast;
        var E = "{YearMonth:'" + $.trim($("#txtYearMonth").val()) + "',SectorID:" + $("#ddlUserSector").val() + "}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/GetVchDate',
            data: E,
            //async: false,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ret = JSON.parse(response.d);
                if (parseInt(ret.Status) == 1) {
                   
                    //Year Month Add date display.
                    $("#txtVoucherDate").val(ret.VchDate); 
                    //SetVchDTP( $("#txtVoucherDate").val());


                    //==================  Start Code For Set DTP in voucher Date ======================================//

                    var currentTime = new Date();
                    // First Date Of the month 
                    var startDateFrom = new Date(currentTime.getFullYear(), currentTime.getMonth(), 1);
                    // Last Date Of the Month 
                    var startDateTo = new Date(currentTime.getFullYear(), currentTime.getMonth() + 1, 0);


                    var month = startDateFrom.getMonth() + 1;

                    var yearMonth = $("#txtYearMonth").val();
                    var get_Month = parseInt(yearMonth.substring(4, 6));
                    var get_Year = parseInt(yearMonth.substring(0, 4));
                 
                    if (get_Month.toString().length == 1) { get_Month = '0' + get_Month; }
                    var dd1 = new Date(get_Month + '/' + '01/' + get_Year);
                   
                    var startDateFrom1 = new Date(dd1.getFullYear(), dd1.getMonth(), 1);
                    var startDateTo1 = new Date(dd1.getFullYear(), dd1.getMonth() + 1, 0);
                    if (parseInt(month) <= parseInt(get_Month) )
                    {
                        //startDateFrom1 = startDateFrom; // Code Hide from Shrawan discussion with Mr. Mantu Sir WBFC
                    }

                    $('#txtVoucherDate').datepicker({
                        autoOpen: false,
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        closeText: 'X',
                        showAnim: 'drop',
                        showButtonPanel: true,
                        duration: 'slow',
                        dateFormat: 'dd/mm/yy',   
                        minDate: startDateFrom1,
                        maxDate: startDateTo1
                    , onSelect: function (event) {
                        if ($('#txtVoucherDate').valid()) {
                            $('#txtVoucherDate').datepicker("hide");
                            $("#lblAccDesc_0").focus();
                        } else {
                            $('#txtVoucherDate').focus();
                        }
                    }
                    });

                    //==================  End Code For Set DTP in voucher Date ======================================//

                    //Current Date Display code here
                    //var todaydate = new Date();
                    //var day = todaydate.getDate();
                    //var month = todaydate.getMonth() + 1;
                    //var year = todaydate.getFullYear();
                    //var datestring = day + "/" + month + "/" + year;
                    //$("#txtVoucherDate").val(datestring);

                    $("#ddlVchType").focus();
                    GetMaxSchemeID();

                } else if (parseInt(ret.Status) == 0) {
                    $("#txtVoucherDate").val('');
                    alert(ret.Info);
                    $("#txtYearMonth").focus();
                    //return false
                }


                $(".loading-overlay").hide();
                //return false;
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
                return false;
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    };
    this.SetMasterFormValues = function () {
        var B = window.voucherInfo.voucherForm;
        var C = B.VoucherMast;
        C.NARRATION = $.trim($("#txtNarration").val()).toString().toUpperCase();
        C.YearMonth = $.trim($("#txtYearMonth").val());
        C.VoucherType = $.trim($("#ddlVchType").val());
        C.VCHDATE = $.trim($("#txtVoucherDate").val());
        C.STATUS = 'D';
        C.VCHAMOUNT = $.trim($("#txtTotalDebit").val());
        //alert(JSON.stringify(B));
        A.CallSaveVoucher();
    };
    this.CallSaveVoucher = function () {
        $(".loading-overlay").show();
        var B = {};
        //alert('b empty');
        B = window.voucherInfo.voucherForm;
        //alert(JSON.stringify(B));
                
        //alert('b added');

        //=================== Start Code For Check Instrument Date from voucher date ============================//
        var VchDetData = {};   
        var chk = '0';
        VchMstData = B.VoucherMast;
        //alert(JSON.stringify(VchMstData));    
        VchDetData = B.VoucherDetail;
        $.each(VchDetData, function (index, result) {
            $.each(result, function (index1, DataInst) {
                for (var k in DataInst) {
                    var valInstDate = DataInst[k].InstDT;
                    var valvoucherdate = $("#txtVoucherDate").val();
                    if (valInstDate != undefined) {
                        valInstDate = valInstDate.split('/');
                        valvoucherdate = valvoucherdate.split('/');
                        var newvalInstDate = new Date(valInstDate[2], valInstDate[1], valInstDate[0]);
                        var newvalvoucherdate = new Date(valvoucherdate[2], valvoucherdate[1], valvoucherdate[0]);
                    
                        if (Date(newvalInstDate) > Date(newvalvoucherdate)) {
                        alert('Instrument Date (' + DataInst[k].InstDT + ') Should Be Less Than Or Equal To \n Voucher Date (' + $('#txtVoucherDate').val() + ')...!');
                        chk = '1';
                        break;
                        return false; 
                    }
                    }
                }
            });
            
        });

        if (chk == '1') { //Instrument Date Should Be Less Than Or Equal To Voucher Date
            $(".loading-overlay").hide();
            $('#lblAccDesc_0').focus();
            return false;
        }
        //=================== End Code For Check Instrument Date from voucher date ============================//

        var E = "{\'VoucherForm\':\'" + JSON.stringify(B).replace("'", "$") + "\'}";
        var C = JSON.stringify(E);
        //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/SaveVoucher',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.voucherInfo.voucherForm = JSON.parse(D.d);
                var B = window.voucherInfo.voucherForm;
                $(".loading-overlay").hide();
                if (B.SessionExpired == "N") {
                    //alert(B.EntryType);
                    if (B.EntryType == "I") {
                        $("#txtVoucherNo").val(B.VoucherMast.VoucherNo);
                        alert("Voucher Saved Successfully. Voucher No is '" + B.VoucherMast.VoucherNo + "'.");
                        //location.href = "ACC_VCHMaster.aspx";
                        A.CallModalConfirmReport();
                        return;
                    }
                    
                    else if (B.EntryType == "E") {
                        if (B.SearchResult == "U") {
                            alert(B.Message);
                            //location.href = "ACC_VCHMaster.aspx";
                            A.CallModalConfirmReport();
                            return;
                        } else if (B.SearchResult == "F") {
                            alert(B.Message);
                            //location.href = "ACC_VCHMaster.aspx";
                            return;
                        } else if (B.SearchResult == "R") {
                            alert(B.Message);
                            //location.href = "ACC_VCHMaster.aspx";
                            return;
                        }
                    }
                } else {
                    alert("Your Session Expired. Login Again");
                    location.href = "Default.aspx";
                    return;
                }
                
               
                //A.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);
                //A.ClearBookingDetails();
                //if (A.checkMasterDetail()) {
                //    $("#cmdSave").removeAttr("disabled");
                //} else {
                //    $("#cmdSave").attr("disabled", "disabled");
                //}
                
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.ShowNarationDialog = function () {
        $("#dialogNaration").dialog({
            title: "Enter Narration",
            resizable: false,
            height: 240,
            
            modal: true,
            buttons: {
                Ok: function () {
                    if ($.trim($("#txtNarration").val()) != "") {
                        $("#cmdSave").removeAttr("disabled");
                        A.SetMasterFormValues();
                        $("#dialogNaration").dialog('close');
                    } else {
                        alert("Enter Narration Before Saving.");
                        return false;
                    }
                    
                }
            },
        });
    };

    

    this.SaveClicked = function () {
        if (A.validator.form()) {
            var B = window.voucherInfo.voucherForm;
            var SectorID = B.VoucherMast.SectorID;
            var C = WBFC.Utils.fixArray(B.VoucherDetail);
            var D = WBFC.Utils.fixArray(B.tmpVchInstType);
            var E = WBFC.Utils.fixArray(B.tmpVchFA);
            if (C == "") {
                alert("Please add voucher detail before saving.");
                $("#lblAccDesc_0").focus();
                return false;
            }
            var TotalDebit = 0;
            var TotalCredit = 0;
            for (var i in C) {

                if (C[i].DRCR == "D") {
                    TotalDebit = parseFloat(TotalDebit) + parseFloat(C[i].AMOUNT);
                } else if (C[i].DRCR == "C") {
                    TotalCredit = parseFloat(TotalCredit) + parseFloat(C[i].AMOUNT);
                }

            }
            
            $("#txtTotalDebit").val(TotalDebit);
            $("#txtTotalCredit").val(TotalCredit);

            if (TotalCredit > 0 && TotalDebit > 0 && TotalCredit == TotalDebit) {
                A.ShowConfirmSaveDialog();
            } else {
                alert("Total Debit must be equal to Total Credit and Greater than zero(0). Cannot Save the Voucher");
                return false;

            }
        } else {
            alert("Required Field not entered");
            A.validator.focusInvalid();
            return false;
        }
        
    };
    this.UpdateClicked = function () {
        var B = window.voucherInfo.voucherForm;
        if (B.EntryType == "E" && B.SearchResult == "Y" && B.VoucherMast.VCHNo>0) {
            A.SaveClicked();
        }
    };  
    this.SetYearMonth = function () {
        var sectorid = $("#ddlUserSector").val();
       
        var B = window.voucherInfo.voucherForm.VoucherMast;
        var E = "{sectorid:" + sectorid + "}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SetYearMon',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ret = JSON.parse(response.d);
               
                $("#txtYearMonth").val(ret.YEAR_MONTH);
                A.YearMonthChanged();         
                $(".loading-overlay").hide();
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };

    this.SearchKeyDown = function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;

        if (iKeyCode == 13) {
            A.GetSearchResult();
        }
    }
    this.GetSearchResult = function () {
        if ($.trim($("#txtYearMonth").val()) == "") {
            alert("Please enter year month to search voucher");
            return false;
        }
        if ($.trim($("#ddlVchType").val()) == "") {
            alert("Please select voucher type to search voucher");
            return false;
        }

        if ($.trim($("#txtVoucherNo").val()) == "") {
            alert("Please enter voucher no to search voucher");
            return false;
        }
        $(".loading-overlay").show();
        var B = {};
        //alert('b empty');
        B = window.voucherInfo.voucherForm;
        //alert('b added');
        var E = "{\'VoucherForm\':\'" + JSON.stringify(B) + "\',YearMonth:'" + $("#txtYearMonth").val() + "',VchType:" + $("#ddlVchType").val() + ",VchNo:'" + $("#txtVoucherNo").val() + "'}";
       // alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/GetSearchVoucherResult',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $(".loading-overlay").hide();
                //alert(JSON.stringify(response.d));
                window.voucherInfo.voucherForm = JSON.parse(response.d);
            
                var B = window.voucherInfo.voucherForm; //alert(JSON.stringify(B));
                //alert(JSON.stringify(B));
                if (B.SearchResult == "Y") {
                    if (B.EntryType == "E") {
                        A.ShowMasterDetail(B);
                    }
                } else {
                    alert("No Record Found for this Year Month, Voucher Type and Voucher No");
                    location.href = "ACC_VCHMaster.aspx";
                    return;
                }
                
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    }

   
    
    this.ShowMasterDetail = function (B) {
        
        var C = WBFC.Utils.fixArray(B.VoucherDetail); //alert(JSON.stringify(C));
        A.ShowMaster(B.VoucherMast);
        A.ShowVoucherDetail(C,"S");

    };
    var GlobalSTATUS = '';
    this.ShowMaster = function (E) {
        $("#txtYearMonth").val(E.YearMonth);
        $("#ddlVchType").val(E.VoucherType);
        $("#txtVoucherDate").val(E.VCHDATE);
        $("#txtVoucherNo").val(E.VoucherNo);
        $("#txtNarration").val(E.NARRATION.replace("$", "'"));
        $("#txtNarrationval").val(E.NARRATION.replace("$","'"));
        if (E.STATUS == "D") {
            //$("#cmdDelete").prop("disabled", false);
            //$("#cmdSave").prop("disabled", false);
            $("#cmdDelete").show();
            $("#cmdUpdate").show();
            $("#cmdSave").hide();
        } else {
            //$("#cmdDelete").prop("disabled", true);
            //$("#cmdSave").prop("disabled", true);
            GlobalSTATUS = 'Complete';
            $("#cmdDelete").hide();
            $("#cmdUpdate").hide();
            $("#cmdSave").hide();
        }
    };

    this.GetDeleteVoucher = function () {
        if (confirm("Are You sure you want to delete this voucher?")) {
            var B = window.voucherInfo.voucherForm;
            if (B.EntryType == "E") {
                if (B.VoucherMast.VCHNo > 0) {
                    if (B.VoucherMast.STATUS == "D") {
                        A.CallDeleteVoucher();
                    } else {
                        alert("Voucher Has been finalised. Cannot delete the voucher");
                        return false;
                    }
                } else {
                    alert("Search a proper voucher to delete.");
                    return false;
                }
            } else {
                alert("Search a Voucher to delete.");
                return false;
            }
        } else {
            return false;
        }

    };
    this.CallDeleteVoucher = function () {
        $(".loading-overlay").show();
        var B = {};
        //alert('b empty');
        B = window.voucherInfo.voucherForm;
        //alert('b added');
        var E = "{\'VoucherForm\':\'" + JSON.stringify(B) + "\'}";
        //alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/DeleteVoucher',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $(".loading-overlay").hide();
                window.voucherInfo.voucherForm = JSON.parse(response.d);

                var B = window.voucherInfo.voucherForm;
                //alert(B);
                if (B.SearchResult == "D") {
                    alert(B.Message);
                    location.href = "ACC_VCHMaster.aspx";
                    return;
                } else if (B.SearchResult == "F") {
                    alert(B.Message);
                    //location.href = "ACC_VCHMaster.aspx";
                    return;
                } else if (B.SearchResult == "R") {
                    alert(B.Message);
                    //location.href = "ACC_VCHMaster.aspx";
                    return;
                }

            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    }

    this.ShowReport = function () {
        $(".loading-overlay").show();
        var FormName = '';
        var ReportName = '';
        var ReportType = '';
        if ($("#rdRep1").is(":checked")) {
            FormName = "ACC_VCHMaster.aspx";
            ReportName = "VOUCHER";
            ReportType = "Voucher Report";
        } else {
            FormName = "ACC_VCHMaster.aspx";
            ReportName = "eadvice";
            ReportType = "Exchange Advise";
        }
        
        
        var B = {};
        //alert('b empty');
        B = window.voucherInfo.voucherForm;
       
        var E = '';
        E = "{\'VoucherForm\':\'" + JSON.stringify(B) + "\', FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "'}";//, Old_SL_ID:'" + Old_SL_ID + "'
        $.ajax({
            type: "POST",
            url: pageUrl + '/SetReportValue',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",            
            success: function (response) {
                window.voucherInfo.voucherForm = JSON.parse(response.d);
                $(".loading-overlay").hide();
                var B = window.voucherInfo.voucherForm;
                //alert(B.Message);
                if (B.Message == "Show") {
                    window.open("ReportView.aspx?E=Y");
                }
                
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                //alert('Some Error Occur');
                //$("#button-personal-details").prop("disabled", false);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);

                $(".loading-overlay").hide();
            }
        });
    }
    this.CallModalConfirmReport = function () {
        $("#spanTxt").html("Show Voucher and Exchage Advise");
        $("#dialog-report").dialog({
            title: "Select The Report Type You Want to view and Click Show Report",
            resizable: false,
            height: 250,
            width: 380,
            modal: true,            
            buttons: {
                "OK": function () {
                    $(this).dialog("close");
                    location.href = "ACC_VCHMaster.aspx";
                }
            },
            close: function (event, ui) {
                A.ClosePopReport();
            },
            open: function () {
                //$(this).siblings('.ui-dialog-buttonpane').find('button:eq(1)').focus();
                //var $dialog = $( this );

                //if( ! $dialog.data( 'titleCloseBound' ) )
                //{
                //    $dialog
                //        .data( 'titleCloseBound', true ) //flag as already bound
                //        .closest( 'div.ui-dialog' ) //traverse up to the outer dialog wrapper
                //            .find( 'a.ui-dialog-titlebar-close' ) //search within it for the X
                //                .bind( 'click', function( e ) //bind it
                //                {
                //                    alert( 'hi' );
                //                    e.preventDefault();
                //                    location.href = "ACC_VCHMaster.aspx";
                //                } );
                //}
            }
        });
    };
    this.ClosePopReport = function () {
        location.href = "ACC_VCHMaster.aspx";
    }
};

WBFC.VoucherInfo = function () {
    var A = this;
    this.voucherForm;
    this.init = function () {
        this.voucherForm = JSON.parse($("#voucherJSON").val());
       // alert(JSON.stringify(this.voucherForm));
        this.masterInfo = new WBFC.VoucherMaster();
        this.masterInfo.init();       

    };
    
    this.findAndRemove = function (array, property, value) {
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;

            }
        });
    };

    this.findAndRemove3 = function (array, property, value, property2, value2, property3, value3) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2 && result[property3] == value3) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
};

function GetMaxSchemeID() {
    if ($("#txtYearMonth").val().trim() == '') { return false;}
    if ($("#ddlVchType").val() == '') { return false;}
    if ($("#ddlUserSector").val() == '') { return false; }

    var W = "{Yearmonth:'" + $("#txtYearMonth").val() + "',VchType:" + $("#ddlVchType").val() + ",SecId:" + $("#ddlUserSector").val() + "}";
    $.ajax({
        type: "POST",
        url: "ACC_VCHMaster.aspx/GET_MaxSchemeID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",   
        success: function (data) {
            var MaxVchNo = jQuery.parseJSON(data.d);
            document.getElementById("txtVoucherNo").placeholder = 'Max Voucher No. :- ' + MaxVchNo;
        },
    });
}