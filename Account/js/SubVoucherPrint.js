﻿
var hdnGLID = '';
var hdnSLID = '';
var hdnSubType = '';
var hdnAllID = '';
$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    $("#txtSL").attr('disabled', true);
    $('#txtFromDt, #txtToDt').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'  
    });

    $("#imgFromDt").click(function () {
        $('#txtFromDt').datepicker("show");
        return false;
    });
    $("#imgSanctionDT").click(function () {
        $('#imgToDt').datepicker("show");
        return false;
    });
    SearchGL();
});
function SearchGL() {
    $("#txtGL").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SubVoucherReport.aspx/GLAutoCompleteData",
                data: "{GL:'" + $('#txtGL').val().trim() + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[3],
                                val: item
                            }
                        }))
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            var arr = i.item.val.split("|");
            hdnGLID=arr[0];
            hdnSLID=arr[1];
            hdnSubType=arr[2];
            if (hdnSubType != "") {
                $("#txtSL").attr('disabled', false);
                SearchSL();
            } else {
                $("#txtSL").attr('disabled', true);

            }
        }
    });
}

function SearchSL() {
    $("#txtSL").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "SubVoucherReport.aspx/SLAutoCompleteData",
                data: "{SL:'" + $('#txtSL').val().trim() + "', SubType:'" + hdnSubType + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[1],
                                val: item
                            }
                        }))
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            var arr = i.item.val.split("|");
            hdnAllID=arr[0];
        }
    });
}

$(document).ready(function () {

    $('#txtGL').keydown(function (evt) {                /// for Backspace
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            hdnGLID='';
            hdnSLID='';
            hdnSubType='';

            $("#txtSL").attr('disabled', true);
            $('#txtSL').val('');
            hdnAllID='';
        }
    });
    $('#txtGL').keyup(function (evt) {                  /// fro Delete     
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            hdnGLID = '';
            hdnSLID = '';
            hdnSubType = '';

            $("#txtSL").attr('disabled', true);
            $('#txtSL').val('');
            hdnAllID = '';
        }
    });

    $("#txtGL").click(function () {                   /// for all select
        var txtArea = document.getElementById("txtGL");
        var selectedText;
        if (txtArea.selectionStart != undefined) {
            var startPosition = txtArea.selectionStart;
            var endPosition = txtArea.selectionEnd;
            selectedText = txtArea.value.substring(startPosition, endPosition);
        }
        //alert("You selected :" + selectedText);
        //alert("You selected: " + selectedText.length);
        
        if ($("#txtGL").val().length == selectedText.length) {
            hdnGLID = '';
            hdnSLID = '';
            hdnSubType = '';

            $("#txtSL").attr('disabled', true);
            $('#txtSL').val('');
            hdnAllID = '';
        }
        //else {
        //    $("#txtSL").attr('disabled', false);
        //}
    });



    $('#txtSL').keydown(function (evt) {               /// for Backspace
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            $('#txtSL').val('');
            hdnAllID = '0';
        }
    });
    $('#txtSL').keyup(function (evt) {                  /// for Delete     
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            $('#txtSL').val('');
            hdnAllID = '0';
        }
    });
});


$(document).ready(function () {
    $('#txtFromDt').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtToDt').focus();
            return false;
        }

    });

    $('#txtToDt').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtGL').focus();
            return false;
        }

    });

    $('#txtGL').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtSL').focus();
            return false;
        }

    });

    $('#txtSL').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#btnPrint').focus();
            return false;
        }

    });
});



$(document).ready(function () {
    $("#btnPrint").click(function (event) {
        if ($("#txtFromDt").val() == '')
        {
            alert("Please select From Date");
            $("#txtFromDt").focus();
            return false;
        }
        if ($("#txtToDt").val() == '') {
            alert("Please select To Date");
            $("#txtToDt").focus();
            return false;
        }
        if (hdnGLID == '') {
            alert("Please select GL");
            $("#txtGL").focus();
            return false;
        }
        ShowReport();
    });
});
function ShowReport() {

    $(".loading-overlay").show();
    var FormName = '';
    var ReportName = '';
    var ReportType = '';
    
        FormName = "SubVoucherReport.aspx";
        ReportName = "SubVoucher";
        ReportType = "Sub Voucher Report";
        
        var FromDT = $('#txtFromDt').val();
        var ToDT = $('#txtToDt').val();

    //    var FDate = $("#txtFromDt").val().split("/");
    //FDate = FDate[2] + "-" + FDate[1] + "-" + FDate[0];

    //var TDate = $("#txtToDt").val().split("/");
    //TDate = TDate[2] + "-" + TDate[1] + "-" + TDate[0];

    var GLID = hdnGLID;
    var SLID = hdnSLID;
    var SubType = hdnSubType;
    var SL_SUB_Code = hdnAllID;
        
    var E = '';
    E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "', FromDT:'" + FromDT + "', ToDT:'" + ToDT + "',  GLID:'" + GLID + "',  SLID:'" + SLID + "', " +
        "  SubType:'" + SubType + "', SL_SUB_Code:'" + SL_SUB_Code + "'}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/SetReportValue',
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var show = response.d;

            if (show == "OK") {
                window.open("ReportView.aspx?E=Y");
            }

        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
            $(".loading-overlay").hide();
        },
        failure: function (response) {
            alert(response.d);

            $(".loading-overlay").hide();
        }
    });
}

