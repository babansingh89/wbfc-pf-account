

var maxdate;
var maxStartDays;

var maxdt = new Date(-100000000 * 86400000);
var mindt = new Date(100000000 * 86400000);

function compare(key, value) {
    if (key == "BookingDate") {
        var arr = value.split("/");
        var dt = new Date();
        dt.setFullYear(arr[2], (arr[1] - 1), arr[0]);
        //alert("arrdt" + dt.getTime() + " min date" + mindt.getTime());
        //alert("   " + dt.getTime() < mindt.getTime());
        dt.setHours(0, 0, 0, 0);
        if (dt.getTime() < mindt.getTime())
            mindt = dt;
    }
}
function traverse(obj, fun) {
    for (prop in obj) {
        fun.apply(this, [prop, obj[prop]]);
        if (typeof (obj[prop]) == "object") {
            traverse(obj[prop], fun);
        }
    }
}


$(document).ready(function () {
    $(".loading-overlay").show();
    $(".loading-overlay").hide();


    var BookingStartDays = $("#BookingStartDays").val();
    var maxopen = $("#MaxOpenDays").val();
    maxStartDays = new Date();
    maxdate = new Date();
    var cdt = $("#curdt").val();
    var curdate = new Date(cdt);
    if (maxopen != null) {
        maxdate.setTime(curdate.getTime() + maxopen * 24 * 60 * 60 * 1000);
    };
    if (BookingStartDays != null) {
        maxStartDays.setTime(curdate.getTime() + BookingStartDays * 24 * 60 * 60 * 1000);
    }
    //alert("cdt  ===  "+cdt+"  ===  curdate " + curdate + "   ===   add days " + maxopen + " =====   added days    " + maxdate);
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        minDate: 0,
        maxDate: maxStartDays
   });

        $('#txtToDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            showButtonPanel: true,
            duration: 'slow',
            minDate: 0,
            maxDate: maxdate
   });

        $("#btnFromDatePicker").click(function () {
            $('#txtFromDate').datepicker("show");
            return false;
        });

        $("#btnToDatePicker").click(function () {
            $('#txtToDate').datepicker("show");
            return false;
        });

        $("#txtFromDate").change(function () {
            check(this);
        });

        $("#txtToDate").change(function () {
            check(this);
        });


    window.blockPage = new WBFDC.BlockPage();
    window.blockPage.init();
    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });



});
function lpad(originalstr, length, strToPad) {
    while (originalstr.length < length)
        originalstr = strToPad + originalstr;
    return originalstr;
}
function rpad(originalstr, length, strToPad) {
    while (originalstr.length < length)
        originalstr = originalstr + strToPad;
    return originalstr;
}
function dateDiff() {
    var frmdate = null, toDate = null, totalDays = "";
    if ($.trim($("#txtFromDate").val())) {
        frmdate = new Date($("#txtFromDate").datepicker('getDate'));
    }
    if ($.trim($("#txtToDate").val())) {
        toDate = new Date($("#txtToDate").datepicker('getDate'));
    }

    if ((frmdate != null && toDate != null))
        totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;
    //alert(frmdate+'   '+ toDate+'  '+ totalDays);
    return totalDays;
}
function check(ch) {
    if (ch.id == "txtFromDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtToDate").datepicker("option", "minDate", '-100Y');
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtToDate").datepicker("option", "minDate", testm);
        }
    } else if (ch.id == "txtToDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtFromDate").datepicker("option", "maxDate", maxStartDays);
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtFromDate").datepicker("option", "maxDate", maxStartDays);
        }
    }
}



WBFDC.MasterInfo = function () {
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {
    this.validator = A.getValidator();
    //this.masterValidator = A.getmasterValidator();

        $("#txtFromDate").change(A.BookingDateChanged);
        $("#txtToDate").change(A.BookingDateChangedTo); ;
        $("#ddlMerchant").change(A.MerchantChange);
        $("#cmdSave").click(A.AddClicked);
        $("#btnSubmit").click(A.SubmitClicked);
        $("#cmdSubmit").click(A.SubmitMaster);
           //$(".DefaultButton").click(A.AddClicked);
    };
    this.MerchantChange = function () {
        window.blockPage.PopulateProperty();
    };
    this.getValidator = function() {
        $("#frmEcom").validate();
        $("#ddlProperty").rules("add", { required: true, messages: { required: "Please enter property name"} });
        $("#txtFromDate").rules("add", { required: true, messages: { required: "Please enter from date"} });
        $("#txtToDate").rules("add", { required: true, messages: { required: "Please enter to date"} });
        $("#txtbookedFor").rules("add", { required: true, messages: { required: "Please enter booked for"} });
        $("#txtblockReason").rules("add", { required: true, messages: { required: "Please enter blocking reason"} });
        return $("#frmEcom").validate(); 
    };
    this.AddClicked = function () {
        if (A.validator.form()) {
            A.setFormValues();
        }
    };
    this.RoomAddClicked = function(){
       window.blockPage.SubmitForm();
    };
    this.SubmitClicked = function () {
       window.blockPage.addAfterValidate();
    };
    this.SubmitMaster = function () {
        window.blockPage.SubmitForm();
    };
    this.setFormValues = function () {
        var B = window.blockPage.blockForm;
        B.BlockMaster.BlockedFor = $("#txtbookedFor").val();
        B.BlockMaster.BlockedReason = $("#txtblockReason").val();
        window.blockPage.showRoomAvailablity();
    };
    this.BookingDateChanged = function () {
        //alert('changed');
        //window.enquiryPage.PopulateTariff();
        check(this);
        var tDays = dateDiff();
        if (tDays != "")
            $("#spanNOD").html(tDays + " Days ");
        else
            $("#spanNOD").html("");
    };
    this.BookingDateChangedTo = function () {
        //alert('changed');
        //A.PopulateTariff();
        check(this);
        var tDays = dateDiff();
        if (tDays != "")
            $("#spanNOD").html(tDays + " Days ");
        else
            $("#spanNOD").html("");
    };
  
};



WBFDC.BlockPage = function () {
    var A = this;
    this.blockForm;
    
    this.init = function () {
        this.blockForm = JSON.parse($("#blockJSON").val());
        //alert(JSON.stringify(this.blockForm));
        this.masterInfo = new WBFDC.MasterInfo();
        this.masterInfo.init();
        var B = A.blockForm;
        var C = B.BlockDetail;
    };
    this.RoomBookedCheked = function (ch) {
            var $this = $(ch);
            var PropID = parseInt($("#ddlProperty").val());
             
            var idtext = $this.attr("id").split("_");
            if ($this.is(":checked")) {

                var dt = idtext[2];
                
                dt = dt.replace("/", "");
                dt = dt.replace("/", "");
                var $i = $("#lr_" + dt);
                    
                    var name = $.trim($this.parents('tr:first').find('th:first').text());
                   
                    var B = window.blockPage.blockForm;
                    var C = WBFDC.Utils.fixArray(B.blockPropertyRoom);
                    var D = WBFDC.Utils.fixArray(B.tempPropertyRoom);

                    var found = false;
                    var foundD = false;
                    //////////////if (C == "" && D == "") {
                        
                    //////////////    C.push({
                    //////////////        PropertyID: PropID,
                    //////////////        BlockDate: idtext[2],
                    //////////////        TotalRoomsBlocked: 1
                    //////////////    });
                        
                    //////////////    D.push({
                    //////////////        PropertyID: PropID,
                    //////////////        BlockDate: idtext[2],
                    //////////////        TotalRoomsBlocked: 1
                    //////////////    });
                        
                    //////////////} else {
                    //////////////    for (var i in C) {
                    //////////////        if (C[i].PropertyID == PropID && C[i].BlockDate == idtext[2]) {
                    //////////////            C[i].TotalRoomsBlocked += 1;
                    //////////////            found = true;
                    //////////////        }
                    //////////////    }
                        
                    //////////////    for (var j in D) {
                    //////////////        if (D[j].PropertyID == PropID && D[j].BlockDate == idtext[2]) {
                    //////////////            D[j].TotalRoomsBlocked += 1;
                    //////////////            foundD = true;
                    //////////////        }
                    //////////////    }
                        
                    //////////////    if (!found) {
                    //////////////        C.push({
                    //////////////            PropertyID: PropID,
                    //////////////            BlockDate: idtext[2],
                    //////////////            TotalRoomsBlocked: 1
                    //////////////        });
                    //////////////    }
                    //////////////    if (!foundD) {
                    //////////////        D.push({
                    //////////////            PropertyID: PropID,
                    //////////////            BlockDate: idtext[2],
                    //////////////            TotalRoomsBlocked: 1
                    //////////////        });
                    //////////////    }
                    //////////////}
                    B.dateRoomID.push({
                        BlockDate: idtext[2],
                        AccomodationDetailID: parseInt(idtext[1]),
                        AccomodationName: name
                    });
                    //alert(JSON.stringify(B));
                    
            } else {
                var dt = idtext[2];
                dt = dt.replace("/", "");
                dt = dt.replace("/", "");
                var $i = $("#lr_" + dt);
                
                var B = window.blockPage.blockForm;
                A.findAndRemove2(B.dateRoomID, "BlockDate", idtext[2], "AccomodationDetailID", parseInt(idtext[1]));
                ////////////var C = WBFDC.Utils.fixArray(B.blockPropertyRoom);
                ////////////var D = WBFDC.Utils.fixArray(B.tempPropertyRoom);
                ////////////for (var i in C) {
                ////////////    if (C[i].PropertyID == PropID && C[i].BlockDate == idtext[2]) {
                ////////////        C[i].TotalRoomsBlocked -= 1;
                        
                ////////////        if (C[i].TotalRoomsBlocked == 0) {
                ////////////            C.splice(i, 1);
                ////////////            break;
                ////////////        }
                ////////////    }
                ////////////}
                ////////////for (var j in D) {
                ////////////    if (D[j].PropertyID == PropID && D[j].BlockDate == idtext[2]) {
                ////////////        D[j].TotalRoomsBlocked -= 1;
                ////////////        if (D[j].TotalRoomsBlocked == 0) {
                ////////////            D.splice(j, 1);
                ////////////         break;
                ////////////        }
                ////////////    }
                ////////////}
            }
        };
    this.checkDateSelected = function () {
        $(".loading-overlay").show();
        var B = window.blockPage.blockForm;
        var C = "{\'blockForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/CheckDateSelected',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var result = JSON.parse(D.d);
                //alert(JSON.stringify(result));
                var resdt = new Date(result.MinDate);
                //alert(resdt);
                //alert(+resdt + "  maxstartdays  " + +maxStartDays);
                //if (+resdt > +maxStartDays) {
                    //alert("Your selected dates are not in the range. Start date must be within " + $("#BookingStartDays").val() + " days. Please check the booking rules and regulation before moving forward.");
                   // $(".loading-overlay").hide();
               // }
                //else {
               // window.blockPage.showEnquiryDetail(window.blockPage.blockForm.BlockDetail);
                    //$(".loading-overlay").hide();
               // }


            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.checkMasterDetail = function () {
        //alert('in submit');
        var B = window.blockPage.blockForm;
        var BlockMaster = B.BlockMaster;
        var BlockID = BlockMaster.BlockID;
        var blockDetail = B.blockDetail;

        if ((parseInt(BlockID) > 0) && (blockDetail.length > 0)) {
            return true;
        } else {
            return false;
        }
    };
    this.SubmitForm = function () {
        $(".loading-overlay").show();
        var B = window.blockPage.blockForm;
        var C = "{\'blockForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SaveEnquiry',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.blockPage.blockForm = JSON.parse(D.d);
                //alert(D.d);

                $(".loading-overlay").hide();
                //A.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);
               // window.blockPage.showEnquiryDetail(window.blockPage.blockForm.BlockDetail);
                A.PageRedirection();
                window.blockPage.showBlockRoomDetail(window.blockPage.blockForm.BlockDetail);
            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.checkMasterDetail = function () {
        var B = window.blockPage.blockForm;
        var BlockMaster = B.BlockMaster;
        var BlockID = BlockMaster.BlockID;
        var BlockDetail = B.BlockDetail;

        if ((parseInt(BlockID) > 0) && (BlockDetail.length > 0)) {
            return true;
        } else {
            return false;
        }
    };
    this.showRoomAvailablity = function (tot) {
        $(".loading-overlay").show();
          var C = "{dateFrom:'" + $("#txtFromDate").val() + "',dateTo:'" + $("#txtToDate").val() +
            "',propertyID:" + $("#ddlProperty").val() + ",TotalRoomAllowed:" + $("#TotalRoomsAllowed").val() + "}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/ShowAvaliablity',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                //window.blockPage.blockForm = JSON.parse(D.d);
                //alert(JSON.parse(D.d));
                //alert("Before 'divroomTable'");
                $("#divroomTable").html(JSON.parse(D.d));
                $(".loading-overlay").hide();
                //callfixtable();
                A.getOtherValue(tot);
                $("#loadBookingDet").show();
                //A.PageRedirection();
            },
            error: function (response) {
                alert('Some Error Occur');
                alert('Error In ShowAvaliablity');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.getOtherValue = function (tot) {
        var otherHtml = '';
        ////        var B = window.blockPage.blockForm;
        ////        var totBooking = $("#TotalBookingNo").val();
        ////        var totAdded = B.enquiryMaster.TotalNoOfRooms;
        ////        var tot = (parseInt(totBooking)) - parseInt(totAdded);
        otherHtml += " Select Prefered Room(s) of " + $.trim($("#ddlProperty option:selected").text());
        //otherHtml += "<div style='font-size:11px;color:maroon;float:right;margin-right:20px;'>Maximum <span id='allowed' >" + tot + "</span> rooms.</div>"
        $("#divCaption").html(otherHtml);
    };
    this.PageRedirection = function () {
        var B = window.blockPage.blockForm;
        if (B.SMSResult == "SUCCESS") {
            //window.location.href = "EnquiryComplete.aspx?EnquiryTokenID=" + B.enquiryMaster.EnquiryTokenID;
            window.location.href = "BlockRoomDetails.aspx?BlockID=" + B.BlockMaster.BlockID;
            
         }
    };
    this.PopulateControl = function (list, control) {
        //alert(JSON.stringify(list));
        if (list.length > 0) {
            control.removeAttr("disabled");
            control.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(list, function () {
                control.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
        else {
            control.empty().append('<option selected="selected" value="-1">Not available<option>');
        }
    };
    this.showMasterData = function (BlockMaster) {
        if (BlockMaster != "undefined" && BlockMaster != null) {
            $('#txtbookedFor').val(BlockMaster.BlockedFor);
            $('#txtblockReason').val(BlockMaster.BlockedReason);
        }
    };
    this.showEnquiryDetail = function (BlockDetail) {
        var html = '';
        if (BlockDetail != "undefined") {

            if (BlockDetail.length > 0) {

                html += " <table align='center' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td colspan='7' class='labelCaption'><hr style='border:solid 1px lightblue' /></td></tr>"
                    + "</table>";
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td class='th'></td>"
                    + "<td class='th'>Block Date</td>"
                    + "<td class='th'>Property Name</td>"
                   // + "<td class='th'>No of Rooms</td>"
                    + "<td class='th'>Preferred Room(s)</td>"
                    + "</tr>";

                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";


                var blkDetails = WBFDC.Utils.fixArray(BlockDetail);

                for (var i in blkDetails) {
                    html += "<tr id='BlockDetailID_" + blkDetails[i].BlockDetailID + "'>"
                                + "<td class='tr'><div><a href='" + pageUrl + '/DeleteBlockDetail' + "' onClick='return window.blockPage.removeBlockDetail(" + blkDetails[i].BlockDetailID + ");'><img src='images/delete.png' id='deleteEnquiryTokenDetID_" + blkDetails[i].BlockDetailID + "' /></a></div></td>"
                                + "<td  class='tr' >" + blkDetails[i].BlockDate + "</td>"
                                + "<td  class='tr' >" + blkDetails[i].PropertyName + "</td>"
                    // + "<td  class='tr' >" + blkDetails[i].NoOfRooms + "</td>";
                    var roomDetail = WBFDC.Utils.fixArray(blkDetails[i].blockRoomDetail);
                    var roomnos = "";

                    if (roomDetail != null && roomDetail.length > 0) {

                        for (var k in roomDetail) {
                            roomnos += roomDetail[k].AccomodationName + ", ";

                        }
                        if (roomnos != "") {
                            roomnos = $.trim(roomnos);
                            roomnos = roomnos.substring(0, roomnos.length - 1);
                        }
                    }
                    html += "<td  class='tr' >" + roomnos + "</td>";
                    html += "</tr>";
                    html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                }
                html += "</table>";

                $("#loadBookingDet").hide();
                $(".loading-overlay").hide();
                window.blockPage.showSubmitButton();
            }
            else {
                window.blockPage.hideSubmitButton();
            }
        }
        $("#divTable").empty().html(html);
        $("#loadBookingDet").hide();
        $(".loading-overlay").hide();
    };
    this.showBlockRoomDetail = function (BlockDetail) {
        var html = '';
        
        if (BlockDetail != "undefined") {
            
            if (BlockDetail.length > 0) {

                html += " <table align='center' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td colspan='7' class='labelCaption'><hr style='border:solid 1px lightblue' /></td></tr>"
                    + "</table>";
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td class='th'></td>"
                    + "<td class='th'>Block Date</td>"
                    + "<td class='th'>Property Name</td>"
                    + "<td class='th'>Preferred Room(s)</td>"
                    + "</tr>";

                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";


                var blkDetails = WBFDC.Utils.fixArray(BlockDetail);

                for (var i in blkDetails) {
                    html += "<tr id='BlockDetailID_" + blkDetails[i].BlockDetailID + "'>"
                                + "<td class='tr'><div><a href='" + pageUrl + '/DeleteBlockDetail' + "' onClick='return window.blockPage.removeBlockDetail(" + blkDetails[i].BlockDetailID + ");'><img src='images/delete.png' id='deleteEnquiryTokenDetID_" + blkDetails[i].BlockDetailID + "' /></a></div></td>"
                                + "<td  class='tr' >" + blkDetails[i].BlockDate + "</td>"
                                + "<td  class='tr' >" + blkDetails[i].PropertyName + "</td>"
                    // + "<td  class='tr' >" + blkDetails[i].NoOfRooms + "</td>";

                    var roomDetail = WBFDC.Utils.fixArray(blkDetails[i].blockRoomDetail);
                    var roomnos = "";

                    // var GuestNames = "";
                    if (roomDetail != null && roomDetail.length > 0) {

                        for (var k in roomDetail) {
                            roomnos += roomDetail[k].AccomodationName + ", ";
                            // GuestNames += roomDetail[k].enquiryName.GuestName + ", ";

                        }
                        //alert(roomnos);
                        if (roomnos != "") {
                            roomnos = $.trim(roomnos);
                            roomnos = roomnos.substring(0, roomnos.length - 1);
                        }
                        //if (GuestNames != "") {
                        // GuestNames = $.trim(GuestNames);
                        // GuestNames = GuestNames.substring(0, GuestNames.length - 1);
                        //}
                    }
                    html += "<td  class='tr' >" + roomnos + "</td>";
                    // html += "<td  class='tr' >" + GuestNames + "</td>";
                    html += "</tr>";
                    html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                }
                html += "</table>";

                $("#loadBookingDet").hide();
                $(".loading-overlay").hide();

            }
        }
        $("#divTable").empty().html(html);
        $("#loadBookingDet").hide();
        $(".loading-overlay").hide();
    };
    this.removeBlockDetail = function (BlockDetailID) {
        //alert('in remove ' + EnquiryTokenDetID);
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#BlockDetailID_" + BlockDetailID;
        //alert(removeElemTr);
        $(removeElemTr).append(ajaxLoader);
        var ajaxUrl = pageUrl + '/DeleteBlockDetail';

        var B = {};
        //alert('b empty');
        B.blockForm = window.blockPage.blockForm;

        A.findAndRemove(B.blockForm.BlockDetail, 'BlockDetailID', BlockDetailID);
        //alert(JSON.stringify(B.enquiryForm));
        //alert('b added');
        var E = "{\'blockForm\':\'" + JSON.stringify(B.blockForm) + "\',\'BlockDetailID\':\'" + BlockDetailID + "\'}";
        //var C = JSON.stringify(E);
        //alert(E);
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.blockPage.blockForm = JSON.parse(D.d);
                alert(JSON.stringify(window.blockPage.blockForm));
                $("#divTable table[id='detailTable']").remove();
                A.showEnquiryDetail(window.blockPage.blockForm.BlockDetail);
                $("#divTable table[id='prodTab']").hide();
                $("#divTable table[id='prodTab']").show("slow");

            }
        });
        return false;
    };
    this.findAndRemove = function (array, property, value) {
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;

            }
        });
    };
    this.findAndRemove3 = function (array, property, value, property2, value2, property3, value3) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2 && result[property3] == value3) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    this.findAndRemove2 = function (array, property, value, property2, value2) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    this.CloseContent = function () {
            $("#loadBookingDet").hide();
            window.blockPage.blockForm.BlockMaster = [];
            window.blockPage.blockForm.BlockDetail = [];
            return false;
    };
    this.showSubmitButton = function () {
        $(document).ready(function () {
            $("#divTable1").show();
        });
    }
    this.hideSubmitButton = function () {
        $(document).ready(function () {
            $("#divTable1").hide();
        });
    }
    this.CloseNamesContent = function () {
        $("#LoadNamesDet").hide();
        window.enquiryPage.enquiryForm.dateRoomID = [];
        window.enquiryPage.enquiryForm.tempLocationRoom = [];
        window.enquiryPage.enquiryForm.tempNames = [];
        return false;
    };
    this.captureDetails = function () {
        $(".loading-overlay").show();
        var B = {};
        B.blockForm = window.blockPage.blockForm;
        var E = "{\'blockForm\':\'" + JSON.stringify(B.blockForm) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/AddDetails',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.blockPage.blockForm = JSON.parse(D.d);
                window.blockPage.showEnquiryDetail(window.blockPage.blockForm.BlockDetail);
                $(".loading-overlay").hide();
            },
            error: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.newNamesAddDetails = function () {
        //var v = A.ValidateNameForm();
        //alert(v);
        $("#enquiry").validate();
        //alert('form  validated');
        //alert($("#enquiry").valid());
        if ($("#enquiry").valid()) {
            //A.addAfterValidate();
            A.SaveTempNames();
            A.addAfterValidate();
            //alert('validated');
        } else {

        }

        return false;

    };
    this.SaveTempNames = function () {

        var B = window.enquiryPage.enquiryForm;
        var dateRoom = WBFDC.Utils.fixArray(A.enquiryForm.dateRoomID);
        if (dateRoom == "") {
            alert('Select Prefered Rooms before adding ');
            return false;
        }

        var arr = WBFDC.Utils.fixArray(B.tempLocationRoom);

        arr = $.map(arr, function (o) { return o.TotalRoomsBooked; });
        var highest = Math.max.apply(this, arr);
        var enqName = WBFDC.Utils.fixArray(B.enquiryName);
        var tempname = []; //WBFDC.Utils.fixArray(B.tempNames);
        if (enqName == "") {
            for (var i = 0; i < highest; i++) {

                var ele1 = $("#nameTable input[name=n_" + i + "]").val();
                var ele2 = $("#nameTable input[name=a_" + i + "]").val();
                var ele3 = $("#nameTable select[name=s_" + i + "]").val();
                //alert(ele1 + " " + ele2 + " " + ele3);
                //alert(tempname);
                tempname.push({
                    GuestName: ele1,
                    Age: parseInt($.trim(ele2)),
                    Sex: ele3
                });


                //alert(JSON.stringify(tempname));
                if (enqName == "") {
                    enqName.push({
                        GuestName: ele1,
                        Age: parseInt($.trim(ele2)),
                        Sex: ele3
                    });
                } else {
                    var found = false;
                    for (var k in enqName) {
                        if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                            found = true;
                        }
                    }
                    if (!found) {
                        enqName.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });
                    }
                }
            }

        } else {
            if ($("#rdOld").is(":checked")) {
                for (var i = 0; i < enqName.length; i++) {

                    var $chk = $("#nameTable input[name=chkN_" + i + "]");
                    if ($chk.is(":checked")) {
                        var ele1 = $("#nameTable input[name=n_" + i + "]").val();
                        var ele2 = $("#nameTable input[name=a_" + i + "]").val();
                        var ele3 = $("#nameTable select[name=s_" + i + "]").val();
                        //alert(ele1 + " " + ele2 + " " + ele3);
                        //alert(tempname);
                        tempname.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });


                        //alert(JSON.stringify(tempname));
                        if (enqName == "") {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        } else {
                            var found = false;
                            for (var k in enqName) {
                                if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                    found = true;
                                }
                            }
                            if (!found) {
                                enqName.push({
                                    GuestName: ele1,
                                    Age: parseInt($.trim(ele2)),
                                    Sex: ele3
                                });
                            }
                        }
                    }
                }
                var enlength = enqName.length;
                if (enlength < highest) {
                    var remain = highest - enlength;
                    for (var j = 0; j < remain; j++) {
                        ////html += "<tr id='trj_" + j + "'>";
                        ////html += "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='nj_" + j + "' name='nj_" + j + "' value=''/></<td>"
                        ////    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='aj_" + j + "' name='aj_" + j + "' value=''/></<td>"
                        ////    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='sj_" + j + "' name='sj_" + j + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                        ////    + "<td></td>";
                        ////html += "</tr>";
                        ///
                        var ele1 = $("#nameTable input[name=nj_" + j + "]").val();
                        var ele2 = $("#nameTable input[name=aj_" + j + "]").val();
                        var ele3 = $("#nameTable select[name=sj_" + j + "]").val();
                        //alert(ele1 + " " + ele2 + " " + ele3);
                        //alert(tempname);
                        tempname.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });


                        //alert(JSON.stringify(tempname));
                        if (enqName == "") {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        } else {
                            var found = false;
                            for (var k in enqName) {
                                if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                    found = true;
                                }
                            }
                            if (!found) {
                                enqName.push({
                                    GuestName: ele1,
                                    Age: parseInt($.trim(ele2)),
                                    Sex: ele3
                                });
                            }
                        }
                    }
                }


            } else if ($("#rdNew").is(":checked")) {
                for (var i = 0; i < highest; i++) {

                    var ele1 = $("#nameNewTable input[name=n_" + i + "]").val();
                    var ele2 = $("#nameNewTable input[name=a_" + i + "]").val();
                    var ele3 = $("#nameNewTable select[name=s_" + i + "]").val();
                    //alert(ele1 + " " + ele2 + " " + ele3);
                    //alert(tempname);
                    tempname.push({
                        GuestName: ele1,
                        Age: parseInt($.trim(ele2)),
                        Sex: ele3
                    });


                    //alert(JSON.stringify(tempname));
                    if (enqName == "") {
                        enqName.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });
                    } else {
                        var found = false;
                        for (var k in enqName) {
                            if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                found = true;
                            }
                        }
                        if (!found) {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        }
                    }
                }
            }
        }
        B.tempNames = tempname;
        //alert(JSON.stringify(B.tempNames));
    }
    this.ClearBookingDetails = function () {
        $("#txtBookingDate").val('');
        $("#txtBookingDate").datepicker("option", "minDate", 0);
        $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);

        $("#txtBookingDateTo").val('');
        $("#txtBookingDateTo").datepicker("option", "minDate", 0);
        $("#txtBookingDateTo").datepicker("option", "maxDate", maxdate);
        $("#spanNOD").html("");
        $("#ddlLocation").val("");
        A.PopulateProperty();
        $("#ddlProperty").val("");
        A.PopulateTariff();
        $("#rdAsc").attr("checked", true);
        $("#ddlAC").val("");
        $("#chkAC").attr("checked", false);
        $("#chkPart").attr("checked", false);
        $("#txtNOR").val("");
        $("#ddlAlternate").val(0);
    }
    this.sortJSON = function (data, key) {
        return data.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    };
    this.addAfterValidate = function () {
        
        var B = window.blockPage.blockForm;
        var arr = WBFDC.Utils.fixArray(B.tempPropertyRoom);

        var frmdate = new Date($("#txtFromDate").datepicker('getDate'));
        var toDate = new Date($("#txtToDate").datepicker('getDate'));
        var totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;
        
        var totBooking = $("#TotalBookingNo").val();
        var C = [];
        for (var i = 0; i < totalDays; i++) {
            var frmdate1 = new Date(frmdate.getTime() + i * 24 * 60 * 60 * 1000);
            var curr_date = frmdate1.getDate();
            var curr_month = frmdate1.getMonth();
            curr_month = curr_month + 1;
            
            var curr_year = frmdate1.getFullYear();
            var cdate = curr_date + '/' + curr_month + '/' + curr_year;
            
            var dateRoom = WBFDC.Utils.fixArray(A.blockForm.dateRoomID);
                var blockRoomDet = [];
                var TotalRoomsSelected = 0;
                var k = 0;
                if (dateRoom.length > 0) {
                    
                    for (var j in dateRoom) {
                        var spldt = (dateRoom[j].BlockDate).split('/');
                        var bookingdt = new Date();
                        bookingdt.setFullYear(spldt[2], (spldt[1] - 1), spldt[0]);
                        var chkdate = new Date();
                        chkdate.setFullYear(curr_year, (curr_month - 1), curr_date);
                        //alert(" bookingdt----> " + bookingdt.getTime() + " ----- frmdate1----> " + chkdate.getTime());
                        if (chkdate.getTime() == bookingdt.getTime()) {
                            k += 1;
                           blockRoomDet.push({
                                BlockDate: cdate,
                                AccomodationDetailID: dateRoom[j].AccomodationDetailID,
                                AccomodationName: dateRoom[j].AccomodationName,
                            });
                            
                        }
                    }
                    if (k > 0) {
                        
                        B.BlockDetail.push({
                            BlockDate: cdate,
                            PropertyID: $("#ddlProperty").val(),
                            PropertyName: $.trim($("#ddlProperty option:selected").text()),
                            blockRoomDetail: blockRoomDet
                        });
                    }
                }
            }
        //alert(JSON.stringify(B.dateRoomID));
        //alert(JSON.stringify(B.blockPropertyRoom));
        //alert(JSON.stringify(B.tempPropertyRoom));
        B.dateRoomID = [];
        B.blockPropertyRoom = [];
        B.tempPropertyRoom = [];
        window.blockPage.captureDetails();
    }
    this.newAddDetails = function () {
        var B = window.blockPage.blockForm;
        var dateRoom = WBFDC.Utils.fixArray(A.blockForm.dateRoomID);
        if (dateRoom == "") {
            alert('Select Prefered Rooms before adding ');
            return false;
        }
        //alert(JSON.stringify(dateRoom));
        mindt = new Date(100000000 * 86400000);
        mindt.setHours(0, 0, 0, 0);
        //mindt = new Date();
        //alert(mindt);
        traverse(dateRoom, compare);
        mindt.setHours(0, 0, 0, 0);
        //alert("mindate " + mindt + " max start date  " + maxStartDays);

        //alert("mindate " + mindt + " max start date  " + maxStartDays);
        //alert(+maxStartDays + "    " + +mindt);
        //alert(+mindt > +maxStartDays);
        if (+mindt > +maxStartDays) {
            alert("Start date must be within " + $("#BookingStartDays").val() + " days;")
            return false;
        }

        //var B = window.enquiryPage.enquiryForm;
        var arr = WBFDC.Utils.fixArray(B.tempLocationRoom);

        arr = $.map(arr, function (o) { return o.TotalRoomsBooked; });
        var highest = Math.max.apply(this, arr);
        //alert(highest);

        var enqName = WBFDC.Utils.fixArray(B.enquiryName);
        var enlength = enqName.length;

        var html = "";
        // html+="<form id='ms'></form> <form id='nameform' >";

        //$("#nameform").validate();
        html += "<table align='center' id='nameTable' class='divTable' cellpadding='5' width='80%'>" + "\n"
                    + "<tr>"
                    + "<td class='th' width='30%'>Guest Name</td>"
                    + "<td class='th' width='20%'>Age</td>"
                    + "<td class='th' width='20%'>Sex</td>"
                    + "<td class='th' width='20%'></td>"
                    + "</tr>";

        html += "<tr><td colspan='4' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
        //var s = $("<select id=\"" + total + "\" name=\"" + total + "\" />");            // 4-
        //for (var val in data) {
        //    $("<option />", { value: val, text: data[val] }).appendTo(s);
        //}
        if (enqName.length <= 0) {
            for (var i = 0; i < highest; i++) {
                html += "<tr id='tr_" + i + "'>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                    + "<td></td>"
                    + "</tr>";
                //$("#n_" + i).rules("add", { required: true, messages: { required: "Please Guest Name" } });
                //$("#a_" + i).rules("add", { required: true, messages: { required: "Please Guest Age" } });
                //$("#s_" + i).rules("add", { required: true, messages: { required: "Please Guest Sex" } });
            }
        } else {

            for (var i = 0; i < enlength; i++) {
                html += "<tr id='tr_" + i + "'>";
                //alert(i);
                //if (i <= highest - 1) {
                //////html += "<td class='tr' align='left'>" + enqName[i].GuestName + "</<td>"
                //////    + "<td class='tr' align='left'>" + enqName[i].Age + "</<td>"
                //////    + "<td class='tr' align='left'>" + (enqName[i].Sex == "M" ? "Male" : "Female") + "</<td>"
                //////    + "<td><input type='checkbox' id='chkN_" + i + "' "+(i<=highest-1?"checked='checked'":"")+" /></td>";

                html += "<td class='tr' align='left'><input type='text' disabled='disabled' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value='" + enqName[i].GuestName + "'/></<td>"
                        + "<td class='tr' align='left'><input type='text' disabled='disabled' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value='" + enqName[i].Age + "'/></<td>"
                        + "<td class='tr' align='left'><select type='text' disabled='disabled' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M' " + (enqName[i].Sex == 'M' ? "selected='selected'" : "") + ">Male</option><option value='F' " + (enqName[i].Sex == 'F' ? "selected='selected'" : "") + ">Female</option></select></<td>"
                        + "<td><input type='checkbox' id='chkN_" + i + "' name='chkN_" + i + "' " + (i <= highest - 1 ? "checked='checked'" : "") + "  /></td>";
                ///////onclick = 'return window.enquiryPage.NamesCheckedClick(this);'
                //}else{
                //    html +=  "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                //    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                //    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                //    + "<td></td>"
                //}
                html += "</tr>";
            }

            if (enlength < highest) {
                var remain = highest - enlength;
                for (var j = 0; j < remain; j++) {
                    html += "<tr id='trj_" + j + "'>";
                    html += "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='nj_" + j + "' name='nj_" + j + "' value=''/></<td>"
                        + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='aj_" + j + "' name='aj_" + j + "' value=''/></<td>"
                        + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='sj_" + j + "' name='sj_" + j + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                        + "<td></td>";
                    html += "</tr>";
                }
            }

        }

        html += "</table>";
        //html += "</form>";

        if (enqName.length > 0) {
            html += "<table align='center' id='nameNewTable' style='display:none;' class='divTable' cellpadding='5' width='80%'>" + "\n"
                        + "<tr>"
                        + "<td class='th' width='30%'>Guest Name</td>"
                        + "<td class='th' width='20%'>Age</td>"
                        + "<td class='th' width='20%'>Sex</td>"
                        + "<td class='th' width='20%'></td>"
                        + "</tr>";

            html += "<tr><td colspan='4' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
            for (var i = 0; i < highest; i++) {
                html += "<tr id='tr_" + i + "'>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                    + "<td></td>"
                    + "</tr>";

            }
            html += "</table>";
        }


        $("#divNameTable").empty().html(html);

        var otherHtml = '';

        otherHtml += " Enter Guest Details of Room(s) of " + $.trim($("#ddlLocation option:selected").text());
        var OptionHtml = '';
        OptionHtml += "<div style='float:right;' >"
            + "<label for='rdOld'  style='padding-right:5px;font-size:11px;padding-left:5px'>Select Old Names</label><input type='radio' checked='checked' id='rdOld' name='rdOld' value='O' onclick='return window.enquiryPage.RadionSelected(this)' />"
            + "<label for='rdNew'  style='padding-right:5px;font-size:11px;padding-left:5px'>Enter New Names</label><input type='radio'  id='rdNew' name='rdOld' value='N' onclick='return window.enquiryPage.RadionSelected(this)'  />"
            + "</div>";

        if (enqName.length > 0) {
            otherHtml += OptionHtml;
        }
        $("#NamesCaption").html(otherHtml);

        $("#loadBookingDet").hide();

        $("#LoadNamesDet").show();
        //return $("#nameform").validate();
    };
    this.RadionSelected = function (ch) {
        var $this = $(ch);
        var optionValue = $this.val();
        if (optionValue == 'O') {
            $("#nameTable").show();
            $("#nameNewTable").hide();
        } else {
            $("#nameTable").hide();
            $("#nameNewTable").show();
        }
    };
    this.NamesCheckedClick = function (ch) {
        var $this = $(ch);
        var thisid = $this.attr("id");
        var pos = thisid.substring(thisid.indexOf("_") + 1);
        if ($this.is(":checked")) {
            var B = window.enquiryPage.enquiryForm;
            var enqName = WBFDC.Utils.fixArray(B.enquiryName);
            $("#tr_" + pos + " input[name=n_" + pos + "]").val(enqName[pos].GuestName).attr("disabled", "disabled");
            $("#tr_" + pos + " input[name=a_" + pos + "]").val(enqName[pos].Age).attr("disabled", "disabled");
            $("#tr_" + pos + " select[name=s_" + pos + "]").val(enqName[pos].Sex).attr("disabled", "disabled");
        } else {
            $("#tr_" + pos + " input[name=n_" + pos + "]").val("").removeAttr("disabled");
            $("#tr_" + pos + " input[name=a_" + pos + "]").val("").removeAttr("disabled");
            $("#tr_" + pos + " select[name=s_" + pos + "]").val("M").removeAttr("disabled");
        }
    };
    this.checkGuestExistence = function () {
        $(".loading-overlay").show();
        var B = window.enquiryPage.enquiryForm;
        var E = "{mobileno:" + B.enquiryMaster.Mobile + ",propertyID:" + $("#ddlProperty").val() + ",locationID:" + $("#ddlLocation").val() + "}";
        //var C = JSON.stringify(E);
        //alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/CheckGuest',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = jQuery.parseJSON(D.d);
                window.enquiryPage.enquiryForm.enquiryMaster.TotalNoOfRooms = t.TotalNoofRooms;

                var frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
                var toDate = new Date($("#txtBookingDateTo").datepicker('getDate'));
                var totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;


                var B = window.enquiryPage.enquiryForm;
                var totBooking = $("#TotalBookingNo").val();
                var nor = ($("#enquiry").field("txtNOR"));


                var totAdded = B.enquiryMaster.TotalNoOfRooms;
                var tot = (parseInt(nor) * parseInt(totalDays)) + parseInt(totAdded);
                //alert("totBooking " + totBooking + "  nor  " + nor + " totAdded " + totAdded + " tot  " + tot + "  frmdate " + frmdate);

                if (tot <= totBooking) {
                    A.showRoomAvailablity(totBooking - parseInt(totAdded));
                    //////////////                    for (var i = 0; i < totalDays; i++) {
                    //////////////                        var frmdate1 = new Date(frmdate.getTime() + i * 24 * 60 * 60 * 1000);
                    //////////////                        var curr_date = frmdate1.getDate();
                    //////////////                        var curr_month = frmdate1.getMonth();
                    //////////////                        curr_month = curr_month + 1;
                    //////////////                        var curr_year = frmdate1.getFullYear();

                    //////////////                        var cdate = curr_date + '/' + curr_month + '/' + curr_year;
                    //////////////                        B.enquiryDetail.push({
                    //////////////                            BookingDate: cdate,
                    //////////////                            LocationID: $("#enquiry").field("ddlLocation"),
                    //////////////                            LocationName: $("#ddlLocation option:selected").text().trim(),
                    //////////////                            PropertyID: $("#enquiry").field("ddlProperty"),
                    //////////////                            PropertyName: $("#ddlProperty option:selected").text().trim(),
                    //////////////                            RangeFrom: $("#enquiry").field("ddlFrom"),
                    //////////////                            RangeTo: $("#enquiry").field("ddlTo"),
                    //////////////                            RangeOrderPreference: ($("#enquiry").field("range")),
                    //////////////                            ACNonAC: ($("#enquiry").field("ddlAC")),
                    //////////////                            ACNonACCompulsory: ($("#chkAC").is(':checked') ? 'Y' : 'N'),
                    //////////////                            NOR: ($("#enquiry").field("txtNOR")),
                    //////////////                            AlternateLocation: ($("#enquiry").field("ddlAlternate")),
                    //////////////                            PartBooking: ($("#chkPart").is(':checked') ? 'Y' : 'N')
                    //////////////                        });
                    //////////////                    }
                    //////////////                    //B.enquiryDetail = enquiryDetail;
                    //////////////                    B.enquiryMaster.TotalNoOfRooms = tot;
                    //////////////                    //alert(JSON.stringify(window.enquiryPage.enquiryForm));
                    //////////////                    window.enquiryPage.captureDetails();
                    //////////////                    //window.enquiryPage.showEnquiryDetail();

                } else {
                    alert("Maximum Allowed Booking is " + totBooking + ". Either " + totBooking + " days OR " + totBooking + " No of Rooms. You have booked " + totAdded + " room(s)/day(s). You can book " + (totBooking - totAdded) + " total no of rooms/days");
                    $(".loading-overlay").hide();
                }

            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });


    };
    this.PopulateProperty = function () {
        //alert('in prop');
        $("#ddlProperty").attr("disabled", "disabled");
        //$("#ddlFrom").attr("disabled", "disabled");
        //$("#ddlTo").attr("disabled", "disabled");
        if ($('#ddlMerchant').val() == "") {
            $('#ddlProperty').empty().append('<option selected="selected" value="">Please select</option>');
            //$('#ddlFrom').empty().append('<option selected="selected" value="">Please select</option>');
            //$('#ddlTo').empty().append('<option selected="selected" value="">Please select</option>');
        }
        else {
            $('#ddlProperty').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                url: pageUrl + '/populateProperty',
                data: '{MerchantID: ' + $('#ddlMerchant').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnPropertyPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };
    this.PopulateTariff = function () {
        //alert($("#txtBookingDate").val().trim());
        if ($.trim($("#txtBookingDate").val()) != "") {
            //alert('in tariff');
            $("#ddlFrom").attr("disabled", "disabled");
            $("#ddlTo").attr("disabled", "disabled");
            //alert($('#ddlProperty').val());
            if ($('#ddlProperty').val() == "") {
                $('#ddlFrom').empty().append('<option selected="selected" value="">Please select</option>');
                $('#ddlTo').empty().append('<option selected="selected" value="">Please select</option>');
            }
            else {
                $('#ddlFrom').empty().append('<option selected="selected" value="">Loading...</option>');
                $('#ddlTo').empty().append('<option selected="selected" value="">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/populateTariffRange',
                    data: "{BookingDate: '" + $('#txtBookingDate').val() + "',LocationID: " + $('#ddlLocation').val() + ",PropertyID: " + $('#ddlProperty').val() + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: A.OnTariffPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }
    };
    this.OnTariffPopulated = function (response) {
        //alert('in success');
        window.enquiryPage.PopulateControl(response.d, $("#ddlFrom"));
        window.enquiryPage.PopulateControl(response.d, $("#ddlTo"));
    };
    this.OnPropertyPopulated = function (response) {
        window.blockPage.PopulateControl(response.d, $("#ddlProperty"));
    };

    //this.ValidateNameForm = function () {
    //    $("#nameform").validate();
    //    alert('after validate called');
    //    $("#nameTable input").each(function () {
    //        var $ele = $(this);
    //        var atr = $ele.attr("id").substring(0, 1);
    //        alert(atr+ "   "+  $ele.attr("id"));
    //        if ( atr== "a") {
    //            $(this).rules("add", { required: true, number: true });
    //        } else {
    //            $(this).rules("add", { required: true });
    //        }

    //    });
    //    alert('end loop after validate called');
    //    return $("#nameform").validate();
    //}

};
















