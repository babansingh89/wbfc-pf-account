﻿function GeneratReport() {
    //var Ledger = "N";
    var Transaction = "N";
    var GreaterZero = "N";
   
    if ($("#txtStartingPeriod").val().trim() == "")
    {
        alert("Enter Starting period! ");
        $("#txtStartingPeriod").focus();
        return false;
    }
    if ($("#txtEndingPeriod").val().trim() == "") {
        alert("Enter Ending period "); $("#txtEndingPeriod").focus();return false;
    }
    if ($("#txtStartingCode").val().trim() == "") {
        alert("Enter Starting code "); $("#txtStartingCode").focus(); return false;
    } else if ($("#txthdnStartingCode").val().trim() == "") {
        alert("Select Starting code from suggestion! "); return false;
    }
    if ($("#txtEndingCode").val().trim() == "") {
        alert("Enter Ending code "); $("#txtEndingCode").focus(); return false;
    } else if ($("#txthdnEndingCode").val().trim() == "") {
        alert("Select Ending code from suggestion!"); return false;
    }

    //if ($("#chckSelectLedger").checked == true)
    //{ Ledger = "Y";  }

    if ($("#chckTransactionduringperiod").checked == true)
    { Transaction = "Y";}
    if ($("#chckGreaterThanZero").checked == true)
    { GreaterZero = "Y"; }
    if ($("#ddlLoanTypeIndicator").val() == "0")
    {
        alert("Select Loan type Indicator! "); $("#ddlLoanTypeIndicator").focus();
        return false;
    }
    //if ($("#chckPrint").checked == true)
    //{
    //    alert("Check print checkbox!");
    //    return false;
    //}     


    var FormName = '';
    var ReportName = '';
    var ReportType = '';
   // var F = '';
    var E = '';
    FormName = "LoanLedger.aspx";
    ReportName = "AccLedgerRep";
    ReportType = "Loan Ledger";   
   
    var W = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  Start_prd:'" + $("#txtStartingPeriod").val()
        + "',  End_prd:'" + $("#txtEndingPeriod").val() + "', strType:'" + $("#ddlLoanTypeIndicator").val() + "', Start_cd:'" + $("#txthdnStartingCode").val() + "', End_cd:'" + $("#txthdnEndingCode").val()
        + "', Balance:'" + GreaterZero + "', Trans:'" + Transaction + "'}";
    $.ajax({
        type: "POST",
        url: "LoanLedger.aspx/SetReportValue",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (response) {
            var show = response.d;
            if (show == "OK") {
                window.open("ReportView.aspx?E=Y"); //querystring E not equal to null (E=Y)..               
            }
        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
            //  $(".loading-overlay").hide();
        },
        failure: function (response) {
            alert(response.d);
            //  $(".loading-overlay").hide();
        }
    });

}

function autocompleteStareCode()
{
    $("#txtStartingCode").autocomplete({
        source: function (request, response) {
            var STcode = $("#txtStartingCode").val();
            var SectorID = $("#ddlUserSector").val();
            var W = "{'STcode':'" + STcode + "',SectorID:" + SectorID +"}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "LoanLedger.aspx/GET_AutoComplete_StartCode",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("|")[0] +'<>'+ item.split("|")[1],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        //   appendTo: "#autocompletePensionerName",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
           // $("#txtGLID").val(arr[1]);
            $("#txthdnStartingCode").val(arr[2]);
            //return false;
        }
    }).click(function () {
        $(this).data("autocomplete").search($(this).val());
    });
  //  alert("check");
}
function autocompleteEndCode() {
    $("#txtEndingCode").autocomplete({
        source: function (request, response) {
            var ENDcode = $("#txtEndingCode").val();
            var SectorID = $("#ddlUserSector").val();
            var W = "{'ENDcode':'" + ENDcode + "',SectorID:" + SectorID +"}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "LoanLedger.aspx/GET_AutoComplete_EndCode",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("|")[0] +"<>"+ item.split("|")[1],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        //   appendTo: "#autocompletePensionerName",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            $("#txthdnEndingCode").val(arr[2]);
          //  $("#txtSLID").val(arr[2]);
            //return false;
        }
    }).click(function () {
        $(this).data("autocomplete").search($(this).val());
    });
}
$(document).ready(function () {
    $("#txtStartingPeriod").focus();
    $('#txtStartingPeriod').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtEndingPeriod').focus();
            return false;
        }
    });
    $('#txtEndingPeriod').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtStartingCode').focus();
            return false;
        }
    });
    $('#txtStartingCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtEndingCode').focus();
            return false;
        }
    });
    $('#txtEndingCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#ddlLoanTypeIndicator').focus();
            return false;
        }
    });
    $('#ddlLoanTypeIndicator').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#chckGreaterThanZero').focus();
            return false;
        }
    });
    $('#chckGreaterThanZero').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#CheckBox1').focus();
            return false;
        }
    });
    $('#CheckBox1').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#btnGenerateReport').focus();
            return false;
        }
    });

    $('#txtStartingPeriod').keypress(function (evt) {
        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
        //    alert("Please Enter a Numeric Value");
            return false;
        }
    });
    $('#txtEndingPeriod').keypress(function (evt) {
        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
         //   alert("Please Enter a Numeric Value");
            return false;
        }
    });

    autocompleteStareCode();
    autocompleteEndCode();
});
