﻿$(document).ready(function () {
    // var allotment = "N";         
    $("#txtSchemeID").keyup(function () {
        if ($("#txtSchemeID").val().trim() == "")
        {
            $("#txthdnSchemeID").val("");
        }
    });
    $("#txtBank").keyup(function () {
        if ($("#txtBank").val().trim() == "") {
            $("#txthdnBank").val("");
        }
    });
    $("#chckNewAllotment").change(function () {        
        if ($("#chckNewAllotment").is(':checked')) {
           // allotment = "Y";
            $("#txtSchemeID").prop("disabled", false);
            $("#txtAllotmentDate").prop("disabled", false);
           // alert("fsfs");
        }
        else {
           // allotment = "N";
            $("#txtSchemeID").val("");
            $("#txtAllotmentDate").val("");
            $("#txthdnSchemeID").val("");
          //  $("#txthdnBank").val("");
            $("#txtSchemeID").prop("disabled", true);
            $("#txtAllotmentDate").prop("disabled", true);
          //  alert("fkkk");
        }
    });
    $('#txtForPeriod').keypress(function (evt) {
        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
          //  alert("Please Enter a Numeric Value");
            return false;
        }
    });
    $('#txtAllotmentDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });
    autocompleteBankID();
    autocompleteSchemeID();
});
function autocompleteBankID() {
   // alert("kk");
    $("#txtBank").autocomplete({
        source: function (request, response) {
            var Bnk = $("#txtBank").val();
            var W = "{'Bnk':'" + Bnk + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "BondInttAndVoucherCreation.aspx/GET_AutoComplete_Bank",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("|")[0],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        //   appendTo: "#autocompletePensionerName",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            // $("#txtGLID").val(arr[1]);
            $("#txthdnBank").val(arr[2]);
            //return false;
        }
    }).click(function () {
        $(this).data("autocomplete").search($(this).val());
    });
    //  alert("check");
}
function autocompleteSchemeID() {
 //   alert("ggggg");
    $("#txtSchemeID").autocomplete({
        source: function (request, response) {
            var scmID = $("#txtSchemeID").val();
            var W = "{'scmID':'" + scmID + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "BondInttAndVoucherCreation.aspx/GET_AutoComplete_SchemeID",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("|")[0],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        //   appendTo: "#autocompletePensionerName",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            // $("#txtGLID").val(arr[1]);
            $("#txthdnSchemeID").val(arr[3]);
            //return false;
        }
    }).click(function () {
        $(this).data("autocomplete").search($(this).val());
    });
    //  alert("check");
}
function btnCreateValidation() {
    if ($("#chckNewAllotment").is(':checked')) {
        if ($("#txtSchemeID").val().trim() == "" || $("#txthdnSchemeID").val() == "") {
            alert("Select a Scheme-ID from suggetion!");
            return false;
        }
        if ($("#txtAllotmentDate").val().trim() == "") {
            alert("Enter a Allotment Date!");
            return false;
        }
        return true;
    }
    if ($("#txtForPeriod").val().trim() == "") {
        alert("Enter a Period!");
        return false;
    }
    if ($("#txtBank").val().trim() == "" || $("#txthdnBank").val() == "") {
        alert("Select a Bank from suggetion!");
        return false;
    }
    return true;
}