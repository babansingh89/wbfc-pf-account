﻿var gUserID = "";
var EditStatus = "N";
var hdnResetUserID = '';
$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    $("#GridLockUser").empty();
    $('#txtUserName').css('text-transform', 'uppercase');
    $('#txtName').css('text-transform', 'uppercase');
    populateUserType();
    populateSector();
    FetchAllData();
   // BindEvents();

    $('#ddlUserResetType').on('change', function () {
        if ($('#ddlUserResetType').val() == 'CP') {
            $('#txtResetPass').prop('disabled', false);
        }
        else {
            $('#txtResetPass').prop('disabled', true);
        }
       
    });

    $("#cmdResetPass").click(function (event) {
        event.preventDefault();
        hdnResetUserID='';
        $("#txtResetUserName").val('');
        $("#txtResetPass").val('');
        document.getElementById('ChkLockUser').checked = false;
        $("#GridLockUser").empty();

        $.ajax({
            type: "POST",
            url: "UserMaster.aspx/Get_CheckedAdmnFlag",
            data: "{}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: true,
            success: function (data) {
                var ChkAdmnFlag = JSON.parse(data.d);
                if (ChkAdmnFlag == 1) { ShowModalPopupUserReset(); $("#txtResetUserName").focus(); return false;}
                else { alert('You Have Not Permission'); return false;}
            }
        });
    });
});

//function BindEvents()
//{
//    $(document).on("change", ".chkSector", function () {
//        //var checkedCheckBoxes = $("#chkSectorContainer").find(".chkSector:checked");
//        var checkedCheckBoxes = $('#chkSectorContainer input[type="checkbox"]:checked');   //vda
//        $.each(checkedCheckBoxes, function (index, value) {
//            console.log($(value).val());
//        });
//        console.log('--------------------------');
//    });
//}

function populateUserType()
{
        $.ajax({
            type: "POST",
            url: "UserMaster.aspx/GetUserType",
            data: '{}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
               // var ddlCustomers = $("[id*=ddlCustomers]");
                $('#ddlUserType').empty().append('<option selected="selected" value="0">Please select</option>');
                $.each(JSON.parse(data.d), function (key, value) {
                    $('#ddlUserType').append($("<option></option>").val(value.TypeCode).html(value.TypeName));
                });
            }
        });
}

function populateSector() {
    var chkSelectContainer = $("#chkSectorContainer");
    chkSelectContainer.empty();
    $.ajax({
        type: "POST",
        url: "UserMaster.aspx/GetSector",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (r) {
            var sectors = JSON.parse(r.d);
            $.each(sectors, function (index, value) {
                //console.log(value);
                chkSelectContainer.append("<input type='checkbox' class='chkSector'  value='" + value.SectorID + "'/><span style='font-size:11px; font-weight:bold; color:#18588a'>" + value.SectorName + "</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                //chkSelectContainer.append("<input type='checkbox' class='chkSector'  value='" + value.SectorID + "'/><label for='cb1'" + value.SectorID + ">"+value.SectorName+"</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            });
        }
    });  
}

function FetchAllData() {
    $.ajax({
        type: "POST",
        url: "UserMaster.aspx/GetAllUser",
        contentType: "application/json;charset=utf-8",
        data: "{}",
        dataType: "json",
        success: function (data) {
            var user=JSON.parse(data.d)
            //if (data.d.length > 0) {
                $('#tabUserMaster tbody').empty();

                $("#tabUserMaster").append("<tr><th class='TableHeader' style='text-align: center; width: 5%; border: 1px solid white;'>Edit</th>  " +
                                                "<th class='TableHeader' style='text-align: center; width: 5%; border: 1px solid white;'>Delete</th>  " +
                                                "<th class='TableHeader' style='text-align: center; width: 10%; border: 1px solid white;'>User Type</th>  " +
                                                "<th class='TableHeader' style='text-align: center; width: 20%; border: 1px solid white;'>User Name</th> " +
                                                "<th class='TableHeader' style='text-align: center; width: 60%; border: 1px solid white;'>Name</th> " +
                                                "</tr>");
                $.each(user, function (index, value) {
                    $("#tabUserMaster").append("<tr><td class='labelCaption' style='text-align: center; width: 5%; border: 1px solid Navy;'><div onClick='EditUserMaster(this);'><span style='cursor:pointer'><img src='images/edit.png'/> <span style='cursor:pointer'> <input type='hidden' id='hdnUserID' value= '" + value.UserID + "'/></span> </td>" +
                                         "<td class='labelCaption' style='text-align: center; width: 5%; border: 1px solid Navy;'><div onClick='DeleteUserMaster_change(this);'><span style='cursor:pointer'><img src='images/delete.png'/> <input type='hidden' id='hdnUserID' value= '" + value.UserID + "'/></span> </td>" +
                                         "<td class='labelCaption' style='text-align: center; width: 10%; border: 1px solid Navy;'> " + value.TypeName + "</td>" +
                                         "<td class='labelCaption' style='text-align: center; width: 20%; border: 1px solid Navy;'> " + value.UserName + "</td>" +
                                         "<td class='labelCaption' style='text-align: center; width: 60%; border: 1px solid Navy;'> " + value.Name + "</td></tr> ");
                });
        },
        error: function (result) {
            alert("Error Records Data");
        }
    });
}


function EditUserMaster(evt) {
    var rowTR = evt.closest('tr');
    //var Name = $(rowTR).find("td:eq(0)").text(); // get value from particular cell of selected row.
    var UserID = $(rowTR).find("td:eq(0)").find('input:hidden[id$="hdnUserID"]').val();
    EditStatus = 'Y';

    $("#trPass").hide();

    //$("#pass1").hide();
    //$("#pass2").hide();
    //$("#pass3").hide();
    $("#cmdSave").val("Update");
    $("#txtUserName").prop("disabled", true);
    
    $.ajax({
        type: "POST",
        url: "UserMaster.aspx/GetUserByUserID",
        data: "{UserID:'" + UserID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (data) {
            var users = JSON.parse(data.d);
            console.log(users);
            $('#ddlUserType').val(users[0].TypeCode);
            $('#txtUserName').val(users[0].UserName);
            $('#txtName').val(users[0].Name);
            gUserID = users[0].UserID;

            var checkedCheckBoxes = $("#chkSectorContainer").find(".chkSector").prop("checked", false);

            $.each(users, function (index, value) {
                $.each(value.ListSector, function (index, value1) {
                    //var tergateChk = $("#chkSectorContainer").find(".chkSector[value='" + value1.SectorID + "']");
                    var tergateChk = $('#chkSectorContainer input[type="checkbox"][value=' + value1.SectorID + ']');
                    if (tergateChk.length > 0)
                    {
                        tergateChk.prop("checked",true);
                    }
                });               
            });
        }
    });
}

$(document).ready(function () {
    $('#cmdSave').click(function (event) {
        
        var TypeUser = $('#ddlUserType').val();
        var UserNamae = $('#txtUserName').val();
        var Password = $('#txtPass').val();
        var RePassword = $('#txtRePass').val();
        var Name = $('#txtName').val();
        var chkSectors = "";
        var cnt = 0;
        
        if (gUserID == "") {

            if ($("#ddlUserType").val() == '0')
            { alert("Please select a User Type !!"); $("#ddlUserType").focus(); return false; }
            if ($("#txtUserName").val().trim() == '')
            { alert("Please enter User Name !!"); $("#txtUserName").focus(); return false; }
            if ($("#txtPass").val() == '')
            { alert("Please enter Password !!"); $("#txtPass").focus(); return false; }
            if ($("#txtRePass").val() == '')
            { alert("Please re-enter Password !!"); $("#txtRePass").focus(); return false; }
            if ($("#txtName").val().trim() == '')
            { alert("Please enter Name !!"); $("#txtName").focus(); return false; }

            if (Password != RePassword) {
                alert("Please re enter correct password !!");
                //$("#txtPass").val('');
                $("#txtRePass").val('');
                $("#txtRePass").focus();
                return false;
            }
        }
        else {
            if ($("#ddlUserType").val() == '0')
            { alert("Please select a User Type !!"); $("#ddlUserType").focus(); return false; }
            if ($("#txtUserName").val().trim() == '')
            { alert("Please enter User Name !!"); $("#txtUserName").focus(); return false; }
            //if ($("#txtPass").val() == '')
            //{ alert("Please enter Password !!"); $("#txtPass").focus(); return false; }
            if ($("#txtName").val().trim() == '')
            { alert("Please enter Name !!"); $("#txtName").focus(); return false; }
        }

        

        var checkedCheckBoxes = $("#chkSectorContainer").find(".chkSector:checked");
        $.each(checkedCheckBoxes, function (index, value) {
            chkSectors = chkSectors + ($(value).val() + ",");
            cnt = cnt + 1;
        });
        chkSectors = chkSectors.substring(0, chkSectors.length - 1)
        if (cnt == 0)
        { alert("Please select at least one sector !!"); return false; }
        //alert(chkSector);

        $.ajax({
            type: "POST",
            url: "UserMaster.aspx/InsertUpdateUserMaster",
            data: "{gUserID:'" + gUserID + "',TypeUser:'" + TypeUser + "', UserNamae:'" + UserNamae + "', Password:'" + Password + "', Name:'" + Name + "', Sectors:'" + chkSectors + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var status = JSON.parse(data.d)
                if (status.Status == "Y") {
                    alert(status.Message);
                }
                else if (status.Status == "N") {
                    alert(status.Message);
                }                           

                $('#ddlUserType').val("0");
                $('#txtUserName').val("");
                $('#txtPass').val("");
                $('#txtRePass').val("");
                $('#txtName').val("");
                $("#chkSectorContainer").find(".chkSector").prop("checked", false);
                EditStatus = 'N';

                if (gUserID != "") {
                    $("#trPass").show();
                    $("#cmdSave").val("Create");
                    $("#txtUserName").prop("disabled", false);
                }

                FetchAllData();
                gUserID = '';       
               
            },
            error: function (result) {
                alert("Error");
            }
        });
    });
});

function DeleteUserMaster(evt) {

    $('<div></div>').appendTo('body')
    .html('<div><h4>Are you sure ?</h4></div>')
    .dialog({
        modal: true,
        title: 'Delete message',
        zIndex: 10000,
        autoOpen: true,
        width: '400px',
        resizable: false,
        buttons: {
            Yes: function () {
                if (EditStatus == 'N') {

                    var rowTR = evt.closest('tr');
                    var UserID = $(rowTR).find("td:eq(0)").find('input:hidden[id$="hdnUserID"]').val();

                    $.ajax({
                        type: "POST",
                        url: "UserMaster.aspx/DeleteUserByUserID",
                        data: "{UserID:'" + UserID + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (data) {
                            var statusDel = JSON.parse(data.d);
                            console.log(statusDel);
                            if (statusDel.Status == "Y") {
                                alert(statusDel.Message);
                            }
                            else if (statusDel.Status == "N") {
                                alert(statusDel.Message);
                            }

                            $('#ddlUserType').val("0");
                            $('#txtUserName').val("");
                            $('#txtPass').val("");
                            $('#txtRePass').val("");
                            $('#txtName').val("");
                            $("#chkSectorContainer").find(".chkSector").prop("checked", false);


                            $("#trPass").show();
                            $("#cmdSave").val("Create");
                            $("#txtUserName").prop("disabled", false);
                            FetchAllData();
                        }
                    });
                }
                else if (EditStatus == 'Y') {
                    alert("Sorry! Unable to Delete, as This record is in MODIFICATION!");
                    return false;
                }
                

                $(this).dialog("close");
            },
            No: function () {
                $(this).dialog("close");
            }
        },
        close: function (event, ui) {
            $(this).remove();
        }
    });
}

$(document).ready(function () {
    autocompleteUserMaster();
    $('#txtRePass').focusout(function () {
        if ($('#txtPass').val() != '' && $('#txtRePass').val() != '') {
            if ($('#txtPass').val() != $('#txtRePass').val()) {
                $('#txtRePass').val('');
                $('#txtRePass').focus();
                alert("Password does not match");
                return false;
            }

        }
    });

    $('#ddlUserType').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtUserName').focus();
            return false;
        }
    });
    $('#txtUserName').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtPass').focus();
            return false;
        }
    });
    $('#txtPass').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtRePass').focus();
            return false;
        }
    });
    $('#txtRePass').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtName').focus();
            return false;
        }
    });
    $('#txtName').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#cmdSave').focus();
            return false;
        }
    });
});

function ShowModalPopupUserReset() {
   
    $("#dialogResetPass").dialog({
        title: "User Reset Password And User Locked",
        width: 550,
        height:250,
        buttons: {
            Save: function () {
                var UserID = '';
                var UserLocked = '';
                var UserPass = '';

                UserLocked = $("#ddlUserResetType").val();
                if (hdnResetUserID == '') {
                    alert('Please Select User Name');
                    $("#txtResetUserName").focus();
                    return false;
                }

                if (UserLocked =='CP') {
                    if ($("#txtResetPass").val() == '') {
                        alert('Please Enter User Reset Password');
                        $("#txtResetPass").focus();
                        return false;
                    }
                } 
                //if (document.getElementById('rbNo').checked) { UserLocked = 'N'; } else { UserLocked = 'Y'; }
                UserID = hdnResetUserID;
                UserPass = $("#txtResetPass").val();
                
                var W = "{UserID:" + UserID + ",UserPass:'" + UserPass + "',UserLocked:'" + UserLocked + "'}";
                $.ajax({
                    type: "POST",
                    url: "UserMaster.aspx/GET_UserReset",
                    contentType: "application/json;charset=utf-8",
                    data: W,
                    dataType: "json",
                    success: function (data) {
                        var msg = data.d;
                        alert(msg);
                        hdnResetUserID='';
                        $("#txtResetUserName").val('');
                        $("#txtResetPass").val('');
                        $("#dialogResetPass").dialog('close');
                        return false;

                    },
                    error: function (result) {
                        alert("Something Missing Please Contact To Admin.....!");
                        $("#dialogResetPass").dialog('close');
                        return false;
                    }
                });
            },
            Close: function () { $("#dialogResetPass").dialog('close'); return false;}
        },
        modal: true
    });
}

function autocompleteUserMaster() {
    $(".AutoUserName").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "UserMaster.aspx/UserCompleteData",
                data: "{'UserName':'" + $("#txtResetUserName").val() + "'}",
                dataType: "json",
                success: function (data) {
                    var userData = JSON.parse(data.d);
                    if ($(userData).length > 0) {
                        var userDataAutoComplete = [];
                        $.each(userData, function (index, item) {
                            userDataAutoComplete.push({
                                label: item.Name,
                                Userval: item.UserID
                            });
                        });
                        response(userDataAutoComplete);
                        return false;
                    }
                    else {
                        alert('No data found.');
                        $('#txtResetUserName').val('');
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            hdnResetUserID=i.item.Userval;
            $("#txtResetUserName").val(i.item.label);
            if ($("#ddlUserResetType").val() == 'CP') {
                $("#txtResetPass").focus();
            }
            
            return false;
        }
    });
}
function ShowLockedUser(chkPassport) {
    if (chkPassport.checked) { LockedUserData(); return false;} else { $("#GridLockUser").empty(); return false;}
}

function LockedUserData() {
    var W = "{}";
    $.ajax({
        type: "POST",
        url: "UserMaster.aspx/Get_LockData",
        data: W,
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (D) {
            var xmlDoc = $.parseXML(D.d);
            var xml = $(xmlDoc);
            $("#GridLockUser").empty();
            $("#GridLockUser").append("<tr style='width:100%;align=center;font-weight: bold; font-family:Verdana;Background:Navy;color:White;'><th>User Type</th><th>User Code</th> <th>User Name</th><th>Branch</th></tr>");
            
            if (xml.length > 0) {
                $(xml).find("Table").each(function () { 
                    $("#GridLockUser").append("<tr><td style='text-align:center; width:16%;'>" +
                        $(this).find("TypeName").text() + "</td> <td style='text-align:center; width:9%;'>" +
                        $(this).find("UserName").text() + "</td> <td style='text-align:center; width:16%;'>" +
                        $(this).find("Name").text() + "</td> <td style='text-align:center; width:20%;'>" +
                        $(this).find("SectorName").text() + "</td> </tr>");
                });
                $(".loading-overlay").hide();
                $(function () {
                    $("[id*=GridLockUser] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridLockUser] tr").each(function (rowindex) {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });

                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                });
                return false;
            }
            else {
                $(".loading-overlay").hide();
                $("#GridLockUser").empty();
                alert("No Records Data");
                return false;
            }
        },
        error: function (result) {
            alert("Error Records Data");  
            $(".loading-overlay").hide();
            return false;
        }

    });  
}

