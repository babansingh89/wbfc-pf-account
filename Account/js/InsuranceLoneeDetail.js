﻿
$(document).ready(function () {
    $(".Default").click(function (event) {
        event.preventDefault();
    });
    
    //SearchMstLoaneeCode();
    SearchInsCode();
    GetIndDetRecLoanee();
    //SearchDetLoaneeCode();
    $("#txtLoaneeSearch").focus();

    $('#grvInsuMaster').on('focus', '.autosuggestMstLoaneeCode', function (event) {
        SearchMstLoaneeCode(this);
    });

    $('#grvInsuMaster').on('focus', '.autoInsCode', function (event) {
        SearchInsCode(this);
    });

    $('#grvInsuDetails').on('focus', '.autosuggestDetLoaneeCode', function (event) {
        SearchDetLoaneeCode(this);
    });
});  

function SearchMstLoaneeCode(t) {
    $(t).autocomplete({
        source: function (request, response) {
            var lons = '';
            lons = $(t).val();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "InsuranceLoneeDetail.aspx/LoaneeAutoCompleteData",
                data: "{'LoaneeCode':'" + lons + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            }
                        }))
                    }
                    else {
                        alert('No data found.');
                        $(t).val('');
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            var txtMstInsID = $(this).parent().parent();
            txtMstInsID.find(".clsMstLoaneesID").val(i.item.val);
            //$("#txtMstLoanees").val('');
            //LoaneeName(i.item.val);
        }
    });
}

function LoaneeName(LoaneeUNQID) {

    var LoaneeName = '';
    var W = "{LoaneeUNQID:'" + LoaneeUNQID + "'}";
    $.ajax({
        type: "POST",
        url: "InsuranceLoneeDetail.aspx/GET_LoaneeName",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var LoaneeName = data.d[0].Loanee_Name;
                $('#txtLoaneeName').val('');
                $('#txtLoaneeName').val(LoaneeName);
                $("#ddlStatus").focus();
            }
        },
        error: function (result) {
            $('#txtLoaneeName').val('');
            alert("Error Records Data...");
        }
    });
}

function SearchInsCode(t3) {
    $(t3).autocomplete({
        source: function (request, response) {
            var InsCode = "";
            //var ControlName = "";
            //var ControlName = $(this).parent().parent();
            ////a.find(".clsDetLoaneesID").val(i.item.val);
            //    //ControlName.find("txtMstInsCode").val(); 
            //InsCode = ControlName.find("txtMstInsCode").val();
            if ($('#txtInsCode').val() != '') {
                InsCode = $(t3).val();
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "InsuranceLoneeDetail.aspx/InsCodeAutoCompleteData",
                data: "{'InsCode':'" + InsCode + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            }
                        }))
                    }
                    else {
                        alert('No data found.');
                        $('#txtInsCode').val('');
                    }
                    
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            var txtMstInsID = $(this).parent().parent();
            txtMstInsID.find(".clsMstInsID").val(i.item.val);
            var ControlName = $(this).parent().parent();
            InsNameAddress(i.item.val, ControlName);
        }
    });
};

function InsNameAddress(InsID, ControlName) {

    var W = "{InsID:" + InsID + "}";
    $.ajax({
        type: "POST",
        url: "InsuranceLoneeDetail.aspx/GET_InsNameAddress",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var InsName = data.d[0].InsName;
                var InsAddress1 = data.d[0].Address1;
                var InsAddress2 = data.d[0].Address2;
                var InsAddress3 = data.d[0].Address3;
                //alert(InsName);
                ControlName.find(".clsMstName").val(InsName); 
                ControlName.find(".clsMstAddrs").val(InsAddress1);
                //return false;
            }
        },
        error: function (result) {
            //ControlName.find(".clsMstName").val('');
            //ControlName.find(".clsMstAddrs").val('');
            alert("Error Records Data...");
            return false;
        }
    });
}

function SearchDetLoaneeCode(t1) {
    $(t1).autocomplete({
        source: function (request, response) {
            var StrLoanee = '';
            StrLoanee = $(t1).val();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "InsuranceLoneeDetail.aspx/LoaneeAutoCompleteData",
                data: "{'LoaneeCode':'" + StrLoanee + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            }
                        }))
                    }
                    else {
                        alert('No data found.');
                        $('#txtLoaneeSearch').val('');
                    }
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            var a = $(this).parent().parent();
            a.find(".clsDetLoaneesID").val(i.item.val);
        }
    });
}

$(document).ready(function () {

    $('#txtLoaneeSearch').keydown(function (evt) {                /// for Backspace
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            //$('#txtLoaneeIDSearch').val('');
            $('#hdnLoaneeID').val('');
            //$('#txtLoaneeID').val('');
        }
    });
    $('#txtLoaneeSearch').keyup(function (evt) {                  /// fro Delete     
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode === 46) {
            $('#hdnLoaneeID').val('');
            //$('#txtLoaneeID').val('');
        }
    });
    $("#txtLoaneeSearch").change(function () {                   /// for all select
        if ($(this).select()) {
            $('#hdnLoaneeID').val('');
            //$('#txtLoaneeID').val('');
            //alert('changed');
        }
    });

    //$('#txtInsCode').keydown(function (evt) {               /// for Backspace
    //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //    if (iKeyCode == 8) {
    //        $('#hdnInsCodeID').val('');
    //    }
    //});
    //$('#txtInsCode').keyup(function (evt) {                  /// for Delete     
    //    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    //    if (iKeyCode === 46) {
    //        $('#hdnInsCodeID').val('');
    //        //$('#txtInsCodeID').val('');
    //    }
    //});

    //$("#txtInsCode").change(function () {
    //    //$(this).select();
    //    if ($(this).select()) {
    //        $('#hdnInsCodeID').val('');
    //        //$('#txtInsCodeID').val('');
    //        //alert('changed');
    //    }
    //});


   
});

$(document).ready(function () {
    $('#txtLoaneeSearch').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#ddlStatus').focus();
            return false;
        }

    });

    $('#ddlStatus').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInsCode').focus();
            return false;
        }

        

    });
    
    $('#txtInsCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#DrpInsPaid').focus();
            return false;
        }

    });

    $('#DrpInsPaid').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#DrpPropType').focus();
            return false;
        }

    });
    
});

$(document).ready(function () {
    $("#btnPrint").click(function (event) {
        ShowReport();
    });
});

function ShowReport() {

    $(".loading-overlay").show();
    var FormName = '';
    var ReportName = '';
    var ReportType = '';
    
    FormName = "PaymentReceived.aspx";
        ReportName = "VOUCHER";
        ReportType = "Voucher Report";
        
        var vchNo = $('#vchNo').val();
        var vchType = $('#vchType').val();
        var vchYearMonth = $('#vchYearMonth').val();
        
    var E = '';
    E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  YearMonth:'" + vchYearMonth + "',  VchNoFrom:'" + vchNo + "', " +
        "  VchNoTo:'" + vchNo + "', VchType:'" + vchType + "'}";
    $.ajax({
        type: "POST",
        url: pageUrl + '/SetReportValue',
        data: E,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var show = response.d;

            if (show == "OK") {
                window.open("ReportView.aspx?E=Y");
            }

        },
        error: function (response) {
            var responseText;
            responseText = JSON.parse(response.responseText);
            alert("Error : " + responseText.Message);
            $(".loading-overlay").hide();
        },
        failure: function (response) {
            alert(response.d);
            $(".loading-overlay").hide();
        }
    });
}

function GetIndDetRecLoanee() {
    var LoaneeCode = '';
    $(".autoSearchInsRecLoanee").autocomplete({
        source: function (request, response) {
            if ($('#txtSearchRecLoanee').val() != '') {
                LoaneeCode = $('#txtSearchRecLoanee').val();
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "InsuranceLoneeDetail.aspx/GET_InsDetonLoaneeCode",
                data: "{'LoaneeCode':'" + LoaneeCode + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            };
                        }));
                    }
                    else {
                        alert('No data found.');
                        $('#txtText').val('');
                        SetInsID(0, InsID); InsID = '';//$("#hdnInsID").val('');
                    }

                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            SetInsID(1, i.item.val); InsID = i.item.val;//$("#hdnInsID").val(i.item.val);
            var a = $(this).parent().parent();
           a.find(".clsid").val(i.item.val);
        }
    });
}
var InsID = '';
function SetInsID(type, InsID) {
    var W = "{type:" + type + ",InsID:'" + InsID + "'}";
    $.ajax({
        type: "POST",
        url: "InsuranceLoneeDetail.aspx/GET_SetInsID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            return false;
        },
        error: function (result) {
            alert("Something Missing...");
            return false;
        }
    });
}

function FunSave() {
    //====================== Master Insurance Coding =====================================//
    var grid = document.getElementById("grvInsuMaster");
    var MstData = [];
    if ($("#txtMstLoaneesID").val() != '') {
    for (var row = 1; row < grid.rows.length - 1; row++) {
        var MstLoaneeID = $("input[id*=txtMstLoaneesID]"); var valMstLoaneeID = $(MstLoaneeID[row - 1]).val() == '' ? 0 : $(MstLoaneeID[row - 1]).val();
        var MstinsID = $("input[id*=txtMstInsCode]"); var valMstinsID = $(MstinsID[row - 1]).val() == '' ? 0 : $(MstinsID[row - 1]).val();
        var MstStatus = $("[id*=ddlMstStatus]"); var valMstStatus = $(MstStatus[row - 1]).val() == '' ? 0 : $(MstStatus[row - 1]).val();
        var MstInsPaids = $("[id*=DrpInsPaids]"); var valMstInsPaids = $(MstInsPaids[row - 1]).val() == '' ? 0 : $(MstInsPaids[row - 1]).val();
        var MstRowID = $("input[id*=txtMstRowID]"); var valMstRowID = $(MstRowID[row - 1]).val() == '' ? 0 : $(MstRowID[row - 1]).val();
        MstData.push({
            'LoaneeID': valMstLoaneeID,
            'insID': valMstinsID,
            'MstStatus': valMstStatus,
            'InsPaids': valMstInsPaids,
            'MstRowID': valMstRowID
            });
        }
    }
    var MstInsurance = JSON.stringify(MstData);
    //alert(JSON.stringify(MstInsurance));
    //====================== Detail Insurance Coding =====================================//
    var grid1 = document.getElementById("grvInsuDetails");
    var DetData = [];
   
    if ($("#txtLoaneesID").val() != '') {
    for (var row1 = 1; row1 < grid1.rows.length - 1; row1++) {
        var DetLoaneeID = $("input[id*=txtLoaneesID]"); var valDetLoaneeID = $(DetLoaneeID[row1 - 1]).val() == '' ? 0 : $(DetLoaneeID[row1 - 1]).val();
        var DetProType = $("[id*=DrpPropType]"); var valDetProType = $(DetProType[row1 - 1]).val() == '' ? 0 : $(DetProType[row1 - 1]).val();
        var DetPolType = $("[id*=drpPolicyType]"); var valDetPolType = $(DetPolType[row1 - 1]).val() == '' ? 0 : $(DetPolType[row1 - 1]).val();
        var PropertyAmt = $("input[id*=txtPropertyAmt]"); var valPropertyAmt = $(PropertyAmt[row1 - 1]).val() == '' ? 0 : $(PropertyAmt[row1 - 1]).val();
        var PlocyNo = $("input[id*=txtPlocyNo]"); var valPlocyNo = $(PlocyNo[row1 - 1]).val() == '' ? '' : $(PlocyNo[row1 - 1]).val();
        var PolicyDate = $("input[id*=txtPolicyDate]"); var valPolicyDate = $(PolicyDate[row1 - 1]).val() == '' ? 0 : $(PolicyDate[row1 - 1]).val();
        var s = '';
        
        if (valPolicyDate != '0') { s = valPolicyDate.split("/"); valPolicyDate = s[2] + "-" + s[1] + "-" + s[0];}
        var PolicyExpDate = $("input[id*=txtPolicyExpDate]"); var valPolicyExpDate = $(PolicyExpDate[row1 - 1]).val() == '' ? 0 : $(PolicyExpDate[row1 - 1]).val();
       
        var s1 = '';
        if (valPolicyExpDate != '0') { s1 = valPolicyExpDate.split("/"); valPolicyExpDate = s1[2] + "-" + s1[1] + "-" + s1[0]; }
        var PremiumAmt = $("input[id*=txtPremiumAmt]"); var valPremiumAmt = $(PremiumAmt[row1 - 1]).val() == '' ? 0 : $(PremiumAmt[row1 - 1]).val();
        var DetRowID = $("input[id*=txtDetRowID]"); var valDetRowID = $(DetRowID[row1 - 1]).val() == '' ? 0 : $(DetRowID[row1 - 1]).val();
       
        DetData.push({
            'LoaneeID': valDetLoaneeID,
            'ProType': valDetProType,
            'PolType': valDetPolType,
            'PropertyAmt': valPropertyAmt,
            'PlocyNo': valPlocyNo,
            'PolicyDate': valPolicyDate,
            'PolicyExpDate': valPolicyExpDate,
            'PremiumAmt': valPremiumAmt,
            'DetRowID': valDetRowID
        });

        }
    }
    var DetInsurance = JSON.stringify(DetData);
    //alert(JSON.stringify(DetInsurance));
    var W = "{MstInsurance:'" + MstInsurance + "',DetInsurance:'" + DetInsurance + "'}";
    //alert(JSON.stringify(W));
    $.ajax({
        type: "POST",
        url: pageUrl + '/GET_SaveInsurance',
        data: W,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var msg = jQuery.parseJSON(data.d);
            alert(msg);
            $(".loading-overlay").hide();
            window.location.href = 'InsuranceLoneeDetail.aspx';
            return false;
        },
        error: function (result) {
            alert("Error Save Data");
            $(".loading-overlay").hide();
            return false;
        }
    });
}
