﻿

var InsertMode = 'Add';
var ModifyrowCount = '0';
var txtHdnSlCodeSearch = '';
var hdnGLID = '';
var hdnSlID = '';
$(document).ready(function () {

    var GridgrdAccGLDtl = document.getElementById("grdAccGLDtl");   //main Grid Initialization!
    GridgrdAccGLDtl.deleteRow(1);
    $('#grdAccGLDtl tr:last').after('<tr><td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' + '</tr>');
    $("[id*=grdAccGLDtl] tr:has(td):last").hide();

    $('#txtOpenCredit').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {

            //=========================Start Form Validation For Blank File Code Here

            if ($('#txtYearMonth').val().trim() == '') {
                alert("Enter Year Month(YYYYMM)");
                $("#txtYearMonth").focus();
                return false;
            }


            if ($('#txtOpenDebit').val().trim() == '') {
                alert("Enter Opening Debit");
                $('#txtOpenDebit').focus();
                return false;
            }


            if ($('#txtOpenCredit').val().trim() == '') {
                alert("Enter Opening Credit");
                $('#txtOpenCredit').focus();
                return false;
            }

            if ($('#txtTotalDebit').val().trim() == '') {
                alert("Enter Total Debit");
                $('#txtTotalDebit').focus();
                return false;
            }

            if ($('#txtTotalCredit').val().trim() == '') {
                alert("Enter Total Credit");
                $('#txtTotalCredit').focus();
                return false;
            }

            if (InsertMode == "Add") {
                var GridIAccGl = document.getElementById("grdAccGLDtl");
                var rowCount = '';
                for (var row = 1; row < GridIAccGl.rows.length; row++) {
                    //  rowCount = row;
                    var GridIAccGl_Cell = GridIAccGl.rows[row].cells[3];
                    var YearMonth = GridIAccGl_Cell.textContent.toString();
                    if (YearMonth == $("#txtYearMonth").val()) {
                        alert("Year Month Already Exists");
                        $('#txtYearMonth').val('');
                        $('#txtOpenDebit').val('');
                        $('#txtOpenCredit').val('');
                        $('#txtTotalDebit').val('');
                        $('#txtTotalCredit').val('');
                        $('#txtCloseDebit').val('');
                        $('#txtCloseCredit').val('');

                        //if (document.getElementById("chkFolio").checked == true) {
                        //    BindGridFolioItem($('#txtRequisitionId').val());
                        //}
                        return false;
                        break;
                    }
                }
            }

            //if (ModifyrowCount != '0')
            //{
            //    rowCount = rowCount + 1;
            //}


            //============ ADD TEXTBOX VALUES TO THE GRIDVIEW

            if (InsertMode == 'Add') {
                if ($('#txtTotalCredit').val() != '') {

                    $('#grdAccGLDtl tr:last').before('<tr>' +
                        '<td>' + "<div onClick='EditAccGlDetail(" + $('#txtYearMonth').val() + " Style='text-align:center;');'><img src='images/Edit.png' id='EditAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' + //EditAccGlDetail_change
                        '<td>' + "<div onClick='CancelAccGlDetail(" + $('#txtYearMonth').val() + " Style='text-align:center;');'><img src='images/close.png' id='CancelAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' + //CancelAccGlDetail_change
                        '<td>' + "<div onClick='DeleteAccGlDetail(" + $('#txtYearMonth').val() + " Style='text-align:center;');'><img src='images/delete.png' id='DeleteAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' + //DeleteAccGlDetail_change
                        '<td Style="text-align:center;">' + $('#txtYearMonth').val() + '</td>' +
                        '<td>' + $('#txtOpenDebit').val() + '</td>' +
                        '<td>' + $('#txtOpenCredit').val() + '</td>' +
                        '<td>' + $('#txtTotalDebit').val() + '</td>' +
                        '<td>' + $('#txtTotalCredit').val() + '</td>' +
                        '<td>' + $('#txtCloseDebit').val() + '</td>' +
                        '<td>' + $('#txtCloseCredit').val() + '</td>' + '</tr>');

                    //======== START Coding For Function to Add Item in SESSION

                    AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());

                    //======== END   Coding For Function to Add Item in SESSION

                    ////////if (ModifyrowCount != '0') {

                    ////////    $('#GridViewAddIssueDetails tr:last').before('<tr>' +
                    ////////  '<td>' + "<div onClick='EditNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/Modify1.png' id='EditNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + "<div onClick='CancelNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/close.png' id='CancelNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + "<div onClick='DeleteNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/delete.png' id='DeleteNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + rowCount + '</td>' +
                    ////////  '<td>' + $('#txtFolioNo').val() + '</td>' +
                    ////////  '<td>' + $('#txtItemDesc').val() + '</td>' +
                    ////////  '<td>' + $('#txtUOM').val() + '</td>' +
                    ////////  '<td>' + $('#txtReqQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtUsedQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtPendingQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtIssuedQty').val() + '</td>' + '</tr>');

                    ////////    //======== START Coding For Function to Add Item in SESSION
                    ////////    AddIssueItemDetailsOnSession($('#txtFolioID').val(), $('#txtIssuedQty').val());
                    ////////    //======== END   Coding For Function to Add Item in SESSION

                    /////////  }
                }
                else
                    alert('Invalid Entry!');
            }
            else {

                //====== In Edit Mode Value Add in Exixts Records code here ====================//
                var GridAccGL = document.getElementById("grdAccGLDtl");

                for (var row1 = 1; row1 < GridAccGL.rows.length; row1++) {
                    var GridIAccGl_Cells = GridAccGL.rows[row1].cells[3];
                    var YearMonths = GridIAccGl_Cells.textContent.toString();

                    if (YearMonths == $("#txtYearMonth").val()) {
                        //   rowCount = row;

                        GridAccGL.rows[row1].cells[3].textContent = $('#txtYearMonth').val();
                        GridAccGL.rows[row1].cells[4].textContent = $('#txtOpenDebit').val();
                        GridAccGL.rows[row1].cells[5].textContent = $('#txtOpenCredit').val();
                        GridAccGL.rows[row1].cells[6].textContent = $('#txtTotalDebit').val();
                        GridAccGL.rows[row1].cells[7].textContent = $('#txtTotalCredit').val();
                        GridAccGL.rows[row1].cells[8].textContent = $('#txtCloseDebit').val();
                        GridAccGL.rows[row1].cells[9].textContent = $('#txtCloseCredit').val();

                        AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());
                        // AddIssueItemDetailsOnSession($('#txtFolioID').val(), $('#txtIssuedQty').val());
                    }
                }
            }

            //================ START Selected Grid Row Color Change coding Here
            $(function () {
                $("[id*=GridViewAddIssueDetails] td").bind("click", function () {
                    if (InsertMode == "Add") {
                        var row = $(this).parent();

                        $("[id*=GridViewAddIssueDetails] tr").each(function (rowindex) {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });

                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    }// if close
                });

            });
            return false;
        } //======Keypree Enter=13 close

        //================ END Selected Grid Row Color Change coding Here

    });

        $('#txtOpenCredit').keydown(function (evt) {   //sm
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 9) {
            //=========================Start Form Validation For Blank File Code Here

            if ($('#txtYearMonth').val().trim() == '') {
                alert("Enter Year Month(YYYYMM)");
                $("#txtYearMonth").focus();
                return false;
            }


            if ($('#txtOpenDebit').val().trim() == '') {
                alert("Enter Opening Debit");
                $('#txtOpenDebit').focus();
                return false;
            }


            if ($('#txtOpenCredit').val().trim() == '') {
                alert("Enter Opening Credit");
                $('#txtOpenCredit').focus();
                return false;
            }

            if ($('#txtTotalDebit').val().trim() == '') {
                alert("Enter Total Debit");
                $('#txtTotalDebit').focus();
                return false;
            }

            if ($('#txtTotalCredit').val().trim() == '') {
                alert("Enter Total Credit");
                $('#txtTotalCredit').focus();
                return false;
            }

            if (InsertMode == "Add") {
                var GridIAccGl = document.getElementById("grdAccGLDtl");
                var rowCount = '';
                for (var row = 1; row < GridIAccGl.rows.length; row++) {
                    //  rowCount = row;
                    var GridIAccGl_Cell = GridIAccGl.rows[row].cells[3];
                    var YearMonth = GridIAccGl_Cell.textContent.toString();
                    if (YearMonth == $("#txtYearMonth").val()) {
                        alert("Year Month Already Exists");
                        $('#txtYearMonth').val('');
                        $('#txtOpenDebit').val('');
                        $('#txtOpenCredit').val('');
                        $('#txtTotalDebit').val('');
                        $('#txtTotalCredit').val('');
                        $('#txtCloseDebit').val('');
                        $('#txtCloseCredit').val('');

                        //if (document.getElementById("chkFolio").checked == true) {
                        //    BindGridFolioItem($('#txtRequisitionId').val());
                        //}
                        return false;
                        break;
                    }
                }
            }

            //if (ModifyrowCount != '0')
            //{
            //    rowCount = rowCount + 1;
            //}


            //============ ADD TEXTBOX VALUES TO THE GRIDVIEW

            if (InsertMode == 'Add') {
                if ($('#txtTotalCredit').val() != '') {

                    $('#grdAccGLDtl tr:last').before('<tr>' +
                        '<td>' + "<div onClick='EditAccGlDetail(" + $('#txtYearMonth').val() + " Style='text-align:center;');'><img src='images/Edit.png' id='EditAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' + //EditAccGlDetail_change
                        '<td>' + "<div onClick='CancelAccGlDetail(" + $('#txtYearMonth').val() + " Style='text-align:center;');'><img src='images/close.png' id='CancelAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' + //CancelAccGlDetail_change
                        '<td>' + "<div onClick='DeleteAccGlDetail(" + $('#txtYearMonth').val() + " Style='text-align:center;');'><img src='images/delete.png' id='DeleteAccGlDetail_" + $('#txtYearMonth').val() + "'/>" + '</td>' + //DeleteAccGlDetail_change
                  '<td>' + $('#txtYearMonth').val() + '</td>' +
                  '<td>' + $('#txtOpenDebit').val() + '</td>' +
                  '<td>' + $('#txtOpenCredit').val() + '</td>' +
                  '<td>' + $('#txtTotalDebit').val() + '</td>' +
                  '<td>' + $('#txtTotalCredit').val() + '</td>' +
                  '<td>' + $('#txtCloseDebit').val() + '</td>' +
                  '<td>' + $('#txtCloseCredit').val() + '</td>' + '</tr>');

                    //======== START Coding For Function to Add Item in SESSION

                    AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());

                    //======== END   Coding For Function to Add Item in SESSION

                    ////////if (ModifyrowCount != '0') {

                    ////////    $('#GridViewAddIssueDetails tr:last').before('<tr>' +
                    ////////  '<td>' + "<div onClick='EditNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/Modify1.png' id='EditNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + "<div onClick='CancelNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/close.png' id='CancelNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + "<div onClick='DeleteNewIssueItemDetail(" + $('#txtFolioID').val() + ");'><img src='images/delete.png' id='DeleteNewIssueItemDetail_" + $('#txtFolioID').val() + "'/>" + '</td>' +
                    ////////  '<td>' + rowCount + '</td>' +
                    ////////  '<td>' + $('#txtFolioNo').val() + '</td>' +
                    ////////  '<td>' + $('#txtItemDesc').val() + '</td>' +
                    ////////  '<td>' + $('#txtUOM').val() + '</td>' +
                    ////////  '<td>' + $('#txtReqQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtUsedQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtPendingQty').val() + '</td>' +
                    ////////  '<td>' + $('#txtIssuedQty').val() + '</td>' + '</tr>');

                    ////////    //======== START Coding For Function to Add Item in SESSION
                    ////////    AddIssueItemDetailsOnSession($('#txtFolioID').val(), $('#txtIssuedQty').val());
                    ////////    //======== END   Coding For Function to Add Item in SESSION

                    /////////  }
                }
                else
                    alert('Invalid Entry!');
            }
            else {

                //====== In Edit Mode Value Add in Exixts Records code here ====================//
                var GridAccGL = document.getElementById("grdAccGLDtl");

                for (var row = 1; row < GridAccGL.rows.length; row++) {
                    var GridIAccGl_Cells = GridAccGL.rows[row].cells[3];
                    var YearMonths = GridIAccGl_Cells.textContent.toString();

                    if (YearMonths == $("#txtYearMonth").val()) {
                        //   rowCount = row;

                        GridAccGL.rows[row].cells[3].textContent = $('#txtYearMonth').val();
                        GridAccGL.rows[row].cells[4].textContent = $('#txtOpenDebit').val();
                        GridAccGL.rows[row].cells[5].textContent = $('#txtOpenCredit').val();
                        GridAccGL.rows[row].cells[6].textContent = $('#txtTotalDebit').val();
                        GridAccGL.rows[row].cells[7].textContent = $('#txtTotalCredit').val();
                        GridAccGL.rows[row].cells[8].textContent = $('#txtCloseDebit').val();
                        GridAccGL.rows[row].cells[9].textContent = $('#txtCloseCredit').val();

                        AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());
                        // AddIssueItemDetailsOnSession($('#txtFolioID').val(), $('#txtIssuedQty').val());
                    }
                }
            }

            //================ START Selected Grid Row Color Change coding Here
            $(function () {
                $("[id*=GridViewAddIssueDetails] td").bind("click", function () {
                    if (InsertMode == "Add") {
                        var row = $(this).parent();

                        $("[id*=GridViewAddIssueDetails] tr").each(function (rowindex) {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });

                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    }// if close
                });

            });
            return false;
        } //======Keypree Enter=13 close

        //================ END Selected Grid Row Color Change coding Here

    });

});

function AddAccGlDetailsOnSession(YearMonth, OpenDebit, OpenCredit, TotalDebit, TotalCredit) {
    // alert("resr");
    var W = "{YearMonth:'" + YearMonth + "', OpenDebit:'" + OpenDebit + "', OpenCredit:'" + OpenCredit + "', TotalDebit:'" + TotalDebit + "', TotalCredit:'" + TotalCredit + "'}";
    //  alert(W);
    $.ajax({
        type: "POST",
        url: "Acc_Sl.aspx/GET_AccGlDetailsOnSession",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (JSONVal) {

            //=========Start function calling To Add Total Count

            //=========End   function calling To Add Total Count

            InsertMode = "Add";           // CLEAR ALL TEXTBOXES
            $('#txtYearMonth').val('');
            $('#txtOpenDebit').val('');
            $('#txtOpenCredit').val('');
            $('#txtTotalDebit').val('');
            $('#txtTotalCredit').val('');
            $('#txtCloseDebit').val('');
            $('#txtCloseCredit').val('');

            var d = new Date();
            var month = (d.getMonth() + 1).toString();
            var year = (d.getFullYear()).toString();
            var yearmon = '';
            if (month.length > 1) {
                yearmon = year + month;
            }
            else {
                yearmon = year + '0' + month;
            }
            // alert(yearmon);
            document.getElementById("txtYearMonth").disabled = false;

            $('#txtYearMonth').val($('#hdnMaxYearMonth').val());
            
            $('#txtOpenDebit').focus();
            return false;
        },
        error: function (JSONVal) {
        }
    });
}

$(document).ready(function () {
    $("#cmdSave").click(function (event) {
        var GlID = '';
        var OldGlID = '';
        var GlName = '';
        var OldSlID = '';
        var SLID = '';
        var GridRowCount = '';



        if ($('#txtGLCode').val().trim() == '') {
            alert("Select GL Code");
            $("#txtGLCode").focus();
            return false;
        }

        if ($('#txtGLName').val().trim() == '') {
            alert("Select GL Name");
            $("#txtGLName").focus();
            return false;
        }
        if ($('#txtSlCode').val().trim() == '') {
            alert("Enter SL Code");
            $("#txtSlCode").focus();
            return false;
        }
        if ($('#txtSlNmae').val().trim() == '') {
            alert("Select SL Name");
            $("#txtSlNmae").focus();
            return false;
        }

        var GridAccGLDtl = document.getElementById("grdAccGLDtl");
        if (hdnSlID == '') { //$('#hdnSlID').val()
            GridRowCount = 2;
        }
        else {
            GridRowCount = 1;
        }
        if (GridAccGLDtl.rows.length <= parseInt(GridRowCount)) {

            alert("Please Insert Year Month(YYYYMM) / Item in Grid");
            $("#txtYearMonth").focus();
            return false;
        }


        //=========================START Fetch TextField Values and CONDITIONS     

        if (InsertMode == 'Add') {
            SLID = hdnSlID;//$('#hdnSlID').val().trim();

            if (hdnGLID == '') { //$('#hdnGLID').val().trim()
                GlID = '';
            }
            else {
                GlID = hdnGLID;//$('#hdnGLID').val().trim();
            }



            if ($('#txtGLCode').val().trim() == '') {
                OldGlID = '';
            }
            else {
                OldGlID = $('#txtGLCode').val().trim();
            }

            if ($('#txtSlCode').val().trim() == '') {
                OldSlID = '';
            }
            else {
                OldSlID = $('#txtSlCode').val().trim();
            }

            if ($('#txtSlNmae').val().trim() == '') {
                GlName = '';
            }
            else {
                GlName = $('#txtSlNmae').val().trim();
            }



            //=========================END   Fetch TExtField Values and CONDITIONS

            $(".loading-overlay").show();

            var W = "{SLID:'" + SLID + "', GlID:'" + GlID + "', OldGlID:'" + OldGlID + "', OldSlID:'" + OldSlID + "', GlName:'" + GlName + "'}";
            //alert(W);
            $.ajax({
                type: "POST",
                url: "Acc_Sl.aspx/Save_AccGlRecord",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    if (SLID == '') {
                        alert("Records Save Successfully");

                        ClearAllField();
                        $(".loading-overlay").hide();
                        return false;
                        //  var ValIssueNo = jQuery.parseJSON(data.d);

                        //var ValReqNo = strReqNo[0]["ReqNumber"];
                        //   $('#txtGenerateIssueNo').val(ValIssueNo);

                        //      $(".loading-overlay").hide();
                        //     PopupModelIssueNoShow();

                        //     BindGetUnitNameDis();

                    }
                    else {
                        ClearAllField();
                        $(".loading-overlay").hide();
                        alert("Records Updated Successfully");
                        ////CurrentDate();
                        //$("#txtRequisitionDate").focus();

                    }

                },
                error: function (response) {
                    $(".loading-overlay").hide();
                    alert("Records Not Saved!");
                    //$("#txtPODate").focus();
                    return false;
                }
            });
            return false;
        }
        else {
            alert("Record In Modification Mode!");
            return false;
        }

    });
});

$(document).ready(function () {
    $("#cmdView").click(function (event) {
        ClearAllField();
        if (InsertMode == "Add") {

            var old_sl_id = '';

            if ($('#txtSlCodeSearch').val() == '') {

                alert("Please select a SL Code  before search.");
                $("#txtSlCodeSearch").css({ "background-color": "#ff9999" });
                $('#txtSlCodeSearch').focus();
                return false;
            }
            else {
                $("#txtSlCodeSearch").css({ "background-color": "" });

                if (txtHdnSlCodeSearch == '') { //$('#txtHdnSlCodeSearch').val()
                    alert("Please select a correct SL Code.");
                    $("#txtSlCodeSearch").css({ "background-color": "#ff9999" });
                    $('#txtSlCodeSearch').focus();
                    $('#txtSlCodeSearch').val('');
                    //$('#txtHdnSlCodeSearch').val('');
                    txtHdnSlCodeSearch = '';
                    //return false;
                }
                old_sl_id = txtHdnSlCodeSearch;
                //old_sl_id = $('#txtHdnSlCodeSearch').val();

            }
            
            SearchSLMaster(old_sl_id);
            SearchSLDetail(old_sl_id);
            $('#txtSlNmae').focus();
        }
        else {

            alert("Sorry! as This record is in MODIFICATION!");
            return false;
        }
    });
});

function SearchSLMaster(old_sl_id) {
    if (InsertMode == "Add") {
        var W = "{old_sl_id:'" + old_sl_id + "'}";
        $.ajax({
            type: "POST",
            url: "Acc_Sl.aspx/SLMasterSearch",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {

                    $("#txtGLCode").val(data.d[0].OLD_GL_ID);
                    $("#txtGLName").val(data.d[0].GL_NAME);
                    //$("#hdnGLID").val(data.d[0].GL_ID);
                    hdnGLID = data.d[0].GL_ID;
                    //$("#hdnSlID").val(data.d[0].SL_ID);
                    hdnSlID = data.d[0].SL_ID;
                    $("#txtSlCode").val(data.d[0].OLD_SL_ID);
                    $("#txtSlNmae").val(data.d[0].SL_NAME);
                    document.getElementById("txtGLCode").disabled = true;
                    document.getElementById("txtSlCode").disabled = true;
                }
                else {
                    $('#txtSlCodeSearch').focus();
                    alert("No record found for this SL Code. Select correct SL Code.");
                    return false;
                }

            },
            error: function (result) {
                alert("Error Records Data");
            }
        });
    }
}      

function SearchSLDetail(old_sl_id) {
    if (InsertMode == "Add") {

        var W = "{old_sl_id:'" + old_sl_id + "'}";
        $.ajax({
            type: "POST",
            url: "Acc_Sl.aspx/SLDetailSearch",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    //$("#grdAccGLDtl").find("tr:not(:nth-child(1)):not(:nth-child(2))").remove();
                    
                    for (var i = 0; i < data.d.length; i++) {

                        var OpenDebit = 0;
                        var OpenCredit = 0;
                        var CloseDebit = 0;
                        var CloseCredit = 0;
                        //alert(data.d[i].OPBAL);
                        //alert(data.d[i].CLBAL);

                        if (data.d[i].OPBAL >= 0) {

                            OpenDebit = data.d[i].OPBAL;
                            OpenCredit = 0;
                        }
                        else {
                            OpenDebit = 0;
                            OpenCredit = data.d[i].OPBAL;
                        }

                        if (data.d[i].CLBAL >= 0) {
                            CloseDebit = data.d[i].CLBAL;
                            CloseCredit = 0;
                        }
                        else {
                            CloseDebit = 0;
                            CloseCredit = data.d[i].CLBAL;;
                        }

                        $("#grdAccGLDtl").append("<tr><td>" +
                            "<div onClick='EditAccGlDetail(" + data.d[i].YEAR_MONTH + ");' Style='text-align:center;'><img src='images/Edit.png' id='EditAccGlDetail_" + data.d[i].YEAR_MONTH + "'/>" + "</td> <td>" + //EditAccGlDetail_change
                            "<div onClick='CancelAccGlDetail(" + data.d[i].YEAR_MONTH + ");' Style='text-align:center;'><img src='images/close.png' id='CancelAccGlDetail_" + data.d[i].YEAR_MONTH + "'/>" + "</td> <td>" + //CancelAccGlDetail_change
                            "<div onClick='DeleteAccGlDetail(" + data.d[i].YEAR_MONTH + ");' Style='text-align:center;');'><img src='images/delete.png' id='DeleteAccGlDetail_" + data.d[i].YEAR_MONTH + "'/>" + "</td> <td Style='text-align:center;'>" + //DeleteAccGlDetail_change
                        data.d[i].YEAR_MONTH + "</td> <td align='right'>" +
                        parseFloat(OpenDebit).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(OpenCredit).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(data.d[i].DEBIT).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(data.d[i].CREDIT).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(CloseDebit).toFixed(2) + "</td> <td td align='right'>" +
                        parseFloat(CloseCredit).toFixed(2) + "</td>" + "</tr>");
                    }
                }
            },
            error: function (result) {
                alert("Error Records Data");
            }
        });
    }
}



function ClearAllField() {

    InsertMode = 'Add';

    $('#txtGLCode').val('');
    $('#txtGLName').val('');
    //$('#hdnGLID').val('');
    hdnGLID = '';
    //$('#hdnSlID').val('');
    hdnSlID = '';
    $('#txtSlCode').val('');
    $('#txtSlNmae').val('');
    document.getElementById("txtGLCode").disabled = false;
    document.getElementById("txtSlCode").disabled = false;

    var GridAccGLDtl = document.getElementById("grdAccGLDtl");

    for (var row = GridAccGLDtl.rows.length - 1; row <= GridAccGLDtl.rows.length; row--) {

        if (row == 0) {
            $('#grdAccGLDtl tr:last').after('<tr><td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' +
                           '<td>' + '' + '</td>' + '</tr>');
            break;
        }
        else {
            GridAccGLDtl.deleteRow(row);
        }

    }
    $("[id*=grdAccGLDtl] tr:has(td):last").hide();
    return false;
}

function EditAccGlDetail(YearMonth) {
    //if ($('#txtHdnSlCodeSearch').val() != '') { alert('Permission Not Allowed For Edit ? Only For New Entry Allow'); return false }
    if (txtHdnSlCodeSearch != '') { alert('Permission Not Allowed For Edit ? Only For New Entry Allow'); return false; }
    $('#txtYearMonth').val(YearMonth);
    var GridAccGLDtl = document.getElementById("grdAccGLDtl");
    var tbody = GridAccGLDtl.getElementsByTagName("tbody")[0];

    tbody.onclick = function (e) {
        if (InsertMode == "Add") {
            e = e || window.event;
            var data = [];
            var target = e.srcElement || e.target;
            while (target && target.nodeName !== "TR") {
                target = target.parentNode;
            }
        }
        if (InsertMode == "Add") {
            if (target) {
                var cells = target.getElementsByTagName("td");
                for (var i = 0; i < cells.length; i++) {
                    data.push(cells[i].textContent);

                    if (i == 3) {
                        $('#txtYearMonth').val(cells[i].textContent);
                    }
                    if (i == 4) {
                        $('#txtOpenDebit').val(cells[i].textContent);
                        ValSrNo = cells[i].textContent;
                    }
                    if (i == 5) {
                        $('#txtOpenCredit').val(cells[i].textContent);
                    }
                    if (i == 6) {
                        $('#txtTotalDebit').val(cells[i].textContent);
                    }
                    if (i == 7) {
                        $('#txtTotalCredit').val(cells[i].textContent);
                    }
                    if (i == 8) {
                        $('#txtCloseDebit').val(cells[i].textContent);
                    }
                    if (i == 9) {
                        $('#txtCloseCredit').val(cells[i].textContent);
                    }
                }
            }

            $('#txtOpenDebit').focus();
            document.getElementById("txtYearMonth").disabled = true;
            DeleteAccGlDetailSession(YearMonth);
            //$('#txtYearMonth').focus();
            InsertMode = "Modify";
            return false;
        } //If close

        else {
            alert("This Record is Already in Modification Mode");
            InsertMode = "Modify";
            $("#txtYearMonth").focus();
            return false;
        }
    };
}

function DeleteAccGlDetailSession(ValYearMonth) {
    $(".loading-overlay").show();
    var W = "{ValYearMonth:" + ValYearMonth + "}";
    $.ajax({
        type: "POST",
        url: "Acc_Sl.aspx/GET_DeleteSessionAccGl",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            $(".loading-overlay").hide();
        },
        error: function (result) {
            alert("Error Records Data");
            $(".loading-overlay").hide();

        }
    });
}

function DeleteAccGlDetail(YearMon) {
    //if ($('#txtHdnSlCodeSearch').val() != '') { alert('Permission Not Allowed For Edit ? Only For New Entry Allow'); return false; }
    if (txtHdnSlCodeSearch != '') { alert('Permission Not Allowed For Edit ? Only For New Entry Allow'); return false; }
    if (confirm("Are You sure you want to delete?")) {
        var StrFolioNo = '';
        var GridRowCount = '';
        //$("#GridViewAddIndentDetail").find("tr:not(:nth-child(1)):not(:nth-child(2))").remove();
        var GridAccGlDetail = document.getElementById("grdAccGLDtl");
        var tbody = GridAccGlDetail.getElementsByTagName("tbody")[0];

        tbody.onclick = function (e) {
            if (InsertMode == "Add") {
                e = e || window.event;

                var data = [];
                var target = e.srcElement || e.target;
                while (target && target.nodeName !== "TR") {
                    target = target.parentNode;
                }
            }
            if (InsertMode == "Add") {
                if (target) {
                    var cells = target.getElementsByTagName("td");
                    for (var i = 0; i < cells.length; i++) {
                        if (i == 3) {
                            StrFolioNo = cells[i].textContent;
                            break;
                        }
                    }
                }

                for (var row = 1; row < GridAccGlDetail.rows.length; row++) {
                    var GridFolioNo_Cell = GridAccGlDetail.rows[row].cells[3];
                    var valueFolioNo = GridFolioNo_Cell.textContent.toString();
                    if (StrFolioNo == valueFolioNo) {
                        GridAccGlDetail.deleteRow(row);
                        //alert("Test");
                        DeleteAccGlDetailSession(YearMon);

                        alert("Record Deleted");
                        ////if (GridAccGlDetail.rows.length - 1 <= parseInt(GridRowCount))
                        ////{
                        ////    document.getElementById("txtRequisitionNo").disabled = false;

                        ////}
                        $("#txtYearMonth").focus();
                        return false;
                        break;
                    }
                }
            }
            else {
                alert("Sorry! Unable to Delete, as This record is in MODIFICATION!");
                $("#txtFolioNo").focus();
                return false;
            }
        };
    }
    else {
        return false;
    }

}

function CancelAccGlDetail(YearMonth) {

    if (InsertMode == 'Modify') {
        var ValYearMonth = parseInt(YearMonth);
        //======== Start Coding For Function calling for Add Item in Session=========//
        ////AddIssueItemDetailsOnSession(ValFolioID, $('#txtIssuedQty').val());
        AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val()); AddAccGlDetailsOnSession($('#txtYearMonth').val(), $('#txtOpenDebit').val(), $('#txtOpenCredit').val(), $('#txtTotalDebit').val(), $('#txtTotalCredit').val());
        return false;
    }
    else { return false; }
}


$(document).ready(function () {
    $('#txtGLCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtSlCode').focus();
            return false;
        }

    });

    $('#txtSlCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtSlNmae').focus();
            return false;
        }

    });

    $('#txtSlNmae').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtYearMonth').focus();
            return false;
        }

    });
});


$(document).ready(function () {
    $("#cmdDelete").click(function (event) {

        if ($('#txtSlCodeSearch').val().trim() == '') {
            alert('Please Select SL Code');
            $('#txtSlCodeSearch').focus();
            return false;
        }

        if ($('#txtGLCode').val().trim() == '' ) {
            alert('Please Click View After Delete!');
            $('#cmdView').focus();
            return false;
        }

        if ($('#txtSlCode').val().trim() == '') {
            alert('Please Click View After Delete!');
            $('#cmdView').focus();
            return false;
        }


        $(".loading-overlay").show();

        var GlID = '';
        var SLID = '';
        GlID = hdnGLID;//$('#hdnGLID').val();
        SLID = hdnSlID;//$('#hdnSlID').val();
        if (GlID == '') { GlID = '0'; }
        var W = "{GL_ID:'" + GlID + "',SL_ID:'" + SLID + "'}";
        $.ajax({
            type: "POST",
            url: "Acc_Sl.aspx/GET_SlNoOnVoucher",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    var objPartNo = data.d[0].SL_ID;
                    $(".loading-overlay").hide();
                    alert('Can Not Delete Record!This SL Code Already Used On Voucher');
                    return false;
                }

                else {
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    if (confirm("Do you want to Delete GL Record?")) {
                        confirm_value.value = "Yes";

                    } else {
                        confirm_value.value = "No";
                        InsertMode = 'Add';
                    }

                    if (confirm_value.value == "Yes") {
                        $(".loading-overlay").show();
                        DeleteSLRecord(SLID);
                        $(".loading-overlay").hide();
                    }
                    else {
                        $('#txtSlCodeSearch').focus();
                        $(".loading-overlay").hide();
                    }
                }



            },
            error: function (result) {
                $(".loading-overlay").hide();
                alert("Error Records Data");
                return false;
            }
        });

    });
});

function DeleteSLRecord(SlID) {
    var W = "{GL_ID:'" + hdnGLID + "',SL_ID:'" + SlID + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Sl.aspx/GET_DelSLRecord",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            InsertMode = 'Add'; 
            $(".loading-overlay").hide();
            alert("Record Delete");
            ClearAllField();
            $("#txtSlCodeSearch").val('');
            //$("#txtHdnSlCodeSearch").val('');

            return false;
        },
        error: function (result) {
            $(".loading-overlay").hide();
            alert("Error Records Data");
            return false;
        }
    });

}
