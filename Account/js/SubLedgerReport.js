﻿
$(document).ready(function () {
    $(".DefaultBtn").click(function (event) {
        event.preventDefault();
    });
});
  

$(document).ready(function () {
    $('#txtFromDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });

    $('#txtToDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });

    $('#txtFromDate').keydown(function (e) {
        alert("Please Select Date Using Date Picker. Manual Entry Not Allowed!");
        e.preventDefault();
        return false;
    });

    $('#txtToDate').keydown(function (e) {
        alert("Please Select Date Using Date Picker! Manual Entry Not Allowed!");
        e.preventDefault();
        return false;
    });
    $('#txtFromDate').focus();
});



$(function () {

    $("[id*=chkAllSubLedger]").bind("click", function () {
        if ($(this).is(":checked")) {
            $("[id*=chkSubLedger] input").prop("checked", true);
        } else {
            $("[id*=chkSubLedger] input").prop("checked", false);
        }
    });

    $("[id*=chkSubLedger] input").bind("click", function () {
        if ($("[id*=chkSubLedger] input:checked").length == $("[id*=chkSubLedger] input").length) {
            $("[id*=chkAllSubLedger]").prop("checked", true);
        } else {
            $("[id*=chkAllSubLedger]").prop("checked", false);
        }
    });

    $("[id*=chkAllSector]").bind("click", function () {
        if ($(this).is(":checked")) {
            $("[id*=ddlSector] input").prop("checked", true); chkAllSubLedger
        } else {
            $("[id*=ddlSector] input").prop("checked", false);
        }
    });

    $("[id*=ddlSector] input").bind("click", function () {
        if ($("[id*=ddlSector] input:checked").length == $("[id*=ddlSector] input").length) {
            $("[id*=chkAllSector]").prop("checked", true);
        } else {
            $("[id*=chkAllSector]").prop("checked", false);
        }
    });
});

function beforeShow()
{
    $("#frmEcom").validate();
    $("#txtFromDate").rules("add", { required: true, messages: { required: "Please Enter Form Date" } });
    $("#txtToDate").rules("add", { required: true, messages: { required: "Please Enter To Date" } });
    
}

function PrintSinglePage() {

    var GridDetails = document.getElementById("tbl");
    if (GridDetails.rows.length <= 3) {
        alert("Please Generate Report!");
        $('#txtFromDate').focus();
        return false;
    }

    var W = "{}";
    $.ajax({
        type: "POST",
        url: "SubLedgerReport.aspx/GET_PrintSingleDataTable",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (TableData) {
            PrintData(TableData);
        },
        error: function (result) {
            $(".loading-overlay").hide();
            alert("Error Records Data");

        }
    });
}

function PrintData(TableData) {
    var i, j, FirstFlag = 0;
    var FromDate = $('#txtFromDate').val();
    var ToDate = $('#txtToDate').val();
    var t = TableData.d;
    var RowCount = TableData.d.length;
    var Credit = 0.00;
    var Debit = 0.00;
    var popupWin = window.open('', '', 'left=0,top=0,width=1000,height=600,status=0');
    popupWin.document.open();
    popupWin.document.write('<html><head><title>Print</title>');
    popupWin.document.write('<style> @media print{ table { page-break-inside:auto } tr { page-break-inside:avoid; page-break-after:auto } thead { display:table-header-group } tfoot { display:table-footer-group } }</style>');
    popupWin.document.write('</head><body onload="window.print()">');


    for (i = 0; i < RowCount; i++) {
        Credit = parseFloat(t[i]["CREDIT"]) + Credit;
        Debit = parseFloat(t[i]["DEBIT"]) + Debit;
    }

    if (FirstFlag == 0) {
        popupWin.document.write('<div style="page-break-after:always;"><center>');
        popupWin.document.write('<label>' + $("#txtOrgName").val() + '</label>');
        popupWin.document.write('<br/>');
        popupWin.document.write('<label>' + $("#txtOrgAddress").val() + '</label>');
        popupWin.document.write('<br/>');
        popupWin.document.write('<br/>');
        popupWin.document.write('<label>SUB LEDGER FROM  ' + FromDate + ' TO ' + ToDate + '</label>');
        popupWin.document.write('<br/>');
        popupWin.document.write('<label>' + t[0]["GL_NAME"] + ' (' + t[0]["OLD_SL_ID"] + ')' + '</label>');
        popupWin.document.write('<br/>');
        popupWin.document.write('<label>===============================================================================================================</label>');
        popupWin.document.write('<table width="98%"><tr><td>DATE</td><td>DOC. TYPE</td><td>PARTICULARS</td><td>CODE</td><td align="right">DEBIT</td><td align="right">CREDIT</td></tr>');
        popupWin.document.write('<tr><td colspan="6"><label>===============================================================================================================</label></td></tr>');
        FirstFlag = 1;
    }

    var Count = 1;

    while (Count <= RowCount) {

        popupWin.document.write('<tr><td>' + t[Count - 1]["VCH_DATE"] + '</td><td>' + t[Count - 1]["DOC_TYPE"] + '</td><td>' + t[Count - 1]["PARTICULARS"] + '</td><td>' + t[Count - 1]["BK_GLSL_CODE"] + '</td><td align="right">' + parseFloat(t[Count - 1]["DEBIT"]).toFixed(2) + '</td><td align="right">' + parseFloat(t[Count - 1]["CREDIT"]).toFixed(2) + '</td></tr>');

        //if (Count % 10 == 0 && Count != RowCount) {
        //    popupWin.document.write('<tr><td colspan="6"><label>===============================================================================================================</label></td></tr>');
        //    popupWin.document.write('</table>');
        //    popupWin.document.write('<label align="right">Continued.......</label>');
        //    popupWin.document.write('</center></div>');

        //    popupWin.document.write('<div style="page-break-after:always;"><center>');
        //    popupWin.document.write('<label>WEST BENGAL FINANCIAL CORPORATION</label>');
        //    popupWin.document.write('<br/>');
        //    popupWin.document.write('<label>12A, N.S. ROAD, CALCUTTA 700001.</label>');
        //    popupWin.document.write('<br/>');
        //    popupWin.document.write('<br/>');
        //    popupWin.document.write('<label>GENERAL LEDGER FROM  ' + FromDate + ' TO ' + ToDate + '</label>');
        //    popupWin.document.write('<br/>');
        //    popupWin.document.write('<label>' + t[0]["GL_NAME"] + ' (' + t[0]["OLD_SL_ID"] + ')' + '</label>');
        //    popupWin.document.write('<br/>');
        //    popupWin.document.write('<label>===============================================================================================================</label>');
        //    popupWin.document.write('<table width="98%"><tr><td>DATE</td><td>DOC. TYPE</td><td>PARTICULARS</td><td>CODE</td><td align="right">DEBIT</td><td align="right">CREDIT</td></tr>');
        //    popupWin.document.write('<tr><td colspan="6"><label>===============================================================================================================</label></td></tr>');
        //}

        if (Count == RowCount) {
            popupWin.document.write('<tr><td colspan="6"><label>===============================================================================================================</label></td></tr>');
            popupWin.document.write('<tr><td>TOTAL:</td><td></td><td></td><td></td><td align="center">' + Debit.toFixed(2) + '</td><td align="right">' + Credit.toFixed(2) + '</td></tr>');
            popupWin.document.write('<tr><td colspan="6"><label>===============================================================================================================</label></td></tr>');
            popupWin.document.write('</table>');
            popupWin.document.write('</center></div>');
            break;
        }
        Count = Count + 1;
    }
    popupWin.document.write('</body></html>');
    popupWin.document.close();
}



function PrintMultiPlePages() {

    var GridDetails = document.getElementById("tbl");
    if (GridDetails.rows.length <= 3) {
        alert("Please Generate Report!");
        $('#txtFromDate').focus();
        return false;
    }

    var W = "{}";
    $.ajax({
        type: "POST",
        url: "SubLedgerReport.aspx/GET_PrintMultiTableData",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (TableData) {
            var i = 0;
            var TableLength = TableData.d.length;
            var DivLength = TableLength / 2;
            var popupWin = window.open('', '', 'left=0,top=0,width=1000,height=600,status=0');
            popupWin.document.open();
            popupWin.document.write('<html><head><title>Print</title>');
            popupWin.document.write('<style> @media print{ table { page-break-inside:auto } tr { page-break-inside:avoid; page-break-after:auto } thead { display:table-header-group } tfoot { display:table-footer-group } }</style>');
            popupWin.document.write('</head><body onload="window.print()">');

            for (i = 0; i < DivLength; i++) {
                popupWin.document.write('<div style="page-break-after:always;"><center>');
                popupWin.document.write('<label>' + $("#txtOrgName").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>' + $("#txtOrgAddress").val() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>' + $('#lblDuration').text() + '</label>');
                popupWin.document.write('<br/>');
                popupWin.document.write(TableData.d[i + DivLength]);
                popupWin.document.write('<br/>');
                popupWin.document.write('<label>=====================================================================================================================</label>');
                popupWin.document.write('<table width="98%"><tr><td>DATE</td><td>DOC. TYPE</td><td>PARTICULARS</td><td>CODE</td><td align="right">DEBIT</td><td align="right">CREDIT</td></tr>');
                popupWin.document.write('<tr><td colspan="6"><label>=====================================================================================================================</label></td></tr>');
                popupWin.document.write(TableData.d[i]);
                popupWin.document.write('</table>');
                popupWin.document.write('</center></div>');
            }
            popupWin.document.write('</body></html>');
            popupWin.document.close();
        },
        error: function (result) {
            $(".loading-overlay").hide();
            alert("Error Records Data");

        }  
    });
}
