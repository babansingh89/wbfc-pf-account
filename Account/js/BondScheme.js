﻿
$(document).ready(function () {
    if (document.getElementById("txtCode").disabled == false) {
        $('#txtCode').focus();
        GetMaxSchemeID();
    }
    else {
        $('#txtName').focus();
    }

});

function GetMaxSchemeID() {
    var W = "{}";
    $.ajax({
        type: "POST",
        url: "LMN_Acc_Scheme.aspx/GET_MaxSchemeID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            var MaxSchemeID = jQuery.parseJSON(data.d);
            document.getElementById("txtCode").placeholder = 'Max Bond Scheme ID :- ' + MaxSchemeID;
        },
    });
}

function DuplicateCheckSchemeID() {
    var SchemeID = '';
    if ($("#txtCode").val().trim() == '') {
        return false;
    }
    $(".loading-overlay").show();
    if ($("#hdnSchemeID").val().trim() == '') {
        SchemeID = '0';
    }
    else { SchemeID = $("#hdnSchemeID").val(); }
    var W = "{OLD_SCHEME_ID:'" + $("#txtCode").val() + "',SCHEME_ID:'" + SchemeID + "'}";
    $.ajax({
        type: "POST",
        url: "LMN_Acc_Scheme.aspx/GET_DupChkSchemeID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            var MaxSchemeID = jQuery.parseJSON(data.d);
            if (MaxSchemeID != '') {
                alert('Bond Scheme Code Already Exists! Max Bond Scheme ID : - ' + MaxSchemeID);
                $(".loading-overlay").hide();
                $("#txtCode").val('');
                $("#txtCode").focus();
                return false;
            }
            else { $(".loading-overlay").hide(); $('#txtName').focus(); return false; }

        },
    });
}

//========= Start Key Press Coding Here ==============


$(document).ready(function () {

    $("input").focus(function () {
        $(this).css("background-color", "#cccccc");
    });
    $("input").blur(function () {
        $(this).css("background-color", "#ffffff");
    });

    $("select").focus(function () {
        $(this).css("background-color", "#cccccc");
    });

    $("select").blur(function () {
        $(this).css("background-color", "#ffffff");
    });

    $('#txtCode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            DuplicateCheckSchemeID();
            //$('#txtName').focus();
            return false;
        }
    });

    $("#txtCode").keydown(function (e) {
        if (e.keyCode == 9) {
            if ($('#txtCode').val().trim() == '') {
                $('#txtGLCode').focus();
                return false;
            }
            else {
                DuplicateCheckSchemeID();
                return true;
            }
        }
    });


    $('#txtName').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            if (document.getElementById("txtGLID").disabled == false) {
                $('#txtGLID').focus();
            }
            else {
                if (document.getElementById("DdlType").disabled == false) {
                    $('#DdlType').focus();
                }
                else {
                    $('#txtFrom').focus();
                }
            }
            return false;

        }
    });

    $('#txtGLID').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            if (document.getElementById("DdlType").disabled == false) {
                if ($('#txtGLID').val().trim() == '') {
                    BindGridGLCode();
                }
                else {
                    $('#DdlType').focus();
                }
            }
            else {
                $('#txtTotalAmmount').focus();
            }
            return false;

        }
    });

    $('#DdlType').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtFrom').focus();
            return false;

        }
    });



    $('#txtFrom').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtTo').focus();
            return false;

        }
    });

    $('#txtTo').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtTotalAmmount').focus();
            return false;

        }
    });

    $('#txtTotalAmmount').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtSingleUnitrate').focus();
            return false;

        }
    });

    $('#txtSingleUnitrate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtInterestRate').focus();
            return false;

        }
    });

    $('#txtInterestRate').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#ddlRepaymentmode').focus();
            return false;

        }
    });

    $('#ddlRepaymentmode').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtISIN_NO').focus();
            return false;

        }
    });

    $('#txtISIN_NO').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtDPID_NO').focus();
            return false;

        }
    });

    $('#txtDPID_NO').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#DdlSlr').focus();
            return false;

        }
    });

    $('#DdlSlr').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtPeriodDays').focus();
            return false;

        }
    });

    $('#txtPeriodDays').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#cmdSave').focus();
            return false;

        }
    });

    $('#txtGLID').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 9) {
            if ($('#txtGLID').val().trim() == '') {
                BindGridGLCode();
            }
            else {
                $('#DdlType').focus();
            }
            return false;

        }
    });

    $("#txtGLID").keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
            alert("Please Enter Key Press Or Tab Key Press For Display GL ID");
            return false;
        }
    });

    $('#txtFrom').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
            alert("Please Select Issue Open From ! Type Not Allowed");
            return false;
        }
    });

    $('#txtTo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
            alert("Please Select Issue Open To ! Type Not Allowed");
            return false;
        }
    });



}); // Main Closed
//========= End Key Press Coding here ================


function BindGridGLCode() {
    $(".loading-overlay").show();

    var W = "{}";

    $.ajax({
        type: "POST",
        url: "LMN_Acc_Scheme.aspx/Search_GlCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            $("#grdGL").empty();
            if (data.d.length > 0) {
                $("#grdGL").append("<tr><th>Gl ID</th><th>Gl Name</th></tr>");

                for (var i = 0; i < data.d.length; i++) {
                    $("#grdGL").append("<tr><td>" +
                    data.d[i].OLD_GL_ID + "</td> <td>" +
                    data.d[i].GL_NAME + "</td></tr>");
                }
                GlPopup();
                $(".loading-overlay").hide();
                $("#txtSearchGL").focus();
                //===================== START Search is not Blank Record Display in GridView
                if ($('#txtSearchGL').val() != '') {
                    var rows;
                    var coldata;
                    $('#grdGL').find('tr:gt(0)').hide();
                    var data = $('#txtSearchGL').val();
                    var len = data.length;
                    if (len > 0) {
                        $('#grdGL').find('tbody tr').each(function () {
                            coldata = $(this).children().eq(0);
                            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                            if (temp === 0) {
                                $(this).show();
                            }
                        });
                    }
                    else {
                        $('#grdGL').find('tr:gt(0)').show();

                    }
                }

                $(document).ready(function () {
                    $("[id*=grdGL] tbody tr").click("click", function () {
                        var strVal = $(this).children("td:eq(0)").text();
                        var valUnitName = $(this).children("td:eq(1)").text();
                        $("#txtSearchGL").val(strVal);
                        ////$("#txtGLName").val(valUnitName);
                        $("#txtSearchGL").focus();
                    });
                });
                //===================== END Selected Gridview in Text Box

            }
            else {
                alert("No Records Data");
                $(".loading-overlay").hide();
                $('#txtGLCode').focus();
            }
        },
        error: function (result) {
            alert("Error Records Data");
            $(".loading-overlay").hide();

        }
    });
}

function GlPopup() {
    $("#dialogGL").dialog({
        title: "GL Search",
        width: 550,
        height: 600,

        buttons:
        {
            Ok: function () {

                var GridGroup = document.getElementById("grdGL");
                var strVal = '';
                if ($('#txtSearchGL').val().trim() == '') {
                    alert("Please Select Gl");
                    $('#txtSearchGL').val('');
                    $("#txtSearchGL").focus();
                    return false;
                }
                if ($('#txtSearchGL').val() != '') {
                    for (var row = 1; row < GridGroup.rows.length; row++) {
                        var GridGroup_Cell = GridGroup.rows[row].cells[0];
                        var valueGrdCell = GridGroup_Cell.textContent.toString();
                        if (valueGrdCell == $('#txtSearchGL').val()) {
                            strVal = valueGrdCell;
                            break;
                        }
                    }

                    if (strVal != $('#txtSearchGL').val()) {
                        alert("Invalid GL ID (GL ID is not in List)");
                        $("#txtSearchGL").focus();
                        return false;
                    }
                    else {
                        $("#txtGLID").val($("#txtSearchGL").val());
                        //BindGlName($('#txtGLCode').val());
                        $("#txtGLID").focus();
                        $("#dialogGL").dialog('close');
                        return false;
                    }
                }
            }
        },
        modal: true
    });
}


$(document).ready(function () {
    $("#txtSearchGL").keypress(function (e) {
        if (e.keyCode == 13) {
            var GridGroup = document.getElementById("grdGL");
            var strVal = '';
            if ($('#txtSearchGL').val().trim() == '') {
                alert("Select a Gl");
                $('#txtSearchGL').val('');
                $("#txtSearchGL").focus();
                return false;
            }
            if ($('#txtSearchGL').val() != '') {
                for (var row = 1; row < GridGroup.rows.length; row++) {
                    var GridGroup_Cell = GridGroup.rows[row].cells[0];
                    var valueGrdCell = GridGroup_Cell.textContent.toString();
                    if (valueGrdCell == $('#txtSearchGL').val()) {
                        strVal = valueGrdCell;
                        break;
                    }
                }

                if (strVal != $('#txtSearchGL').val()) {
                    alert("Invalid GL ID (GL ID is not in List)");
                    $("#txtSearchGL").focus();
                    return false;
                }
                else {
                    $("#txtGLID").val($("#txtSearchGL").val());
                    //BindGlName($('#txtGLCode').val());

                    $("#txtGLID").focus();
                    $("#dialogGL").dialog('close');
                    return false;
                }
            }
        }
    });
});

// 
$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtSearchGL').keyup(function () {

        if (document.getElementById("RBGlId").checked == true) {


            $('#grdGL').find('tr:gt(0)').hide();
            var data = $('#txtSearchGL').val();
            var len = data.length;
            if (len > 0) {
                $('#grdGL').find('tbody tr').each(function () {
                    coldata = $(this).children().eq(0);
                    var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                    if (temp === 0) {
                        $(this).show();
                    }
                });
            } else {
                $('#grdGL').find('tr:gt(0)').show();
            }

        }

        //=============== GL Name Code ======================

        if (document.getElementById("RBGlName").checked == true) {


            $('#grdGL').find('tr:gt(1)').hide();
            var data = $('#txtSearchGL').val();
            var len = data.length;
            if (len > 0) {
                $('#grdGL').find('tbody tr').each(function () {
                    coldata = $(this).children().eq(1);
                    var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                    if (temp === 0) {
                        $(this).show();
                    }
                });
            } else {
                $('#grdGL').find('tr:gt(0)').show();
            }

        }

        //============== GL Name Code

    });
});


function FunGLID() {
    $("#txtSearchGL").focus();
    return false;
}

function FunGLName() {
    $("#txtSearchGL").focus();
    return false;
}