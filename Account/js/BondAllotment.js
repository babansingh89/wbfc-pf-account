﻿$(document).ready(function () {
    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });
    AutoComplete_SchemeID();
});

function AutoComplete_SchemeID() {  
    $("#txtSchemeID").autocomplete({
       source: function (request, response) {        

           var W = "{Scheme_OLD_ID:'" + $('#txtSchemeID').val().trim() + "'}";
       //    alert(W);
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "BondAllotment.aspx/GET_SchemeIDDisplay",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                 //   alert(data.d);
                    response($.map(data.d, function (item) {
                        return {                           
                            label: item.split("|")[1] + "  <>  " + item.split("|")[2],
                            val: item,
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
    //   appendTo: "#autocompleteSchemeID",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            $("#txtSchemeID").val(arr[1]);
            $("#txtSchemeName").val(arr[2]);
            $("#txtAmount").val(arr[3]);
            $("#txtScheme_ID").val(arr[0]);
            //return false;
            $('#txtAmountTotalAllot').val("");
            $('#txtAmountTotalApply').val("");
            //BondAllotGrid(arr[0]);
            BondAllotGrid($("#txtScheme_ID").val(), $("#ddlStatus").val());
        }
    }).click(function () {
        $(this).data("autocomplete").search($(this).val());
    });
}

$(document).ready(function () {
    $("#ddlStatus").change(function (event) {
        if ($("#ddlStatus").val() != 'NA')
        { $("#cmdSave").attr('disabled', true); } else { $("#cmdSave").attr('disabled', false); }

        if ($("#txtScheme_ID").val() != "")
        {  BondAllotGrid($("#txtScheme_ID").val(),$("#ddlStatus").val());  }
    });
});

function BondAllotGrid(SchemeID,Status) {
    
    var W = "{SchemeID:'" + SchemeID + "',Status:'" + Status + "'}";
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "BondAllotment.aspx/GET_BondAllotGridpopu",
        data: W,
        dataType: "json",
        success: function (data) {                        
            $("#gvDetails").empty();
            $("#gvDetails").append("<tr><th>DEPOSIT ID</th><TH>APPLICATION NO </th><th>ENCASEMENT DATE</th><th>APPLY AMOUNT</th><th>APPLICANT NAME </th><th>ALLOTMENT AMOUNT </th></tr>");
            if ($("#ddlStatus").val() == 'NA') {
                for (var i = 0; i < data.d.length; i++) {
                    $("#gvDetails").append("<tr><td>" + data.d[i].DEPOSIT_ID + "</td><td>" +
                        data.d[i].APPL_NO + "</td><td>" + data.d[i].APPLICATION_DATE + "</td><td>" +
                        data.d[i].APPL_AMOUNT + "</td><td>" + data.d[i].APPLICANT_NAME + "</td><td><input type='text' id='txtAllotAmnt' style='width:150px;height:25px; border:none; font-size:medium;' onkeyup='CountTotalApplyAndIssueAmount()'  value=" +
                        data.d[i].ISSU_AMOUNT + "></td><td><div onclick='SingleAllotment(" + data.d[i].DEPOSIT_ID + ")'>Allot This</div></td></tr>");
                }
            } else {
                for (var i = 0; i < data.d.length; i++) {
                    $("#gvDetails").append("<tr><td>" + data.d[i].DEPOSIT_ID + "</td><td>" +
                        data.d[i].APPL_NO + "</td><td>" + data.d[i].APPLICATION_DATE + "</td><td>" +
                        data.d[i].APPL_AMOUNT + "</td><td>" + data.d[i].APPLICANT_NAME + "</td><td><input type='text' id='txtAllotAmnt' style='width:150px;height:25px; border:none; font-size:medium;' onkeyup='CountTotalApplyAndIssueAmount()'  value=" +
                        data.d[i].ISSU_AMOUNT + "></td></tr>");
                }
            }
            CountTotalApplyAndIssueAmount();
        },
        error: function (result) {
            alert("Error");
        }
    });

}

function SingleAllotment(Deposit_ID)
{
    var GridView = document.getElementById("gvDetails");
    for (var row = 1; row < GridView.rows.length; row++) {

        var GridApplyAmt_Cell0 = GridView.rows[row].cells[0];
        var DepositID = GridApplyAmt_Cell0.textContent.toString();

        //alert(DepositID +"/"+ Deposit_ID);
        var GridApplyAmt_Cell1 = GridView.rows[row].cells[3];
        var ApplyAmount = GridApplyAmt_Cell1.textContent.toString();

        var ValAllotAmt = $("input[id*=txtAllotAmnt]");
        var IssueAmount = ValAllotAmt[row - 1].value.trim();
     
        if (DepositID == Deposit_ID) {
            //ValTds = valueTds;
            //ValTdsID = GridViewTds.rows[row].cells[0].textContent.toString();
            break;
        }
    }

    if (parseInt(IssueAmount) == 0 || IssueAmount == "")
    { alert("please enter allotment amount!!"); return false; }

    var TotalAmount= parseInt($('#txtAmount').val());
    if (parseInt(TotalAmount) >= parseInt(IssueAmount)) {
         if (parseInt(ApplyAmount) >= parseInt(IssueAmount)) {         
            AddBondAllotment($('#txtScheme_ID').val(), DepositID, IssueAmount);
         }
         else {
             alert("Allotment amount should not greater than Apply amount!!!"); return false;
         }
       
    }
    else {
        alert(" Allotment Amount Should not greater than Total Amount!!!"); return false;
    }
    
    BondAllotGrid($("#txtScheme_ID").val(), $("#ddlStatus").val());   
}

$(document).ready(function () {
    $("#cmdSave").click(function (event) {
        var GridGroup = document.getElementById("gvDetails");
        //alert(GridGroup.rows.length);
        if (GridGroup.rows.length < 2)
        { alert("There are no Record for allotment!!"); return false; }

        if ($('#txtAmountTotalAllot').val() != "" || $('#txtAmountTotalAllot').val() != 0) {
            //var GridGroup = document.getElementById("gvDetails");
            for (var row = 1; row < GridGroup.rows.length; row++) {

                var GridApplyAmt_Cell0 = GridGroup.rows[row].cells[0];
                var DepositID = GridApplyAmt_Cell0.textContent.toString();

                var GridApplyAmt_Cell1 = GridGroup.rows[row].cells[3];
                var ApplyAmount = GridApplyAmt_Cell1.textContent.toString();
                //    alert(DepositID);
                var ValAllotAmt = $("input[id*=txtAllotAmnt]");
                var IssueAmount = ValAllotAmt[row - 1].value.trim();
                if (IssueAmount.trim() == "")
                { IssueAmount = '0'; }
                var TotalAllotAmount = parseInt($('#txtAmountTotalAllot').val());
                var TotalAmount = parseInt($('#txtAmount').val());

                if (parseInt(TotalAmount) >= parseInt(TotalAllotAmount)) {

                    if (parseInt(ApplyAmount) >= parseInt(IssueAmount)) {
                        //if(parseInt($('#txtAmount').val()))
                        //alert('IssueAmount =' + IssueAmount + " ApplyAmount " + ApplyAmount + "  " + DepositID);
                        AddBondAllotment($('#txtScheme_ID').val(), DepositID, IssueAmount);
                    } else { alert("Allotment amount should not greater than Apply amount!!!");  return false; }
                    //    alert(UpdtValue);alert('IssueAmount =' + IssueAmount + " ApplyAmount " + ApplyAmount + "  " + DepositID);
                } else { alert("Total Allotment Amount Should not greater than Total Amount!!!"); return false; }
            }
            alert("Allotment Successfull");
        } else { alert("There are no Record for allotment!!"); return false; }
    });
});

function AddBondAllotment(SchemeID, DepositID, IssueAmount) {
    
        var W = "{SchemeID:'" + SchemeID + "', DepositID:'" + DepositID + "', IssueAmount:'" + IssueAmount + "'}";
        //        alert(W);
        $.ajax({
            type: "POST",
            url: "BondAllotment.aspx/GET_BondAllotmentProceed",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (JSONVal) {              
                //alert("Allotment Successfull");
            },
            error: function (JSONVal) {
            }
        }); 
}

function CountTotalApplyAndIssueAmount() {
    var GridItemDetail = document.getElementById("gvDetails");
    var TotalAmount = '';
    var Sum = 0;
    //alert(GridItemDetail.rows.length);
    for (var row = 1; row < GridItemDetail.rows.length; row++) {
        var GridApplyAmount = GridItemDetail.rows[row].cells[3];
        var GValAllo = GridApplyAmount.textContent.toString();
        if (GValAllo == '') {
             GValAllo=0;
        }
        //        alert(GValAllo);
        if (TotalAmount == '') {
            TotalAmount = GValAllo;
        }
        else {
            TotalAmount = parseFloat(TotalAmount) + parseFloat(GValAllo);
        }     
    }      
        $("#txtAmountTotalApply").val(TotalAmount);          
    
    for (var row = 1; row < GridItemDetail.rows.length; row++) {
        var txtAmountAllot = $("input[id*=txtAllotAmnt]");
        var Amount = txtAmountAllot[row-1].value.trim();
        if (Amount == '')
        { Amount = 0;}
        Sum = parseFloat(Sum) + parseFloat(Amount);
        //        alert(Sum);
        $("#txtAmountTotalAllot").val(parseFloat(Sum));
    }   
}