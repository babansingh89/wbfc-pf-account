﻿

var maxdate;
var maxStartDays;


var maxdt = new Date(-100000000 * 86400000);
var mindt = new Date(100000000 * 86400000);

function compare(key, value) {
    if(key == "BookingDate"){
        var arr = value.split("/");
        var dt = new Date();
       dt.setFullYear(arr[2], (arr[1]-1), arr[0]);
       //alert("arrdt" + dt.getTime() + " min date" + mindt.getTime());
        //alert("   " + dt.getTime() < mindt.getTime());
       dt.setHours(0, 0, 0, 0);
       if (dt.getTime() < mindt.getTime())
            mindt = dt;
    }
}

function traverse(obj, fun) {
    for (prop in obj) {
        fun.apply(this, [prop, obj[prop]]);
        if (typeof (obj[prop]) == "object") {
            traverse(obj[prop], fun);
        }
    }
}


$(document).ready(function () {
    $(".loading-overlay").show();
    $(".loading-overlay").hide();

    
    var BookingStartDays = $("#BookingStartDays").val();
    var maxopen = $("#MaxOpenDays").val();
    maxStartDays= new Date();
    maxdate = new Date();
    var cdt = $("#curdt").val();
    var curdate = new Date(cdt);
    if (maxopen != null) {
        maxdate.setTime(curdate.getTime() + maxopen * 24 * 60 * 60 * 1000);
    };
    if (BookingStartDays != null) {
        maxStartDays.setTime(curdate.getTime() + BookingStartDays * 24 * 60 * 60 * 1000);
    }
    //alert("cdt  ===  "+cdt+"  ===  curdate " + curdate + "   ===   add days " + maxopen + " =====   added days    " + maxdate);
    $.datepicker.setDefaults($.datepicker.regional['en-GB']);
    $('#txtBookingDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        minDate: 0,
        maxDate: maxStartDays
//        onSelect: function (selectedDate) {
//            $("#txtBookingDateTo").datepicker("option", "minDate", selectedDate);
//        }

    });

    $('#txtBookingDateTo').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        minDate: 0,
        maxDate: maxdate
//        onSelect: function (selectedDate) {
//            $("#txtBookingDate").datepicker("option", "maxDate", selectedDate);
//        }

    });

    $("#btnDatePicker").click(function () {
        $('#txtBookingDate').datepicker("show");
        return false;
    });

    $("#btnDatePickerTo").click(function () {
        $('#txtBookingDateTo').datepicker("show");
        return false;
    });
    
//    $("#txtBookingDateTo").change(function () {

//        check(this);
//        

//    });
    

    window.enquiryPage = new WBFDC.EnquiryPage();
    window.enquiryPage.init();
    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });



});

//function callfixtable() {
////    $('#roomTable').fixheadertable({
////        caption: 'My employees',
////        showhide: true, // default
////        height: 200,
////        width: 800
////    });



//    $(".tableDiv").each(function () {
//        var Id = $(this).get(0).id;
//        var maintbheight = 500;
//        var maintbwidth = 600;
//        //alert(Id);
//        //if (Id != null && Id != "") {
//        $("#" + Id + " .FixedTables").fixedTable({
//                //width: maintbwidth,
//                //height: maintbheight,
//                fixedColumns:-1,
//                // header style
//                classHeader: "th1",
//                // footer style       
//                //classFooter: "fixedFoot",
//                // fixed column on the left       
//                //classColumn: "fixedColumn",
//                // the width of fixed column on the left     
//                //fixedColumnWidth: 100,
//                // table's parent div's id          
//                outerId: Id,
//                // tds' in content area default background color                    
//                Contentbackcolor: "#FFFFFF",
//                // tds' in content area background color while hover.    
//                ////Contenthovercolor: "#99CCFF",
//                // tds' in fixed column default background color  
//                fixedColumnbackcolor: "#97E6FF;",
//                // tds' in fixed column background color while hover.
//                ////fixedColumnhovercolor: "#99CCFF"
//            });
//        //}
//    });
//}

function lpad(originalstr, length, strToPad) {
    while (originalstr.length < length)
        originalstr = strToPad + originalstr;
    return originalstr;
}

function rpad(originalstr, length, strToPad) {
    while (originalstr.length < length)
        originalstr = originalstr + strToPad;
    return originalstr;
}

function dateDiff(){
    var frmdate=null,toDate=null,totalDays="";
    if ($.trim($("#txtBookingDate").val())){
        frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
    }
    if ($.trim($("#txtBookingDateTo").val())){
        toDate  = new Date($("#txtBookingDateTo").datepicker('getDate'));
    }
    
    if ((frmdate!=null && toDate!=null) )
        totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;
    //alert(frmdate+'   '+ toDate+'  '+ totalDays);
    return totalDays;
}
function check(ch) {
    if (ch.id == "txtBookingDate") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtBookingDateTo").datepicker("option", "minDate", '-100Y');
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtBookingDateTo").datepicker("option", "minDate", testm);
        }
    } else if (ch.id == "txtBookingDateTo") {
        //alert(ch.id);
        test = $(ch).datepicker('getDate');
        if (test == null) {
            $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);
        } else {
            testm = new Date(test.getTime());
            testm.setDate(testm.getDate());
            $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);
        }
    }
}
WBFDC.MasterInfo = function () {
    var A = this;
    this.validator;
    this.masterValidator;
    this.init = function () {
        this.validator = A.getValidator();
        //this.masterValidator = A.getmasterValidator();
        $("#txtNOC").change(A.NOCChanged);
        $("#ddlLocation").change(window.enquiryPage.PopulateProperty);
        $("#txtBookingDate").change(A.BookingDateChanged);
        $("#txtBookingDateTo").change(A.BookingDateChangedTo);
        $("#ddlProperty").change(window.enquiryPage.PopulateTariff);
       
        $("#btnAdd").click(A.AddClicked);
        $("#cmdSave").click(A.SubmitClicked);
        $("#btnRoomAdd").click(A.RoomAddClicked);
        $("#btnNamesAdd").click(A.NamesAddClicked);
        if (window.enquiryPage.checkMasterDetail()) {
            $("#cmdSave").removeAttr("disabled");
        } else {
            $("#cmdSave").attr("disabled", "disabled");
        }
        window.enquiryPage.showMasterData(window.enquiryPage.enquiryForm.enquiryMaster);
        window.enquiryPage.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);

    };
    this.NOCChanged=function(){
        var nocValue=$.trim($("#txtNOC").val());
        if (parseInt(nocValue)>1){
            alert("Maximum No of Children Allowed is 1(one).");
            $("#txtNOC").val("1");
        }
    };
    this.RoomAddClicked=function(){
        window.enquiryPage.newAddDetails();
    };
    this.NamesAddClicked = function () {
        window.enquiryPage.newNamesAddDetails();
    };
    this.getValidator = function () {
        return $("#enquiry").validate({ rules: {
            txtName: { required: true },
            txtAddress:{required:true},
            txtCity: { required: true },
            txtState: { required: true },
            txtCountry: { required: true },
            txtEmail: { email: true },
            txtMobile: { required: true, number: true, minlength: 10 },
            //ddlIDProofType: { required: true },
            //txtIDProofNo: { required: true },
            txtNOA: { required: true, number: true },
            txtNOC: { number: true },

            txtBookingDate: { required: true },
            ddlLocation: { required: true },
            ddlProperty: { required: true },
            ddlFrom: { required: true },
            ddlTo: { required: true },
            range: { required: true },
            //ddlAC: { required: true },
            txtNOR: { required: true, number: true }

        },
            messages: {
                txtName: "Please enter your name",
                txtAddress: "Please enter your valid address",
                txtCity: "Please enter your city",
                txtState: "Please select your state",
                txtCountry: "Please select your Country",
                txtEmail: { email: "Please enter a valid email address" },
                mobile: {
                    required: "Please provide a valid mobile number",
                    number: "Please provide a valid mobile number",
                    minlength: "Please provide a valid mobile number"
                },
                //ddlIDProofType: {
                //    required: "Please select a valid ID proof"
                //},
                //txtIDProofNo: "Please enter a valid ID proof no",
                txtNOA: {
                    required: "Please provide a valid no of adult",
                    number: "Please provide a valid no of adult"
                },
                txtNOC: {
                    number: "Number Required"
                },
                txtBookingDate: {
                    required: "Please provide a valid booking date"
                },
                ddlLocation: {
                    required: "Please provide a valid location"
                },
                ddlProperty: {
                    required: "Please provide a valid property"
                },
                ddlFrom: { required: "Please provide a valid range from" },
                ddlTo: { required: "Please provide a valid range to" },
                range: { required: "Please provide a valid range preference" },
                //ddlAC: { required: "Please provide a valid AC/Non AC type" },
                txtNOR: { required: "Please provide a valid no of rooms", number: "Please provide a valid no of rooms" }
            }
        })
    };




    this.AddClicked = function () {

        //alert('addclicked');
        if (A.validator.form()) {
            A.setFormValues();
        }
    };
    this.SubmitClicked = function () {

        //alert('addclicked');
        //if (A.masterValidator.form()) {
        if (window.enquiryPage.checkMasterDetail()) {
            //alert('in submit');
            //window.enquiryPage.SubmitForm();
            window.enquiryPage.checkDateSelected();
        } else {
            alert("Please add details before submitting.");
        }
        //}
    };

    this.BookingDateChanged = function () {
        //alert('changed');
        window.enquiryPage.PopulateTariff();
        check(this);
        var tDays=dateDiff();
        if (tDays!="")
            $("#spanNOD").html(tDays+" Days ");
        else
            $("#spanNOD").html("");
    };
    this.BookingDateChangedTo = function () {
        //alert('changed');
        //A.PopulateTariff();
        check(this);
        var tDays=dateDiff();
        if (tDays!="")
            $("#spanNOD").html(tDays+" Days ");
        else
            $("#spanNOD").html("");
    };
    this.setFormValues = function () {
        var B = window.enquiryPage.enquiryForm;

        B.enquiryMaster.Name = $("#enquiry").field("txtName");

        B.enquiryMaster.Address = $("#enquiry").field("txtAddress");
        B.enquiryMaster.City = $("#enquiry").field("txtCity");
        B.enquiryMaster.State = $("#enquiry").field("txtState");
        B.enquiryMaster.Country = $("#enquiry").field("txtCountry");
        B.enquiryMaster.Mobile = $("#enquiry").field("txtMobile");
        B.enquiryMaster.Email = $("#enquiry").field("txtEmail");
        B.enquiryMaster.IDProofTypeID = $("#enquiry").field("ddlIDProofType");
        B.enquiryMaster.IDProofNo = $("#enquiry").field("txtIDProofNo");
        B.enquiryMaster.NOA = $("#enquiry").field("txtNOA");
        B.enquiryMaster.NOC = $("#enquiry").field("txtNOC");
        //alert('master added');
        window.enquiryPage.checkGuestExistence();

        //window.enquiryPage.showRoomAvailablity();

    };
    
};
WBFDC.EnquiryPage = function () {
    var A = this;
    this.enquiryForm;
    this.init = function () {
        this.enquiryForm = JSON.parse($("#enquiryJSON").val());
        //alert(JSON.stringify(this.enquiryForm));
        this.masterInfo = new WBFDC.MasterInfo();
        this.masterInfo.init();
    };
    this.showRoomAvailablity = function (tot) {
        $(".loading-overlay").show();
        var C = "{dateFrom:'" + $("#txtBookingDate").val() + "',dateTo:'" + $("#txtBookingDateTo").val() +
                "',propertyID:" + $("#ddlProperty").val() + ",locationID:" + $("#ddlLocation").val() +
                ",RangeFrom:" + $("#ddlFrom").val() + ",RangeTo:" + $("#ddlTo").val() + ",RangeOrderPreference:'" +
                (($("#rdAsc").is(":checked")) ? $("#rdAsc").val() : $("#rdDesc").val()) + "',TotalRoomAllowed:" + $("#TotalRoomsAllowed").val() + "}"
        $.ajax({
            type: "POST",
            url: pageUrl + '/ShowAvaliablity',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                //window.enquiryPage.enquiryForm = JSON.parse(D.d);
                //alert(JSON.parse(D.d));
                //alert("this is it");
                $("#divroomTable").html(JSON.parse(D.d));
                $(".loading-overlay").hide();
                //callfixtable();
                A.getOtherValue(tot);
                $("#loadBookingDet").show();
                //A.PageRedirection();
            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.getOtherValue = function (tot) {
        var otherHtml = '';
        ////        var B = window.enquiryPage.enquiryForm;
        ////        var totBooking = $("#TotalBookingNo").val();
        ////        var totAdded = B.enquiryMaster.TotalNoOfRooms;
        ////        var tot = (parseInt(totBooking)) - parseInt(totAdded);
        otherHtml += " Select Prefered Room(s) of " + $.trim($("#ddlLocation option:selected").text());
        //otherHtml += "<div style='font-size:11px;color:maroon;float:right;margin-right:20px;'>Maximum <span id='allowed' >" + tot + "</span> rooms.</div>"
        $("#divCaption").html(otherHtml);
    };
    this.RoomBookedCheked = function (ch) {
        var $this = $(ch);

        var LocID = parseInt($("#ddlLocation").val());
        var PropID = parseInt($("#ddlProperty").val());


        //var allowed = $("#allowed").html();
        //alert($this.attr("id") + "  allowed " + allowed);
        var idtext = $this.attr("id").split("_");
        if ($this.is(":checked")) {
            var dt = idtext[2];
            dt = dt.replace("/", "");
            dt = dt.replace("/", "");
            var $i = $("#lr_" + dt);
            var allowed = $i.html();
            //alert(allowed);
            if (parseInt(allowed) > 0) {
                var name = $.trim($this.parents('tr:first').find('th:first').text());
                //alert(idtext[0] + "  " + idtext[1] + "  " + idtext[2]+"   name   "+name);
                //allowed = parseInt(allowed) - 1;
                //$("#allowed").html(allowed);


                allowed = parseInt(allowed) - 1;
                $i.html(allowed);



                var B = window.enquiryPage.enquiryForm;
                var C = WBFDC.Utils.fixArray(B.enquiryLocationRoom);
                var D = WBFDC.Utils.fixArray(B.tempLocationRoom);
                //alert(D);
                var found = false;
                var foundD = false;
                if (C == "" && D == "") {
                    C.push({
                        LocationID: LocID,
                        PropertyID: PropID,
                        BookingDate: idtext[2],
                        TotalRoomsBooked: 1
                    });
                    D.push({
                        LocationID: LocID,
                        PropertyID: PropID,
                        BookingDate: idtext[2],
                        TotalRoomsBooked: 1
                    });
                } else {
                    for (var i in C) {
                        if (C[i].LocationID == LocID && C[i].PropertyID == PropID && C[i].BookingDate == idtext[2]) {
                            C[i].TotalRoomsBooked += 1;
                            found = true;
                        }
                    }
                    for (var j in D) {
                        if (D[j].LocationID == LocID && D[j].PropertyID == PropID && D[j].BookingDate == idtext[2]) {
                            D[j].TotalRoomsBooked += 1;
                            foundD = true;
                        }
                    }
                    if (!found) {
                        C.push({
                            LocationID: LocID,
                            PropertyID: PropID,
                            BookingDate: idtext[2],
                            TotalRoomsBooked: 1
                        });
                    }
                    if (!foundD) {
                        D.push({
                            LocationID: LocID,
                            PropertyID: PropID,
                            BookingDate: idtext[2],
                            TotalRoomsBooked: 1
                        });
                    }
                }
                B.dateRoomID.push({
                    BookingDate: idtext[2],
                    AccomodationDetailID: parseInt(idtext[1]),
                    AccomodationName: name
                });
                //alert(JSON.stringify(B));

            } else {
                alert("You have reached your maximum room selection.");
                $this.attr("checked", false);
            }
        } else {
            var dt = idtext[2];
            dt = dt.replace("/", "");
            dt = dt.replace("/", "");
            var $i = $("#lr_" + dt);
            var allowed = $i.html();
            allowed = parseInt(allowed) + 1;
            $i.html(allowed);

            var B = window.enquiryPage.enquiryForm;
            A.findAndRemove2(B.dateRoomID, "BookingDate", idtext[2], "AccomodationDetailID", parseInt(idtext[1]));
            var C = WBFDC.Utils.fixArray(B.enquiryLocationRoom);
            var D = WBFDC.Utils.fixArray(B.tempLocationRoom);
            for (var i in C) {
                if (C[i].LocationID == LocID && C[i].PropertyID == PropID && C[i].BookingDate == idtext[2]) {
                    C[i].TotalRoomsBooked -= 1;
                    //alert(C[i].TotalRoomsBooked);
                    if (C[i].TotalRoomsBooked == 0) {
                        C.splice(i, 1);
                        break;
                    }
                }
            }
            for (var j in D) {
                if (D[j].LocationID == LocID && D[j].PropertyID == PropID && D[j].BookingDate == idtext[2]) {
                    D[j].TotalRoomsBooked -= 1;
                    //alert(C[i].TotalRoomsBooked);
                    if (D[j].TotalRoomsBooked == 0) {
                        D.splice(j, 1);
                        break;
                    }
                }
            }


            //alert(JSON.stringify(B));
            //allowed = parseInt(allowed) + 1;
            //$("#allowed").html(allowed);
        }
    };

    this.checkDateSelected = function () {
        $(".loading-overlay").show();
        var B = window.enquiryPage.enquiryForm;
        var C = "{\'enquiryForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/CheckDateSelected',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var result = JSON.parse(D.d);
                //alert(JSON.stringify(result));
                var resdt = new Date(result.MinDate);
                //alert(resdt);
                //alert(+resdt + "  maxstartdays  " + +maxStartDays);
                if (+resdt > +maxStartDays) {
                    alert("Your selected dates are not in the range. Start date must be within " + $("#BookingStartDays").val() + " days. Please check the booking rules and regulation before moving forward.");
                    $(".loading-overlay").hide();
                }
                else {
                    window.enquiryPage.SubmitForm();
                    //$(".loading-overlay").hide();
                }
                

            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };

    this.SubmitForm = function () {
        $(".loading-overlay").show();
        var B = window.enquiryPage.enquiryForm;
        var C = "{\'enquiryForm\':\'" + JSON.stringify(B) + "\'}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SaveEnquiry',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.enquiryPage.enquiryForm = JSON.parse(D.d);
                //alert(D.d);
                $(".loading-overlay").hide();
                A.PageRedirection();
            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };

    this.PageRedirection = function () {
        var B = window.enquiryPage.enquiryForm;
        if (B.SMSResult == "SUCCESS") {
            //alert("Success");
            //window.location.href = "EnquiryComplete.aspx?EnquiryTokenID=" + B.enquiryMaster.EnquiryTokenID;
            window.location.href = "Booking.aspx?TID=" + B.enquiryMaster.EnquiryTokenID;
        }
    };
    this.PopulateControl = function (list, control) {
        //alert(JSON.stringify(list));
        if (list.length > 0) {
            control.removeAttr("disabled");
            control.empty().append('<option selected="selected" value="">Please select</option>');
            $.each(list, function () {
                control.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
        else {
            control.empty().append('<option selected="selected" value="-1">Not available<option>');
        }
    };
    this.showMasterData = function (enquiryMaster) {
        //alert(JSON.stringify(enquiryMaster));
        $('#enquiry').field("txtName", enquiryMaster.Name);
        //$('#enquiry').field("txtAddess", enquiryMaster.Address);
        $('#txtAddress').val(enquiryMaster.Address);
        $('#enquiry').field("txtCity", enquiryMaster.City);
        $('#enquiry').field("txtState", enquiryMaster.State);
        $('#enquiry').field("txtCountry", enquiryMaster.Country);
        $('#enquiry').field("txtEmail", enquiryMaster.Email);
        $('#enquiry').field("txtMobile", enquiryMaster.Mobile);
        $('#ddlIDProofType').val(enquiryMaster.IDProofTypeID);
        $('#enquiry').field("txtIDProofNo", enquiryMaster.IDProofNo);
        $('#enquiry').field("txtNOA", enquiryMaster.NOA);
        $('#enquiry').field("txtNOC", enquiryMaster.NOC);
    };

    this.showEnquiryDetail = function (enquiryDetail) {
        var html = '';


        if (enquiryDetail != "undefined") {
            if (enquiryDetail.length > 0) {

                html += " <table align='center' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td colspan='7' class='labelCaption'><hr style='border:solid 1px lightblue' /></td></tr>"
                    + "</table>";
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td class='th'></td>"
                    + "<td class='th'>Booking Date</td>"
                    + "<td class='th'>Location</td>"
                    + "<td class='th'>Property</td>"
                    + "<td class='th'>Tariff Range</td>"
                    + "<td class='th'>No of Rooms</td>"
                    + "<td class='th'>Prefered Room(s)</td>"
                    + "<td class='th'>Guest Names</td>"
                    + "</tr>";

                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";


                var enqDetails = WBFDC.Utils.fixArray(enquiryDetail);
                for (var i in enqDetails) {
                    html += "<tr id='EnquiryTokenDetID_" + enqDetails[i].EnquiryTokenDetID + "'>"
                                    + "<td class='tr'><div><a href='" + pageUrl + '/DeleteEnquiryDetail' + "' onClick='return window.enquiryPage.removeEnquiryDetail(" + enqDetails[i].EnquiryTokenDetID + ");'><img src='images/delete.png' id='deleteEnquiryTokenDetID_" + enqDetails[i].EnquiryTokenDetID + "' /></a></div></td>"
                                + "<td  class='tr' >" + enqDetails[i].BookingDate + "</td>"
                                + "<td  class='tr' >" + enqDetails[i].LocationName + "</td>"
                                + "<td  class='tr' >" + enqDetails[i].PropertyName + "</td>"
                                + "<td  class='tr' >" + enqDetails[i].RangeFrom + ' - ' + enqDetails[i].RangeTo + "</td>"
                                + "<td  class='tr' >" + enqDetails[i].NOR + "</td>";
                    var roomDetail = WBFDC.Utils.fixArray(enqDetails[i].enquiryRoomDetail);
                    var roomnos = "";
                    var GuestNames = "";
                    if (roomDetail != null && roomDetail.length > 0) {
                        //alert('in roomdetail');
                        for (var k in roomDetail) {
                            roomnos += roomDetail[k].AccomodationName + ", ";
                            GuestNames += roomDetail[k].enquiryName.GuestName + ", ";
                        }
                        //alert(roomnos);
                        if (roomnos != "") {
                            roomnos = $.trim(roomnos);
                            roomnos = roomnos.substring(0, roomnos.length - 1);
                        }
                        if (GuestNames != "") {
                            GuestNames = $.trim(GuestNames);
                            GuestNames = GuestNames.substring(0, GuestNames.length - 1);
                        }
                    }
                    html += "<td  class='tr' >" + roomnos + "</td>";
                    html += "<td  class='tr' >" + GuestNames + "</td>";
                    html += "</tr>";
                    html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
                }
                html += "</table>";
            }
        }
        $("#divTable").empty().html(html);
    };
    this.removeEnquiryDetail = function (EnquiryTokenDetID) {
        //alert('in remove ' + EnquiryTokenDetID);
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#EnquiryTokenDetID_" + EnquiryTokenDetID;
        $(removeElemTr).append(ajaxLoader);
        var ajaxUrl = pageUrl + '/DeleteEnquiryDetail';

        var B = {};
        //alert('b empty');
        B.enquiryForm = window.enquiryPage.enquiryForm;

        A.findAndRemove(B.enquiryForm.enquiryDetail, 'EnquiryTokenDetID', EnquiryTokenDetID);
        //alert(JSON.stringify(B.enquiryForm));
        //alert('b added');
        var E = "{\'enquiryForm\':\'" + JSON.stringify(B.enquiryForm) + "\',\'EnquiryTokenDetID\':\'" + EnquiryTokenDetID + "\'}";
        //var C = JSON.stringify(E);
        //alert(C);
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.enquiryPage.enquiryForm = JSON.parse(D.d);

                //alert(JSON.stringify(window.enquiryPage.enquiryForm));
                $("#divTable table[id='detailTable']").remove();
                A.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);
                $("#divTable table[id='prodTab']").hide();
                $("#divTable table[id='prodTab']").show("slow");

            }
        });
        return false;
    };

    this.checkMasterDetail = function () {
        var B = window.enquiryPage.enquiryForm;
        var enquiryMaster = B.enquiryMaster;
        var EnquiryTokenID = enquiryMaster.EnquiryTokenID;
        var enquiryDetail = B.enquiryDetail;

        if ((parseInt(EnquiryTokenID) > 0) && (enquiryDetail.length > 0)) {
            return true;
        } else {
            return false;
        }
    };

    this.findAndRemove = function (array, property, value) {
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;

            }
        });
    };

    this.findAndRemove3 = function (array, property, value, property2, value2, property3, value3) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2 && result[property3] == value3) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };

    this.findAndRemove2 = function (array, property, value, property2, value2) {
        $.each(array, function (index, result) {
            if (result[property] == value && result[property2] == value2) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    this.CloseContent = function () {
        $("#loadBookingDet").hide();
        window.enquiryPage.enquiryForm.dateRoomID = [];
        window.enquiryPage.enquiryForm.tempLocationRoom = [];
        window.enquiryPage.enquiryForm.tempNames = [];
        return false;
    };
    this.CloseNamesContent = function () {
        $("#LoadNamesDet").hide();
        window.enquiryPage.enquiryForm.dateRoomID = [];
        window.enquiryPage.enquiryForm.tempLocationRoom = [];
        window.enquiryPage.enquiryForm.tempNames = [];
        return false;
    };
    this.captureDetails = function () {
        $(".loading-overlay").show();
        var B = {};
        //alert('b empty');
        B.enquiryForm = window.enquiryPage.enquiryForm;
        //alert('b added');
        var E = "{\'enquiryForm\':\'" + JSON.stringify(B.enquiryForm) + "\'}";
        //var C = JSON.stringify(E);
        //alert(C);
        $.ajax({
            type: "POST",
            url: pageUrl + '/AddDetails',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                window.enquiryPage.enquiryForm = JSON.parse(D.d);
                //alert(JSON.stringify(D));
                //alert(JSON.stringify(window.enquiryPage.enquiryForm));
                //alert("this is again");
                //alert(window.enquiryPage.enquiryForm.enquiryMaster.NOA);
                A.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);
                A.ClearBookingDetails();
                if (A.checkMasterDetail()) {
                    $("#cmdSave").removeAttr("disabled");
                } else {
                    $("#cmdSave").attr("disabled", "disabled");
                }
                $(".loading-overlay").hide();
            },
            error: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };
    this.newNamesAddDetails = function () {
        //var v = A.ValidateNameForm();
        //alert(v);
        $("#enquiry").validate();
        //alert('form  validated');
        //alert($("#enquiry").valid());
        if ($("#enquiry").valid()) {
            //A.addAfterValidate();
            A.SaveTempNames();
            A.addAfterValidate();
            //alert('validated');
        } else {

        }

        return false;

    };
    this.SaveTempNames = function () {

        var B = window.enquiryPage.enquiryForm;
        var dateRoom = WBFDC.Utils.fixArray(A.enquiryForm.dateRoomID);
        if (dateRoom == "") {
            alert('Select Prefered Rooms before adding ');
            return false;
        }

        var arr = WBFDC.Utils.fixArray(B.tempLocationRoom);

        arr = $.map(arr, function (o) { return o.TotalRoomsBooked; });
        var highest = Math.max.apply(this, arr);
        var enqName = WBFDC.Utils.fixArray(B.enquiryName);
        var tempname = []; //WBFDC.Utils.fixArray(B.tempNames);
        if (enqName == "") {
            for (var i = 0; i < highest; i++) {

                var ele1 = $("#nameTable input[name=n_" + i + "]").val();
                var ele2 = $("#nameTable input[name=a_" + i + "]").val();
                var ele3 = $("#nameTable select[name=s_" + i + "]").val();
                //alert(ele1 + " " + ele2 + " " + ele3);
                //alert(tempname);
                tempname.push({
                    GuestName: ele1,
                    Age: parseInt($.trim(ele2)),
                    Sex: ele3
                });


                //alert(JSON.stringify(tempname));
                if (enqName == "") {
                    enqName.push({
                        GuestName: ele1,
                        Age: parseInt($.trim(ele2)),
                        Sex: ele3
                    });
                } else {
                    var found = false;
                    for (var k in enqName) {
                        if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                            found = true;
                        }
                    }
                    if (!found) {
                        enqName.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });
                    }
                }
            }

        } else {
            if ($("#rdOld").is(":checked")) {
                for (var i = 0; i < enqName.length; i++) {

                    var $chk = $("#nameTable input[name=chkN_" + i + "]");
                    if ($chk.is(":checked")) {
                        var ele1 = $("#nameTable input[name=n_" + i + "]").val();
                        var ele2 = $("#nameTable input[name=a_" + i + "]").val();
                        var ele3 = $("#nameTable select[name=s_" + i + "]").val();
                        //alert(ele1 + " " + ele2 + " " + ele3);
                        //alert(tempname);
                        tempname.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });


                        //alert(JSON.stringify(tempname));
                        if (enqName == "") {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        } else {
                            var found = false;
                            for (var k in enqName) {
                                if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                    found = true;
                                }
                            }
                            if (!found) {
                                enqName.push({
                                    GuestName: ele1,
                                    Age: parseInt($.trim(ele2)),
                                    Sex: ele3
                                });
                            }
                        }
                    }
                }
                var enlength = enqName.length;
                if (enlength < highest) {
                    var remain = highest - enlength;
                    for (var j = 0; j < remain; j++) {
                        ////html += "<tr id='trj_" + j + "'>";
                        ////html += "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='nj_" + j + "' name='nj_" + j + "' value=''/></<td>"
                        ////    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='aj_" + j + "' name='aj_" + j + "' value=''/></<td>"
                        ////    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='sj_" + j + "' name='sj_" + j + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                        ////    + "<td></td>";
                        ////html += "</tr>";
                        ///
                        var ele1 = $("#nameTable input[name=nj_" + j + "]").val();
                        var ele2 = $("#nameTable input[name=aj_" + j + "]").val();
                        var ele3 = $("#nameTable select[name=sj_" + j + "]").val();
                        //alert(ele1 + " " + ele2 + " " + ele3);
                        //alert(tempname);
                        tempname.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });


                        //alert(JSON.stringify(tempname));
                        if (enqName == "") {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        } else {
                            var found = false;
                            for (var k in enqName) {
                                if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                    found = true;
                                }
                            }
                            if (!found) {
                                enqName.push({
                                    GuestName: ele1,
                                    Age: parseInt($.trim(ele2)),
                                    Sex: ele3
                                });
                            }
                        }
                    }
                }


            } else if ($("#rdNew").is(":checked")) {
                for (var i = 0; i < highest; i++) {

                    var ele1 = $("#nameNewTable input[name=n_" + i + "]").val();
                    var ele2 = $("#nameNewTable input[name=a_" + i + "]").val();
                    var ele3 = $("#nameNewTable select[name=s_" + i + "]").val();
                    //alert(ele1 + " " + ele2 + " " + ele3);
                    //alert(tempname);
                    tempname.push({
                        GuestName: ele1,
                        Age: parseInt($.trim(ele2)),
                        Sex: ele3
                    });


                    //alert(JSON.stringify(tempname));
                    if (enqName == "") {
                        enqName.push({
                            GuestName: ele1,
                            Age: parseInt($.trim(ele2)),
                            Sex: ele3
                        });
                    } else {
                        var found = false;
                        for (var k in enqName) {
                            if (enqName[k].GuestName == $.trim(ele1) && enqName[k].Age == parseInt($.trim(ele2))) {
                                found = true;
                            }
                        }
                        if (!found) {
                            enqName.push({
                                GuestName: ele1,
                                Age: parseInt($.trim(ele2)),
                                Sex: ele3
                            });
                        }
                    }
                }
            }
        }
        B.tempNames = tempname;
        //alert(JSON.stringify(B.tempNames));
    }
    this.ClearBookingDetails = function () {
        $("#txtBookingDate").val('');
        $("#txtBookingDate").datepicker("option", "minDate", 0);
        $("#txtBookingDate").datepicker("option", "maxDate", maxStartDays);

        $("#txtBookingDateTo").val('');
        $("#txtBookingDateTo").datepicker("option", "minDate", 0);
        $("#txtBookingDateTo").datepicker("option", "maxDate", maxdate);
        $("#spanNOD").html("");
        $("#ddlLocation").val("");
        A.PopulateProperty();
        $("#ddlProperty").val("");
        A.PopulateTariff();
        $("#rdAsc").attr("checked", true);
        $("#ddlAC").val("");
        $("#chkAC").attr("checked", false);
        $("#chkPart").attr("checked", false);
        $("#txtNOR").val("");
        $("#ddlAlternate").val(0);
    }
    this.sortJSON = function (data, key) {
        return data.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    };
    this.addAfterValidate = function () {

        //////////////var people = [ 
        //////////////{ 'myKey': 'A', 'status': 0 },
        //////////////{ 'myKey': 'D', 'status': 3 },
        //////////////{ 'myKey': 'E', 'status': 3 },
        //////////////{ 'myKey': 'F', 'status': 2 },
        //////////////{ 'myKey': 'G', 'status': 7 }      
        //////////////];
        //////////////var people2 = A.sortJSON(people, 'status');
        //////////////alert("1. After processing (0 to x): " + JSON.stringify(people2));

        var B = window.enquiryPage.enquiryForm;
        var arr = WBFDC.Utils.fixArray(B.tempLocationRoom);
        arr = $.map(arr, function (o) { return o.TotalRoomsBooked; });
        var highest = Math.max.apply(this, arr);
        //alert(highest);

        var tempname = WBFDC.Utils.fixArray(B.tempNames);
        if (tempname == "") {
            alert('Guest Names Details Not Added');
            return false;
        } else if (tempname.length != highest) {
            alert('Guest Names Details Not Added');
            return false;
        }

        var frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
        var toDate = new Date($("#txtBookingDateTo").datepicker('getDate'));
        var totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;


        var totBooking = $("#TotalBookingNo").val();
        var nor = ($("#enquiry").field("txtNOR"));
        var totAdded = B.enquiryMaster.TotalNoOfRooms;
        var tot = (parseInt(nor) * parseInt(totalDays)) + parseInt(totAdded);

        if (tot <= totBooking) {
            for (var i = 0; i < totalDays; i++) {
                var frmdate1 = new Date(frmdate.getTime() + i * 24 * 60 * 60 * 1000);
                var curr_date = frmdate1.getDate();
                var curr_month = frmdate1.getMonth();
                curr_month = curr_month + 1;
                var curr_year = frmdate1.getFullYear();
                var cdate = curr_date + '/' + curr_month + '/' + curr_year;
                var dateRoom = WBFDC.Utils.fixArray(A.enquiryForm.dateRoomID);
                var enqName1 = WBFDC.Utils.fixArray(A.enquiryForm.tempNames);
                var enqName = A.sortJSON(enqName1, "Age");
                ////alert("EnqName : =>  " + JSON.stringify(enqName));
                var enquiryRoomDet = [];
                var TotalRoomsSelected = 0;
                var k = 0;
                if (dateRoom.length > 0) {
                    //alert(dateRoom.length);

                    for (var j in dateRoom) {
                        var spldt = (dateRoom[j].BookingDate).split('/');
                        var bookingdt = new Date();
                        bookingdt.setFullYear(spldt[2], (spldt[1] - 1), spldt[0]);
                        var chkdate = new Date();
                        chkdate.setFullYear(curr_year, (curr_month - 1), curr_date);
                        //alert(" bookingdt----> " + bookingdt.getTime() + " ----- frmdate1----> " + chkdate.getTime());
                        if (chkdate.getTime() == bookingdt.getTime()) {
                            //alert('matched cdate=' + cdate + ' roomid=' + dateRoom[j].AccomodationDetailID + '  i=' + i + " j=" + j + " k=" + k);
                            //var enquirName=B.enquiryDetail.enquiryRoomDetail.enquiryName ;
                            var enquirName = {};
                            //.push({
                            enquirName.GuestName = enqName[k].GuestName;
                            enquirName.Age = enqName[k].Age;
                            enquirName.Sex = enqName[k].Sex;
                            //});
                            k += 1;
                            enquiryRoomDet.push({
                                BookingDate: cdate,
                                AccomodationDetailID: dateRoom[j].AccomodationDetailID,
                                AccomodationName: dateRoom[j].AccomodationName,
                                enquiryName: enquirName
                            });
                            //alert(JSON.stringify(enquiryRoomDet));
                        }
                    }
                    if (k > 0) {
                        B.enquiryDetail.push({
                            BookingDate: cdate,
                            LocationID: $("#enquiry").field("ddlLocation"),
                            LocationName: $.trim($("#ddlLocation option:selected").text()),
                            PropertyID: $("#enquiry").field("ddlProperty"),
                            PropertyName: $.trim($("#ddlProperty option:selected").text()),
                            RangeFrom: $("#enquiry").field("ddlFrom"),
                            RangeTo: $("#enquiry").field("ddlTo"),
                            RangeOrderPreference: ($("#enquiry").field("range")),
                            ACNonAC: ($("#enquiry").field("ddlAC")),
                            ACNonACCompulsory: ($("#chkAC").is(':checked') ? 'Y' : 'N'),
                            //NOR: ($("#enquiry").field("txtNOR")),
                            NOR: k,
                            AlternateLocation: ($("#enquiry").field("ddlAlternate")),
                            PartBooking: ($("#chkPart").is(':checked') ? 'Y' : 'N'),
                            enquiryRoomDetail: enquiryRoomDet
                        });
                    }
                }
            }

            B.enquiryMaster.TotalNoOfRooms = tot;

            B.dateRoomID = [];
            B.tempLocationRoom = [];
            B.tempNames = [];
            //alert(JSON.stringify(window.enquiryPage.enquiryForm));
            //$("#loadBookingDet").hide();
            $("#LoadNamesDet").hide();
            window.enquiryPage.captureDetails();
            //window.enquiryPage.showEnquiryDetail();

        } else {
            alert("Maximum Allowed Booking is " + totBooking + ". Either " + totBooking + " days OR " + totBooking + " No of Rooms. You have booked " + totAdded + " room(s)/day(s). You can book " + (totBooking - totAdded) + " total no of rooms/days");
            $(".loading-overlay").hide();
        }
    }
    this.newAddDetails = function () {
        var B = window.enquiryPage.enquiryForm;
        var dateRoom = WBFDC.Utils.fixArray(A.enquiryForm.dateRoomID);
        if (dateRoom == "") {
            alert('Select Prefered Rooms before adding ');
            return false;
        }
        //alert(JSON.stringify(dateRoom));
        mindt = new Date(100000000 * 86400000);
        mindt.setHours(0, 0, 0, 0);
        //mindt = new Date();
        //alert(mindt);
        traverse(dateRoom, compare);
        mindt.setHours(0, 0, 0, 0);
        //alert("mindate " + mindt + " max start date  " + maxStartDays);

        //alert("mindate " + mindt + " max start date  " + maxStartDays);
        //alert(+maxStartDays + "    " + +mindt);
        //alert(+mindt > +maxStartDays);
        if (+mindt > +maxStartDays) {
            alert("Start date must be within " + $("#BookingStartDays").val() + " days;")
            return false;
        }

        //var B = window.enquiryPage.enquiryForm;
        var arr = WBFDC.Utils.fixArray(B.tempLocationRoom);

        arr = $.map(arr, function (o) { return o.TotalRoomsBooked; });
        var highest = Math.max.apply(this, arr);
        //alert(highest);

        var enqName = WBFDC.Utils.fixArray(B.enquiryName);
        var enlength = enqName.length;

        var html = "";
        // html+="<form id='ms'></form> <form id='nameform' >";

        //$("#nameform").validate();
        html += "<table align='center' id='nameTable' class='divTable' cellpadding='5' width='80%'>" + "\n"
                    + "<tr>"
                    + "<td class='th' width='30%'>Guest Name</td>"
                    + "<td class='th' width='20%'>Age</td>"
                    + "<td class='th' width='20%'>Sex</td>"
                    + "<td class='th' width='20%'></td>"
                    + "</tr>";

        html += "<tr><td colspan='4' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
        //var s = $("<select id=\"" + total + "\" name=\"" + total + "\" />");            // 4-
        //for (var val in data) {
        //    $("<option />", { value: val, text: data[val] }).appendTo(s);
        //}
        if (enqName.length <= 0) {
            for (var i = 0; i < highest; i++) {
                html += "<tr id='tr_" + i + "'>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                    + "<td></td>"
                    + "</tr>";
                //$("#n_" + i).rules("add", { required: true, messages: { required: "Please Guest Name" } });
                //$("#a_" + i).rules("add", { required: true, messages: { required: "Please Guest Age" } });
                //$("#s_" + i).rules("add", { required: true, messages: { required: "Please Guest Sex" } });
            }
        } else {

            for (var i = 0; i < enlength; i++) {
                html += "<tr id='tr_" + i + "'>";
                //alert(i);
                //if (i <= highest - 1) {
                //////html += "<td class='tr' align='left'>" + enqName[i].GuestName + "</<td>"
                //////    + "<td class='tr' align='left'>" + enqName[i].Age + "</<td>"
                //////    + "<td class='tr' align='left'>" + (enqName[i].Sex == "M" ? "Male" : "Female") + "</<td>"
                //////    + "<td><input type='checkbox' id='chkN_" + i + "' "+(i<=highest-1?"checked='checked'":"")+" /></td>";

                html += "<td class='tr' align='left'><input type='text' disabled='disabled' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value='" + enqName[i].GuestName + "'/></<td>"
                        + "<td class='tr' align='left'><input type='text' disabled='disabled' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value='" + enqName[i].Age + "'/></<td>"
                        + "<td class='tr' align='left'><select type='text' disabled='disabled' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M' " + (enqName[i].Sex == 'M' ? "selected='selected'" : "") + ">Male</option><option value='F' " + (enqName[i].Sex == 'F' ? "selected='selected'" : "") + ">Female</option></select></<td>"
                        + "<td><input type='checkbox' id='chkN_" + i + "' name='chkN_" + i + "' " + (i <= highest - 1 ? "checked='checked'" : "") + "  /></td>";
                ///////onclick = 'return window.enquiryPage.NamesCheckedClick(this);'
                //}else{
                //    html +=  "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                //    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                //    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                //    + "<td></td>"
                //}
                html += "</tr>";
            }

            if (enlength < highest) {
                var remain = highest - enlength;
                for (var j = 0; j < remain; j++) {
                    html += "<tr id='trj_" + j + "'>";
                    html += "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='nj_" + j + "' name='nj_" + j + "' value=''/></<td>"
                        + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='aj_" + j + "' name='aj_" + j + "' value=''/></<td>"
                        + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='sj_" + j + "' name='sj_" + j + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                        + "<td></td>";
                    html += "</tr>";
                }
            }

        }

        html += "</table>";
        //html += "</form>";

        if (enqName.length > 0) {
            html += "<table align='center' id='nameNewTable' style='display:none;' class='divTable' cellpadding='5' width='80%'>" + "\n"
                        + "<tr>"
                        + "<td class='th' width='30%'>Guest Name</td>"
                        + "<td class='th' width='20%'>Age</td>"
                        + "<td class='th' width='20%'>Sex</td>"
                        + "<td class='th' width='20%'></td>"
                        + "</tr>";

            html += "<tr><td colspan='4' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";
            for (var i = 0; i < highest; i++) {
                html += "<tr id='tr_" + i + "'>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required'  id='n_" + i + "' name='n_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><input type='text' class='inputbox2 required number' maxlength='2' style='width:80px;' id='a_" + i + "' name='a_" + i + "' value=''/></<td>"
                    + "<td class='tr' align='left'><select type='text' class='citybox2' style='width:100px;' id='s_" + i + "' name='s_" + i + "' ><option value='M'>Male</option><option value='F'>Female</option></select></<td>"
                    + "<td></td>"
                    + "</tr>";

            }
            html += "</table>";
        }


        $("#divNameTable").empty().html(html);

        var otherHtml = '';

        otherHtml += " Enter Guest Details of Room(s) of " + $.trim($("#ddlLocation option:selected").text());
        var OptionHtml = '';
        OptionHtml += "<div style='float:right;' >"
            + "<label for='rdOld'  style='padding-right:5px;font-size:11px;padding-left:5px'>Select Old Names</label><input type='radio' checked='checked' id='rdOld' name='rdOld' value='O' onclick='return window.enquiryPage.RadionSelected(this)' />"
            + "<label for='rdNew'  style='padding-right:5px;font-size:11px;padding-left:5px'>Enter New Names</label><input type='radio'  id='rdNew' name='rdOld' value='N' onclick='return window.enquiryPage.RadionSelected(this)'  />"
            + "</div>";

        if (enqName.length > 0) {
            otherHtml += OptionHtml;
        }
        $("#NamesCaption").html(otherHtml);

        $("#loadBookingDet").hide();

        $("#LoadNamesDet").show();
        //return $("#nameform").validate();
    };
    this.RadionSelected = function (ch) {
        var $this = $(ch);
        var optionValue = $this.val();
        if (optionValue == 'O') {
            $("#nameTable").show();
            $("#nameNewTable").hide();
        } else {
            $("#nameTable").hide();
            $("#nameNewTable").show();
        }
    };
    this.NamesCheckedClick = function (ch) {
        var $this = $(ch);
        var thisid = $this.attr("id");
        var pos = thisid.substring(thisid.indexOf("_") + 1);
        if ($this.is(":checked")) {
            var B = window.enquiryPage.enquiryForm;
            var enqName = WBFDC.Utils.fixArray(B.enquiryName);
            $("#tr_" + pos + " input[name=n_" + pos + "]").val(enqName[pos].GuestName).attr("disabled", "disabled");
            $("#tr_" + pos + " input[name=a_" + pos + "]").val(enqName[pos].Age).attr("disabled", "disabled");
            $("#tr_" + pos + " select[name=s_" + pos + "]").val(enqName[pos].Sex).attr("disabled", "disabled");
        } else {
            $("#tr_" + pos + " input[name=n_" + pos + "]").val("").removeAttr("disabled");
            $("#tr_" + pos + " input[name=a_" + pos + "]").val("").removeAttr("disabled");
            $("#tr_" + pos + " select[name=s_" + pos + "]").val("M").removeAttr("disabled");
        }
    };
    this.checkGuestExistence = function () {
        $(".loading-overlay").show();
        var B = window.enquiryPage.enquiryForm;
        var E = "{mobileno:" + B.enquiryMaster.Mobile + ",propertyID:" + $("#ddlProperty").val() + ",locationID:" + $("#ddlLocation").val() + "}";
        //var C = JSON.stringify(E);
        //alert(E);
        $.ajax({
            type: "POST",
            url: pageUrl + '/CheckGuest',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = jQuery.parseJSON(D.d);
                window.enquiryPage.enquiryForm.enquiryMaster.TotalNoOfRooms = t.TotalNoofRooms;

                var frmdate = new Date($("#txtBookingDate").datepicker('getDate'));
                var toDate = new Date($("#txtBookingDateTo").datepicker('getDate'));
                var totalDays = Math.floor((toDate - frmdate) / (1000 * 60 * 60 * 24)) + 1;


                var B = window.enquiryPage.enquiryForm;
                var totBooking = $("#TotalBookingNo").val();
                var nor = ($("#enquiry").field("txtNOR"));


                var totAdded = B.enquiryMaster.TotalNoOfRooms;
                var tot = (parseInt(nor) * parseInt(totalDays)) + parseInt(totAdded);
                //alert("totBooking " + totBooking + "  nor  " + nor + " totAdded " + totAdded + " tot  " + tot + "  frmdate " + frmdate);

                if (tot <= totBooking) {
                    A.showRoomAvailablity(totBooking - parseInt(totAdded));
                    //////////////                    for (var i = 0; i < totalDays; i++) {
                    //////////////                        var frmdate1 = new Date(frmdate.getTime() + i * 24 * 60 * 60 * 1000);
                    //////////////                        var curr_date = frmdate1.getDate();
                    //////////////                        var curr_month = frmdate1.getMonth();
                    //////////////                        curr_month = curr_month + 1;
                    //////////////                        var curr_year = frmdate1.getFullYear();

                    //////////////                        var cdate = curr_date + '/' + curr_month + '/' + curr_year;
                    //////////////                        B.enquiryDetail.push({
                    //////////////                            BookingDate: cdate,
                    //////////////                            LocationID: $("#enquiry").field("ddlLocation"),
                    //////////////                            LocationName: $("#ddlLocation option:selected").text().trim(),
                    //////////////                            PropertyID: $("#enquiry").field("ddlProperty"),
                    //////////////                            PropertyName: $("#ddlProperty option:selected").text().trim(),
                    //////////////                            RangeFrom: $("#enquiry").field("ddlFrom"),
                    //////////////                            RangeTo: $("#enquiry").field("ddlTo"),
                    //////////////                            RangeOrderPreference: ($("#enquiry").field("range")),
                    //////////////                            ACNonAC: ($("#enquiry").field("ddlAC")),
                    //////////////                            ACNonACCompulsory: ($("#chkAC").is(':checked') ? 'Y' : 'N'),
                    //////////////                            NOR: ($("#enquiry").field("txtNOR")),
                    //////////////                            AlternateLocation: ($("#enquiry").field("ddlAlternate")),
                    //////////////                            PartBooking: ($("#chkPart").is(':checked') ? 'Y' : 'N')
                    //////////////                        });
                    //////////////                    }
                    //////////////                    //B.enquiryDetail = enquiryDetail;
                    //////////////                    B.enquiryMaster.TotalNoOfRooms = tot;
                    //////////////                    //alert(JSON.stringify(window.enquiryPage.enquiryForm));
                    //////////////                    window.enquiryPage.captureDetails();
                    //////////////                    //window.enquiryPage.showEnquiryDetail();

                } else {
                    alert("Maximum Allowed Booking is " + totBooking + ". Either " + totBooking + " days OR " + totBooking + " No of Rooms. You have booked " + totAdded + " room(s)/day(s). You can book " + (totBooking - totAdded) + " total no of rooms/days");
                    $(".loading-overlay").hide();
                }

            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });


    };


    this.PopulateProperty = function () {
        //alert('in prop');
        $("#ddlProperty").attr("disabled", "disabled");
        $("#ddlFrom").attr("disabled", "disabled");
        $("#ddlTo").attr("disabled", "disabled");
        if ($('#ddlLocation').val() == "") {
            $('#ddlProperty').empty().append('<option selected="selected" value="">Please select</option>');
            $('#ddlFrom').empty().append('<option selected="selected" value="">Please select</option>');
            $('#ddlTo').empty().append('<option selected="selected" value="">Please select</option>');
        }
        else {
            $('#ddlProperty').empty().append('<option selected="selected" value="">Loading...</option>');
            $.ajax({
                type: "POST",
                url: pageUrl + '/populateProperty',
                data: '{LocationID: ' + $('#ddlLocation').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: A.OnPropertyPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });


        }
    };

    this.PopulateTariff = function () {
        //alert($("#txtBookingDate").val().trim());
        if ($.trim($("#txtBookingDate").val()) != "") {
            //alert('in tariff');
            $("#ddlFrom").attr("disabled", "disabled");
            $("#ddlTo").attr("disabled", "disabled");
            //alert($('#ddlProperty').val());
            if ($('#ddlProperty').val() == "") {
                $('#ddlFrom').empty().append('<option selected="selected" value="">Please select</option>');
                $('#ddlTo').empty().append('<option selected="selected" value="">Please select</option>');
            }
            else {
                $('#ddlFrom').empty().append('<option selected="selected" value="">Loading...</option>');
                $('#ddlTo').empty().append('<option selected="selected" value="">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/populateTariffRange',
                    data: "{BookingDate: '" + $('#txtBookingDate').val() + "',LocationID: " + $('#ddlLocation').val() + ",PropertyID: " + $('#ddlProperty').val() + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: A.OnTariffPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }
    };

    this.OnTariffPopulated = function (response) {
        //alert('in success');
        window.enquiryPage.PopulateControl(response.d, $("#ddlFrom"));
        window.enquiryPage.PopulateControl(response.d, $("#ddlTo"));
    };

    this.OnPropertyPopulated = function (response) {
        window.enquiryPage.PopulateControl(response.d, $("#ddlProperty"));
    };

    //this.ValidateNameForm = function () {
    //    $("#nameform").validate();
    //    alert('after validate called');
    //    $("#nameTable input").each(function () {
    //        var $ele = $(this);
    //        var atr = $ele.attr("id").substring(0, 1);
    //        alert(atr+ "   "+  $ele.attr("id"));
    //        if ( atr== "a") {
    //            $(this).rules("add", { required: true, number: true });
    //        } else {
    //            $(this).rules("add", { required: true });
    //        }

    //    });
    //    alert('end loop after validate called');
    //    return $("#nameform").validate();
    //}

};
















