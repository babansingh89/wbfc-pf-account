﻿

$(document).ready(function () {
   

    window.merchantPage = new WBFDC.MerchantPage();
    window.merchantPage.init();
    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });

});

WBFDC.CancelInfo = function () {
    var A = this;
    var validator;
    this.init = function () {
        this.validator = A.getValidator();
        $("#btnCancelInfoAdd").click(A.AddClicked);
    };

    this.AddClicked = function () {
        A.getValidator();
        if (A.validator.form()) {
            A.setFormValues();

        }
    };

    this.setFormValues = function () {
        var B = window.merchantPage.merchantForm;
        var C = WBFDC.Utils.fixArray(B.MerchantCancel);
        var FrmDt = parseInt($("#txtFromDays").val());
        var ToDt = parseInt($("#txtToDays").val());
        var Perc = parseFloat($("#txtPercentage").val());
        C.push({
            FromDays: FrmDt,
            ToDays: ToDt,
            Percentage: Perc
        });
//        alert(JSON.stringify(C));
//        alert(JSON.stringify(B));
        window.merchantPage.newAddDetails();
        A.clearTxt();
    };

    this.getValidator = function () {
        $("#txtFromDays").removeClass("ignore");
        $("txtToDays").removeClass("ignore");
        $("txtPercentage").removeClass("ignore");

        $("#frmEcom").validate();
        $("#txtFromDays").rules("add", { required: true, number: true, messages: { required: "Please from days", number: "Number Required"} });
        $("#txtToDays").rules("add", { required: true, number: true, messages: { required: "Please to days", number: "Number Required"} });
        $("#txtPercentage").rules("add", { required: true, number: true, messages: { required: "Please percentage", number: "Number Required"} });

        return $("#frmEcom").validate();
    };
    this.clearTxt = function () {
        ($("#txtFromDays").val("0"));
        ($("#txtToDays").val("0"));
        ($("#txtPercentage").val("0"));
    }
};

WBFDC.MerchantPage = function () {
    var A = this;
    this.merchantForm;
    this.init = function () {
        this.merchantForm = JSON.parse($("#merchantJSON").val());
        //alert(JSON.stringify(this.merchantForm));
        this.cancelInfo = new WBFDC.CancelInfo();
        this.cancelInfo.init();
        var B = A.merchantForm;
        var C = B.MerchantCancel;
        A.showMerchantDetail(C);
    };

    this.newAddDetails = function (tot) {
        $(".loading-overlay").show();
        var B = {};
        B.merchantForm = window.merchantPage.merchantForm;

        var E = "{\'merchantForm\':\'" + JSON.stringify(B.merchantForm) + "\'}";
        //alert(E);
        //var C = "{FromDays:" + $("#txtFromDays").val() + ",ToDays:" + $("#txtToDays").val() +
        //      ",Percentage:" + $("#txtPercentage").val() + "}"
        //var C = "{CancelPolicyID:'" + $("#hdnID").val() + "}"
        $.ajax({
            type: "POST",
            url: pageUrl + '/AddDetails',
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                A.merchantForm = JSON.parse(D.d);
                //alert(JSON.stringify(A.merchantForm));
                var B = A.merchantForm;
                var C = B.MerchantCancel;
                A.showMerchantDetail(C);
                $(".loading-overlay").hide();

            },
            error: function (response) {
                alert('Some Error Occur');
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });
    };

    this.showMerchantDetail = function (cancelInfo) {
        var html = '';

        if (cancelInfo != "undefined") {
            if (cancelInfo.length > 0) {

                html += " <table align='center' cellpadding='5' width='98%'>" + "\n"
                    + "<tr><td colspan='7' class='labelCaption'><hr style='border:solid 1px lightblue' /></td></tr>"
                    + "</table>";
                html += "<table align='center' id='detailTable' class='divTable' cellpadding='5' width='98%'>" + "\n"
                    //+ "<tr><td class='th'></td>"
                    + "<td class='th'>From Days</td>"
                    + "<td class='th'>To Days</td>"
                    + "<td class='th'>Percentage</td>"
                    + "</tr>";

                html += "<tr><td colspan='8' class='labelCaption'><hr style='border:solid 1px #cbc7c7' /></td></tr>";

                var enqDetails = WBFDC.Utils.fixArray(cancelInfo);
                for (var i in enqDetails) {
                    html += "<tr id='CancelInfoID_" + enqDetails[i].CancelPolicyID + "'>"
                                //+ "<td class='tr'><div><a href='" + pageUrl + '/DeleteMerchantDetail' + "' onClick='return window.merchantPage.removeMerchantDetail(" + enqDetails[i].CancelPolicyID + ");'><img src='images/delete.png' id='deleteCancelInfoID_" + enqDetails[i].CancelPolicyID + "' /></a></div></td>"
                                + "<td  class='tr' >" + enqDetails[i].FromDays + "</td>"
                                + "<td  class='tr' >" + enqDetails[i].ToDays + "</td>"
                                + "<td  class='tr' >" + enqDetails[i].Percentage + "</td>";

                }
                html += "</table>";
            }
        }
        //alert(html);
        $("#divTable").empty().html(html);
    };

    this.removeMerchantDetail = function (CancelPolicyID) {
        //alert('in remove ' + EnquiryTokenDetID);
        var ajaxLoader = '<img src="images/ajax-loader.gif"/>';
        var removeElemTr = "#CancelPolicyID_" + CancelPolicyID;
        $(removeElemTr).append(ajaxLoader);
        var ajaxUrl = pageUrl + '/DeleteMerchantDetail';

        var B = {};
        //alert('b empty');
        B.merchantForm = A.merchantForm;

        A.findAndRemove(B.merchantForm.MerchantCancel, 'CancelPolicyID', CancelPolicyID);
        //alert(JSON.stringify(B.enquiryForm));
        //alert('b added');
        var E = "{\'merchantForm\':\'" + JSON.stringify(B.merchantForm) + "\',\'CancelPolicyID\':\'" + CancelPolicyID + "\'}";
        //var C = JSON.stringify(E);
        //alert(C);
        $.ajax({
            type: "POST",
            url: ajaxUrl,
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                A.merchantForm = JSON.parse(D.d);
                //alert(JSON.stringify(A.merchantForm));
                var B = A.merchantForm;
                var C = B.MerchantCancel;
                A.showMerchantDetail(C);
//                window.enquiryPage.enquiryForm = JSON.parse(D.d);

//                //alert(JSON.stringify(window.enquiryPage.enquiryForm));
//                $("#divTable table[id='detailTable']").remove();
//                A.showEnquiryDetail(window.enquiryPage.enquiryForm.enquiryDetail);
//                $("#divTable table[id='prodTab']").hide();
//                $("#divTable table[id='prodTab']").show("slow");
            }
        });
        return false;
    };

    this.findAndRemove = function (array, property, value) {
        $.each(array, function (index, result) {
            //alert(" result[property]  "+result[property] +"  val  "+value + " index "+ index);
            if (result[property] == value) {
                //Remove from array
                array.splice(index, 1);
                return false;
            }
        });
    };
    
    this.sortJSON = function (data, key) {
        return data.sort(function (a, b) {
            var x = a[key]; var y = b[key];
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        });
    };
};