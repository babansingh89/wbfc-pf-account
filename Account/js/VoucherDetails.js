﻿
//var panval = '';
var hdTransID = '';
var hdGLID1 = '';
var hdSLID1 = '';
var hdnSUBTYPE = '';
var hdSLID = '';
var hdGLID2 = '';
var hdSLID2 = '';
$(document).ready(function () {
    Autocomplete_GL();
    Autocomplete_SL();
    SearchBank();
    BindGridView();

    $(".DefaultButton").click(function (event) {
        event.preventDefault();
    });

    $('#txtAmount').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $("#txtYearmonth").keypress(function (event) {
        if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            event.preventDefault();
        }
    });

    $("#txtVoucherno").keypress(function (event) {
        if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            event.preventDefault();
        }
    });

    $('#txtDatePay').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        showButtonPanel: true,
        duration: 'slow',
        dateformat: 'dd/mm/yyyy'
    });

    $("#txtGL").keyup(function () {
        if ($("#txtGL").val().trim() == "")
        {
            hdGLID1 = '';
            hdSLID1 = '';
            hdnSUBTYPE='';
        }
    });
    $("#txtSL").keyup(function () {
        if ($("#txtSL").val().trim() == "") {           
            hdSLID = '';
        }
    });
    $("#txtBank").keyup(function () {
        if ($("#txtGL").val().trim() == "") {
            hdGLID2 = '';
            hdSLID2 = '';
        }
    });
});

$(document).ready(function () {
    $('#ddlPayType').focus();
    $('#txtBank').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtAmount').focus();
            return false;
        }
    });

    $('#txtAmount').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtDraweeBank').focus();          
            return false;
        }
    });

    $('#txtDatePay').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtSL').focus();
            return false;
        }
    });

    $('#txtSL').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtPan').focus();
            return false;
        }
    });

    $('#txtPan').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtBank').focus();
            return false;
        }
    });

    $('#ddlPayType').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtGL').focus();
            return false;
        }
    });

    $('#txtGL').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtDatePay').focus();
            return false;
        }
    });

    $('#txtDraweeBank').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#ddlInstrumentType').focus();
            return false;
        }
    });

    $('#ddlInstrumentType').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtRemarks').focus();
            
            return false;
        }
    });

    $('#txtNo').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#cmdSearch').focus();
            return false;
        }
    });

    $('#txtRemarks').keypress(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13) {
            $('#txtNo').focus();           
            //$('#txtNo').focus();
            return false;
        }
    });

    $('#ddlInstrumentType').change(function () {
        if ($('#ddlInstrumentType').val() == 'C') {
            $('#InstNo').show();
        } else {
            $('#InstNo').hide();
        }
    });    
});

$(document).ready(function () {
    $('#btnSave').click(function (event) {
        if (hdGLID1 == '')
        { alert("Please select a GL !!"); $("#txtGL").focus(); return false; }

        if (hdnSUBTYPE == 'P') {
            if ($("#txtPan").val().trim() == '')
            { alert("Please enter a PAN number!!"); $("#txtPan").focus(); return false; }
        }      

        //if ($("#hdSLID").val() == '')
        //{ alert("Please select  a SL !!"); $("#txtSL").focus(); return false; }

        if (hdSLID2 == '')
        { alert("Please select  a Bank !!"); $("#txtBank").focus(); return false; }
        //if ($("#hdnBankID").val() == '')
        //{ alert("Please select a Bank !!"); $("#txtBank").focus(); return false; }
        if ($("#txtAmount").val().trim() == '')
        { alert("Enter A amount !!"); $("#txtAmount").focus(); return false; }

        if ($("#txtDatePay").val().trim() == '')
        { alert("Please select a Date !!"); $("#txtDatePay").focus(); return false; }

        if ($("#txtDraweeBank").val().trim() == '')
        { alert("Please Enter a Drawee Bank Name !!"); $("#txtDraweeBank").focus(); return false; }

        if ($("#ddlInstrumentType").val() == '')
        { alert("Please select a Instrument type !!"); $("#ddlInstrumentType").focus(); return false; }

        if ($('#ddlInstrumentType').val() == 'C') {
            if ($("#txtNo").val().trim() == '')
            { alert("Please enter The Instrument Number !!"); $("#txtNo").focus(); return false; }
        }

        if ($("#txtRemarks").val().trim() == '')
        { alert("Please write some remark !!"); $("#txtRemarks").focus(); return false; }

        var H = "{TRANS_ID:'" + hdTransID + "',RCV_PAY:'" + $("#ddlPayType").val() + "',GL_ID_1:'" + hdGLID1 + "',SL_ID_1:'" + hdSLID1 + "',SL_SUB_CODE:'" + hdSLID + "',VCH_DATE:'" + $("#txtDatePay").val().trim() + "',VCH_AMT:'" + $("#txtAmount").val().trim() + "',REMARKS:'" + $("#txtRemarks").val().trim() + "',PAN:'" + $("#txtPan").val().trim() + "',GL_ID_2:'" + hdGLID2 + "',SL_ID_2:'" + hdSLID2 + "',INST_TYPE:'" + $("#ddlInstrumentType").val() + "',INST_NO:'" + $("#txtNo").val().trim() + "',DRAWEE:'" + $("#txtDraweeBank").val().trim() + "'}";
        $(".loading-overlay").show();
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "VoucherDetails.aspx/SAVE_deatails",
            data: H,
            dataType: "json",
            success: function (data) {
                alert(data.d);
                $("#btnSave").val("Save");
                $("#txtPan").attr('disabled',false);

                hdTransID='';
                $("#ddlPayType").val('');
                $("#txtSL").val('');
                hdSLID='';
                $("#txtPan").val('');
                $("#txtGL").val('');
                hdGLID1='';
                hdSLID1='';
                $("#txtDatePay").val('');
                $("#txtBank").val('');
                hdGLID2='';
                hdSLID2='';
                $("#txtAmount").val('');
                $("#txtDraweeBank").val('');
                $("#ddlInstrumentType").val('');
                $("#txtRemarks").val('');
                $("#txtNo").val('');
                BindGridView();
                $(".loading-overlay").hide();
                $('#ddlPayType').focus();
            },
            error: function (result) {
                alert("Error");
                $(".loading-overlay").hide();
                return false;
            }
        });
        //panval = '';
        //PAN_duplicatecheck($("#txtPan").val().trim());      
        //if (panval == 'false')
        //{ return false; } else {
        //    //  alert(H);
        //    SAVEajax(H);
        //}
    });
});

//$(document).ready(function () {
//    $("#txtPan").keyup(function () {
//        if ($("#txtPan").val().trim().length == 10)
//        {
//            Validate_PAN($("#txtPan").val().trim());
//        }
//    });
//    $("#txtPan").blur(function () {        
//            Validate_PAN($("#txtPan").val().trim());        
//    });
//});

function Autocomplete_GL() {
    $("#txtGL").autocomplete({
        source: function (request, response) {
            var W = "{GL:'" + $('#txtGL').val().trim() + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "VoucherDetails.aspx/GET_glID",
                data: W,
                dataType: "json",
                success: function (data) {
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("|")[3],
                            val: item
                        };
                    }));
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        //   appendTo: "#autocompleteSchemeID",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            hdGLID1=arr[0];
            hdSLID1=arr[1];
            hdnSUBTYPE=arr[2];
            //$("#txtSchemeName").val(arr[2]);  
            if (hdnSUBTYPE == "P") {
                $("#txtSL").attr('disabled', true);
                $("#txtSL").val("");
                hdSLID='';
                $("#PanNo").show();
            } else {
                $("#txtSL").attr('disabled', false);
                $("#PanNo").hide();
            }
        }
    }).click(function () {
        $(this).data("autocomplete").search($(this).val());
    });
}

function Autocomplete_SL() {
    $("#txtSL").autocomplete({
        source: function (request, response) {
            if (hdnSUBTYPE == 'P')
            { return false;}
            var W = "{SL:'" + $('#txtSL').val().trim() + "',SUBTYPE:'" + hdnSUBTYPE + "'}";
            //    alert(W);
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "VoucherDetails.aspx/GET_slID",
                data: W,
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    //   alert(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split("|")[1],
                            val: item
                        };
                    }));
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        //   appendTo: "#autocompleteSchemeID",
        minLength: 0,
        select: function (e, i) {
            var arr = i.item.val.split("|");
            hdSLID=arr[0];
            //$("#txtSchemeName").val(arr[2]);            
        }
    }).click(function () {
        if (hdnSUBTYPE == "")
        {
            alert("Please select a GL...");
            return false;
        }
        $(this).data("autocomplete").search($(this).val());
    });
}

function SearchBank() {
    $("#txtBank").autocomplete({
        source: function (request, response) {
            var bank = "";
            if ($('#txtBank').val() != '') {
                bank = $('#txtBank').val();
            }
            //if ($('#txtDraweeBank').val() != '') {
            //    bank = $('#txtDraweeBank').val();
            //}
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "VoucherDetails.aspx/AutoComplete_Bank",
                data: "{'Bank':'" + bank + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    if (data.d.length > 0) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item,
                            }
                        }))
                    }
                    else {
                        alert('No data found.');
                        $('#txtBank').val('');
                    }

                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            var arr = i.item.val.split('|');
            hdGLID2 = arr[1];
            hdSLID2=arr[2];
        }
    });
}

function Validate_PAN(Obj) {                                           ///PAN Number 
    if (Obj == null) Obj = window.event.srcElement;
    //if (Obj == '') Obj = window.event.srcElement;
    if (Obj.value.trim() != "") {
    //if (Obj != "") {
        ObjVal = Obj.value;
        //ObjVal = Obj;
        var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
        var code = /([C,P,H,F,A,T,B,L,J,G])/;
        var code_chk = ObjVal.substring(3, 4);
        if (ObjVal.search(panPat) == -1) {
            alert("Invalid Pan No");
            //  Obj.focus();
            $("#txtPan").val("");
            return false;
        }
        if (code.test(code_chk) == false) {
            alert("Invaild PAN Card No.");
            $("#txtPan").val("");
            return false;
        }
        //panval = '';
        //    alert(panval);

        //PAN_duplicatecheck(Obj.value.trim());

        //PAN_duplicatecheck(Obj);
     //   alert(panval);
        //if (panval!='') {        
        //if (panval == 'false')
        //{ alert("This PAN number already exist. Please try another PAN number!!"); return false; }
    //}
    }
}

function PAN_duplicatecheck(pannumber)
{
    //alert("KL");
    //panval = '';
    var W = "{PAN:'" + pannumber + "'}";
       //alert(W);
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "VoucherDetails.aspx/Check_PAN",
        data: W,
        dataType: "json",
        success: function (data) {
            //alert(data.d);
            if (data.d == '1') {
                alert("This PAN number already exist. Please try another PAN number!!");
                //panval = 'false';                
                $("#txtPan").val('');
                //return panval;
            } else { panval = ''; }
            //return panval;
            //alert(panval);
        },
        error: function (result) {
            alert("Error");
        }
    });
}

function BindGridView()
{
    $(".loading-overlay").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "VoucherDetails.aspx/Bind_GRID",
        data: "{}",
        dataType: "json",
        success: function (data) {                        
            $("#gvDetails").empty(); 
            $("#gvDetails").append("<tr><th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Edit</th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Delete</th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Voucher Date</th>" +
                                    "<TH style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Voucher Amount </th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>PAN </th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Drawee Bank</th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Instrument Number </th>" +
                                    "<th style='background-color:dodgerblue;height:20px;font-size:medium;color:white;'>Remarks</th></tr>");
           for (var i = 0; i < data.d.length; i++) {
               $("#gvDetails").append("<tr><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'><div onclick='Edit(" + data.d[i].TRANS_ID + ")'><img src='images/edit.png' id='Edit" + data.d[i].TRANS_ID + "' style='cursor: pointer;' /></div></td>" +
                   "<td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'><div onclick='Delete(" + data.d[i].TRANS_ID + ")'><img src='images/delete.gif' id='Delete" + data.d[i].TRANS_ID + "' style='cursor: pointer;'/></div></td>" +
                                         "<td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                   data.d[i].VCH_DATE + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                 data.d[i].VCH_AMT + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                 data.d[i].PAN + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                 data.d[i].DRAWEE + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:center;'>" +
                 data.d[i].INST_NO + "</td><td style='background-color:honeydew;height:20px;font-size:medium;text-align:left;'>" +
                 data.d[i].REMARKS + "</td></tr>");
            }
            $(".loading-overlay").hide();
        },
        error: function (result) {
            alert("Error");
        }
    });
}

function Edit(TransID)
{
    hdTransID = TransID;
    $(".loading-overlay").show();
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "VoucherDetails.aspx/Edit_GRID",
        data: "{TransID:'" + TransID + "'}",
        dataType: "json",
        success: function (data) { 
            $("#ddlPayType").val(data.d[0].RCV_PAY);
            $("#txtSL").val(data.d[0].SL);
            hdSLID=data.d[0].SL_SUB_CODE;
            $("#txtPan").val(data.d[0].PAN);
            $("#txtGL").val(data.d[0].GL);
            hdGLID1=data.d[0].GL_ID_1;
           
            hdnSUBTYPE=data.d[0].SUBTYPE;

            hdSLID1=data.d[0].SL_ID_1;
            $("#txtDatePay").val(data.d[0].VCH_DATE);
            $("#txtBank").val(data.d[0].Bank);
            hdGLID2=data.d[0].GL_ID_2;
            hdSLID2=data.d[0].SL_ID_2;
            $("#txtAmount").val(data.d[0].VCH_AMT);
            $("#txtDraweeBank").val(data.d[0].DRAWEE);
            $("#ddlInstrumentType").val(data.d[0].INST_TYPE);

            $("#txtRemarks").val(data.d[0].REMARKS);
            $("#txtNo").val(data.d[0].INST_NO);
            
            $("#btnSave").val('Update');
            $("#txtPan").attr('disabled', true);
            $(".loading-overlay").hide();

        },
        error: function (result) {
            alert("Something Missing");
            $(".loading-overlay").hide();
            return false;
        }
    });
}

function Delete(TransID)
{
    var conf = confirm("Are you sure whant to delete this data??");
    if (conf == true) {
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "VoucherDetails.aspx/Delete_data",
            data: "{TransID:'" + TransID + "'}",
            dataType: "json",
            success: function (data) {
                alert(data.d);
                BindGridView();
            },
            error: function (result) {
                alert("Error");
            }
        });
    } else { return false;}
    //alert(conf);
}

// yearmonth, vchtype, vchno

$(document).ready(function () {
    $("#cmdShow").click(function () {
        
        if ($("#txtYearmonth").val().trim() == "") {
            alert("Please enter Year & month!!");
            $("#txtYearmonth").focus();
            return false;
        }

        if ($("#txtVoucherno").val().trim() == "") {
            alert("Please enter Voucher Number!!");
            $("#txtVoucherno").focus();
            return false;
        }

        var vchtype = 0;
        var jsondata = "{yearmonth:'" + $("#txtYearmonth").val().trim() + "', vchtype:'" + vchtype + "', vchno:'" + $("#txtVoucherno").val().trim() + "'}";
      //  alert(jsondata);
        Search(jsondata);
    })
})

  function Search(jsondata) {
      //  alert(jsondata);    
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "VoucherDetails.aspx/Search_data",
            data: jsondata,
            dataType: "json",
            success: function (xmldata) { // RCV_PAY, GL, ASV.GL_ID_1,ASV.SL_FROM,ASV.SL_ID_1,ASV.SUB_ID_1, SL ,ASV.SL_SUB_CODE , Bank , ASV.GL_ID_2,ASV.SL_ID_2,ASV.SUB_ID_2,ASV.INST_TYPE,ASV.POST_FLAG,ASV.VCH_NO
                var xml = $.parseXML(xmldata.d);
                var xmltable = $(xml);
                var resultdata = xmltable.find("Table");
              //  alert(resultdata.length);
                if (resultdata.length == 0)
                {
                    alert("No record found !!");
                    return false;
                }

              //  alert("ff");
                hdTransID=$(resultdata).find("TRANS_ID").text();
                $("#ddlPayType").val($(resultdata).find("RCV_PAY").text());
                $("#txtSL").val($(resultdata).find("SL").text());
                hdSLID=$(resultdata).find("SL_SUB_CODE").text();
                $("#txtPan").val($(resultdata).find("PAN").text());
                $("#txtGL").val($(resultdata).find("GL").text());
                hdGLID1=$(resultdata).find("GL_ID_1").text();

                hdnSUBTYPE=$(resultdata).find("SUBTYPE").text();

                hdSLID1=$(resultdata).find("SL_ID_1").text();
                $("#txtDatePay").val($(resultdata).find("VCH_DATE").text());
                $("#txtBank").val($(resultdata).find("Bank").text() );
                hdGLID2=$(resultdata).find("GL_ID_2").text();
                hdSLID2=$(resultdata).find("SL_ID_2").text();
                $("#txtAmount").val($(resultdata).find("VCH_AMT").text());
                $("#txtDraweeBank").val($(resultdata).find("DRAWEE").text());
                $("#ddlInstrumentType").val($(resultdata).find("INST_TYPE").text());

                //alert($("#ddlInstrumentType").val());

                $("#txtRemarks").val($(resultdata).find("REMARKS").text());
                $("#txtNo").val($(resultdata).find("INST_NO").text());

                $("#btnSave").val('Update');
                $("#txtPan").attr('disabled', true);

            },
            error: function (result) {
                alert("Error");
            }
        });
    }
    //=================================== NOT USE ===============================

    //$(document).ready(function () {   

    //    if ($('#hdnLoaneeID').val() != '') {
    //       // LoaneeSattlTypeHeealthCode($('#hdnLoaneeID').val());
    //    }
    //    SearchLoaneeCode();
    //    calculateGrandTotal();
    //   // SearchBank();
    //    GetSearchValue();
    //});

    function SearchLoaneeCode() {
        $(".autosuggestLoaneeCode").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "PaymentReceived.aspx/LoaneeAutoCompleteData",
                    data: "{'LoaneeCode':'" + document.getElementById('txtLoaneeSearch').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length > 0) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        }
                        else {
                            alert('No data found.');
                            $('#txtLoaneeSearch').val('');
                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            },
            select: function (e, i) {

                $("#hdnLoaneeID").val(i.item.val);
                LoaneeSattlTypeHeealthCode(i.item.val);
            }
        });
    }

    function LoaneeSattlTypeHeealthCode(LoaneeUNQID) {
        var getdate = $('#txtDatePay').val();
        var year = getdate.substring(6, 10);
        var month = parseInt(getdate.substring(3, 5)) - 1;  // pervious month

        if (month.toString().length == 1) {
            month = '0' + month.toString();
        }
        var yearmonth = year + month;

        //alert(yearmonth);

        var LoaneeName = '';
        var W = "{LoaneeUNQID:'" + LoaneeUNQID + "', YearMonth:'" + yearmonth + "'}";
        $.ajax({
            type: "POST",
            url: "PaymentReceived.aspx/GET_LoaneeSattlTypeHeealthCode",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    var LoaneeName = data.d[0].Loanee_Name;
                    $('#ddlRepaymentTerm').val('');
                    $('#txtHealthCode').val('');
                    $('#ddlRepaymentTerm').val(data.d[0].Loanee_SettlementType);
                    $('#txtHealthCode').val(data.d[0].Loanee_HealthCode);;
                }
            },
            error: function (result) {
                alert("Error Records Data...");
            }
        });
    }

    function GetSearchValue() {
        $(".autosuggestSearch").autocomplete({
            source: function (request, response) {
                var LoaneeID = "";
                var Yearmonth = "";
                if ($('#txtYearmonth').val() != '') {
                    Yearmonth = $('#txtYearmonth').val();
                }
                else {
                    alert('Please enter a correct Year Month(YYYYMM)');
                    $('#txtYearmonth').val('');
                    $('#txtYearmonth').focus();
                    $('#txtText').val('');
                    $("#hdnRcvID").val('');
                    return false;
                }
                if ($('#txtText').val() != '') {
                    LoaneeID = $('#txtText').val();
                }
            
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "PaymentReceived.aspx/SearchAutoCompleteData",
                    data: "{'LoaneeID':'" + LoaneeID + "','Yearmonth':'" + Yearmonth + "'}",
                    dataType: "json",
                    success: function (data) {
                        //response(data.d);
                        if (data.d.length > 0) {

                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        }
                        else {
                            alert('No data found.');
                            $('#txtText').val('');
                            $("#hdnRcvID").val('');
                        }
                    
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            },
            select: function (e, i) {
                $("#hdnRcvID").val(i.item.val);
            }
        });
    };

    function calculateGrandTotal() {

        //var rowTR = evt.closest('tr');
        //var OrgName = $(rowTR).find("td:eq(0)").text().trim();

        var Amount = 0.00;
        var amt = $('#txtAmount').val();
        $("#tbl input[name*='txtActAmount']").each(function (index) {
            //Check if number is not empty 
        
        
            if ($.trim($(this).val()) != "")
                //Check if number is a valid integer
                if (!isNaN($(this).val())) {               
                    Amount = parseFloat(Amount) + parseFloat($(this).val());
                }       
        });
        $("#tbl span[id*='lblTotalAmount']").text(Amount);    
    };








