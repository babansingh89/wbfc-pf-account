﻿
//=======================================Strat Search for AccGL============================================
$(document).ready(function () {

    $('#txtGlCodeSearch').keydown(function (evt) {                /// for Backspace
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode == 8) {
            //$('#txtLoaneeIDSearch').val('');
            $('#txtHdnGlCodeSearch').val('');
        }
    });
    $('#txtGlCodeSearch').keyup(function (evt) {                  /// fro Delete     
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode === 46) {
            $('#txtHdnGlCodeSearch').val('');
        }
    });
    $("#txtGlCodeSearch").change(function () {                   /// for all select
        if ($(this).select()) {
            //$('#txtHdnGlCodeSearch').val('');
            //alert('changed');
        }
    });


    SearchGL();
});
function SearchGL() {
    $(".autosuggestGL").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Acc_Gl.aspx/AccGlAutoCompleteData",
                data: "{'OLD_GL_ID':'" + document.getElementById('txtGlCodeSearch').value + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    response($.map(data.d, function (item) {
                        return {
                            label: item.split('|')[0],
                            val: item.split('|')[1]
                        }
                    }))
                },
                error: function (result) {
                    alert("Error");
                }
            });
        },
        select: function (e, i) {
            //$("#txtLoaneeIDSearch").val(i.item.val);
            $("#txtHdnGlCodeSearch").val(i.item.val);
        }
    });    
}

//$(document).ready(function () {
//    $('#txtGlCodeSearch').keypress(function (evt) {
//        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
//        if (iKeyCode == 13) {
            
//            BindGridGlCodeSearch();
//            return false;
           
//        }
//        else {
//            alert("Please presss 'ENTER' key for search..");
//            return false;
//        }

//    });
//});

//$(document).ready(function () {
//    $('#txtGlCodeSearch').keydown(function (evt) {
//        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
//        if (iKeyCode == 8) {
//            $('#txtGlCodeSearch').val('');
//            $('#txtHdnGlCodeSearch').val('');
//        }
//        if (iKeyCode == 46) {
//            if ($('#txtGlCodeSearch').val().length < 3) {
//                $('#txtGlCodeSearch').val('');
//                $('#txtGlCodeSearch').val('');
//            }
//        }
//    });
//});



function BindGlCodeByID(GlCode, GlID) {
    var StrGLCode = '';
    StrGLCode = GlCode;
    StrGLID = GlID;
    //   $(".loading-overlay").show();
    var W = "{StrGLCode:'" + StrGLCode + "', StrGLID:'" + StrGLID + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/GET_GlCodeByID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var strGlID = data.d[0].GL_ID;
                var strGl_Code = data.d[0].OLD_GL_ID;

                $('#txtGlCodeSearch').val('');
                $('#txtGlCodeSearch').val(strGl_Code);
                $('#txtHdnGlCodeSearch').val('');
                $('#txtHdnGlCodeSearch').val(strGlID);
                alert($('#txtHdnGlCodeSearch').val());
                // $(".loading-overlay").hide();
            }
            else {
                alert("No Records Data");
                $('#txtGlCodeSearch').val('');
                $('#txtHdnGlCodeSearch').val('');
                // $(".loading-overlay").hide();
                BindGridGlCodeSearch();
                $('#txtGlCodeSearch').focus();
            }
        },
        error: function (result) {
            alert("Error Records Data...");
            //   $(".loading-overlay").hide();

        }
    });
}

function BindGridGlCodeSearch() {
    //  $(".loading-overlay").show();
    var W = "{}";

    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/Search_GlCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            $("#grdSearchGlCode").empty();
            if (data.d.length > 0) {
                $("#grdSearchGlCode").append("<tr><th>GL Code</th><th>GL ID</th><th>GL_Name</th></tr>");

                for (var i = 0; i < data.d.length; i++) {
                    $("#grdSearchGlCode").append("<tr><td>" +
                    data.d[i].OLD_GL_ID + "</td> <td>" +
                    data.d[i].GL_ID + "</td> <td>" +
                    data.d[i].GL_NAME + "</td></tr>");
                }

                var th = $("[id*=grdSearchGlCode] th:contains('GL ID')");
                th.css("display", "none");
                $("[id*=grdSearchGlCode] tr").each(function () {
                    $(this).find("td").eq(th.index()).css("display", "none");
                });
                GlPopup();
                //===================== START Search is not Blank Record Display in GridView
                if ($('#txtSearchGlCode').val() != '') {
                    var rows;
                    var coldata;
                    $('#grdSearchGlCode').find('tr:gt(0)').hide();
                    var data = $('#txtSearchGlCode').val();
                    var len = data.length;
                    if (len > 0) {
                        $('#grdSearchGlCode').find('tbody tr').each(function () {
                            coldata = $(this).children().eq(0);
                            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                            if (temp === 0) {
                                $(this).show();
                            }
                        });
                    }
                    else {
                        $('#grdSearchGlCode').find('tr:gt(0)').show();

                    }
                }

                $(document).ready(function ()
                {
                    $("[id*=grdSearchGlCode] tbody tr").click("click", function ()
                    {
                        var strVal = $(this).children("td:eq(0)").text();
                        var valUnitName = $(this).children("td:eq(1)").text();
                        $("#txtSearchGlCode").val(strVal);
                        $("#txtGlCodeSearch").val(strVal);
                        $("#txtHdnGlCodeSearch").val(valUnitName);
                        $("#txtSearchGlCode").focus();
                    });
                });
                //===================== END Selected Gridview in Text Box

            }
            else {
                alert("No Records Data");
                //     $(".loading-overlay").hide();
                $('#txtGrpCode').focus();
            }
        },
        error: function (result) {
            alert("Error Records Data");
            //     $(".loading-overlay").hide();

        }
    });
}

function GlPopup() {
    $("#dialogGL").dialog({
        title: "GL Search",
        width: 550,

        buttons:
        {
            Ok: function () {

                var GridGroup = document.getElementById("grdSearchGlCode");
                var strVal = '';
                var strVal1 = '';
                if ($('#txtSearchGlCode').val().trim() == '') {
                    alert("Select a GL Code");
                    $('#txtSearchGlCode').val('');
                    $("#txtSearchGlCode").focus();
                    return false;
                }
                if ($('#txtSearchGlCode').val() != '') {
                    for (var row = 1; row < GridGroup.rows.length; row++) {
                        var GridGroup_Cell = GridGroup.rows[row].cells[0];
                        var valueGrdCell = GridGroup_Cell.textContent.toString();
                        var GridGroup_Cell1 = GridGroup.rows[row].cells[1];
                        var valueGrdCell1 = GridGroup_Cell1.textContent.toString();
                        if (valueGrdCell == $('#txtSearchGlCode').val()) {
                            strVal = valueGrdCell;
                            strVal1 = valueGrdCell1;
                            break;
                        }
                    }

                    if (strVal != $('#txtSearchGlCode').val()) {
                        alert("Invalid GL Code (Gl Code is not in List)");
                        $("#txtSearchGlCode").focus();
                        return false;
                    }
                    else {
                        $("#txtGlCodeSearch").val($("#txtSearchGlCode").val());
                        BindGlCodeByID($('#txtGlCodeSearch').val(), $('#txtHdnGlCodeSearch').val());
                        $("#txtGlCodeSearch").focus();
                        $("#dialogGL").dialog('close');
                        return false;
                    }
                }
            }
        },
        modal: true
    });
}

$(document).ready(function () {
    $("#txtSearchGlCode").keypress(function (e) {
        if (e.keyCode == 13) {
            var GridGroup = document.getElementById("grdSearchGlCode");
            var strVal = '';
            var strVal1 = '';
            if ($('#txtSearchGlCode').val().trim() == '') {
                alert("Select a GL Code");
                $('#txtSearchGlCode').val('');
                $("#txtSearchGlCode").focus();
                return false;
            }
            if ($('#txtSearchGlCode').val() != '') {
                for (var row = 1; row < GridGroup.rows.length; row++) {
                    var GridGroup_Cell = GridGroup.rows[row].cells[0];
                    var valueGrdCell = GridGroup_Cell.textContent.toString();
                    var GridGroup_Cell1 = GridGroup.rows[row].cells[1];
                    var valueGrdCell1 = GridGroup_Cell1.textContent.toString();
                    if (valueGrdCell == $('#txtSearchGlCode').val()) {
                        strVal = valueGrdCell;
                        strVal1 = valueGrdCell1;
                        break;
                    }
                }

                if (strVal != $('#txtSearchGlCode').val()) {
                    alert("Invalid GL Code (Gl Code is not in List)");
                    $("#txtSearchGlCode").focus();
                    return false;
                }
                else {
                    $("#txtGlCodeSearch").val($("#txtSearchGlCode").val());
                    BindGlCodeByID($('#txtGlCodeSearch').val(), $('#txtHdnGlCodeSearch').val());
                    $("#txtGlCodeSearch").focus();
                    $("#dialogGL").dialog('close');
                    return false;
                }
            }
        }
    });
});

$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtSearchGlCode').keyup(function () {
        $('#grdSearchGlCode').find('tr:gt(0)').hide();
        var data = $('#txtSearchGlCode').val();
        var len = data.length;
        if (len > 0) {
            $('#grdSearchGlCode').find('tbody tr').each(function () {
                coldata = $(this).children().eq(0);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        } else {
            $('#grdSearchGlCode').find('tr:gt(0)').show();
        }

    });
});

//=======================================End ==============================================================

$(document).ready(function () {
    $('#txtGrpCode').keypress(function (evt)
    {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 13)
        {
            if ($("#txtGrpCode").val().trim() != '')
            {
                if ($("#txtGrpName").val().trim() != '')
                {
                    BindGridGroupCode();
                    $('#txtSearchOldGroupID').val($('#txtGrpCode').val());
                }
                BindGroupName($('#txtGrpCode').val());
                $('#txtGrpCode').focus();
                
                return false;
            }
            else
            {
                $("#txtGrpCode").val('');
                $('#hdnGroupID').val('');
                $('#txtGrpName').val('');
                BindGridGroupCode();
                $('#txtSearchOldGroupID').focus();
                return false;
            }

            

        }
        //if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57))
        //{
        //    alert("Please Enter a Numeric Value");
        //    $("#txtGrpCode").focus();
        //    return false;
        //}

       

    });
});
//========== End Unit Code Keypress 

$(document).ready(function () {
    $('#txtGrpCode').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $('#txtGrpName').val('');
            $('#hdnGroupID').val('');
        }
        if (iKeyCode == 46) {
            if ($('#txtGrpCode').val().length < 3) {
                $('#txtGrpName').val('');
                $('#hdnGroupID').val('');
            }
        }
    });
});

// ====== [ GetUnit() Removed ]

//============= Start Display Group Name
function BindGroupName(GroupCode)
{
    var StrGroupCode = '';
    StrGroupCode = GroupCode;
 //   $(".loading-overlay").show();
    var W = "{StrGroupCode:'" + StrGroupCode + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/GET_GroupNameByGrpID",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data)
        {
            if (data.d.length > 0)
            {
                var strGroupID = data.d[0].GroupID;
                var strOldGroupID = data.d[0].OldGroupID;
                var strGroupName  = data.d[0].GroupName;
                $('#hdnGroupID').val('');
                $('#hdnGroupID').val(strGroupID);
                $('#txtGrpCode').val('');
                $('#txtGrpCode').val(strOldGroupID);
                $('#txtGrpName').val('');
                $('#txtGrpName').val(strGroupName);
                $('#txtGlCode').focus();
             // $(".loading-overlay").hide();
            }
            else
            {
                alert("No Records Data");
                $('#hdnGroupID').val('');
                $('#txtGrpCode').val('');
                $('#txtGrpName').val('');
               // $(".loading-overlay").hide();
                BindGridGroupCode();
                $('#txtGrpCode').focus();
            }
        },
        error: function (result)
        {
            alert("Error Records Data...");
         //   $(".loading-overlay").hide();

        }
    });
}
//============= End Display Unit Name

//==============Start Unit Popup

function BindGridGroupCode()
{
  //  $(".loading-overlay").show();
    var W = "{}";

    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/GET_GroupName",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data)
        {
            $("#grdGroup").empty();
            if (data.d.length > 0)
            {
                $("#grdGroup").append("<tr><th>Group ID</th><th>Group Name</th></tr>");
                
                for (var i = 0; i < data.d.length; i++)
                {
                    $("#grdGroup").append("<tr><td>" +
                    data.d[i].OldGroupID + "</td> <td>" +
                    data.d[i].GroupName + "</td></tr>");
                }
                GroupPopup();
                //===================== START Search is not Blank Record Display in GridView
                if ($('#txtSearchOldGroupID').val() != '')
                {
                    var rows;
                    var coldata;
                    $('#grdGroup').find('tr:gt(0)').hide();
                    var data = $('#txtSearchOldGroupID').val();
                    var len = data.length;
                    if (len > 0) {
                        $('#grdGroup').find('tbody tr').each(function ()
                        {
                            coldata = $(this).children().eq(0);
                            var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                            if (temp === 0)
                            {
                                $(this).show();
                            }
                        });
                    }
                    else
                    {
                        $('#grdGroup').find('tr:gt(0)').show();

                    }
                }
                //===================== END Search is not Blank Record Display in GridView

             //   $(".loading-overlay").hide();
                ////////////////$(function () {
                ////////////////    $("[id*=grdGroup] td").bind("click", function () {
                ////////////////        var row = $(this).parent();
                ////////////////        $("[id*=grdGroup] tr").each(function (rowindex) {
                ////////////////            if ($(this)[0] != row[0]) {
                ////////////////                $("td", this).removeClass("selected_row");
                ////////////////            }
                ////////////////        });

                ////////////////        $("td", row).each(function () {
                ////////////////            if (!$(this).hasClass("selected_row")) {
                ////////////////                $(this).addClass("selected_row");
                ////////////////            } else {
                ////////////////                $(this).removeClass("selected_row");
                ////////////////            }
                ////////////////        });
                ////////////////    });
                ////////////////});

                //===================== START Selected Gridview in Text Box
                $(document).ready(function ()
                {
                    $("[id*=grdGroup] tbody tr").click("click", function ()
                    {
                        var strVal = $(this).children("td:eq(0)").text();
                        var valUnitName = $(this).children("td:eq(1)").text();
                        $("#txtSearchOldGroupID").val(strVal);
                        $("#txtGrpName").val(valUnitName);
                        $("#txtSearchOldGroupID").focus();
                    });
                });
                //===================== END Selected Gridview in Text Box

            }
            else
            {
                alert("No Records Data");
           //     $(".loading-overlay").hide();
                $('#txtGrpCode').focus();
            }
        },
        error: function (result)
        {
            alert("Error Records Data");
       //     $(".loading-overlay").hide();

        }
    });
}
///======================= START UNIT keypress
$(document).ready(function ()
{
    $("#txtSearchOldGroupID").keypress(function (e)
    {
        if (e.keyCode == 13)
        {
            var GridGroup = document.getElementById("grdGroup");
            var strVal = '';
            if ($('#txtSearchOldGroupID').val().trim() == '')
            {
                alert("Select a Group");
                $('#txtSearchOldGroupID').val('');
                $("#txtSearchOldGroupID").focus();
                return false;
            }
            if ($('#txtSearchOldGroupID').val() != '')
            {
                for (var row = 1; row < GridGroup.rows.length; row++)
                {
                    var GridGroup_Cell = GridGroup.rows[row].cells[0];
                    var strGrdVal = GridGroup_Cell.textContent.toString();
                    if (strGrdVal == $('#txtSearchOldGroupID').val())
                    {
                        strVal = strGrdVal;
                        break;
                    }
                }

                if (strVal != $('#txtSearchOldGroupID').val())
                {
                    alert("Invalid Group ID (Group ID is not in List)");
                    $("#txtSearchOldGroupID").focus();
                    return false;
                }

                else
                {
                    $("#txtGrpCode").val($("#txtSearchOldGroupID").val());
                    BindGroupName($('#txtGrpCode').val());
                    $("#txtGrpCode").focus();
                    $("#dialogGroup").dialog('close');

                    return false;
                }
            }
        }
    });
});

//==============End  Unit Popup

function GroupPopup()
{
    $("#dialogGroup").dialog({
        title: "Group Search",
        width: 550,

        buttons:
        {
            Ok: function ()
            {

                var GridGroup = document.getElementById("grdGroup");
                var strVal = '';
                if ($('#txtSearchOldGroupID').val().trim() == '')
                {
                    alert("Select a Group");
                    $('#txtSearchOldGroupID').val('');
                    $("#txtSearchOldGroupID").focus();
                    return false;
                }
                if ($('#txtSearchOldGroupID').val() != '')
                {
                    for (var row = 1; row < GridGroup.rows.length; row++)
                    {
                        var GridGroup_Cell = GridGroup.rows[row].cells[0];
                        var valueGrdCell = GridGroup_Cell.textContent.toString();
                        if (valueGrdCell == $('#txtSearchOldGroupID').val())
                        {
                            strVal = valueGrdCell;
                            break;
                        }
                    }

                    if (strVal != $('#txtSearchOldGroupID').val())
                    {
                        alert("Invalid Group ID (Group ID is not in List)");
                        $("#txtSearchOldGroupID").focus();
                        return false;
                    }
                    else
                    {
                        $("#txtGrpCode").val($("#txtSearchOldGroupID").val());
                        BindGroupName($('#txtGrpCode').val());
                        $("#txtGrpCode").focus();
                        $("#dialogGroup").dialog('close');
                        return false;
                    }
                }
            }
        },
        modal: true
    });
}

$(document).ready(function () {
    var rows;
    var coldata;
    $('#txtSearchOldGroupID').keyup(function ()
    {
        $('#grdGroup').find('tr:gt(0)').hide();
        var data = $('#txtSearchOldGroupID').val();
        var len = data.length;
        if (len > 0) {
            $('#grdGroup').find('tbody tr').each(function () {
                coldata = $(this).children().eq(0);
                var temp = coldata.text().toUpperCase().indexOf(data.toUpperCase());
                if (temp === 0) {
                    $(this).show();
                }
            });
        } else {
            $('#grdGroup').find('tr:gt(0)').show();
        }

    });
});


$(document).ready(function () {

    //$('#txtGlCode').keyup(function (evt) {
    //    //   $(".loading-overlay").show();
    //    if ($('#txtGrpCode').val().trim() == '')
    //    {
    //        alert('Please First Select Group Code');
    //        $('#txtGrpCode').focus();
    //        $('#txtGlCode').val('');
    //        return false;
    //    }
    //    var StrGLCode = '';
    //    StrGLCode = $('#txtGlCode').val().trim();
    //    //  alert(StrGLCode);
    //    var W = "{StrGLCode:'" + StrGLCode + "'}";
    //    $.ajax({
    //        type: "POST",
    //        url: "Acc_Gl.aspx/Chk_GLCode",
    //        contentType: "application/json;charset=utf-8",
    //        data: W,
    //        dataType: "json",
    //        success: function (data) {
    //            if (data.d.length > 0) {s
    //                var strGl_Code = data.d[0].OLD_GL_ID;

    //                if (strGl_Code != '') {
    //                    $('#txtGlCode').val('');
    //                    alert("This Gl Code already Exist...");
    //                    return false;
    //                }

    //            }

    //        },
    //        error: function (result) {
    //            alert("Error Records Data...");
    //            //   $(".loading-overlay").hide();

    //        }
    //    });
    //});
    


    //$('#txtGlCode').change(function (evt) {     
    //    //   $(".loading-overlay").show();
    //    //if ($('#txtGrpCode').val().trim() == '') {
    //    //    alert('Please First Select Group Code');
    //    //    $('#txtGrpCode').focus();
    //    //    $('#txtGlCode').val('');
    //    //    return false;
    //    //}

    //    var StrGLCode = '';
    //    StrGLCode = $('#txtGlCode').val().trim();
    //  //  alert(StrGLCode);
    //    var W = "{StrGLCode:'" + StrGLCode + "'}";
    //    $.ajax({
    //        type: "POST",
    //        url: "Acc_Gl.aspx/Chk_GLCode",
    //        contentType: "application/json;charset=utf-8",
    //        data: W,
    //        dataType: "json",
    //        success: function (data) {
    //            if (data.d.length > 0) {
    //                var strGl_Code = data.d[0].OLD_GL_ID;
                    
    //                if (strGl_Code != '') {
    //                    $('#txtGlCode').val('');
    //                    alert("This Gl Code already Exist...");
    //                    return false;
    //                }
                    
    //            }
                
    //        },
    //        error: function (result) {
    //            alert("Error Records Data...");
    //            //   $(".loading-overlay").hide();

    //        }
    //    });
    //});
});

///====================== Set Test box Length ==================
$(document).ready(function () {
    $('#txtGlCode').attr({ maxLength: 10 });
    $('#txtGlName').attr({ maxLength: 50 });
    $('#txtSchedule').attr({ maxLength: 10 });
    $('#txtYearMonth').attr({ maxLength: 6 });

    $(".DefaultButton").click(function (event) {
        //alert('prevent');
        event.preventDefault();
    });
    var d = new Date();
    var month = (d.getMonth()+1).toString();
    var year = (d.getFullYear()).toString();
    var yearmon = '';
    if (month.length > 1) {
        yearmon = year + month;
    }
    else {        
        yearmon = year + '0' + month;
    }
   // alert(yearmon);
    $('#txtYearMonth').val($('#hdnMaxYearMonth').val());

});

//$(document).ready(function ()
//{
//    $('#txtYearMonth').datepicker({
//        showOtherMonths: true,
//        selectOtherMonths: true,
//        closeText: 'X',
//        showAnim: 'drop',
//        showButtonPanel: true,
//        duration: 'slow',
//        dateformat: 'dd/mm/yyyy'
        

//    });
//});

$(document).ready(function ()
{
    var flag1 = 0;
    var flag2 = 0;
    var flag3 = 0;
    var flag4 = 0;
    $("#txtYearMonth").keypress(function (evt) {

        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
            alert("Please Enter a Numeric Value");
            return false;
        }

        if (evt.keyCode == 13) {
            $("#txtOpenDebit").focus();
            return false;
        }
    });

    $("#txtOpenDebit").keypress(function (evt)
    {  
        if (flag1 == 0)
        {
            //if ($("#txtOpenDebit").val > 0) {
            //    if (evt.charCode >= 48 && (evt.charCode <= 57 || evt.charCode >= 57)) {
            //        return false;
            //    }

            //}
            //else {
                //if (evt.charCode >= 49 && (evt.charCode <= 57 || evt.charCode >= 57)) {
                //    return false;
                //}
            //}
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else
        {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) ) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag1 = 1;
        }

        
        
        if (evt.keyCode == 13)
        {
            if ($("#txtOpenDebit").val() != '0' || $("#txtOpenDebit").val() == '0') {
                //$("#txtOpenCredit").attr('disabled', 'disabled');
                $("#txtOpenCredit").val('0');
                $("#txtTotalDebit").val('0');
                $("#txtTotalCredit").val('0');
                $("#txtCloseDebit").val('0');
                $("#txtCloseCredit").val('0');
                $("#txtOpenCredit").focus();
            }
            else {
                $("#txtOpenCredit").focus();
            }
            

        //    $("#txtOpenCredit").focus();
            return false;
        }
    });

    $("#txtOpenCredit").keypress(function (evt)
    {
        if (flag2 == 0) {
            //if ($("#txtOpenCredit").val > 0) {
            //    if (evt.charCode >= 48 && (evt.charCode <= 57 || evt.charCode >= 57)) {
            //        return false;
            //    }

            //}
            //else {
                //if (evt.charCode >= 49 && (evt.charCode <= 57 || evt.charCode >= 57)) {
                //    return false;
                //}
            //}
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag2 = 1;
        }

        if (evt.keyCode == 13) {
            $("#txtTotalDebit").focus();
            return false;
        }
    });

    $("#txtTotalDebit").keypress(function (evt)
    {
        if (flag3 == 0) {
            
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag3 = 1;
        }
        if (evt.keyCode == 13) {
            $("#txtTotalCredit").focus();
            return false;
        }
    });

    $("#txtTotalCredit").keypress(function (evt)
    {
        if (flag4 == 0) {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        else {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
                alert("Please Enter a Numeric Value");
                return false;
            }
        }
        if (evt.charCode == 46) {
            flag4 = 1;
        }
        if (evt.keyCode == 13) {
            $("#txtCloseDebit").focus();
            return false;
        }
    });

    $("#txtCloseDebit").keypress(function (e) {
        if (e.keyCode == 13) {
            $("#txtCloseCredit").focus();
            return false;
        }
    });
});

$(document).ready(function ()
{
    var OpenBal;
    var CloseBal;
    var OpenDebit;
    var ClaoseDebit;

    $('#txtOpenCredit').keyup(function ()
    {
        if ($('#txtOpenDebit').val() != '' && $('#txtOpenCredit').val() != '') {
            OpenBal = parseFloat($('#txtOpenDebit').val()) - parseFloat($('#txtOpenCredit').val());
        }
        else
        {
            OpenBal = 0;
        }

  //      calculation();
        //alert(OpenBal);
        return false;

    });
    $('#txtOpenDebit').keyup(function ()
    {
        if ($('#txtOpenDebit').val() != '' && $('#txtOpenCredit').val() != '')
        {
            OpenBal = parseFloat($('#txtOpenDebit').val()) - parseFloat($('#txtOpenCredit').val());
        }
        else {
            OpenBal = 0;
        }
   //     calculation();
       // alert(OpenBal);
        return false;

    });

    $('#txtTotalDebit').keyup(function ()
    {
        calculation();

        return false;

    });

    $('#txtTotalCredit').keyup(function () {
        
    //    calculation();
        return false;

    });
});

function calculation() {
    if ($('#txtOpenDebit').val() != '' && $('#txtOpenCredit').val() != '' && $('#txtTotalDebit').val() != '' && $('#txtTotalCredit').val() != '') {
        CloseBal = (parseFloat($('#txtOpenDebit').val()) - parseFloat($('#txtOpenCredit').val())) + (parseFloat($('#txtTotalDebit').val()) - parseFloat($('#txtTotalCredit').val()))
    }
    else {
        CloseBal = 0;
    }

    if (CloseBal < 0) {
        $('#txtCloseCredit').val(CloseBal);
        $('#txtCloseDebit').val('0');
    }
    else {
        $('#txtCloseDebit').val(CloseBal);
        $('#txtCloseCredit').val('0');
    }
}


$(document).ready(function () {
    $("input").focus(function () {
        $(this).css("background-color", "#cccccc");
    });
    $("input").blur(function () {
        $(this).css("background-color", "#ffffff");
    });

    $("select").focus(function () {
        $(this).css("background-color", "#cccccc");
    });

    $("select").blur(function () {
        $(this).css("background-color", "#ffffff");
    });

});

$(document).ready(function () {
    $('#txtGlCode').keypress(function (evt) {                  /// fro Delete     
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode === 13) {
            $('#txtGlName').focus();
        }
    });

    $("#txtGlCode").blur(function () {
        ChekDuplicateGLCode();
    });
});


function ChekDuplicateGLCode() {
    //   $(".loading-overlay").show();
    //alert($('#hdnGlID').val());
    var GLId = '';
    GLId = $('#hdnGlID').val();
    if (GLId == '')
    {
        GLId = '0';

    }
    if ($('#txtGrpCode').val().trim() == '') {
        alert('Please First Select Group Code');
        $('#txtGrpCode').focus();
        $('#txtGlCode').val('');
        return false;
    }
    var StrGLCode = '';
    StrGLCode = $('#txtGlCode').val().trim();
    //  alert(StrGLCode);
    var W = "{StrGLCode:'" + StrGLCode + "',StrGLID:'" + GLId + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/Chk_GLCode",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var strGl_Code = data.d[0].OLD_GL_ID;

                if (strGl_Code != '') {
                    $('#txtGlCode').val('');
                    alert("This Gl Code already Exist...");
                    $('#txtGlCode').focus();
                    return false;
                }

            }     

        },
        error: function (data) {
            alert("Error Records Data SK...");
            //   $(".loading-overlay").hide();

        }
    });
}


$(document).ready(function () {
    $("#txtGlName").blur(function () {
        //ChekDuplicateGLName(); // This Code Hide from Client Mr. Montu And Ashish wbfc Date - 06/02/2017
    });
});


function ChekDuplicateGLName() {
    if ($('#txtGrpCode').val().trim() == '') {
        alert('Please First Select Group Code');
        $('#txtGrpCode').focus();
        $('#txtGrpCode').val('');
        return false;
    }

    if ($('#txtGlCode').val().trim() == '') {
        //alert('Please First Type GL Code');
        $('#txtGlCode').focus();
        //$('#txtGlCode').val('');
        return false;
    }

    if ($('#txtGlName').val().trim() == '') {
        //alert('Please Type GL Name ');
        $('#txtGlName').val('');
        $('#txtGlName').focus();
        return false;
    }
    var GLId = '';
    GLId = $('#hdnGlID').val();
    if (GLId == '') {
        GLId = '0';

    }
    var StrGLName = '';
    StrGLName = $('#txtGlName').val().trim();
    var W = "{StrGLName:'" + StrGLName + "',StrGLID:'" + GLId + "'}";
    $.ajax({
        type: "POST",
        url: "Acc_Gl.aspx/Chk_GLName",
        contentType: "application/json;charset=utf-8",
        data: W,
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                var strGl_Name = data.d[0].GlName;

                if (strGl_Name != '') {
                    $('#txtGlName').val('');
                    alert("This Gl Name already Exist...");
                    $('#txtGlName').focus();
                    return false;
                }

            }

        },
        error: function (result) {
            alert("Error Records Data...");

        }
    });
}















    

    

