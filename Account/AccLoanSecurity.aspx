﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="AccLoanSecurity.aspx.cs" Inherits="AccLoanSecurity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/LoanSecurity.js?v=2"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtLoaneeSearch").focus();
        });
        
        function beforeSave() {   
            $("#frmEcom").validate();    

            $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please select a Unit" } });
            $("#txtLoaneeSearch").rules("add", { required: true, messages: { required: "Please select a Loanee Code" } });
            $("#txtSecurityType").rules("add", { required: true, messages: { required: "Please enter Instrument Type" } });
            $("#txtSecurityNumber").rules("add", { required: true, messages: { required: "Please enter Instrument Number" } });
            $("#txtSecurityDate").rules("add", { required: true, messages: { required: "Please enter Instrument Date" } });

        }       
        function beforeSave1() {
            $("#frmEcom").validate();

            $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please select a Unit" } });
            $("#txtLoaneeSearch").rules("add", { required: true, messages: { required: "Please select a Loanee Code" } });
        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
    </script>
    <style type="text/css">
        .floatLeft { 
            float: left;
        }

        .f_Right {
            float: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Loan Security</td>

            </tr>
        </table>
       <%-- <br />--%>
        <table width="98%" cellpadding="0" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="0" AutoGenerateRows="False"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td class="labelCaption">Loanee Code &nbsp;&nbsp;<span class="require">*</span>  </td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption" colspan="6" >
                                        <asp:TextBox ID="txtLoaneeSearch" autocomplete="off" PlaceHolder="---(Select Loanee Code)---" Width="565px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestLoaneeCode"></asp:TextBox>
                                    </td>
                                    <td>
                                       <asp:Button ID="cmdSearch" autocomplete="off" runat="server" Text="Search" CommandName="Loanee Search" OnClick="cmdSearch_Click"
                                            Width="120px" Height="35px" CssClass="save-button " OnClientClick='javascript: return beforeSave1()'/>  <%-- DefaultButton--%>
                                    </td>
                                    <td style="text-align:right;"></td>
                                    
                                </tr>
                                <tr>
                                    <td class="labelCaption">Instrument Type &nbsp;&nbsp;<span class="require">*</span></td> 
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtSecurityType" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Instrument Number &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtSecurityNumber" autocomplete="off" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Instrument Date &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtSecurityDate" autocomplete="off" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgSecurityDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Bank Name</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtBankName" autocomplete="off" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Branch Name</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtBranchName" autocomplete="off" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Face Value</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtFaceValue" autocomplete="off" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Maturity Value</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtMaturityValue" autocomplete="off" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Maturity Date</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtMaturityDate" autocomplete="off" Width="175" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgMaturityDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                    <td class="labelCaption">Pledge Date</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtPledgeDate"  autocomplete="off" Width="175" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgPledgeDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Holder's Name</td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="7" class="labelCaption">
                                        <asp:TextBox ID="txtHolderName" autocomplete="off" Width="565" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="labelCaption">Release Date</td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="7" class="labelCaption">
                                        <asp:TextBox ID="txtReleaseDate" autocomplete="off" Width="150" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgReleaseDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>

                                </tr>
                                <tr>
                                    <td class="labelCaption">Folder and  Cabinet Name,
                                        <br />
                                        Register & Page No.</td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="7" class="labelCaption">
                                        <asp:TextBox ID="txtFolderName" autocomplete="off" Width="565" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Remarks</td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="7" class="labelCaption">
                                        <asp:TextBox ID="txtRemarks" autocomplete="off" Width="565" Height="40px" TextMode="MultiLine" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>

                            </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                
                                <tr>
                                    <td class="labelCaption">Loanee Code &nbsp;&nbsp;<span class="require">*</span>  </td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption" colspan="7">
                                        <asp:TextBox ID="txtLoaneeSearch" autocomplete="off" PlaceHolder="---(Select Loanee Code)---" Width="555px" Text='<%# Eval("LOANEE_NAME") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestLoaneeCode" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Security Type</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtSecurityType" autocomplete="off" Width="200" Text='<%# Eval("S_TYPE") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Security Number</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtSecurityNumber" autocomplete="off" Width="200" Text='<%# Eval("S_NO") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Security Date</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtSecurityDate" autocomplete="off" Width="150" Text='<%# Eval("S_DATE") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgSecurityDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Bank Name</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtBankName" autocomplete="off" Width="200" Text='<%# Eval("BANKNAME") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Branch Name</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtBranchName" autocomplete="off" Width="200" Text='<%# Eval("BRANCHNAME") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Face Value</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtFaceValue" autocomplete="off" Width="200" Text='<%# Eval("FACEVALUE", "{0:0.00}") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Maturity Value</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtMaturityValue" autocomplete="off" Width="200" Text='<%# Eval("MATVALUE", "{0:0.00}") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Maturity Date</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtMaturityDate" autocomplete="off" Width="175" Text='<%# Eval("MATDATE") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgMaturityDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                    <td class="labelCaption">Pledge Date</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtPledgeDate" autocomplete="off" Width="175" Text='<%# Eval("PLEDGEDATE") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgPledgeDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Holder's Name</td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="7" class="labelCaption">
                                        <asp:TextBox ID="txtHolderName" autocomplete="off" Width="550" Text='<%# Eval("HOLDERNAME") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="labelCaption">Release Date</td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="7" class="labelCaption">
                                        <asp:TextBox ID="txtReleaseDate" autocomplete="off" Width="150" Text='<%# Eval("RELEASEDATE") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgReleaseDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>

                                </tr>
                                <tr>
                                    <td class="labelCaption">Folder and  Cabinet Name,
                                        <br />
                                        Register & Page No.</td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="7" class="labelCaption">
                                        <asp:TextBox ID="txtFolderName" autocomplete="off" Width="550" Text='<%# Eval("FOLDER_NO") %>' ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Remarks</td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="7" class="labelCaption">
                                        <asp:TextBox ID="txtRemarks" autocomplete="off" Width="550" Text='<%# Eval("REMARKS") %>' Height="40px" TextMode="MultiLine" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>

                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" autocomplete="off" runat="server" Text="Create" CommandName="Add" Height="30px"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdCancel" autocomplete="off" runat="server" Text="Cancel" Height="30px"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both" OnPageIndexChanging="OnPageIndexChanging" PageSize="30" Font-Bold="true" Font-Size="13px"
                            AutoGenerateColumns="false" DataKeyNames="LoaneeSecurityID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="true">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName='Select' autocomplete="off" ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("LoaneeSecurityID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" autocomplete="off"
                                            OnClientClick='<%#"return Delete("+(Eval("LoaneeSecurityID")).ToString()+") " %>'
                                            CommandArgument='<%# Eval("LoaneeSecurityID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="LOANEE_CODE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Loanee Code" />
                                <asp:BoundField DataField="S_TYPE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Instrument Type" />
                                <asp:BoundField DataField="S_NO" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Instrument No" />
                                <asp:BoundField DataField="S_DATE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Instrument Date" />
                                <asp:BoundField DataField="BANKNAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Bank Branch Name" />
                                <asp:BoundField DataField="FACEVALUE" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" DataFormatString="{0:0.00}" HeaderText="Face Value" />
                                <asp:BoundField DataField="MATVALUE" ItemStyle-CssClass="labelCaptionright" HeaderStyle-CssClass="TableHeader" DataFormatString="{0:0.00}" HeaderText="Maturity value" />
                                <asp:BoundField DataField="MATDATE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Maturity Date" />
                                <asp:BoundField DataField="PLEDGEDATE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Pledge Date" />
                            </Columns>
                        </asp:GridView>

                        
                                    
                       
                    </div>
                </td>
                
            </tr>
            
        </table>
        <table>
            <tr>
                 <td class="labelCaption" style="font-weight:bold">TOTAL </td>
                <td class="labelCaption">:</td>
                <td colspan="0" class="labelCaption">
                    <asp:TextBox ID="TxtTotalRec" autocomplete="off" Width="200" Font-Size="14px" ForeColor="Green" Font-Bold="true" BorderStyle="None" ReadOnly="true"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>
                <td style="text-align:right;width:100%"></td>
        </tr>
    </table>
    </div>
</asp:Content>
