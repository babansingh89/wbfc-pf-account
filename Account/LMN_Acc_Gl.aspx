﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_Acc_Gl.aspx.cs" Inherits="LMN_Acc_Gl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/Acc_Gl.js"></script>
    <script type="text/javascript" src="js/Acc_Gl_Master.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function beforeSave() {
            $("#frmEcom").validate();

            //  $("#txtGrpCode").rules("add", { required: true, messages: { required: "Please enter Group Type" } });
            //$("#txtStateCode").rules("add", { required: true, messages: { required: "Please enter State Code"} });

            // $("#txtYearMonth").rules("add", { required: true, messages: { required: "Please enter Year Month" } });

        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Account GL</td>
            </tr>
        </table>
        <br />

        <%-- ================ Group Code POP UP GRIDVIEW =====================  --%>
        <div id="dialogGroup" style="display: none">
            <asp:TextBox ID="txtSearchOldGroupID" autocomplete="off" runat="server" Width="516px" CssClass="TextSearchVendor" MaxLength="150"></asp:TextBox>

            <asp:GridView ID="grdGroup" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc; width: 520px;" AutoGenerateColumns="false" CssClass="GridVendor" PageSize="5" AllowPaging="true">
                <Columns>
                    <asp:BoundField DataField="OldGroupID" HeaderText="Group ID" ItemStyle-Width="150" />
                    <asp:BoundField DataField="GroupName" HeaderText="Group Name" ItemStyle-Width="150" />
                </Columns>
            </asp:GridView>

        </div>
        <%-- ================ END   Group Code POP UP GRIDVIEW =====================  --%>

        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False"
                         DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">

                                <tr>
                                    <td colspan="6">
                                        <table class="headingCaptionHead" width="98%" align="center">
                                            <tr>
                                                <td style="height:15px;" align="left">Account GL Search</td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td class="labelCaption">Gl Code  </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                    <asp:TextBox ID="txtGlCodeSearch" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6">
                                        <table class="headingCaptionHead" width="98%" align="center">
                                            <tr>
                                                <td align="left">Account GL Entry</td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                </tr>

                                <tr>

                                    <td class="labelCaption">Group Code &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGrpCode" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <%--<asp:TextBox ID="txtGrpName" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:TextBox ID="TextBox2" width ="30" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>--%>
                                    </td>

                                    <td class="labelCaption">Group Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGrpName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2" Enabled="false"></asp:TextBox>
                                        <asp:TextBox ID="hdnGroupID" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2" Visible="true"></asp:TextBox>
                                        <asp:TextBox ID="hdnGlID" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2" Visible="true"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">Gl Code &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGlCode" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">GL Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGlName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">Gl Type&nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGlType" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="Normal" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="Deposit" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="Branch" Value="B"></asp:ListItem>
                                            <asp:ListItem Text="Fixed Assets" Value="F"></asp:ListItem>
                                            <asp:ListItem Text="Loan" Value="L"></asp:ListItem>
                                            <asp:ListItem Text="Employee" Value="E"></asp:ListItem>
                                            <asp:ListItem Text="Re-Finance" Value="R"></asp:ListItem>
                                            <asp:ListItem Text="Bank" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Cash" Value="C"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkUnitOnly" autocomplete="off" class="headFont" runat="server" Font-Size="small" Text="For Unit Only" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">GL Master &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td style="padding: 5px;">
                                        <asp:RadioButton ID="rdNormal" autocomplete="off" Checked="true" class="inputbox2" runat="server" Font-Size="small" GroupName="rd" Text="Normal" />
                                        <asp:RadioButton ID="rdMaster" autocomplete="off" class="inputbox2" runat="server" Font-Size="small" GroupName="rd" Text="Master" />
                                    </td>

                                    <td class="labelCaption">Schedule &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSchedule" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>



                                </tr>

                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6">
                                        <table class="headingCaptionHead" width="98%" align="center">
                                            <tr>
                                                <td align="left">Account GL Details</td>
                                            </tr>
                                        </table>
                                    </td>
                                    
                                </tr>

                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <table align="center" cellpadding="5" width="100%">
                                            <tr>
                                                <th>Year/Month(YYYYMM)</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;Opening Debit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;Opening Credit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Debit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Credit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;Closing Debit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;Closing Credit</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtYearMonth" autocomplete="off" Enabled="true" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtOpenDebit" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtOpenCredit" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalDebit" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalCredit" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtCloseDebit" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" Enabled="false" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtCloseCredit" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" Enabled="false" CssClass="inputbox2"></asp:TextBox></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6">
                                        <div style="border: 1px solid white; width: 100%; font-size: small">
                                            <asp:GridView ID="grdAccGLDtl" autocomplete="off" ClientIDMode="Static" DataKeyNames="ItemID"
                                                clientID="grdAccGLDtl" runat="server" AutoGenerateColumns="False" CssClass="Grid">
                                                <AlternatingRowStyle BackColor="Honeydew" />

                                                <Columns>
                                                    <asp:BoundField DataField="ItemID" HeaderText="Edit" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="ItemID" HeaderText="Cancel" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="ItemID" HeaderText="Delete" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="YearMonth" HeaderText="Year/Month(YYYYMM)" ItemStyle-Width="10%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="OpeningDebit" HeaderText="Opening Debit" ItemStyle-Width="10%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="OpeningCredit" HeaderText="Opening Credit" ItemStyle-Width="10%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="TotalDebit" HeaderText="Total Debit" ItemStyle-Width="10%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="TotalCredit" HeaderText="Total Credit" ItemStyle-Width="10%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="ClosingDebit" HeaderText="Closing Debit" ItemStyle-Width="10%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="ClosingCredit" HeaderText="Closing Credit" ItemStyle-Width="10%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>

                            </table>

                        </InsertItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton"  />
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <%--<asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="LoanTypeID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("LoanTypeID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete"
                                            OnClientClick='<%#"return Delete("+(Eval("LoanTypeID")).ToString()+") " %>'
                                            CommandArgument='<%# Eval("LoanTypeID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="LoanType" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Loan Type" />
                            </Columns>
                        </asp:GridView>--%>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
