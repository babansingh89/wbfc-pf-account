﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="UserMaster.aspx.cs" Inherits="UserMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/UserMaster.js?v=2"></script>
    <script type="text/javascript">

        function beforeSave() {
            $("#frmEcom").validate();
        }


    </script>
    <style type="text/css">
        .loginBtn {
            box-sizing: border-box;
            position: relative;
            /* width: 13em;  - apply for fixed size */
            margin: 0.2em;
            padding: 0 15px 0 46px;
            border: none;
            text-align: left;
            line-height: 34px;
            white-space: nowrap;
            border-radius: 0.2em;
            font-size: 16px;
            color: #FFF;
        }

            .loginBtn:before {
                content: "";
                box-sizing: border-box;
                position: absolute;
                top: 0;
                left: 0;
                width: 34px;
                height: 100%;
            }

            .loginBtn:focus {
                outline: none;
            }

            .loginBtn:active {
                box-shadow: inset 0 0 0 32px rgba(0,0,0,0.1);
            }


        /* Facebook */
        .loginBtn--facebook {
            background-color: #4C69BA;
            background-image: linear-gradient(#4C69BA, #3B55A0);
            /*font-family: "Helvetica neue", Helvetica Neue, Helvetica, Arial, sans-serif;*/
            text-shadow: 0 -1px 0 #354C8C;
        }

            .loginBtn--facebook:before {
                border-right: #364e92 1px solid;
                background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png') 6px 6px no-repeat;
            }

            .loginBtn--facebook:hover,
            .loginBtn--facebook:focus {
                background-color: #5B7BD5;
                background-image: linear-gradient(#5B7BD5, #4864B1);
            }


        /* Google */
        .loginBtn--google {
            /*font-family: "Roboto", Roboto, arial, sans-serif;*/
            background: #DD4B39;
        }

            .loginBtn--google:before {
                border-right: #BB3F30 1px solid;
                background: url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_google.png') 6px 6px no-repeat;
            }

            .loginBtn--google:hover,
            .loginBtn--google:focus {
                background: #E74B37;
            }

        .selected_row {
            background-color: #A1DCF2 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">

    <div id="dialogResetPass" style="display: none;">
        <div>
            <table>
                <tr>
                    <td class="labelCaption" style="font-weight: bold;">User Locked &nbsp;&nbsp;<span class="require">*</span> </td>
                    <td class="labelCaption">:</td>
                    <td align="left">
                        <asp:DropDownList ID="ddlUserResetType" autocomplete="off" Width="150px" Height="25px" Font-Size="14px" Font-Bold="true" ClientIDMode="Static" AppendDataBoundItems="true" runat="server" CssClass="inputbox2" ForeColor="#18588a">
                            <asp:ListItem Text="Password Reset" Value="CP"></asp:ListItem>
                            <asp:ListItem Text="User Locked" Value="UL"></asp:ListItem>
                            <asp:ListItem Text="User UnLocked" Value="UUL"></asp:ListItem>
                        </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="ChkLockUser" autocomplete="off" runat="server" Text=" Show Locked User" Font-Size="14px" Font-Bold="true" class="labelCaption" ClientIDMode="Static" onclick="ShowLockedUser(this)" />
                    </td>
                </tr>
                <tr>
                    <td class="labelCaption" style="font-weight: bold;">User Name &nbsp;&nbsp;<span class="require">*</span> </td>
                    <td class="labelCaption">:</td>
                    <td align="left">
                        <asp:TextBox ID="txtResetUserName" autocomplete="off" Width="300px" ClientIDMode="Static" runat="server" CssClass="inputbox2 AutoUserName"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="labelCaption" style="font-weight: bold;">User Reset Password &nbsp;&nbsp;<span class="require">*</span> </td>
                    <td class="labelCaption">:</td>
                    <td align="left">
                        <asp:TextBox ID="txtResetPass" autocomplete="off" runat="server" Text="" Width="300px" CssClass="inputbox2"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table style="width: 100%">
                <tr>
                    <td>
                        <div id="divLockUser" style="width: 100%;">
                            <asp:GridView ID="GridLockUser" autocomplete="off" Width="100%" ClientIDMode="Static" Font-Size="12px" Font-Bold="true" ForeColor="Blue"
                                clientID="GridLockUser" runat="server" AutoGenerateColumns="False" CssClass="Grid">
                                <Columns>
                                    <asp:BoundField HeaderText="User Type" ItemStyle-Width="100%" />
                                    <asp:BoundField HeaderText="User Code" ItemStyle-Width="100%" />
                                    <asp:BoundField HeaderText="User Name" ItemStyle-Width="100%" />
                                    <asp:BoundField HeaderText="Branch" ItemStyle-Width="100%" />
                                </Columns>
                            </asp:GridView>
                        </div>

                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table>
            </table>
        </div>
    </div>

    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>User Master</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>

                                    <td class="labelCaption">User Type &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlUserType" autocomplete="off" Width="120px" Height="25px" ClientIDMode="Static" AppendDataBoundItems="true" runat="server" CssClass="inputbox2" ForeColor="#18588a">
                                            <%-- <asp:ListItem Text="-Select User Type-" Value="0"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">User Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUserName" autocomplete="off" MaxLength="50" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr id="trPass">
                                    <td class="labelCaption">Password &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPass" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2" TextMode="Password"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Re enter Password &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtRePass" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2" TextMode="Password"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="4" align="left">
                                        <asp:TextBox ID="txtName" autocomplete="off" Width="300px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td style="padding: 5px;" class="labelCaption">Sector</td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td colspan="4" style="padding: 5px;" align="left">
                                        <div id="chkSectorContainer" autocomplete="off" runat="server" clientidmode="Static" style="height: 80px; width: 930px; overflow-y: scroll; font-size: 10px">
                                            <%-- <asp:CheckBoxList ID="ddlSector" RepeatColumns="6"
                                                 CssClass="textbox" Width="850px" runat="server"
                                                AppendDataBoundItems="true">
                                            </asp:CheckBoxList>--%>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>

                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()' />

                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button " OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                            <asp:Button ID="cmdResetPass" runat="server" Text="Reset Password" autocomplete="off"
                                                Width="150px" CssClass="save-button DefaultButton" />

                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divUserMaster" style="width: 98%;">
                        <table id="c" cellpadding="3" width="95%" style="margin-left: 15px; border-collapse: collapse;">
                        </table>
                    </div>

                </td>
            </tr>
        </table>
    </div>
</asp:Content>
    