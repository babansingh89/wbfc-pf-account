﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;

public partial class LoanLedger : System.Web.UI.Page
{
    static string StrFormula = "";
    protected string YearMonth = "";
    protected string VchType = "";
    protected string VchDate = "";
    protected string VchNo = "";
    protected string str = "";
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string[] GET_AutoComplete_StartCode(string STcode, int SectorID)
    {
        List<string> Detail = new List<string>();
        DataTable dt = DBHandler.GetResult("GET_MST_AccLoan_LoaneeCodeName", STcode, SectorID);
        foreach (DataRow dtRow in dt.Rows)
        {
            Detail.Add(dtRow["LOANEE_CODE"].ToString() + "|" + dtRow["LOANEE_NAME"].ToString() + "|" + dtRow["LOANEE_UNQ_ID"].ToString());
        }
        return Detail.ToArray();
    }
      
    [WebMethod]
    public static string[] GET_AutoComplete_EndCode(string ENDcode,int SectorID)
    {
        List<string> Detail = new List<string>();
        DataTable dt = DBHandler.GetResult("GET_MST_AccLoan_LoaneeCodeName", ENDcode,SectorID);
        foreach (DataRow dtRow in dt.Rows)
        {
            Detail.Add(dtRow["LOANEE_CODE"].ToString() + "|" + dtRow["LOANEE_NAME"].ToString() + "|" + dtRow["LOANEE_UNQ_ID"].ToString());
        }
        return Detail.ToArray();
    }
  
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string Start_prd, string End_prd, string strType, string Start_cd, string End_cd, string Balance, string Trans)
    
                                     
    {
        string JSONVal = "";       
        try
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            String Msg = "";
            StrFormula = "";
            int UserID;
            int SectorID;
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            StrFormula = "";       
    
            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, Start_prd, End_prd, strType, Start_cd, End_cd, Balance,Trans, SectorID,"", "");           
            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }
}