﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;

public partial class FundFlowMaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_FundFlowByID", ID);
                Session["ID"] = ID;
                dv.DataSource = dtResult;
                dv.DataBind();

               

                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    ((DropDownList)dv.FindControl("ddlStyle")).SelectedValue = dtResult.Rows[0]["Style"].ToString();
                    ((DropDownList)dv.FindControl("ddlActiveFlag")).SelectedValue = dtResult.Rows[0]["ActiveFlag"].ToString();
                    
                    
                }
                
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_FundFlowByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {

                TextBox txtFunFlowTypeDesc = (TextBox)dv.FindControl("txtFunFlowTypeDesc");
                TextBox txtGrp = (TextBox)dv.FindControl("txtGrp");
                DropDownList ddlStyle = (DropDownList)dv.FindControl("ddlStyle");
                DropDownList ddlActiveFlag = (DropDownList)dv.FindControl("ddlActiveFlag");


                DBHandler.Execute("Insert_FundFlow", txtFunFlowTypeDesc.Text, txtGrp.Text, ddlStyle.SelectedValue, ddlActiveFlag.SelectedValue, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {

                TextBox txtFunFlowTypeDesc = (TextBox)dv.FindControl("txtFunFlowTypeDesc");
                TextBox txtGrp = (TextBox)dv.FindControl("txtGrp");
                DropDownList ddlStyle = (DropDownList)dv.FindControl("ddlStyle");
                DropDownList ddlActiveFlag = (DropDownList)dv.FindControl("ddlActiveFlag");
                
                
                string ID = (string)Session["ID"];
                DBHandler.Execute("Update_FundFlow", ID, txtFunFlowTypeDesc.Text, txtGrp.Text, ddlStyle.SelectedValue, ddlActiveFlag.SelectedValue, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_FundFlow");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    
}