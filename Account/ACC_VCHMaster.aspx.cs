﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ACC_VCHMaster : System.Web.UI.Page
{
    static string StrFormula = "";

    public void SetPageNoCache()
    {
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
        //Response.Cache.SetAllowResponseInBrowserHistory(false);
        //Response.Cache.SetNoStore();
        //Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        //Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
        //Response.AppendHeader("Expires", "0");

        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetExpires(DateTime.UtcNow.AddSeconds(-1));
        Response.Cache.SetAllowResponseInBrowserHistory(false);
        Response.Cache.SetNoStore();
        HttpResponse.RemoveOutputCacheItem("/ACC_VCHMaster.aspx");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            SetPageNoCache();
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
               
            }
            
            PopulateGLSLGrid();
            PopulateSubGrid();
            if (!IsPostBack)
            {
                PopulateDropDown("Inst");
                PopulateDropDown("Supplier");
                PopulateDropDown("Location");
                PopulateJSON();
            }
            

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateSubGrid()
    {
        try
        {
            DataTable SUB = new DataTable();
            SUB.Columns.Add("OLD_SUB_ID");
            SUB.Columns.Add("SUB_ID");
            SUB.Columns.Add("SUB_NAME");
            SUB.Columns.Add("SUB_TYPE");
            SUB.Rows.Add();
            grdSUB.DataSource = SUB;
            grdSUB.DataBind();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateDropDown(string type)
    {
        try
        {
            if (type == "Inst")
            {
                DataTable dtInst = DBHandler.GetResult("Get_Acc_VchInstType_Pf");
                ddlInstType.DataSource = dtInst;
                ddlInstType.DataValueField = "INST_TYPE";
                ddlInstType.DataTextField = "INST_TYPE_DESC";
                ddlInstType.DataBind();
                //ddlInstType.SelectedValue = "H";    

            }
            else if (type == "Supplier")
            {
                DataTable dt = DBHandler.GetResult("Get_Acc_VchSupplier_Pf", "",Session[SiteConstants.SSN_SECTOR_ID]);
                ddlSupplier.DataSource = dt;
                ddlSupplier.DataValueField = "VENDOR_CODE";
                ddlSupplier.DataTextField = "VENDOR_NAME";
                ddlSupplier.DataBind();
            }

            else if (type == "Location")
            {
                DataTable dt = DBHandler.GetResult("Get_Acc_VchFASector_Pf", "");
                ddlLocation.DataSource = dt;
                ddlLocation.DataValueField = "SectorID";
                ddlLocation.DataTextField = "SectorName";
                ddlLocation.DataBind();
            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGLSLGrid()
    {
        try
        {
            DataTable GlSl = new DataTable();
            GlSl.Columns.Add("OLD_SL_ID");
            GlSl.Columns.Add("GL_ID");
            GlSl.Columns.Add("GL_NAME");
            GlSl.Columns.Add("GL_TYPE");
            GlSl.Columns.Add("SL_ID");
            GlSl.Rows.Add();
            grdGLSL.DataSource = GlSl;
            grdGLSL.DataBind();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateJSON()
    {
        try
        {
            VoucherInfo voucherInfo = new VoucherInfo();
            VoucherMaster vchMast = new VoucherMaster();
            List<VoucherDetail> vchDet = new List<VoucherDetail>();
            List<VoucherInstType> vchInstType = new List<VoucherInstType>();
            List<VoucherFixedAsset> vchfa = new List<VoucherFixedAsset>();

            voucherInfo.SessionExpired = "N";
            voucherInfo.EntryType = "I";
            vchMast.SectorID =Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);
            voucherInfo.VoucherMast = vchMast;
            voucherInfo.VoucherDetail = vchDet.ToArray();
            voucherInfo.tmpVchInstType = vchInstType.ToArray();
            voucherInfo.tmpVchFA = vchfa.ToArray();
            ltlj.Text = "<input type='hidden' value='" + voucherInfo.ToJSON() + "' id='voucherJSON' name='voucherJSON'/>";
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetVchDate(string YearMonth, int SectorID)
    {
        string JSONVal = "";

        try
        {
           
            DataTable dtVchDate1 = DBHandler.GetResult("Get_Acc_VChDate_Pf", YearMonth,SectorID, HttpContext.Current.Session["Menu_For"].ToString());

            if (dtVchDate1.Rows.Count > 0)
            {
                var check = new
                {
                    Status = dtVchDate1.Rows[0]["Status"].ToString(),
                    VchDate = dtVchDate1.Rows[0]["Vchdate"].ToString(),
                    Info = dtVchDate1.Rows[0]["Info"].ToString()
                };
                JSONVal = check.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetLoaneeOutDet(string GLID, string SLID, string YRMN, int SectorID)
    {
        string JSONVal = "";

        try
        {

            DataTable dtLoanOut = DBHandler.GetResult("Acc_Get_Loanee_Out_dtls_Pf", GLID, SLID, YRMN, SectorID);

            if (dtLoanOut.Rows.Count > 0)
            {
                var check = new
                {
                    Message = dtLoanOut.Rows[0]["mess"].ToString()
                };
                JSONVal = check.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetYearMon(int sectorid)
    {
        string JSONVal = "";
        //string YEAR_MONTH = "";
        try
        {

            DataTable dtVchDate = DBHandler.GetResult("Get_Year_Month_Pf", sectorid, HttpContext.Current.Session["Menu_For"].ToString());

            if (dtVchDate.Rows.Count > 0)
            {
                //YEAR_MONTH = dtVchDate.Rows[0]["YEAR_MONTH"].ToString();
                var check = new
                {
                    YEAR_MONTH = dtVchDate.Rows[0]["YEAR_MONTH"].ToString(),
                    StrDate= dtVchDate.Rows[0]["YEAR_MONTH"].ToString()
                };
                JSONVal = check.ToJSON();
                //JSONVal = YEAR_MONTH.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static GLSL[] GET_DisGlSl(string GlSlDesc,string Type)
    {
        List<GLSL> glsldet = new List<GLSL>();
        try
        {

            DataTable dtGlSl = DBHandler.GetResult("Get_Acc_VchGLSL_Pf", Type,GlSlDesc, HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID], HttpContext.Current.Session["Menu_For"].ToString());
            foreach (DataRow row in dtGlSl.Rows)
            {
                GLSL glsl = new GLSL();
                glsl.GL_ID = row["GL_ID"].ToString();
                glsl.OLD_SL_ID = row["OLD_SL_ID"].ToString();
                glsl.GL_NAME = row["GL_NAME"].ToString();
                glsl.GL_TYPE = row["GL_TYPE"].ToString();
                glsl.SL_ID = row["SL_ID"].ToString();
                glsldet.Add(glsl);

            }
        }    
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return glsldet.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static SLSUB[] GET_DisSub(string SubDesc,string Type)
    {
        List<SLSUB> SUBdet = new List<SLSUB>();
        try
        {

            DataTable dtSUB = DBHandler.GetResult("Get_Acc_VchSLSub_Pf", Type, SubDesc, HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
            foreach (DataRow row in dtSUB.Rows)
            {
                SLSUB glsl = new SLSUB();
                
                glsl.OLD_SUB_ID = row["OLD_SUB_ID"].ToString();
                glsl.SUB_NAME = row["SUB_NAME"].ToString();
                glsl.SUB_TYPE = row["SUB_TYPE"].ToString();
                glsl.SUB_ID = row["SUB_ID"].ToString();
                SUBdet.Add(glsl);

            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return SUBdet.ToArray();
    }
       
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveVoucher(string VoucherForm)
    {
        string JSONVal = "";
        try
        {
            string noNewLines = VoucherForm.Replace("\n", "");
            VoucherInfo voucherInfo = JSONJavascriptHelper.FromJSON<VoucherInfo>(noNewLines);
            VoucherMaster voucherMast = voucherInfo.VoucherMast;
            VoucherDetail[] voucherDetail = voucherInfo.VoucherDetail; 
            if (!CheckSession())
            {
                if (voucherMast != null)
                {
                    if (voucherInfo.EntryType == "I")
                    {
                        SaveVoucherMaster(voucherMast);

                        if (voucherMast.VCHNo > 0)
                        {
                            if (voucherDetail.Length > 0)
                            {
                                SaveVoucherDetail(voucherDetail, voucherMast);
                            }
                        }
                    }
                    else if (voucherInfo.EntryType == "E")
                    {
                        string Result=UpdateVoucherMast(voucherMast);
                        if (Result == "Y")
                        {
                            DataTable dtDelVoucher = DBHandler.GetResult("Delete_VoucherDetail_Pf", voucherMast.YearMonth, voucherMast.VCHNo, voucherMast.VoucherType, voucherMast.SectorID,
                                HttpContext.Current.Session["Menu_For"].ToString());
                            SaveVoucherDetail(voucherDetail, voucherMast);
                            voucherInfo.SearchResult = "U";
                            voucherInfo.Message = "Voucher No " + voucherMast.VoucherNo + " Update Successfully.";
                        }
                        else if (Result == "N")
                        {
                            voucherInfo.SearchResult = "F";
                            voucherInfo.Message = "Voucher has been finalized cannot update the voucher.";
                        }
                        else if (Result == "R")
                        {
                            voucherInfo.SearchResult = "R";
                            voucherInfo.Message = "Error in Deleting Voucher";
                        }
                    }
                    

                }
            }
            else  
            {
                voucherInfo.SessionExpired = "Y";
            }

            JSONVal = voucherInfo.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    private static string UpdateVoucherMast(VoucherMaster voucherMast)
    {
        string Result="";
        try
        {
            if (voucherMast != null)
            {
                DateTime dtVCHDate = DateTime.ParseExact(voucherMast.VCHDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DataTable dtVchMast = DBHandler.GetResult("Update_VoucherMaster_Pf",
                    voucherMast.YearMonth,
                    voucherMast.VoucherType,
                    voucherMast.VCHNo,
                    voucherMast.SectorID,
                    dtVCHDate.ToShortDateString(),
                    voucherMast.VCHAMOUNT,
                    voucherMast.NARRATION,
                    HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID],
                    HttpContext.Current.Session["Menu_For"].ToString()
                    );

                if (dtVchMast.Rows.Count > 0)
                {
                    Result = dtVchMast.Rows[0]["Result"].ToString();
                }
                else
                {
                    Result = "R";
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Result;
    }

    private static void SaveVoucherDetail(VoucherDetail[] voucherDetail, VoucherMaster voucherMast)
    {
        try
        {
            DateTime dtVchDate = (DateTime.ParseExact(voucherMast.VCHDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture));
            if (voucherDetail.Length > 0)
            {
                foreach (VoucherDetail vchdet in voucherDetail)
                {
                    DataTable dtVchDet = DBHandler.GetResult("Insert_Acc_VoucherDetail_Pf",
                        voucherMast.YearMonth,
                        voucherMast.VoucherType,
                        voucherMast.VCHNo,
                        vchdet.GLID,
                        vchdet.SLID,
                        vchdet.SubID,
                        vchdet.DRCR,
                        vchdet.AMOUNT,
                        vchdet.GlType,
                        vchdet.SectorID,
                        HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]
                        );

                    if (dtVchDet.Rows.Count > 0)
                    {
                        vchdet.VchSrl=Convert.ToInt32(dtVchDet.Rows[0]["VCHSRL"].ToString());
                        if (vchdet.VchInstType.Length > 0)
                        {
                            foreach (VoucherInstType vchinst in vchdet.VchInstType)
                            {
                                //DateTime dtInstDate = (vchinst.InstType == "O" ? DateTime.ParseExact(voucherMast.VCHDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture) : DateTime.ParseExact(vchinst.InstDT, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                                DateTime dtInstDate = (vchinst.InstDT=="" ? DateTime.ParseExact(voucherMast.VCHDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture) : DateTime.ParseExact(vchinst.InstDT, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                                
                                DataTable dtVchInst=DBHandler.GetResult("Insert_Acc_VoucherInstrument_Pf",
                                        voucherMast.YearMonth,
                                        voucherMast.VoucherType,
                                        voucherMast.VCHNo,
                                        vchdet.GLID,
                                        vchdet.SLID,
                                        vchdet.DRCR,
                                        vchinst.SlipNo,
                                        vchinst.InstType,
                                        vchinst.InstNo,
                                        (dtInstDate.ToShortDateString()),
                                        vchinst.Amount,
                                        vchinst.ActualAmount,
                                        vchinst.Drawee,
                                        vchinst.SectorID,
                                        vchdet.GlType,
                                        dtVchDate.ToShortDateString(),
                                        HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]
                                    );
                            }
                        }
                        if (vchdet.VchFA.Length > 0)
                        {
                            foreach (VoucherFixedAsset vfa in vchdet.VchFA)
                            {

                                DataTable dtFA = DBHandler.GetResult("Insert_Acc_VchFixedAsset_Pf",
                                    vchdet.GLID,
                                    vfa.FA_DESC,
                                    vfa.VENDOR_CODE,
                                    vfa.DEP_RATE,
                                    vfa.DEP_METHOD,
                                    (vchdet.DRCR=="D"?(object)vfa.PUR_VALUE:DBNull.Value),
                                    (vchdet.DRCR=="D"?(object)voucherMast.VCHNo:DBNull.Value),
                                    (vchdet.DRCR=="D"?(object)dtVchDate.ToShortDateString():DBNull.Value),
                                    vfa.VCH_SECTORID,
                                    (vchdet.DRCR=="C"?(object)vfa.SOL_VALUE:DBNull.Value),
                                    (vchdet.DRCR=="C"?(object)voucherMast.VCHNo:DBNull.Value),
                                    (vchdet.DRCR=="C"?(object)dtVchDate.ToShortDateString():DBNull.Value),
                                    vfa.SECTORID,
                                    vchdet.GlType,
                                    vchdet.DRCR,
                                    HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]
                                    );

                                if (dtFA.Rows.Count > 0)
                                {
                                    vfa.FA_ID = Convert.ToInt32(dtFA.Rows[0]["FA_ID"].ToString());
                                }

                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
        
    private static void SaveVoucherMaster(VoucherMaster voucherMast)
    {
        try
        {
            if (voucherMast != null)
            {
                DateTime dtVCHDate=DateTime.ParseExact(voucherMast.VCHDATE,"dd/MM/yyyy",CultureInfo.InvariantCulture);
                DataTable dtVchMast = DBHandler.GetResult("Insert_Acc_VoucherMaster_Pf",
                    voucherMast.YearMonth,
                    voucherMast.VoucherType,
                    dtVCHDate.ToShortDateString(),
                    voucherMast.VCHAMOUNT,
                    voucherMast.NARRATION,
                    voucherMast.STATUS,
                    voucherMast.SectorID,
                    HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID],
                    HttpContext.Current.Session["Menu_For"].ToString()
                    );

                if (dtVchMast.Rows.Count > 0)
                {
                    voucherMast.VCHNo = Convert.ToInt32(dtVchMast.Rows[0]["VchNo"].ToString());
                    voucherMast.VoucherNo = dtVchMast.Rows[0]["VoucherNo"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private static bool CheckSession()
    {
        bool retValue = false;
        try
        {
            if (HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID] == null)
            {
                retValue = true;
            }
            else
            {
                retValue = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return retValue;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteVoucher(string VoucherForm)
    {
        string JSONVal = "";
        try
        {
            string noNewLines = VoucherForm.Replace("\n", "");
            VoucherInfo voucherInfo = JSONJavascriptHelper.FromJSON<VoucherInfo>(noNewLines);
            VoucherMaster voucherMast = voucherInfo.VoucherMast;

            if (voucherMast != null)
            {
                if (voucherMast.VCHNo > 0)
                {
                    DataTable dtDeleteVoucher = DBHandler.GetResult("Delete_Voucher_Pf", voucherMast.VCHNo, voucherMast.YearMonth, voucherMast.VoucherType, 
                        HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID], HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID],
                        HttpContext.Current.Session["Menu_For"].ToString());
                    if (dtDeleteVoucher.Rows.Count > 0)
                    {
                        if (dtDeleteVoucher.Rows[0]["Result"].ToString() == "Y")
                        {
                            voucherInfo.SearchResult = "D";
                            voucherInfo.Message = "Voucher has been successfully deleted";
                        }
                        else if (dtDeleteVoucher.Rows[0]["Result"].ToString() == "N")
                        {
                            voucherInfo.SearchResult = "F";
                            voucherInfo.Message = "Voucher has been finalized cannot delete the voucher.";
                        }

                    }
                    else
                    {
                        voucherInfo.SearchResult = "R";
                        voucherInfo.Message = "Error in Deleting Voucher";
                    }

                }
            }

            JSONVal = voucherInfo.ToJSON();
        }
        catch (Exception ex)
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GetSearchVoucherResult(string VoucherForm, string YearMonth,int VchType, string VchNo)
    {
        string JSONVal = "";
        try
        {
            ////TextBox txtYearMonth = (TextBox)dv.FindControl("txtYearMonth");
            ////DropDownList ddlVchType = (DropDownList)dv.FindControl("ddlVchType");
            ////TextBox txtVoucherNo = (TextBox)dv.FindControl("txtVoucherNo");
            DataSet ds = DBHandler.GetResults("Get_VoucherSearchInfo_Pf", VchNo,
                VchType,
                YearMonth,
                HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID],
                HttpContext.Current.Session["Menu_For"].ToString());

            if (ds.Tables.Count > 0)
            {
                string noNewLines = VoucherForm.Replace("\n", "");
                VoucherInfo voucherInfo = JSONJavascriptHelper.FromJSON<VoucherInfo>(noNewLines);
                VoucherMaster vchMast = new VoucherMaster();
                List<VoucherDetail> vchDet = new List<VoucherDetail>();
                List<VoucherInstType> vchTmpInstType = new List<VoucherInstType>();
                List<VoucherFixedAsset> vchTmpfa = new List<VoucherFixedAsset>();

                List<VoucherFixedAsset> vchfa = new List<VoucherFixedAsset>();

                DataTable dtMast = ds.Tables[0];
                DataTable dtDetail = ds.Tables[1];
                DataTable dtInst = ds.Tables[2];
                if (dtMast.Rows.Count > 0)
                {
                    vchMast.YearMonth = dtMast.Rows[0]["YEAR_MONTH"].ToString();
                    vchMast.VCHDATE = dtMast.Rows[0]["VCH_DATE"].ToString();
                    vchMast.VCHNo = Convert.ToInt32(dtMast.Rows[0]["VCH_NO"].ToString());
                    vchMast.VoucherType = Convert.ToInt32(dtMast.Rows[0]["VCH_TYPE"].ToString());
                    vchMast.STATUS = dtMast.Rows[0]["STATUS"].ToString();
                    vchMast.VoucherNo = dtMast.Rows[0]["VoucherNo"].ToString();
                    vchMast.SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
                    vchMast.NARRATION = dtMast.Rows[0]["NARRATION"].ToString();
                    vchMast.VCHAMOUNT = dtMast.Rows[0]["VCH_AMOUNT"].ToString();

                    foreach (DataRow dr in dtDetail.Rows)
                    {
                        VoucherDetail vd = new VoucherDetail();
                        vd.YearMonth = dr["YEAR_MONTH"].ToString();
                        vd.VoucherType = Convert.ToInt32(dr["VCH_TYPE"].ToString());
                        vd.VCHNo = Convert.ToInt32(dr["VCH_NO"].ToString());
                        vd.VchSrl = Convert.ToInt32(dr["VCH_SRL"].ToString());
                        vd.OLDSLID = (dr["OLD_SL_ID"].ToString());
                        vd.GLName = (dr["GL_NAME"].ToString());
                        vd.GLID = (dr["GL_ID"].ToString());
                        vd.SLID = (dr["SL_ID"].ToString());
                        vd.SubID = (dr["SUB_ID"].ToString());
                        vd.OLDSUBID = (dr["OLD_SUB_ID"].ToString());
                        vd.SUBDesc = (dr["Sub_Name"].ToString());
                        vd.DRCR = (dr["DR_CR"].ToString());
                        vd.GlType = (dr["GL_TYPE"].ToString());
                        vd.AMOUNT = Convert.ToDouble(dr["AMOUNT"].ToString());
                        vd.SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

                        DataRow[] rows = dtInst.Select("YEAR_MONTH='" + dr["YEAR_MONTH"].ToString() + "' and VCH_TYPE="
                            + dr["VCH_TYPE"].ToString() + " and VCH_NO=" + dr["VCH_NO"].ToString()
                            + " and GL_ID='" + dr["GL_ID"].ToString() + "' and SL_ID='" + dr["SL_ID"].ToString()+"'");
                        List<VoucherInstType> vchInstType = new List<VoucherInstType>();
                        List<VoucherFixedAsset> vchFA = new List<VoucherFixedAsset>();
                        int i = 0;
                        foreach (DataRow row in rows)
                        {
                            VoucherInstType vit = new VoucherInstType();
                            vit.InstSrNo = i + 1;
                            vit.YearMonth = row["YEAR_MONTH"].ToString();
                            vit.VoucherType = Convert.ToInt32(row["VCH_TYPE"].ToString());
                            vit.VCHNo = Convert.ToInt32(row["VCH_NO"].ToString());
                            vit.GLID = (row["GL_ID"].ToString());
                            vit.SLID = (row["SL_ID"].ToString());
                            vit.DRCR = (row["DR_CR"].ToString());
                            vit.SlipNo = (row["SLIP_NO"].ToString());
                            vit.InstType = (row["INST_TYPE"].ToString());
                            vit.InstTypeName = (row["InstTypeName"].ToString());
                            vit.InstNo = (row["INST_NO"].ToString());
                            vit.InstDT = (row["INST_DT"].ToString());
                            vit.Amount = Convert.ToDouble(row["amount"].ToString());
                            vit.ActualAmount = Convert.ToDouble(row["ACTUAL_AMOUNT"].ToString());
                            vit.Drawee = (row["DRAWEE"].ToString());
                            vit.Cleared = (row["CLEARED"].ToString());
                            vit.ClearedOn = (row["CLEARED_ON"].ToString());
                            vit.SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
                            vchInstType.Add(vit);
                        }

                        vd.VchInstType = vchInstType.ToArray();
                        vd.VchFA = vchFA.ToArray();
                        vchDet.Add(vd);

                    }



                    voucherInfo.SearchResult = "Y";

                }
                else
                {
                    voucherInfo.SearchResult = "N";
                }
                voucherInfo.SessionExpired = "N";
                voucherInfo.EntryType = "E";
                voucherInfo.VoucherMast = vchMast;
                voucherInfo.VoucherDetail = vchDet.ToArray();
                voucherInfo.tmpVchInstType = vchTmpInstType.ToArray();
                voucherInfo.tmpVchFA = vchTmpfa.ToArray();
                
                //ltlj.Text = "<input type='hidden' value='" + voucherInfo.ToJSON() + "' id='voucherJSON' name='voucherJSON'/>";
                JSONVal = voucherInfo.ToJSON();
            }
        }
        catch (Exception ex)
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string VoucherForm, string FormName, string ReportName, string ReportType)
    {
        string JSONVal = "";

        try
        {
            string noNewLines = VoucherForm.Replace("\n", "");
            VoucherInfo voucherInfo = JSONJavascriptHelper.FromJSON<VoucherInfo>(noNewLines);
            VoucherMaster voucherMast = voucherInfo.VoucherMast;
            VoucherDetail[] voucherDetail = voucherInfo.VoucherDetail;
            if (!CheckSession())
            {
                if (voucherMast != null)
                {
                    if (voucherMast.VCHNo > 0)
                    {
                        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
                        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
                        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
                        String Msg = "";
                        StrFormula = "";
                        int UserID;
                        int SectorID;
                        string StrPaperSize = "";
                        StrPaperSize = "Page - A4";
                        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                        SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

                        StrFormula = "";

                        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
                        DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, voucherMast.YearMonth, voucherMast.VoucherType, voucherMast.VoucherNo, voucherMast.VoucherNo, voucherMast.SectorID, "", "", "", "", "");
                        voucherInfo.Message = "Show";
                    }
                }
            }
            else
            {
                voucherInfo.SessionExpired = "Y";
            }



            JSONVal = voucherInfo.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
          
    }

    //============================ Start Max Voucher Number Coding Start here===================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_MaxSchemeID(string Yearmonth,int VchType,int SecId)
    {
        string MaxVchNo = "";
        DataTable dtGetData = DBHandler.GetResult("Get_MaxVoucherNo_Pf", Yearmonth, VchType, SecId, HttpContext.Current.Session["Menu_For"].ToString());
        if (dtGetData.Rows.Count > 0)
        {
            MaxVchNo = Convert.ToString(dtGetData.Rows[0]["VoucherNo"]);
        }

        return MaxVchNo;
    }
    //============================End Max Voucher Number Coding Start here===================================//

    protected void btnSearchVch_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    TextBox txtYearMonth=(TextBox)dv.FindControl("txtYearMonth");
        //    DropDownList ddlVchType=(DropDownList)dv.FindControl("ddlVchType");
        //    TextBox txtVoucherNo=(TextBox)dv.FindControl("txtVoucherNo");
        //    DataSet ds = DBHandler.GetResults("Get_VoucherSearchInfo", txtVoucherNo.Text, 
        //        ddlVchType.SelectedValue, 
        //        txtYearMonth.Text, 
        //        Session[SiteConstants.SSN_INT_USER_ID]);

        //    if (ds.Tables.Count > 0)
        //    {
        //        VoucherInfo voucherInfo = new VoucherInfo();
        //        VoucherMaster vchMast = new VoucherMaster();
        //        List<VoucherDetail> vchDet = new List<VoucherDetail>();
        //         List<VoucherInstType> vchTmpInstType = new List<VoucherInstType>();
        //        List<VoucherFixedAsset> vchTmpfa = new List<VoucherFixedAsset>();
                
        //        List<VoucherFixedAsset> vchfa = new List<VoucherFixedAsset>();

        //        DataTable dtMast = ds.Tables[0];
        //        DataTable dtDetail = ds.Tables[1];
        //        DataTable dtInst = ds.Tables[2];
        //        if (dtMast.Rows.Count > 0)
        //        {
        //            vchMast.YearMonth = dtMast.Rows[0]["YEAR_MONTH"].ToString();
        //            vchMast.VCHDATE = dtMast.Rows[0]["VCH_DATE"].ToString();
        //            vchMast.VCHNo = Convert.ToInt32(dtMast.Rows[0]["VCH_NO"].ToString());
        //            vchMast.VoucherType = Convert.ToInt32(dtMast.Rows[0]["VCH_TYPE"].ToString());
        //            vchMast.STATUS = dtMast.Rows[0]["STATUS"].ToString();
        //            vchMast.VoucherNo = dtMast.Rows[0]["VoucherNo"].ToString();
        //            vchMast.SectorID = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);
        //            vchMast.NARRATION = dtMast.Rows[0]["NARRATION"].ToString();
        //            vchMast.VCHAMOUNT = dtMast.Rows[0]["VCH_AMOUNT"].ToString();

        //            foreach (DataRow dr in dtDetail.Rows)
        //            {
        //                VoucherDetail vd = new VoucherDetail();
        //                vd.YearMonth = dr["YEAR_MONTH"].ToString();
        //                vd.VoucherType =Convert.ToInt32(dr["VCH_TYPE"].ToString());
        //                vd.VCHNo = Convert.ToInt32(dr["VCH_NO"].ToString());
        //                vd.VchSrl = Convert.ToInt32(dr["VCH_SRL"].ToString());
        //                vd.OLDSLID = (dr["OLD_SL_ID"].ToString());
        //                vd.GLName = (dr["GL_NAME"].ToString());
        //                vd.GLID = (dr["GL_ID"].ToString());
        //                vd.SLID = (dr["SL_ID"].ToString());
        //                vd.SubID = (dr["SUB_ID"].ToString());
        //                vd.OLDSUBID = (dr["OLD_SUB_ID"].ToString());
        //                vd.SUBDesc = (dr["Sub_Name"].ToString());
        //                vd.DRCR = (dr["DR_CR"].ToString());
        //                vd.GlType = (dr["GL_TYPE"].ToString());
        //                vd.AMOUNT = Convert.ToDouble(dr["AMOUNT"].ToString());
        //                vd.SectorID = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);

        //                DataRow[] rows = dtInst.Select("YEAR_MONTH='" + dr["YEAR_MONTH"].ToString() + "' and VCH_TYPE="
        //                    + dr["VCH_TYPE"].ToString() + " and VCH_NO=" + dr["VCH_NO"].ToString()
        //                    + " and GL_ID='" + dr["GL_ID"].ToString() + "' and SL_ID='" + dr["SL_ID"].ToString());
        //                List<VoucherInstType> vchInstType = new List<VoucherInstType>();
        //                int i=0;
        //                foreach (DataRow row in rows)
        //                {
        //                    VoucherInstType vit = new VoucherInstType();
        //                    vit.InstSrNo = i + 1;
        //                    vit.YearMonth = row["YEAR_MONTH"].ToString();
        //                    vit.VoucherType = Convert.ToInt32(row["VCH_TYPE"].ToString());
        //                    vit.VCHNo = Convert.ToInt32(row["VCH_NO"].ToString());
        //                    vit.GLID = (row["GL_ID"].ToString());
        //                    vit.SLID = (row["SL_ID"].ToString());
        //                    vit.DRCR = (row["DR_CR"].ToString());
        //                    vit.SlipNo = (row["SLIP_NO"].ToString());
        //                    vit.InstType = (row["INST_TYPE"].ToString());
        //                    vit.InstTypeName = (row["InstTypeName"].ToString());
        //                    vit.InstNo = (row["INST_NO"].ToString());
        //                    vit.InstDT = (row["INST_DT"].ToString());
        //                    vit.Amount = Convert.ToDouble(row["amount"].ToString());
        //                    vit.ActualAmount = Convert.ToDouble(row["ACTUAL_AMOUNT"].ToString());
        //                    vit.Drawee = (row["DRAWEE"].ToString());
        //                    vit.Cleared = (row["CLEARED"].ToString());
        //                    vit.ClearedOn = (row["CLEARED_ON"].ToString());
        //                    vit.SectorID = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);
        //                    vchInstType.Add(vit);
        //                }

        //                vd.VchInstType = vchInstType.ToArray();
        //                vchDet.Add(vd);

        //            }

        //        }

        //        voucherInfo.SessionExpired = "N";
        //        voucherInfo.EntryType = "E";
                
        //        voucherInfo.VoucherMast = vchMast;
        //        voucherInfo.VoucherDetail = vchDet.ToArray();
        //        voucherInfo.tmpVchInstType = vchTmpInstType.ToArray();
        //        voucherInfo.tmpVchFA = vchTmpfa.ToArray();
        //        ltlj.Text = "<input type='hidden' value='" + voucherInfo.ToJSON() + "' id='voucherJSON' name='voucherJSON'/>";
        //    }
        //}
        //catch (Exception ex)
        //{
        //    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        //}
    }
}