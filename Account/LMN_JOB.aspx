﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_JOB.aspx.cs" Inherits="LMN_JOB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <script type="text/javascript">
         $(document).ready(function () {

         });

         function beforeSave() {
             $("#frmEcom").validate();

             $("#ddlJObCategoryId").rules("add", { required: true, messages: { required: "Please Select Job Category ID" } });
             $("#txtJobDescription").rules("add", { required: true, messages: { required: "Please enter Job Description" } });
             $("#txtJobOrder").rules("add", { required: true, messages: { required: "Please enter job order" } });
             $("#txtTimeToCompleteMin").rules("add", { required: true, messages: { required: "Please enter Account Code time To Complete Min" } });
         }

         function Delete(id) { 
             if (confirm("Are You sure you want to delete?")) {
                 $("#frmEcom").validate().currentForm = '';
                 return true;
             } else {
                 return false;
             }
         }

         function unvalidate() {
             $("#frmEcom").validate().currentForm = '';
             return true;
         }

         $(document).ready(function () {
             $("#txtindustryCode").keypress(function (e) {
                 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                     alert("Please Enter Numeric Value");
                     return false;
                 }
             });
         }); 

         $(document).ready(function () {
             $("#txtJobOrder").keypress(function (e) {
                 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                     alert("Please Enter Numeric Value");
                     return false;
                 }
             });
         });

         $(document).ready(function () {
             $("#txtTimeToCompleteMin").keypress(function (e) {
                 if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                     alert("Please Enter Numeric Value");
                     return false;
                 }
             });
         });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>JOB MASTER</td>
            </tr>
        </table>
        <br />
        
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Job Category ID &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:DropDownList ID="ddlJObCategoryId" autocomplete="off" Width="200px" runat="server" AppendDataBoundItems="true" AutoPostBack="true"  DataSource='<%# drpload() %>'
                                             DataValueField="JobCategoryID" 
                                             DataTextField="JobCategoryDesc">
                                         <asp:ListItem Text="(Select Job Category ID)" Value=""></asp:ListItem>
                                         </asp:DropDownList>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="labelCaption">Job Description &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtJobDescription" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    
                                    <td class="labelCaption">Job Description &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:DropDownList ID="ddljobdescription" autocomplete="off" Width="200px" runat="server" AppendDataBoundItems="true" AutoPostBack="true"  DataSource='<%# drpload1() %>'
                                             DataValueField="JobCategoryID" 
                                             DataTextField="JobDescription">
                                         <%--<asp:ListItem Text="(Select Job Category ID)" Value=""></asp:ListItem>--%>
                                         </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Job Order<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtJobOrder" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Time To Complete Min<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTimeToCompleteMin" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>


                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Job Category ID &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:DropDownList ID="ddlJObCategoryId" autocomplete="off" Width="200px" runat="server" AppendDataBoundItems="true" AutoPostBack="true"  DataSource='<%# drpload() %>'
                                             DataValueField="JobCategoryID" 
                                             DataTextField="JobCategoryDesc">
                                         <asp:ListItem Text="(Select A Industry Type)" Value=""></asp:ListItem>
                                         </asp:DropDownList>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td class="labelCaption">Job Description &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtJobDescription" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("JobDescription") %>'></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Job Description &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:DropDownList ID="ddljobdescription" autocomplete="off" Width="200px" runat="server" AppendDataBoundItems="true" AutoPostBack="true"  DataSource='<%# drpload1() %>'
                                             DataValueField="JobCategoryID" 
                                             DataTextField="JobDescription">
                                         <%--<asp:ListItem Text="(Select Job Category ID)" Value=""></asp:ListItem>--%>
                                         </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Job Order<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtJobOrder" autocomplete="off"  ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("JobOrder") %>'></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Time To Complete Min<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTimeToCompleteMin" autocomplete="off"  ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("TimeToCompleteInMin") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
             <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="JobID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("JobID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("JobID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("JobID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                                
                                <asp:BoundField DataField="JobCategoryDesc" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="job Category ID" />
                                <asp:BoundField DataField="JobDescription" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Job Description" />
                                <asp:BoundField DataField="JobOrder" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Job Order" />
                                <asp:BoundField DataField="TimeToCompleteInMin" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Time To Complete Min " />                              
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

