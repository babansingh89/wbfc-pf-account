﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;

public partial class VoucherReport : System.Web.UI.Page
{
    static string StrFormula = "";

    protected string YearMonth = "";
    protected string VchType = "";
    protected string VchDate = "";
    protected string VchNo = "";
    protected string VchStatus = "";
    protected string str = "";  
    protected string SLGLID;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
              
            if (!IsPostBack)
            {
                GridValue();
            }
        }     
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void GridValue()
    {
        DataTable Bound = new DataTable();
        Bound.Columns.Add("desc_dtl");
        Bound.Columns.Add("InstNo");
        Bound.Columns.Add("InstDate");
        Bound.Columns.Add("Amount");
        Bound.Columns.Add("Bank");
        Bound.Rows.Add();
        tbl.DataSource = Bound;
        tbl.DataBind();
    }

    protected DataTable drpload()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Load_InsertedVoucher");
            return dt;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            
            if (cmdSave.CommandName == "search")
            {
                
                TextBox txtYearmonth = (TextBox)dv.FindControl("txtYearmonth");
                TextBox txtVchNo = (TextBox)dv.FindControl("txtVchNo");
                DropDownList ddlVoucharType = (DropDownList)dv.FindControl("ddlVoucharType");

                DropDownList ddlEmp = (DropDownList)dv.FindControl("ddlEmp");
                TextBox txtVchAmount = (TextBox)dv.FindControl("txtVchAmount");
                TextBox txtNarration = (TextBox)dv.FindControl("txtNarration");
                HiddenField txtHdnSlCodeSearch = (HiddenField)dv.FindControl("txtHdnSlCodeSearch");
                TextBox txtVchFromDate = (TextBox)dv.FindControl("txtVchFromDate");
                TextBox txtVchToDate = (TextBox)dv.FindControl("txtVchToDate");

                if (txtVchToDate.Text != "")
                {
                    DateTime dtFrom = DateTime.ParseExact(txtVchFromDate.Text, "d/M/yyyy", CultureInfo.InvariantCulture);
                    txtVchFromDate.Text = dtFrom.ToString("yyyy-MM-dd");
                }

                if (txtVchToDate.Text != "")
                {
                    DateTime dtTo = DateTime.ParseExact(txtVchToDate.Text, "d/M/yyyy", CultureInfo.InvariantCulture);
                    txtVchToDate.Text = dtTo.ToString("yyyy-MM-dd");
                }
                if (txtHdnSlCodeSearch.Value == "")
                {
                    SLGLID = "0";
                }
                else {
                    SLGLID = txtHdnSlCodeSearch.Value;
                }

                PopulateGrid(txtVchNo.Text, txtYearmonth.Text, ddlVoucharType.SelectedValue, ddlEmp.SelectedValue, txtVchAmount.Text, txtNarration.Text, SLGLID, txtVchFromDate.Text, txtVchToDate.Text);
              
            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid(string VoucherNo, string Year_Month, string VoucherType, string EmpID, string VchAmount, string Narration, string GLSLID, string VchFromDate, string VchToDate)
    { 
        try     
        {
            panVchFinal.Visible = true;
            string Rsinword = "";
            if (VoucherNo == "")
            {
                DataTable dt = DBHandler.GetResult("Get_VchNoByYearmonth", Convert.ToInt32(VoucherType), Year_Month, Session[SiteConstants.SSN_SECTOR_ID].ToString(), EmpID, VchAmount, Narration, GLSLID, VchFromDate, VchToDate);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        string vchno = dt.Rows[i]["VoucherNo"].ToString();
                        DataSet ds = DBHandler.GetResults("Get_GLSLVoucherReport", dt.Rows[i]["VoucherNo"].ToString(), Convert.ToInt32(VoucherType), Year_Month, Session[SiteConstants.SSN_SECTOR_ID].ToString(), EmpID, VchAmount, Narration);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            YearMonth = ds.Tables[0].Rows[0]["YEAR_MONTH"].ToString();
                            VchType = ds.Tables[0].Rows[0]["VCH_TYPE"].ToString();
                            VchDate = ds.Tables[0].Rows[0]["VCH_DATE"].ToString();
                            VchNo = ds.Tables[0].Rows[0]["VoucherNo"].ToString();
                            VchStatus = ds.Tables[0].Rows[0]["STATUS"].ToString();
                            string DRCR = "";
                            decimal DrAmt = 0;
                            decimal CrAmt = 0;
                            decimal SumDrAmt = 0;
                            decimal SumCrAmt = 0;

                            //str += "<table width='80%' align='center'>";
                            //str += "<tr>";
                            //str += "<td style='font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;'>Year Month : <font style='font-family: Segoe UI; font-size: 12px; color: Navy;'>" + YearMonth + "</font></td>";
                            //str += "</tr>";

                            //str += "<table width='80%' align='center'>";
                            //str += "<tr>";
                            //str += "<td style='font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;'>Voucher Type : <font style='font-family: Segoe UI; font-size: 12px; color: Navy;'>" + VchType + "</font></td>";
                            //str += "</tr>";

                            str += "<table width='100%' style='margin-left: 65px; border-collapse: collapse;' align='center'>";
                            str += "<tr>";
                            str += "<td style='font-family: Segoe UI; font-size: 16px; font-weight: bold; color: Navy; width:auto;'>Voucher No : <font style='font-family: Segoe UI; font-size: 16px; color: Navy;'>" + VchNo + "</font></td>";
                            str += "<td style='font-family: Segoe UI; font-size: 16px; font-weight: bold; color: Navy; padding-left: 130px; width:auto;'>Voucher Status : <font style='font-family: Segoe UI; font-size: 16px; color: Navy;'>" + VchStatus + "</font></td>";
                            str += "<td style='font-family: Segoe UI; font-size: 16px; font-weight: bold; color: Navy; padding-left: 40px; width:auto;'>Voucher Date : <font style='font-family: Segoe UI; font-size: 16px; color: Navy;'>" + VchDate + "</font></td>";
                            str += "</tr>";

                            //str += "<table width='80%' align='center'>";
                            //str += "<tr>";
                            //str += "<td style='font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;'>Voucher No : <font style='font-family: Segoe UI; font-size: 12px; color: Navy;'>" + VchNo + "</font></td>";
                            //str += "</tr>";

                            //str += "</br>";              

                            str += "<table id='tabDetail' cellpadding='3' width='100%' style='margin-left: 65px; border-collapse: collapse;' align='center'>";
                            str += "<tr>";
                            str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>GL_type</th>";
                            str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>GL_ID</th>";
                            str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>SL_ID</th>";
                            str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>YearMonth</th>";
                            str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>VchNo</th>";
                            str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>SectorID</th>";
                            
                            str += "<th class='TableHeader' style='text-align: center; width: 10%; border: 1px solid white;'>Code</th>";
                            str += "<th class='TableHeader' style='text-align: center; width: 10%; border: 1px solid white;'>Sub ID</th>";
                            str += "<th class='TableHeader' style='text-align: center; width: 10%; border: 1px solid white;'>Sub Name</th>";
                            str += "<th class='TableHeader' style='text-align: center; width: 45%; border: 1px solid white;'>Particulars</th>";
                            //str += "<th class='TableHeader' style='text-align: center; width: 45%; border: 1px solid white;'>AmountInWords</th>";
                            str += "<th class='TableHeader' style='text-align: center; width: 9%; border: 1px solid white;'>Dr/Cr</th>";
                            str += "<th class='TableHeader' style='text-align: center; width: 15%; border: 1px solid white;'>Debit</th>";
                            str += "<th class='TableHeader' style='text-align: center; width: 15%; border: 1px solid white;'>Credit</th>";
                            str += "</tr>";
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                for (int k = 0; k <= ds.Tables[1].Rows.Count - 1; k++)
                                {
                                    string Amt = Convert.ToString(Math.Round(SumDrAmt, 2));
                                    //Rsinword = changeToWords(Amt);
                                    Rsinword = AmountInWords(Convert.ToDecimal(Amt));

                                    DRCR = ds.Tables[1].Rows[k]["DR_CR"].ToString();
                                    DrAmt = (DRCR == "Debit") ? Convert.ToDecimal(ds.Tables[1].Rows[k]["AMOUNT"].ToString()) : Convert.ToDecimal("0.00");
                                    SumDrAmt += DrAmt;
                                    CrAmt = (DRCR == "Credit") ? Convert.ToDecimal(ds.Tables[1].Rows[k]["AMOUNT"].ToString()) : Convert.ToDecimal("0.00");
                                    SumCrAmt += CrAmt;
                                    str += "<tr>";
                                    str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["GL_TYPE"].ToString() + "</td>";
                                    str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["GL_ID"].ToString() + "</td>";
                                    str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["SL_ID"].ToString() + "</td>";
                                    str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["YEAR_MONTH"].ToString() + "</td>";
                                    str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["VCH_NO"].ToString() + "</td>";
                                    str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["SectorID"].ToString() + "</td>";

                                    str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["OLD_SL_ID"].ToString() + "</td>";
                                    str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["OLD_SUB_ID"].ToString() + "</td>";
                                    //str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["SUB_NAME"].ToString() + "</td>";

                                    str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'> <div onClick='FetchData(this);'><span style='cursor:pointer'>" + ds.Tables[1].Rows[k]["SUB_NAME"].ToString() + "</span></td>";
                                    str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'> <div onClick='FetchData(this);'><span style='cursor:pointer'>" + ds.Tables[1].Rows[k]["GL_NAME"].ToString() + "</span></td>";
                                    str += "<td class='labelCaption' style='text-align: left; width: 9%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["DR_CR"].ToString() + "</td>";
                                    str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy;'> " + Math.Round(DrAmt, 2) + "</td>";
                                    str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy;'>" + Math.Round(CrAmt, 2) + "</td>";
                                    str += "</tr>";
                                }
                                str += "<tr id='summary'>";
                                str += "<td colspan='4' class='labelCaption' style='text-align: left; width: 9%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'><span ID='lbltext' runat='server'></span></td>";
                                str += "<td class='labelCaption' style='text-align: right; width: 9%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>Total :- </td>";
                                str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>" + Math.Round(SumDrAmt, 2) + " </td>";
                                str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>" + Math.Round(SumCrAmt, 2) + "</td>";
                                str += "</tr>";
                                str += "<tr>";
                                str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>Narration : </td>";
                                str += "<td colspan='6' class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'> " + ds.Tables[0].Rows[0]["NARRATION"].ToString() + "</td>";
                                str += "</tr>";

                                str += "<tr>";
                                str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>Amount in Words : </td>";
                                str += "<td colspan='6' class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'> " + Rsinword + "</td>";
                                str += "</tr>";

                            }
                            str += "<tr>";
                            str += "<td colspan='9' class='labelCaption'>";
                            str += "<hr class='borderStyle' />";
                            str += "</td>";
                            str += "</tr>";
                            str += "</table> <br/>";
                        }
                        else
                        {    
                            panVchFinal.Visible = false;
                            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No record found. Please enter correct Voucher No along with Voucher Type and Year Month.')</script>");
                        }
                    }

                      
                }
            }
            else
            {
                DataSet ds = DBHandler.GetResults("Get_GLSLVoucherReport", VoucherNo, Convert.ToInt32(VoucherType), Year_Month, Session[SiteConstants.SSN_SECTOR_ID].ToString(), EmpID, VchAmount, Narration);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    YearMonth = ds.Tables[0].Rows[0]["YEAR_MONTH"].ToString();
                    VchType = ds.Tables[0].Rows[0]["VCH_TYPE"].ToString();
                    VchDate = ds.Tables[0].Rows[0]["VCH_DATE"].ToString();
                    VchNo = ds.Tables[0].Rows[0]["VoucherNo"].ToString();
                    VchStatus = ds.Tables[0].Rows[0]["STATUS"].ToString();
                    string DRCR = "";
                    decimal DrAmt = 0;
                    decimal CrAmt = 0;
                    decimal SumDrAmt = 0;
                    decimal SumCrAmt = 0;

                    
                    
                    //str += "<table width='80%' align='center'>";  style="width:auto"
                    //str += "<tr>";
                    //str += "<td style='font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;'>Year Month : <font style='font-family: Segoe UI; font-size: 12px; color: Navy;'>" + YearMonth + "</font></td>";
                    //str += "</tr>";

                    //str += "<table width='80%' align='center'>";
                    //str += "<tr>";
                    //str += "<td style='font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;'>Voucher Type : <font style='font-family: Segoe UI; font-size: 12px; color: Navy;'>" + VchType + "</font></td>";
                    //str += "</tr>";

                    str += "<table width='100%' style='margin-left: 65px; border-collapse: collapse;' align='center'>";
                    str += "<tr>";
                    str += "<td style='font-family: Segoe UI; font-size: 16px; font-weight: bold; color: Navy; width:auto;'>Voucher No : <font style='font-family: Segoe UI; font-size: 16px; color: Navy;'>" + VchNo + "</font></td>";
                    str += "<td style='font-family: Segoe UI; font-size: 16px; font-weight: bold; color: Navy; padding-left: 130px; width:auto;'>Voucher Status : <font style='font-family: Segoe UI; font-size: 16px; color: Navy;'>" + VchStatus + "</font></td>";
                    str += "<td style='font-family: Segoe UI; font-size: 16px; font-weight: bold; color: Navy; padding-left: 40px; width:auto;'>Voucher Date : <font style='font-family: Segoe UI; font-size: 16px; color: Navy;'>" + VchDate + "</font></td>";
                    str += "</tr>";

                    //str += "<table width='80%' align='center'>";
                    //str += "<tr>";
                    //str += "<td style='font-family: Segoe UI; font-size: 14px; font-weight: bold; color: Navy; padding-left: 170px;'>Voucher No : <font style='font-family: Segoe UI; font-size: 12px; color: Navy;'>" + VchNo + "</font></td>";
                    //str += "</tr>";

                    //str += "</br>";              

                    str += "<table id='tabDetail' cellpadding='3' width='100%' style='margin-left: 65px; border-collapse: collapse;' align='center'>";
                    str += "<tr>";
                    str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>GL_type</th>";
                    str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>GL_ID</th>";
                    str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>SL_ID</th>";
                    str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>YearMonth</th>";
                    str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>VchNo</th>";
                    str += "<th class='TableHeader, hiddencol' style='text-align: center; width: 10%; border: 1px solid white;'>SectorID</th>";

                    str += "<th class='TableHeader' style='text-align: center; width: 10%; border: 1px solid white;'>Code</th>";
                    str += "<th class='TableHeader' style='text-align: center; width: 10%; border: 1px solid white;'>Sub ID</th>";
                    str += "<th class='TableHeader' style='text-align: center; width: 10%; border: 1px solid white;'>Sub Name</th>";
                    str += "<th class='TableHeader' style='text-align: center; width: 45%; border: 1px solid white;'>Particulars</th>";
                    str += "<th class='TableHeader' style='text-align: center; width: 9%; border: 1px solid white;'>Dr/Cr</th>";
                    str += "<th class='TableHeader' style='text-align: center; width: 15%; border: 1px solid white;'>Debit</th>";
                    str += "<th class='TableHeader' style='text-align: center; width: 15%; border: 1px solid white;'>Credit</th>";
                    str += "</tr>";
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        for (int k = 0; k <= ds.Tables[1].Rows.Count - 1; k++)
                        {
                            string Amt = Convert.ToString(Math.Round(SumDrAmt, 2));

                            //Rsinword = changeToWords(Amt);
                            Rsinword = AmountInWords(Convert.ToDecimal(Amt));
                            DRCR = ds.Tables[1].Rows[k]["DR_CR"].ToString();
                            DrAmt = (DRCR == "Debit") ? Convert.ToDecimal(ds.Tables[1].Rows[k]["AMOUNT"].ToString()) : Convert.ToDecimal("0.00");
                            SumDrAmt += DrAmt;
                            CrAmt = (DRCR == "Credit") ? Convert.ToDecimal(ds.Tables[1].Rows[k]["AMOUNT"].ToString()) : Convert.ToDecimal("0.00");
                            SumCrAmt += CrAmt;
                            str += "<tr>";
                            str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["GL_TYPE"].ToString() + "</td>";
                            str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["GL_ID"].ToString() + "</td>";
                            str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["SL_ID"].ToString() + "</td>";
                            str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["YEAR_MONTH"].ToString() + "</td>";
                            str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["VCH_NO"].ToString() + "</td>";
                            str += "<td class='labelCaption, hiddencol' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["SectorID"].ToString() + "</td>";

                            str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["OLD_SL_ID"].ToString() + "</td>";
                            str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["OLD_SUB_ID"].ToString() + "</td>";
                            //str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["SUB_NAME"].ToString() + "</td>";
                            str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'><div onClick='FetchData(this);'><span style='cursor:pointer'>" + ds.Tables[1].Rows[k]["SUB_NAME"].ToString() + "</span></td>";
                            str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'><div onClick='FetchData(this);'><span style='cursor:pointer'>" + ds.Tables[1].Rows[k]["GL_NAME"].ToString() + "</span></td>";
                            str += "<td class='labelCaption' style='text-align: left; width: 9%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["DR_CR"].ToString() + "</td>";
                            str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy;'> " + Math.Round(DrAmt, 2) + "</td>";
                            str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy;'>" + Math.Round(CrAmt, 2) + "</td>";
                            str += "</tr>";
                        }
                        str += "<tr id='summary'>";
                        str += "<td colspan='4' class='labelCaption' style='text-align: justify; width: 9%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'> <span ID='lbltext' runat='server'></span></td>";
                        str += "<td class='labelCaption' style='text-align: right; width: 9%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>Total :- </td>";
                        str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>" + Math.Round(SumDrAmt, 2) + " </td>";
                        str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>" + Math.Round(SumCrAmt, 2) + "</td>";
                        str += "</tr>";
                        str += "<tr>";
                        str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>Narration : </td>";
                        str += "<td colspan='6' class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'> " + ds.Tables[0].Rows[0]["NARRATION"].ToString() + "</td>";
                        str += "</tr>";

                        str += "<tr>";
                        str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>Amount in Words : </td>";
                        str += "<td colspan='6' class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'> " + Rsinword + "</td>";
                        str += "</tr>";
                    }
                    str += "<tr>";
                    str += "<td colspan='9' class='labelCaption'>";
                    str += "<hr class='borderStyle' />";
                    str += "</td>";
                    str += "</tr>";
                    str += "</table> <br/>";
                }
                else
                {
                    panVchFinal.Visible = false;
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No record found. Please enter correct Voucher No along with Voucher Type and Year Month.')</script>");
                }
            }
            
            
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    //================== New Amounts in words Start =================//
    public string AmountInWords(decimal Num)
    {
        string returnValue;
        //I have created this function for converting amount in indian rupees (INR).
        //You can manipulate as you wish like decimal setting, Doller (any currency) Prefix.


        string strNum;
        string strNumDec;
        string StrWord;
        strNum = Num.ToString();


        if (strNum.IndexOf(".") + 1 != 0)
        {
            strNumDec = strNum.Substring(strNum.IndexOf(".") + 2 - 1);


            if (strNumDec.Length == 1)
            {
                strNumDec = strNumDec + "0";
            }
            if (strNumDec.Length > 2)
            {
                strNumDec = strNumDec.Substring(0, 2);
            }


            strNum = strNum.Substring(0, strNum.IndexOf(".") + 0);
            if (strNum == "0")
            {
                StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : "") + NumToWord((decimal)(double.Parse(strNum))) + ((double.Parse(strNumDec) > 0) ? (cWord3((decimal)(double.Parse(strNumDec))) + " Paise") : "");
            }
            else
            { 
                StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : "") + (NumToWord((decimal)(double.Parse(strNum))) + " Rupees ") + ((double.Parse(strNumDec) > 0) ? (" Rupees And " + cWord3((decimal)(double.Parse(strNumDec))) + " Paise") : "");
                //StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : " Rupees ") + NumToWord((decimal)(double.Parse(strNum))) + ((double.Parse(strNumDec) > 0) ? (" Rupees And " + cWord3((decimal)(double.Parse(strNumDec))) + " Paise") : "");
            }
        }
        else
        {
            StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : "") + (NumToWord((decimal)(double.Parse(strNum))) + " Rupees ");
            //StrWord = ((double.Parse(strNum) == 1) ? " Rupee " : " Rupees ") + NumToWord((decimal)(double.Parse(strNum)));
        }
        returnValue = StrWord + " Only /-";
        return returnValue;
    }
    static public string NumToWord(decimal Num)
    {
        string returnValue;


        //I divided this function in two part.
        //1. Three or less digit number.
        //2. more than three digit number.
        string strNum;
        string StrWord;
        strNum = Num.ToString();


        if (strNum.Length <= 3)
        {
            StrWord = cWord3((decimal)(double.Parse(strNum)));
        }
        else
        {
            StrWord = cWordG3((decimal)(double.Parse(strNum.Substring(0, strNum.Length - 3)))) + " " + cWord3((decimal)(double.Parse(strNum.Substring(strNum.Length - 2 - 1))));
        }
        returnValue = StrWord;
        return returnValue;
    }
    static public string cWordG3(decimal Num)
    {
        string returnValue;
        //2. more than three digit number.
        string strNum = "";
        string StrWord = "";
        string readNum = "";
        strNum = Num.ToString();
        if (strNum.Length % 2 != 0)
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 1)));
            if (readNum != "0")
            {
                StrWord = retWord(decimal.Parse(readNum));
                readNum = System.Convert.ToString(double.Parse("1" + strReplicate("0", strNum.Length - 1) + "000"));
                StrWord = StrWord + " " + retWord(decimal.Parse(readNum));
            }
            strNum = strNum.Substring(1);
        }
        while (!System.Convert.ToBoolean(strNum.Length == 0))
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 2)));
            if (readNum != "0")
            {
                StrWord = StrWord + " " + cWord3(decimal.Parse(readNum));
                readNum = System.Convert.ToString(double.Parse("1" + strReplicate("0", strNum.Length - 2) + "000"));
                StrWord = StrWord + " " + retWord(decimal.Parse(readNum));
            }
            strNum = strNum.Substring(2);
        }
        returnValue = StrWord;
        return returnValue;
    }
    static public string cWord3(decimal Num)
    {
        string returnValue;
        //1. Three or less digit number.
        string strNum = "";
        string StrWord = "";
        string readNum = "";
        if (Num < 0)
        {
            Num = Num * -1;
        }
        strNum = Num.ToString();


        if (strNum.Length == 3)
        {
            readNum = System.Convert.ToString(double.Parse(strNum.Substring(0, 1)));
            StrWord = retWord(decimal.Parse(readNum)) + " Hundred";
            strNum = strNum.Substring(1, strNum.Length - 1);
        }


        if (strNum.Length <= 2)
        {
            if (double.Parse(strNum) >= 0 && double.Parse(strNum) <= 20)
            {
                StrWord = StrWord + " " + retWord((decimal)(double.Parse(strNum)));
            }
            else
            {
                StrWord = StrWord + " " + retWord((decimal)(System.Convert.ToDouble(strNum.Substring(0, 1) + "0"))) + " " + retWord((decimal)(double.Parse(strNum.Substring(1, 1))));
            }
        }


        strNum = Num.ToString();
        returnValue = StrWord;
        return returnValue;
    }
    static public string retWord(decimal Num)
    {
        string returnValue;
        //This two dimensional array store the primary word convertion of number.
        returnValue = "";
        object[,] ArrWordList = new object[,] { { 0, "" }, { 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" }, { 6, "Six" }, { 7, "Seven" }, { 8, "Eight" }, { 9, "Nine" }, { 10, "Ten" }, { 11, "Eleven" }, { 12, "Twelve" }, { 13, "Thirteen" }, { 14, "Fourteen" }, { 15, "Fifteen" }, { 16, "Sixteen" }, { 17, "Seventeen" }, { 18, "Eighteen" }, { 19, "Nineteen" }, { 20, "Twenty" }, { 30, "Thirty" }, { 40, "Forty" }, { 50, "Fifty" }, { 60, "Sixty" }, { 70, "Seventy" }, { 80, "Eighty" }, { 90, "Ninety" }, { 100, "Hundred" }, { 1000, "Thousand" }, { 100000, "Lakh" }, { 10000000, "Crore" } };


        int i;
        for (i = 0; i <= (ArrWordList.Length - 1); i++)
        {
            if (Num == System.Convert.ToDecimal(ArrWordList[i, 0]))
            {
                returnValue = (string)(ArrWordList[i, 1]);
                break;
            }
        }
        return returnValue;
    }
    static public string strReplicate(string str, int intD)
    {
        string returnValue;
        //This fucntion padded "0" after the number to evaluate hundred, thousand and on....
        //using this function you can replicate any Charactor with given string.
        int i;
        returnValue = "";
        for (i = 1; i <= intD; i++)
        {
            returnValue = returnValue + str;
        }
        return returnValue;
    }
    //================== New Amounts in Words End ====================//

    public String changeToWords(String numb)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = (" Only.");
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    //int p = points.Length;
                    //char[] TPot = points.ToCharArray();
                    andStr = (" and ");// just to separate whole numbers from points/Rupees                   
                    //for(int i=0;i<p;i++)
                    //{
                    //    andStr += ones(Convert.ToString(TPot[i]))+" ";
                    //}
                    andStr += translateWholeNumber(points).Trim() + " Paise";

                }
            }
            val = String.Format("{0} {1}{2} {3}", translateWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch
        {
            ;
        }
        return val;
    }

    private String translateWholeNumber(String number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX
            bool isDone = false;//test if already translated
            double dblAmt = (Convert.ToDouble(number));
            //if ((dblAmt > 0) && number.StartsWith("0"))

            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric
                beginsZero = number.StartsWith("0");
                int numDigits = number.Length;
                int pos = 0;//store digit grouping
                String place = "";//digit grouping name:hundres,thousand,etc...
                switch (numDigits)
                {
                    case 1://ones' range
                        word = ones(number);
                        isDone = true;
                        break;
                    case 2://tens' range
                        word = tens(number);
                        isDone = true;
                        break;
                    case 3://hundreds' range
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range
                    case 5:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 6:

                    case 7://millions' range
                        pos = (numDigits % 6) + 1;
                        // place = " Million ";
                        place = " Lakh ";
                        break;
                    case 8:
                    case 9:

                    case 10://Billions's range
                        pos = (numDigits % 8) + 1;
                        place = " Core ";
                        break;
                    //add extra case options for anything above Billion...
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!)
                    if (beginsZero) place = "";
                    word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos));
                    //check for trailing zeros
                    if (beginsZero) word = " and " + word.Trim();
                }
                //ignore digit grouping names
                if (word.Trim().Equals(place.Trim())) word = "";
            }
        }
        catch
        {
            ;
        }
        return word.Trim();
    }

    private String tens(String digit)
    {
        int digt = Convert.ToInt32(digit);
        String name = null;
        switch (digt)
        {
            case 10:
                name = "Ten";
                break;
            case 11:
                name = "Eleven";
                break;
            case 12:
                name = "Twelve";
                break;
            case 13:
                name = "Thirteen";
                break;
            case 14:
                name = "Fourteen";
                break;
            case 15:
                name = "Fifteen";
                break;
            case 16:
                name = "Sixteen";
                break;
            case 17:
                name = "Seventeen";
                break;
            case 18:
                name = "Eighteen";
                break;
            case 19:
                name = "Nineteen";
                break;
            case 20:
                name = "Twenty";
                break;
            case 30:
                name = "Thirty";
                break;
            case 40:
                name = "Fourty";
                break;
            case 50:
                name = "Fifty";
                break;
            case 60:
                name = "Sixty";
                break;
            case 70:
                name = "Seventy";
                break;
            case 80:
                name = "Eighty";
                break;
            case 90:
                name = "Ninety";
                break;
            default:
                if (digt > 0)
                {
                    name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1));
                }
                break;
        }
        return name;
    }

    private String ones(String digit)
    {
        Int64 digt = Convert.ToInt64(digit);
        String name = "";
        switch (digt)
        {
            case 1:
                name = "One";
                break;
            case 2:
                name = "Two";
                break;
            case 3:
                name = "Three";
                break;
            case 4:
                name = "Four";
                break;
            case 5:
                name = "Five";
                break;
            case 6:
                name = "Six";
                break;
            case 7:
                name = "Seven";
                break;
            case 8:
                name = "Eight";
                break;
            case 9:
                name = "Nine";
                break;
        }
        return name;
    }

    protected void cmdCancelSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
          
    [WebMethod]
    public static FetchValueCount[] FetchDetailValue(string GL_TYPE, string GL_ID, string SL_ID, string YEAR_MONTH, string VCH_NO, string SectorID)
    {
        string GLType = "";
        GLType = GL_TYPE;
        List<FetchValueCount> Detail = new List<FetchValueCount>();
        DataTable dtGetData = DBHandler.GetResult("Get_VoucherReportText", GL_TYPE, GL_ID, SL_ID, YEAR_MONTH, VCH_NO, SectorID);
        for (int i = 0; i < dtGetData.Rows.Count; i++)
        {
            FetchValueCount DataObj = new FetchValueCount();
            DataObj.DetailTest = dtGetData.Rows[i]["desc_dtl"].ToString();
            if (GLType == "A" || GLType == "B")
            {
                DataObj.DetailTest1 = dtGetData.Rows[i]["InstNo"].ToString();
                DataObj.DetailTest2 = dtGetData.Rows[i]["InstDate"].ToString();
                DataObj.DetailTest3 = dtGetData.Rows[i]["Amount"].ToString();
                DataObj.DetailTest4 = dtGetData.Rows[i]["Bank"].ToString();
            }

            Detail.Add(DataObj);
            
        }
        return Detail.ToArray();
    }
    public class FetchValueCount
    {
        public string DetailTest { get; set; }
        public string DetailTest1 { get; set; }
        public string DetailTest2 { get; set; }
        public string DetailTest3 { get; set; }
        public string DetailTest4 { get; set; }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string YearMonth, string VchType, string VchNoFrom, string VchNoTo, string VchFromDate, string VchToDate)
    {
        string JSONVal = "";

        try
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            String Msg = "";
            StrFormula = "";
            int UserID;
            int SectorID;
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
            StrFormula = "";

            if (ReportName == "Voucher Type")
            {
                StrFormula = "{DAT_Acc_Voucher.YEAR_MONTH}='" + YearMonth + "' and {MST_Sector.SectorID}=" + SectorID;
            }
             
            if (ReportName == "Voucher Type Detail")
            {
                StrFormula = "{MST_Acc_Voucher.YEAR_MONTH}='" + YearMonth + "' and {MST_Sector.SectorID}=" + SectorID;
            }

            if (VchFromDate != "" && VchToDate != "" && ReportName=="Voucher Type")
            {
                StrFormula = StrFormula + " And {MST_Acc_Voucher.VCH_DATE} >= cdate('" + VchFromDate + "') and {MST_Acc_Voucher.VCH_DATE} <= cdate('" + VchToDate + "')";
            }
            if (VchFromDate != "" && VchToDate != "" && ReportName == "Voucher Type Detail")
            {
                StrFormula = StrFormula + " And {MST_Acc_Voucher.VCH_DATE} >= cdate('" + VchFromDate + "') and {MST_Acc_Voucher.VCH_DATE} <= cdate('" + VchToDate + "')";
            }
            if (VchFromDate != "" && VchToDate != "" && (ReportName == "VOUCHER 8x6" || ReportName == "VOUCHER"))
            {
                StrFormula = StrFormula + " cdate({Acc_Voucher_Prn;1.VCH_DATE}) >= cdate('" + VchFromDate + "') and cdate({Acc_Voucher_Prn;1.VCH_DATE}) <= cdate('" + VchToDate + "')";
            }
            if (VchFromDate != "" && VchToDate != "" && ReportName == "eadvice")
            {
                StrFormula = StrFormula + " cdate({Acc_EAdvise_Prn;1.DT}) >= cdate('" + VchFromDate + "') and cdate({Acc_EAdvise_Prn;1.DT}) <= cdate('" + VchToDate + "')";
            }
            

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            if (VchNoFrom != "")
            {
                DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, VchType, VchNoFrom, VchNoTo, SectorID, "", "", "", "", "");
            }
            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

    //[WebMethod(EnableSession = true)]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //public static GLSL[] GET_DisGlSl(string GlSlDesc, string Type)
    //{
    //    List<GLSL> glsldet = new List<GLSL>();
    //    try
    //    {

    //        DataTable dtGlSl = DBHandler.GetResult("Get_Acc_VchGLSL", Type, GlSlDesc, HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
    //        foreach (DataRow row in dtGlSl.Rows)
    //        {
    //            GLSL glsl = new GLSL();
    //            glsl.GL_ID = row["GL_ID"].ToString();
    //            glsl.OLD_SL_ID = row["OLD_SL_ID"].ToString();
    //            glsl.GL_NAME = row["GL_NAME"].ToString();
    //            glsl.GL_TYPE = row["GL_TYPE"].ToString();  
    //            glsl.SL_ID = row["SL_ID"].ToString();
    //            glsl.SUBID = row["SUBID"].ToString();
    //            glsl.SUBTYPE = row["SUBTYPE"].ToString();
    //            glsldet.Add(glsl);

    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //    return glsldet.ToArray();
    //}

    [WebMethod]
    public static string[] AccGLSlAutoCompleteData(string GlSlDesc, string Type)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Get_Acc_VchGLSL", Type, GlSlDesc, HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", '(' + dtRow["OLD_SL_ID"].ToString() + ')' + dtRow["GL_NAME"].ToString(), dtRow["SL_ID"].ToString()));
            //result.Add(string.Format("{0}|{1}", '(' + dtRow["OLD_SL_ID"].ToString() + ')' + dtRow["GL_NAME"].ToString(), dtRow["GL_ID"].ToString()));
        }
        return result.ToArray();
    }

}

















