﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always"  CodeFile="ACC_Sl_Sub.aspx.cs" Inherits="ACC_Sl_Sub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript">
    $(document).ready(function () {
        $("input").focus(function () {
            $(this).css("background-color", "#cccccc");
        });
        $("input").blur(function () {
            $(this).css("background-color", "#ffffff");
        });

        $("select").focus(function () {
            $(this).css("background-color", "#cccccc");
        });

        $("select").blur(function () {
            $(this).css("background-color", "#ffffff");
        });


        if (document.getElementById("txtOldSlSub").disabled == false) {
            $('#txtOldSlSub').focus();
        }

        else {
            $("#txtSubName").focus();
        }

        
        
        $('#txtOldSlSub').attr({ maxLength: 10 });
        $('#txtSubName').attr({ maxLength: 50 });
        $('#txtSubType').attr({ maxLength: 1 });
        $('#txtSubType').keypress(function (evt) {
            var key = (evt.which) ? evt.which : evt.keyCode
            if (!((key == 8) || (key == 32) || (key == 46) || (key == 13) || (key == 80) || (key == 73) || (key == 79) || (key == 89) || (key == 78) ||
                                                              (key == 112) || (key == 105) || (key == 111) || (key == 121) || (key == 110))) {
                //evt.preventDefault();
                alert('Only accept P, I, O, Y, N');
                return false;
            }            
        });

        $('#txtOldSlSub').keypress(function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 13) {
                if ($('#txtOldSlSub').val().trim() == '') {
                    alert('Please Type Sub ID');
                    $('#txtOldSlSub').focus();
                    return false;
                }
                else {
                    DuplicateCheckOldSubID();
                    return false;
                }
            }
        });


        $('#txtOldSlSub').keydown(function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 9) {
                if ($('#txtOldSlSub').val().trim() == '') {
                    alert('Please Type Sub ID');
                    $('#txtOldSlSub').focus();
                    return false;
                }
                else {
                    DuplicateCheckOldSubID();
                    return false;
                }
            }
        });

        $('#txtSubName').keypress(function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 13) {
                $('#txtSubType').focus();
                return false;
            }

        });

        $('#txtSubType').keypress(function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode == 13) {
                $('#cmdSave').focus();
                return false;
            }
        });
        
    });

    function DuplicateCheckOldSubID() {
        var SubID = '';
        if ($("#txtOldSlSub").val().trim() == '') {
            return false;
        }
        $(".loading-overlay").show();
        if ($("#hdnSubID").val() == '') {
            SubID = '0';
        }
        else { SubID = $("#hdnSubID").val(); }
        var W = "{SubID:'" + SubID + "',OldSubID:'" + $("#txtOldSlSub").val() + "'}";
        $.ajax({
            type: "POST",
            url: "ACC_Sl_Sub.aspx/GET_DupChkOldSubID",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                var GroupID = jQuery.parseJSON(data.d);
                if (GroupID != '') {
                    alert('Sub ID Already Exists!');
                    $(".loading-overlay").hide();
                    $("#txtOldSlSub").val('');
                    //$("#txtOldSlSub").select();
                    $("#txtOldSlSub").focus();
                    return false;
                }
                else { $(".loading-overlay").hide(); $('#txtSubName').focus(); return false; }

            },
        });
    }

    function beforeSave() {
        $("#frmEcom").validate();
        
        $("#txtOldSlSub").rules("add", { required: true, messages: { required: "Please enter Old SL Sub ID" } });
        $("#txtSubName").rules("add", { required: true, messages: { required: "Please enter Sub Name" } });
        $("#txtSubType").rules("add", { required: true, messages: { required: "Please enter Sub Type" } });
        
    }

    function Delete(id) {
        if (confirm("Are You sure you want to delete?")) {
            $("#frmEcom").validate().currentForm = '';
            return true;
        } else {
            return false;
        }
    }

    function unvalidate() {
        $("#frmEcom").validate().currentForm = '';
        return true;
    }
</script>
    <%--<style type="text/css">
        input { 
    text-transform: uppercase;
}
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">

     <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>
   <%-- Duplicate Value check for Data Save--%>
    <asp:TextBox ID="hdnSubID" Width="100" ClientIDMode="Static" runat="server" style="display:none"></asp:TextBox>

    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Loan Sub Ledger </td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    
                                    <td class="labelCaption">Sub ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtOldSlSub" style="text-transform:uppercase" Width="100px" ClientIDMode="Static" runat="server" autocomplete="off" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                    <td class="labelCaption">Sub Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSubName" style="text-transform:uppercase" Width="400px" ClientIDMode="Static" runat="server" autocomplete="off" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td class="labelCaption">Sub Type &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSubType" style="text-transform:uppercase" Width="100px" ClientIDMode="Static" runat="server" autocomplete="off" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>  
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>                                     
                                    <td class="labelCaption">Sub ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtOldSlSub" Enabled="false" autocomplete="off" style="text-transform:uppercase" Width="100px" ClientIDMode="Static" Text='<%# Eval("OLD_SUB_ID") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Sub Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSubName" style="text-transform:uppercase" autocomplete="off" Width="400px" ClientIDMode="Static" Text='<%# Eval("SUB_NAME") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="labelCaption">Sub Type &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSubType" style="text-transform:uppercase" autocomplete="off" Width="100px" ClientIDMode="Static" Text='<%# Eval("SUB_TYPE") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                    
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" autocomplete="off"
                            AutoGenerateColumns="false" DataKeyNames="SUB_ID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            autocomplete="off" runat="server" ID="btnEdit" CommandArgument='<%# Eval("SUB_ID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" autocomplete="off"
                                    OnClientClick='<%#"return Delete("+(Eval("SUB_ID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("SUB_ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                                
                                <asp:BoundField DataField="SUB_ID" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Labor Type" /> 
                                <asp:BoundField DataField="OLD_SUB_ID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sub ID" />
                                <asp:BoundField DataField="SUB_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sub Name" />
                                <asp:BoundField DataField="SUB_TYPE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sub Type" />                               
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>