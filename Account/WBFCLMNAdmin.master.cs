﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Text;
using System.IO;

namespace WBFCACC
{

    public partial class WBFCLMNAdmin : System.Web.UI.MasterPage
    {
        DataTable dtMenu;
        string[] tags = { "__VIEWSTATE", "__EVENTTARGET", "__EVENTARGUMENT", "__EVENTVALIDATION" };
        //protected override void Render(HtmlTextWriter writer)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    HtmlTextWriter htw = new HtmlTextWriter(new StringWriter(sb));
        //    base.Render(htw);
        //    string html = sb.ToString();
        //    foreach (string tag in tags)
        //        html = this.RemoveTag(tag, html);
        //    writer.Write(html);
        //}
        //public string RemoveTag(string tag, string html)
        //{
        //    int lowerBound = html.IndexOf(tag);
        //    if (lowerBound < 0) return html;
        //    while (html[lowerBound--] != '<') ;
        //    int upperBound = html.IndexOf("/>", lowerBound) + 2;
        //    html = html.Remove(lowerBound, upperBound - lowerBound);
        //    if (html.Contains(tag)) html = this.RemoveTag(tag, html);
        //    return html;
        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session[SiteConstants.SSN_INT_USER_ID] == null)
                {
                    Response.Redirect("default.aspx", false);
                    return;
                }
                if (!IsPostBack)
                {
                    //Response.Write(Session[SiteConstants.SSN_INT_USER_ID]);
                    DataRow drRow = (DataRow)(Session[SiteConstants.SSN_DR_USER_DETAILS]);
                    lblLoginName.Text = "Welcome " + drRow["Name"].ToString()  + " ";
                    //int MerchantID=Convert.ToInt32(drRow["MerchantID"].ToString());
                    //DataTable dtMerchantDetails = DBHandler.GetResult("Get_MerchantDetailsByID", MerchantID);
                    //if (dtMerchantDetails.Rows.Count > 0)
                    //{
                    //    headerLogo.Style.Add("background", "url("+dtMerchantDetails.Rows[0]["CompanyLogoFileName"].ToString()+") no-repeat;");
                    //    headerLogo.InnerHtml = dtMerchantDetails.Rows[0]["DisplayName"].ToString();
                    //}
                    propulateTreeView();
                    PopulateUserSector();
                    
                    
                   // CreateHiddenFileds();
                    

                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
            }
            
            //if (Session["TreeViewState"] != null)
            //    treeView.ExpandDepth = -1;
        }

        private void PopulateUserSector()
        {
            try
            {
                DataTable dtUserSector = DBHandler.GetResult("Get_UserWiseSector",Session[SiteConstants.SSN_INT_USER_ID]);
                ddlUserSector.DataSource = dtUserSector;
                ddlUserSector.DataValueField = "SectorID";
                ddlUserSector.DataTextField = "SectorName";
                ddlUserSector.DataBind();
                if (Session[SiteConstants.SSN_SECTOR_ID] == null)
                {
                    if (dtUserSector.Rows.Count == 1)
                    {
                        ddlUserSector.SelectedIndex = 1;
                        Session[SiteConstants.SSN_SECTOR_ID] = ddlUserSector.SelectedValue;
                    }
                }
                else
                {
                    ddlUserSector.SelectedValue = Session[SiteConstants.SSN_SECTOR_ID].ToString();
                }
                
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
            }
        }
        

        //private void CreateHiddenFileds()
        //{
        //    try
        //    {
        //        if (Session[SiteConstants.SSN_BOOKING_PARAMS]!=null)
        //        {
        //            DataTable dt=(DataTable)Session[SiteConstants.SSN_BOOKING_PARAMS];
        //            StringBuilder sb = new StringBuilder("");
        //            foreach (DataColumn dc in dt.Columns)
        //            {
        //                sb.AppendLine("<input type='hidden' id='" + dc.ColumnName + "' name='" + dc.ColumnName + "' value='" + dt.Rows[0][dc.ColumnName] + "'/>");
                        
        //            }
        //            lt1.Text = sb.ToString();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Page.RegisterStartupScript( "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        //    }
        //}

        private void propulateTreeView()
        {
            
            dtMenu = DBHandler.GetResult("Get_UserMenu",Session[SiteConstants.SSN_INT_USER_ID],Session[SiteConstants.SSN_MODULE]);
            DataTable objt = new DataTable();
            objt = GetData(0);
            foreach (DataRow dr in objt.Rows)
            {
                TreeNode node = new TreeNode();
                node.Text = dr["MenuName"].ToString() ;
                node.Value = dr["MenuID"].ToString();
                node.NavigateUrl = dr["PageName"].ToString();
                AddNodes(node);
                treeView.Nodes.Add(node);
            }
            
        }
        protected void AddNodes(TreeNode node)
        {
            DataTable dt = new DataTable();
            dt = GetData(Convert.ToInt32(node.Value));
            foreach (DataRow row in dt.Rows)
            {
                TreeNode nNode = new TreeNode();
                nNode.Value = row["MenuID"].ToString();
                nNode.Text = row["MenuName"].ToString();
                nNode.NavigateUrl = row["PageName"].ToString();
                AddNodes(nNode);
                node.ChildNodes.Add(nNode);
            }

        }
        protected DataTable GetData(int intPID)
        {
            DataTable objT = new DataTable();
            objT.Columns.Add("MenuID");
            objT.Columns.Add("MenuName");
            objT.Columns.Add("ParentMenuID");
            objT.Columns.Add("PageName");
            

            foreach (DataRow dr in dtMenu.Rows)
            {
                if (dr[2].ToString() != intPID.ToString())
                {
                    continue;
                }
                DataRow r = objT.NewRow();
                r[0] = dr[0];
                r[1] = dr[1];
                r[2] = dr[2];
                r[3] = dr[3];
                
                objT.Rows.Add(r);
            }

            return objT;
        }
        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                //DataRow drRow = (DataRow)(Session[SiteConstants.SSN_DR_USER_DETAILS]);
                //DBHandler.Execute("Get_Logout", drRow["UserID"]);
                Session[SiteConstants.SSN_INT_USER_ID] = null;
                Session[SiteConstants.SSN_DR_USER_DETAILS] = null;
                Session.RemoveAll();
                Session.Abandon();
                if (Session[SiteConstants.SSN_INT_USER_ID]==null)
                {
                    Response.Redirect("http://localhost:28883/Administration/AdminHome/SelectModule", false);
                }
            }
            catch (Exception ex)
            {
                Page.RegisterStartupScript("Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
            }
        }
        protected void ddlUserSector_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlUserSector.SelectedIndex>0)
                {
                    
                    Session[SiteConstants.SSN_SECTOR_ID] = ddlUserSector.SelectedValue;
                    Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
                }
                else
                {
                    ddlUserSector.SelectedValue = Session[SiteConstants.SSN_SECTOR_ID].ToString();
                    Page.RegisterStartupScript("Error", "<script>alert('Select Proper Unit')</script>");
                }
            }
            
            catch (Exception ex)
            {
                Page.RegisterStartupScript("Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
            }
        }
}
}
