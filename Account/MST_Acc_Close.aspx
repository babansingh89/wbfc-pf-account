﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MST_Acc_Close.aspx.cs" Inherits="MST_Acc_Close" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('#TxtYearMonth').attr({ maxlength: 6 });
            $("#TxtYearMonth").attr('minlength', '6');
        });
                    
        function beforeSave() {
            $("#frmEcom").validate();

            $("#TxtYearMonth").rules("add", { required: true, messages: { required: "Please enter year month" } });
            $("#ddlTransactionType").rules("add", { required: true, messages: { required: "Please select Transaction Type" } });
            $("#DdlTransactionStatus").rules("add", { required: true, messages: { required: "Please enter transaction status" } });
            $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please Select A unit" } });
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }

        $(document).ready(function () {
            $("#TxtYearMonth").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    alert("Please Enter numeric value");
                    return false;
                }
            });
        });
        $(document).ready(function () {
            $("#cmdSave").click(function (e) {
                Save();
            });
        });

        function Save() {
            if ($("#TxtYearMonth").val().trim() == '') { alert("Please enter YearMonth !"); $("#TxtYearMonth").focus(); return false; }
            if ($("#ddlTransactionType").val() == '') { alert("Please select Transaction Type !"); $("#ddlTransactionType").focus(); return false; }
            $(".loading-overlay").show();
            $.ajax({
                type: "POST",
                url: "MST_Acc_Close.aspx/Get_Save",
                contentType: "application/json;charset=utf-8",
                data: "{'TxtYearMonth':'" + $("#TxtYearMonth").val().trim() + "', 'ddlTransactionType':'" + $("#ddlTransactionType").val() + "', 'DdlTransactionStatus':'" + $("#DdlTransactionStatus").val() + "'}",
                dataType: "json",
                success: function (data) {
                    var strmsg = data.d;
                    window.location.href = "MST_Acc_Close.aspx";
                    alert(strmsg);
                    $(".loading-overlay").hide();
                },
                error: function (result) {
                    alert("Error Records Data...");
                    $(".loading-overlay").hide();
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">

    <div class="loading-overlay">
        <div class="loadwrapper">
            <div class="ajax-loader-outer">Loading...</div>
        </div>
    </div>

    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Account / Parameter  Closing Master</td>
            </tr>
        </table>
        <table></table>

        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Year Month &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtYearMonth" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2" PlaceHolder="YYYYMM" ToolTip="Please Enter at least 6 digit"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Transaction Type &nbsp;&nbsp<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlTransactionType" autocomplete="off" Width="209px" Height="25px" runat="server" AppendDataBoundItems="true" AutoPostBack="false">
                                            <asp:ListItem Text="(Select Trans Type)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="A">Account Opening</asp:ListItem>
                                            <asp:ListItem Value="L">Loan Opening</asp:ListItem>
                                            <asp:ListItem Value="I">Loan Interest Calculation</asp:ListItem>
                                             <asp:ListItem Value="H">Health Code Generation</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Transaction Status &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlTransactionStatus" autocomplete="off" Width="209px" Height="25px" runat="server" AppendDataBoundItems="true" AutoPostBack="true" Enabled="false">
                                            <asp:ListItem Value="0">Open</asp:ListItem>
                                            <asp:ListItem Value="1">Transfer</asp:ListItem>
                                            <asp:ListItem Value="2">Closed</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                        <%---------------------------------------------------------------------------------------------- END OF INSERT ITEM TEMPLATE---------------------------------------------------------------- --%>

                        <%-- <EditItemTemplate>
                                 <table align="center" cellpadding="5" width="100%">
                                     <tr>
                                         <td class="labelCaption">Year Month &nbsp;&nbsp;<span class="require">*</span></td>
                                         <td class="labelCaption">:</td>
                                         <td align="left">
                                             <asp:TextBox ID="TxtYearMonth" runat="server" ClientIDMode="Static" MaxLength="6" CssClass="inputbox2" PlaceHolder="YYYYMM"></asp:TextBox>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td class="labelCaption">Transaction Type &nbsp;&nbsp<span class="require">*</span></td>
                                         <td class="labelCaption">:</td>
                                         <td align="left">
                                          <asp:DropDownList ID="ddlTransactionType" Width="200px" runat="server" AppendDataBoundItems="true" AutoPostBack="true">
                                             <asp:ListItem Value="A">Account Opening</asp:ListItem>
                                             <asp:ListItem Value="L">Loan Opening</asp:ListItem>
                                              <asp:ListItem Value="I">Loan Interest Calculation</asp:ListItem>
                                         </asp:DropDownList>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td class="labelCaption">Transaction Status &nbsp;&nbsp;<span class="require">*</span></td>
                                         <td class="labelCaption">:</td>
                                         <td align="left">
                                              <asp:DropDownList ID="DdlTransactionStatus" Width="200px" runat="server" AppendDataBoundItems="true" AutoPostBack="true">
                                             <asp:ListItem Value="0">Open</asp:ListItem>
                                             <asp:ListItem Value="1">Transfer</asp:ListItem>
                                              <asp:ListItem Value="2">Closed</asp:ListItem>
                                         </asp:DropDownList>
                                         </td>
                                     </tr>
                                 </table>
                            </EditItemTemplate>--%>

                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()' />
                                                 
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 99%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="TRANS_STATUS, TRANS_TYPE, SECTORID" CellPadding="2" CellSpacing="2" OnRowCommand="tbl_RowCommand"  OnRowDataBound="tbl_OnRowDataBound" AllowPaging="false" Height="100px">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField HeaderText="Year Month">
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <div>
                                            <asp:TextBox ID="TxtyearMonth" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2" Text='<%# Eval("YEAR_MONTH") %>' Enabled="false"></asp:TextBox>
                                        </div>
                                    </ItemTemplate>  
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Transaction Type">
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlTransType" autocomplete="off" Enabled="false" runat="server" Width="209px" Height="25px" AppendDataBoundItems="true" AutoPostBack="false" Text='<%# Eval("TRANS_TYPE") %>'>
                                            <asp:ListItem Text="(Select Trans Type)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="A">Account Opening</asp:ListItem>
                                            <asp:ListItem Value="L">Loan Opening</asp:ListItem>
                                            <asp:ListItem Value="I">Loan Interest Calculation</asp:ListItem>
                                            <asp:ListItem Value="H">Health Code Generation</asp:ListItem>  
                                                                                 
                                        </asp:DropDownList>  
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Transaction Status">
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:DropDownList ID="DdlTransStatus" autocomplete="off" runat="server" Width="209px" Height="25px" AppendDataBoundItems="true" AutoPostBack="false" Text='<%# Eval("TRANS_STATUS") %>'>
                                            <asp:ListItem Value="0">Open</asp:ListItem>
                                            <asp:ListItem Value="1">Transfer</asp:ListItem>
                                            <asp:ListItem Value="2">Closed</asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>    
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:Button ID="cmdSave" runat="server" autocomplete="off"  CommandName="Addition" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                            Text="Save" CssClass="edit-address-button" ClientIDMode="Static" Width="100px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

