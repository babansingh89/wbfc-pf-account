﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_TDS.aspx.cs" Inherits="LMN_TDS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
              $(document).ready(function () {
            $("#txttdsrate").keypress(function (event) {
                if (event.which < 46
                || event.which > 59) {
                    alert("Please Enter Numeric Value");
                    return false;
                } // prevent if not number/dot

                if (event.which == 46
                && $(this).val().indexOf('.') != -1) {
                    alert("Please Enter Numeric Value");
                    return false;
                } // prevent if already dot
            });
            if ($("#txttdsid").val() == '') {
                var currentDate = new Date();
                $("#txtwef").datepicker("setDate", currentDate);
            }
        });

      

        $(document).ready(function () {
            $('#txtwef').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                onSelect: function (selected) {
                    $("#txtwef").datepicker("option", "minDate", selected)
                }
            });
        });

       
        $(document).ready(function () {

        });

        function beforeSave() {
            $("#frmEcom").validate();
            $("#txttdsid").rules("add", { required: true, messages: { required: "Please enter tds Id" } });
            $("#txttdsdesc").rules("add", { required: true, messages: { required: "Please enter Tds DESC " } });
            $("#txttdsrate").rules("add", { required: true, messages: { required: "Please enter Tds Rate" } });
            $("#txtwef").rules("add", { required: true, messages: { required: "Please enter WEF" } });
            $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please select Unit Name" } });

        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
</script>

   
    <style type="text/css">
        .auto-style1 {
            font-family: Segoe UI;
            font-size: 12px;
            color: Navy;
            text-align: left;
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>TDS MASTER</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                 <tr>
                                    <td class="labelCaption">Tds ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txttdsid" autocomplete="off" MaxLength="10" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                                <tr>
                                    <td class="labelCaption">Tds DESC &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txttdsdesc" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                                 <tr>
                                    <td class="labelCaption">Tds Rate &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txttdsrate" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                                
                                 <tr>
                                    <td class="labelCaption">WEF &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtwef" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                   </tr>
                                 </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Tds ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txttdsid" autocomplete="off" MaxLength="10" ReadOnly="true" ClientIDMode="Static" Text='<%# Eval("TDS_ID")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                                <tr>
                                    <td class="labelCaption">Tds DESC &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txttdsdesc" autocomplete="off" MaxLength="100" ClientIDMode="Static" Text='<%# Eval("TDS_DESC")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                       
                                    </td>
                                    </tr>
                                 <tr>
                                    <td class="labelCaption">Tds Rate &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txttdsrate" autocomplete="off" MaxLength="100" ClientIDMode="Static" Text='<%# Eval("TDS_RATE")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                               

                                 <tr>
                                    <td class="labelCaption">Wef &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:TextBox ID="txtwef" autocomplete="off" MaxLength="100" ClientIDMode="Static" Readonly="true" Text='<%# Eval("WEF")%>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                      
                                    </td>
                                   </tr>
                                 
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="auto-style1">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <div style="overflow-y: scroll;  width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="TDS_ID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false" >
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("TDS_ID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("TDS_ID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("TDS_ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>      
                                 <asp:BoundField DataField="TDS_ID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="TDS ID" />                          
                                <asp:BoundField DataField="TDS_DESC" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Tds DESC" />    
                                <asp:BoundField DataField="TDS_RATE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Tds Rate" />
                                <asp:BoundField DataField="WEF" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="WEF"  DataFormatString="{0:d}"/>
                                                           
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            </table>
    </div>
</asp:Content>

