﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MSTAccLoan.aspx.cs" Inherits="MST_Acc_Loan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/MstLmnLoan.js?v=5"></script>
  <%--  <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ddlPwrStatus").val('A');
            $('#txtApplicationAmt,#txtProjCost,#txtSanctioAmt,#txtSancanAmt,#txtMortPri,#txtHypoPri,#txtPlePri,#txtMortCol,#txtHypoCol,#txtPleCol').css('text-align', 'right');
            $("input[name='chkSearch']").click(function () {
                if ($('#A').is(":checked")) {
                    InsertMode = 'Add';   
                    $('#searchLoaneeCode').show();
                    $('#searchLoaneeName').hide();
                    ClearLoanDetail();
                    ClearLoanMaster();
                    $('#txtLoaneeSearch').focus();
                    $('#txtLoaneeSearch').val('');
                    //__doPostBack('__Page', '');
                } else {
                    InsertMode = 'Add';
                    $('#searchLoaneeCode').hide();
                    $('#searchLoaneeName').show();
                    ClearLoanDetail();
                    ClearLoanMaster();
                    $('#txtLoaneeNameSearch').focus();
                    $('#txtLoaneeNameSearch').val('');
                }
            });
        });

    </script>
    <style type="text/css">
        .hovertable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #999999;
            border-collapse: collapse;
        }

            .hovertable th {
                background-color: #c3dde0;
                border-width: 1px;
                padding: 4px;
                border-style: solid;
                border-color: #a9c6c9;
            }

            .hovertable tr {
                background-color: #c3dde0;
            }

                .hovertable tr:hover {
                    background-color: #d4e3e5;
                }

            .hovertable td {
                border-width: 1px;
                padding: 4px;
                border-style: solid;
                border-color: #a9c6c9;
            }

        .myButton1 {
            -moz-box-shadow: inset 0px 39px 0px -24px #6496e5;
            -webkit-box-shadow: inset 0px 39px 0px -24px #6496e5;
            box-shadow: inset 0px 39px 0px -24px #6496e5;
            background-color: #6496e5;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            border: 1px solid #6496e5;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: Arial;
            font-size: 20px;
            font-weight: bold;
            padding: 6px 15px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #b23e35;
        }

        .myButton:hover {
            background-color: #ffffff;
        }

        .myButton:active {
            position: relative;
            top: 1px;
        }
    </style>
    <script type="text/javascript">
        
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div class="loading-overlay">
        <div class="loadwrapper">
            <div class="ajax-loader-outer">
                Loading...
            </div>
        </div>
    </div>
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Loan Master</td>
            </tr>
        </table>
      

        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td align="left">Loan Master Search</td>
                             <td align="left" colspan="0">
                                <input type='radio' name='chkSearch' autocomplete="off" runat="server" clientidmode="Static" id='A'/><font size="2px">Search by Loanee Code</font>
                                <input type='radio' name='chkSearch' autocomplete="off" runat="server" clientidmode="Static" id='B' /><font size="2px">Search by Loanee Name</font>
                              
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="98%" align="center">
                    
                        <tr>
                            <td class="labelCaption">
                                <div id="searchLoaneeCode" style="width: 100%;">
                                    <asp:Label autocomplete="off" style="width: 95px" ID="lblLoaneeCode" CssClass="labelCaption" runat="server">Loanee Code&nbsp; : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:Label>
                                    <asp:TextBox ID="txtLoaneeSearch" PlaceHolder="---(Select Loanee Code)---" Width="500px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestLoaneeCode"></asp:TextBox>
                                </div>
                                <div id="searchLoaneeName" style="width: 100%; display: none;">
                                    <asp:Label style="width: 90px" ID="lblLoaneeName" CssClass="labelCaption" runat="server">Loanee Name&nbsp; : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:Label>
                                    <asp:TextBox ID="txtLoaneeNameSearch" autocomplete="off" PlaceHolder="---(Select Loanee Name)---" Width="500px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestLoaneeName"></asp:TextBox>
                                </div>
                            </td>
                            <td>
                                <asp:Button ID="btnLoanSearch" runat="server" Text="Search" CommandName="search" Height="30px" autocomplete="off"
                                    Width="80px" CssClass="save-button DefaultButton" />
                                <asp:Button ID="btnLoanSearchCancel" runat="server" Text="Refresh" CommandName="search" Height="30px" autocomplete="off"
                                    Width="80px" OnClick="cmdRefreshCancel_Click" CssClass="save-button" />
                                 <asp:Button ID="btnPrint" runat="server" Text="Print" CommandName="search" Height="30px" autocomplete="off"
                                    Width="80px" OnClick="cmdRefreshCancel_Click" CssClass="save-button" />
                                <asp:Button ID="btnRepaySchedule" runat="server" Text="Repayment Schedule" CommandName="Repayment Schedule" Height="30px" autocomplete="off"
                                    Width="150px" CssClass="save-button" />


                            </td>
                        </tr>
                       <%-- <tr>
                            <td colspan="9" class="labelCaption">
                                <hr class="borderStyle" />
                            </td>
                        </tr>--%>
                    </table>
                </td>
            </tr>
           

            <tr>
                <td colspan="12">
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td align="left">Loan Master Entry</td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td style="width: 95px" class="labelCaption">Loanee &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLoanee" autocomplete="off" Width="100px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                        <asp:TextBox ID="txtLoaneeName" autocomplete="off" Width="310px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td style="width: 95px" class="labelCaption">Loan Type &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLoanType" autocomplete="off" DataSource='<%# drpload("LoanType") %>'
                                            DataValueField="LoanTypeID"
                                            DataTextField="LoanType" Width="230px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select Loan Type--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblOrgCode" autocomplete="off" CssClass="labelCaption" runat="server">&nbsp;&nbsp; Orginal Code<span class="require">*</span></asp:Label>&nbsp;
                                     <asp:TextBox runat="server" autocomplete="off" ID="txtOrginalCode" Width="95px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                           
                            <table width="100%">
                                <tr>
                                    <td style="width: 90px" class="labelCaption">Office Address1</td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtOffAddress" autocomplete="off" runat="server" Width="420px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox><br />
                                    </td>

                                    <td class="labelCaption">Office Address2</span></td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtOffAddress2" autocomplete="off" runat="server" Width="420px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox><br />
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 90px" class="labelCaption">Office Address3</span></td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtOffAddress3" autocomplete="off" runat="server" Width="420px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td style="width: 90px" class="labelCaption">Pin Code</span></td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtOffPin" autocomplete="off" runat="server" Width="120px" PlaceHolder="Office PIN" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                        <asp:Label ID="lblTelephone" autocomplete="off" CssClass="labelCaption" runat="server">&nbsp;&nbsp; Telephone</asp:Label>&nbsp;
                                        <asp:TextBox ID="txtOffTel" autocomplete="off" runat="server" Width="220px" PlaceHolder="Office Telephone" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>


                                </tr>
                                <tr>
                                    <td style="width: 90px" class="labelCaption">Pan No.</span></td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtPanNo" autocomplete="off" runat="server" MaxLength="10" Width="420px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td style="width: 90px" class="labelCaption">GST No.</span></td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtGstNo" autocomplete="off" runat="server" MaxLength="20" Width="420px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>

                            </table>
                            </br>
                            <table align="center" cellpadding="0" width="100%">

                                <tr>
                                    <td style="width: 95px" class="labelCaption">Factory Address1</td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtFactoryAddr" autocomplete="off" runat="server" Width="420px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox><br />
                                    </td>

                                    <td style="width: 95px" class="labelCaption">Factory Address2</td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtFactoryAddr2" autocomplete="off" runat="server" Width="420px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 95px" class="labelCaption">Factory Address3</td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtFactoryAddr3" autocomplete="off" runat="server" Width="420px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td style="width: 95px" class="labelCaption">Pin Code</td>
                                    <td class="labelCaption">:</td>
                                    <td>
                                        <asp:TextBox ID="txtFactoryPin" autocomplete="off" runat="server" Width="120px" PlaceHolder="Factory PIN" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                        <asp:Label ID="lblFacTel" autocomplete="off" CssClass="labelCaption" runat="server">&nbsp;&nbsp; Telephone</asp:Label>
                                        <asp:TextBox ID="txtFactoryTel" autocomplete="off" runat="server" Width="220px" PlaceHolder="Factory Telephone" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>

                            <br />
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td class="labelCaption">Implementation Status </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlImplement" autocomplete="off" Width="209px" Height="25px" CssClass="inputbox2" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="(--Select Implementation Status--)" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Under Implementation" Value="U"></asp:ListItem>
                                            <asp:ListItem Text="Delay" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="Complete" Value="C"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Date of Commercial Operation </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox runat="server" autocomplete="off" ID="dtCommOperation" ClientIDMode="Static" Width="150px" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgCommOperation" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">District</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtDistrict" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlDistrict" autocomplete="off" DataSource='<%# drpload("District") %>'
                                            DataValueField="MainDistrictID"
                                            DataTextField="MainDistrictName" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select District--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Industry</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtIndustry" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlIndustry" autocomplete="off" DataSource='<%# drpload("Industry") %>'
                                            DataValueField="IndustryType"
                                            DataTextField="IndustryTypeDesc" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select Industry--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Constitution</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtConstitution" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlConstitution" autocomplete="off" DataSource='<%# drpload("Constitution") %>'
                                            DataValueField="ConstitutionUnitTypeID"
                                            DataTextField="ConstitutionUnitTypeName" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select Constitution--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Area</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox runat="server" ID="txtArea" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlArea" autocomplete="off" DataSource='<%# drpload("Area") %>'
                                            DataValueField="Area_Code"
                                            DataTextField="Area_Desc" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select Area--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Sector</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtSector" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlSector" autocomplete="off" DataSource='<%# drpload("Sector") %>'
                                            DataValueField="SectorID"
                                            DataTextField="SectorName" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select Sector--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Purpose</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtPurpose" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlPurpose" autocomplete="off" DataSource='<%# drpload("Purpose") %>'
                                            DataValueField="PUR_CODE"
                                            DataTextField="PUR_NAME" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select Purpose--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Application Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox runat="server" ID="txtApplicationDt" autocomplete="off" ClientIDMode="Static" Width="150px" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgApplicationDt" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                    <td class="labelCaption">Application Amount</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtApplicationAmt" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Project Cost</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtProjCost" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Sanction Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSanctionDT" autocomplete="off" runat="server" ClientIDMode="Static" Width="150px" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgSanctionDT" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                    <td class="labelCaption">Sanction Amount</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSanctioAmt" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Sanction Cancel Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSancandt" autocomplete="off" runat="server" ClientIDMode="Static" Width="150px" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgSancandt" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Sanction Cancel Amount</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSancanAmt" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Repayment Term  &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox runat="server" ID="txtRepaymentTerm" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlRepaymentTerm" autocomplete="off" Width="209px" Height="25px" CssClass="inputbox2" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="(Select Repayment Term)" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Monthly" Value="M"></asp:ListItem>
                                            <asp:ListItem Text="Quarterly" Value="Q"></asp:ListItem>
                                            <asp:ListItem Text="Half-yearly" Value="H"></asp:ListItem>
                                            <asp:ListItem Text="Yearly" Value="Y"></asp:ListItem>
                                            <asp:ListItem Text="Other" Value="O"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td class="labelCaption">Rep Start Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox runat="server" autocomplete="off" ID="txtRepStaartDt" ClientIDMode="Static" Width="150px" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgRepStaartDt" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" cellpadding="0" width="100%" class="borderStyle">
                                <tr>
                                    <td>
                                        <table align="center" cellpadding="0" width="50%" style="float: left;">
                                            <tr>
                                                <th class="labelCaption" colspan="3" style="text-align: center; font-size: 14px;">Primary</th>
                                            </tr>
                                            <tr>
                                                <td class="labelCaption" autocomplete="off" runat="server" clientidmode="Static" style="text-align: center;">Mortgaged</td>
                                                <td class="labelCaption" autocomplete="off" runat="server" clientidmode="Static" style="text-align: center;">Hypothctd</td>
                                                <td class="labelCaption" autocomplete="off" runat="server" clientidmode="Static" style="text-align: center;">Pledged</td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:TextBox ID="txtMortPri" autocomplete="off" runat="server" Width="100px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                                </td>
                                                <td align="center">
                                                    <asp:TextBox ID="txtHypoPri" autocomplete="off" runat="server" Width="100px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                                </td>
                                                <td align="center">
                                                    <asp:TextBox ID="txtPlePri" autocomplete="off" runat="server" Width="100px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table align="center" cellpadding="0" width="50%" style="float: right;">
                                            <tr>
                                                <th class="labelCaption" colspan="3" style="text-align: center; font-size: 14px; border-left: solid 1px #428dc7;">Co-Lateral</th>
                                            </tr>
                                            <tr>
                                                <td class="labelCaption" autocomplete="off" runat="server" clientidmode="Static" style="text-align: center; border-left: solid 1px #428dc7;">Mortgaged</td>
                                                <td class="labelCaption" autocomplete="off" runat="server" clientidmode="Static" style="text-align: center;">Hypothctd</td>
                                                <td class="labelCaption" autocomplete="off" runat="server" clientidmode="Static" style="text-align: center;">Pledged</td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="border-left: solid 1px #428dc7;">
                                                    <asp:TextBox ID="txtMortCol" autocomplete="off" runat="server" Width="100px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                                </td>
                                                <td align="center">
                                                    <asp:TextBox ID="txtHypoCol" autocomplete="off" runat="server" Width="100px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                                </td>
                                                <td align="center">
                                                    <asp:TextBox ID="txtPleCol" autocomplete="off" runat="server" Width="100px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>

                            </table>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td class="labelCaption">Chief Promoter</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtChiefPromoter" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>  
                                    <td class="labelCaption">Pwr Status</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPwrStatus" autocomplete="off" DataSource='<%# drpload("pwrStatus") %>'
                                            DataValueField="PWR_STATUS_CODE" DataTextField="PWR_STATUS_NAME"
                                            AppendDataBoundItems="true" Width="209px" Height="25px" CssClass="inputbox2" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="(--Select PWR Status--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td class="labelCaption">Product</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtProduct" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="labelCaption">Consultant</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtConsultant" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="txtConsultant" autocomplete="off" DataSource='<%# drpload("Consultant") %>'
                                            DataValueField="CONSULT_CODE" DataTextField="CONSULT_NAME"
                                            AppendDataBoundItems="true" Width="209px" Height="25px" CssClass="inputbox2" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="(--Select a Consultant--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Project Employment</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtProjectEmployment" autocomplete="off" runat="server" onkeypress="return isNumberKey(event)" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Actual Employment</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="txtActualEmployment" autocomplete="off" runat="server" onkeypress="return isNumberKey(event)" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="labelCaption">Projected Implemention Date &nbsp; &nbsp; &nbsp; &nbsp;</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtProjImpleDate" autocomplete="off" runat="server" CssClass="inputbox2" Width="150px" ClientIDMode="Static"></asp:TextBox>
                                        <asp:ImageButton ID="imgProjImpleDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>

                                    <td class="labelCaption">Actual Implemention Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtActualImpleDate" autocomplete="off" runat="server" CssClass="inputbox2" Width="150px" ClientIDMode="Static"></asp:TextBox>
                                        <asp:ImageButton ID="imgActualImpleDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                    <td class="labelCaption">Sanction By
                                        <br />
                                        <span style="color: red">Bank_Gurantee</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox runat="server" ID="txtSanctionBy" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlEmpSanction" autocomplete="off" DataSource='<%# drpload("EmpSanction") %>'
                                            DataValueField="SANC_AUTHORITY_CODE"
                                            DataTextField="SANC_AUTHORITY_NAME" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select Sanction By--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="9" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" cellpadding="0" width="100%">

                                <tr>
                                    <th class="labelCaption" width="200px" style="text-align: left; font-size: 14px;">Mobile No./Contact No.</th>
                                    <th class="labelCaption" width="700px" style="text-align: left; font-size: 14px;">Email ID</th>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:TextBox autocomplete="off" onchange='javascript:checkLength(this);' ID="txtMobileNo1" runat="server" PlaceHolder="Mobile Number " CssClass="inputbox2 clr" ClientIDMode="Static"></asp:TextBox>
                                        <asp:TextBox autocomplete="off" onchange='javascript:checkEmail(this);' ID="txtEmail1" Width="600px" runat="server" PlaceHolder="Email ID " CssClass="inputbox2 clr" ClientIDMode="Static"></asp:TextBox>
                                        <asp:Button ID="btnMailPhone" autocomplete="off" runat="server" Style="font-size: small; margin-top: 2px" Width="80px" Height="27px" CssClass="myButton1" Text="Add" />
                                        <table id="tblMailPhone" width="90%" class="hovertable">
                                            <tr>
                                                <th autocomplete="off" runat="server" clientidmode="Static" style="width: 35%">Mobile Number</th>
                                                <th autocomplete="off" runat="server" clientidmode="Static" style="width: 45%">E-mail ID</th>
                                                <th autocomplete="off" runat="server" clientidmode="Static" style="width: 10%">Edit</th>
                                                <th autocomplete="off" runat="server" clientidmode="Static" style="width: 10%">Delete</th>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                                <%--<tr>
                                    <td colspan="9">
                                        <div style="text-align: right;">
                                            <asp:Button ID="BtnPromoterdat" CssClass="edit-address-button" runat="server" Text="Promoter Details" Width="140px" />
                                            <asp:Button ID="btnSecuritydat" CssClass="edit-address-button" runat="server" Text="Security Details" Width="140px" />
                                        </div>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td colspan="9" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td class="labelCaption">Inspection Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInspectionDT" autocomplete="off" runat="server" Width="65" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgInspectionDT" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>

                                    <td class="labelCaption">Processing Off</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtProcessinfOFF" runat="server" Width="200" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                         <asp:DropDownList ID="ddlProcessinfOFF" autocomplete="off" DataSource='<%# drpload("Employee") %>'
                                            DataValueField="EmployeeID"
                                            DataTextField="EmpName" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>

                                    </td>

                                    <td class="labelCaption">Disburse Off</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtDisburseOff" runat="server" Width="200" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddDisburseOff" autocomplete="off" DataSource='<%# drpload("Employee") %>'
                                            DataValueField="EmployeeID"
                                            DataTextField="EmpName" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Reco Off</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtRecoOff" runat="server" Width="200" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddRecoOff" autocomplete="off" DataSource='<%# drpload("Employee") %>'
                                            DataValueField="EmployeeID"
                                            DataTextField="EmpName" Width="209px" Height="25px" runat="server" ForeColor="#18588a"
                                            AppendDataBoundItems="true" ClientIDMode="Static" CssClass="inputbox2">
                                            <asp:ListItem Text="(--Select--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="12" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                            </table>

                            <%--=============================================================================================================================================================================--%>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="12">
                                        <table class="headingCaptionHead" width="98%" align="center">
                                            <tr>
                                                <td align="left">Loan Detail Entry</td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>

                                <%--<tr>
                                    <td class="labelCaption">Loanee &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left" colspan="9">
                                        <asp:TextBox ID="txtLoaneeDat" Width="75px" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                        <asp:TextBox ID="txtloaneeDat2" runat="server" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>--%>
                            </table>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td class="labelCaption">Proc. Year/Month</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtProcYearMon" autocomplete="off" PlaceHolder="YYYYMM" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Installment No 1</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallNo1" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Installment Amount 1</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallAmt1" autocomplete="off" runat="server" Style="text-align: right" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">First Disb Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtFirstDisbdt" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgFirstDisbdt" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">Interest Rate</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInterestRate" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Installment No 2</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallNo2" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Installment Amount 2</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallAmt2" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Install Due Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallDuedt" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgInstallDuedt" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Rebate Rate</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtrebaterate" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Installment No 3</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallNo3" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Installment Amount 3</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallAmt3" autocomplete="off" runat="server" Style="text-align: right" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Disb Amount</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDisbAmt" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Penalty Rate</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPenaltyRate" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Installment No 4</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallNo4" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Installment Amount 4</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInstallAmt4" autocomplete="off" runat="server" Style="text-align: right" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Health Code</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtHealthCode" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Cuml Debit Amount</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCumlDrAmt" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Cuml Credit Amount</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCumlCrAmt" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Total O/S</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTotalOS" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Service Charge</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlServiceCharge" autocomplete="off" Width="120px" Height="25px" runat="server" CssClass="inputbox2" ForeColor="#18588a">
                                            <asp:ListItem Text="(Select Service)" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="N"></asp:ListItem>
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Principle Default</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPrincipleDefault" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2" Enabled="true"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Interest Default</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInterestDef" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2" Enabled="true"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Other Default</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtOtherDef" autocomplete="off" runat="server" Width="120px" Style="text-align: right" ClientIDMode="Static" CssClass="inputbox2" Enabled="true"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Default Status Code</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDefStatuCode" autocomplete="off" Style="text-align: right" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Account Status Code</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <%--<asp:TextBox ID="txtAccStatuCode" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                        <asp:DropDownList ID="txtAccStatuCode" autocomplete="off" DataSource='<%# drpload("AccStatus") %>'
                                            DataValueField="ACCOUNT_STATUS_CODE" DataTextField="ACCOUNT_STATUS_NAME"
                                            AppendDataBoundItems="true" Width="120px" Height="25px" CssClass="inputbox2" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="(--Select Account Status--)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>

                                    <td class="labelCaption">Status</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStatus" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Re Structure Date</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtReStructureDate" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                        <asp:ImageButton ID="imgReStructureDate" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                            AlternateText="Click to show calendar" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Button ID="cmdAddDtl" runat="server" Text="Add Detail" CommandName="AddDtl" Height="30px" autocomplete="off"
                                            Width="100px" CssClass="save-button DefaultButton" />
                                    </td>
                                </tr>
                            </table>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td>
                                        <div style="overflow-y: scroll; height: auto; width: 100%">
                                            <asp:GridView ID="grdLoanDetail" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                                                AutoGenerateColumns="false" DataKeyNames="YEAR_MONTH" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                                <AlternatingRowStyle BackColor="Honeydew" />
                                                <Columns> 
                                                    <asp:BoundField DataField="YEAR_MONTH" HeaderText="Edit" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <%--<asp:BoundField DataField="YEAR_MONTH" HeaderText="Cancel" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />--%>
                                                    <asp:BoundField DataField="YEAR_MONTH" HeaderText="Delete" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="YEAR_MONTH" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Year Month" />
                                                    <asp:BoundField DataField="INSTAL_AMT_1" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Installment Amount 1" />
                                                    <asp:BoundField DataField="Health_Code" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Health Code" />
                                                    <asp:BoundField DataField="Cuml_DR" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Cumelative Debit" />
                                                    <asp:BoundField DataField="Cuml_CR" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Cumelative Credit" />
                                                    <asp:BoundField DataField="TOT_OS" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Total OS" />
                                                    <asp:BoundField DataField="Pri_Default" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Principle Default" />
                                                    <asp:BoundField DataField="Inst_Default" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Interest Default" />
                                                    <asp:BoundField DataField="Oth_Default" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Other Default" />
                                                    <asp:BoundField DataField="Status" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Status" />
                                                </Columns> 
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table align="center" cellpadding="0" width="100%">
                                <tr style="width:100%" >
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr> 
                                    <td class="labelCaption" style="text-align:left;width:10%">Pre Pay Date &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption" style="width:1%">:</td>
                                    <td class="labelCaption" style="text-align:left;width:15%">
                                        <asp:TextBox ID="txtPrePayDate" autocomplete="off" PlaceHolder="DDMMYYYY" Width="100px" MaxLength="10" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption" style="text-align:left;width:10%">Pre Pay Amount &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption" style="width:1%">:</td>
                                    <td class="labelCaption" style="text-align:left" >
                                        <asp:TextBox ID="txtPrePayAmount" autocomplete="off" PlaceHolder="Pre Pay Amount" Width="200px" MaxLength="15" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                   
                                </tr>
                                </table>
                        </InsertItemTemplate>

                        <%--<EditItemTemplate>
                            
                        </EditItemTemplate>--%>
                        <FooterTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Submit" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
    </div>


    <div id="dialog" align="right" title="Repayment Schedule" style="display: none">
        <asp:Button ID="btnNewSchedule" autocomplete="off" runat="server" Width="40%" CssClass="myButton1" Text="Create New Repayment Schedule" />
        <br />
        <br />
        <div id="tableDiv">
            <table id="tblRepayDetails" autocomplete="off" runat="server" clientidmode="Static" style="width: 100%" class="hovertable">
                <tr>
                    <th>Sr No.</th>
                    <th>Loanee</th>
                    <th>Show Details</th>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
