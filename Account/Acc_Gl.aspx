﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Acc_Gl.aspx.cs" Inherits="Acc_Gl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/Acc_Gl.js?v=2"></script>
    <script type="text/javascript" src="js/Acc_Gl_Master.js?v=2"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
        });

        $(document).ready(function () {

            $('#txtHdnGlCodeSearch').css("display", "none");
            $('#hdnGroupID').css("display", "none");
            $('#hdnGlID').css("display", "none");
        });

        function beforeSave() {
            $("#frmEcom").validate();

        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
    </script>
    <style type="text/css">
        input {
            text-transform: uppercase;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td> GL Master</td>
            </tr>
        </table>
       

        <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>


        <%-- ================ GL Code Search POP UP GRIDVIEW =====================  --%>
        <div id="dialogGL" style="display: none;">
            <asp:TextBox ID="txtSearchGlCode" runat="server" ClientIDMode="Static" Width="516px" autocomplete="off" CssClass="TextSearchVendor" MaxLength="150"></asp:TextBox>

            <asp:GridView ID="grdSearchGlCode" runat="server" ClientIDMode="Static" autocomplete="off" Style="border: 1px solid #59bdcc; width: 520px;" AutoGenerateColumns="false" CssClass="GridVendor" PageSize="5" AllowPaging="true">
                <Columns>

                    <asp:BoundField DataField="OLD_GL_ID" HeaderText="Gl Code" ItemStyle-Width="150" />
                    <asp:BoundField DataField="GL_ID" HeaderText="Gl ID" ItemStyle-Width="150" />
                    <asp:BoundField DataField="GL_NAME" HeaderText="Gl_Name" ItemStyle-Width="150" />
                </Columns>
            </asp:GridView>

        </div>
        <%-- ================ END GL Code Search POP UP GRIDVIEW =====================  --%>

        <%-- ================ Group Code POP UP GRIDVIEW =====================  --%>
        <div id="dialogGroup" style="display: none">
            <asp:TextBox ID="txtSearchOldGroupID" runat="server" ClientIDMode="Static" autocomplete="off" Width="516px" CssClass="TextSearchVendor" MaxLength="150"></asp:TextBox>

            <asp:GridView ID="grdGroup" runat="server" ClientIDMode="Static" autocomplete="off" Style="border: 1px solid #59bdcc; width: 520px;" AutoGenerateColumns="false" CssClass="GridVendor" PageSize="5" AllowPaging="true">
                <Columns>
                    <asp:BoundField DataField="OldGroupID" HeaderText="Group ID" ItemStyle-Width="150" />
                    <asp:BoundField DataField="GroupName" HeaderText="Group Name" ItemStyle-Width="150" />
                </Columns>
            </asp:GridView>

        </div>
        <%-- ================ END   Group Code POP UP GRIDVIEW =====================  --%>

        <table width="98%" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" ClientIDMode="Static" autocomplete="off" Width="99%" AutoGenerateRows="False"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" width="100%">

                                <tr>
                                    <td colspan="6">
                                        <table class="headingCaptionHead" width="100%" align="left">
                                            <tr>
                                                <td style="height: 15px;" align="left">Account GL Search</td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>   
                                <tr>           
                                    <td  class="labelCaption" style="width:7%;"> Gl Code &nbsp;&nbsp;<span class="require">*</span>  </td>
                                    <td class="labelCaption">:</td>
                                    <td align="right">
                                        <asp:TextBox ID="txtGlCodeSearch" Width="100%" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="inputbox2 autosuggestGL"></asp:TextBox>
                                        <asp:TextBox ID="txtHdnGlCodeSearch" runat="server" ClientIDMode="Static" autocomplete="off" ></asp:TextBox>

                                    </td>
                                    <td style="width:27%">
                                        <asp:Button ID="cmdView" ClientIDMode="Static" runat="server" Text="View" CommandName="Add" autocomplete="off"
                                            Width="80px" hight="5px" CssClass="save-button DefaultButton" />

                                        <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" ClientIDMode="Static" autocomplete="off"
                                                Width="80px" hight="5px" CssClass="save-button" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />

                                        <asp:Button ID="cmdDelete" runat="server" Text="Delete" ClientIDMode="Static" autocomplete="off"
                                        Width="80px" hight="5px" CssClass="save-button DefaultButton" />	

                                    </td>
                                </tr>
                                </table>
                                
                                <table align="center" width="100%">
                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6">
                                        <table class="headingCaptionHead" width="100%" align="left">
                                            <tr>
                                                <td align="left">Account GL Entry</td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>

                                <tr>        

                                    <td class="labelCaption">Group Code &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGrpCode" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Group Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGrpName" autocomplete="off" ClientIDMode="Static" Width="300" runat="server" CssClass="inputbox2" Enabled="false"></asp:TextBox>
                                        <asp:TextBox ID="hdnGroupID" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2" Visible="true"></asp:TextBox>
                                        <asp:TextBox ID="hdnGlID" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2" Visible="true"></asp:TextBox>
                                        <asp:TextBox ID="hdnMaxYearMonth" autocomplete="off" Width="100" ClientIDMode="Static" runat="server" style="display:none"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">Gl Code &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGlCode" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">GL Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtGlName" Width="300" ClientIDMode="Static" runat="server" autocomplete="off" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">Gl Type&nbsp;&nbsp;<span class="require"></span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGlType" Width="66%"  runat="server" ClientIDMode="Static" autocomplete="off" CssClass="citybox2">
                                            <asp:ListItem Text="Normal" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="Deposit" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="Branch" Value="B"></asp:ListItem>
                                            <asp:ListItem Text="Fixed Assets" Value="F"></asp:ListItem>
                                            <asp:ListItem Text="Loan" Value="L"></asp:ListItem>
                                            <asp:ListItem Text="Employee" Value="E"></asp:ListItem>
                                            <asp:ListItem Text="Re-Finance" Value="R"></asp:ListItem>
                                            <asp:ListItem Text="Bank" Value="A"></asp:ListItem>
                                            <asp:ListItem Text="Cash" Value="C"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    
                                   <td class="labelCaption">For Unit Only &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkUnitOnly" class="headFont" Width="100%" runat="server" ClientIDMode="Static" autocomplete="off" Font-Size="small" Text="" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">GL Master &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td style="padding: 5px;">
                                        <asp:RadioButton ID="rdNormal" Checked="true" class="inputbox2" runat="server" ClientIDMode="Static" autocomplete="off" Font-Size="small" GroupName="rd" Text="Normal" />
                                        <asp:RadioButton ID="rdMaster" class="inputbox2" runat="server" ClientIDMode="Static" autocomplete="off" Font-Size="small" GroupName="rd" Text="Master" />
                                    </td>

                                    <td class="labelCaption">Schedule &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSchedule" Width="300" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6">
                                        <table class="headingCaptionHead" width="100%" align="left">
                                            <tr>
                                                <td align="left">GL Details</td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>

                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <table align="center" cellpadding="5" width="100%">
                                            <tr>
                                                <th>Year/Month(YYYYMM)</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;Opening Debit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;Opening Credit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Debit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Credit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;Closing Debit</th>
                                                <th>&nbsp;&nbsp;&nbsp;&nbsp;Closing Credit</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtYearMonth" Enabled="true" Width="120" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtOpenDebit" Width="100" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtOpenCredit" Width="100" ClientIDMode="Static" autocomplete="off" runat="server" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalDebit" Width="100" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="false" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtTotalCredit" Width="100" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="false" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtCloseDebit" Width="100" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="false" CssClass="inputbox2"></asp:TextBox></td>
                                                <td>
                                                    <asp:TextBox ID="txtCloseCredit" Width="100" ClientIDMode="Static" autocomplete="off" runat="server" Enabled="false" CssClass="inputbox2"></asp:TextBox></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                    </table>
                                     <table align="left" width="100%">
                                <tr>
                                    <td >
                                        <div style="border: 1px solid white; width:100%; font-size: small">
                                            <asp:GridView ID="grdAccGLDtl" ClientIDMode="Static" Width="100%" DataKeyNames="ItemID" autocomplete="off"
                                                clientID="grdAccGLDtl" runat="server" AutoGenerateColumns="False" CssClass="Grid">
                                                <AlternatingRowStyle BackColor="Honeydew" />

                                                <Columns>
                                                    <asp:BoundField DataField="ItemID" HeaderText="Edit" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="ItemID" HeaderText="Cancel" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="ItemID" HeaderText="Delete" ItemStyle-Width="5%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="YearMonth" HeaderText="Year/Month(YYYYMM)" ItemStyle-Width="12%" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="OpeningDebit" HeaderText="Opening Debit" ItemStyle-Width="12%" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="OpeningCredit" HeaderText="Opening Credit" ItemStyle-Width="12%" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="TotalDebit" HeaderText="Total Debit" ItemStyle-Width="12%" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="TotalCredit" HeaderText="Total Credit" ItemStyle-Width="13%" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="ClosingDebit" HeaderText="Closing Debit" ItemStyle-Width="12%" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />
                                                    <asp:BoundField DataField="ClosingCredit" HeaderText="Closing Credit" ItemStyle-Width="12%" DataFormatString="{0:0.00}" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" />

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>

                            </table>

                        </InsertItemTemplate>
                        
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" ClientIDMode="Static" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" />
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" ClientIDMode="Static" autocomplete="off"
                                                Width="100px" CssClass="save-button" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
