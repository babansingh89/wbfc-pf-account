﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="SecurityReport.aspx.cs" Inherits="SecurityReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<script type="text/javascript" src="js/SanctionReport.js"></script>--%>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert('Only Numeric Value Accept!');
                return false;
            }
            return true;
        }

        $(function () {
            $("#ddlReportType").change(function () {
                var selectedText = $(this).find("option:selected").text(); 
                var selectedValue = $(this).val();
                if (selectedValue == 'M') {
                    $('#txtStartingCode').attr('disabled', true);
                    $('#txtEndingCode').attr('disabled', true);
                }
                else if (selectedValue == 'S' || selectedValue == 'L') {
                    $('#txtStartingCode').attr('disabled', false);
                    $('#txtEndingCode').attr('disabled', true);
                }
                else if (selectedValue == 'A') {
                    $('#txtStartingCode').attr('disabled', false);
                    $('#txtEndingCode').attr('disabled', false);
                }
                else {
                    $('#txtStartingCode').attr('disabled', true);
                    $('#txtEndingCode').attr('disabled', true);
                }

                $("#txtStartingCode").val('');
                $("#txthdnStartingCode").val('');
                $("#txtEndingCode").val('');
                $("#txthdnEndingCode").val('');
                return false;
                //alert("Selected Text: " + selectedText + " Value: " + selectedValue);
            });
        });

        //$(document).ready(function () {
        //$("#ddlReportType").change(function () {
        //    alert($("#ddlReportType").val());
        //    });
        //});

        function autocompleteStareCode() {
            $("#txtStartingCode").autocomplete({
                source: function (request, response) {
                    var STcode = $("#txtStartingCode").val();
                    var SectorID = $("#ddlUserSector").val();
                    var W = "{'STcode':'" + STcode + "',SectorID:" + SectorID + "}";
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "SecurityReport.aspx/GET_AutoComplete_FromLoanee",
                        data: W,
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split("|")[0] + '<>' + item.split("|")[1],
                                    val: item,
                                }
                            }))
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                minLength: 0,
                select: function (e, i) {
                    var arr = i.item.val.split("|");
                    $("#txthdnStartingCode").val(arr[2]);
                }
            }).click(function () {
                $(this).data("autocomplete").search($(this).val());
            });
        }

        function autocompleteEndCode() {
            $("#txtEndingCode").autocomplete({
                source: function (request, response) {
                    var ENDcode = $("#txtEndingCode").val();
                    var SectorID = $("#ddlUserSector").val();
                    var W = "{'ENDcode':'" + ENDcode + "',SectorID:" + SectorID + "}";
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "SecurityReport.aspx/GET_AutoComplete_ToLoanee",
                        data: W,
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split("|")[0] + "<>" + item.split("|")[1],
                                    val: item,
                                }
                            }))
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                minLength: 0,
                select: function (e, i) {
                    var arr = i.item.val.split("|");
                    $("#txthdnEndingCode").val(arr[2]);
                }
            }).click(function () {
                $(this).data("autocomplete").search($(this).val());
            });
        }

        $(document).ready(function () {
            $('#txtStartingCode').attr('disabled', true);
            $('#txtEndingCode').attr('disabled', true);
            $('#txtFrmDate, #txtToDate').datepicker({
                showOtherMonths: true,
                selectOtherMonths: true,
                changeMonth: true,
                changeYear: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });
            $("#txtFrmDate").focus();

            autocompleteStareCode();
            autocompleteEndCode();

            $("#ddlReportName").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#txtFrmDate").focus();
                    return false;
                }
            });

            $("#txtFrmDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#txtToDate").focus();
                    return false;
                }
            });

            $("#txtToDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#btnGenerateReport").focus();
                    return false;
                }
            });

            $("#btnGenerateReport").click(function (event) {
                if ($("#ddlReportName").val() == "0") {
                    alert("Select a Report Name!");
                    $("#ddlReportName").focus();
                    return false;
                }
                if ($("#txtFrmDate").val() == "") {
                    alert("Please Enter From Date !");
                    $("#txtFrmDate").focus();
                    return false;
                }
                if ($("#txtToDate").val() == "") {
                    alert("Please Enter To Date !");
                    $("#txtToDate").focus();
                    return false;
                }
                if ($("#ddlReportType").val() == 'S' || $("#ddlReportType").val() == 'L') {
                    if ($("#txthdnStartingCode").val() == '') { alert('Please Select Loanee Code'); $("#txtStartingCode").focus(); return false; }
                }
                else if ($("#ddlReportType").val() == 'A') {
                    if ($("#txthdnStartingCode").val() == '') { alert('Please Select From Loanee Code'); $("#txtStartingCode").focus(); return false; }
                    if ($("#txthdnEndingCode").val() == '') { alert('Please Select TO Loanee Code'); $("#txtEndingCode").focus(); return false; }
                }
                ShowReport();
                return false;
            });

            function ShowReport() {
                var FormName = '';
                var ReportName = '';
                var ReportType = '';
                var RecFromDate = '';
                var RecToDate = '';
                var FromLoaneeID = '';
                var ToLoaneeID = '';
                var SectorID = '';
                FormName = "SecurityReport.aspx";
                RecFromDate = $('#txtFrmDate').val().split("/");
                RecToDate = $('#txtToDate').val().split("/");
                RecFromDate = RecFromDate[2] + "-" + RecFromDate[1] + "-" + RecFromDate[0];
                RecToDate = RecToDate[2] + "-" + RecToDate[1] + "-" + RecToDate[0];

                var RptType = $("#ddlReportType").val();

                if (RptType == 'M' || RptType == 'A') {
                    ReportName = "AllSecurity";
                    ReportType = "All Security Report";
                }
                else {
                    ReportName = "SingleSecurity";
                    ReportType = "Security Report With Loanee";
                }
                FromLoaneeID = $("#txthdnStartingCode").val();
                TLoaneeID = $("#txthdnEndingCode").val();
                if (FromLoaneeID == '') { FromLoaneeID = 0; }
                if (ToLoaneeID == '') { ToLoaneeID = 0; }
                SectorID = $("#ddlUserSector").val();
                var E = '';
                E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  RecFromDate:'" + RecFromDate + "',  RecToDate:'" + RecToDate + "',RptType:'" + RptType + "',FromLoaneeID:" + FromLoaneeID + ",ToLoaneeID:" + ToLoaneeID + ",SectorID:" + SectorID +"}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/SetReportValue',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var show = response.d;
                        if (show == "OK") {
                            window.open("ReportView.aspx?E=Y");
                        }

                    },
                    error: function (response) {
                        var responseText;
                        responseText = JSON.parse(response.responseText);
                        alert("Error : " + responseText.Message);
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            
        });
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="100%" align="center">
            <tr>
                <td>SECURITY REPORT</td>
            </tr>
        </table>
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            

            <tr>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">From Date<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="text-align:left">
                    <asp:TextBox ID="txtFrmDate" autocomplete="off" ReadOnly="false" MaxLength="10" placeholder="DDMMYYYY" Width="190px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                </td>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">To Date<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>
                    <asp:TextBox ID="txtToDate" autocomplete="off" ReadOnly="false" MaxLength="10" placeholder="DDMMYYYY" Width="190px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" style="font-size: 15px; color: navy; font-weight: bold;">Report Name<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="width: 50px;">
                    <asp:DropDownList ID="ddlReportName" autocomplete="off" runat="server" Width="200px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                        <%--<asp:ListItem Value="0">(Select Report Name)</asp:ListItem>--%>
                        <asp:ListItem Value="RR">Recovery Report</asp:ListItem>
                    </asp:DropDownList>
                </td>
               <td align="right" style="font-size: 15px; color: navy; font-weight: bold;">Report Type<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="width: 50px;text-align:center;">
                    <asp:DropDownList ID="ddlReportType" autocomplete="off" runat="server" Width="195px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                           
                        <asp:ListItem Value="M">Maturity Wise</asp:ListItem>
                        <asp:ListItem Value="S">For Single Unit</asp:ListItem>
                         <asp:ListItem Value="A">For All Unit</asp:ListItem>
                        <asp:ListItem Value="L">Cancelation Of LIC</asp:ListItem>
                        <%--<asp:ListItem Value="I">Incasement Of FDR</asp:ListItem>
                        <asp:ListItem Value="F">Fixed Deposite</asp:ListItem>
                        <asp:ListItem Value="R">Release Of FDR</asp:ListItem>--%>
                    </asp:DropDownList> 

                </td>
               
            </tr>

            <tr>
                <td align="right" style="font-size: 15px; color: navy; font-weight: bold;">From Loanee<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="width: 50px;">
                    <asp:TextBox ID="txtStartingCode" autocomplete="off" Width="190Px" Style="text-transform: uppercase;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                    <asp:TextBox ID="txthdnStartingCode" autocomplete="off" Width="10px" Style="text-transform: uppercase; display: none;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>
                <td align="right" style="font-size: 15px; color: navy; font-weight: bold;">To Loanee<span class="require"> *&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="width: 50px;">
                    <asp:TextBox ID="txtEndingCode" autocomplete="off" Width="190Px" Style="text-transform: uppercase;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                    <asp:TextBox ID="txthdnEndingCode" autocomplete="off" Width="10px" Style="display: none;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                    </td>
                <td style="width:20%"></td>
            </tr>
          
        </table>

       <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <%--<tr style="width: 100%">
                <td></td>
                <td></td>
            </tr>   --%> 
           
             <tr style="width: 100%;text-align:center;">
                 <td style="width:40%;text-align:left;"></td>
                <td >
                    <asp:Button ID="btnGenerateReport" autocomplete="off" runat="server" Text="Report" Width="100px" CssClass="save-button DefaultButton" />
                    <asp:Button ID="btnCancel" autocomplete="off" runat="server" Text="Cancel " Width="100px" CssClass="save-button" OnClick="btnCancel_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
