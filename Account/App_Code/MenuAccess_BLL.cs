﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DataAccess;

/// <summary>
/// Summary description for MenuAccess_BLL
/// </summary>
public class MenuAccess_BLL
{
    public List<Menu> GetAllMenu(string moduleCode)
    {
        List<Menu> ListMenu = new List<Menu>();
        DataTable dtMenu = DBHandler.GetResult("Get_MenuForMenuAccess", DBNull.Value, Convert.ToInt32(moduleCode));
        if (dtMenu.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dtMenu.Rows)            
            {
                Menu menu = new Menu();
                menu.MenuCode = Convert.ToInt32(dtRow["MenuCode"].ToString());
                menu.Menuname = dtRow["MenuName"].ToString();
                menu.ParentMenuCode = Convert.ToInt32(dtRow["ParentMenuCode"].ToString());

                ListMenu.Add(menu);
            }
        }
        return ListMenu;
    }

    public List<MstModule> GetAllModule()
    {
        List<MstModule> ListModule = new List<MstModule>();
        DataTable dtModule = DBHandler.GetResult("Get_Module");
        if (dtModule.Rows.Count > 0)
        {
            foreach(DataRow dtrow in dtModule.Rows)
            {
                MstModule module = new MstModule();
                module.ModuleCode = Convert.ToInt32(dtrow["ModuleCode"].ToString());
                module.ModuleName = dtrow["ModuleName"].ToString();
                ListModule.Add(module);
            }
        }
        return ListModule;
    }

    public List<Mst_User> userCompleteData(string username)
    {
        List<Mst_User> ListUsers = new List<Mst_User>();
        DataTable dtUsers = DBHandler.GetResult("GET_UsersAutoComplete", username);
        foreach (DataRow dtRow in dtUsers.Rows)
        {
            Mst_User user = new Mst_User();
            user.UserID = Convert.ToInt32(dtRow["UserID"].ToString());
            user.Name = dtRow["Name1"].ToString();

            ListUsers.Add(user);
        }
        return ListUsers;
    }

    public List<UserMenu> getCheckedMenu(string UserID, string ModuleCode)
    {
        List<UserMenu> ListUserMenus = new List<UserMenu>();
        DataTable dtUserMenu = DBHandler.GetResult("GET_MenuByUserID", Convert.ToInt32(UserID), Convert.ToInt32(ModuleCode));
        foreach (DataRow dtRow in dtUserMenu.Rows)
        {
            UserMenu userMenu = new UserMenu();
            userMenu.UserID = Convert.ToInt32(dtRow["UserID"].ToString());
            userMenu.MenuCode = Convert.ToInt32(dtRow["MenuCode"].ToString());

            ListUserMenus.Add(userMenu);
        }
        return ListUserMenus;
    }

    public string insertUserMenu(string userID, string Menus, string ModuleCode)
    {
        string status="";
        int insertedby = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID].ToString());
        DataTable dtMstUserMenu = DBHandler.GetResult("Insert_UserMenu", Convert.ToInt32(userID), Menus, Convert.ToInt32(ModuleCode), insertedby);
        if (dtMstUserMenu.Rows.Count > 0)
        {
            status = dtMstUserMenu.Rows[0]["UserID"].ToString();
        }
        else
        {
            return status="";
        }
        return status;
    }
}