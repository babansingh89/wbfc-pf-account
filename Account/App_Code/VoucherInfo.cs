﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherInfo
/// </summary>
public class VoucherInfo
{
    public VoucherMaster VoucherMast = new VoucherMaster();
    public VoucherDetail[] VoucherDetail;
    public VoucherInstType[] tmpVchInstType;
    public VoucherFixedAsset[] tmpVchFA;

    public string SessionExpired { get; set; }
    public string EntryType { get; set; }
    public string SearchResult { get; set; }
    public string Message { get; set; }


	public VoucherInfo()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}