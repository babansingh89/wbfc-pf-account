﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherFixedAsset
/// </summary>
public class VoucherFixedAsset
{
    public int FA_ID { get; set; }
    public string GL_ID { get; set; }
    public string FA_DESC { get; set; }
    public int VENDOR_CODE { get; set; }
    public string VENDOR_NAME { get; set; }
    public double DEP_RATE { get; set; }
    public string DEP_METHOD { get; set; }
    public double PUR_VALUE { get; set; }
    public int PUR_VCH_NO { get; set; }
    public string PUR_VCH_DATE { get; set; }
    public int VCH_SECTORID { get; set; } // the sectorid where the voucher was created
    
    public double SOL_VALUE { get; set; }
    public int SOL_VCH_NO { get; set; }
    public string SOL_VCH_DATE { get; set; }
    public int SECTORID { get; set; } // for whom the fa is purchased
    public string SECTORNAME { get; set; } // for whom the fa is purchased

	public VoucherFixedAsset()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}