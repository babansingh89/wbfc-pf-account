﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherDetail
/// </summary>
public class VoucherDetail
{

    public string YearMonth { get; set; }
    public int VoucherType { get; set; }
    public int VCHNo { get; set; }
    public int VchSrl { get; set; }
    public string OLDSLID { get; set; }
    public string GLName { get; set; }
    public string GLID { get; set; }
    public string SLID { get; set; }
    public string SubID { get; set; }
    public string OLDSUBID { get; set; }
    public string SUBDesc { get; set; }   
    public string DRCR { get; set; }
    public string GlType { get; set; }
    public double AMOUNT { get; set; }
    public int SectorID { get; set; }

    public VoucherInstType[] VchInstType;
    public VoucherFixedAsset[] VchFA;

	public VoucherDetail()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}