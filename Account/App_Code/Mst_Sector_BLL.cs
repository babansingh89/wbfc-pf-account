﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DataAccess;
/// <summary>
/// Summary description for Mst_Sector_BLL
/// </summary>
public class Mst_Sector_BLL
{
    public List<Mst_Sector> GetAllSector()
    {
        List<Mst_Sector> lstSector = new List<Mst_Sector>();
        DataTable dtSector = DBHandler.GetResult("Get_Sector_Name");
        if (dtSector.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dtSector.Rows)
            {
                Mst_Sector sector = new Mst_Sector();
                sector.SectorID = Convert.ToInt32(dtRow["SectorID"].ToString());
                sector.SectorName = dtRow["SectorNameCode"].ToString();

                lstSector.Add(sector);
            }
        }
        return lstSector;
    }
}