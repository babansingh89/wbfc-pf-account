﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DataAccess;

/// <summary>
/// Summary description for UserType_BLL
/// </summary>
public class UserType_BLL
{
    public List<Typ_User> GetAllTypeUser()
    {
        List<Typ_User> lstTypeUser = new List<Typ_User>();
        DataTable dtTypeUser = DBHandler.GetResult("Get_UserType");
        if (dtTypeUser.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dtTypeUser.Rows)
            {
                Typ_User tu = new Typ_User();
                tu.TypeCode = Convert.ToInt32(dtRow["TypeCode"].ToString());
                tu.TypeName = dtRow["TypeName"].ToString();

                lstTypeUser.Add(tu);
            }
        }
        return lstTypeUser;
    }
}