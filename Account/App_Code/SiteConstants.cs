﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for SiteConstants
/// </summary>
public class SiteConstants 
{
    public const string SSN_DR_USER_DETAILS = "UserDetails";
    public const string SSN_INT_USER_ID = "UserID";    
    public const string SSN_USER_TYPE = "UserType";
    public const string SSN_MODULE = "Module";
    public const string SSN_SECTOR_ID = "SectorID";
    public const string StrFormName = "FormName";
    public const string StrReportName = "ReportName";
    public const string StrReportType = "ReportType";


    //================ Transfer Data From One WebForm To Another ===================//
    public const string SSN_LOANEE_ID = "SSN_LOANEE_ID";
    public const string SSN_SCHEDULE_ID = "SSN_SCHEDULE_ID";
    public const string SSN_SANCTION_AMT = "SSN_SANCTION_AMT";
    public const string SSN_REPAYMENT_TERM = "SSN_REPAYMENT_TERM";
    public const string SSN_REPAYMENT_START_DATE = "SSN_REPAYMENT_START_DATE";
    //================ Transfer Data From One WebForm To Another ===================//
   
    public SiteConstants()
    {

    }
}
