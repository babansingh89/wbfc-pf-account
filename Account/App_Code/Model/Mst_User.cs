﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Mst_User
/// </summary>
public class Mst_User
{
    public int UserID { get; set; }
    public int TypeCode { get; set; }
    public string TypeName { get; set; }
    public string UserName { get; set; }
    public string Name { get; set; }
    public string LockFlag { get; set; }
    public int? InsertedBy { get; set; }
    public DateTime InsertedOn { get; set; }
    public int? ModifiedBy { get; set; }
    public DateTime ModifiedOn { get; set; }
    public int CurrentUserID { get; set; }
    public List<Mst_Sector> ListSector { get; set; }
}