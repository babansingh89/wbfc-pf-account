﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Typ_User
/// </summary>
public class Typ_User
{
    public int TypeCode { get; set; }
    public string TypeName { get; set; }
}