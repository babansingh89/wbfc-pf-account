﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Menu
/// </summary>
public class Menu
{
    public int MenuCode { get; set; }
    public string Menuname { get; set; }
    public int? ParentMenuCode { get; set; }                 
}