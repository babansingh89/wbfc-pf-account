﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Mst_Sector
/// </summary>
public class Mst_Sector
{
    public int SectorID { get; set; }
    public string SectorName { get; set; }
}