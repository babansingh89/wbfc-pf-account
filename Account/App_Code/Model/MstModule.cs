﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MstModule
/// </summary>
public class MstModule
{
    public int ModuleCode { get; set; }
    public string ModuleName { get; set; }
}