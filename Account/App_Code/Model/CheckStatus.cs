﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CheckStatus
/// </summary>
public class CheckStatus
{
    public string Status { get; set; }
    public object Data { get; set; }
    public string Message { get; set; }
}