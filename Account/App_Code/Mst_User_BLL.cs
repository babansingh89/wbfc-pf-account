﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DataAccess;

/// <summary>
/// Summary description for Mst_User_DAL
/// </summary>
public class Mst_User_BLL
{
	public List<Mst_User>  GetAll()
	{

        List<Mst_User> lstUserMaster = new List<Mst_User>();
        DataSet dsMstUser = DBHandler.GetResults("Get_UserAll",DBNull.Value);
        if (dsMstUser.Tables[0].Rows.Count>0)
        {
            foreach (DataRow dtRow in dsMstUser.Tables[0].Rows)            
            {
                Mst_User um = new Mst_User();
                um.UserID = Convert.ToInt32(dtRow["UserID"].ToString());
                um.TypeName = dtRow["TypeName"].ToString();
                um.UserName = dtRow["UserName"].ToString();
                um.Name = dtRow["Name"].ToString();

                lstUserMaster.Add(um);
            }
        }
        return lstUserMaster;
	}

    public List<Mst_User> GetUserByID(string UserID)
    {
        List<Mst_User> lstUserMaster = new List<Mst_User>();
        DataSet dsMstUser = DBHandler.GetResults("Get_UserAll", Convert.ToInt32(UserID));
        if (dsMstUser.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dtRow in dsMstUser.Tables[0].Rows)
            {
                Mst_User um = new Mst_User();
                um.UserID = Convert.ToInt32(dtRow["UserID"].ToString());
                um.TypeCode = Convert.ToInt32(dtRow["TypeCode"].ToString());
                um.UserName = dtRow["UserName"].ToString();
                um.Name = dtRow["Name"].ToString();
                um.ListSector = dsMstUser.Tables[1].AsEnumerable().Select(t => new Mst_Sector() {
                SectorID = t.Field<int>("SectorID"),
                   //SectorName=t.Field<string>("")
                }).ToList();

                lstUserMaster.Add(um);
            }
        }
        return lstUserMaster;
    }

    public Mst_User InsertUser(Mst_User obj, string password)
    {
        int insertedby = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID].ToString());
        string []p=obj.ListSector.Select(t=>t.SectorID.ToString()).ToArray();
        string sector = string.Join(",",p);
        DataTable dtMstUser = DBHandler.GetResult("Insert_UserMaster", obj.TypeCode, obj.UserName, obj.Name, password, "N", sector, insertedby);
        if (dtMstUser.Rows.Count > 0)
        {
            obj.UserID = Convert.ToInt32(dtMstUser.Rows[0]["UserID"].ToString());
        }
        else
        {
            return null;
        }
        return obj;
    }

    public Mst_User UpdateUser(Mst_User obj)
    {
        int updatedby = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID].ToString());
        string[] p = obj.ListSector.Select(t => t.SectorID.ToString()).ToArray();
        string sector = string.Join(",", p);
        DataTable dtMstUser = DBHandler.GetResult("Update_UserMaster",obj.UserID, obj.TypeCode, obj.UserName, obj.Name, sector, updatedby);
        if (dtMstUser.Rows.Count > 0)
        {
            obj.UserID = Convert.ToInt32(dtMstUser.Rows[0]["UserID"].ToString());
        }
        else
        {
            return null;
        }
        return obj;
    }

    public string DeleteUser(string UserID)
    {
        string str = "";
        int CurrentUser = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID].ToString());
        //Mst_User um = new Mst_User();
        if (CurrentUser != Convert.ToInt32(UserID))
        {
            DataTable dtMstUser = DBHandler.GetResult("Delete_UserMaster", UserID);
            str = "Y";
        }
        else
        {
            str = "N";
        }  
        return str ;
    }
}