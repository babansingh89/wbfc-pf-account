﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using WebUtility;
using DataAccess;

/// <summary>
/// Summary description for SiteUtility
/// </summary>
public class SiteUtility
{
    public static void SetImageLink(TableRow tr, string ID, string EditPage, string EditImage, string EditTitle)
    {
        EditPage = EditPage + "?ID=" + ID;

        string CellImage = "<a href='" + EditPage + "'><img border = '0' src='Images/" + EditImage + "' title='" + EditTitle + "'/></a>";
        WebUtility.Utility.AddTableCell(tr, CellImage).Style.Add("text-align", "center");
    }

}