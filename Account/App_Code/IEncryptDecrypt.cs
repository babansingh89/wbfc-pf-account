﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IEncryptDecrypt
/// </summary>
public interface IEncryptDecrypt
{
    string Encrypt(string text);
    string Decrypt(string cipherText);
}