﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Sector_Type.aspx.cs" Inherits="Sector_Type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("input").focus(function () {
                $(this).css("background-color", "#cccccc");
            });
            $("input").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

            $("select").focus(function () {
                $(this).css("background-color", "#cccccc");
            });

            $("select").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

            if (document.getElementById("txtSectorcode").disabled == false) {
                $("#txtSectorcode").focus();
            }

            else {
                $("#txtsectorname").focus();
            }
            
            $('#txtSectorcode').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#txtSectorcode').val().trim() == '') {
                        alert('Please Type Group ID');
                        $('#txtSectorcode').focus();
                        return false;
                    }
                    else {
                        DuplicateCheckSectorCode();
                        return false;
                    }
                }
            });


            $('#txtSectorcode').keydown(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 9) {
                    if ($('#txtSectorcode').val().trim() == '') {
                        alert('Please Type Group ID');
                        $('#txtSectorcode').focus();
                        return false;
                    }
                    else {
                        DuplicateCheckSectorCode();
                        return false;
                    }
                }
            });


            $('#txtsectorname').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#txtsectorname').val().trim() == '') {
                        alert('Please Type Sector Name');
                        $('#txtsectorname').focus();
                        return false;
                    }
                    else {
                        $('#cmdSave').focus();
                        return false;
                    }
                }
            });


        });

        function DuplicateCheckSectorCode() {
            var SectorID = '';
            if ($("#txtSectorcode").val().trim() == '') {
                return false;
            }
            $(".loading-overlay").show();
            if ($("#hdnSectorID").val() == '') {
                SectorID = '0';
            }
            else { SectorID = $("#hdnSectorID").val(); }
            var W = "{SectorCode:'" + $("#txtSectorcode").val() + "',SectorID:'" + SectorID + "'}";
            $.ajax({
                type: "POST",
                url: "Sector_Type.aspx/GET_DupChkSectorCode",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    var GroupID = jQuery.parseJSON(data.d);
                    if (GroupID != '') {
                        alert('Sector Code Already Exists!');
                        $(".loading-overlay").hide();
                        $("#txtSectorcode").val('');
                        $("#txtSectorcode").focus();
                        return false;
                    }
                    else { $(".loading-overlay").hide(); $('#txtsectorname').focus(); return false; }

                },
            });
        }


        function beforeSave() {
            $("#frmEcom").validate();
            $("#txtSectorcode").rules("add", { required: true, messages: { required: "Please enter Sector Code" } });
            $("#txtsectorname").rules("add", { required: true, messages: { required: "Please enter Sector Name" } });
            $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please select Unit Name" } });
          

        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }


</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
     <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>SECTOR TYPE</td>
            </tr>
        </table>
        <br />

         <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>


         <asp:TextBox ID="hdnSectorID" Width="100" ClientIDMode="Static" runat="server" style="display:none"></asp:TextBox>

        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    
                                    <td class="labelCaption">SectorCode  &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSectorcode" autocomplete="off" placeholder="Please Enter a New Sector Code"  MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                                <tr>
                                    
                                    <td class="labelCaption">SectorName &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtsectorname" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    </tr>
                            </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                              <tr>
                                    
                                    <td class="labelCaption">SectorCode  &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSectorcode" autocomplete="off" MaxLength="100" Enabled="false" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("SectorCode") %>' ></asp:TextBox>
                                    </td>
                                    </tr>
                                <tr>
                                    
                                    <td class="labelCaption">SectorName &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtsectorname" autocomplete="off" MaxLength="100" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("SectorName") %>' ></asp:TextBox>
                                    </td>
                                    </tr>
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="SectorID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("SectorID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("SectorID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("SectorID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                                
                                <asp:BoundField DataField="SectorCode" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sector Code" />                                
                                <asp:BoundField DataField="SectorName" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sector Name" />                                
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

