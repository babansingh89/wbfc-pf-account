﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BondInttAndVoucherCreation.aspx.cs" Inherits="BondInttAndVoucherCreation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="js/BondInttAndVoucherCreation.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
     <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Bond Installment and Voucher Creation</td>
            </tr>
        </table>        
        <br/>
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
              <tr>      
                <td></td>  
                <td></td>        
                      <td align="center" colspan="2" style="font-size:15px;">
                     New Allotment&nbsp;&nbsp;&nbsp;&nbsp;<span>:</span>&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chckNewAllotment" runat="server" />
                </td> 
                 <td  align="right" style="font-size:15px;"></td> 
                <td></td>              
            </tr>
             <tr>
                <td align="right" style="font-size:15px;">Scheme ID&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span>  </td>
                 <td><span>:</span> </td>
                 <td> <asp:TextBox ID="txtSchemeID" autocomplete="off" Enabled="false" style="text-transform:uppercase;"  Width="200px"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                        <asp:TextBox ID="txthdnSchemeID" autocomplete="off" Width="10px" style="text-transform:uppercase;display: none "  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>      
                 </td>

                  <td style="font-size:15px;" align="right">Allotment Date<span class="require">*&nbsp;&nbsp;</span> </td>
                 <td><span>:</span> </td> 
                <td>                  
                      <asp:TextBox ID="txtAllotmentDate" autocomplete="off" Enabled="false"  Width="200px" placeholder="dd/MM/yyyy" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>                          
                
            </tr>       
            <tr>
                <td style="font-size:15px;" align="right">For Period <span class="require">*&nbsp;&nbsp;</span>  </td>
                <td><span>:</span></td> 
                <td>                  
                      <asp:TextBox ID="txtForPeriod" autocomplete="off" MaxLength="6" Width="200px" placeholder="YYYYMM" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>   
                
                  <td style="font-size:15px;"   align="right" >Bank&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span>  </td>
                 <td><span>:</span>  </td> 
                <td>                
                    <asp:TextBox ID="txtBank" autocomplete="off" Width="200px" style="text-transform:uppercase;"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>  
                     <asp:TextBox ID="txthdnBank" autocomplete="off"  Width="10px" style="text-transform:uppercase;display: none "  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>                   
                </td>                                
                                  
            </tr>      
                      
            <tr>      
                <td></td>  
                <td></td>        
               <td align="center" style="font-size:15px;">
                      
                </td> 
                <td> <asp:Button ID="btnCreate" autocomplete="off" runat="server" Text="Create"  Width="100px" CssClass="save-button" OnClick="btnCreate_click" OnClientClick='javascript: return btnCreateValidation()'/>
                       <asp:Button ID="btnCancel" autocomplete="off" runat="server" Text="Cance; "  Width="100px" CssClass="save-button" OnClick="btnCancel_Click" /></td> 
                <td></td>              
            </tr>        
        </table>        
    </div>
</asp:Content>

