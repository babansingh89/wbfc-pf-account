﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_Job_Documents.aspx.cs" Inherits="LMN_Job_Documents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function beforeSave() {
            $("#frmEcom").validate();

            $("#Ddljobid").rules("add", { required: true, messages: { required: "Please Select Job ID" } });
            $("#DdlDocumentypeid").rules("add", { required: true, messages: { required: "Please Select Document type ID" } });
            $("#DdlCompulsoryFlag").rules("add", { required: true, messages: { required: "Please select CompulsoryFlag" } });
            
        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
</script>

   
    <style type="text/css">
        .auto-style1 {
            font-family: Segoe UI;
            font-size: 12px;
            color: Navy;
            text-align: left;
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Job Document</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                 
                                <tr>
                                    <td class="labelCaption" runat="server" autocomplete="off" >Job ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddljobid" autocomplete="off" runat="server" Width="210px" AppendDataBoundItems="true" AutoPostBack="true" DataSource='<%# drpload1() %>'
                                            DataValueField="JobID"
                                            DataTextField="JobDescription">
                                            <asp:ListItem Text="(Select Item)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        <tr>
                                    <td class="labelCaption">Document Type ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlDocumentypeid" autocomplete="off" runat="server" Width="210px" AppendDataBoundItems="true" AutoPostBack="true" DataSource='<%# drpload() %>'
                                            DataTextField="DocumentTypeDesc"
                                            DataValueField="DocumentTypeID">
                                             <asp:ListItem Text="(Select Item)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        <tr>
                                    <td class="labelCaption">Compulsory Flag &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlCompulsoryFlag" autocomplete="off" runat="server" Width="210px" AppendDataBoundItems="true" AutoPostBack="true">
                                             <asp:ListItem Text="(Select Item)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="Y">yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                       
                                    </td>
                                    </tr>
                                  </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                  <tr>
                                    <td class="labelCaption">Job ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddljobid" autocomplete="off" runat="server" Width="210px" AppendDataBoundItems="true" AutoPostBack="true">
                                             <asp:ListItem Text="(Select Item)" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        <tr>
                                    <td class="labelCaption">Document Type ID &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlDocumentypeid" autocomplete="off" runat="server" Width="210px" AppendDataBoundItems="true" AutoPostBack="true">
                                             <asp:ListItem Text="(Select Item)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                        <tr>
                                    <td class="labelCaption">Compulsory Flag &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DdlCompulsoryFlag" autocomplete="off" runat="server" Width="210px" AppendDataBoundItems="true" AutoPostBack="true">
                                             <asp:ListItem Text="(Select Item)" Value=""></asp:ListItem>
                                            <asp:ListItem Value="Y">yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                       
                                    </td>
                                    </tr>
                                 
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>

            <tr>
                <td colspan="4" class="auto-style1">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <div style="overflow-y: scroll;  width: 100%">
                         <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="JobID" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("JobID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("JobID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("JobID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                                
                                <asp:BoundField DataField="JobID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Job ID" />    
                                <asp:BoundField DataField="DocumentTypeID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Document Type ID" />
                                <asp:BoundField DataField="CompulsoryFlag" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Compulsory Flag" />                            
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            </table>
    </div>
</asp:Content>

