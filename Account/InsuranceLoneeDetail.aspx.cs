﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;    
using System.Globalization;
using System.Web.Services;
using System.Data.SqlClient;
using System.Threading;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

public partial class InsuranceLoneeDetail : System.Web.UI.Page
{
    protected string pay_current_Date = "";
    protected string YearMonth = "";
    protected string h_code = "";
    protected string s_type = "";
    static string hdnInsIDs = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack) {
            FirstGridViewRow();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    private void FirstGridViewRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        //dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("Col1", typeof(string)));
        dt.Columns.Add(new DataColumn("Col2", typeof(string)));
        dt.Columns.Add(new DataColumn("Col3", typeof(string)));
        dt.Columns.Add(new DataColumn("Col4", typeof(string)));
        dt.Columns.Add(new DataColumn("Col5", typeof(string)));
        dt.Columns.Add(new DataColumn("Col6", typeof(string)));
        dt.Columns.Add(new DataColumn("Col7", typeof(string)));
        dt.Columns.Add(new DataColumn("Col8", typeof(string)));
        dt.Columns.Add(new DataColumn("Col9", typeof(string)));
        dt.Columns.Add(new DataColumn("Col10", typeof(string)));
        dr = dt.NewRow();
        //dr["RowNumber"] = 1;
        dr["Col1"] = string.Empty;
        dr["Col2"] = string.Empty;
        dr["Col3"] = string.Empty;
        dr["Col4"] = string.Empty;
        dr["Col5"] = string.Empty;
        dr["Col6"] = string.Empty;
        dr["Col7"] = string.Empty;
        dr["Col8"] = string.Empty;
        dr["Col9"] = string.Empty;
        dr["Col10"] = string.Empty;
        dt.Rows.Add(dr);

        ViewState["CurrentTable"] = dt;


        grvInsuDetails.DataSource = dt;
        grvInsuDetails.DataBind();


        DataTable dt1 = new DataTable();
        DataRow dr1 = null;
        dt1.Columns.Add(new DataColumn("Cols1", typeof(string)));
        dt1.Columns.Add(new DataColumn("Cols2", typeof(string)));
        dt1.Columns.Add(new DataColumn("Cols3", typeof(string)));
        dt1.Columns.Add(new DataColumn("Cols4", typeof(string)));
        dt1.Columns.Add(new DataColumn("Cols5", typeof(string)));
        dt1.Columns.Add(new DataColumn("Cols6", typeof(string)));
        dt1.Columns.Add(new DataColumn("Cols7", typeof(string)));
        dt1.Columns.Add(new DataColumn("Cols8", typeof(string)));
        dt1.Columns.Add(new DataColumn("Cols9", typeof(string)));
        dr1 = dt1.NewRow();
        dr1["Cols1"] = string.Empty;
        dr1["Cols2"] = string.Empty;
        dr1["Cols3"] = string.Empty;
        dr1["Cols4"] = string.Empty;
        dr1["Cols5"] = string.Empty;
        dr1["Cols6"] = string.Empty;
        dr1["Cols7"] = string.Empty;
        dr1["Cols8"] = string.Empty;
        dr1["Cols9"] = string.Empty;
        dt1.Rows.Add(dr1);
        ViewState["CurrentTable1"] = dt1;
        grvInsuMaster.DataSource = dt1;
        grvInsuMaster.DataBind();
        

        DropDownList DrpPropType = (DropDownList)grvInsuDetails.Rows[0].Cells[1].FindControl("DrpPropType");
        DrpPropType.Focus();
        Button btnAdd = (Button)grvInsuDetails.FooterRow.Cells[7].FindControl("ButtonAdd");
        Page.Form.DefaultFocus = btnAdd.ClientID;
                
    }

    protected DataTable drpload()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Acc_VchInstType");
            return dt;         
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected DataTable LoaneeStatus()
    {
        try
        {
            DataTable dtStatus = DBHandler.GetResult("Loanee_Status");
            return dtStatus;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected DataTable PropertyType()
    {
        try
        {
            DataTable dtProType = DBHandler.GetResult("Acc_PropertyType");
            return dtProType;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected DataTable PolicyType()
    {
        try
        {
            DataTable dtPolicy = DBHandler.GetResult("Acc_PolicyType");
            return dtPolicy;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string[] GET_InsDetonLoaneeCode(string LoaneeCode)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_LoaneeOnInsuraneDet", LoaneeCode, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["LOANEE_NAME1"].ToString(), dtRow["LOANEE_UNQ_ID"].ToString()));
        }
        return result.ToArray();
    }

 
    [WebMethod]
    public static string[] LoaneeAutoCompleteData(string LoaneeCode)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_LoaneeCodeOnInsu", LoaneeCode, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["LOANEE_NAME1"].ToString(), dtRow["LOANEE_UNQ_ID"].ToString()));
        }
        return result.ToArray();
    }

    [WebMethod]
    public static Loanee[] GET_LoaneeName(string LoaneeUNQID)
    {
        List<Loanee> Detail = new List<Loanee>();

        DataTable dtGetData = DBHandler.GetResult("Acc_Loanee_Name", Convert.ToInt64(LoaneeUNQID));
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            Loanee DataObj = new Loanee();
            DataObj.Loanee_Name = dtRow["LOANEE_NAME"].ToString();
            Detail.Add(DataObj);

        }
        return Detail.ToArray();
    }
    public class Loanee 
    {
        public string Loanee_Name { get; set; }
    }

    [WebMethod]
    public static string[] InsCodeAutoCompleteData(string InsCode)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_InsCode", InsCode);
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["INS_CODE"].ToString(), dtRow["INS_ID"].ToString()));
        }
        return result.ToArray();
    }

    [WebMethod]
    public static InsNameAddress[] GET_InsNameAddress(int InsID)
    {
        List<InsNameAddress> Detail1 = new List<InsNameAddress>();

        DataTable dtGetData = DBHandler.GetResult("Acc_Ins_Name_Address", Convert.ToInt64(InsID));
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            InsNameAddress DataObj = new InsNameAddress();
            DataObj.InsName = dtRow["INS_NAME"].ToString();
            DataObj.Address1 = dtRow["INS_ADD1"].ToString();
            DataObj.Address2 = dtRow["INS_ADD2"].ToString();
            DataObj.Address3 = dtRow["INS_ADD3"].ToString();
            Detail1.Add(DataObj);
          
        }
        return Detail1.ToArray();
    }
    public class InsNameAddress
    {
        public string InsName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            HiddenField hdnLoaneeID = (HiddenField)dv.FindControl("hdnLoaneeID");
            string loaneeenqid = hdnLoaneeID.Value;
            if (loaneeenqid == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Select a Loanee .')</script>");
                return;
            }
            //PopulateGrid();

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
        
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdShow_Click(object sender, EventArgs e)
    {
        try
        {
            //Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
            int DELROW = 0;
            DataTable dtGetData = DBHandler.GetResult("Get_InsuranceDetShow", Convert.ToInt64(hdnInsIDs), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()));

            if (dtGetData.Rows.Count > 0)
            {
                DataTable dtCurrentTable1 = (DataTable)ViewState["CurrentTable1"];
                DataRow drCurrentRow1 = null;
                int rowIndex1 = 0;
                int i = 1;
                foreach (DataRow dtRow1 in dtGetData.Rows)
                {
                    if (ViewState["CurrentTable1"] != null)
                    {
                        TextBox txtMstLoanees = (TextBox)grvInsuMaster.Rows[rowIndex1].Cells[1].FindControl("txtMstLoanees");
                        TextBox txtMstLoaneesID = (TextBox)grvInsuMaster.Rows[rowIndex1].Cells[2].FindControl("txtMstLoaneesID");
                        TextBox txtMstInsCode = (TextBox)grvInsuMaster.Rows[rowIndex1].Cells[3].FindControl("txtMstInsCode");
                        TextBox txtMstInsID = (TextBox)grvInsuMaster.Rows[rowIndex1].Cells[4].FindControl("txtMstInsID");
                        TextBox txtMstInsName = (TextBox)grvInsuMaster.Rows[rowIndex1].Cells[5].FindControl("txtMstInsName");
                        TextBox txtMstAddress = (TextBox)grvInsuMaster.Rows[rowIndex1].Cells[6].FindControl("txtMstAddress");
                        DropDownList ddlMstStatus = (DropDownList)grvInsuMaster.Rows[rowIndex1].Cells[7].FindControl("ddlMstStatus");
                        DropDownList DrpInsPaids = (DropDownList)grvInsuMaster.Rows[rowIndex1].Cells[8].FindControl("DrpInsPaids");
                        TextBox txtMstRowID = (TextBox)grvInsuMaster.Rows[rowIndex1].Cells[9].FindControl("txtMstRowID");
                        drCurrentRow1 = dtCurrentTable1.NewRow();

                        dtCurrentTable1.Rows[i - 1]["Cols1"] = Convert.ToString(dtRow1["LOANEE_NAME1"]);
                        dtCurrentTable1.Rows[i - 1]["Cols2"] = Convert.ToInt64(dtRow1["LOANEE_UNQ_ID"]);
                        dtCurrentTable1.Rows[i - 1]["Cols3"] = dtRow1["INS_CODE"].ToString();
                        dtCurrentTable1.Rows[i - 1]["Cols4"] = Convert.ToString(dtRow1["INS_ID"]);  
                        dtCurrentTable1.Rows[i - 1]["Cols5"] = Convert.ToString(dtRow1["INS_NAME"]);
                        dtCurrentTable1.Rows[i - 1]["Cols6"] = dtRow1["INS_ADD1"].ToString();
                        dtCurrentTable1.Rows[i - 1]["Cols7"] = Convert.ToString(dtRow1["L_STATUS"]);    
                        dtCurrentTable1.Rows[i - 1]["Cols8"] = Convert.ToString(dtRow1["INS_PAID"]);    
                        dtCurrentTable1.Rows[i - 1]["Cols9"] = Convert.ToInt64(dtRow1["ROW_ID"]);
                    }

                    rowIndex1++;
                    i = i + 1;
                    DELROW = i + 1;
                    if (dtGetData.Rows.Count != i - 1)
                    {
                        dtCurrentTable1.Rows.Add(drCurrentRow1);
                    }
                    ViewState["CurrentTable1"] = dtCurrentTable1;
                    grvInsuMaster.DataSource = dtCurrentTable1;
                    grvInsuMaster.DataBind();
                    SetPreviousData1();
                }
            }

            DataTable dtGridData = DBHandler.GetResult("Get_InsuranceDetGridShow", Convert.ToInt64(hdnInsIDs), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()));
            if (dtGridData.Rows.Count > 0)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                int rowIndex = 0;
                int i = 1;
                foreach (DataRow dtRow in dtGridData.Rows)
                {
                    if (ViewState["CurrentTable"] != null)
                    {
                        //if (i == 1)
                        //{
                        TextBox txtLoanees = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[1].FindControl("txtLoanees");
                        TextBox txtLoaneesID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[2].FindControl("txtLoaneesID");
                        DropDownList DrpPropType = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[3].FindControl("DrpPropType");
                        DropDownList drpPolicyType = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[4].FindControl("drpPolicyType");
                        TextBox txtPropertyAmt = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[5].FindControl("txtPropertyAmt");
                        TextBox txtPlocyNo = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[6].FindControl("txtPlocyNo");
                        TextBox txtPolicyDate = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[7].FindControl("txtPolicyDate");
                        TextBox txtPolicyExpDate = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[8].FindControl("txtPolicyExpDate");
                        TextBox txtPremiumAmt = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[9].FindControl("txtPremiumAmt");
                        TextBox txtDetRowID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[10].FindControl("txtDetRowID");
                        drCurrentRow = dtCurrentTable.NewRow();
                        
                        dtCurrentTable.Rows[i - 1]["Col1"] = Convert.ToString(dtRow["LOANEE_NAME1"]);
                        dtCurrentTable.Rows[i - 1]["Col2"] = Convert.ToInt64(dtRow["LOANEE_UNQ_ID"]);
                        dtCurrentTable.Rows[i - 1]["Col3"] = dtRow["PROPERTY_TYPE"].ToString();
                        dtCurrentTable.Rows[i - 1]["Col4"] = dtRow["POLICY_TYPE"].ToString();
                        dtCurrentTable.Rows[i - 1]["Col5"] =dtRow["PROPERTY_AMT"];
                        dtCurrentTable.Rows[i - 1]["Col6"] = dtRow["POLICY_NO"].ToString();
                        dtCurrentTable.Rows[i - 1]["Col7"] = dtRow["POLICY_DT"];// Convert.ToDateTime(dtRow["POLICY_DT"]).ToShortDateString();
                        dtCurrentTable.Rows[i - 1]["Col8"] = dtRow["POLICY_EXPDT"];// Convert.ToDateTime(dtRow["POLICY_EXPDT"]).ToShortDateString();  
                        dtCurrentTable.Rows[i - 1]["Col9"] = dtRow["PREMIUM_AMT"];
                        dtCurrentTable.Rows[i - 1]["Col10"] = Convert.ToInt64(dtRow["ROW_ID"]);
                    }

                    rowIndex++;
                    i = i + 1;
                    DELROW = i + 1;
                    if (dtGridData.Rows.Count != i-1)
                    {
                        dtCurrentTable.Rows.Add(drCurrentRow);
                    }
                    ViewState["CurrentTable"] = dtCurrentTable;
                    grvInsuDetails.DataSource = dtCurrentTable;
                    grvInsuDetails.DataBind();
                    SetPreviousData();
                    
                }
                
                
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdShowCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    

    // New Coding Start Her

    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["CurrentTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox txtLoanees = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[1].FindControl("txtLoanees");
                    TextBox txtLoaneesID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[2].FindControl("txtLoaneesID");
                    DropDownList DrpPropType = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[3].FindControl("DrpPropType");
                    DropDownList drpPolicyType = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[4].FindControl("drpPolicyType");
                    TextBox txtPropertyAmt = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[5].FindControl("txtPropertyAmt");
                    TextBox txtPlocyNo = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[6].FindControl("txtPlocyNo");
                    TextBox txtPolicyDate = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[7].FindControl("txtPolicyDate");
                    TextBox txtPolicyExpDate = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[8].FindControl("txtPolicyExpDate");
                    TextBox txtPremiumAmt = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[9].FindControl("txtPremiumAmt");
                    TextBox txtDetRowID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[10].FindControl("txtDetRowID");
                    // drCurrentRow["RowNumber"] = i + 1;
                    //grvInsuDetails.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                    txtLoanees.Text = dt.Rows[i]["Col1"].ToString();
                    txtLoaneesID.Text = dt.Rows[i]["Col2"].ToString();

                    DrpPropType.Text = dt.Rows[i]["Col3"].ToString();
                    drpPolicyType.Text = dt.Rows[i]["Col4"].ToString();

                    txtPropertyAmt.Text = dt.Rows[i]["Col5"].ToString();
                    txtPlocyNo.Text = dt.Rows[i]["Col6"].ToString();
                    txtPolicyDate.Text = dt.Rows[i]["Col7"].ToString();
                    txtPolicyExpDate.Text = dt.Rows[i]["Col8"].ToString();
                    txtPremiumAmt.Text = dt.Rows[i]["Col9"].ToString();
                    txtDetRowID.Text = dt.Rows[i]["Col10"].ToString();
                    rowIndex++;
                }
            }
        }
    }

    protected void grvInsuMaster_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SetRowData1();
        if (ViewState["CurrentTable1"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable1"];
            DataRow drCurrentRow = null;
            int rowIndex = Convert.ToInt32(e.RowIndex);
            if (dt.Rows.Count > 1)
            {
                string RowID = ((TextBox)grvInsuDetails.Rows[e.RowIndex].FindControl("txtMstRowID")).Text;
                string LoaneeIDs = ((TextBox)grvInsuDetails.Rows[e.RowIndex].FindControl("txtMstLoanees")).Text;
                if (RowID != "")
                {
                    DataTable dtGetData = DBHandler.GetResult("Delete_DAT_LoaneeInsuranceDetil", Convert.ToInt64(RowID), Convert.ToInt64(LoaneeIDs), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()), "Master");
                }

                dt.Rows.Remove(dt.Rows[rowIndex]);
                drCurrentRow = dt.NewRow();
                ViewState["CurrentTable1"] = dt;
                grvInsuMaster.DataSource = dt;
                grvInsuMaster.DataBind();

                for (int i = 0; i < grvInsuMaster.Rows.Count - 1; i++)
                {
                    grvInsuMaster.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                }
                SetPreviousData1();
            }
        }
    }

    protected void grvInsuDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        SetRowData();
        if (ViewState["CurrentTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;
            int rowIndex = Convert.ToInt32(e.RowIndex);
            if (dt.Rows.Count > 1)
            {
                string RowID = ((TextBox)grvInsuDetails.Rows[e.RowIndex].FindControl("txtDetRowID")).Text;
                string LoaneeIDs = ((TextBox)grvInsuDetails.Rows[e.RowIndex].FindControl("txtLoaneesID")).Text;
                if (RowID != "") {
                    DataTable dtGetData = DBHandler.GetResult("Delete_DAT_LoaneeInsuranceDetil", Convert.ToInt64(RowID), Convert.ToInt64(LoaneeIDs), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()), "Detail");
                }
                dt.Rows.Remove(dt.Rows[rowIndex]);
                drCurrentRow = dt.NewRow();

                ViewState["CurrentTable"] = dt;
                grvInsuDetails.DataSource = dt;
                grvInsuDetails.DataBind();


                for (int i = 0; i < grvInsuDetails.Rows.Count - 1; i++)
                {
                    grvInsuDetails.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                }
                SetPreviousData();
            }
        }
    }

    private void SetRowData()
    {
        int rowIndex = 0;

        if (ViewState["CurrentTable"] != null)
        {
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;
            if (dtCurrentTable.Rows.Count > 0)
            {
                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                {
                    TextBox txtLoanees = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[1].FindControl("txtLoanees");
                    TextBox txtLoaneesID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[2].FindControl("txtLoaneesID");
                    DropDownList DrpPropType = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[3].FindControl("DrpPropType");
                    DropDownList drpPolicyType = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[4].FindControl("drpPolicyType");
                    TextBox txtPropertyAmt = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[5].FindControl("txtPropertyAmt");
                    TextBox txtPlocyNo = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[6].FindControl("txtPlocyNo");
                    TextBox txtPolicyDate = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[7].FindControl("txtPolicyDate");
                    TextBox txtPolicyExpDate = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[8].FindControl("txtPolicyExpDate");
                    TextBox txtPremiumAmt = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[9].FindControl("txtPremiumAmt");
                    TextBox txtDetRowID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[10].FindControl("txtDetRowID");
                    drCurrentRow = dtCurrentTable.NewRow();
                    //drCurrentRow["RowNumber"] = i + 1;
                    dtCurrentTable.Rows[i - 1]["Col1"] = txtLoanees.Text;
                    dtCurrentTable.Rows[i - 1]["Col2"] = txtLoaneesID.Text;
                    dtCurrentTable.Rows[i - 1]["Col3"] = DrpPropType.SelectedValue;
                    dtCurrentTable.Rows[i - 1]["Col4"] = drpPolicyType.SelectedValue;
                    dtCurrentTable.Rows[i - 1]["Col5"] = txtPropertyAmt.Text;
                    dtCurrentTable.Rows[i - 1]["Col6"] = txtPlocyNo.Text;
                    dtCurrentTable.Rows[i - 1]["Col7"] = txtPolicyDate.Text;
                    dtCurrentTable.Rows[i - 1]["Col8"] = txtPolicyExpDate.Text;
                    dtCurrentTable.Rows[i - 1]["Col9"] = txtPremiumAmt.Text;
                    dtCurrentTable.Rows[i - 1]["Col10"] = txtDetRowID.Text;
                    rowIndex++;
                }

                ViewState["CurrentTable"] = dtCurrentTable;
                grvInsuDetails.DataSource = dtCurrentTable;
                grvInsuDetails.DataBind();
            }
        }
        else
        {
            Response.Write("ViewState is null");
        }
        SetPreviousData();
    }

    private void SetRowData1()
    {
        int rowIndex = 0;

        if (ViewState["CurrentTable1"] != null)
        {
            DataTable dtCurrentTable1 = (DataTable)ViewState["CurrentTable1"];
            DataRow drCurrentRow1 = null;
            if (dtCurrentTable1.Rows.Count > 0)
            {
                for (int i = 1; i <= dtCurrentTable1.Rows.Count; i++)
                {
                    TextBox txtMstLoanees = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[1].FindControl("txtMstLoanees");
                    TextBox txtMstLoaneesID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[2].FindControl("txtMstLoaneesID");
                    TextBox txtMstInsCode = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[3].FindControl("txtMstInsCode");
                    TextBox txtMstInsID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[4].FindControl("txtMstInsID");
                    TextBox txtMstInsName = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[5].FindControl("txtMstInsName");
                    TextBox txtMstAddress = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[6].FindControl("txtMstAddress");
                    DropDownList ddlMstStatus = (DropDownList)grvInsuMaster.Rows[rowIndex].Cells[7].FindControl("ddlMstStatus");
                    DropDownList DrpInsPaids = (DropDownList)grvInsuMaster.Rows[rowIndex].Cells[8].FindControl("DrpInsPaids");
                    TextBox txtMstRowID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[9].FindControl("txtMstRowID");
                    //TextBox txtDetRowID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[10].FindControl("txtDetRowID");
                    //txtLoaneesID.Attributes["myid"] = "cls_" + i;
                    drCurrentRow1 = dtCurrentTable1.NewRow();
                    //drCurrentRow["RowNumber"] = i + 1;

                    dtCurrentTable1.Rows[i - 1]["Cols1"] = txtMstLoanees.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols2"] = txtMstLoaneesID.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols3"] = txtMstInsCode.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols4"] = txtMstInsID.Text; //drpPolicyType.SelectedValue;
                    dtCurrentTable1.Rows[i - 1]["Cols5"] = txtMstInsName.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols6"] = txtMstAddress.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols7"] = ddlMstStatus.SelectedValue;
                    dtCurrentTable1.Rows[i - 1]["Cols8"] = DrpInsPaids.SelectedValue;
                    dtCurrentTable1.Rows[i - 1]["Cols9"] = txtMstRowID.Text;
                    //dtCurrentTable1.Rows[i - 1]["Cols10"] = txtDetRowID.Text;
                    rowIndex++;
                }

                ViewState["CurrentTable1"] = dtCurrentTable1;
                grvInsuMaster.DataSource = dtCurrentTable1;
                grvInsuMaster.DataBind();
            }
        }
        else
        {
            Response.Write("ViewState is null");
        }
        SetPreviousData1();
    }
    //protected void btnSave_Click(object sender, EventArgs e)
    //{
    //    int UserID;
    //    UserID = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

    //    //HiddenField hdnInsID = (HiddenField)dv.FindControl("hdnInsID"); 
    //    //HiddenField hdnLoaneeID = (HiddenField)dv.FindControl("hdnLoaneeID"); 
    //    TextBox txtLoaneeID = (TextBox)dv.FindControl("txtLoaneeID");
    //    DropDownList ddlStatus = (DropDownList)dv.FindControl("ddlStatus");
    //    TextBox txtInsCodeID = (TextBox)dv.FindControl("txtInsCodeID");
    //    HiddenField hdnInsCodeID = (HiddenField)dv.FindControl("hdnInsCodeID");
    //    DropDownList DrpInsPaid = (DropDownList)dv.FindControl("DrpInsPaid");
                        
    //    string ConString = "";
    //    string msg = "";
    //    ConString = DataAccess.DBHandler.GetConnectionString();

    //    SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());
    //    SqlCommand cmd = new SqlCommand();
    //    SqlTransaction transaction;
    //    db.Open();
    //    transaction = db.BeginTransaction();
    //    try
    //    {
    //        try
    //        {

    //            if (hdnInsID.Value == "")
    //            {
    //                hdnInsID.Value = "0";
    //                cmd = new SqlCommand("Save_MST_LoaneeInsuranceDet " + Convert.ToInt64(hdnInsID.Value) + "," + Convert.ToInt64(txtLoaneeID.Text) + ",'" + ddlStatus.SelectedValue + "'," + Convert.ToInt64(txtInsCodeID.Text) + ",'" + DrpInsPaid.SelectedValue + "'," + Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()) + "," + Convert.ToInt32(UserID) + "", db, transaction);
    //                    cmd.ExecuteNonQuery();
    //                    msg = "Record Save Successfully!";
                    
    //            }
    //            else
    //            {
    //                cmd = new SqlCommand("Exec Save_MST_LoaneeInsuranceDet " + Convert.ToInt64(hdnInsID.Value) + "," + Convert.ToInt64(txtLoaneeID.Text) + ",'" + ddlStatus.SelectedValue + "'," + Convert.ToInt64(txtInsCodeID.Text) + ",'" + DrpInsPaid.SelectedValue + "'," + Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()) + "," + Convert.ToInt32(UserID) + "", db, transaction);
    //                cmd.ExecuteNonQuery();

    //                cmd = new SqlCommand("Exec Delete_DAT_LoaneeInsuranceDetil " + Convert.ToInt64(hdnInsID.Value) + "," + Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()) + "", db, transaction);
    //                cmd.ExecuteNonQuery();
    //                msg = "Record Update Successfully!";
    //            }

    //            //=========================== Detail parts save ==================//

    //            SetRowData();
    //            DataTable table = ViewState["CurrentTable"] as DataTable;

    //            if (table != null)
    //            {
    //                foreach (DataRow row in table.Rows)
    //                {
    //                    string DrpPropType = row.ItemArray[0] as string;
    //                    string drpPolicyType = row.ItemArray[1] as string;
    //                    string txtPropertyAmt = row.ItemArray[2] as string;
    //                    string txtPlocyNo = row.ItemArray[3] as string;
    //                    string txtPolicyDate = row.ItemArray[4] as string;
    //                    string txtPolicyExpDate = row.ItemArray[5] as string;
    //                    string txtPremiumAmt = row.ItemArray[6] as string;

    //                    //if (DrpPropType != null || DrpPropType != "" ||
    //                    //    drpPolicyType != null || drpPolicyType != "" ||
    //                    //    txtPropertyAmt != null || txtPropertyAmt != "" ||
    //                    //    txtPlocyNo != null || txtPlocyNo != "" ||
    //                    //    txtPolicyDate != null || txtPolicyDate != "" ||
    //                    //    txtPolicyExpDate != null || txtPolicyExpDate != "" ||
    //                    //    txtPremiumAmt != null || txtPremiumAmt != "")
    //                    //{

    //                    if (DrpPropType != "" ||
    //                        drpPolicyType != "" ||
    //                        txtPropertyAmt != "" ||
    //                        txtPlocyNo != "" ||
    //                        txtPolicyDate != "" ||
    //                        txtPolicyExpDate != "" ||
    //                        txtPremiumAmt != "")
    //                    {
    //                        DateTime PolicyDate = DateTime.Parse(txtPolicyDate);
    //                        DateTime PolicyExpDate = DateTime.Parse(txtPolicyExpDate);

    //                        cmd = new SqlCommand("Exec Save_DAT_LoaneeInsuranceDetil " + Convert.ToInt64(hdnInsID.Value) + "," + Convert.ToInt64(txtLoaneeID.Text) + ",'" + DrpPropType + "','" + drpPolicyType + "'," + Convert.ToDecimal(txtPropertyAmt) + ",'" + txtPlocyNo + "','" + PolicyDate.ToString("yyyyMMdd") + "','" + PolicyExpDate.ToString("yyyyMMdd") + "','" + Convert.ToDecimal(txtPremiumAmt) + "'," + Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()) + "," + Convert.ToInt32(UserID) + "", db, transaction);
    //                        cmd.ExecuteNonQuery();

    //                    } 
                           
    //                }
    //            }
    //            transaction.Commit();
    //            db.Close();
    //            ClientScript.RegisterStartupScript(this.GetType(), "WBFC", "<script>alert('" + msg + "')</script>");
    //            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
    //        }
    //        catch (SqlException sqlError)
    //        {
    //            transaction.Rollback();
    //            msg = "Records Not Save Successfully ! Data Mismatch";
    //            ClientScript.RegisterStartupScript(this.GetType(), "WBFC", "<script>alert('" + msg + "')</script>");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        transaction.Rollback();
    //        db.Close();
    //        throw new Exception(ex.Message);
    //    }
    //}

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        AddNewRow();
    }

    private void AddNewRow()
    {
        int rowIndex = 0;



        if (ViewState["CurrentTable"] != null)
        {
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;
            if (dtCurrentTable.Rows.Count > 0)
            {
                for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                {
                    TextBox txtLoanees = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[1].FindControl("txtLoanees");
                    TextBox txtLoaneesID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[2].FindControl("txtLoaneesID");
                    DropDownList DrpPropType = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[3].FindControl("DrpPropType");
                    DropDownList drpPolicyType = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[4].FindControl("drpPolicyType");
                    TextBox txtPropertyAmt = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[5].FindControl("txtPropertyAmt");
                    TextBox txtPlocyNo = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[6].FindControl("txtPlocyNo");
                    TextBox txtPolicyDate = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[7].FindControl("txtPolicyDate");
                    TextBox txtPolicyExpDate = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[8].FindControl("txtPolicyExpDate");
                    TextBox txtPremiumAmt = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[9].FindControl("txtPremiumAmt");
                    TextBox txtDetRowID = (TextBox)grvInsuDetails.Rows[rowIndex].Cells[10].FindControl("txtDetRowID");
                    txtLoaneesID.Attributes["myid"] = "cls_" + i;
                    drCurrentRow = dtCurrentTable.NewRow();
                    //drCurrentRow["RowNumber"] = i + 1;
                    
                    dtCurrentTable.Rows[i - 1]["Col1"] = txtLoanees.Text;
                    dtCurrentTable.Rows[i - 1]["Col2"] = txtLoaneesID.Text;
                    dtCurrentTable.Rows[i - 1]["Col3"] = DrpPropType.SelectedValue;
                    dtCurrentTable.Rows[i - 1]["Col4"] = drpPolicyType.SelectedValue;
                    dtCurrentTable.Rows[i - 1]["Col5"] = txtPropertyAmt.Text;
                    dtCurrentTable.Rows[i - 1]["Col6"] = txtPlocyNo.Text;
                    dtCurrentTable.Rows[i - 1]["Col7"] = txtPolicyDate.Text;
                    dtCurrentTable.Rows[i - 1]["Col8"] = txtPolicyExpDate.Text;
                    dtCurrentTable.Rows[i - 1]["Col9"] = txtPremiumAmt.Text;
                    dtCurrentTable.Rows[i - 1]["Col10"] = txtDetRowID.Text;
                    rowIndex++;
                }
                dtCurrentTable.Rows.Add(drCurrentRow);
                ViewState["CurrentTable"] = dtCurrentTable;

                grvInsuDetails.DataSource = dtCurrentTable;
                grvInsuDetails.DataBind();

                DropDownList DrpPropTypeFoucs = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[1].FindControl("DrpPropType");
                DrpPropTypeFoucs.Focus();
               
            }
        }
        else
        {
            Response.Write("ViewState is null");
        }
        SetPreviousData();
    }


    protected void ButtonAddMst_Click(object sender, EventArgs e)
    {
        AddNewRow1();

    }
    private void AddNewRow1()
    {
        int rowIndex = 0;

        if (ViewState["CurrentTable1"] != null)
        {
            DataTable dtCurrentTable1 = (DataTable)ViewState["CurrentTable1"];
            DataRow drCurrentRow1 = null;
            if (dtCurrentTable1.Rows.Count > 0)
            {
                for (int i = 1; i <= dtCurrentTable1.Rows.Count; i++)
                {
                    TextBox txtMstLoanees = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[1].FindControl("txtMstLoanees");
                    TextBox txtMstLoaneesID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[2].FindControl("txtMstLoaneesID");
                    TextBox txtMstInsCode = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[3].FindControl("txtMstInsCode");
                    TextBox txtMstInsID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[4].FindControl("txtMstInsID");
                    TextBox txtMstInsName = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[5].FindControl("txtMstInsName");
                    TextBox txtMstAddress = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[6].FindControl("txtMstAddress");
                    DropDownList ddlMstStatus = (DropDownList)grvInsuMaster.Rows[rowIndex].Cells[7].FindControl("ddlMstStatus");
                    DropDownList DrpInsPaids = (DropDownList)grvInsuMaster.Rows[rowIndex].Cells[8].FindControl("DrpInsPaids");
                    TextBox txtMstRowID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[9].FindControl("txtMstRowID");
                    //txtLoaneesID.Attributes["myid"] = "cls_" + i;
                    drCurrentRow1 = dtCurrentTable1.NewRow();
                    //drCurrentRow["RowNumber"] = i + 1;

                    dtCurrentTable1.Rows[i - 1]["Cols1"] = txtMstLoanees.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols2"] = txtMstLoaneesID.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols3"] = txtMstInsCode.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols4"] = txtMstInsID.Text; //drpPolicyType.SelectedValue;
                    dtCurrentTable1.Rows[i - 1]["Cols5"] = txtMstInsName.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols6"] = txtMstAddress.Text;
                    dtCurrentTable1.Rows[i - 1]["Cols7"] = ddlMstStatus.SelectedValue;
                    dtCurrentTable1.Rows[i - 1]["Cols8"] = DrpInsPaids.SelectedValue;
                    dtCurrentTable1.Rows[i - 1]["Cols9"] = txtMstRowID.Text;
                    rowIndex++;
                }
                dtCurrentTable1.Rows.Add(drCurrentRow1);
                ViewState["CurrentTable1"] = dtCurrentTable1;

                grvInsuMaster.DataSource = dtCurrentTable1;
                grvInsuMaster.DataBind();

                //DropDownList DrpPropTypeFoucs = (DropDownList)grvInsuDetails.Rows[rowIndex].Cells[1].FindControl("DrpPropType");
                //DrpPropTypeFoucs.Focus();

            }
        }
        else
        {
            Response.Write("ViewState is null");
        }
        SetPreviousData1();
    }

    private void SetPreviousData1()
    {
        int rowIndex = 0;
        if (ViewState["CurrentTable1"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable1"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox txtMstLoanees = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[1].FindControl("txtMstLoanees");
                    TextBox txtMstLoaneesID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[2].FindControl("txtMstLoaneesID");
                    TextBox txtMstInsCode = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[3].FindControl("txtMstInsCode");
                    TextBox txtMstInsID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[4].FindControl("txtMstInsID");
                    TextBox txtMstInsName = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[5].FindControl("txtMstInsName");
                    TextBox txtMstAddress = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[6].FindControl("txtMstAddress");
                    DropDownList ddlMstStatus = (DropDownList)grvInsuMaster.Rows[rowIndex].Cells[7].FindControl("ddlMstStatus");
                    DropDownList DrpInsPaids = (DropDownList)grvInsuMaster.Rows[rowIndex].Cells[8].FindControl("DrpInsPaids");
                    TextBox txtMstRowID = (TextBox)grvInsuMaster.Rows[rowIndex].Cells[9].FindControl("txtMstRowID");
                    // drCurrentRow["RowNumber"] = i + 1;
                    //grvInsuDetails.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                    txtMstLoanees.Text = dt.Rows[i]["Cols1"].ToString();
                    txtMstLoaneesID.Text = dt.Rows[i]["Cols2"].ToString();

                    txtMstInsCode.Text = dt.Rows[i]["Cols3"].ToString();
                    txtMstInsID.Text = dt.Rows[i]["Cols4"].ToString();

                    txtMstInsName.Text = dt.Rows[i]["Cols5"].ToString();
                    txtMstAddress.Text = dt.Rows[i]["Cols6"].ToString();
                    ddlMstStatus.Text = dt.Rows[i]["Cols7"].ToString();
                    DrpInsPaids.Text = dt.Rows[i]["Cols8"].ToString();
                    txtMstRowID.Text = dt.Rows[i]["Cols9"].ToString();
                    rowIndex++;
                }
            }
        }   
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_SaveInsurance(string MstInsurance, string DetInsurance)
    {
        string JSonVal = "";
        string ConString = "";
        string result = "Fail";
        string msg = "";
        ConString = DataAccess.DBHandler.GetConnectionString();
        SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());
        SqlCommand cmd = new SqlCommand();
        System.Data.SqlClient.SqlTransaction transaction;
        db.Open();
        transaction = db.BeginTransaction();
        try
        {
            try
            {
                var MstInsurances = new JavaScriptSerializer();
                List<MstValue> subChild = MstInsurances.Deserialize<List<MstValue>>(MstInsurance);

                for (int i = 0; i < subChild.Count; i++)
                {
                    cmd = new SqlCommand("Exec Save_MST_LoaneeInsuranceDet " +
                    subChild[i].MstRowID + "," + subChild[i].LoaneeID + " ,'" + subChild[i].MstStatus + "','" + subChild[i].insID + "','" + subChild[i].InsPaids + "'," + HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID] + "," + HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID] + "", db, transaction);
                    cmd.ExecuteNonQuery();
                }

                var DetInsurances = new JavaScriptSerializer();
                List<DetValue> subChild1 = DetInsurances.Deserialize<List<DetValue>>(DetInsurance);
                for (int i = 0; i < subChild1.Count; i++)
                {
               
                    cmd = new SqlCommand("Exec Save_DAT_LoaneeInsuranceDetil " +
                    subChild1[i].DetRowID + "," + subChild1[i].LoaneeID + " ,'" + subChild1[i].ProType + "','" + subChild1[i].PolType + "','" + subChild1[i].PropertyAmt + "','" + subChild1[i].PlocyNo + "','" + subChild1[i].PolicyDate + "','" + subChild1[i].PolicyExpDate + "','" + subChild1[i].PremiumAmt + "'," + HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID] + "," + HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID] + "", db, transaction);
                    cmd.ExecuteNonQuery();
                }

                transaction.Commit();
                msg = "Record Update Successfully";
                JSonVal = msg.ToJSON();
            }
            catch (SqlException sqlError)
            {
                transaction.Rollback();
                JSonVal = result.ToJSON();
            }
            db.Close();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            db.Close();
            throw new Exception(ex.Message);
        }
        return JSonVal;
    }

    class MstValue
    {
        public string LoaneeID { get; set; }
        public string insID { get; set; }
        public string MstStatus { get; set; }
        public string InsPaids { get; set; }
        public string MstRowID { get; set; }
    }

    class DetValue
    {
        public string LoaneeID { get; set; }
        public string ProType { get; set; }
        public string PolType { get; set; }
        public string PropertyAmt { get; set; }
        public string PlocyNo { get; set; }
        public string PolicyDate { get; set; }
        public string PolicyExpDate { get; set; }
        public string PremiumAmt { get; set; }
        public string DetRowID { get; set; }
    }

    [WebMethod]
    public static string GET_SetInsID(int type, string InsID)
    {
        string msg = "";
        if (type == 0) { hdnInsIDs = ""; }
        else { hdnInsIDs = InsID; }
        return msg;
    }

    protected void CmdDelete_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnInsIDs != "") { 
            DataTable dtGetData = DBHandler.GetResult("Delete_DAT_LoaneeInsuranceDetil", 0, Convert.ToInt64(hdnInsIDs), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()), "Loanee");
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
            }
        }
        catch (Exception ex)
        {

            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}