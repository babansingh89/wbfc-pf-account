﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" ViewStateEncryptionMode="Always" CodeFile="UnlockUser.aspx.cs" Inherits="UnlockUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Unlock User</title>
    <script type="text/css">
        var pageUrl = '<%=ResolveUrl("~/BookingEnquiry.aspx")%>'
        if (typeof WBFDC == 'undefined') {
            WBFDC = {};
        };
    </script>
    <link type="text/css" rel="shortcut icon" href="favicon.ico"/>
    <link rel="icon" type="image/ico" href="favicon.ico"/>
    <link href="css/enquiry.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.12.1.js"></script>

    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css" />

    <script src="js/utility.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker-en-GB.js" type="text/javascript"></script>
    <script src="js/jquery.validate.js" type="text/javascript"></script>
    <script src="js/jquery.ui.datepicker.validation.min.js" type="text/javascript"></script>
      
    <script src="js/json2.js" type="text/javascript"></script>

</head>
<body>
    <form id="enquiry" runat="server">
        <div id="page">
            <div class="wrapper">
                <div class="top">
                    <div class="name">WEST BENGAL FOREST DEVELOPMENT CORP. LTD.</div>
                    <div class="top-right">
                        <div class="top-right-top">
                            <asp:Label ID="lblLoginName" autocomplete="off" runat="server" Text=""></asp:Label>
                        </div>
                        
                    </div>
                </div>
                <div style="padding-left: 10px;">
                    <img alt="img" src="images/line.jpg" />
                </div>
                <div class="clear"></div>
                <div>
                    
                    <div class="bodybg">
                        <div class="body-content">

                            <div align="center" class="enquiry-info">

                                <table class="headingCaption" width="95%" align="center">
                                    <tr>
                                        <td style="font-size: 14px; line-height: 20px;">Unlock User</td>
                                    </tr>
                                </table>
                                <br />
                                <table width="95%" cellpadding="5" class="borderStyle" cellspacing="0">
                                    <tr>
                                        <td>
                                            <table align="center" cellpadding="5" width="95%">
                                                <tr>
                                                    <td colspan="3" class="labelCaption">Enter Your Username and click on Unlock button </td>
                                                </tr>
                                                <tr>
                                                    <td class="labelCaption">User User&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left" colspan="3">
                                                        <asp:TextBox ID="txtUserName" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="labelCaption">
                                                        <hr class="borderStyle" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="center" cellpadding="5" width="95%">

                                                <tr>
                                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                                    <td>&nbsp;</td>
                                                    <td colspan="3" align="right">
                                                        <div style="float: right;">
                                                            <asp:Button ID="cmdUnlock" autocomplete="off" runat="server" Text="Unlock" OnClick="cmdUnlock_Click" ToolTip="Unlock"
                                                                CssClass="DefaultButton unlock" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" align="right">
                                                        <div style="float: right;">
                                                            <a href="Default.aspx" autocomplete="off" runat="server" clientidmode="Static" style="text-decoration: none; font-size:14px;font-family:'Segoe UI';color:navy;font-weight:bold;">Back to Login Page</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
        </div>
        <div class="footerpage">
            <div class="footer">
                <div class="clear"></div>
                <div class="copyright">
                    &copy; copyright 2012 West Bengal Forest Development Corporation Ltd.  
                </div>
            </div>
        </div>
    </form>
</body>
</html>
