﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using System.Configuration;
public partial class RePayment_Schedule : System.Web.UI.Page
{
    public static int IntSheduleID;
    static string hdnLoaneeUniqueId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        GetLoneeDetails();
    }

    private void GetLoneeDetails()
    {
        if (Session[SiteConstants.SSN_LOANEE_ID] == null) { Response.Redirect("MSTAccLoan.aspx"); }
        string LOANEE_ID = Session[SiteConstants.SSN_LOANEE_ID].ToString();
        string SCHEDULE_ID = Session[SiteConstants.SSN_SCHEDULE_ID].ToString();
        string SANCTION_AMT = Session[SiteConstants.SSN_SANCTION_AMT].ToString();
        string REPAYMENT_TERM = Session[SiteConstants.SSN_REPAYMENT_TERM].ToString();
        string REPAYMENT_START_DATE = Session[SiteConstants.SSN_REPAYMENT_START_DATE].ToString();
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dt = DBHandler.GetResult("GET_LOANEE_DETAILS_BY_ID", LOANEE_ID, SectorID);
        hdnLoaneeUniqueId = LOANEE_ID;
        txtLoaneeName.Text = dt.Rows[0]["LOANEE_NAME"].ToString();
        ddlRepaymentTerm.SelectedValue = REPAYMENT_TERM;
        txtSanctionAmount.Text = SANCTION_AMT;
        txtLoaneeName.Enabled = false;
    }

    [WebMethod]
    public static string GET_LoaneeUniqueId()
    {
        string Result = "";
        Result = hdnLoaneeUniqueId;
        return Result;
    }

    //================================Fetch Data For Repayment Schedule===================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetExistData()
    {
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        string SCHEDULE_ID = HttpContext.Current.Session[SiteConstants.SSN_SCHEDULE_ID].ToString();
        string LOANEE_ID = HttpContext.Current.Session[SiteConstants.SSN_LOANEE_ID].ToString();
        try
        {
            if (SCHEDULE_ID != "")
            {
                DataSet dtDetails = DBHandler.GetResults("Get_RepayDetails", SCHEDULE_ID, LOANEE_ID, SectorID);
                return dtDetails.GetXml();
            }
            else
            {
                return "0";
            }
        }
        catch (Exception EX)
        {
           return EX.Message;
        }
    }

    //================================Fetch Data For Repayment Schedule===================================//
    //================================Save Repayment Shedule Data In Database=============================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SaveData(object StrMaster, object StrDetails)
    {
        int UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        string SCHEDULE_ID = HttpContext.Current.Session[SiteConstants.SSN_SCHEDULE_ID].ToString();

        string ConString = DataAccess.DBHandler.GetConnectionString();
        var db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());
        var cmd = new SqlCommand();
        System.Data.SqlClient.SqlTransaction transaction;
        db.Open();
        transaction = db.BeginTransaction();
        try
        {
            var SrJSSerializer = new JavaScriptSerializer();
            //==================================================================================
            List<Details> DetailsData = SrJSSerializer.ConvertToType<List<Details>>(StrDetails);
            List<MasterData> MainData = SrJSSerializer.ConvertToType<List<MasterData>>(StrMaster);
            //==================================================================================
            if (SCHEDULE_ID == "")
            {
                cmd = new SqlCommand("Exec Insert_RepayShedule '" + MainData[0].LoaneeID + "','" + SectorID + "'," + MainData[0].SanctionAmt + ",'" + MainData[0].RepayTerm + "','" + MainData[0].RepayStartDate + "','" + MainData[0].FirstDisburseDate + "'," + MainData[0].MoratPeriod + "," + Convert.ToDecimal(MainData[0].InterestRate) + "," + Convert.ToDecimal(MainData[0].RebateRate) + "," + Convert.ToDecimal(MainData[0].PenaltyRate) + ",'" + MainData[0].EffectiveFrom + "','" + UserID + "'", db, transaction);
                var da = new SqlDataAdapter(cmd);
                var dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    IntSheduleID = Convert.ToInt16(dt.Rows[0]["strID"]);
                }
            }
            else
            {
                cmd = new SqlCommand("Exec Update_RepayShedule " + Convert.ToInt32(SCHEDULE_ID) + ",'" + MainData[0].LoaneeID + "','" + SectorID + "'," + MainData[0].SanctionAmt + ",'" + MainData[0].RepayTerm + "','" + MainData[0].RepayStartDate + "','" + MainData[0].FirstDisburseDate + "'," + MainData[0].MoratPeriod + "," + Convert.ToDecimal(MainData[0].InterestRate) + "," + Convert.ToDecimal(MainData[0].RebateRate) + "," + Convert.ToDecimal(MainData[0].PenaltyRate) + ",'" + MainData[0].EffectiveFrom + "','" + UserID + "'", db, transaction);
                cmd.ExecuteNonQuery();
                IntSheduleID = Convert.ToInt32(SCHEDULE_ID);
            }
            //================================================================================================================================================================================================================================================================================================================================================================================================
            for (int i = 0; i < DetailsData.Count; i++)
            {
                cmd = new SqlCommand("Exec Insert_RepaySheduleDetails " + IntSheduleID + ",'" + DetailsData[i].SerialNo + "','" + DetailsData[i].NoOfInst + "'," + DetailsData[i].InterAmt + "," + UserID + "", db, transaction);
                cmd.ExecuteNonQuery();
            }
            //================================================================================================================================================================================================================================================================================================================================================================================================
            transaction.Commit();
        }
        catch (Exception)
        {
            transaction.Rollback();
            return "0";
        }
        return "Record Save Successfully..!";
    }

    class MasterData
    {
        public string LoaneeID { get; set; }
        public string SanctionAmt { get; set; }
        public string RepayTerm { get; set; }
        public string RepayStartDate { get; set; }
        public string FirstDisburseDate { get; set; }
        public string MoratPeriod { get; set; }
        public string InterestRate { get; set; }
        public string RebateRate { get; set; }
        public string PenaltyRate { get; set; }
        public string EffectiveFrom { get; set; }
    }
     class Details
     {
         public string SerialNo { get; set; }
         public string NoOfInst { get; set; }
         public string InterAmt { get; set; }
     }
}