﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ACCOUNT_BRS.aspx.cs" Inherits="ACCOUNT_BRS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script type="text/javascript">
    var hdnSlipID = '';
    function searchback() {//alert(window.opener)
        var grdRow = tbl.parentNode.parentNode;
        var grdControl = grdRow.getElementsByTagName("input");

        // LOOP THROUGH EACH INPUT CONTROL IN THE GRIDVIEW.
        for (var i = 0; i < grdControl.length; i++) {
            if (grdControl[i].type = 'text') {
                if (grdControl[i].value == $('#txtSearchInstrumentNo').val().toUpperCase()) {
                    if (grdControl[i].value != '') {
                        grdControl[i].style.backgroundColor = 'Yellow';
                    }
                    break;
                    return false; 
                }
                else {
                    grdControl[i].style.backgroundColor = 'White';
                }
            }
        }
        //var GridFromIndentNo = document.getElementById("tbl");
        //var ValFromIndentNo = '';
        //if ($('#txtSearchInstrumentNo').val().trim() == '') {
        //    alert("Please Enter Instrument No.");
        //    $('#txtSearchInstrumentNo').val('');
        //    $("#txtSearchInstrumentNo").focus();
        //    return false;      
        //}
        //if ($('#InstrumentNo').val() != '') {
        //    for (var row = 1; row < GridFromIndentNo.rows.length; row++) {
        //        var GridFromIndentNo_Cell = GridFromIndentNo.rows[row].cells[5]; 
        //        var valueFromIndentNo = GridFromIndentNo_Cell.innerText.toString();
        //        alert(GridFromIndentNo_Cell);
        //        if (valueFromIndentNo == $('#txtSearchInstrumentNo').val()) {
        //            GridFromIndentNo.rows[i + 1].style.backgroundColor = 'Yellow';
        //            alert('yes');
        //            break;
        //        }
        //    }
        //}
    }

    $(document).ready(function () {
        $("#txtSearchInstrumentNo").keyup(function (event) {
            var grdRow = tbl.parentNode.parentNode;
            var grdControl = grdRow.getElementsByTagName("input");

            // LOOP THROUGH EACH INPUT CONTROL IN THE GRIDVIEW.
            for (var i = 0; i < grdControl.length; i++) {
                if (grdControl[i].type = 'text') {
                    if (grdControl[i].value == $('#txtSearchInstrumentNo').val().toUpperCase()) {
                        if (grdControl[i].value != '') {
                            grdControl[i].style.backgroundColor = 'Yellow';
                        }
                        break;
                        return false;
                    }
                    else {
                        grdControl[i].style.backgroundColor = 'White';
                    }
                }
            }
        });
    });

    $(document).ready(function () {
        $("[id*=txtClrDt]").keypress(function (evt) {
            var iKeyCode = (evt.which) ? evt.which : evt.keyCode
            if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57))
            return false;
        });
    });

    $(function () {
        //$(document).ready(function () {
        var E = "{}";
        $.ajax({
            type: "POST",
            url: pageUrl + '/SetYearMon',
            data: E,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var ret = JSON.parse(response.d);
                $("#hdnForCurrYrMn").val(ret.YEAR_MONTH);
                if ($("#hdnForCurrYrMn").val() == $("#txtYrMn").val()) {
                    $("#txtYrMn").val(ret.YEAR_MONTH);
                }
                if ($("#txtYrMn").val() == '') { $("#txtYrMn").val(ret.YEAR_MONTH); }
                var ValYear = $("#txtYrMn").val().substr(0, 4);
                var ValMonth = $("#txtYrMn").val().substr(4, 2);
                var ValMinDate = '01' + '/' + ValMonth + '/' + ValYear;
                var daysInMonth = new Date(ValYear, ValMonth, 1, -1).getDate();
                var ValMaxDate = daysInMonth + '/' + ValMonth + '/' + ValYear;

                $(".txtClrDt").datepicker({
                    maxDate: ValMaxDate,
                    minDate: ValMinDate,
                    onSelect: function (event) {
                        var grid = document.getElementById("tbl");

                        for (var row = 0; row < grid.rows.length; row++) {
                            var InstDate = $("[id*=txtInsDate]");
                            var ClearDate = $("[id*=txtClrDt]");
                            if (ClearDate[row].value == event) {

                                var newClearDate = ClearDate[row].value.split("/");
                                var newClearDate1 = newClearDate[2] + "-" + newClearDate[1] + "-" + newClearDate[0];

                                var newInstDate = InstDate[row].value.split("/");
                                var newInstDate1 = newInstDate[2] + "-" + newInstDate[1] + "-" + newInstDate[0];

                                var ConvertClearDate = new Date(newClearDate1);
                                var ConvertInstDate = new Date(newInstDate1);

                                if (ConvertClearDate < ConvertInstDate) {
                                    alert('Cleared on Date Should Be Greater Than Or Equal To Instrument Date');
                                    ClearDate[row].focus();
                                    ClearDate[row].value = '';
                                    break;
                                    return false;

                                }
                                else {
                                    $("[id*=txtClrDt]").datepicker("hide");
                                    return false
                                }
                            }
                        }
                    }
                });

                //$(".loading-overlay").hide();
            },
            error: function (response) {
                var responseText;
                responseText = JSON.parse(response.responseText);
                alert("Error : " + responseText.Message);
                $(".loading-overlay").hide();
            },
            failure: function (response) {
                alert(response.d);
                $(".loading-overlay").hide();
            }
        });

    });

    $(document).ready(function () {

        SearchLoanType();
        $('input[type="text"]').bind("cut copy paste", function (e) {
            e.preventDefault();
        });
        });
    
    function SearchLoanType() {
        $(".autosuggestBankCode").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ACCOUNT_BRS.aspx/BankCodeAutoCompleteData",
                    data: "{OLD_SL_ID:'" + document.getElementById('txtBankSearch').value + "'}",
                    dataType: "json",
                    success: function (data) {
                        //response(data.d);
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('|')[0],
                                val: item.split('|')[1]
                            }
                        }))
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            },
            select: function (e, i) {
                //$("#txtLoaneeIDSearch").val(i.item.val);
                //$("#hdnBankID").val(i.item.val); 
                SetBankID(1, i.item.val);

            }
        });
    }

    var BankID = '';
    function SetBankID(type, BankID) {
        var W = "{type:" + type + ",BankID:'" + BankID + "'}";
        $.ajax({
            type: "POST",
            url: "ACCOUNT_BRS.aspx/GET_SetBankID",
            contentType: "application/json;charset=utf-8",
            data: W,
            dataType: "json",
            success: function (data) {
                return false;
            },
            error: function (result) {
                alert("Something Missing...");
                return false;
            }
        });
    }

    $(document).ready(function () {
        $('#txtBankSearch').keyup(function (evt) {                    
            var iKeyCode = evt.which ? evt.which : evt.keyCode;
            if (iKeyCode === 46) {
                SetBankID(0, BankID);
            }
        });

        $('#txtBankSearch').keydown(function (evt) {
            var iKeyCode = evt.which ? evt.which : evt.keyCode;
            if (iKeyCode == 8) {
                SetBankID(0, BankID);
            }
        });
    });


    function GetDateTime() {
        var param1 = new Date();
        var param2 = param1.getDate() + '/' + (param1.getMonth() + 1) + '/' + param1.getFullYear() + ' ' + param1.getHours() + ':' + param1.getMinutes() + ':' + param1.getSeconds();
        document.getElementById('txtClrDt').innerHTML = param2;
    }
    //function lastdaymonth(month, year) {
        
    //    return new Date(year, month, 0).getDate();

    //}


    $(document).ready(function () {
        $("[id*=ddlCleared]").change(function () {

            var txt = $("#txtYrMn").val()
            var mm = txt.substr(txt.length - 2);
            var yyyy = txt.substring(0, 4);
            // var last3 = txt.substr(txt.length, 4);

            //  alert(last3);

            var t = lastdaymonth(mm, yyyy);
            // alert(t);

            var lastmonth = t + '/' + mm + '/' + yyyy;
            // alert(lastmonth);

            var grid = document.getElementById("tbl");
            var rowid = $(this).closest('tr').find('td:eq(0)').text();

            var lre = /^\s*/;
            var rre = /\s*$/;
            rowid = rowid.replace(lre, "");
            rowid = rowid.replace(rre, "");
            //alert(rowid);
            for (var row = 1; row < grid.rows.length; row++) {
                var GridFrom_Cell = grid.rows[row].cells[0];
                var lvcsno = GridFrom_Cell.textContent.toString();

                var lre = /^\s*/;
                var rre = /\s*$/;
                lvcsno = lvcsno.replace(lre, "");
                lvcsno = lvcsno.replace(rre, "");
                // alert(lvcsno);

                if (lvcsno == rowid) {
                    hdnSlipID = lvcsno;//$("#hdnSlipID").val(lvcsno);
                    
                    var month = ""; var date = ""; var year = "";

                    // alert("matched");
                    for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                        var ValPOQty = $("[id*=ddlCleared]");

                        var cleared = $(ValPOQty[row1]).val();
                        if (cleared == 'R' || cleared == 'Y') {
                            var InstclrDt = $("[id*=txtClrDt]");
                            InstclrDt[row1].disabled = false;

                            var param1 = new Date();
                            date = param1.getDate() < 10 ? '0' + param1.getDate() : param1.getDate();

                            if (param1.getMonth() == 9)
                                month = parseInt(param1.getMonth()) + 1;
                            else
                                month = param1.getMonth() < 10 ? '0' + (parseInt(param1.getMonth()) + 1) : (parseInt(param1.getMonth()) + 1); //alert(dorMonth);
                            year = param1.getFullYear();

                            var param2 = date + '/' + month + '/' + year;
                            // document.getElementById('InstclrDt').innerHTML = param2;
                            if (param2 < lastmonth) {

                                //  InstclrDt[row1].value = param2;
                                InstclrDt[row1].value = lastmonth;
                                InstclrDt[row1].focus();
                                //  $("#InstclrDt[row1]").focus();
                            }
                            else {
                                InstclrDt[row1].value = param2;
                                InstclrDt[row1].focus();
                            }

                            var InstAmt = $("[id*=txtInsAmt]");
                            var ValInstAmt = $(InstAmt[row1]).val();

                            var AmtVched = $("[id*=txtAmtVouched]");
                            AmtVched[row1].value = ValInstAmt;

                        }
                        else {
                            var InstclrDt = $("[id*=txtClrDt]");
                            InstclrDt[row1].disabled = true;
                            InstclrDt[row1].value = '';

                            var AmtVched = $("[id*=txtAmtVouched]");
                            //AmtVched[row1].value = '';
                        }
                    }

                }
            }

        });

    });

   
    $(document).ready(function () {
        $("#txtBankSearch").keypress(function (event) {
            var ikeycode = (event.which) ? event.which : event.keyCode;
            if (ikeycode == 13) {
                $("#txtYrMn").focus();
                event.preventDefault();
            }
        });

        $("#txtYrMn").keypress(function (event) {
            var ikeycode = (event.which) ? event.which : event.keyCode;
            if (ikeycode == 13) {
                $("#ddlInsRecvd").focus();
                event.preventDefault();
            }
        });

        $("#ddlInsRecvd").keypress(function (event) {
            var ikeycode = (event.which) ? event.which : event.keyCode;
            if (ikeycode == 13) {
                $("#txtInstrumentNo").focus();
                event.preventDefault();
            }
        });



        $("#txtInstrumentNo").keypress(function (event) {
            var ikeycode = (event.which) ? event.which : event.keyCode;
            if (ikeycode == 13) {
                $("#btnBankSearch").focus();
                event.preventDefault();
            }
        });
       

    });

    
    $(document).ready(function () {
        $("[id*=txtClrDt]").keypress(function (event) {
            var ikeycode = (event.which) ? event.which : event.keyCode;
            if (ikeycode == 13) {
                //alert('2343');
                var grid = document.getElementById("tbl");

                for (var row = 1; row < grid.rows.length; row++) {
                    var GridFrom_Cell = grid.rows[row].cells[0];
                    var lvcsno = GridFrom_Cell.textContent.toString();

                    var lre = /^\s*/;
                    var rre = /\s*$/;
                    lvcsno = lvcsno.replace(lre, "");
                    lvcsno = lvcsno.replace(rre, "");
                    //alert(lvcsno);

                    //var hdnSlipID = $("#hdnSlipID").val();
                    if (hdnSlipID == lvcsno) {
                        //alert('MATCHED');
                        for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                            var AmcVch = $("[id*=txtAmtVouched]");

                            AmcVch[row1].focus();
                            return false;
                        }

                    }
                }
            }
        });

        $("[id*=txtAmtVouched]").keypress(function (event) {
            var ikeycode = (event.which) ? event.which : event.keyCode;
            if (ikeycode == 13) {
                //alert('2343');
                var grid = document.getElementById("tbl");

                for (var row = 1; row < grid.rows.length; row++) {
                    var GridFrom_Cell = grid.rows[row].cells[0];
                    var lvcsno = GridFrom_Cell.textContent.toString();

                    var lre = /^\s*/;
                    var rre = /\s*$/;
                    lvcsno = lvcsno.replace(lre, "");
                    lvcsno = lvcsno.replace(rre, "");
                    //alert(lvcsno);

                    //var hdnSlipID = $("#hdnSlipID").val();
                    if (document.getElementById("txtDraweBB").disabled == false) {
                        //document.getElementById("txtDraweBB").disabled = true;
                        if (hdnSlipID == lvcsno) {
                            //alert('MATCHED');
                            for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                                var InsclrDt = $("[id*=txtDraweBB]");

                                InsclrDt[row1].focus();
                                return false;
                            }
                        }
                    }
                    else {
                        hdnSlipID = hdnSlipID + 1;
                        if (hdnSlipID == lvcsno + 1) {
                            //alert('MATCHED');
                            for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                                var InsclrDt = $("[id*=ddlCleared]");

                                InsclrDt[row1 + 1].focus();
                                return false;
                            }

                        }
                    }
                }
            }
        });

        $("[id*=txtDraweBB]").keypress(function (event) {
            var ikeycode = (event.which) ? event.which : event.keyCode;
            if (ikeycode == 13) {
                //alert('2343');
                var grid = document.getElementById("tbl");

                for (var row = 1; row < grid.rows.length; row++) {
                    var GridFrom_Cell = grid.rows[row].cells[0];
                    var lvcsno = GridFrom_Cell.textContent.toString();

                    var lre = /^\s*/;
                    var rre = /\s*$/;
                    lvcsno = lvcsno.replace(lre, "");
                    lvcsno = lvcsno.replace(rre, "");
                    //alert(lvcsno);

                    //var hdnSlipID = $("#hdnSlipID").val();

                    hdnSlipID = hdnSlipID + 1;
                    if (hdnSlipID == lvcsno + 1) {
                        //alert('MATCHED');
                        for (var row1 = (parseInt(row) - 1) ; row1 < row; row1++) {
                            var InsclrDt = $("[id*=ddlCleared]");

                            InsclrDt[row1 + 1].focus();
                            return false;
                        }

                    }
                }

            }
        });
    });

    function SetClearOnDate()
    {

     
        //var event = $("#txtYrMn").val().substr(4, 2);
        //alert(event);
        //$("#txtClrDt ").datepicker("option", "maxDate", event);
    }


</script>


    <style type="text/css">
        .header {
            /*visibility:hidden;*/
            display: none;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Account BRS</td>
            </tr>
        </table> 
        <br />

        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                 <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">

                                <tr>
                                    <td class="labelCaption">Bank Code </td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption" colspan="1">
                                        <asp:TextBox ID="txtBankSearch" autocomplete="off" PlaceHolder="---(Select Bank Code)---" Width="500px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestBankCode"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Option For Corr.</td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption" colspan="8">
                                        <asp:DropDownList ID="ddlOption" autocomplete="off" Width="110px" Height="25px" runat="server" CssClass="inputbox2" ForeColor="#18588a">
                                            <asp:ListItem Text="Normal" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="Cleared" Value="C"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="labelCaption">Year&Month(YYMM)</td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtYrMn" autocomplete="off" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="hdnForCurrYrMn" autocomplete="off" style="display:none;" runat="server" Width="120px" ClientIDMode="Static" CssClass="inputbox2" MaxLength="6"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                        <span class="labelCaption">Instrument Received/Issued</span>:&nbsp;&nbsp;
                                        <span class="labelCaption">:</span>&nbsp;&nbsp;
                                        <asp:DropDownList ID="ddlInsRecvd" autocomplete="off" Width="150px" Height="25px" runat="server" CssClass="inputbox2" ForeColor="#18588a">
                                            <asp:ListItem Text="(Select Instrument)" Value=""></asp:ListItem>
                                            <asp:ListItem Text="RECEIVED" Value="D"></asp:ListItem>
                                            <asp:ListItem Text="ISSUED" Value="C"></asp:ListItem>
                                        </asp:DropDownList>
                                        <td class="labelCaption">Instrument No</td>
                                        <td class="labelCaption">:</td>
                                        <td align="left">
                                            <asp:TextBox ID="txtInstrumentNo" autocomplete="off" runat="server" Width="100px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                                        </td>

                                 <td>
                                    <asp:Button ID="btnBankSearch" autocomplete="off" runat="server" Text="Search" CommandName="search" Height="30px"
                                    Width="100px" CssClass="save-button DefaultButton" OnClick="cmdSearch_Click" OnClientClick="SetClearOnDate()" />
                                </td
                             </tr>
                       </table>
                    </InsertItemTemplate>
                  </asp:FormView>

                     <tr>
                        <td colspan="4" class="labelCaption">
                        <hr class="borderStyle" />
                        </td>
                    </tr>

                    <table>
                        <tr align="left">
                            <td class="labelCaption" style="font-weight: bold; font-family: Arial">Search Instrument No</td>
                            <td class="labelCaption">:</td>
                            <td align="left">
                                <asp:TextBox ID="txtSearchInstrumentNo" autocomplete="off" runat="server" Width="200px" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>
                            </td>
                            <td style="width: 68%"></td>
                        </tr>
                    </table>
                    </td>
            </tr>

            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" runat="server" autocomplete="off" Width="100%" align="center" GridLines="Both" OnRowDataBound="tbl_RowDataBound"
                            AutoGenerateColumns="false" DataKeyNames="ROW_ID" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField HeaderText="Slip Number" HeaderStyle-Width="0%">
                                    <ItemTemplate>
                                        <asp:Label ID="hdnID" autocomplete="off" Width="1%" ClientIDMode="Static" runat="server" Text='<%# Bind("ROW_ID") %>' CssClass="inputbox2"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="header" />
                                    <ItemStyle CssClass="header" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="VchNo" HeaderStyle-Width="0%">
                                    <ItemTemplate>
                                        <asp:Label ID="hdnVchNo" autocomplete="off" Width="1%" ClientIDMode="Static" runat="server" Text='<%# Bind("VCH_NO") %>' CssClass="inputbox2"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="header" />
                                    <ItemStyle CssClass="header" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="VchType" HeaderStyle-Width="0%">
                                    <ItemTemplate>
                                        <asp:Label ID="hdnVchType" autocomplete="off" Width="50px" ClientIDMode="Static" runat="server" Text='<%# Bind("VCH_TYPE") %>' CssClass="inputbox2"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="header" />
                                    <ItemStyle CssClass="header" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Instrument Type">
                                    <ItemTemplate>
                                        <asp:DropDownList autocomplete="off" ClientIDMode="Static" ID="ddlInsType" runat="server" Enabled="false">
                                            <asp:ListItem Text="(Select InstType )" Value="0"></asp:ListItem>
                                            <%--<asp:ListItem Text="Cheque" Value="C"></asp:ListItem>
                                                    <asp:ListItem Text="Draft" Value="D"></asp:ListItem>
                                                    <asp:ListItem Text="PayOrder" Value="P"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Slip Number">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtSlipNo" autocomplete="off" Width="50px" ClientIDMode="Static" runat="server" Text='<%# Bind("SLIP_NO") %>' CssClass="inputbox2" Enabled="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Instrument Number">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtInsNo" autocomplete="off" Width="100px" ClientIDMode="Static" runat="server" Text='<%# Bind("INST_NO") %>' CssClass="inputbox2" Enabled="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Instrument Date">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtInsDate" autocomplete="off" Width="100px" ClientIDMode="Static" runat="server" Text='<%# Bind("INST_DT") %>' CssClass="inputbox2" Enabled="false"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Instrument Amount">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtInsAmt" autocomplete="off" Width="100px" ClientIDMode="Static" runat="server" Text='<%# Bind("AMOUNT","{0:F}") %>' CssClass="inputbox2" Enabled="false" Style="text-align: right"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%-- <asp:TemplateField HeaderText="Slip Total">
                                     <ItemTemplate >
                                            <asp:TextBox ID="txtSlipTotal"  Width="80px" ClientIDMode="Static" runat="server" Text='<%# Bind("ACTUAL_AMOUNT") %>' CssClass="inputbox2" Enabled="false" ></asp:TextBox>
                                    </ItemTemplate>
                                     </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="Cleared">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlCleared" autocomplete="off" Height="20px" ClientIDMode="Static" runat="server" Text='<%# Bind("CLEARED") %>'>
                                            <%--<asp:ListItem Text="(Select Cleared )" Value=""></asp:ListItem>--%>
                                            <asp:ListItem Text="Pending" Value="N"></asp:ListItem>
                                            <asp:ListItem Text="Cleared" Value="Y"></asp:ListItem>
                                            <asp:ListItem Text="Return" Value="R"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Cleared On Date">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtClrDt" autocomplete="off" AutoPostBack="false" Width="70px" ClientIDMode="Predictable" runat="server" Text='<%# Bind("CLEARED_ON") %>' Height="22px" class="txtClrDt"></asp:TextBox>


                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="Amount Vouched">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtAmtVouched" autocomplete="off" AutoPostBack="false" Width="100px" ClientIDMode="Static" runat="server" Text='<%# Bind("ACTUAL_AMOUNT","{0:F}") %>' CssClass="inputbox2" Style="text-align: right"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Drawee Bank/Branch">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDraweBB" autocomplete="off" AutoPostBack="false" Enabled="true" Width="150px" ClientIDMode="Static" runat="server" Text='<%# Bind("DRAWEE") %>' CssClass="inputbox2"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--             <asp:TemplateField HeaderText="Save">
                                           
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName='Add' ImageUrl="images/Edit.jpg" 
                                            runat="server" ID="btnSave" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>--%>


                                <%--<asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Instrument Type" /> --%>
                            </Columns>
                        </asp:GridView>
                    </div>

                </td>
            </tr>
                                       
            <div id="savebrs" style="width: 100%; display: grid;">

                <table align="center" width="100%">
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>

                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" Height="30px" CommandName="ADD" autocomplete="off"
                                Width="100px" CssClass="save-button" OnClick="btnSave_Click" />

                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" Height="30px" autocomplete="off"
                                Width="100px" CssClass="save-button" OnClick="cmdCancel_Click" />
                        </td>



                    </tr>
                </table>
            </div>
        </table>
    </div>  

</asp:Content>

