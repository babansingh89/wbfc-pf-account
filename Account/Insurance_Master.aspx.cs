﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;

public partial class Insurance_Master : System.Web.UI.Page
{
    static string hdnID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                 PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            hdnID = ID;
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_LMN_InsuBYID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_LMN_InsuranceByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {

                TextBox txtInsCode = (TextBox)dv.FindControl("txtInsCode");
                TextBox txtInsName = (TextBox)dv.FindControl("txtInsName");
                TextBox txtAdd1 = (TextBox)dv.FindControl("txtAdd1");
                TextBox txtAdd2 = (TextBox)dv.FindControl("txtAdd2");
                TextBox txtAdd3 = (TextBox)dv.FindControl("txtAdd3");


                DBHandler.Execute("Insert_LMN_Insu", txtInsCode.Text, txtInsName.Text, txtAdd1.Text, txtAdd2.Text, txtAdd3.Text, Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]),
                    Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {

                TextBox txtInsCode = (TextBox)dv.FindControl("txtInsCode");
                TextBox txtInsName = (TextBox)dv.FindControl("txtInsName");
                TextBox txtAdd1 = (TextBox)dv.FindControl("txtAdd1");
                TextBox txtAdd2 = (TextBox)dv.FindControl("txtAdd2");
                TextBox txtAdd3 = (TextBox)dv.FindControl("txtAdd3");

                string ID = hdnID;// ((HiddenField)dv.FindControl("hdnID")).Value;

                DBHandler.Execute("Update_LMN_INSU", ID, txtInsCode.Text, txtInsName.Text, txtAdd1.Text, txtAdd2.Text, txtAdd3.Text,Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]),
                    Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
            hdnID = "";
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_LMN_Insu");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}