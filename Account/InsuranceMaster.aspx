﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="InsuranceMaster.aspx.cs" Inherits="InsuranceMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/InsuranceMaster.js"></script>

<script type="text/javascript">

    function beforeSave() {
            $("#frmEcom").validate();

            $("#txtCode").rules("add", { required: true, messages: { required: "Please enter a Insurance Code" } });
            $("#txtName").rules("add", { required: true, messages: { required: "Please Enter A Insurance Name" } });
            //DuplicateCheckInsuCode();
        }
    </script>

    <style type="text/css">
        .textbox {
            border: 1px solid #c4c4c4;
            height: 15px;
            width: 210px;
            font-size: 13px;
            padding: 4px 4px 4px 4px;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            box-shadow: 0px 0px 8px #d9d9d9;
            -moz-box-shadow: 0px 0px 8px #d9d9d9;
            -webkit-box-shadow: 0px 0px 8px #d9d9d9;
        }

            .textbox:focus {
                outline: none;
                border: 1px solid #7bc1f7;
                box-shadow: 0px 0px 8px #7bc1f7;
                -moz-box-shadow: 0px 0px 8px #7bc1f7;
                -webkit-box-shadow: 0px 0px 8px #7bc1f7;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">

     <div class="loading-overlay">
            <div class="loadwrapper">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>

    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Insurance Master</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="0" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td></td>
            </tr>

            <tr>
                <td>
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td align="left">Insurance Master Entry</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <table id="tblMain" align="center" cellpadding="2" width="80%">
                        <tr>
                            <td class="labelCaption">Insurance Code &nbsp;&nbsp;<span class="require">*</span></td>
                            <td class="labelCaption">:</td>
                            <td class="labelCaption" colspan="">
                                <asp:TextBox ID="txtCode" autocomplete="off" Width="100px" MaxLength="20" ClientIDMode="Static" runat="server" CssClass="textbox autosuggestLoaneeCode"></asp:TextBox>
                                
                            </td>
                            <td class="labelCaption">
                                <asp:Button ID="btnSearch" autocomplete="off" runat="server" Text="Search" CommandName="Search" Height="30"
                                    Width="70px" CssClass="save-button DefaultButton"/>
                            </td>
                            <td class="labelCaption">Insurance Name <span class="require">*</span></td>
                            <td class="labelCaption">:</td>
                            <td class="labelCaption">
                                <asp:TextBox ID="txtName" autocomplete="off" Width="330px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                            </td>
                            <td style="width:70px"></td>
                        </tr>
                        <tr>
                            <td class="labelCaption">Address 1 &nbsp;&nbsp;<span class="require"></span></td>
                            <td class="labelCaption">:</td>
                            <td class="labelCaption" colspan="5">
                                <asp:TextBox ID="txtAdd1" autocomplete="off" Width="650px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                            </td>

                        </tr>

                        <tr>
                            <td class="labelCaption">Address 2 &nbsp;&nbsp;<span class="require"></span></td>
                            <td class="labelCaption">:</td>
                            <td class="labelCaption" colspan="5">
                                <asp:TextBox ID="txtAdd2" autocomplete="off" Width="650px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelCaption">Address 3 &nbsp;&nbsp;<span class="require"></span></td>
                            <td class="labelCaption">:</td>
                            <td class="labelCaption" colspan="5">
                                <asp:TextBox ID="txtAdd3" autocomplete="off" Width="650px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                            </td>
                        </tr>

                        <br />
                        <tr>
                            <td colspan="7">
                                <div style="padding-left: 400px">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CommandName="Add" Height="30" autocomplete="off"
                                        Width="70px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'/>
                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" Height="30" autocomplete="off"
                                        Width="70px" CssClass="save-button" OnClick="cmdCancel_Click" />
                                </div>
                            </td>


                        </tr>
                        <%--<tr>
                            <td colspan="10" class="labelCaption">
                                <hr class="borderStyle" />
                            </td>
                        </tr>--%>

                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="10" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; max-height: 500px; width: 100%;">

                        <asp:GridView ID="gvDetails" Width="100%" runat="server" CssClass="Grid" autocomplete="off" AutoGenerateColumns="false">
                            <HeaderStyle BackColor="#DC5807" Font-Bold="true" ForeColor="White" />
                            <Columns>
                                <%-- <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Date" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Amount" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="PAN Number" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Drawee Bank" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Instrument Number" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Remarks" />--%>
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

