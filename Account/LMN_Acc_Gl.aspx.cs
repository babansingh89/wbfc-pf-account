﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;

public partial class LMN_Acc_Gl : System.Web.UI.Page
{
    static DataTable dtAccGl;
    static int AccGlID;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
                PopulateGrid1();
                PopulateGridgrdAccGLDtl();

                HttpContext.Current.Session["AccGlDetail"] = null;
                dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];
            }
             CheckBox chkUnitOnly = (CheckBox)dv.FindControl("chkUnitOnly");
            
               
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_LMN_LoanTypeByID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_LMN_LoanTypeByID", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static GroupCode[] GET_GroupNameByGrpID(string StrGroupCode)
    {
        //int UserID;
        List<GroupCode> Detail = new List<GroupCode>();
        //UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_GroupNameByGroupID", StrGroupCode);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GroupCode DataObj = new GroupCode();
            DataObj.GroupID = dtRow["GroupID"].ToString();
            DataObj.OldGroupID = dtRow["OldGroupID"].ToString();
            DataObj.GroupName  = dtRow["GroupName"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GroupCode //Class for binding data
    {
        public string GroupID { get; set; }
        public string OldGroupID { get; set; }
        public string GroupName { get; set; }
    }

    [WebMethod]
    public static GroupName1[] GET_GroupName()
    {
        List<GroupName1> Detail = new List<GroupName1>();
        DataTable dtGetData = DBHandler.GetResult("Get_GroupNameAll");
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GroupName1 DataObj = new GroupName1();
           // DataObj.GroupID = dtRow["GroupID"].ToString();
            DataObj.OldGroupID = dtRow["OldGroupID"].ToString();
            DataObj.GroupName = dtRow["GroupName"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GroupName1 //Class for binding data
    {
        //public string GroupID { get; set; }
        public string OldGroupID { get; set; }
        public string GroupName { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_AccGlDetailsOnSession(string YearMonth, string OpenDebit, string OpenCredit, string TotalDebit, string TotalCredit)
    {
        string JSONVal = "";
        try
        {
            
            if (HttpContext.Current.Session["AccGlDetail"] != null)
            {
                DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                if (dtdetail.Rows.Count > 0)
                {
                    DataRow dtrow = dtdetail.NewRow();
                    dtrow["YearMonth"] = YearMonth;
                    dtrow["OpenDebit"] = OpenDebit;
                    dtrow["OpenCredit"] = OpenCredit;
                    dtrow["TotalDebit"] = TotalDebit;
                    dtrow["TotalCredit"] = TotalCredit;
                    dtdetail.Rows.Add(dtrow);
                    HttpContext.Current.Session["IssueDetails"] = dtdetail;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("YearMonth");
                dt.Columns.Add("OpenDebit");
                dt.Columns.Add("OpenCredit");
                dt.Columns.Add("TotalDebit");
                dt.Columns.Add("TotalCredit");

                DataRow dtrow = dt.NewRow();
                dtrow["YearMonth"] = YearMonth;
                dtrow["OpenDebit"] = OpenDebit;
                dtrow["OpenCredit"] = OpenCredit;
                dtrow["TotalDebit"] = TotalDebit;
                dtrow["TotalCredit"] = TotalCredit;

                dt.Rows.Add(dtrow);
                HttpContext.Current.Session["AccGlDetail"] = dt;
            }

            string result = "success";
            JSONVal = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_AccGlRecord(string GlID, string Group_ID, string OldGlID, string OldSlID, string GlName, string GlMaster, string GlType, string Schedule, string chkSectorID)
    {
        int UserID, SectorID;

        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        string JSonVal = "";
        string ConString = "";
        string GL_ID = "";
   //     ConString = DataAccess.DBHandler.GetConnectionString();

     
        try
        {
            if (GlID == "")
            {

                DataTable dtResult = DBHandler.GetResult("Insert_AccGl",  Group_ID, OldGlID, OldSlID, GlName, GlMaster, GlType, Schedule, chkSectorID.Equals("N") ? DBNull.Value : (object)SectorID, UserID);
                if (dtResult.Rows.Count > 0)
                {
                    GL_ID = dtResult.Rows[0]["GL_ID"].ToString();
                }
            }
            else
            {

            }
            DataTable dtdetail = (DataTable)HttpContext.Current.Session["AccGlDetail"];
            if (dtdetail.Rows.Count > 0)
            {
                foreach (DataRow objDR in dtdetail.Rows)
                {
                    DBHandler.Execute("Insert_AccGlOpning", GL_ID, objDR["YearMonth"], Convert.ToDecimal(objDR["OpenDebit"]), Convert.ToDecimal(objDR["OpenCredit"]), Convert.ToDecimal(objDR["TotalDebit"]), Convert.ToDecimal(objDR["TotalCredit"]), SectorID, UserID);
                    //cmd = new SqlCommand("Exec Save_IssueDetail " + Convert.ToInt32(IntIssueId) + ",'" + IssueNo + "','" + IssueDate + "'," + Convert.ToInt16(SrNo) + "," + Convert.ToInt16(ReqID) + "," + Convert.ToInt32(objDR["FolioID"]) + ",'" + Convert.ToDecimal(objDR["IssuedQuantity"]) + "'," + Convert.ToInt16(UNIT_COMBO_CODE) + ", " + Convert.ToInt16(UserID) + "", db, transaction);
                    //cmd.ExecuteNonQuery();
                    //SrNo++;
                }
            }
            HttpContext.Current.Session["AccGlDetail"] = null;
            dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        //////////try
        //////////{
        //////////    try
        //////////    {
        //////////        if (Gl_Id == "")
        //////////        {
        //////////            DataTable dtMaxIssue = DBHandler.GetResult("DAT_MaxIssueNo", UNIT_COMBO_CODE, IssueDate);

        //////////            IssueNo = Convert.ToString(dtMaxIssue.Rows[0]["MaxIssueNo"]);
        //////////            cmd = new SqlCommand("Exec Save_IssueHdr'" + IssueNo + "','" + IssueDate + "'," + Convert.ToInt16(UNIT_COMBO_CODE) + ",'" + IssueStatus + "','" + IssuedBy + "','" + DocumentNo + "','" + Description + "'," + Convert.ToInt16(ReqID) + "," + Convert.ToInt32(VendorID) + "," + Convert.ToInt32(SectionID) + "," + Convert.ToDecimal(DepotID) + "," + UserID + "", db, transaction);
        //////////            cmd.ExecuteNonQuery();
        //////////            IntIssueId = 0;
        //////////        }
        //////////        else
        //////////        {
        //////////            cmd = new SqlCommand("Exec Update_IssueHdr " + Convert.ToInt32(IssueID) + ",'" + IssueDate + "'," + Convert.ToInt16(UNIT_COMBO_CODE) + ",'" + IssueStatus + "','" + IssuedBy + "','" + DocumentNo + "','" + Description + "'," + Convert.ToInt16(ReqID) + "," + Convert.ToInt32(VendorID) + "," + Convert.ToInt32(SectionID) + "," + Convert.ToDecimal(DepotID) + "," + UserID + "", db, transaction);
        //////////            cmd.ExecuteNonQuery();

        //////////            cmd = new SqlCommand("Exec Delete_IssueDetail " + Convert.ToInt32(IssueID) + "", db, transaction);
        //////////            cmd.ExecuteNonQuery();
        //////////            IntIssueId = Convert.ToInt32(IssueID);
        //////////        }
        //////////        DataTable dtdetail = (DataTable)HttpContext.Current.Session["IssueDetails"];

                
        //////////        int SrNo = 1;
        //////////        if (dtdetail.Rows.Count > 0)
        //////////        {
        //////////            foreach (DataRow objDR in dtdetail.Rows)
        //////////            {

        //////////                cmd = new SqlCommand("Exec Save_IssueDetail " + Convert.ToInt32(IntIssueId) + ",'" + IssueNo + "','" + IssueDate + "'," + Convert.ToInt16(SrNo) + "," + Convert.ToInt16(ReqID) + "," + Convert.ToInt32(objDR["FolioID"]) + ",'" + Convert.ToDecimal(objDR["IssuedQuantity"]) + "'," + Convert.ToInt16(UNIT_COMBO_CODE) + ", " + Convert.ToInt16(UserID) + "", db, transaction);
        //////////                cmd.ExecuteNonQuery();
        //////////                SrNo++;
        //////////            }
        //////////        }

                
        //////////        transaction.Commit();
        //////////        HttpContext.Current.Session["IssueDetails"] = null;
        //////////        dtIssueDetail = (DataTable)HttpContext.Current.Session["IssueDetails"];
              
        //////////        JSonVal = IssueNo.ToJSON();
        //////////    }
        //////////    catch (SqlException sqlError)
        //////////    {
        //////////        transaction.Rollback();
        //////////        HttpContext.Current.Session["IssueDetails"] = null;
        //////////        dtIssueDetail = (DataTable)HttpContext.Current.Session["IssueDetails"];

                

        //////////        string result = "Fail";
        //////////        JSonVal = result.ToJSON();
        //////////    }
           

            
        //////////}
        //////////catch (Exception ex)
        //////////{
        //////////    throw new Exception(ex.Message);
        //////////}

        return JSonVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DeleteSessionAccGl(string ValYearMonth)
    {
        StringBuilder objSB = new StringBuilder();
        string JSONVal = string.Empty;
        string strYearMon = "";
        DataTable dtDetails = (DataTable)HttpContext.Current.Session["AccGlDetail"];
        if (HttpContext.Current.Session["AccGlDetail"] != null)
        {
            strYearMon = ValYearMonth;
            try
            {
                if (dtDetails.Rows.Count == 1)
                {
                    HttpContext.Current.Session["IssueDetails"] = null;
                    dtAccGl = (DataTable)HttpContext.Current.Session["AccGlDetail"];
                }
                else
                {
                    dtDetails = dtDetails.AsEnumerable().Where(x => x.Field<String>("FolioID") != strYearMon).CopyToDataTable();
                    HttpContext.Current.Session["AccGlDetail"] = dtDetails;
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session["AccGlDetail"] = null;
            }
        }
        return JSONVal;
    }

   

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            //////DataTable dt = DBHandler.GetResult("Get_LMN_LoanType");
            //////tbl.DataSource = dt;
            //////tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGrid1()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("OldGroupID");
            dt.Columns.Add("GroupName");
            dt.Rows.Add();
            grdGroup.DataSource = dt;
            grdGroup.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGridgrdAccGLDtl()
    {
        try
        {
            GridView grdAccGLDtl = (GridView)dv.FindControl("grdAccGLDtl");
            DataTable dt = new DataTable();
            dt.Columns.Add("ItemID");
            dt.Columns.Add("YearMonth");
            dt.Columns.Add("OpeningDebit");
            dt.Columns.Add("OpeningCredit");
            dt.Columns.Add("TotalDebit");
            dt.Columns.Add("TotalCredit");
            dt.Columns.Add("ClosingDebit");
            dt.Columns.Add("ClosingCredit");
            dt.Rows.Add();
            grdAccGLDtl.DataSource = dt;
            grdAccGLDtl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //protected DataTable drpload()
    //{
    //    try
    //    {
    //        DataTable dt = DBHandler.GetResult("LoadCountry");
    //        return dt;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}
}