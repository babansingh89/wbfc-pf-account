﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="VoucherDetails.aspx.cs" Inherits="VoucherDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript" src="js/VoucherDetails.js?v=2"></script>
	<script type="text/javascript">
		function beforeSave() {
			$("#frmEcom").validate();
			$("#txtLoaneeSearch").rules("add", { required: true, messages: { required: "Please select a Loanee." } });
			$("#txtBank").rules("add", { required: true, messages: { required: "Please select a Bank." } });
			$("#txtAmount").rules("add", { required: true, messages: { required: "Please enter total Amount." } });
			$("#txtDatePay").rules("add", { required: true, messages: { required: "Select a Date" } });
		}
		
	</script>
    <style type="text/css">
        .rightAlign {
            text-align: right;
        }

        .hiddencol {
            display: none;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .vs {
            pointer-events: none;
        }
    </style>
    <style type="text/css">
        .textbox {
            border: 1px solid #c4c4c4;
            height: 15px;
            width: 210px;
            font-size: 13px;
            padding: 4px 4px 4px 4px;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            box-shadow: 0px 0px 8px #d9d9d9;
            -moz-box-shadow: 0px 0px 8px #d9d9d9;
            -webkit-box-shadow: 0px 0px 8px #d9d9d9;
        }

            .textbox:focus {
                outline: none;
                border: 1px solid #7bc1f7;
                box-shadow: 0px 0px 8px #7bc1f7;
                -moz-box-shadow: 0px 0px 8px #7bc1f7;
                -webkit-box-shadow: 0px 0px 8px #7bc1f7;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
     <div class="loading-overlay">
        <div class="loadwrapper">
            <div class="ajax-loader-outer">Loading...</div>
        </div>
    </div>
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Voucher Details</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td align="left">Voucher Detail Search</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="98%" align="center">
                        <tr>
                            <td class="labelCaption">Year & Month(YYYYMM) &nbsp;&nbsp;<span class="require">*</span> </td>
                            <td class="labelCaption">:</td>
                            <td align="left">
                                <asp:TextBox ID="txtYearmonth" autocomplete="off" Width="100" ClientIDMode="Static" MaxLength="6" runat="server" CssClass="inputbox2"></asp:TextBox>
                            </td>
                            <td class="labelCaption">Voucher Number &nbsp;&nbsp;<span class="require">*</span> </td>
                            <td class="labelCaption">:</td>
                            <td align="left">
                                <asp:TextBox ID="txtVoucherno" autocomplete="off" Width="100" ClientIDMode="Static" MaxLength="10" runat="server" CssClass="inputbox2"></asp:TextBox>
                            </td>

                            <td>
                                <asp:Button ID="cmdShow" runat="server" Text="Search" CommandName="Add" Height="30" autocomplete="off"
                                    Width="70px" CssClass="save-button DefaultButton" />
                                <asp:Button ID="cmdShowCancel" runat="server" Text="Refresh" Height="30" autocomplete="off"
                                    Width="70px" CssClass="save-button" OnClick="cmdCancel_Click" />
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td align="left">Voucher Detail Entry</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Received/Payment  &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPayType" autocomplete="off" Width="160px" Height="25px" CssClass="textbox" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="Payment" Value="P"></asp:ListItem>
                                            <asp:ListItem Text="Received" Value="R"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">GL &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtGL" autocomplete="off" PlaceHolder="---(Select GL)---" Width="300px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">Date &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtDatePay" autocomplete="off" Width="150px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>

                                    <td class="labelCaption">SL &nbsp;&nbsp;<span class="require"></span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption" colspan="">
                                        <asp:TextBox ID="txtSL" autocomplete="off" PlaceHolder="---(Select SL)---" Width="300px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>

                                    <td class="labelCaption">PAN &nbsp;&nbsp;<span id="PanNo" class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtPan" autocomplete="off" Width="150px" onblur="Validate_PAN(this);" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>

                                </tr>

                                <tr>
                                    <td class="labelCaption">Bank &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtBank" autocomplete="off" PlaceHolder="---(Select Bank)---" Width="300px" ClientIDMode="Static" runat="server" CssClass="textbox autosuggestBank"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Total Amount &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtAmount" autocomplete="off" Width="150px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Drawee Bank &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption">
                                        <asp:TextBox ID="txtDraweeBank" autocomplete="off" Width="300px" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Instrument Type  &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td colspan="4" align="left">
                                        <asp:DropDownList ID="ddlInstrumentType" autocomplete="off" DataSource='<%# drpload() %>'
                                            DataValueField="INST_TYPE" DataTextField="INST_TYPE_DESC" AppendDataBoundItems="true"
                                            Width="160px" Height="25px" CssClass="textbox" ForeColor="#18588a" runat="server">
                                            <asp:ListItem Text="(Select Instrument Type)" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </tr>
                                <tr>
                                    <td class="labelCaption">Remarks &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td class="labelCaption" rowspan="2">
                                        <asp:TextBox ID="txtRemarks"  autocomplete="off" Width="300px" Height="60px" MaxLength="100" TextMode="MultiLine" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Instrument No. &nbsp;&nbsp;<span id="InstNo" class="require">*</span></td>
                                    <td class="labelCaption">:  </td>
                                    <td>
                                        <asp:TextBox ID="txtNo" autocomplete="off" Width="150px" MaxLength="20" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <br />
                                <tr>
                                    <td colspan="6">
                                        <div style="padding-left: 450px">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CommandName="Add" Height="30" autocomplete="off"
                                                Width="70px" CssClass="save-button DefaultButton" />
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" Height="30" autocomplete="off"
                                                Width="70px" CssClass="save-button" OnClick="cmdCancel_Click" />
                                        </div>
                                    </td>


                                </tr>
                                <tr>
                                    <td colspan="10" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; max-height: 300px; width: 100%">

                        <asp:GridView ID="gvDetails" autocomplete="off" Width="100%" runat="server" CssClass="Grid" AutoGenerateColumns="false">
                            <HeaderStyle BackColor="#DC5807" Font-Bold="true" ForeColor="White" />
                            <Columns>
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Date" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Amount" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="PAN Number" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Drawee Bank" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Instrument Number" />
                                <asp:BoundField ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Remarks" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
