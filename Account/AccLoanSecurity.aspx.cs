﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Globalization;

public partial class AccLoanSecurity : System.Web.UI.Page
{
    static string hdnLoaneeID = "";
    static string hdnLoaneeID1 = "";
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Session[SiteConstants.SSN_SECTOR_ID] == null)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
            return;
        }       
        PopulateGrid();       
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                TextBox txtLoaneeSearch = (TextBox)dv.FindControl("txtLoaneeSearch");
                txtLoaneeSearch.Text = "";
            }         
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            //hdnLoaneeID = ID;
            if (flag == 1)
            {
                
                    //int str = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);
                    DataTable dtResult = DBHandler.GetResult("Get_AccLoanSecurityByLoaneeID",Convert.ToInt64(ID), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]));
                    dv.DataSource = dtResult;
                    dv.DataBind();
                hdnLoaneeID = dtResult.Rows[0]["LOANEE_UNQ_ID"].ToString();
                hdnLoaneeID1 = dtResult.Rows[0]["LoaneeSecurityID"].ToString();
                TextBox txtSecurityType = (TextBox)dv.FindControl("txtSecurityType");
                    txtSecurityType.Focus();
            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_AccLoanSecurity", Convert.ToInt64(ID), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]));
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        } 
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGrid()
    {
        try
        {
            string LoaneeUNQID = hdnLoaneeID;// ((HiddenField)dv.FindControl("hdnLoaneeID")).Value;

            if (LoaneeUNQID == "")
            {
                int a = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);
                //DataTable dt = DBHandler.GetResult("Get_AccLoanSecurity", sector_id);
                DataTable dt = DBHandler.GetResult("Get_AccLoanSecurity", 0, Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]));
                tbl.DataSource = dt;
                tbl.DataBind();
                TxtTotalRec.Text = dt.Rows.Count.ToString();
            }
            else
            {
                DataTable dt = DBHandler.GetResult("Get_AccLoanSecurity", Convert.ToInt64(LoaneeUNQID), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]));
                tbl.DataSource = dt;
                tbl.DataBind();
                TxtTotalRec.Text = dt.Rows.Count.ToString();
            }
            

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSearch = (Button)sender;
            string LoaneeUNQID = hdnLoaneeID;// ((HiddenField)dv.FindControl("hdnLoaneeID")).Value;
            PopulateGridSearchByLoanee(LoaneeUNQID);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);  
        }
    }

    private void PopulateGridSearchByLoanee(string LoaneeUNQID)
    {
        try
        {
            dv.ChangeMode(FormViewMode.Insert);
            if (LoaneeUNQID != "")
            {
                DataTable dt = DBHandler.GetResult("Get_AccLoanSecurity", Convert.ToInt64(LoaneeUNQID), Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]));
                if (dt.Rows.Count > 0)
                {
                    tbl.DataSource = dt;
                    tbl.DataBind();
                }
                else
                {
                    dv.ChangeMode(FormViewMode.Insert);
                    tbl.DataSource = null;
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No Record found for this Loanee Code.')</script>");
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This Loanee Code not present in selected Unit. Please select a correct Loanee Code.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string GET_LoaneeID(int type, string LoaneeID)
    {
        string msg = "";
        if (type == 0) { hdnLoaneeID = ""; }
        else { hdnLoaneeID = LoaneeID; }
        return msg;
    }
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                string LoaneeUNQID = hdnLoaneeID;// ((HiddenField)dv.FindControl("hdnLoaneeID")).Value;
                TextBox txtSecurityType = (TextBox)dv.FindControl("txtSecurityType");
                TextBox txtSecurityNumber = (TextBox)dv.FindControl("txtSecurityNumber");
                TextBox txtSecurityDate = (TextBox)dv.FindControl("txtSecurityDate");
                TextBox txtBankName = (TextBox)dv.FindControl("txtBankName");
                TextBox txtBranchName = (TextBox)dv.FindControl("txtBranchName");
                TextBox txtFaceValue = (TextBox)dv.FindControl("txtFaceValue");
                TextBox txtMaturityValue = (TextBox)dv.FindControl("txtMaturityValue");
                TextBox txtMaturityDate = (TextBox)dv.FindControl("txtMaturityDate");
                TextBox txtPledgeDate = (TextBox)dv.FindControl("txtPledgeDate");
                TextBox txtHolderName = (TextBox)dv.FindControl("txtHolderName");
                TextBox txtReleaseDate = (TextBox)dv.FindControl("txtReleaseDate");
                TextBox txtFolderName = (TextBox)dv.FindControl("txtFolderName");
                TextBox txtRemarks = (TextBox)dv.FindControl("txtRemarks");

                DateTime dtSecurityDate = new DateTime();
                DateTime dtMaturityDate = new DateTime();
                DateTime dtPledgeDate = new DateTime();
                DateTime dtReleaseDate = new DateTime();

                if (txtSecurityDate.Text != "")
                {
                    dtSecurityDate = DateTime.ParseExact(txtSecurityDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (txtMaturityDate.Text != "")
                {
                    dtMaturityDate = DateTime.ParseExact(txtMaturityDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (txtPledgeDate.Text != "")
                {
                    dtPledgeDate = DateTime.ParseExact(txtPledgeDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (txtReleaseDate.Text != "")
                {
                    dtReleaseDate = DateTime.ParseExact(txtReleaseDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                if (LoaneeUNQID == "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This Loanee Code not present in selected Unit. Please select a correct Loanee Code.')</script>");
                    return;
                }

                DBHandler.Execute("Insert_AccLoanSecurity", Convert.ToInt64(LoaneeUNQID), 
                                    txtSecurityType.Text.Trim().Equals("") ? DBNull.Value : (object)txtSecurityType.Text.Trim(),
                                    txtSecurityNumber.Text.Trim().Equals("") ? DBNull.Value : (object)txtSecurityNumber.Text.Trim(),
                                    txtSecurityDate.Text.Equals("") ? DBNull.Value : (object)dtSecurityDate.ToShortDateString(),
                                    txtBankName.Text.Trim().Equals("") ? DBNull.Value : (object)txtBankName.Text.Trim(),
                                    txtBranchName.Text.Trim().Equals("") ? DBNull.Value : (object)txtBranchName.Text.Trim(),
                                    txtFaceValue.Text.Trim().Equals("") ? DBNull.Value : (object)txtFaceValue.Text.Trim(),
                                    txtMaturityValue.Text.Trim().Equals("") ? DBNull.Value : (object)txtMaturityValue.Text.Trim(),
                                    txtMaturityDate.Text.Equals("") ? DBNull.Value : (object)dtMaturityDate.ToShortDateString(),
                                    txtPledgeDate.Text.Equals("") ? DBNull.Value : (object)dtPledgeDate.ToShortDateString(),
                                    txtHolderName.Text.Trim().Equals("") ? DBNull.Value : (object)txtHolderName.Text.Trim(),
                                    txtReleaseDate.Text.Equals("") ? DBNull.Value : (object)dtReleaseDate.ToShortDateString(),
                                    txtFolderName.Text.Trim().Equals("") ? DBNull.Value : (object)txtFolderName.Text.Trim(),
                                    txtRemarks.Text.Trim().Equals("") ? DBNull.Value : (object)txtRemarks.Text.Trim(),
                                    Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]),
                                    Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID])
                                    );
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                hdnLoaneeID = "";
                hdnLoaneeID1 = "";
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
            }
            else if (cmdSave.CommandName == "Edit")
            {

                //string LoaneeID = ((HiddenField)dv.FindControl("hdnLoaneeID")).Value;
                TextBox txtSecurityType = (TextBox)dv.FindControl("txtSecurityType");
                TextBox txtSecurityNumber = (TextBox)dv.FindControl("txtSecurityNumber");
                TextBox txtSecurityDate = (TextBox)dv.FindControl("txtSecurityDate");
                TextBox txtBankName = (TextBox)dv.FindControl("txtBankName");
                TextBox txtBranchName = (TextBox)dv.FindControl("txtBranchName");
                TextBox txtFaceValue = (TextBox)dv.FindControl("txtFaceValue");
                TextBox txtMaturityValue = (TextBox)dv.FindControl("txtMaturityValue");
                TextBox txtMaturityDate = (TextBox)dv.FindControl("txtMaturityDate");
                TextBox txtPledgeDate = (TextBox)dv.FindControl("txtPledgeDate");
                TextBox txtHolderName = (TextBox)dv.FindControl("txtHolderName");
                TextBox txtReleaseDate = (TextBox)dv.FindControl("txtReleaseDate");
                TextBox txtFolderName = (TextBox)dv.FindControl("txtFolderName");
                TextBox txtRemarks = (TextBox)dv.FindControl("txtRemarks");


                DateTime dtSecurityDate = new DateTime();
                DateTime dtMaturityDate = new DateTime();
                DateTime dtPledgeDate = new DateTime();
                DateTime dtReleaseDate = new DateTime();

                if (txtSecurityDate.Text != "")
                {
                    dtSecurityDate = DateTime.ParseExact(txtSecurityDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (txtMaturityDate.Text != "")
                {
                    dtMaturityDate = DateTime.ParseExact(txtMaturityDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (txtPledgeDate.Text != "")
                {
                    dtPledgeDate = DateTime.ParseExact(txtPledgeDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (txtReleaseDate.Text != "")
                {
                    dtReleaseDate = DateTime.ParseExact(txtReleaseDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                string LoaneeUNQ_ID = hdnLoaneeID;// ((HiddenField)dv.FindControl("hdnLoaneeID")).Value;
                string LoaneeSecurity_ID = hdnLoaneeID1;// ((HiddenField)dv.FindControl("hdnLoaneeID1")).Value;

                int sectorID  = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID].ToString());

                if (LoaneeUNQ_ID == "")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This Loanee Code not present in selected Unit. Please select a correct Loanee Code.')</script>");
                    return;
                }

                DBHandler.Execute("Update_AccLoanSecurity", Convert.ToInt64(LoaneeSecurity_ID), Convert.ToInt64(LoaneeUNQ_ID),
                                    txtSecurityType.Text.Trim().Equals("") ? DBNull.Value : (object)txtSecurityType.Text.Trim(),
                                    txtSecurityNumber.Text.Trim().Equals("") ? DBNull.Value : (object)txtSecurityNumber.Text.Trim(),
                                    txtSecurityDate.Text.Equals("") ? DBNull.Value : (object)dtSecurityDate.ToShortDateString(),
                                    txtBankName.Text.Trim().Equals("") ? DBNull.Value : (object)txtBankName.Text.Trim(),
                                    txtBranchName.Text.Trim().Equals("") ? DBNull.Value : (object)txtBranchName.Text.Trim(),
                                    txtFaceValue.Text.Trim().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(txtFaceValue.Text.Trim()),
                                    txtMaturityValue.Text.Trim().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(txtMaturityValue.Text.Trim()),
                                    txtMaturityDate.Text.Equals("") ? DBNull.Value : (object)dtMaturityDate.ToShortDateString(),
                                    txtPledgeDate.Text.Equals("") ? DBNull.Value : (object)dtPledgeDate.ToShortDateString(),
                                    txtHolderName.Text.Trim().Equals("") ? DBNull.Value : (object)txtHolderName.Text.Trim(),
                                    txtReleaseDate.Text.Equals("") ? DBNull.Value : (object)dtReleaseDate.ToShortDateString(),
                                    txtFolderName.Text.Trim().Equals("") ? DBNull.Value : (object)txtFolderName.Text.Trim(),
                                    txtRemarks.Text.Trim().Equals("") ? DBNull.Value : (object)txtRemarks.Text.Trim(),
                                    Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]),
                                    Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID])
                                    );
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                hdnLoaneeID = "";
                hdnLoaneeID1 = "";
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                
                if (Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]) != 0)
                {
                    dv.ChangeMode(FormViewMode.Edit);
                    UpdateDeleteRecord(1, e.CommandArgument.ToString());
                    Button cmdSave = (Button)dv.FindControl("cmdSave");
                    cmdSave.CommandName = "Edit";
                    cmdSave.ToolTip = "Update";
                    cmdSave.Text = "Update";
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Select a Sector before edit record.')</script>");
                    return;
                }
                
                
            }
            else if (e.CommandName == "Del")
            {
                if (Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]) != 0)
                {
                    UpdateDeleteRecord(2, e.CommandArgument.ToString());
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Select a Sector before edit record.')</script>");
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        tbl.PageIndex = e.NewPageIndex;
        PopulateGrid();
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string[] LoaneeAutoCompleteData(string LoaneeCode)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_LoaneeIDByCode", LoaneeCode, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {

            result.Add(string.Format("{0}|{1}", dtRow["LOANEE_NAME1"].ToString(), dtRow["LOANEE_UNQ_ID"].ToString()));
        }
        return result.ToArray();
    }

    
}