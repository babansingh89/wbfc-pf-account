﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DataAccess;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

public partial class _Default : System.Web.UI.Page
{
    IEncryptDecrypt chiperAlgorithm;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string QueryStringData = Request.QueryString["Data"];
            if (QueryStringData != null) {
                chiperAlgorithm = new AES_Algorithm("EIIPL@201^");
                string decryptedURL = chiperAlgorithm.Decrypt(QueryStringData);
                string urlDecodedData = HttpUtility.UrlDecode(decryptedURL);
                JavaScriptSerializer js = new JavaScriptSerializer();
                var serializedData = js.Deserialize<QueryData>(urlDecodedData);

                string QueryStringUserID = serializedData.UserID;
                string QueryStringMID = serializedData.MID;
                string QueryStringKMS = serializedData.KMS;
                string QueryStringUserName = serializedData.UserName;
                string QueryStringPassword = serializedData.Password;
                if (QueryStringUserID != null && QueryStringMID != null && QueryStringKMS != null && QueryStringUserName != null && QueryStringPassword != null)
                {
                    HttpContext.Current.Session["QUERY_USERID"] = QueryStringUserID;
                    HttpContext.Current.Session["QUERY_MID"] = QueryStringMID;
                    HttpContext.Current.Session["QUERY_KMS"] = QueryStringKMS;
                    HttpContext.Current.Session["QUERY_UserName"] = QueryStringUserName;
                    HttpContext.Current.Session["QUERY_Password"] = QueryStringPassword;

                    HttpContext.Current.Session["Menu_For"] = QueryStringMID == "5" ? "P" : "C";
                    //Response.Redirect("/Default.aspx");
                    checkLogin(QueryStringUserName, QueryStringPassword);
                }
            }
        }
    }

    protected void checkLogin(string UserID, string Password)
    {
        try
        {
            txtPWD.Text = "";
            if (UserID == "") { this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Name can not be blank')</script>"); txtUserID.Focus(); return; }
            if (Password == "") { this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Password can not be blank')</script>"); txtPWD.Focus(); return; }
            DataRow dr = DBHandler.GetResult("Get_LoginCredentials", UserID, Password).Rows[0];

            switch ((int)dr[0])
            {
                case 1://Successful
                    Session[SiteConstants.SSN_DR_USER_DETAILS] = dr;
                    Session[SiteConstants.SSN_INT_USER_ID] = dr["UserID"];
                    Session[SiteConstants.SSN_USER_TYPE] = dr["TypeCode"];
                    HttpContext.Current.Session["Menu_For"] = "P";
                    Response.Redirect("welcome.aspx", false);
                    break;
                case 2://Locked
                    //this.Page.RegisterStartupScript("Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                    break;
                case 3://Failed
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Failed; Please provide correct User Name and Password')</script>");
                    break;
                case 4://Failed
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('The User is Blocked Due to Non Payment of Due Amount')</script>");
                    break;
                    //return "Login Failed; Please provide correct User Name and Password";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void btnSubmit_Clicked(object sender, EventArgs e)
    {
        try
        {
            string UserID = Page.Request["txtUserID"];
            string Password = Page.Request["txtPWD"];
            txtPWD.Text = "";
            if (UserID == "") { this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Name can not be blank')</script>"); txtUserID.Focus(); return; }
            if (Password == "") { this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Password can not be blank')</script>"); txtPWD.Focus(); return; }
            DataRow dr = DBHandler.GetResult("Get_LoginCredentials", UserID, Password).Rows[0];

            switch ((int)dr[0])
            {
                case 1://Successful
                    Session[SiteConstants.SSN_DR_USER_DETAILS] = dr;
                    Session[SiteConstants.SSN_INT_USER_ID] = dr["UserID"];
                    Session[SiteConstants.SSN_USER_TYPE] = dr["TypeCode"];
                    HttpContext.Current.Session["Menu_For"] = "P";
                    Response.Redirect("welcome.aspx", false);
                    break;
                case 2://Locked
                    //this.Page.RegisterStartupScript("Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<Script>alert('Your Account is Locked; Please contact your Administrator')</Script>");
                    break;
                case 3://Failed
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('Login Failed; Please provide correct User Name and Password')</script>");
                    break;
                case 4://Failed
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Login Error", "<script>alert('The User is Blocked Due to Non Payment of Due Amount')</script>");
                    break;
                    //return "Login Failed; Please provide correct User Name and Password";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    public class QueryData
    {
        public string UserID { get; set; }
        public string MID { get; set; }
        public string KMS { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}