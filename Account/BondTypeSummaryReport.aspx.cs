﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;

using System.Web.Script.Services;
using System.Web.Services;

public partial class BondTypeSummaryReport : System.Web.UI.Page
{
    static string StrFormula = "";
    static string ConnString="";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            else if (!IsPostBack)
            {
                GridSummary_populate();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    public void GridSummary_populate()
    {
        DataTable dt = new DataTable();
        dt.Rows.Add();
        GridSummary.DataSource = dt;
        GridSummary.DataBind();
    }
    
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static Summary[] Grid_Bind(string ASonDATE, string BondType)
    {
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DateTime dtASonDATE = new DateTime();
        if (ASonDATE != "")
        { dtASonDATE = DateTime.ParseExact(ASonDATE, "dd/MM/yyyy", CultureInfo.InvariantCulture); }
        List<Summary> Detail = new List<Summary>();
        DataTable dtGetData = DBHandler.GetResult("Acc_Bond_type", dtASonDATE.ToShortDateString(), SectorID, BondType);
        for (int i = 0; i < dtGetData.Rows.Count; i++)
        {
            Summary DataObj = new Summary();
            DataObj.SCHEME_Name = dtGetData.Rows[i]["SCHEME_Name"].ToString();
            DataObj.ISIN_NO = dtGetData.Rows[i]["ISIN_NO"].ToString();
            DataObj.Amount = dtGetData.Rows[i]["Amount"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class Summary
    {
        public string SCHEME_Name { get; set; }
        public string ISIN_NO { get; set; }
        public string Amount { get; set; }
    }

    [WebMethod]
    public static string GET_SectorAddress()
    {
        string address="";
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dt = DBHandler.GetResult("Get_SectorAddressByID", SectorID);
        address = dt.Rows[0]["Address"].ToString();
        return address;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string PrintBondInterestAnnx(string yearmonth, string xyz, string secId)
    {
        string FormName = "";
        string ReportName = "";    
        string ReportType = "";
        StrFormula = "";
        int SectorID;
        FormName = "BondTypeSummaryReport.aspx";
        ReportName = "Bond Interest Annex";
        ReportType = "Bond Interest Annex.";
        ConnString = DataAccess.DBHandler.GetConnectionString();
        System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
        System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;

        

        StrFormula = "";
        int UserID;
        string StrPaperSize = "";
        StrPaperSize = "Page - A4";
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

        DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

        DataTable DTCheckReportVal1 = DBHandler.GetResult("Get_Update_RptParameterValue", UserID, FormName, ReportName, ReportType, yearmonth, xyz, SectorID, "", "", "", "", "", "", "");
        string JSONVal = "";
        return JSONVal;
    }
}