﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Services;
using System.Data.SqlClient;
using System.Threading;
using System.Web.Script.Services;

public partial class VoucherDetails : System.Web.UI.Page
{
    static string StrFormula = "";
    protected string pay_current_Date = "";
    protected string YearMonth = "";
    protected string h_code = "";
    protected string s_type = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
           
            if (!IsPostBack)
            {
              //  Get_YearMonth();
              //  Get_CurrentFinancialYear();

                Blank_Grid();
               
            }
            else
            {
              //  TextBox txtDate = (TextBox)dv.FindControl("txtDatePay");
              //  pay_current_Date = txtDate.Text;
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Acc_VchInstType");
            return dt;         
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void Get_CurrentFinancialYear()
    {
        try
        {
            string FinancialYear = "";
            DataTable dtResult = DBHandler.GetResult("Get_Financial_Year");
            if (dtResult.Rows.Count > 0)
            {
                pay_current_Date = dtResult.Rows[0]["cur_date"].ToString();
                FinancialYear = dtResult.Rows[0]["Financial_Year"].ToString();
                //First_Date = "01" + "/04/" + FinancialYear.Substring(0, 4);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void Get_YearMonth()
    {
        try
        {
            DataTable dtResult = DBHandler.GetResult("Get_Year_Month");
            if (dtResult.Rows.Count > 0)
            {
                YearMonth = dtResult.Rows[0]["YEAR_MONTH"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void Blank_Grid()
    {
        DataTable dt = new DataTable();
        dt.Rows.Add();
        gvDetails.DataSource = dt;
        gvDetails.DataBind();
    }

    [WebMethod]
    public static string[] AutoComplete_Bank(string Bank)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_Bank", Bank, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(dtRow["GL_NAME1"].ToString()+"|"+dtRow["GL_ID"].ToString()+"|"+dtRow["SL_ID"].ToString());
        }
        return result.ToArray();
    }

    [WebMethod]
    public static string[] GET_glID(string GL)
    {
        List<string> Detail = new List<string>();
        //int Sectorid = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        try
        {
            DataTable dt = DBHandler.GetResult("GET_GLID_BySubID1",GL);
            foreach (DataRow dtRow in dt.Rows)
            {
                Detail.Add(dtRow["GL_ID"].ToString() + "|" + dtRow["SL_ID"].ToString() + "|" + dtRow["SUBTYPE"].ToString() + "|" + dtRow["GL"].ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return Detail.ToArray();             
    }

    [WebMethod]
    public static string[] GET_slID(string SL, string SUBTYPE)
    {
        List<string> Detail = new List<string>();
        int Sectorid = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dt = DBHandler.GetResult("GET_SLID_BySubType", SL,SUBTYPE,Sectorid);
        foreach (DataRow dtRow in dt.Rows)
        {
            Detail.Add(dtRow["LOANEE_UNQ_ID"].ToString() + "|" + dtRow["SL"].ToString());
        }
        return Detail.ToArray();  
    }

    [WebMethod]
    public static string Check_PAN(string PAN)
    {
        string result = "";
        DataTable dt = DBHandler.GetResult("GET_PAN_accsubvoucherdtls", PAN);
        if (dt.Rows.Count > 0)
        {
            result = "1";
        }
        else { result = "0"; }

        return result;
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod] 
    public static string SAVE_deatails(string TRANS_ID,string RCV_PAY,string GL_ID_1, string SL_ID_1,string SL_SUB_CODE, string VCH_DATE,string VCH_AMT,string REMARKS,string PAN,string GL_ID_2, string SL_ID_2, string INST_TYPE,string INST_NO,string DRAWEE)
    {
        string json = "";
        try
        {
            string result = "";
           
            string SL_FROM = "L";           
            string SUB_ID_1 = "0000000000";              
            string SUB_ID_2 = "0000000000";
            string POST_FLAG = "N";
            string VCH_NO = "";

            DateTime dtVCH_DATE = DateTime.ParseExact(VCH_DATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            int SECTORID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
            int INSERTED_BY = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            if (TRANS_ID == "")
            {
                DBHandler.Execute("Insert_Acc_Sub_Voucher_dtls", RCV_PAY, GL_ID_1, SL_FROM, SL_ID_1, SUB_ID_1,
                                                 SL_SUB_CODE.Equals("") ? DBNull.Value : (object)Convert.ToInt32(SL_SUB_CODE), dtVCH_DATE.ToShortDateString(),
                                                 Convert.ToDecimal(VCH_AMT), REMARKS, PAN.Equals("") ? DBNull.Value : (object)PAN, GL_ID_2, SL_ID_2, SUB_ID_2,
                                                 INST_TYPE, INST_NO.Equals("") ? DBNull.Value : (object)INST_NO, DRAWEE, POST_FLAG,
                                                 VCH_NO.Equals("") ? DBNull.Value : (object)Convert.ToInt32(VCH_NO), SECTORID, INSERTED_BY);

                result = "Data Save Successfully.";
            }
            else {
                DBHandler.Execute("Update_ACCsubvoucherDtls_TrnsID", Convert.ToInt32(TRANS_ID), RCV_PAY, GL_ID_1, SL_FROM, SL_ID_1, SUB_ID_1,
                                                                    SL_SUB_CODE.Equals("") ? DBNull.Value : (object)Convert.ToInt32(SL_SUB_CODE), dtVCH_DATE.ToShortDateString(),
                                                                     Convert.ToDecimal(VCH_AMT), REMARKS, PAN.Equals("") ? DBNull.Value : (object)PAN,
                                                                     GL_ID_2, SL_ID_2, SUB_ID_2, INST_TYPE,
                                                                     INST_NO.Equals("") ? DBNull.Value : (object)INST_NO, DRAWEE, POST_FLAG,
                                                                      VCH_NO.Equals("") ? DBNull.Value : (object)Convert.ToInt32(VCH_NO), SECTORID, INSERTED_BY);
                result = "Record Update successfully.";
            }
            json = result.ToJSON();
        }
        catch(Exception ex) {
            throw new Exception(ex.Message);
        }

        return json;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ClsBondAllot[] Bind_GRID()
    {
        List<ClsBondAllot> Detail = new List<ClsBondAllot>();
        int SECTORID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtGetData = DBHandler.GetResult("GET_ACCsubvoucherdtls_Details", SECTORID);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ClsBondAllot DataObj = new ClsBondAllot();
            DataObj.TRANS_ID = dtRow["TRANS_ID"].ToString();
            DataObj.VCH_DATE = dtRow["VCH_DATE"].ToString();
            DataObj.VCH_AMT = dtRow["VCH_AMT"].ToString();
            DataObj.REMARKS = dtRow["REMARKS"].ToString();
            DataObj.PAN = dtRow["PAN"].ToString();
            DataObj.INST_NO = dtRow["INST_NO"].ToString();
            DataObj.DRAWEE = dtRow["DRAWEE"].ToString();
            Detail.Add(DataObj);
        }

        return Detail.ToArray();
    } 
    public class ClsBondAllot
    {
        public string TRANS_ID { get; set; }
        public string VCH_DATE { get; set; }
        public string VCH_AMT { get; set; }
        public string REMARKS { get; set; }
        public string PAN { get; set; }
        public string INST_NO { get; set; }
        public string DRAWEE { get; set; }

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static ClsEdit[] Edit_GRID(string TransID)
    {
        List<ClsEdit> Detail = new List<ClsEdit>();
        //int SECTORID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtGetData = DBHandler.GetResult("GET_ACCsubvoucherDtls_TrnsID", Convert.ToInt32(TransID));
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            ClsEdit DataObj = new ClsEdit();
            DataObj.TRANS_ID = dtRow["TRANS_ID"].ToString();
            DataObj.VCH_DATE = dtRow["VCH_DATE"].ToString();
            DataObj.VCH_AMT = dtRow["VCH_AMT"].ToString();
            DataObj.REMARKS = dtRow["REMARKS"].ToString();
            DataObj.PAN = dtRow["PAN"].ToString();
            DataObj.INST_NO = dtRow["INST_NO"].ToString();
            DataObj.DRAWEE = dtRow["DRAWEE"].ToString();

            DataObj.POST_FLAG = dtRow["POST_FLAG"].ToString();
            DataObj.VCH_NO = dtRow["VCH_NO"].ToString();
            DataObj.RCV_PAY = dtRow["RCV_PAY"].ToString();
            DataObj.GL = dtRow["GL"].ToString();
            DataObj.GL_ID_1 = dtRow["GL_ID_1"].ToString();
            DataObj.SL_FROM = dtRow["SL_FROM"].ToString();
            DataObj.SL_ID_1 = dtRow["SL_ID_1"].ToString();

            DataObj.SUB_ID_1 = dtRow["SUB_ID_1"].ToString();
            DataObj.SL_SUB_CODE = dtRow["SL_SUB_CODE"].ToString();
            DataObj.SL = dtRow["SL"].ToString();
            DataObj.Bank = dtRow["Bank"].ToString();
            DataObj.GL_ID_2 = dtRow["GL_ID_2"].ToString();
            DataObj.SL_ID_2 = dtRow["SL_ID_2"].ToString();
            DataObj.SUB_ID_2 = dtRow["SUB_ID_2"].ToString();
            DataObj.INST_TYPE = dtRow["INST_TYPE"].ToString();
            DataObj.SUBID = dtRow["SUBID"].ToString();
            DataObj.SUBTYPE = dtRow["SUBTYPE"].ToString();
            
            Detail.Add(DataObj);
        } // g.SUBID, g.SUBTYPE

        return Detail.ToArray();
    }
    public class ClsEdit
    { //  RCV_PAY, GL, ASV.GL_ID_1,ASV.SL_FROM,ASV.SL_ID_1,ASV.SUB_ID_1, SL ,ASV.SL_SUB_CODE , Bank , ASV.GL_ID_2,ASV.SL_ID_2,ASV.SUB_ID_2,ASV.INST_TYPE,ASV.POST_FLAG,ASV.VCH_NO
        public string TRANS_ID { get; set; }
        public string VCH_DATE { get; set; }
        public string VCH_AMT { get; set; }
        public string REMARKS { get; set; }
        public string PAN { get; set; }
        public string INST_NO { get; set; }
        public string DRAWEE { get; set; }

        public string POST_FLAG { get; set; }
        public string VCH_NO { get; set; }       

        public string RCV_PAY { get; set; }
        public string GL { get; set; }
        public string GL_ID_1 { get; set; }
        public string SL_FROM { get; set; }
        public string SL_ID_1 { get; set; }

        public string SUB_ID_1 { get; set; }       
        public string SL_SUB_CODE { get; set; }
        public string SL { get; set; }
        public string Bank { get; set; }

        public string GL_ID_2 { get; set; }
        public string SL_ID_2 { get; set; }      
        public string SUB_ID_2 { get; set; }
        public string INST_TYPE { get; set; }
        public string SUBID { get; set; }
        public string SUBTYPE { get; set; }

    }   // g.SUBID, g.SUBTYPE

    [WebMethod]
    public static string Delete_Data(string TransID)
    { 
        string json="";
        try
        {
            DBHandler.Execute("Delete_ACCsubvoucherDtls_TrnsID", Convert.ToInt32(TransID));
            string result = "Delete successfull.";
            json = result.ToJSON();
        }
        catch (Exception ex)
        { throw new Exception(ex.Message);
        }
        return json;    
    }

    [WebMethod]
    public static string Search_data(string yearmonth, string vchtype, string vchno)
    {
        try
        {
            int sexid = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
            DataSet ds = DBHandler.GetResults("GET_ACCsubvoucherDtls_Search", yearmonth, Convert.ToInt32(vchtype), vchno,sexid);
            return ds.GetXml();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
   
}