﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;

public partial class UnlockUser : System.Web.UI.Page
{
    DataTable dtMaster;
    DataTable dtDetail;
    DataTable dtRoom;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdUnlock_Click(object sender, EventArgs e)
    {
        try
        {
            DBHandler.Execute("Get_UnlockUser", txtUserName.Text);
            txtUserName.Text = "";
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('User Unlocked Successfully')</script>");
        }
        catch(Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}