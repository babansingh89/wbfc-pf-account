﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="SanctionReport.aspx.cs" Inherits="SanctionReport" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="js/SanctionReport.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>SANCTION REPORT</td>
            </tr>
        </table>
        <br />
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">Report Name&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="width:50px;">
                    <asp:DropDownList ID="ddlReportName" autocomplete="off" runat="server" Width="200px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                        <asp:ListItem Value="0">(Select Report Name)</asp:ListItem>
                        <asp:ListItem Value="SR">Sanction Report</asp:ListItem>
                    </asp:DropDownList>
                </td>  
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">Report Short By&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td >
                    <asp:DropDownList ID="ddlRptShortBy" autocomplete="off" runat="server" Width="110px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                        <asp:ListItem Value="0">Loanee Code</asp:ListItem>
                        <asp:ListItem Value="1">Loanee Name</asp:ListItem>
                        <asp:ListItem Value="2">Sanction Date</asp:ListItem>
                        <asp:ListItem Value="3">Sanction</asp:ListItem>
                        <asp:ListItem Value="4">Sector</asp:ListItem>
                        <asp:ListItem Value="5">Industry</asp:ListItem>
                        <asp:ListItem Value="6">District</asp:ListItem>
                        <asp:ListItem Value="7">Sanction By</asp:ListItem>
                    </asp:DropDownList>
                    <asp:CheckBox ID="chkTotProjCost" autocomplete="off" ForeColor="Navy" Font-Bold="true" ClientIDMode="Static" runat="server" Text="Total Project Cost" ToolTip="Print Total & Grand Total Project Cost on Report." />
                </td>
            </tr>

            <tr>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">Sanction From Date&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td ">
                    <asp:TextBox ID="txtFrmDate" autocomplete="off" ReadOnly="true" placeholder="DD/MM/YYYYY" Width="190px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">Sanction To Date&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td>
                    <asp:TextBox ID="txtToDate" autocomplete="off" ReadOnly="true" placeholder="DD/MM/YYYYY" Width="103px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                    <asp:CheckBox ID="chkAllSector" autocomplete="off" ForeColor="Navy" Font-Bold="true" ClientIDMode="Static" runat="server" Text="All Sector" ToolTip="Print All Sector" />
                </td>
            </tr>
            <tr style="width:100%">
                <td></td>
                <td></td>
            </tr>
        </table>

        <table>
            <tr style="width: 100%">
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnGenerateReport" autocomplete="off" runat="server" Text="Report" Width="100px" CssClass="save-button DefaultButton" />
                    <asp:Button ID="btnCancel" autocomplete="off" runat="server" Text="Cancel " Width="100px" CssClass="save-button" OnClick="btnCancel_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
