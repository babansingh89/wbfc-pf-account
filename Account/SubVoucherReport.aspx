﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="SubVoucherReport.aspx.cs" Inherits="SubVoucherReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/SubVoucherPrint.js?v=2"></script>
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Sub Voucher Report</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td colspan="6">
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td style="height: 15px;" align="left">Sub Voucher Report Search</td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    <asp:formview id="dv" runat="server" width="99%" cellpadding="4" autogeneraterows="False" autocomplete="off"
                        defaultmode="Insert" horizontalalign="Center" gridlines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">

                                <tr>
                                    <td>
                                        <div id="searchYM" style="width: 100%;">
                                            <table align="center" width="100%">                                              
                                                <tr>
                                                    <td class="labelCaption">From Date &nbsp;&nbsp;<span class="require">*</span></td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox runat="server" autocomplete="off" ID="txtFromDt" ClientIDMode="Static" Width="150px" CssClass="inputbox2"></asp:TextBox>
                                                        <asp:ImageButton ID="imgFromDt" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                                            AlternateText="Click to show calendar" runat="server" />
                                                    </td>

                                                    <td class="labelCaption">To Date &nbsp;&nbsp;<span class="require">*</span></td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox runat="server" autocomplete="off" ID="txtToDt" ClientIDMode="Static" Width="150px" CssClass="inputbox2"></asp:TextBox>
                                                        <asp:ImageButton ID="imgToDt" autocomplete="off" CausesValidation="false" ImageUrl="images/date.png"
                                                            AlternateText="Click to show calendar" runat="server" />
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td class="labelCaption">GL &nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td class="labelCaption">
                                                        <asp:TextBox ID="txtGL" autocomplete="off" PlaceHolder="---(Select GL)---" Width="370px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestLoaneeCode"></asp:TextBox>
                                                    </td>

                                                    <td class="labelCaption">SL</td>
                                                    <td class="labelCaption">:</td>
                                                    <td class="labelCaption">
                                                        <asp:TextBox ID="txtSL" autocomplete="off" PlaceHolder="---(All SL)---" Width="370px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autosuggestLoaneeCode"></asp:TextBox>
                                                    </td>

                                                    
                                                </tr>                                                
                                            </table>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="6">

                                        <div style="float: right; margin-left: 200px;">
                                            <asp:Button ID="btnPrint" runat="server" Text="Show" CommandName="print" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" />
                                            
                                            <asp:Button ID="cmdCancelSearch" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button" OnClick="cmdCancelSearch_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>

                    </asp:formview>
                </td>
            </tr>

        </table>
    </div>
</asp:Content>
