﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LoaneeSandryDetail.aspx.cs" Inherits="LoaneeSandryDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            //$('#txtToDate').datepicker().datepicker('setDate', 'today');
            $('#txtDate').datepicker().datepicker('setDate', '<%= (current_Date = string.IsNullOrEmpty(current_Date) ? "" : current_Date)%>');
            $('#txtDate').datepicker({
                changeMonth: true,
                changeYear: true,
                showOtherMonths: true,
                selectOtherMonths: true, 
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy'
            });

            $("#imgDate").click(function () {
                $('#txtDate').datepicker("show");
                return false;
            });
        });

        //$(document).ready(function () {

        //    $('.DueDate').datepicker({
        //        showOtherMonths: true,
        //        selectOtherMonths: true,
        //        closeText: 'X',
        //        showAnim: 'drop',
        //        showButtonPanel: true,
        //        duration: 'slow',
        //        dateformat: 'dd/mm/yyyy'
        //    });

        //    //$("[id$=imgDueDate]").click(function () {
        //    //    $("[id$=txtDueDate]").datepicker("show");
        //    //    return false;
        //    //});
        //});  


        $(document).ready(function () {
            $("#ddlSearch").focus();

            $('#ddlSearch').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    $('#txtDate').focus();
                    return false;
                }
            });
            $('#txtDate').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    $('#cmdSearch').focus();
                    return false;
                }
            });



            var SearchVal = $('#ddlSearch').val();
            if (SearchVal == 'D') {
                $("#txtDate").prop('disabled', true);
                $("#imgDate").prop('disabled', true);

            }
            else {
                $("#txtDate").prop('disabled', false);
                $("#imgDate").prop('disabled', false);
            }
            $('#ddlSearch').change(function () {
                var SearchVal = $('#ddlSearch').val();
                if (SearchVal == 'D') {
                    $("#txtDate").prop('disabled', true);
                    $("#imgDate").prop('disabled', true);

                }
                else {
                    $("#txtDate").prop('disabled', false);
                    $("#imgDate").prop('disabled', false);
                }
            })

            $(function () {
                $("[id$=txtDueDate]").mask("99/99/9999", { placeholder: "DD/MM/YYYY" });
            });

        });

    </script>
    <style type="text/css">
        .rightAlign {
            text-align: right;
        }

        .hiddencol {
            display: none;
        }

        .uppercase {
            text-transform: uppercase;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Loanee Sandry Details</td>
            </tr>
        </table>
       
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
          <%--  <tr>
                <td colspan="6">
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td style="height: 15px;" align="left">Loanee Sandry Details</td>
                        </tr>
                    </table>
                </td>

            </tr>--%>
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">

                                <tr>
                                    <td>
                                        <div id="searchYM" style="width: 100%;">
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td class="labelCaption" style="font-weight:bold;">Search Type&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlSearch" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2" Width="200px" Height="25px" ForeColor="#18588a">
                                                            <asp:ListItem Text="--Select Type--" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="Un Adjusted" Value="U"></asp:ListItem>
                                                            <asp:ListItem Text="Adjusted" Value="A"></asp:ListItem>
                                                            <asp:ListItem Text="Due Date" Value="D"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>

                                                    <td class="labelCaption" style="font-weight:bold;">Date &nbsp; &nbsp; &nbsp; &nbsp;</td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtDate" autocomplete="off" runat="server" CssClass="inputbox2" Width="150px" ClientIDMode="Static"></asp:TextBox>
                                                       <%-- <asp:ImageButton ID="imgDate" CausesValidation="false" ImageUrl="images/date.png"
                                                            AlternateText="Click to show calendar" runat="server" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <div style="float: right; margin-left: 200px;">
                                            <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="search" Height="30" autocomplete="off"
                                                Width="70px" CssClass="save-button DefaultButton"
                                                OnClick="cmdSearch_Click" />
                                            <asp:Button ID="cmdCancelSearch" runat="server" Text="Cancel" Height="30" autocomplete="off"
                                                Width="70px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                        <%--<FooterTemplate>
                            
                        </FooterTemplate>--%>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Panel ID="panGrd" runat="server" autocomplete="off" Width="100%" Visible="false">
                        <div id="divLoanInt" style="width: 100%;">
                            <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="60%" align="center" GridLines="Both" ShowFooter="false"
                                AutoGenerateColumns="false" DataKeyNames="VCH_NO, GL_ID, SL_ID, SUB_ID, SectorID" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <SelectedRowStyle BackColor="#87CEEB" />
                                <Columns>
                                    <asp:BoundField DataField="VCH_NO" ItemStyle-CssClass="labelCaption, hiddencol" HeaderStyle-CssClass="TableHeader, hiddencol" HeaderText="VCH_NO" />
                                    <asp:BoundField DataField="VoucherNo" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher No" />
                                    <asp:BoundField DataField="VCH_DATE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Date" />
                                    <asp:BoundField DataField="OLD_SL_ID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Loanee Code" />
                                    <asp:BoundField DataField="AMOUNT" ItemStyle-CssClass="labelCaptionright" DataFormatString="{0:0.00}" HeaderStyle-CssClass="TableHeader" HeaderText="Amount" />
                                    <asp:TemplateField HeaderText="Due Date">
                                        <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDueDate" autocomplete="off" Width="150px" Height="20px" ForeColor="Navy" Text='<%# Eval("DUE_DATE")%>' runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="labelCaption" BackColor="#99ccff" />
                            </asp:GridView>
                        </div>
                        <div style="float: right; margin-left: 200px;">
                            <asp:Button ID="cmdUpdate" autocomplete="off" runat="server" Text="Update" CommandName="search" Height="30" Visible="false"
                                Width="70px" CssClass="save-button DefaultButton"
                                OnClick="cmdUpdate_Click" />
                            <asp:Button ID="cmdRefresh" autocomplete="off" runat="server" Text="Refresh" Height="30" Visible="false"
                                Width="70px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" />
                        </div>
                    </asp:Panel>

                </td>
            </tr>
            <tr>
                <td colspan="6"></td>
            </tr>

            <tr>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>
