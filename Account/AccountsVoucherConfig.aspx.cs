﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Globalization;

public partial class AccountsVoucherConfig : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            PopulateGridView();
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void PopulateGridView()
    {
        DataTable dt = DBHandler.GetResult("GET_Acc_Vouch_Config_GridView");
        tbl.DataSource = dt;
        tbl.DataBind();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GET_AutoCompleteProcNameDetails(string ProcName)
    {
        List<string> Detail = new List<string>();
        DataTable dtGetData = DBHandler.GetResult("AutoCompleteProcNameDetails", ProcName);
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            Detail.Add(dtRow["PROC_NAME"].ToString() + "/" + dtRow["POST_TYPE"].ToString() + "/" + dtRow["POST_ORD"].ToString());
        }
        return Detail.ToArray();
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GET_AutoCompleteDebitCreditDetails(string ID, string Type)
    {
        List<string> Detail = new List<string>();
        DataTable dtGetData = DBHandler.GetResult("AutoCompleteDebitAndCreditDetails", ID, Type);
        if (Type == "G")
        {
            foreach (DataRow dtRow in dtGetData.Rows)
            {
                Detail.Add(dtRow["OLD_GL_ID"].ToString() + "/" + dtRow["GL_ID"].ToString());
            }
        }

        else if (Type == "S")
        {
            foreach (DataRow dtRow in dtGetData.Rows)
            {
                Detail.Add(dtRow["OLD_SL_ID"].ToString() + "/" + dtRow["SL_ID"].ToString());
            }
        }

        else if (Type == "T")
        {
            foreach (DataRow dtRow in dtGetData.Rows)
            {
                Detail.Add(dtRow["OLD_SUB_ID"].ToString() + "/" + dtRow["SUB_ID"].ToString());
            }
        }

        return Detail.ToArray();
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        TextBox txtProcName = (TextBox)dv.FindControl("txtProcName");
        TextBox txtProcType = (TextBox)dv.FindControl("txtProcType");
        TextBox txtProcOrder = (TextBox)dv.FindControl("txtProcOrder");
        TextBox hdnDr_GLId = (TextBox)dv.FindControl("hdnDr_GLId");
        TextBox hdnDr_SLId = (TextBox)dv.FindControl("hdnDr_SLId");
        TextBox hdnDr_SubId = (TextBox)dv.FindControl("hdnDr_SubId");
        TextBox hdnCr_GLId = (TextBox)dv.FindControl("hdnCr_GLId");
        TextBox hdnCr_SLId = (TextBox)dv.FindControl("hdnCr_SLId");
        TextBox hdnCr_SubId = (TextBox)dv.FindControl("hdnCr_SubId");

        try
        {
            DBHandler.Execute("SAVE_Acc_Vouch_Config", txtProcName.Text, txtProcType.Text, Convert.ToInt32(txtProcOrder.Text), hdnDr_GLId.Text,
                                                       hdnDr_SLId.Text, hdnDr_SubId.Text, hdnCr_GLId.Text, hdnCr_SLId.Text, hdnCr_SubId.Text);

            TextBox txtDr_GLId = (TextBox)dv.FindControl("txtDr_GLId");
            TextBox txtDr_SLId = (TextBox)dv.FindControl("txtDr_SLId");
            TextBox txtDr_SubId = (TextBox)dv.FindControl("txtDr_SubId");
            TextBox txtCr_GLId = (TextBox)dv.FindControl("txtCr_GLId");
            TextBox txtCr_SLId = (TextBox)dv.FindControl("txtCr_SLId");
            TextBox txtCr_SubId = (TextBox)dv.FindControl("txtCr_SubId");
            txtProcName.Text = "";
            txtProcType.Text = "";
            txtProcOrder.Text = "";
            txtDr_GLId.Text = "";
            txtDr_SLId.Text = "";
            txtDr_SubId.Text = "";
            txtCr_GLId.Text = "";
            txtCr_SLId.Text = "";
            txtCr_SubId.Text = "";
            PopulateGridView();
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Saved Successfully!')</script>");
        }

        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}