﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Globalization;

public partial class Sector_Type : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_sectorType_BYID", ID);
                Session["ID"] = ID;
                hdnSectorID.Text = ID;
                dv.DataSource = dtResult;
                dv.DataBind();
            }
            else if (flag == 2)
            {
                DataTable dtGroupID = DBHandler.GetResult("Get_CheckSecCodeOnLoanMaster", ID);
                if (dtGroupID.Rows.Count > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Can Not Delete Record! There are many records with this sector code in Loan Master')</script>");

                }
                else
                {
                    DBHandler.Execute("Delete_Sector_TypeByid", ID);
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            String SectorID = "";
            if (cmdSave.CommandName == "Add")
            {

                TextBox txtSectorCode = (TextBox)dv.FindControl("txtSectorcode");
                TextBox txtSectorName = (TextBox)dv.FindControl("txtsectorname");
                if (hdnSectorID.Text == "")
                {
                    SectorID = "0";
                }
                else { SectorID = hdnSectorID.Text; }
                {
                    DataTable dtGetData = DBHandler.GetResult("Get_DupCheckSectorCode", SectorID, txtSectorCode.Text);
                    if (dtGetData.Rows.Count > 0)
                    {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Sector Code Already Exists!')</script>");
                    }
                    else
                    {
                        DBHandler.Execute("Insert_SectorType", txtSectorCode.Text, txtSectorName.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                        cmdSave.Text = "Create";
                        dv.ChangeMode(FormViewMode.Insert);
                        PopulateGrid();
                        dv.DataBind();
                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
                    }
                }
            }
            else if (cmdSave.CommandName == "Edit")
            {


                TextBox txtSectorCode = (TextBox)dv.FindControl("txtSectorcode");
                TextBox txtSectorName = (TextBox)dv.FindControl("txtsectorname");

               
                {
                    //TextBox valConstitutionUnitTypeID = (TextBox)dv.FindControl("ConstitutionUnitTypeID");
                    String ID = (String)Session["ID"];

                    //string ID = ((HiddenField)dv.FindControl("hdnID")).Value;

                    DBHandler.Execute("Update_SectorType", ID, txtSectorCode.Text,txtSectorName.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                    cmdSave.Text = "Create";
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                    Session["ID"] = null;
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }  

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_SectorType");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //============================ Start Duplicate Check Group ID Coding Start here===================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DupChkSectorCode(string SectorCode, string SectorID)
    {
        string ValSectorCode = "";
        DataTable dtGetData = DBHandler.GetResult("Get_DupCheckSectorCode", SectorID, SectorCode);
        if (dtGetData.Rows.Count > 0)
        {
            ValSectorCode = Convert.ToString(dtGetData.Rows[0]["SectorCode"]);
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(ValSectorCode);
    }
    //============================End Duplicate Check Group ID Coding Start here===================================//
}