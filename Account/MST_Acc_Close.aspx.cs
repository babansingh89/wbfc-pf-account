﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Data.SqlClient;
using System.Web.Services;

public partial class MST_Acc_Close : System.Web.UI.Page
{
    static int sector;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //PopulateGrid();
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string Get_Save(string TxtYearMonth, string ddlTransactionType, string DdlTransactionStatus)
    {
        string msg = "";
        try     
        {   
            //DataTable dTCHK = DBHandler.GetResult("Duplication_checking_Of_MST_ACC_CLOSE_YEAR_MONTH", TxtYearMonth, ddlTransactionType);
            //if (dTCHK.Rows.Count > 0)
            //{
            //    msg = "This Year Month " + TxtYearMonth + " " + "Already in use." + " " + "Please Provide Another Year month";
            //}
            //else
            //{

                if (ddlTransactionType != "H")
                {
                    DataTable dTCHK = DBHandler.GetResult("Duplication_checking_Of_MST_ACC_CLOSE_YEAR_MONTH_Pf", TxtYearMonth, ddlTransactionType, 
                        Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]), HttpContext.Current.Session["Menu_For"].ToString());
                    if (dTCHK.Rows.Count > 0)
                    {
                        msg = "This Year Month " + TxtYearMonth + " " + "Already in use." + " " + "Please Provide Another Year month";
                    }
                    else
                    {
                        DBHandler.Execute("Insert_MST_ACC_CLOSE_Pf", TxtYearMonth, ddlTransactionType, DdlTransactionStatus, 
                            Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]), 
                            Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]),
                            HttpContext.Current.Session["Menu_For"].ToString());
                        msg = "Record Inserted Successfully.";
                    }
                }
                else
                {
                    DBHandler.Execute("Acc_Loan_Health_Updation_Pf", TxtYearMonth, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
                    msg = "Record Inserted Successfully.";

                }
                
            }
        //}
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return msg;
    }

    //protected void cmdSave_Click(object sender, EventArgs e)
    //{
    //    try     
    //    { 
    //        Button cmdSave = (Button)sender;
    //        if (cmdSave.CommandName == "Add")
    //        {
    //            TextBox TxtYearMonth = (TextBox)dv.FindControl("TxtYearMonth");
    //            DropDownList ddlTransactionType = (DropDownList)dv.FindControl("ddlTransactionType");
    //            DropDownList DdlTransactionStatus = (DropDownList)dv.FindControl("DdlTransactionStatus");
    //            String YearMonth = TxtYearMonth.Text.Trim();
    //            //if (ddlTransactionType.SelectedValue == "H")
    //            //{
    //            //    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Health Code Generation Not Allowed For New Create Entry.')</script>");
    //            //}
    //            //else
    //            //{
    //                DataTable dTCHK = DBHandler.GetResult("Duplication_checking_Of_MST_ACC_CLOSE_YEAR_MONTH", YearMonth, ddlTransactionType.SelectedValue);
    //                if (dTCHK.Rows.Count > 0)
    //                {
    //                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This Year Month " + YearMonth + "" + "Already in use." + " " + "Please Provide Another Year month')</script>");
    //                }
    //                else
    //                {

    //                    if (ddlTransactionType.SelectedValue != "H")
    //                    {
    //                        DBHandler.Execute("Insert_MST_ACC_CLOSE", TxtYearMonth.Text, ddlTransactionType.SelectedValue, DdlTransactionStatus.SelectedValue, Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]), Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
    //                    }
    //                    else {
    //                        DBHandler.Execute("Acc_Loan_Health_Updation", TxtYearMonth.Text, Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]));
                        
    //                    }
    //                        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Inserted Successfully.')</script>");
    //                    cmdSave.Text = "Create";
    //                    dv.ChangeMode(FormViewMode.Insert);
    //                    dv.DataBind();
    //                    PopulateGrid();
    //                //}
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
    //    }
    //}

    protected void tbl_OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //int index1 = e.Row.RowIndex;
                DropDownList ddlTransType1 = (e.Row.FindControl("ddlTransType") as DropDownList);            
                string transtype1 = ddlTransType1.SelectedValue;

                DropDownList DdlTransStatus1 = (e.Row.FindControl("DdlTransStatus") as DropDownList);

                if (transtype1 == "I")
                {
                    DdlTransStatus1.Items.Clear();

                    DdlTransStatus1.Items.Insert(0, new ListItem() { Text = "Open", Value = "0" });
                    DdlTransStatus1.Items.Insert(1, new ListItem() { Text = "Calculate", Value = "1" });
                    DdlTransStatus1.Items.Insert(2, new ListItem() { Text = "Close", Value = "2" });
                }
                //else
                //{
                //    DdlTransStatus1.Items.Insert(0, new ListItem() { Text = "Open", Value = "0" });
                //    DdlTransStatus1.Items.Insert(1, new ListItem() { Text = "Transfer", Value = "1" });
                //    DdlTransStatus1.Items.Insert(2, new ListItem() { Text = "Close", Value = "2" });
                //}
                //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(tbl, "Select$" + e.Row.RowIndex);
                //e.Row.Attributes["style"] = "cursor:pointer";

                //string group = tbl.DataKeys[e.Row.RowIndex].Values[1].ToString();

                //foreach (TableCell cell in e.Row.Cells)
                //{
                //    if (group == "M")
                //    {
                //        e.Row.BackColor = System.Drawing.Color.Cyan;
                //    }

                //}
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }



    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
         if (e.CommandName == "Addition")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = tbl.Rows[index];
                TextBox txtyear = (TextBox)(tbl.Rows[index].Cells[0].FindControl("TxtyearMonth"));
                string years = txtyear.Text;
          
                DropDownList ddlTransType = (DropDownList)(tbl.Rows[index].Cells[1].FindControl("ddlTransType"));
                 string transtype = ddlTransType.SelectedValue;
                DropDownList DdlTransStatus = (DropDownList)(tbl.Rows[index].Cells[2].FindControl("DdlTransStatus"));
                string transStatus = DdlTransStatus.SelectedValue;

                

                string StatusVal = tbl.DataKeys[index].Values[0].ToString();
                string TransType = tbl.DataKeys[index].Values[1].ToString();
                string SectorID = tbl.DataKeys[index].Values[2].ToString();

                string MonthYear = txtyear.Text;
                string year = MonthYear.Substring(0, 4);
                string month = MonthYear.Substring(4,2);

                if (StatusVal == transStatus)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please change Transaction Status..')</script>");
                    PopulateGrid();
                    return;
                }

                //if (StatusVal == "0" && transStatus == "2")
                //{
                    
                //        ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('You can not change Transfer Status Open to Close. Please select Transfer before Close')</script>");
                //        PopulateGrid();
                //        return;
                     
                //}

                if (StatusVal == "1" && transStatus == "0")
                {

                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('You can not change Transfer Status from Transfer to Open.')</script>");
                    PopulateGrid();
                    return;

                }

                if (Convert.ToInt32(transStatus)==1)
                {
                    if (Convert.ToInt32(month) == 12)
                    {
                        year = (Convert.ToInt32(year) + 1).ToString();
                        month = "01";
                    }
                    else
                    {
                        month = (Convert.ToInt32(month) + 1).ToString();
                    }
                    MonthYear = year + (month.Length > 1 ? month : "0" + month);

                    DataTable dtCheckTransStatus = DBHandler.GetResult("Get_MST_Acc_Close_CheckTransStatus_Pf", TransType, MonthYear, HttpContext.Current.Session["Menu_For"].ToString());
                    if (dtCheckTransStatus.Rows.Count == 0)
                    {
                        string Status = "0";
                        DBHandler.Execute("Insert_MST_ACC_CLOSE_Pf", MonthYear, transtype, Status, SectorID, 
                            Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]), HttpContext.Current.Session["Menu_For"].ToString());
                    }

                    DBHandler.Execute("Update_Acc_Close_Pf", years, transtype, transStatus, SectorID, 
                        Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]), HttpContext.Current.Session["Menu_For"].ToString());
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                    dv.ChangeMode(FormViewMode.Insert);
                    dv.DataBind();
                    PopulateGrid();

                }

                if (Convert.ToInt32(transStatus) == 2)
                {

                    if (Convert.ToInt32(month) == 12)
                    {
                        year = (Convert.ToInt32(year) + 1).ToString();
                        month = "01";
                    }
                    else
                    {
                        month = (Convert.ToInt32(month) + 1).ToString();
                    }
                    MonthYear = year + (month.Length > 1 ? month : "0" + month);

                    DataTable dtCheckTransStatus = DBHandler.GetResult("Get_MST_Acc_Close_CheckTransStatus_Pf", TransType, MonthYear);
                    if (dtCheckTransStatus.Rows.Count == 0)
                    {
                        string Status = "0";
                        DBHandler.Execute("Insert_MST_ACC_CLOSE_Pf", MonthYear, transtype, Status, SectorID, 
                            Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]), HttpContext.Current.Session["Menu_For"].ToString());
                    }

                    DBHandler.Execute("Update_Acc_Close_Pf", years, transtype, transStatus, SectorID, 
                        Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]), HttpContext.Current.Session["Menu_For"].ToString());
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Account closing Successfully.')</script>");
                    dv.ChangeMode(FormViewMode.Insert);
                    dv.DataBind();
                    PopulateGrid();
                }
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
           
            DataTable dt = DBHandler.GetResult("Get_MST_Acc_Close_Pf", Session[SiteConstants.SSN_SECTOR_ID], HttpContext.Current.Session["Menu_For"].ToString());
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)     
        {
            throw new Exception(ex.Message);
        }
    }

    
    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    
}



