﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="GeneralLedger.aspx.cs" Inherits="GeneralLedger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/GeneralLedger.js"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center" class="borderStyle" style="width: 98%">
        <table class="headingCaption" width="100%" align="center">
            <tr>
                <td>General Ledger Report</td>
                
            </tr>
        </table>
         <asp:TextBox ID="txtOrgName" autocomplete="off" runat="server" ClientIDMode="Static" style="display:none" ></asp:TextBox>
        <asp:TextBox ID="txtOrgAddress" autocomplete="off" runat="server" ClientIDMode="Static" style="display:none"></asp:TextBox>
        <table style="width: 100%" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="100%" AutoGenerateRows="False"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">

                        <InsertItemTemplate>
                            <table style="width:100%;text-align:center;">
                                <tr>
                                    <td style="padding: 5px;font-weight:bold;" class="labelCaption">From Date &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="txtFromDate" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td style="padding: 5px;font-weight:bold;" class="labelCaption">To Date &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:TextBox ID="txtToDate" autocomplete="off" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td style="padding: 5px;" align="left">
                                        <asp:CheckBox ID="chkNarration" autocomplete="off" runat="server" Text=" Narration" TextAlign="Right" Font-Bold="true" Font-Size="12px" ForeColor="Navy"/>
                                    </td>

                                    <td style="padding: 5px;" align="left">
                                        <asp:CheckBox ID="chkInstrument" autocomplete="off" runat="server" Text=" Instrument" TextAlign="Right" Font-Bold="true" Font-Size="12px" ForeColor="Navy"/>
                                    </td>

                                </tr>
                                 </table>
                               <%-- <tr>
                                    <td style="padding: 5px;" class="labelCaption">Narration &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    
                                    <td style="padding: 5px;" class="labelCaption">Instrument &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    
                                </tr>--%>
                                <tr>
                                    <td class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                
                               <%-- <tr>
                                    <td style="padding: 5px;" class="labelCaption">Select All GL&nbsp;&nbsp;<span class="require"></span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                   
                                    <td style="padding: 5px;" class="labelCaption">Select All Sectors &nbsp;&nbsp;<span class="require"></span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    
                                </tr>

                                <tr>
                                    <td colspan="6" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>--%>
                               

                                <table style="width: 100%" cellpadding="5" align="center" cellspacing="0">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                         <td style="padding: 5px;" align="left">
                                      <asp:CheckBox ID="chkAllGl" autocomplete="off" runat="server" Text=" Select All GL" TextAlign="Right" Font-Bold="true" Font-Size="12px" ForeColor="Navy"/>  
                                    </td>
                                        <td></td>
                                        <td></td>
                                        <td style="padding: 5px;" align="left">
                                        <asp:CheckBox ID="chkAllSector" autocomplete="off" runat="server" Text=" Select All Sectors" TextAlign="Right" Font-Bold="true" Font-Size="12px" ForeColor="Navy"/>
                                    </td>
                                    </tr>
                                <tr>
                                    <td style="padding: 5px; width: 30px;font-weight:bold;" class="labelCaption">GL &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <div style="height: 150px; width:450px; overflow-y: scroll;">
                                            <asp:CheckBoxList ID="ddlGLdetails" autocomplete="off" DataSource='<%# DataTableGLload() %>' RepeatColumns="1"
                                                RepeatDirection="Vertical" DataValueField="GL_ID" CssClass="textbox" Width="250px" Font-Bold="true"
                                                DataTextField="GL_NAME" runat="server" AppendDataBoundItems="true">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>

                                    <td style="padding: 5px; width:130px;font-weight:bold;" class="labelCaption">Sector &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td style="padding: 5px;" align="left">
                                        <div style="height: 150px; width: 250px; overflow-y: scroll;">
                                            <asp:CheckBoxList ID="ddlSector" DataSource='<%# DataTableSector() %>' RepeatColumns="1" autocomplete="off"
                                                RepeatDirection="Vertical" Font-Bold="true"
                                                DataValueField="SectorID" CssClass="textbox" Width="180px"
                                                DataTextField="SectorName" runat="server"
                                                AppendDataBoundItems="true">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                        </InsertItemTemplate>

                        <FooterTemplate>
                            <table align="center" width="100%">
                                
                                <tr>
                                    <td class="labelCaption"><span class="require" style="font-weight:bold;">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSearch" runat="server" Text="Show" CommandName="Add" autocomplete="off"
                                                Width="90" Height="30" CssClass="save-button DefaultButton"
                                                OnClick="cmdSearch_Click" />

                                            <asp:Button ID="cmdCancel" runat="server" Text="Refresh" autocomplete="off"
                                                Width="90" Height="30" CssClass="save-button DefaultButton"
                                                OnClick="cmdCancel_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
        </table>

        <table width="100%" style="text-align: center;">
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td width="25%"></td>
                <td width="35%">
                    <asp:Button ID="btnPrevious" autocomplete="off" runat="server" Text="<<Previous" OnClick="btnPrevious_Click" Width="90" Height="30" CssClass="save-button" />
                </td>
                <td width="15%">
                    <asp:Button ID="btnNext" autocomplete="off" runat="server" Text="Next>>" OnClick="btnNext_Click" Width="90" Height="30" CssClass="save-button" />
                </td>
                <td width="25%"></td>
            </tr>
        </table>

        <div id="GridPrintDiv" class="borderStyle" align="center" style="width: 99%;">
            <div style="text-align: right">
                <asp:Label ID="lblPage" autocomplete="off" runat="server" Font-Size="Medium" Text="" Style="ruby-align: right"></asp:Label>
            </div>
              
            <center>
                <asp:Label ID="lblOffice1" autocomplete="off" runat="server" ClientIDMode="Static" Font-Size="Large" Text="WEST BENGAL FINANCIAL CORPORATION"></asp:Label>
                <br />
                <asp:Label ID="lblOffice2" autocomplete="off" runat="server" ClientIDMode="Static" Font-Size="Large" Text="DD22, SECTOR - 1, BIDHANNAGAR, KOLKATA -700064"></asp:Label>
                <br />
                <br />
                <asp:Label ID="lblDuration" autocomplete="off" runat="server" Font-Size="Medium" Text=""></asp:Label>
                <br />
                <asp:Label ID="lblReportName" autocomplete="off" runat="server" Font-Size="Medium" Text=""></asp:Label>
            </center>

            <asp:GridView ID="tbl" runat="server" autocomplete="off" Width="100%" BorderColor="White" GridLines="Both" ShowFooter="true"
                AutoGenerateColumns="false" CellSpacing="2" AllowPaging="true" PageSize="15" OnPageIndexChanging="tbl_PageIndexChanging">

                <AlternatingRowStyle BackColor="Honeydew" />
                <SelectedRowStyle BackColor="#87CEEB" />
                <Columns>
                    <asp:BoundField DataField="VCH_DATE" HeaderText="DATE" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="DOC_TYPE" HeaderText="DOC_TYPE" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="PARTICULARS" HeaderText="PARTICULARS" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="BK_GLSL_CODE" HeaderText="CODE" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="DEBIT" HeaderText="DEBIT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField DataField="CREDIT" HeaderText="CREDIT" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                </Columns>
                <FooterStyle CssClass="labelCaption" BackColor="#99ccff" Font-Bold="true" HorizontalAlign="Right"/>
            </asp:GridView>
            <asp:Label ID="lblBorder2" runat="server" Text=""></asp:Label>
        </div>
        <br />
        <table width="100%" style="text-align: center;">
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td colspan="3" width="40%"></td>
                <td>
                    <asp:Button ID="btnPrintSingle" autocomplete="off" runat="server" OnClientClick="PrintSinglePage()" Text="Print Current Report" Width="150" Height="40" CssClass="save-button DefaultBtn" />
                    <asp:Button ID="btnPrintAll" autocomplete="off" runat="server" OnClientClick="PrintMultiPlePages()" Text="Print All Reports" Width="150" Height="40" CssClass="save-button DefaultBtn" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

