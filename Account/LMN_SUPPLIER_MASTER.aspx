﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LMN_SUPPLIER_MASTER.aspx.cs" Inherits="LMN_SUPPLIER_MASTER" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("input").focus(function () {
                $(this).css("background-color", "#cccccc");
            });
            $("input").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

            $("select").focus(function () {
                $(this).css("background-color", "#cccccc");
            });

            $("select").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

            $('#TxtVendorName').focus();

            $('#TxtVendorName').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtVendorName').val().trim() == '') {
                        alert('Please Type Vendor Name');
                        $('#TxtVendorName').focus();
                        return false;
                    }
                    else {
                        $('#TxtAddress1').focus();
                        return false;
                    }
                }
            });

            $('#TxtAddress1').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtAddress1').val().trim() == '') {
                        alert('Please Type Address');
                        $('#TxtAddress1').focus();
                        return false;
                    }
                    else {
                        $('#TxtAddress2').focus();
                        return false;
                    }
                }
            });

            $('#TxtAddress2').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtAddress2').val().trim() == '') {
                        alert('Please Type Address');
                        $('#TxtAddress2').focus();
                        return false;
                    }
                    else {
                        $('#TxtAddress3').focus();
                        return false;
                    }
                }
            });

            $('#TxtAddress3').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtAddress3').val().trim() == '') {
                        alert('Please Type Address');
                        $('#TxtAddress3').focus();
                        return false;
                    }
                    else {
                        $('#TxtPin').focus();
                        return false;
                    }
                }
            });

            $('#TxtPin').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtPin').val().trim() == '') {
                        alert('Please Type Pin');
                        $('#TxtPin').focus();
                        return false;
                    }
                    else {
                        $('#TxtPhone').focus();
                        return false;
                    }
                }
            });

            $('#TxtPhone').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtPhone').val().trim() == '') {
                        alert('Please Type Phone Number');
                        $('#TxtPhone').focus();
                        return false;
                    }
                    else {
                        $('#TxtPan').focus();
                        return false;
                    }
                }
            });

            $('#TxtPan').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtPan').val().trim() == '') {
                        alert('Please Type Pan Number');
                        $('#TxtPan').focus();
                        return false;
                    }
                    else {
                        $('#TxtVat').focus();
                        return false;
                    }
                }
            });

            $('#TxtVat').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtVat').val().trim() == '') {
                        alert('Please Type Vat Number');
                        $('#TxtVat').focus();
                        return false;
                    }
                    else {
                        $('#TxtCST').focus();
                        return false;
                    }
                }
            });

            $('#TxtCST').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtCST').val().trim() == '') {
                        alert('Please Type CST Number');
                        $('#TxtCST').focus();
                        return false;
                    }
                    else {
                        $('#TxtST').focus();
                        return false;
                    }
                }
            });

            $('#TxtST').keypress(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode == 13) {
                    if ($('#TxtST').val().trim() == '') {
                        alert('Please Type ST');
                        $('#TxtST').focus();
                        return false;
                    }
                    else {
                        $('#cmdSave').focus();
                        return false;
                    }
                }
            });


        }); // Main Closed

        function beforeSave() {
            $("#frmEcom").validate();

            $("#TxtVendorName").rules("add", { required: true, messages: { required: "Please enter Name" } });
            $("#TxtAddress1").rules("add", { required: true, messages: { required: "Please enter Your Address" } });
            $("#TxtAddress2").rules("add", { required: true, messages: { required: "Please enter Your Address" } });
            $("#TxtAddress3").rules("add", { required: true, messages: { required: "Please enter your Address" } });
            $("#TxtPin").rules("add", { required: true, messages: { required: "Please enter your Pin" } });
            $("#TxtPhone").rules("add", { required: true, messages: { required: "Please enter Phone number" } });
            $("#TxtPan").rules("add", { required: true, messages: { required: "Please enter Your Pan number" } });
            $("#TxtVat").rules("add", { required: true, messages: { required: "Please enter your Vat No." } });
            $("#TxtCST").rules("add", { required: true, messages: { required: "Please enter CST" } });
            $("#TxtST").rules("add", { required: true, messages: { required: "Please enter ST" } });
        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }

        $(document).ready(function () {
            $('#TxtVendorName').keypress(function (e) {
                var regex = new RegExp("^[\\sa-zA-Z]*$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                else {
                    e.preventDefault();
                    alert('Please Enter Alphabate');
                    return false;
                }
            });
        });

        $(document).ready(function () {
            $("#TxtPhone,#TxtPan,#TxtPin,#TxtVat,#TxtCST,#TxtST").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    if (e.which != 13) {
                        alert("Please Enter Numeric Value");
                    }
                    return false;
                }
            });
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>SUPPLIER MASTER</td>
            </tr>
        </table>
        <br />
        
        <table width="98%" cellpadding="0" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="0" AutoGenerateRows="False" autocomplete="off"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None" >
                        <InsertItemTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td class="labelCaption">Vendor Name &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:TextBox ID="TxtVendorName" autocomplete="off" Width="250px"  ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Address 1 &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtAddress1" autocomplete="off" Width="250px"  ClientIDMode="Static" runat="server" CssClass="inputbox2" PlaceHolder="ADD1"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Address 2 &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtAddress2" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" PlaceHolder="ADD2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Address 3 &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtAddress3" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" PlaceHolder="ADD3"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Pin<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtPin" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Phone NO.<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtPhone" autocomplete="off" Width="250px" MaxLength="10" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">PAN NO. &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtPan" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">VAT NO. &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtVat" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="labelCaption">CST &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtCST" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                      <td class="labelCaption">ST &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtST" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>


                        <EditItemTemplate>
                             <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td class="labelCaption">Vendor Name &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                         <asp:TextBox ID="TxtVendorName"  autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("VENDOR_NAME") %>'></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Address 1 &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtAddress1" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" PlaceHolder="ADD1" Text='<%# Eval("VENDOR_ADD1") %>'></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Address 2 &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtAddress2" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" PlaceHolder="ADD2" Text='<%# Eval("VENDOR_ADD2") %>'></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Address 3 &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtAddress3" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" PlaceHolder="ADD3" Text='<%# Eval("VENDOR_ADD3") %>'></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">Pin<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtPin" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("VENDOR_PIN_CODE") %>'></asp:TextBox>
                                    </td>
                                     <td class="labelCaption">Phone NO.<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtPhone" autocomplete="off" MaxLength="10" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("VENDOR_PHONE") %>'></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption">PAN NO. &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtPan" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("VENDOR_PAN_NO") %>'></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">VAT NO. &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtVat" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("VENDOR_VAT_NO") %>'></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="labelCaption">CST &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtCST" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("VENDOR_CST_NO") %>'></asp:TextBox>
                                    </td>
                                     <td class="labelCaption">ST &nbsp;&nbsp;<span class="require">*</span></td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtST" autocomplete="off" Width="250px" ClientIDMode="Static" runat="server" CssClass="inputbox2" Text='<%# Eval("VENDOR_ST_NO") %>'></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="0" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdRefresh_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
             <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                            AutoGenerateColumns="false" DataKeyNames="VENDOR_CODE" OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("VENDOR_CODE") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton autocomplete="off" CommandName='Del' ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("VENDOR_CODE")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("VENDOR_CODE") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                                
                                <asp:BoundField DataField="VENDOR_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Name" />
                                <asp:BoundField DataField="VENDOR_ADD1" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Address 1" />
                                <asp:BoundField DataField="VENDOR_ADD2" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Address 2" />
                                <asp:BoundField DataField="VENDOR_ADD3" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Address 3 " />             
                                <asp:BoundField DataField="VENDOR_PIN_CODE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Pin" />
                                <asp:BoundField DataField="VENDOR_PHONE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Phone" />
                                <asp:BoundField DataField="VENDOR_PAN_NO" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Pan NO" />
                                <asp:BoundField DataField="VENDOR_VAT_NO" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Vat" /> 
                                <asp:BoundField DataField="VENDOR_CST_NO" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="CST NO" />
                                <asp:BoundField DataField="VENDOR_ST_NO" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="ST NO 1" />         
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

