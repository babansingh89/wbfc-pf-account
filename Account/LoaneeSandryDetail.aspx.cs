﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Drawing;
using System.Configuration;

public partial class LoaneeSandryDetail : System.Web.UI.Page
{
    protected string current_Date = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }

            if (!IsPostBack)
            {
                Get_CurrentFinancialYear();
            }
            else
            {
                TextBox txtDate = (TextBox)dv.FindControl("txtDate");
                current_Date = txtDate.Text;
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void Get_CurrentFinancialYear()
    {
        try
        {
            string FinancialYear = "";
            DataTable dtResult = DBHandler.GetResult("Get_Financial_Year");
            if (dtResult.Rows.Count > 0)
            {
                current_Date = dtResult.Rows[0]["cur_date"].ToString();
                FinancialYear = dtResult.Rows[0]["Financial_Year"].ToString();
                //First_Date = "01" + "/04/" + FinancialYear.Substring(0, 4);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "search")
            {
                TextBox txtDate = (TextBox)dv.FindControl("txtDate");
                DropDownList ddlSearch = (DropDownList)dv.FindControl("ddlSearch");
                string SectorID = HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID].ToString();
                if (ddlSearch.SelectedValue != "")
                {
                    PopulateGrid(ddlSearch.SelectedValue, txtDate.Text, SectorID);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please select a search type.')</script>");
                    Get_CurrentFinancialYear();
                    ddlSearch.Focus();
                }
                
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid(string searchType, string dueDate, string sector)
    {
        try
        {
            DropDownList ddlSearch = (DropDownList)dv.FindControl("ddlSearch");
            TextBox txtDate = (TextBox)dv.FindControl("txtDate");
            DateTime dtdue_Date = new DateTime();
            if (dueDate != "")
            {
                dtdue_Date = DateTime.ParseExact(dueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                dueDate = "";
            }
            DataTable dt = DBHandler.GetResult("Get_LoaneeSandryDetail", searchType,
                                            dueDate.Equals("") ? DBNull.Value : (object)dtdue_Date.ToShortDateString(), Convert.ToInt32(sector));

            if (dt.Rows.Count > 0)
            {
                panGrd.Visible = true;
                tbl.DataSource = dt;
                tbl.DataBind();
            }
            else
            {
                Get_CurrentFinancialYear();
                panGrd.Visible = false;
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No records found.')</script>");
            }
            if (ddlSearch.SelectedValue == "D")
            {
                cmdUpdate.Visible = true;
                cmdRefresh.Visible = true;
                Get_CurrentFinancialYear();
            }
            else
            {
                current_Date = txtDate.Text;
            }            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    

    protected void cmdUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            string VchNo = "";
            string GLID = "";
            string SLID = "";
            string SUBID = "";
            string SectorID = "";
            DropDownList ddlSearch = (DropDownList)dv.FindControl("ddlSearch");
            TextBox txtDate = (TextBox)dv.FindControl("txtDate");
            if (tbl.Rows.Count > 0)
            {
                for (int i = 0; i <= tbl.Rows.Count - 1; i++)
                {
                    VchNo = tbl.DataKeys[i].Values[0].ToString();
                    GLID = tbl.DataKeys[i].Values[1].ToString();
                    SLID = tbl.DataKeys[i].Values[2].ToString();
                    SUBID = tbl.DataKeys[i].Values[3].ToString();
                    SectorID = tbl.DataKeys[i].Values[4].ToString();

                    TextBox txtDueDate = tbl.Rows[i].Cells[5].FindControl("txtDueDate") as TextBox;
                    string duedate = txtDueDate.Text;
                    DateTime dtdue_Date = new DateTime();
                    if (duedate != "")
                    {
                        dtdue_Date = DateTime.ParseExact(duedate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        duedate = "";
                    }

                    DataTable dtResult = DBHandler.GetResult("Update_LoaneeSandryDetail", Convert.ToInt64(VchNo), GLID, SLID, SUBID,
                                            duedate.Equals("") ? DBNull.Value : (object)dtdue_Date.ToShortDateString(), Convert.ToInt32(SectorID),
                                            Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID].ToString()));

                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Updated Successfullly.')</script>");

                    PopulateGrid(ddlSearch.SelectedValue, txtDate.Text, SectorID);
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
  
}