﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Drawing;
using System.Configuration;
using System.Web.Script.Services;
using System.Web.Services;

public partial class LoanInterestReport : System.Web.UI.Page
{
    static string StrFormula = "";
    static string StrSecID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            panGrd.Visible = false;
            if (!IsPostBack)
            {
                
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload()
    {
        DataTable dt = DBHandler.GetResult("Get_UserWiseSector", Session[SiteConstants.SSN_INT_USER_ID]);
        return dt;
    }

    private void PopulateGrid(string yearmon, string sectors)
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_LoanInterestReport",
                                            yearmon.Equals("") ? DBNull.Value : (object)yearmon,  sectors);
            DataTable dt1 = new DataTable();
            foreach (DataColumn dc in dt.Columns)
            {
                dt1.Columns.Add(dc.ColumnName, dc.DataType);
            }
            dt1.Columns.Add("penalty",typeof( decimal));
            dt1.Columns.Add("rebate",typeof( decimal));

            foreach (DataRow dr in dt.Rows)
            {
                DataRow dr1 = dt1.NewRow();
                foreach (DataColumn dc in dt.Columns)
                {
                    dr1[dc.ColumnName]=dr[dc.ColumnName];
                }
                dr1 = calculateAdditional(dr1);
                dt1.Rows.Add(dr1);
            }


            if (dt1.Rows.Count > 0)
            {
                panGrd.Visible = true;

                //DataTable dtt = dt1.AsEnumerable().Where(t => t.Field<decimal>("AMOUNT") < 700).CopyToDataTable();
                tbl.DataSource = dt1;
                tbl.DataBind();

                //decimal total = dt.AsEnumerable().Sum(row => row.Field<decimal>("AMOUNT"));

                //tbl.FooterRow.Cells[4].Text = "Total";
                //tbl.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                //tbl.FooterRow.Cells[4].Font.Bold = true;
                //tbl.FooterRow.Cells[4].Font.Size = 9;

                //tbl.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                //tbl.FooterRow.Cells[5].Text = total.ToString("N2");
                //tbl.FooterRow.Cells[5].Font.Bold = true;
                //tbl.FooterRow.Cells[5].Font.Size = 9;
            }
            else
            {
                panGrd.Visible = false;
                clearAll();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Enter Correct Year month(YYYYMM) and Sectors. No records found for this Year month and Sectors.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    DataRow calculateAdditional(DataRow dr)
    {
         try
        {
            string sub_code = dr["SUB_CODE"].ToString().Trim();
               
                    if (sub_code == "26")
                    {
                        dr["rebate"] = dr["AMOUNT"];
                        dr["AMOUNT"] = "0.00";
                        dr["penalty"] = "0.00";
                    }
                    else if (sub_code == "27")
                    {
                        dr["penalty"] = dr["AMOUNT"];
                        dr["AMOUNT"] = "0.00";
                        dr["rebate"] = "0.00";
                        
                    }
                    else
                    {
                        dr["penalty"] = "0.00";
                       dr["rebate"] = "0.00";
                    }             
         }
         catch(Exception ex)
         {
             ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
         }
        return dr;
    }

    string ln_type = "";
    decimal subTotal_interest = 0;
    decimal subTotal_penalty = 0;
    decimal subTotal_Rebate = 0;
    decimal total_interest = 0;
    decimal total_penalty= 0;
    decimal total_rebate = 0;
    int subTotalRowIndex = 0;

    protected void OnRowCreated(object sender, GridViewRowEventArgs e)
    {
        if (tbl.DataSource == null)
        {
            return;
        }
        subTotal_interest = 0;
        subTotal_penalty = 0;
        subTotal_Rebate = 0;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataTable dt = new DataTable();
            DataRowView rowView = (DataRowView)e.Row.DataItem;

            dt = rowView.DataView.Table;
            
            string LOANEE_TYPE = dt.Rows[e.Row.RowIndex]["LOANEE_TYPE"].ToString();
            total_interest += Convert.ToDecimal(dt.Rows[e.Row.RowIndex][5].ToString());
            total_penalty += Convert.ToDecimal(dt.Rows[e.Row.RowIndex][6].ToString());
            total_rebate += Convert.ToDecimal(dt.Rows[e.Row.RowIndex][7].ToString());

            if (LOANEE_TYPE != ln_type)
            {
                if (e.Row.RowIndex > 0)
                {
                    for (int i = subTotalRowIndex; i < e.Row.RowIndex; i++)
                    {
                        subTotal_interest += Convert.ToDecimal(tbl.Rows[i].Cells[5].Text);
                        subTotal_penalty += Convert.ToDecimal(tbl.Rows[i].Cells[6].Text);
                        subTotal_Rebate += Convert.ToDecimal(tbl.Rows[i].Cells[7].Text);
                    }
                    this.AddTotalRow("Sub Total", subTotal_interest.ToString("N2"), subTotal_penalty.ToString("N2"), subTotal_Rebate.ToString("N2"));
                    subTotalRowIndex = e.Row.RowIndex;
                }
                ln_type = LOANEE_TYPE;
            }
        }
    }

    private void AddTotalRow(string labelText, string value1, string value2, string value3)
    {
        GridViewRow row = new GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal);
        row.BackColor = ColorTranslator.FromHtml("#99ccff");
        row.Font.Size = 9;
        row.Font.Bold = true; 
        row.Cells.AddRange(new TableCell[8] { new TableCell (), new TableCell (), new TableCell (), new TableCell (),//Empty Cell
                                        new TableCell { Text = labelText, HorizontalAlign = HorizontalAlign.Right},
                                        new TableCell { Text = value1, HorizontalAlign = HorizontalAlign.Right },
                                        new TableCell { Text = value2, HorizontalAlign = HorizontalAlign.Right },
                                        new TableCell { Text = value3, HorizontalAlign = HorizontalAlign.Right }});
    
       

        tbl.Controls[0].Controls.Add(row);
    }

    protected void OnDataBound(object sender, EventArgs e)
    {
        for (int i = subTotalRowIndex; i < tbl.Rows.Count; i++)
        {
            subTotal_interest += Convert.ToDecimal(tbl.Rows[i].Cells[5].Text);
            subTotal_penalty += Convert.ToDecimal(tbl.Rows[i].Cells[6].Text);
            subTotal_Rebate += Convert.ToDecimal(tbl.Rows[i].Cells[7].Text);
        }
        this.AddTotalRow("Sub Total", subTotal_interest.ToString("N2"), subTotal_penalty.ToString("N2"), subTotal_Rebate.ToString("N2"));
        this.AddTotalRow("Grand Total", total_interest.ToString("N2"), total_penalty.ToString("N2"), total_rebate.ToString("N2"));
    }

    protected void tbl_OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        //try
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        string sub_code = tbl.DataKeys[e.Row.RowIndex].Values[1].ToString();

        //        foreach (TableCell cell in e.Row.Cells)
        //        {
        //            if (sub_code == "26")
        //            {
        //                e.Row.Cells[5].Text = "0.00";
        //                e.Row.Cells[6].Text = "0.00";
        //            }
        //            else if (sub_code == "27")
        //            {
        //                e.Row.Cells[5].Text = "0.00";
        //                e.Row.Cells[7].Text = "0.00";
        //            }
        //            else
        //            {
        //                e.Row.Cells[6].Text = "0.00";
        //                e.Row.Cells[7].Text = "0.00";
        //            }

        //            //total_interest += Convert.ToDecimal(e.Row.Cells[5].Text);
        //            //total_penalty += Convert.ToDecimal(e.Row.Cells[6].Text);
        //            //total_rebate += Convert.ToDecimal(e.Row.Cells[7].Text);
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        //}

    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "search")
            {
                string strSector = "";
                TextBox txtYearmonth = (TextBox)dv.FindControl("txtYearmonth");

                CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");
                int cnt = 0;
                for (int j = 0; j < ddlSector.Items.Count; j++)
                {
                    if (ddlSector.Items[j].Selected == true)
                    {
                        cnt++;
                    }
                }
                if (cnt == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please check at least one Sector for search.')</script>");
                    return;
                }
                cnt = 0;
                for (int k = 0; k < ddlSector.Items.Count; k++)
                {
                    if (ddlSector.Items[k].Selected == true)
                    {
                        strSector = strSector + ddlSector.Items[k].Value + ",";
                    }
                }

                strSector = strSector.Substring(0, strSector.Length - 1);


                PopulateGrid(txtYearmonth.Text, strSector);
            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    private void clearAll()
    {
        try
        {
            TextBox txtYearmonth = (TextBox)dv.FindControl("txtYearmonth");
            tbl.DataSource = null;
            tbl.DataBind();
            panGrd.Visible = false;
            txtYearmonth.Text = "";
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void cmdCancelSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string YearMonth, string SecotrId, string StandNPA)
    {
        string JSONVal = "";
        
        try   
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            String Msg = "";
            StrFormula = "";
            int UserID;
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SecotrId = StrSecID;
             
            StrFormula = "{Get_LoanInterestReport;1.LOANEE_TYPE}='" + StandNPA + "'";

            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, YearMonth, SecotrId, "", "", "", "", "", "", "", "");

            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }


    protected void ddlSector_SelectedIndexChanged(object sender, EventArgs e)
    {
        StrSecID = "";
        CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");
        for (int i = 0; i < ddlSector.Items.Count; i++)
        {
            if (ddlSector.Items[i].Selected)
            {
                if (StrSecID == "")
                {
                    StrSecID = ddlSector.Items[i].Value.ToString();
                }
                else {
                    StrSecID= StrSecID + "," + ddlSector.Items[i].Value.ToString();
                }
            }
        }
    }
}