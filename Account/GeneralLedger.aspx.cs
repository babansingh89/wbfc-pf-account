﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;
using System.Data;
using System.Web.Services;
using System.Collections;
using System.Text;
using System.Web.Script.Services;
using System.Globalization;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.IO;
using System.Reflection;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using System.Configuration;

public partial class GeneralLedger : System.Web.UI.Page
{
    static DataTable MST_Table;
    static DataTable[] DT_Details = new DataTable[100];
    static DataTable[] DT_Details1 = new DataTable[100];
    static int MaxLength = -1, TempMax = 0;
    static string FromDate;
    static string ToDate;
    int PageNo = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateGrid();
            MST_Table = null;
            if (MaxLength != -1)
            {
                for (int i = 0; i <= MaxLength; i++)
                {
                    DT_Details[i] = null;
                    DT_Details1[i] = null;
                }
            }
            MaxLength = -1;
            TempMax = 0;
            FromDate = "";
            ToDate = "";
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("VCH_DATE");
            dt.Columns.Add("DOC_TYPE");
            dt.Columns.Add("PARTICULARS");
            dt.Columns.Add("BK_GLSL_CODE");
            dt.Columns.Add("DEBIT");
            dt.Columns.Add("CREDIT");
            dt.Rows.Add();
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected DataTable DataTableGLload()
    {
        DataTable dt = DBHandler.GetResult("Get_GLForDropDown_Pf", HttpContext.Current.Session["Menu_For"].ToString());
        return dt;
    }

    protected DataTable DataTableSector()
    {
        DataTable dt = DBHandler.GetResult("Get_UserWiseSector_Pf", Session[SiteConstants.SSN_INT_USER_ID], HttpContext.Current.Session["Menu_For"].ToString());
        return dt;
    }

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        int SecLen = 0;
        TextBox txtFromDate = (TextBox)dv.FindControl("txtFromDate");
        TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
        CheckBox chkNarration = (CheckBox)dv.FindControl("chkNarration");
        CheckBox chkInstrument = (CheckBox)dv.FindControl("chkInstrument");
        CheckBoxList ddlGLdetails = (CheckBoxList)dv.FindControl("ddlGLdetails");
        CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");

        FromDate = txtFromDate.Text.ToString();
        ToDate = txtToDate.Text.ToString();
        DateTime fDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);
        DateTime tDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy",
                                           System.Globalization.CultureInfo.InvariantCulture);

        string Narration;
        if (chkNarration.Checked)
        {
            Narration = "y";
        }
        else
        {
            Narration = "n";
        }
        string Instrument;
        if (chkInstrument.Checked)
        {
            Instrument = "y";
        }
        else
        {
            Instrument = "n";
        }
        string GLDetails = "";
        for (int i = 0; i < ddlGLdetails.Items.Count; i++)
        {
            if (ddlGLdetails.Items[i].Selected)
            {
                GLDetails += ddlGLdetails.Items[i].Value.ToString() + ",";
            }
        }

        string SectorDetails = "";
        for (int i = 0; i < ddlSector.Items.Count; i++)
        {
            if (ddlSector.Items[i].Selected)
            {
                SectorDetails += ddlSector.Items[i].Value.ToString() + ",";
                SecLen++;
            }
        }

        //===================== Start Sector Name , Address And Org Name code Here ==============================//
       
        txtOrgName.Text = "";
        txtOrgAddress.Text = "";
        DataTable dtSec = DBHandler.GetResult("GET_SecNameWithAddress_Pf", SectorDetails, SecLen);
        if (dtSec.Rows.Count > 0)
        {
            foreach (DataRow dtRow in dtSec.Rows)
            {
                if (txtOrgName.Text == "")
                {
                    txtOrgName.Text = dtRow["OrgName"].ToString();
                    txtOrgAddress.Text = dtRow["Sector"].ToString();
                }
                else
                {
                    txtOrgAddress.Text = txtOrgAddress.Text + " , " + dtRow["Sector"].ToString();
                } 
            }
        }
           
        lblOffice1.Text = txtOrgName.Text;
        lblOffice2.Text = txtOrgAddress.Text;
        //===================== End Sector Name , Address And Org Name code Here ==============================//
        if (FromDate == "" || ToDate == "" || GLDetails == "" || SectorDetails == "")
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Select Required Fields Prior To Report Generation!')</script>");
        }
        else if (fDate > tDate)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('From Date Cannot Be Greater Than To Date!')</script>");
        }
        else
        {
            MST_Table = DBHandler.GetResult("Acc_Gl_Ledger_Pf", FromDate, ToDate, GLDetails, Narration, Instrument, SectorDetails, HttpContext.Current.Session["Menu_For"].ToString());
            //string OLD_GL_ID;

            if (MST_Table.Rows.Count > 0)
            {
                string temp = "";
                int i = -1;
                foreach (DataRow Mst_Obj in MST_Table.Rows)
                {
                    if (temp == "" || temp != Mst_Obj["OLD_GL_ID"].ToString())
                    {
                        i = i + 1;
                        MaxLength = i;
                        DT_Details[i] = new DataTable();
                        DT_Details1[i] = new DataTable();
                        DT_Details[i].Columns.Add("SL_NO");
                        DT_Details[i].Columns.Add("GL_ID");
                        DT_Details[i].Columns.Add("OLD_GL_ID");
                        DT_Details[i].Columns.Add("GL_NAME");
                        DT_Details[i].Columns.Add("SL_ID");
                        DT_Details[i].Columns.Add("OLD_SL_ID");
                        DT_Details[i].Columns.Add("BK_GL_NAME");
                        DT_Details[i].Columns.Add("BK_GLSL_CODE");
                        DT_Details[i].Columns.Add("VCH_DATE");
                        DT_Details[i].Columns.Add("DOC_TYPE");
                        DT_Details[i].Columns.Add("PARTICULARS");
                        DT_Details[i].Columns.Add("DEBIT");
                        DT_Details[i].Columns.Add("CREDIT");
                        DT_Details[i].Rows.Add();

                        DT_Details[i].Rows.RemoveAt(0);
                        DT_Details[i].Rows.Add(Mst_Obj.ItemArray);
                        
                        temp = Mst_Obj["OLD_GL_ID"].ToString();
                    }
                    else
                    {
                        DT_Details[i].Rows.Add(Mst_Obj.ItemArray);
                    }
                }
                temp = "";
                i = -1;
                foreach (DataRow Mst_Obj1 in MST_Table.Rows)
                {
                    if (temp == "" || temp != Mst_Obj1["OLD_GL_ID"].ToString())
                    {
                        i = i + 1;
                        MaxLength = i;
                        DT_Details1[i].Columns.Add("SL_NO");
                        DT_Details1[i].Columns.Add("GL_ID");
                        DT_Details1[i].Columns.Add("OLD_GL_ID");
                        DT_Details1[i].Columns.Add("GL_NAME");
                        DT_Details1[i].Columns.Add("SL_ID");
                        DT_Details1[i].Columns.Add("OLD_SL_ID");
                        DT_Details1[i].Columns.Add("BK_GL_NAME");
                        DT_Details1[i].Columns.Add("BK_GLSL_CODE");
                        DT_Details1[i].Columns.Add("VCH_DATE");
                        DT_Details1[i].Columns.Add("DOC_TYPE");
                        DT_Details1[i].Columns.Add("PARTICULARS");
                        DT_Details1[i].Columns.Add("DEBIT");
                        DT_Details1[i].Columns.Add("CREDIT");
                        DT_Details1[i].Rows.Add();

                        DT_Details1[i].Rows.RemoveAt(0);
                        DT_Details1[i].Rows.Add(Mst_Obj1.ItemArray);
                        temp = Mst_Obj1["OLD_GL_ID"].ToString();
                    }
                    else
                    {

                        for (int w = 0; w < Mst_Obj1.ItemArray.Length; w++)
                        {
                            string x = Mst_Obj1.ItemArray[w].ToString();
                            x = x.Replace("<br>", "");
                            if (w == 10)
                            {
                                Mst_Obj1["PARTICULARS"] = x;
                            }
                        }
                        DT_Details1[i].Rows.Add(Mst_Obj1.ItemArray);
                    }
                }
            }
           
            if (MaxLength != -1)
            {
                tbl.DataSource = DT_Details1[0];
                tbl.DataBind();
                lblPage.Text = "Page : " + Convert.ToString(TempMax + 1) + " of " + Convert.ToString(MaxLength + 1);
                lblDuration.Text = "GENERAL LEDGER FROM  " + FromDate + "  TO  " + ToDate;
                lblReportName.Text = DT_Details1[0].Rows[0]["GL_NAME"].ToString() + " (" + DT_Details1[0].Rows[0]["OLD_GL_ID"].ToString() + ")";
                if (DT_Details1[0].Rows.Count <= 15)
                {
                    double TotalDebit = 0, TotalCredit = 0;
                    tbl.FooterRow.Cells[0].Text = "Total :";
                    foreach (DataRow dtRow in DT_Details1[0].Rows)
                    {
                        TotalDebit += Convert.ToDouble(dtRow["DEBIT"].ToString());
                        TotalCredit += Convert.ToDouble(dtRow["CREDIT"].ToString());
                    }
                    tbl.FooterRow.Cells[4].Text = TotalDebit.ToString();

                    tbl.FooterRow.Cells[5].Text = TotalCredit.ToString();
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No Record Found With These Specifications!')</script>");
            }
        }
    }

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        try
        {
            if (MaxLength == -1)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Generate Report Prior To Report Redirection!')</script>");
            }
            else
            {
                if (TempMax > 0)
                {
                    TempMax -= 1;
                    tbl.DataSource = DT_Details1[TempMax];
                    tbl.DataBind();
                    lblDuration.Text = "GENERAL LEDGER FROM " + FromDate + " TO " + ToDate;
                    lblReportName.Text = DT_Details1[TempMax].Rows[1]["GL_NAME"].ToString() + " " + DT_Details1[TempMax].Rows[1]["OLD_GL_ID"].ToString();
                    lblPage.Text = "Page : " + Convert.ToString(TempMax + 1) + " of " + Convert.ToString(MaxLength + 1);

                    if (DT_Details1[TempMax].Rows.Count <= 15)
                    {
                        double TotalDebit = 0, TotalCredit = 0;
                        tbl.FooterRow.Cells[0].Text = "Total :";
                        foreach (DataRow dtRow in DT_Details1[TempMax].Rows)
                        {
                            TotalDebit += Convert.ToDouble(dtRow["DEBIT"].ToString());
                            TotalCredit += Convert.ToDouble(dtRow["CREDIT"].ToString());
                        }
                        tbl.FooterRow.Cells[4].Text = TotalDebit.ToString();
                        tbl.FooterRow.Cells[5].Text = TotalCredit.ToString();
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Reached Start Page! You Cannot Go Previous!')</script>");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            if (MaxLength == -1)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Generate Report Prior To Report Redirection!')</script>");
            }
            else
            {
                if (TempMax < MaxLength)
                {
                    TempMax += 1;
                    tbl.DataSource = DT_Details1[TempMax];
                    tbl.DataBind();
                    lblDuration.Text = "GENERAL LEDGER FROM " + FromDate + " TO " + ToDate;
                    lblReportName.Text = DT_Details1[TempMax].Rows[1]["GL_NAME"].ToString() + "  (" + DT_Details1[TempMax].Rows[1]["OLD_GL_ID"].ToString() + ")";
                    lblPage.Text = "Page : " + Convert.ToString(TempMax + 1) + " of " + Convert.ToString(MaxLength + 1);

                    if (DT_Details1[TempMax].Rows.Count <= 15)
                    {
                        double TotalDebit = 0, TotalCredit = 0;
                        tbl.FooterRow.Cells[0].Text = "Total :";
                        foreach (DataRow dtRow in DT_Details1[TempMax].Rows)
                        {
                            TotalDebit += Convert.ToDouble(dtRow["DEBIT"].ToString());
                            TotalCredit += Convert.ToDouble(dtRow["CREDIT"].ToString());
                        }
                        tbl.FooterRow.Cells[4].Text = TotalDebit.ToString();
                        tbl.FooterRow.Cells[5].Text = TotalCredit.ToString();
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Reached END Page! You Cannot Go Next!')</script>");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void tbl_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int srNo = 1;
        tbl.PageIndex = e.NewPageIndex; 
        tbl.DataSource = DT_Details1[TempMax];
        tbl.DataBind();
        srNo = srNo + Convert.ToInt32(tbl.PageIndex);
        lblPage.Text = "Page : 1 of " + Convert.ToString(srNo);
        if (Convert.ToInt32(tbl.PageCount) == (Convert.ToInt32(tbl.PageIndex) + 1) || DT_Details1[TempMax].Rows.Count <= 15 || (DT_Details1[TempMax].Rows.Count % 15 == 0 && Convert.ToInt32(tbl.PageCount) == Convert.ToInt32(tbl.PageIndex)))
        {
            double TotalDebit = 0, TotalCredit = 0;
            tbl.FooterRow.Cells[0].Text = "Total :";
            foreach (DataRow dtRow in DT_Details1[TempMax].Rows)
            {
                TotalDebit += Convert.ToDouble(dtRow["DEBIT"].ToString());
                TotalCredit += Convert.ToDouble(dtRow["CREDIT"].ToString());
            }
            tbl.FooterRow.Cells[4].Text = TotalDebit.ToString();
            tbl.FooterRow.Cells[5].Text = TotalCredit.ToString();
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            MST_Table = null;
            if (MaxLength != -1)
            {
                for (int i = 0; i <= MaxLength; i++)
                {
                    DT_Details[i] = null;
                    DT_Details1[i] = null;
                }
            }
            MaxLength = -1;
            TempMax = 0;
            FromDate = "";
            ToDate = "";
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static DataTableDetails[] GET_PrintSingleDataTable()
    {
        List<DataTableDetails> Detail = new List<DataTableDetails>();
        foreach (DataRow dtRow in DT_Details1[TempMax].Rows)
        {
            DataTableDetails DataObj = new DataTableDetails();

            DataObj.SL_NO = dtRow["SL_NO"].ToString();
            DataObj.GL_ID = dtRow["GL_ID"].ToString();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            DataObj.SL_ID = dtRow["SL_ID"].ToString();
            DataObj.OLD_SL_ID = dtRow["OLD_SL_ID"].ToString();
            DataObj.BK_GL_NAME = dtRow["BK_GL_NAME"].ToString();
            DataObj.BK_GLSL_CODE = dtRow["BK_GLSL_CODE"].ToString();
            DataObj.VCH_DATE = dtRow["VCH_DATE"].ToString();
            DataObj.DOC_TYPE = dtRow["DOC_TYPE"].ToString();
            DataObj.PARTICULARS = dtRow["PARTICULARS"].ToString();
            DataObj.DEBIT = dtRow["DEBIT"].ToString();
            DataObj.CREDIT = dtRow["CREDIT"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }

    public class DataTableDetails //Class for binding data
    {
        public string SL_NO { get; set; }
        public string GL_ID { get; set; }
        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
        public string SL_ID { get; set; }
        public string OLD_SL_ID { get; set; }
        public string BK_GL_NAME { get; set; }
        public string VCH_DATE { get; set; }
        public string DOC_TYPE { get; set; }
        public string PARTICULARS { get; set; }
        public string DEBIT { get; set; }
        public string CREDIT { get; set; }
        public string BK_GLSL_CODE { get; set; }
    }


    [WebMethod]
    public static DBDetails[] GET_PrintMultiDataTable(int TbCount)
    {
        List<DBDetails> Detail = new List<DBDetails>();
        //Label lblPageno = (Label)dv.FindControl("lblPageno");
        //string f = lblPageno.Text;
        foreach (DataRow dtRow in DT_Details[TbCount].Rows)
        {
            DBDetails DataObj = new DBDetails();

            DataObj.SL_NO = dtRow["SL_NO"].ToString();
            DataObj.GL_ID = dtRow["GL_ID"].ToString();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            DataObj.SL_ID = dtRow["SL_ID"].ToString();
            DataObj.OLD_SL_ID = dtRow["OLD_SL_ID"].ToString();
            DataObj.BK_GL_NAME = dtRow["BK_GL_NAME"].ToString();
            DataObj.BK_GLSL_CODE = dtRow["BK_GLSL_CODE"].ToString();
            DataObj.VCH_DATE = dtRow["VCH_DATE"].ToString();
            DataObj.DOC_TYPE = dtRow["DOC_TYPE"].ToString();
            DataObj.PARTICULARS = dtRow["PARTICULARS"].ToString();
            DataObj.DEBIT = dtRow["DEBIT"].ToString();
            DataObj.CREDIT = dtRow["CREDIT"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }

    public class DBDetails //Class for binding data
    {
        public string SL_NO { get; set; }
        public string GL_ID { get; set; }
        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
        public string SL_ID { get; set; }
        public string OLD_SL_ID { get; set; }
        public string BK_GL_NAME { get; set; }
        public string VCH_DATE { get; set; }
        public string DOC_TYPE { get; set; }
        public string PARTICULARS { get; set; }
        public string DEBIT { get; set; }
        public string CREDIT { get; set; }
        public string BK_GLSL_CODE { get; set; }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string[] GET_PrintMultiTableData()
    {   
        int MaxDivider = MaxLength + 1;
        int Count;
        string[] PrintString = new string[MaxDivider * 2];
        double DebitTotal, CreditTotal;
        for (int i = 0; i <= MaxLength; i++)
        {
            string GlName = "", OldGlName = "";
            Count = 0;
            PrintString[i] = "";
            DebitTotal = 0.00;
            CreditTotal = 0.00;
            foreach (DataRow dtRow in DT_Details[i].Rows)
            {
                Count += 1;
                PrintString[i] += "<tr><td>" + dtRow["VCH_DATE"].ToString() + "</td><td>" + dtRow["DOC_TYPE"].ToString() + "</td><td>" + dtRow["PARTICULARS"].ToString() + "</td><td>" + dtRow["BK_GLSL_CODE"].ToString() + "</td><td align='right'>" + dtRow["DEBIT"].ToString() + "</td><td align='right'>" + dtRow["CREDIT"].ToString() + "</td></tr>";
                DebitTotal = DebitTotal + Convert.ToDouble(dtRow["DEBIT"].ToString());
                CreditTotal = CreditTotal + Convert.ToDouble(dtRow["CREDIT"].ToString());
                if (Count == DT_Details[i].Rows.Count - 1)
                {
                    GlName = dtRow["GL_NAME"].ToString();
                    OldGlName = dtRow["OLD_GL_ID"].ToString();
                }
            }
            PrintString[i] += "<tr><td colspan='6'><label>===============================================================================================================</label></td></tr>";
            PrintString[i] += "<tr><td>TOTAL:</td><td></td><td></td><td></td><td align='right'>" + DebitTotal + "</td><td align='right'>" + CreditTotal + "</td></tr>";
            PrintString[i] += "<tr><td colspan='6'><label>===============================================================================================================</label></td></tr>";
            PrintString[i + MaxDivider] = "<label>" + GlName + " (" + OldGlName + ")" + "</label>";
        }
        return PrintString;
    }

} // END