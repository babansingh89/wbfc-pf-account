﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Change_Password.aspx.cs" Inherits="Change_Password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript">
    $(document).ready(function () {
        
    });
    function beforeSave() { 
        $("#frmEcom").validate();
        $("#txtOldPass").rules("add", { required: true, messages: { required: "Please Enter Your Old Password"} });
        $("#txtNewPass").rules("add", { required: true, messages: { required: "Please Enter Your New Password"} });
        $("#txtConfirmPass").rules("add", { required: true, equalTo: "#txtNewPass", messages: { required: "Please Re-Enter Your New Password", equalTo: "Your passwords do not match!" } });
       
        
    }
    
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
<%--<form id="michimichi"></form>
<form id="frmMaster">--%>
<div align="center" >
        
            <table class="headingCaption" style="width:98%;text-align:center"><tr><td>Change Password </td></tr></table>
        <br />
        <table width="98%" cellpadding="5"  class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv"  runat="server"  Width="99%" 
                    CellPadding="4"  AutoGenerateRows="False" 
                    OnModeChanging="dv_ModeChanging"                            
                    DefaultMode="Insert" HorizontalAlign="Center" 
                    GridLines="None"  >
                    <InsertItemTemplate>
                        <table align="center" cellpadding="5" width="100%">
                            <tr>
                            <td class="labelCaption">Old Password&nbsp;&nbsp;<span class="require">*</span> </td>
                            <td class="labelCaption">:</td>
                            <td align="left" ><asp:TextBox ID="txtOldPass" autocomplete="off" TextMode="Password"  ClientIDMode="Static"  runat="server" CssClass="inputbox2"></asp:TextBox>                            
                            </td>
                            </tr>

                            <tr>
                            <td class="labelCaption">New Password&nbsp;&nbsp;<span class="require">*</span> </td>
                            <td class="labelCaption">:</td>
                            <td align="left" ><asp:TextBox ID="txtNewPass" autocomplete="off" TextMode="Password"  ClientIDMode="Static"  runat="server" CssClass="inputbox2"></asp:TextBox>                            
                            </td>
                            </tr>

                            <tr>
                            <td class="labelCaption">Confirm Password&nbsp;&nbsp;<span class="require">*</span> </td>
                            <td class="labelCaption">:</td>
                            <td align="left" ><asp:TextBox ID="txtConfirmPass" autocomplete="off" TextMode="Password"  ClientIDMode="Static"  runat="server" CssClass="inputbox2"></asp:TextBox>                            
                            <%--<asp:CompareValidator id="comparePasswords" 
                                              runat="server"
                                              ControlToCompare="txtNewPass"
                                              ControlToValidate="txtConfirmPass"
                                              ErrorMessage="Your passwords do not match!"
                                              Display="Dynamic" />--%>

                            </td>
                            </tr>
                        </table>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        
                    </EditItemTemplate>
                    <FooterTemplate>
                        <table align="center" cellpadding="5" width="100%">
                        <tr><td colspan="4" class="labelCaption"><hr style="border:solid 1px lightblue" /></td></tr>
                        <tr>
                            <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                            <td>&nbsp;</td>
                            <td align="left" >
                            <div style="float:left;margin-left:200px;">
                                <asp:Button ID="cmdSave" runat="server" Text="Change Password" CommandName="Add" autocomplete="off"
                                Width="200px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()' 
                                OnClick="cmdSave_Click" /> 
                                <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()'/>
                            </div>
                            </td>
                        </tr>
                                    
                        </table>
                    </FooterTemplate>
                    </asp:FormView>
                    
                </td>
            </tr>
           
        </table>
    </div>
<%--</form>--%>
</asp:Content>

