﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Web.Script.Services;


public partial class MenuAccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string GetModule()
    {
        MenuAccess_BLL menuModule = new MenuAccess_BLL();

        List<MstModule> module = menuModule.GetAllModule();
        return module.ToJSON();
    }

    [WebMethod]
    public static string GetMenu(string Module)
    {
        MenuAccess_BLL MenuBLL = new MenuAccess_BLL();

        List<Menu> menus = MenuBLL.GetAllMenu(Module);
        return menus.ToJSON();
    }

    [WebMethod]
    public static string UserCompleteData(string UserName)
    {
        MenuAccess_BLL MenuBLL = new MenuAccess_BLL();
        List<Mst_User> users = MenuBLL.userCompleteData(UserName);

        return users.ToJSON();
    }

    [WebMethod]
    public static string GetCheckedMenu(string UserID, string moduleCode)
    {
        MenuAccess_BLL MenuBLL = new MenuAccess_BLL();
        List<UserMenu> UserMenus = MenuBLL.getCheckedMenu(UserID, moduleCode);

        return UserMenus.ToJSON();
    }

    [WebMethod]
    public static string InsertUserMenu(string UserID, string Menus, string ModuleCode)
    {
        CheckStatus cs = new CheckStatus();
        try 
        {
            MenuAccess_BLL MenuBLL = new MenuAccess_BLL();
            string userStatus = MenuBLL.insertUserMenu(UserID, Menus, ModuleCode);

            if (userStatus != "")
            {
                cs.Message = "Successfully done";
                cs.Status = "Y";
            }
            else if (userStatus == "")
            {
                cs.Message = "Error--------------";
                cs.Status = "N";
            }
        }
        catch (Exception ex)
        {
            cs.Message = ex.Message;
            cs.Status = "N";
        }
        return cs.ToJSON();        
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

}