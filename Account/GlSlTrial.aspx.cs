﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Services;
using System.Web.UI.DataVisualization.Charting;
using System.IO;

public partial class GlSlTrial : System.Web.UI.Page
{
    protected string GL_Name = "";
    protected string SL_Name = "";
    protected string SL_NameMonth = "";
    protected string From_Date = "";
    protected string To_Date = "";
    protected string Sectors = "";

    protected string YearMonth = "";
    protected string VchType = "";
    protected string VchDate = "";
    protected string VchNo = "";

    protected string str = "";
    static string hdnGLID = "";
    //protected decimal totalDebit = 0;
    //protected decimal totalCredit = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            TextBox txtGLName = (TextBox)dv.FindControl("txtGLName");
            txtGLName.Attributes.Add("readonly", "readonly");
            if (!IsPostBack)
            {
                //PopulateGrid();
                PopulateGrid1();
            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected DataTable drpload()
    {
       // DataTable dt = DBHandler.GetResult("Get_SectorForGlSLTrial");
        DataTable dt = DBHandler.GetResult("Get_UserWiseSector", Session[SiteConstants.SSN_INT_USER_ID], "P");
        return dt;
    }

    protected void cmdBack_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdBack = (Button)sender;
            if (cmdBack.CommandName == "Add1")
            {
                pan_tblSL.Visible = false;
                pan_tbl.Visible = true;
                panVoucherMon.Visible = false;
                ShowGL();
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdSLBack_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSLBack = (Button)sender;
            if (cmdSLBack.CommandName == "Add2")
            {
                
                pan_tbl.Visible = false;
                pan_tblSL.Visible = true;
                panVoucherMon.Visible = false;
                panVchMonthDtl.Visible = false;
                panVchFinal.Visible = false;
                string gl_id = Session["GL_id"].ToString();
                string gl_name = Session["GL_name"].ToString();
                string gl_code = Session["GL_code"].ToString();
                ShowSL(gl_id, gl_name, gl_code);
                //Session["GL_id"] = null;
                //Session["GL_name"] = null;
            }
            else if (cmdSLBack.CommandName == "Update")
            {
                pan_tblSL.Visible = false;
                pan_tbl.Visible = true;
                panVoucherMon.Visible = false;
                panVchMonthDtl.Visible = false;
                panVchFinal.Visible = false;
                ShowGL();
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmbVchMonBack_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSLBack = (Button)sender;
            if (cmdSLBack.CommandName == "Add3")
            {

                pan_tbl.Visible = false;
                pan_tblSL.Visible = false;
                panVoucherMon.Visible = true;
                panVchMonthDtl.Visible = false;
                panVchFinal.Visible = false;
                string gl_id = Session["GL_id"].ToString();
                string sl_id = Session["SL_id"].ToString();
                string old_gl_id = Session["GL_code"].ToString();
                string old_sl_id = Session["SL_code"].ToString();
                string sl_name = Session["SL_Name"].ToString();
                PopulateVoucherMonthWise(gl_id, sl_id, old_gl_id, old_sl_id, sl_name);
                //Session["GL_id"] = null;
                //Session["GL_name"] = null;
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmbVchReport_Click(object sender, EventArgs e)
    {
        try
        {
            Button btnVchReport = (Button)sender;
            if (btnVchReport.CommandName == "Add4")
            {

                pan_tbl.Visible = false;
                pan_tblSL.Visible = false;
                panVoucherMon.Visible = false;
                panVchMonthDtl.Visible = true;
                panVchFinal.Visible = false;
                string gl_id = Session["GL_id"].ToString();
                string sl_id = Session["SL_id"].ToString();
                string month = Session["Month"].ToString();
                string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(month.Substring(4,2)));
                PopulateVoucherMonth(gl_id, sl_id, strMonthName);
                //Session["GL_id"] = null;
                //Session["GL_name"] = null;
            }

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    

    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSearch = (Button)sender;
            if (cmdSearch.CommandName == "Add")
            {
                ShowGL();
            }
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void ShowGL()
    {
        try
        {
            string strSector = "";
            TextBox txtFrmDate = (TextBox)dv.FindControl("txtFrmDate");
            TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
            int SecLen = 0;
            CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");
            int cnt = 0;
            for (int j = 0; j < ddlSector.Items.Count; j++)
            {
                if (ddlSector.Items[j].Selected == true)
                {
                    cnt++;
                    SecLen++;
                }
            }
            if (cnt == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please check at least one Sector for search.')</script>");
                return;
            }
            cnt = 0;
            for (int k = 0; k < ddlSector.Items.Count; k++)
            {
                if (ddlSector.Items[k].Selected == true)
                {
                    strSector = strSector + ddlSector.Items[k].Value + ",";
                }
            }
               
            strSector = strSector.Substring(0, strSector.Length - 1);
            txtOrgName.Text = "";
            txtOrgAddress.Text = "";
            DataTable dtSec = DBHandler.GetResult("GET_SecNameWithAddress_Pf", strSector, SecLen);
            if (dtSec.Rows.Count > 0)
            {
                foreach (DataRow dtRow in dtSec.Rows)
                {
                    if (txtOrgName.Text == "")
                    {
                        txtOrgName.Text = dtRow["OrgName"].ToString();  
                        txtOrgAddress.Text = dtRow["Sector"].ToString();
                    }
                    else {
                        txtOrgAddress.Text= txtOrgAddress.Text + " , " + dtRow["Sector"].ToString();
                    }
                }

               
            }
                PopulateGrid(txtFrmDate.Text, txtToDate.Text, strSector);

            
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGrid(string frmDT, string toDT, string sectors)
    {
        try
        {
            DateTime dtFromDate = DateTime.ParseExact(frmDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtToDate = DateTime.ParseExact(toDT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            //HiddenField hdnGLID = (HiddenField)dv.FindControl("hdnGLID");
            TextBox txtGLName = (TextBox)dv.FindControl("txtGLName");
            string zero = "";
            CheckBox chkZero = (CheckBox)dv.FindControl("chkZero");
            if (chkZero.Checked == true)
            {
                zero = "Y";
            }
            else
            {
                zero = "N";
            }

            if (hdnGLID == "") //if (hdnGLID.Value == "")
            {
                pan_tbl.Visible = true;
                pan_tblSL.Visible = false;
                panVoucherMon.Visible = false;
                panVchMonthDtl.Visible = false;
                DataTable dt = DBHandler.GetResult("Acc_Gl_trial_Pf", dtFromDate.ToShortDateString(), dtToDate.ToShortDateString(), zero, sectors, "P");
                if(dt.Rows.Count > 0 )
                {
                    tbl.DataSource = dt;
                    tbl.DataBind();
                    // -----------------------------  Add Footer ---------------------------------------------------------------------------
                    decimal OP_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("OP_DR"));
                    decimal OP_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("OP_CR"));
                    decimal TR_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("TR_DR"));
                    decimal TR_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("TR_CR"));
                    decimal CL_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("CL_DR"));
                    decimal CL_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("CL_CR"));

                    tbl.FooterRow.Cells[1].Text = "Total";
                    tbl.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                    tbl.FooterRow.Cells[1].Font.Bold = true;
                    tbl.FooterRow.Cells[1].Font.Size = 10;

                    tbl.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                    tbl.FooterRow.Cells[2].Text = OP_DR.ToString("N2");
                    tbl.FooterRow.Cells[2].Font.Bold = true;
                    tbl.FooterRow.Cells[2].Font.Size = 10;

                    tbl.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                    tbl.FooterRow.Cells[3].Text = OP_CR.ToString("N2");
                    tbl.FooterRow.Cells[3].Font.Bold = true;
                    tbl.FooterRow.Cells[3].Font.Size = 10;

                    tbl.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                    tbl.FooterRow.Cells[4].Text = TR_DR.ToString("N2");
                    tbl.FooterRow.Cells[4].Font.Bold = true;
                    tbl.FooterRow.Cells[4].Font.Size = 10;

                    tbl.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                    tbl.FooterRow.Cells[5].Text = TR_CR.ToString("N2");
                    tbl.FooterRow.Cells[5].Font.Bold = true;
                    tbl.FooterRow.Cells[5].Font.Size = 10;

                    tbl.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                    tbl.FooterRow.Cells[6].Text = CL_DR.ToString("N2");
                    tbl.FooterRow.Cells[6].Font.Bold = true;
                    tbl.FooterRow.Cells[6].Font.Size = 10;

                    tbl.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                    tbl.FooterRow.Cells[7].Text = CL_CR.ToString("N2");
                    tbl.FooterRow.Cells[7].Font.Bold = true;
                    tbl.FooterRow.Cells[7].Font.Size = 10;
                    //------------------------------------------------------------------------------------------------------------------------------------------------
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('No Record found.')</script>");
                    return;
                }            

            }
            else
            {
                pan_tbl.Visible = false;
                pan_tblSL.Visible = true;
                panVoucherMon.Visible = false;
                panVchMonthDtl.Visible = false;
                cmdBack.Visible = false;
                panVchFinal.Visible = false;
                //DataTable dt = DBHandler.GetResult("Acc_Sl_trial", dtFromDate.ToShortDateString(), dtToDate.ToShortDateString(), hdnGLID.Value.Trim(), sectors);
                DataTable dt = DBHandler.GetResult("Acc_Sl_trial_Pf", dtFromDate.ToShortDateString(), dtToDate.ToShortDateString(), hdnGLID, sectors, "P");
                tblSL.DataSource = dt;
                tblSL.DataBind();

                //GL_Name = hdnGLID.Value.Trim() + " - " + txtGLName.Text.Trim();
                GL_Name = hdnGLID + " - " + txtGLName.Text.Trim();

                // ----------------------------------------------------  Add Footer ---------------------------------------------------------------------------

                decimal OP_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("OP_DR"));
                decimal OP_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("OP_CR"));
                decimal TR_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("TR_DR"));
                decimal TR_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("TR_CR"));
                decimal CL_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("CL_DR"));
                decimal CL_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("CL_CR"));

                tblSL.FooterRow.Cells[1].Text = "Total";
                tblSL.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[1].Font.Bold = true;
                tblSL.FooterRow.Cells[1].Font.Size = 10;

                tblSL.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[2].Text = OP_DR.ToString("N2");
                tblSL.FooterRow.Cells[2].Font.Bold = true;
                tblSL.FooterRow.Cells[2].Font.Size = 10;

                tblSL.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[3].Text = OP_CR.ToString("N2");
                tblSL.FooterRow.Cells[3].Font.Bold = true;
                tblSL.FooterRow.Cells[3].Font.Size = 10;

                tblSL.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[4].Text = TR_DR.ToString("N2");
                tblSL.FooterRow.Cells[4].Font.Bold = true;
                tblSL.FooterRow.Cells[4].Font.Size = 10;

                tblSL.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[5].Text = TR_CR.ToString("N2");
                tblSL.FooterRow.Cells[5].Font.Bold = true;
                tblSL.FooterRow.Cells[5].Font.Size = 10;

                tblSL.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[6].Text = CL_DR.ToString("N2");
                tblSL.FooterRow.Cells[6].Font.Bold = true;
                tblSL.FooterRow.Cells[6].Font.Size = 10;

                tblSL.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[7].Text = CL_CR.ToString("N2");
                tblSL.FooterRow.Cells[7].Font.Bold = true;
                tblSL.FooterRow.Cells[7].Font.Size = 10;
        //------------------------------------------------------------------------------------------------------------------------------------
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void tbl_OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(tbl, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";

                string group = tbl.DataKeys[e.Row.RowIndex].Values[1].ToString();

                foreach (TableCell cell in e.Row.Cells)
                {
                    if (group == "M")
                    {
                        e.Row.BackColor = System.Drawing.Color.Cyan;
                    }
                    
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }

    protected void tbl_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int index = tbl.SelectedRow.RowIndex;
           // string gl_id = tbl.SelectedRow.Cells[0].Text;
            string gl_id = tbl.DataKeys[tbl.SelectedIndex].Values[0].ToString();

            string gl_code = tbl.SelectedRow.Cells[0].Text;
            string gl_name = tbl.SelectedRow.Cells[1].Text;

            Session["GL_id"] = gl_id;
            Session["GL_code"] = gl_code;
            Session["GL_name"] = gl_name;
            ShowSL(gl_id, gl_name, gl_code);
            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
        
    }

    private void ShowSL( string GLID, string GLNAME, string GLCODE)
    {
        try
        {
            TextBox txtFrmDate = (TextBox)dv.FindControl("txtFrmDate");
            TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
            CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");
            string strSector = ""; 
            if (txtFrmDate.Text == "" && txtToDate.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please select From Date and To Date.')</script>");
                return;
            }
            int cnt = 0;
            for (int j = 0; j < ddlSector.Items.Count; j++)
            {
                if (ddlSector.Items[j].Selected == true)
                {
                    cnt++;
                }
            }
            if (cnt == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please check at least one Sector for search.')</script>");
                return;
            }

            
            for (int k = 0; k < ddlSector.Items.Count; k++)
            {
                if (ddlSector.Items[k].Selected == true)
                {
                    strSector = strSector + ddlSector.Items[k].Value + ",";
                }
            }

            strSector = strSector.Substring(0, strSector.Length - 1);
        
            DateTime dtFromDate = DateTime.ParseExact(txtFrmDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtToDate = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DataTable dt = DBHandler.GetResult("Acc_Sl_trial_Pf", dtFromDate.ToShortDateString(), dtToDate.ToShortDateString(), GLID, strSector, "P");
            cmdBack.Visible = true;
            pan_tbl.Visible = false;
            pan_tblSL.Visible = true;
            panVoucherMon.Visible = false;
            panVchMonthDtl.Visible = false;
            panVchFinal.Visible = false;
            tblSL.DataSource = dt;
            tblSL.DataBind();
            
            if (tblSL.Rows.Count > 0)
            {
                // -----------------------------  Add Footer ---------------------------------------------------------
                decimal OP_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("OP_DR"));
                decimal OP_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("OP_CR"));
                decimal TR_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("TR_DR"));
                decimal TR_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("TR_CR"));
                decimal CL_DR = dt.AsEnumerable().Sum(row => row.Field<decimal>("CL_DR"));
                decimal CL_CR = dt.AsEnumerable().Sum(row => row.Field<decimal>("CL_CR"));

                tblSL.FooterRow.Cells[1].Text = "Total";
                tblSL.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[1].Font.Bold = true;
                tblSL.FooterRow.Cells[1].Font.Size = 10;

                tblSL.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[2].Text = OP_DR.ToString("N2");
                tblSL.FooterRow.Cells[2].Font.Bold = true;
                tblSL.FooterRow.Cells[2].Font.Size = 10;

                tblSL.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[3].Text = OP_CR.ToString("N2");
                tblSL.FooterRow.Cells[3].Font.Bold = true;
                tblSL.FooterRow.Cells[3].Font.Size = 10;

                tblSL.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[4].Text = TR_DR.ToString("N2");
                tblSL.FooterRow.Cells[4].Font.Bold = true;
                tblSL.FooterRow.Cells[4].Font.Size = 10;

                tblSL.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[5].Text = TR_CR.ToString("N2");
                tblSL.FooterRow.Cells[5].Font.Bold = true;
                tblSL.FooterRow.Cells[5].Font.Size = 10;

                tblSL.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[6].Text = CL_DR.ToString("N2");
                tblSL.FooterRow.Cells[6].Font.Bold = true;
                tblSL.FooterRow.Cells[6].Font.Size = 10;

                tblSL.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                tblSL.FooterRow.Cells[7].Text = CL_CR.ToString("N2");
                tblSL.FooterRow.Cells[7].Font.Bold = true;
                tblSL.FooterRow.Cells[7].Font.Size = 10;
                //-----------------------------------------------------------------------------------------------------------------
            }
            
            GL_Name = GLCODE + " - " + GLNAME;

            if (tblSL.Rows.Count == 0)
            {
                PopulateVoucherMonthWise(GLID, GLID, Session["GL_code"].ToString(), Session["GL_code"].ToString(), GLNAME);    /// For GL (GL_MAster--> N & S) SL ID same as GL ID... It show the Graph PAnel
                cmdSl.Text = "GL";
                cmdSl.CommandName = "Update";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void tblSL_OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(tblSL, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }

    protected void tblSL_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string gl_id = tblSL.DataKeys[tblSL.SelectedIndex].Values[0].ToString();
            string sl_id = tblSL.DataKeys[tblSL.SelectedIndex].Values[1].ToString();
            string old_gl_id = tblSL.DataKeys[tblSL.SelectedIndex].Values[2].ToString();
            string ols_sl_id = tblSL.DataKeys[tblSL.SelectedIndex].Values[3].ToString();

           // string sl_id = tblSL.SelectedRow.Cells[0].Text;
            string sl_name = tblSL.SelectedRow.Cells[1].Text;

            Session["GL_id"] = gl_id;
            Session["SL_id"] = sl_id;
            Session["GL_code"] = old_gl_id;
            Session["SL_code"] = ols_sl_id;
            Session["SL_Name"] = sl_name;
          //  Session["GL_name"] = "";
            PopulateVoucherMonthWise(gl_id, sl_id, old_gl_id, ols_sl_id, sl_name);           
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }

    private void PopulateVoucherMonthWise(string GL_ID, string SL_ID, string Old_Gl_id, string Old_Sl_id, string SLNAME)
    {
        try
        {
            
            TextBox txtFrmDate = (TextBox)dv.FindControl("txtFrmDate");
            TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
            CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");
            string strSector = "";
            int sectorTextCnt = 0;
            if (txtFrmDate.Text != "" && txtToDate.Text != "")
            {          
                int cnt = 0;
                for (int j = 0; j < ddlSector.Items.Count; j++)
                {
                    if (ddlSector.Items[j].Selected == true)
                    {
                        cnt++;
                    }
                }
                if (cnt == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please check at least one Sector for search.')</script>");
                    return;
                }

                for (int k = 0; k < ddlSector.Items.Count; k++)
                {
                    if (ddlSector.Items[k].Selected == true)
                    {
                        strSector = strSector + ddlSector.Items[k].Value + ",";
                        Sectors = Sectors + ddlSector.Items[k].Text + ",  ";
                        sectorTextCnt++;
                    }
                }

                strSector = strSector.Substring(0, strSector.Length - 1);

                DateTime dtFromDate = DateTime.ParseExact(txtFrmDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DateTime dtToDate = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DataTable dt = DBHandler.GetResult("Acc_Month_Wise_Dtls_Pf",
                                                    txtFrmDate.Text.Equals("") ? DBNull.Value : (object)dtFromDate.ToShortDateString(),
                                                    txtToDate.Text.Equals("") ? DBNull.Value : (object)dtToDate.ToShortDateString(),
                                                    GL_ID.Equals("") ? DBNull.Value : (object)GL_ID,
                                                    Old_Gl_id.Equals("") ? DBNull.Value : (object)Old_Gl_id,
                                                    SL_ID.Equals("") ? DBNull.Value : (object)SL_ID,
                                                    Old_Sl_id.Equals("") ? DBNull.Value : (object)Old_Sl_id,
                                                    strSector.Equals("") ? DBNull.Value : (object)strSector);
                cmdBack.Visible = true;
                pan_tbl.Visible = false;
                pan_tblSL.Visible = false;
                panVoucherMon.Visible = true;
                panVchMonthDtl.Visible = false;
                panVchFinal.Visible = false;
                tblVoucherMon.DataSource = dt;
                tblVoucherMon.DataBind();
            // -----------------------------  For Chart------------------------------------------------------
                chartGLSL.DataSource = dt;
                chartGLSL.DataBind();
                chartGLSL.Series["Series1"]["PixelPointWidth"] = "55";
            //--------------------------------------------------------------------------------------------------   

           // -----------------------------  Add Footer ---------------------------------------------------------
                decimal totalDebit = dt.AsEnumerable().Sum(row => row.Field<decimal>("TR_DR"));
                decimal totalCredit = dt.AsEnumerable().Sum(row => row.Field<decimal>("TR_CR"));

                tblVoucherMon.FooterRow.Cells[1].Text = "Total";
                tblVoucherMon.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                tblVoucherMon.FooterRow.Cells[1].Font.Bold = true;
                tblVoucherMon.FooterRow.Cells[1].Font.Size = 10;

                tblVoucherMon.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                tblVoucherMon.FooterRow.Cells[2].Text = totalDebit.ToString("N2");
                tblVoucherMon.FooterRow.Cells[2].Font.Bold = true;
                tblVoucherMon.FooterRow.Cells[2].Font.Size = 10;

                tblVoucherMon.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                tblVoucherMon.FooterRow.Cells[3].Text = totalCredit.ToString("N2");
                tblVoucherMon.FooterRow.Cells[3].Font.Bold = true;
                tblVoucherMon.FooterRow.Cells[3].Font.Size = 10;
           //--------------------------------------------------------------------------------------------------------------   
          // -----------------------------  For Upper Heading --------------------------------------------------------------
                if (sectorTextCnt > 4)
                {
                    Sectors = "Consolidated";
                }
                else
                {
                    Sectors = Sectors.Substring(0, Sectors.Length - 3); /// 3 for 1 samicolon and 2 space
                }

                SL_Name = Old_Sl_id + " - " + SLNAME;
                From_Date = txtFrmDate.Text;
                To_Date = txtToDate.Text;
         //------------------------------------------------------------------------------------------------------------------------  
                cmdSl.Text = "SL";
                cmdSl.CommandName = "Add2";
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please select From Date and To Date.')</script>");
                return;
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tblVoucherMon_OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(tblVoucherMon, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
            }         
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }

    protected void tblVoucherMon_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string gl_id = tblVoucherMon.DataKeys[tblVoucherMon.SelectedIndex].Values[0].ToString();
            string sl_id = tblVoucherMon.DataKeys[tblVoucherMon.SelectedIndex].Values[1].ToString();
            string old_gl_id = tblVoucherMon.DataKeys[tblVoucherMon.SelectedIndex].Values[2].ToString();
            string old_sl_id = tblVoucherMon.DataKeys[tblVoucherMon.SelectedIndex].Values[3].ToString();
            string month = tblVoucherMon.SelectedRow.Cells[0].Text;

            Session["GL_id"] = gl_id;
            Session["SL_id"] = sl_id;
            Session["GL_code"] = old_gl_id;
            Session["SL_code"] = old_sl_id;
            //Session["SL_Name"] = sl_name;

            Session["Month"] = month;
           // Session["GL_name"] = "";
            PopulateVoucherMonth(gl_id, sl_id, month);  

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }

    private void PopulateVoucherMonth(string gl_id, string sl_id, string month)
    {
        try
        {
            string mnt = ""; string finalLstDate = "";
            TextBox txtFrmDate = (TextBox)dv.FindControl("txtFrmDate");
            TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
            CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");
            string strSector = "";
            if (txtFrmDate.Text == "" && txtToDate.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please select From Date and To Date.')</script>");
                return;
            }
            int cnt = 0;
            for (int j = 0; j < ddlSector.Items.Count; j++)
            {
                if (ddlSector.Items[j].Selected == true)
                {
                    cnt++;
                }
            }
            if (cnt == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please check at least one Sector for search.')</script>");
                return;
            }


            for (int k = 0; k < ddlSector.Items.Count; k++)
            {
                if (ddlSector.Items[k].Selected == true)
                {
                    strSector = strSector + ddlSector.Items[k].Value + ",";
                }
            }

            strSector = strSector.Substring(0, strSector.Length - 1);
          //  'March 1 2011'
            //month = "March";
            //month = month + " 12 " + "2016";

            DateTime dtFromDate = DateTime.ParseExact(txtFrmDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtToDate = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
              
            if(month=="JANUARY"){mnt="01";}if(month=="FEBRUARY"){mnt="02";}if(month=="MARCH"){mnt="03";}if(month=="APRIL"){mnt="04";}if(month=="MAY"){mnt="05";}
            if(month=="JUNE"){mnt="06";}if(month=="JULY"){mnt="07";}if(month=="AUGUST"){mnt="08";}if(month=="SEPTEMBER"){mnt="09";}if(month=="OCTOBER"){mnt="10";}
            if(month=="NOVEMBER"){mnt="11";}if(month=="DECEMBER"){mnt="12";}

            string[] t = txtFrmDate.Text.Split('/');
            string selectedYear = t[2];

            string frmDate = "01/" + mnt + "/" + selectedYear;
            DateTime dt1 = DateTime.ParseExact(frmDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime last_date = new DateTime(dt1.Year, dt1.Month, DateTime.DaysInMonth(dt1.Year, dt1.Month));

            string[] g = frmDate.Split('/');
            string g1 = g[0]; string g2 = g[1]; string g3 = g[2];
            string frst = g3 + "-" + g2 + "-" + g1;

            if (last_date < dtToDate) {
                finalLstDate = last_date.ToString("yyyy-MM-dd");
            }
            else
            {
                finalLstDate = dtToDate.ToString("yyyy-MM-dd");
            }


            DataTable dt = DBHandler.GetResult("Get_GLSLTrialMonthWise_Pf", gl_id, sl_id, mnt, frst, finalLstDate, strSector, "P");
            pan_tbl.Visible = false;
            pan_tblSL.Visible = false;
            panVoucherMon.Visible = false;
            panVchFinal.Visible = false;
            panVchMonthDtl.Visible = true;
            tblVchMonthDtl.DataSource = dt;
            tblVchMonthDtl.DataBind();

            if (Session["SL_code"].ToString() != "")
            {
                ///SL_NameMonth = Session["SL_code"].ToString() + '-' + Session["SL_Name"].ToString();
                //SL_NameMonth = Session["GL_code"].ToString() + '-' + Session["GL_name"].ToString();
            }
            else
            {
                //SL_NameMonth = Session["SL_code"].ToString() + '-' + Session["SL_Name"].ToString();
                ///SL_NameMonth = Session["GL_code"].ToString() + '-' + Session["GL_name"].ToString();
            }

            
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tblVchMonthDtl_OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(tblVchMonthDtl, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";

                
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }

    protected void tblVchMonthDtl_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
                           
            string gl_id = tblVchMonthDtl.DataKeys[tblVchMonthDtl.SelectedIndex].Values[0].ToString();
            string sl_id = tblVchMonthDtl.DataKeys[tblVchMonthDtl.SelectedIndex].Values[1].ToString();
            string vch_no = tblVchMonthDtl.DataKeys[tblVchMonthDtl.SelectedIndex].Values[2].ToString(); //tblVchMonthDtl.DataKeys[tblVchMonthDtl.SelectedIndex].Values[2].ToString();
            string vch_type = tblVchMonthDtl.DataKeys[tblVchMonthDtl.SelectedIndex].Values[3].ToString();
            string sector_id = tblVchMonthDtl.DataKeys[tblVchMonthDtl.SelectedIndex].Values[4].ToString();
            string month = tblVchMonthDtl.SelectedRow.Cells[0].Text;

            Session["GL_id"] = gl_id;
            Session["SL_id"] = sl_id;
            Session["Month"] = month;
            // Session["GL_name"] = "";
            PopulateVoucherReport(vch_no, vch_type, month, sector_id);

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }
       
    private void PopulateVoucherReport(string vch_no, string vch_type, string month, string sector_id)
    {
        try
        {
            TextBox txtFrmDate = (TextBox)dv.FindControl("txtFrmDate");
            TextBox txtToDate = (TextBox)dv.FindControl("txtToDate");
            CheckBoxList ddlSector = (CheckBoxList)dv.FindControl("ddlSector");
            string strSector = "";
            if (txtFrmDate.Text == "" && txtToDate.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please select From Date and To Date.')</script>");
                return;
            }
            int cnt = 0;
            for (int j = 0; j < ddlSector.Items.Count; j++)
            {
                if (ddlSector.Items[j].Selected == true)
                {
                    cnt++;
                }
            }
            if (cnt == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please check at least one Sector for search.')</script>");
                return;
            }


            for (int k = 0; k < ddlSector.Items.Count; k++)
            {
                if (ddlSector.Items[k].Selected == true)
                {
                    strSector = strSector + ddlSector.Items[k].Value + ",";
                }
            }

            strSector = strSector.Substring(0, strSector.Length - 1);

            DateTime dtFromDate = DateTime.ParseExact(txtFrmDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime dtToDate = DateTime.ParseExact(txtToDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DataSet ds = DBHandler.GetResults("Get_GLSLTrialVoucherReport_Pf", Convert.ToInt32(vch_no), Convert.ToInt32(vch_type), month, strSector,"","","", "P");
            
            // skm check Proc
            if (ds.Tables[0].Rows.Count > 0)
            {
                YearMonth = ds.Tables[0].Rows[0]["YEAR_MONTH"].ToString();
                VchType = ds.Tables[0].Rows[0]["VCH_TYPE"].ToString();
                VchDate = ds.Tables[0].Rows[0]["VCH_DATE"].ToString();
                VchNo = ds.Tables[0].Rows[0]["VoucherNo"].ToString();
                string DRCR = "";
                decimal DrAmt = 0;
                decimal CrAmt = 0;
                decimal SumDrAmt = 0;
                decimal SumCrAmt = 0;
                //str +="<table id='tabDetail' cellpadding='3' width='100%' style='margin-left: 65px; border-collapse: collapse;' align='center'>";
                //    str += "<tr>";
                //        str += "<th class='TableHeader' style='text-align: center; width: 25%; border: 1px solid white;'>YearMonth :-" + ds.Tables[0].Rows[0]["YEAR_MONTH"].ToString() + " </th>";
                //        str += "<th class='TableHeader' style='text-align: center; width: 25%; border: 1px solid white;'>Voucher Type :-" + ds.Tables[0].Rows[0]["VCH_TYPE"].ToString() + " </th>";
                //        str += "<th class='TableHeader' style='text-align: center; width: 25%; border: 1px solid white;'>Voucher Date :-" + ds.Tables[0].Rows[0]["VCH_DATE"].ToString() + " </th>";
                //        str += "<th class='TableHeader' style='text-align: center; width: 25%; border: 1px solid white;'>Voucher No :-" + ds.Tables[0].Rows[0]["VoucherNo"].ToString() + " </th>";
                //    str += "</tr>";
                //str += "</table>";
                str += "<table id='tabDetail' cellpadding='3' width='90%' style='margin-left: 5%; border-collapse: collapse;' align='center'>";
                    str += "<tr>";
                        str += "<th class='TableHeader' style='text-align: center; width: 10%; border: 1px solid navy;'>Code</th>";
                        str += "<th class='TableHeader' style='text-align: center; width: 45%; border: 1px solid navy;'>Particulars</th>";
                        str += "<th class='TableHeader' style='text-align: center; width: 15%; border: 1px solid navy;'>Dr/Cr</th>";
                        str += "<th class='TableHeader' style='text-align: center; width: 15%; border: 1px solid navy;'>Debit</th>";
                        str += "<th class='TableHeader' style='text-align: center; width: 15%; border: 1px solid navy;'>Credit</th>";
                    str += "</tr>";
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        for (int k = 0; k <= ds.Tables[1].Rows.Count - 1; k++)
                        {
                            DRCR = ds.Tables[1].Rows[k]["DR_CR"].ToString();
                            DrAmt = (DRCR == "Debit") ? Convert.ToDecimal(ds.Tables[1].Rows[k]["AMOUNT"].ToString()) : Convert.ToDecimal("0.00");
                            SumDrAmt += DrAmt;
                            CrAmt = (DRCR == "Credit") ? Convert.ToDecimal(ds.Tables[1].Rows[k]["AMOUNT"].ToString()) : Convert.ToDecimal("0.00");
                            SumCrAmt += CrAmt;
                            str += "<tr>";
                                str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["OLD_SL_ID"].ToString() + "</td>";
                                str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["GL_NAME"].ToString() + "</td>";
                                str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy;'>" + ds.Tables[1].Rows[k]["DR_CR"].ToString() + "</td>";
                                str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy;'> " + Math.Round(DrAmt,2) + "</td>";
                                str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy;'>" + Math.Round(CrAmt,2) + "</td>";                
                            str += "</tr>";                            
                        }
                            str += "<tr>";
                                str += "<td colspan='3' class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>Total :- </td>";                           
                                str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>" + Math.Round(SumDrAmt, 2) + " </td>";
                                str += "<td class='labelCaption' style='text-align: right; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>" + Math.Round(SumCrAmt, 2) + "</td>";
                            str += "</tr>";
                            str += "<tr>";
                                str += "<td class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'>Narration : </td>";
                                str += "<td colspan='4' class='labelCaption' style='text-align: left; width: 10%; border: 1px solid Navy; background-color : #99ccff; font-weight: bold;'> " + ds.Tables[0].Rows[0]["NARRATION"].ToString() + "</td>";
                            str += "</tr>";
                    }
                str += "</table>";
                
            }
            pan_tbl.Visible = false;
            pan_tblSL.Visible = false;
            panVoucherMon.Visible = false;
            panVchMonthDtl.Visible = false;
            panVchFinal.Visible = true;
            //tblVchFinal.DataSource = dt;
            //tblVchFinal.DataBind();

            //decimal totalDebit = dt.AsEnumerable().Sum(row => row.Field<decimal>("AMOUNT"));
            //decimal totalCredit = dt.AsEnumerable().Sum(row => row.Field<decimal>("AMOUNT"));

            //tblVchFinal.FooterRow.Cells[5].Text = "Total";
            //tblVchFinal.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Right;
            //tblVchFinal.FooterRow.Cells[5].Font.Bold = true;
            //tblVchFinal.FooterRow.Cells[5].Font.Size = 10;

            //tblVchFinal.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Right;
            //tblVchFinal.FooterRow.Cells[6].Text = totalDebit.ToString("N2");
            //tblVchFinal.FooterRow.Cells[6].Font.Bold = true;
            //tblVchFinal.FooterRow.Cells[6].Font.Size = 10;

            //tblVchFinal.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Right;
            //tblVchFinal.FooterRow.Cells[7].Text = totalCredit.ToString("N2");
            //tblVchFinal.FooterRow.Cells[7].Font.Bold = true;
            //tblVchFinal.FooterRow.Cells[7].Font.Size = 10;
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tblVchFinal_OnRowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(tblVchFinal, "Select$" + e.Row.RowIndex);
                //e.Row.Attributes["style"] = "cursor:pointer";
                string DR_CR = e.Row.Cells[5].Text;
                foreach (TableCell cell in e.Row.Cells)
                {
                    if (DR_CR == "D")
                    {
                        e.Row.Cells[7].Text = "0";
                    }

                    if (DR_CR == "C")
                    {
                        e.Row.Cells[6].Text = "0";
                    }
                }               
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }

    }

  
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Session["GL_id"] = null;
            Session["GL_name"] = null;
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static GL[] GET_GLNameByGLID(string StrGLCode)
    {
        //int UserID;
        List<GL> Detail = new List<GL>();
        //UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        DataTable dtGetData = DBHandler.GetResult("Get_GL_NameByID_Pf", StrGLCode, "P");
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GL DataObj = new GL();
            DataObj.GL_ID = dtRow["GL_ID"].ToString();
            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GL //Class for binding data
    {
        public string GL_ID { get; set; }
        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
    }

    [WebMethod]
    public static GLAll[] Search_GlCode()
    {
        List<GLAll> Detail = new List<GLAll>();
        DataTable dtGetData = DBHandler.GetResult("Get_GlCodeAll_M_Pf", "P");
        foreach (DataRow dtRow in dtGetData.Rows)
        {
            GLAll DataObj = new GLAll();

            DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
            DataObj.GL_NAME = dtRow["GL_NAME"].ToString();
            Detail.Add(DataObj);
        }
        return Detail.ToArray();
    }
    public class GLAll //Class for binding data
    {

        public string OLD_GL_ID { get; set; }
        public string GL_NAME { get; set; }
    }

    private void PopulateGrid1()
    {
        try
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("OLD_GL_ID");
            dt.Columns.Add("GL_NAME");
            dt.Rows.Add();
            grdGL.DataSource = dt;
            grdGL.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    [WebMethod]
    public static string GET_SetGLCode(int type,string StrGLCode)
    {
        string msg = "";
        if (type == 0) { hdnGLID = ""; }
        else { hdnGLID = StrGLCode; }
        return msg;
    }

    public void datecompare_Greater(string d1, string d2)
    {
        string[] fd = d1.Split('/');
        string[] ld = d2.Split('/');

        if (Convert.ToInt32(fd[2]) > Convert.ToInt32(ld[2]))
        {
            Console.WriteLine("greatre date is  {0}", d1);
            Console.ReadLine();
        }
        else {
          //  Console.WriteLine("greatre date is  {0}", d2);
            if (Convert.ToInt32(fd[2]) < Convert.ToInt32(ld[2]))
            {
                Console.WriteLine("greatre date is  {0}", d2);
                Console.ReadLine();
            }
            else
            {
              //  Console.WriteLine("greatre date is  {0}", d2);
                if (Convert.ToInt32(fd[1]) > Convert.ToInt32(ld[1]))
                {
                    Console.WriteLine("greatre date is  {0}", d1);
                    Console.ReadLine();
                }
                else
                {
                    //  Console.WriteLine("greatre date is  {0}", d2);
                    if (Convert.ToInt32(fd[1]) < Convert.ToInt32(ld[1]))
                    {
                        Console.WriteLine("greatre date is  {0}", d2);
                        Console.ReadLine();
                    }
                    else
                    {
                        //  Console.WriteLine("greatre date is  {0}", d2);
                        if (Convert.ToInt32(fd[0]) > Convert.ToInt32(ld[0]))
                        {
                            Console.WriteLine("greatre date is  {0}", d1);
                            Console.ReadLine();
                        }
                        else
                        {
                            //  Console.WriteLine("greatre date is  {0}", d2);
                            if (Convert.ToInt32(fd[0]) < Convert.ToInt32(ld[0]))
                            {
                                Console.WriteLine("greatre date is  {0}", d2);
                                Console.ReadLine();
                            }
                            else
                            {
                                  Console.WriteLine("Both are same date");
                                  Console.ReadLine();
                            }
                        }
                    }
                }
            }
        }      

    }

    //public string datecompare_less(string d1, string d2)
    //{
    //    string lessdate = "";
    //    string[] fd = d1.Split('/');
    //    string[] ld = d2.Split('/');
    //    if(Convert.ToInt32(fd[2])<)
    //    {

    //    }
    //    return lessdate;
    //}
    //string FileName ="Vithal"+DateTime.Now+".xls";
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        TextBox frmDT = (TextBox)dv.FindControl("txtFrmDate");
        TextBox toDT = (TextBox)dv.FindControl("txtToDate");
        DataTable dt = new DataTable();
        DateTime dtFromDate = DateTime.ParseExact(frmDT.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        DateTime dtToDate = DateTime.ParseExact(toDT.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        TextBox txtGLName = (TextBox)dv.FindControl("txtGLName");
        string zero = "";
        CheckBox chkZero = (CheckBox)dv.FindControl("chkZero");
        if (chkZero.Checked == true)
        {
            zero = "Y";
        }
        else
        {
            zero = "N";
        }
        if (hdnGLID == "") 
        {
            pan_tbl.Visible = true;
            pan_tblSL.Visible = false;
            panVoucherMon.Visible = false;
            panVchMonthDtl.Visible = false;
            dt = DBHandler.GetResult("Acc_Gl_trial_Excel_Pf", dtFromDate.ToShortDateString(), dtToDate.ToShortDateString(), zero, 1, "P");
        }
        string FileName = "TrailBal" + DateTime.Now + ".xls";
        string attachment = "attachment; filename=" + FileName;
        //string attachment = "attachment; filename=Malhotra.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.ms-excel";
        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");
        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                Response.Write(tab + dr[i].ToString());
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }
}