﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="NPASummaryReport.aspx.cs" Inherits="NPASummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="js/jquery.maskedinput.js"></script>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert('Only Numeric Value Accept!');
                return false;
            }
            return true;
        }

        function FunNPASummary() {
            var FromYearMonth = '';
            var ToYearMonth = '';
            if ($('#txtFrmDate').val().trim()== '') {
                alert("Please Enter From Year And Month");
                $('#txtFrmDate').focus();
                return false;
            }

            //if ($('#txtToDate').val().trim() == '') {
            //    alert("Please Enter To Year And Month");
            //    $('#txtToDate').focus();
            //    return false;
            //}
            FromYearMonth = $('#txtFrmDate').val();
            ToYearMonth = '0';// $('#txtToDate').val();
            $(".loading-overlay").show();
            var W = "{FromYearMonth:'" + FromYearMonth + "',ToYearMonth:'" + ToYearMonth + "'}";
            $.ajax({
                type: "POST",
                url: "NPASummaryReport.aspx/GET_NPASummary",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (D) {
                    var xmlDoc = $.parseXML(D.d);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");
                    var Total_A=0;
                    var Total_B=0;
                    var Total_C=0;
                    var Total_DA1=0;
                    var Total_DA2=0;
                    var Total_DA3=0;
                    var Total_LA=0;
                    var Total_DFA=0;
                    $("#GridNPASummary").empty();
                    $("#GridNPASummary").append("<tr style='background-color:navy;color:white;font-weight:bold;font-size:12px;'><th style='display:none'>Branch ID</th><th>Branch</th> <th>A</th><th>B</th><th>C</th><th>DA1</th><th>DA2</th><th>DA3</th><th>LA</th><th>Total</th></tr>");
                    if (Table.length > 0) {
                        $(xml).find("Table").each(function () {
                            if ($(this).find("HGROUP_A").text() != '') { Total_A = parseFloat(Total_A) + parseFloat($(this).find("HGROUP_A").text()); }
                            if ($(this).find("HGROUP_B").text() != '') { Total_B = parseFloat(Total_B) + parseFloat($(this).find("HGROUP_B").text()); }
                            if ($(this).find("HGROUP_C").text() != '') { Total_C = parseFloat(Total_C) + parseFloat($(this).find("HGROUP_C").text()); }
                            if ($(this).find("HGROUP_DA1").text() != '') { Total_DA1 = parseFloat(Total_DA1) + parseFloat($(this).find("HGROUP_DA1").text()); }
                            if ($(this).find("HGROUP_DA2").text() != '') { Total_DA2 = parseFloat(Total_DA2) + parseFloat($(this).find("HGROUP_DA2").text()); }
                            if ($(this).find("HGROUP_DA3").text() != '') { Total_DA3 = parseFloat(Total_DA3) + parseFloat($(this).find("HGROUP_DA3").text()); }
                            if ($(this).find("HGROUP_LA").text() != '') { Total_LA = parseFloat(Total_LA) + parseFloat($(this).find("HGROUP_LA").text()); }
                            if ($(this).find("TOT_DEFA").text() != '') { Total_DFA = parseFloat(Total_DFA) + parseFloat($(this).find("TOT_DEFA").text()); }

                            $("#GridNPASummary").append("<tr style=';font-size:11px;'><td align='center' style='height:13px;width:50px; padding: 5px;display:none;'>" +
                                       $(this).find("SECTORID").text() + "</td> <td align='center' style='height:13px;width:400px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("SECTORCODE").text() + "</td> <td align='right' style='height:13px;width:400px;padding: 5px;cursor:pointer;color:navy;font-weight:bold;' onClick='FunctionA(this," + $(this).find("SECTORID").text() + "," + $(this).find("HGROUP_A").text() + ");'>" +
                                       $(this).find("HGROUP_A").text() + "</td> <td align='right' style='height:13px;width:100px;padding: 5px;cursor:pointer;color:navy;font-weight:bold;' onClick='FunctionB(this," + $(this).find("SECTORID").text() + "," + $(this).find("HGROUP_B").text() + ");'>" +
                                       $(this).find("HGROUP_B").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;cursor:pointer;color:navy;font-weight:bold;' onClick='FunctionC(this," + $(this).find("SECTORID").text() + "," + $(this).find("HGROUP_C").text() + ");'>" +
                                       $(this).find("HGROUP_C").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;cursor:pointer;color:navy;font-weight:bold;' onClick='FunctionD1(this," + $(this).find("SECTORID").text() + "," + $(this).find("HGROUP_DA1").text() + ");'>" +
                                       $(this).find("HGROUP_DA1").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;cursor:pointer;color:navy;font-weight:bold;' onClick='FunctionD2(this," + $(this).find("SECTORID").text() + "," + $(this).find("HGROUP_DA2").text() + ");'>" +
                                       $(this).find("HGROUP_DA2").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;cursor:pointer;color:navy;font-weight:bold;' onClick='FunctionD3(this," + $(this).find("SECTORID").text() + "," + $(this).find("HGROUP_DA3").text() + ");'>" +
                                       $(this).find("HGROUP_DA3").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;cursor:pointer;color:navy;font-weight:bold;' onClick='FunctionLA(this," + $(this).find("SECTORID").text() + "," + $(this).find("HGROUP_LA").text() + ");'>" +
                                       $(this).find("HGROUP_LA").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                       $(this).find("TOT_DEFA").text() + "</td></tr>");
                        });

                        $("#GridNPASummary").append("<tr style=';font-size:11px;'><td align='center' style='height:13px;width:50px; padding: 5px;display:none;'>" +
                                            0 + "</td> <td align='center' style='height:13px;width:400px;padding: 5px;color:red;font-weight:bold;'>" +
                                            'Total' + "</td> <td align='right' style='height:13px;width:400px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(Total_A).toFixed(2) + "</td> <td align='right' style='height:13px;width:100px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(Total_B).toFixed(2) + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(Total_C).toFixed(2) + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(Total_DA1).toFixed(2) + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(Total_DA2).toFixed(2) + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(Total_DA3).toFixed(2) + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(Total_LA).toFixed(2) + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'>" +
                                            parseFloat(Total_DFA).toFixed(2) + "</td></tr>");
                    }
                    $(".loading-overlay").hide();
                    return false;
                },
                error: function (result) {
                    alert("Error Records Data NPA Summary");
                    return false;
                }
            });

        }

        function FunctionA(td, secid,HCAmount)
        {
            //if (secid == 0) { alert('Invalid Selection!.....'); return false; }
            if (HCAmount == 0) { alert('No Records Found!.....'); return false; }
            FunNPADetail(secid, 'A');
            return false;
        }

        function FunctionB(td, secid, HCAmount) {
            if (HCAmount == 0) { alert('No Records Found!.....'); return false; }
            FunNPADetail(secid, 'B');
            return false;
        }

        function FunctionC(td, secid, HCAmount) {
            if (HCAmount == 0) { alert('No Records Found!.....'); return false; }
            FunNPADetail(secid, 'C');
            return false;
        }

        function FunctionD1(td, secid, HCAmount) {
            if (HCAmount == 0) { alert('No Records Found!.....'); return false; }
            FunNPADetail(secid, 'DA1');
            return false;
        }

        function FunctionD2(td, secid, HCAmount) {
            if (HCAmount == 0) { alert('No Records Found!.....'); return false; }
            FunNPADetail(secid, 'DA2');
            return false;
        }

        function FunctionD3(td, secid, HCAmount) {
            if (HCAmount == 0) { alert('No Records Found!.....'); return false; }
            FunNPADetail(secid, 'DA3');
            return false;
        }

        function FunctionLA(td, secid, HCAmount) {
            if (HCAmount == 0) { alert('No Records Found!.....'); return false; }
            FunNPADetail(secid, 'LA');
            return false;
        }
        var ValHealthCode = '';
        var ValSectorID = '';
        function FunNPADetail(SectorID, HealthCode)
        {
            ValHealthCode = HealthCode;
            ValSectorID = SectorID;
            var FromYearMonth = '';
            var ToYearMonth = '';
            if ($('#txtFrmDate').val().trim() == '') {
                alert("Please Enter From Year And Month");
                $('#txtFrmDate').focus();
                return false;
            }

            //if ($('#txtToDate').val().trim() == '') {
            //    alert("Please Enter To Year And Month");
            //    $('#txtToDate').focus();
            //    return false;
            //}

            FromYearMonth = $('#txtFrmDate').val();
            ToYearMonth = 0;// $('#txtToDate').val();
            $(".loading-overlay").show();
            var W = "{FromYearMonth:'" + FromYearMonth + "',ToYearMonth:'" + ToYearMonth + "',SectorID:" + SectorID + ",HealthCode:'" + HealthCode + "'}";
            $.ajax({
                type: "POST",
                url: "NPASummaryReport.aspx/GET_NPADetail",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (D) {
                    var xmlDoc = $.parseXML(D.d);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");
                    var BAL = 0;
                    var GROSS_ASSTES = 0;
                    var SECU = 0;
                    var PROV = 0;
                    var NET_ASSETS = 0;
                    var GROSS_NPA_ASSETS = 0;
                    var NET_NPA_ASSETS = 0;
                    var PRINCIPAL = 0;
                    var INTEREST = 0;
                    var OTHER = 0;
                    $("#GridViewNPADetail").empty();
                    $("#GridViewNPADetail").append("<tr style='background-color:navy;color:white;font-weight:bold;'><th style='display:none'>LOANEE ID</th><th>Loanee Code</th><th>Loanee Name</th><th>Health Code</th><th>Balance</th><th>Principal Default</th><th>Interest Default</th><th>Other Default</th><th>Gross Assets</th><th>Provision</th><th>Net Assets</th></tr>");
                    if (Table.length > 0) {
                        $(xml).find("Table").each(function () {
                            if ($(this).find("BAL").text() != '') { BAL = parseFloat(BAL) + parseFloat($(this).find("BAL").text()); }
                            if ($(this).find("GROSS_ASSTES").text() != '') { GROSS_ASSTES = parseFloat(GROSS_ASSTES) + parseFloat($(this).find("GROSS_ASSTES").text()); }
                            if ($(this).find("SECU").text() != '') { SECU = parseFloat(SECU) + parseFloat($(this).find("SECU").text()); }
                            if ($(this).find("PROV").text() != '') { PROV = parseFloat(PROV) + parseFloat($(this).find("PROV").text()); }
                            if ($(this).find("NET_ASSETS").text() != '') { NET_ASSETS = parseFloat(NET_ASSETS) + parseFloat($(this).find("NET_ASSETS").text()); }
                            if ($(this).find("GROSS_NPA_ASSETS").text() != '') { GROSS_NPA_ASSETS = parseFloat(GROSS_NPA_ASSETS) + parseFloat($(this).find("GROSS_NPA_ASSETS").text()); }
                            if ($(this).find("NET_NPA_ASSETS").text() != '') { NET_NPA_ASSETS = parseFloat(NET_NPA_ASSETS) + parseFloat($(this).find("NET_NPA_ASSETS").text()); }
                            if ($(this).find("PRINCIPAL_DEFAULT").text() != '') { PRINCIPAL = parseFloat(PRINCIPAL) + parseFloat($(this).find("PRINCIPAL_DEFAULT").text()); }
                            if ($(this).find("INTEREST_DEFAULT").text() != '') { INTEREST = parseFloat(INTEREST) + parseFloat($(this).find("INTEREST_DEFAULT").text()); }
                            if ($(this).find("OTHER_DEFAULT").text() != '') { OTHER = parseFloat(OTHER) + parseFloat($(this).find("OTHER_DEFAULT").text()); }
                            $("#GridViewNPADetail").append("<tr><td align='center' style='height:13px;width:50px; padding: 5px;display:none'>" +
                                       $(this).find("LOANEE_UNQ_ID").text() + "</td> <td align='center' style='height:13px;width:400px;padding: 5px;color:navy;font-weight:bold;cursor:pointer;'  onClick='FunInspection(this," + $(this).find("LOANEE_UNQ_ID").text() + ");'>" +
                                       $(this).find("LOANEE_CODE").text() + "</td> <td align='center' style='height:13px;width:400px;padding: 5px;color:navy;font-weight:bold;cursor:pointer;' onClick='FunInspection(this," + $(this).find("LOANEE_UNQ_ID").text() + ");'>" +
                                       $(this).find("LOANEE_NAME").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("HEALTH_CODE").text() + "</td> <td align='right' style='height:13px;width:100px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("BAL").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       
                                       $(this).find("PRINCIPAL_DEFAULT").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("INTEREST_DEFAULT").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("OTHER_DEFAULT").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +

                                       $(this).find("GROSS_ASSTES").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("PROV").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       
                                       $(this).find("NET_ASSETS").text() + "</td></tr>");

                        });
                        $("#GridViewNPADetail").append("<tr align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'><td style='display:none'>" +
                                       '' + "</td> <td>" +
                                       '' + "</td> <td>" +
                                       '' + "</td> <td >" +
                                       'Total' + "</td> <td >" +
                                       parseFloat(BAL).toFixed(2) + "</td> <td >" +
                                       parseFloat(PRINCIPAL).toFixed(2) + "</td> <td >" +
                                       parseFloat(INTEREST).toFixed(2) + "</td> <td >" +
                                       parseFloat(OTHER).toFixed(2) + "</td> <td >" +
                                       parseFloat(GROSS_ASSTES).toFixed(2) + "</td> <td >" +
                                       parseFloat(PROV).toFixed(2) + "</td> <td >" +
                                       parseFloat(NET_ASSETS).toFixed(2) + "</td></tr>");
                    }
                    ShowSpecModalPopup();
                    $(".loading-overlay").hide();
                    return false;
                },
                error: function (result) {
                    alert("Error Records Data NPA Detail");
                    return false;
                }
            });
        }


        function ShowSpecModalPopup() {
            $("#dialogNPADetail").dialog({
                title: "NPA Detail's",
                width: 950,
                overflow:scroll,
                resizable: false,
                position: ['middle', 60],
                buttons: {
                    Print: function () {
                        alert('Comming Soon.........Please Wait........!'); return false;
                    },
                    Close: function () {
                        $("#dialogNPADetail").dialog('close');
                        }
                },
                modal: true
            });
        }

        function ShowNPAOrgCode() {
            $("#dialogNPAOrgCode").dialog({
                title: "NPA Detail's",
                width: 950,
                overflow: scroll,
                resizable: false,
                position: ['middle', 60],
                buttons: {
                    Print: function () {
                        alert('Comming Soon.........Please Wait........!'); return false;
                    },
                    Close: function () {
                        $("#dialogNPAOrgCode").dialog('close');
                    }
                },
                modal: true
            });
        }

        // ========== User Rights Coding Here ====================//
        function ShowUser(LOANEE_ID, InspDate) {
            $(".loading-overlay").show();
            var W = "{LOANEE_ID:" + LOANEE_ID + ",InspDate:'" + InspDate + "'}";
            $.ajax({
                type: "POST",
                url: "NPASummaryReport.aspx/GET_UserName",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    var xmlDoc = $.parseXML(data.d);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");
                    var ApprovalFlag = '';
                    ApprovalFlag = $(Table).find("ApprovalFlag").text();
                    $('#txtInspectionBy').val('');
                    $('#txtInspHeading').val('');
                    $('#txtRemarks').val('');
                    $('#txtInspectionBy').val($(Table).find("UserName").text());
                    $('#txtInspHeading').val($(Table).find("InspHeading").text());
                    $('#txtRemarks').val($(Table).find("Remarks").text());
                    $('#txtInspectionID').val($(Table).find("InspectionID").text());
                    if ($(Table).find("InspApprove").text() == 0) { $("#txtInspectionDate,#txtInspHeading,#txtRemarks").attr("disabled", false); }
                    else { $("#txtInspectionDate,#txtInspHeading,#txtRemarks").attr("disabled", true); }
                     
                    $("#GridViewInspRec").empty();
                    var srNo = 1;
                    if (Table.length > 0) {
                        if (ApprovalFlag == 0) {
                            $('#cmdpermission').hide();
                            $("#GridViewInspRec").append("<tr style='background-color:navy;color:white;font-weight:bold;'><th style='display:none'>Edit</th><th style='display:none'>Delete</th><th style='display:none'>Approved</th><th>Sr. No.</th><th>User Name</th><th>Inspection Date</th><th>Inspection Heading</th><th style='display:none;'>Remarks</th></tr>");
                            $(xml).find("Table1").each(function (ApprovalFlag) {
                                $("#GridViewInspRec").append('<tr style="height:13px;width:400px;padding: 5px;color:navy;font-weight:bold;"><td style="display:none;">' + "<div onClick='EditInspection(this," + $(this).find("InspectionID").text() + ");'><img src='images/Modify1.png' style='text-align:center;cursor:pointer' id='" + $(this).find("InspectionID").text() + "'/>" + '</td>' +
                            '<td style="display:none;" >' + "<div onClick='DeleteInspection(this," + $(this).find("InspectionID").text() + ");'><img src='images/close.png' style='text-align:center;cursor:pointer' id='" + $(this).find("InspectionID").text() + "'/>" + '</td>' +
                            '<td style="display:none;">' + "<div onClick='ApprovedInspection(this," + $(this).find("InspectionID").text() + ");'><img src='images/delete.png' style='text-align:center;cursor:pointer' id='" + $(this).find("InspectionID").text() + "'/>" + '</td>' +
                            '<td align="center">' + srNo + '</td>' +
                            '<td align="center">' + $(this).find("Name").text() + '</td>' +
                            '<td align="center">' + $(this).find("InspectionDate").text() + '</td>' +
                            '<td align="left">' + $(this).find("InspHeading").text() + '</td>' +
                            '<td style="display:none;" >' + $(this).find("Remarks").text() + '</td>' + '</tr>');

                                srNo = parseInt(srNo) + 1;
                            });
                        } 
                        else {
                            $("#GridViewInspRec").append("<tr style='background-color:navy;color:white;font-weight:bold;'><th >Edit</th><th >Delete</th><th>Approved</th><th>Sr. No.</th><th>User Name</th><th>Inspection Date</th><th>Inspection Heading</th><th style='display:none;'>Remarks</th></tr>");
                            $(xml).find("Table1").each(function (ApprovalFlag) {
                        $("#GridViewInspRec").append('<tr style="height:13px;width:400px;padding: 5px;color:navy;font-weight:bold;"><td align="center">' + "<div onClick='EditInspection(this," + $(this).find("InspectionID").text() + ");'><img src='images/Modify1.png' style='text-align:center;cursor:pointer' id='" + $(this).find("InspectionID").text() + "'/>" + '</td>' +
                       '<td align="center">' + "<div onClick='DeleteInspection(this," + $(this).find("InspectionID").text() + ");'><img src='images/close.png' style='text-align:center;cursor:pointer' id='" + $(this).find("InspectionID").text() + "'/>" + '</td>' +
                       '<td align="center">' + "<div ><input type='CheckBox' id='ChkTerms' style='text-align:center;cursor:pointer'  value='" + $(this).find("InspectionID").text() + "' onchange='SaveApproved(this," + $(this).find("InspectionID").text() + ")'/>" + '</td>' +
                       '<td align="center">' + srNo + '</td>' +
                       '<td align="center">' + $(this).find("Name").text() + '</td>' +
                       '<td align="center">' + $(this).find("InspectionDate").text() + '</td>' +
                       '<td align="left">' + $(this).find("InspHeading").text() + '</td>' +
                       '<td style="display:none;">' + $(this).find("Remarks").text() + '</td>' + '</tr>');
                        var CheckTerm = GridViewInspRec.rows[srNo].cells[2].getElementsByTagName('input')[0];
                        if ($(this).find("LockedFlag").text() == '0') { CheckTerm.checked = false; } else { CheckTerm.checked = true; }
                                srNo = parseInt(srNo) + 1;
                            });
                        }
                    }
                    $(".loading-overlay").hide();
                    return false;
                },
            });
        }
        var ValLOANEEID = '';
        function EditInspection(t, InspectionID)
        {
            var s = $(t).closest('tr').find("input[id='ChkTerms']:checkbox");
            var Approvelvalue = $(s).is(':checked');
            if (Approvelvalue == true) { alert('Records Already Approved..Can Not Edit..!'); return false; }
            var InsDate = $(t).closest('tr').find('td:eq(5)').text();
            var InspHead = $(t).closest('tr').find('td:eq(6)').text();
            var Remarks = $(t).closest('tr').find('td:eq(7)').text();
            $("#txtInspectionDate").val(InsDate);
            $("#txtInspHeading").val(InspHead);
            $("#txtRemarks").val(Remarks);
            $('#txtInspectionID').val(InspectionID);
            return false;
        }

        function DeleteInspection(t, InspectionID)
        {
            var s = $(t).closest('tr').find("input[id='ChkTerms']:checkbox");
            var Approvelvalue = $(s).is(':checked');
            if (Approvelvalue == true) { alert('Records Already Approved..Can Not Delete Record..!'); return false; }
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("Do you want to Delete Inspection?")) {confirm_value.value = "Yes";}
            else {confirm_value.value = "No";}
            if (confirm_value.value == "Yes") {
                ApprovedAndDelete('Delete', InspectionID, 0);
            }
            return false;
        }

        function SaveApproved(g, InspectionID) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";

            if (confirm("Do you want to Approved Inspection?")) {confirm_value.value = "Yes";}
            else { confirm_value.value = "No"; $(g).prop('checked', false); }
            if (confirm_value.value == "Yes") {
                var Approvelvalue = $(g).is(':checked');
                Approvelvalue = Approvelvalue == true ? 1 : 0;
                ApprovedAndDelete('Approved', InspectionID, Approvelvalue);
            }
            return false;
        }

        function ApprovedAndDelete(Process, InspectionID, Approvelvalue) {
            if (Process == '') { alert('Invalid Process...!.. Try Again..!'); return false; }
            if (InspectionID == '') { alert('Invalid Process...!.. Try Again..!'); return false; }
            $(".loading-overlay").show();
            var W = "{Process:'" + Process + "',InspectionID:" + InspectionID + ",Approvelvalue:" + Approvelvalue + "}";
            $.ajax({
                type: "POST",
                url: "NPASummaryReport.aspx/GET_ApprovAndDel",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (response) {
                    var msg = response.d;
                    alert(msg);
                    $(".loading-overlay").hide();
                    return false;
                },
                error: function (result) {
                    alert("Error Records Save Approval And Delete");
                    return false;
                }
            });

        }
        function RbSelectType() {
            if (document.getElementById('RBInspection').checked) {
                document.getElementById("RbLoaneeName").checked = false;
                document.getElementById("RBOrgCode").checked = false;
                return false;
            }
        }
        function RbSelectType1() {
             if (document.getElementById('RbLoaneeName').checked) {
                document.getElementById("RBInspection").checked = false;
                document.getElementById("RBOrgCode").checked = false;
                return false;
            }
        }
        function RbSelectType2() {
            if (document.getElementById('RBOrgCode').checked) {
                document.getElementById("RBInspection").checked = false;
                document.getElementById("RbLoaneeName").checked = false;
                return false;
            }
        }

        function FunInspection(t, LOANEE_ID)
        {
            if (document.getElementById('RBInspection').checked) {
                FunInspectionRec(t, LOANEE_ID);
            }
            if (document.getElementById('RbLoaneeName').checked ) {
                FunOriginalCode(t, LOANEE_ID,'LN');
            }

            if (document.getElementById('RBOrgCode').checked) {
                FunOriginalCode(t, LOANEE_ID, 'LOC');
            }
            return false;
        }

        function FunOriginalCode(t, LOANEE_ID, valSearchType)
        {
            var FromYearMonth = '';
            var ToYearMonth = '';
            var SearchType = '';
            SearchType = valSearchType;
            if ($('#txtFrmDate').val().trim() == '') {
                alert("Please Enter From Year And Month");
                $('#txtFrmDate').focus();
                return false;
            }

            FromYearMonth = $('#txtFrmDate').val();
            ToYearMonth = 0;
            var SectorID = ValSectorID;
            var HealthCode = ValHealthCode;
            $(".loading-overlay").show();
            var W = "{FromYearMonth:'" + FromYearMonth + "',SectorID:" + SectorID + ",HealthCode:'" + HealthCode + "',LOANEE_ID:" + LOANEE_ID + ",SearchType:'" + SearchType + "'}";
            $.ajax({
                type: "POST",
                url: "NPASummaryReport.aspx/GET_NPAOrgCode",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (D) {
                    var xmlDoc = $.parseXML(D.d);
                    var xml = $(xmlDoc);
                    var Table = xml.find("Table");
                    var BAL = 0;
                    var GROSS_ASSTES = 0;
                    var SECU = 0;
                    var PROV = 0;
                    var NET_ASSETS = 0;
                    var GROSS_NPA_ASSETS = 0;
                    var NET_NPA_ASSETS = 0;
                    var PRINCIPAL = 0;
                    var INTEREST = 0;
                    var OTHER = 0;
                    var OriginalCode = '';
                    $("#GridViewNPAOrgCode").empty();
                    $("#GridViewNPAOrgCode").append("<tr style='background-color:navy;color:white;font-weight:bold;'><th style='display:none'>LOANEE ID</th><th>Loanee Code</th><th>Loanee Name</th><th>Health Code</th><th>Balance</th><th>Principal Default</th><th>Interest Default</th><th>Other Default</th><th>Gross Assets</th><th>Provision</th><th>Net Assets</th></tr>");
                    if (Table.length > 0) {
                        $(xml).find("Table").each(function () {
                            if ($(this).find("BAL").text() != '') { BAL = parseFloat(BAL) + parseFloat($(this).find("BAL").text()); }
                            if ($(this).find("GROSS_ASSTES").text() != '') { GROSS_ASSTES = parseFloat(GROSS_ASSTES) + parseFloat($(this).find("GROSS_ASSTES").text()); }
                            if ($(this).find("SECU").text() != '') { SECU = parseFloat(SECU) + parseFloat($(this).find("SECU").text()); }
                            if ($(this).find("PROV").text() != '') { PROV = parseFloat(PROV) + parseFloat($(this).find("PROV").text()); }
                            if ($(this).find("NET_ASSETS").text() != '') { NET_ASSETS = parseFloat(NET_ASSETS) + parseFloat($(this).find("NET_ASSETS").text()); }
                            if ($(this).find("GROSS_NPA_ASSETS").text() != '') { GROSS_NPA_ASSETS = parseFloat(GROSS_NPA_ASSETS) + parseFloat($(this).find("GROSS_NPA_ASSETS").text()); }
                            if ($(this).find("NET_NPA_ASSETS").text() != '') { NET_NPA_ASSETS = parseFloat(NET_NPA_ASSETS) + parseFloat($(this).find("NET_NPA_ASSETS").text()); }
                            if ($(this).find("PRINCIPAL_DEFAULT").text() != '') { PRINCIPAL = parseFloat(PRINCIPAL) + parseFloat($(this).find("PRINCIPAL_DEFAULT").text()); }
                            if ($(this).find("INTEREST_DEFAULT").text() != '') { INTEREST = parseFloat(INTEREST) + parseFloat($(this).find("INTEREST_DEFAULT").text()); }
                            if ($(this).find("OTHER_DEFAULT").text() != '') { OTHER = parseFloat(OTHER) + parseFloat($(this).find("OTHER_DEFAULT").text()); }
                            OriginalCode = $(this).find("Original_Code").text();
                            $("#GridViewNPAOrgCode").append("<tr><td align='center' style='height:13px;width:50px; padding: 5px;display:none'>" +
                                       $(this).find("LOANEE_UNQ_ID").text() + "</td> <td align='center' style='height:13px;width:400px;padding: 5px;color:navy;font-weight:bold;cursor:pointer;'  onClick='FunInspection(this," + $(this).find("LOANEE_UNQ_ID").text() + ");'>" +
                                       $(this).find("LOANEE_CODE").text() + "</td> <td align='center' style='height:13px;width:400px;padding: 5px;color:navy;font-weight:bold;cursor:pointer;' onClick='FunInspection(this," + $(this).find("LOANEE_UNQ_ID").text() + ");'>" +
                                       $(this).find("LOANEE_NAME").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("HEALTH_CODE").text() + "</td> <td align='right' style='height:13px;width:100px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("BAL").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +

                                       $(this).find("PRINCIPAL_DEFAULT").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("INTEREST_DEFAULT").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("OTHER_DEFAULT").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +

                                       $(this).find("GROSS_ASSTES").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +
                                       $(this).find("PROV").text() + "</td> <td align='right' style='height:13px;width:200px;padding: 5px;color:navy;font-weight:bold;'>" +

                                       $(this).find("NET_ASSETS").text() + "</td></tr>");

                        });
                        $("#GridViewNPAOrgCode").append("<tr align='right' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'><td style='display:none'>" +
                                       '' + "</td> <td>" +
                                       'Original Code' + "</td> <td align='center'>" +
                                       OriginalCode + "</td> <td >" +
                                       'Total' + "</td> <td >" +
                                       parseFloat(BAL).toFixed(2) + "</td> <td >" +
                                       parseFloat(PRINCIPAL).toFixed(2) + "</td> <td >" +
                                       parseFloat(INTEREST).toFixed(2) + "</td> <td >" +
                                       parseFloat(OTHER).toFixed(2) + "</td> <td >" +
                                       parseFloat(GROSS_ASSTES).toFixed(2) + "</td> <td >" +
                                       parseFloat(PROV).toFixed(2) + "</td> <td >" +
                                       parseFloat(NET_ASSETS).toFixed(2) + "</td></tr>");
                    }
                    ShowNPAOrgCode();
                    $(".loading-overlay").hide();
                    return false;
                },
                error: function (result) {
                    alert("Error Records Data NPA Detail");
                    return false;
                }
            });
        }

        function FunInspectionRec(t, LOANEE_ID)
        {
            ValLOANEEID = LOANEE_ID;
            $("#dialogInspection").dialog({
                title: "Inspection Detail's",
                width: 970,
                Height: 600,
                resizable: false,
                position: ['middle', 60],
                modal: true
            });

            $("#txtLoaneeCode").val(LOANEE_ID);
            var LOANEE_CODE = $(t).closest('tr').find('td:eq(1)').text(); //$(t).closest("tr, td").addClass(".LOANEE_CODE").text();
            var LOANEE_NAME = $(t).closest('tr').find('td:eq(2)').text();

            var HEALTH_CODE = $(t).closest('tr').find('td:eq(3)').text();
            var BAL = $(t).closest('tr').find('td:eq(4)').text();
            var PRINCIPAL_DEFAULT = $(t).closest('tr').find('td:eq(5)').text();
            var INTEREST_DEFAULT = $(t).closest('tr').find('td:eq(6)').text();
            var OTHER_DEFAULT = $(t).closest('tr').find('td:eq(7)').text();



            $("#GridViewInsp").empty();
            $("#GridViewInsp").append("<tr style='background-color:navy;color:white;font-weight:bold;'><th>Loanee Code</th><th>Loanee Name</th><th>Health Code</th><th>Year & Month</th><th>Balance</th><th>Principal Default</th><th>Interest Default</th><th>Other Default</th></tr>");
            $("#GridViewInsp").append("<tr align='middle' style='height:13px;width:200px;padding: 5px;color:red;font-weight:bold;'><td>" +
                                       LOANEE_CODE + "</td> <td>" +
                                       LOANEE_NAME + "</td> <td>" +
                                       HEALTH_CODE + "</td> <td>" +
                                       $("#txtFrmDate").val() + "</td> <td>" +
                                       parseFloat(BAL).toFixed(2) + "</td> <td>" +
                                       parseFloat(PRINCIPAL_DEFAULT).toFixed(2) + "</td> <td>" +
                                       parseFloat(INTEREST_DEFAULT).toFixed(2) + "</td> <td>" +
                                       parseFloat(OTHER_DEFAULT).toFixed(2) + "</td></tr>");

            var InspDate = '';
            if ($('#txtInspectionDate').val() != '') {
                var insDate = $("#txtInspectionDate").val().split("/");
                InspDate = insDate[2] + "-" + insDate[1] + "-" + insDate[0];
            }
            ShowUser(LOANEE_ID, InspDate);
            $('#txtInspectionDate').focus();
            return false;
        }


        $(document).ready(function () {
            $("#cmdSave").click(function (evt) {
                var InspectionDate = '';
                var InspHeading = '';
                var Remarks = '';
                var LoaneeID = '';
                var InspectionID = '';
                if ($("#txtInspectionDate").val().trim() == '') { alert('Please seelct Date'); $("#txtInspectionDate").focus(); return false; }
                if ($("#txtInspHeading").val().trim() == '') { alert('Please Enter Inspection Heading'); $("#txtInspHeading").focus(); $("#txtInspHeading").val(''); return false; }
                if ($("#txtRemarks").val().trim() == '') { alert('Please Enter Remarks'); $("#txtRemarks").focus(); $("#txtRemarks").val(''); return false; }
                if ($('#txtInspectionDate').val() != '') {
                    var insDate = $("#txtInspectionDate").val().split("/");
                    InspectionDate = insDate[2] + "-" + insDate[1] + "-" + insDate[0];
                }
                InspHeading = $("#txtInspHeading").val().replace("'", "^");
                Remarks = $("#txtRemarks").val().replace("'", "^");
                LoaneeID = ValLOANEEID;
                InspectionID = $("#txtInspectionID").val();
                $(".loading-overlay").show();
                var E = "{InspectionID:" + InspectionID + ",LoaneeID:" + LoaneeID + ", InspectionDate: '" + InspectionDate + "',InspHeading: '" + InspHeading.replace('"', "\\\"") + "',Remarks: '" + Remarks.replace('"', "\\\"") + "' }";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/Save_Inspection',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var msg = response.d;
                        if (msg != 'Records Already Exists') { $('#txtRemarks').val(''); }
                        ShowUser(ValLOANEEID, InspectionDate);
                        ShowCurrentDate();
                        alert(msg);
                        $('#txtInspectionID').val(0);

                        $("#txtInspectionDate").focus();
                        $(".loading-overlay").hide();
                        return false;
                    },
                    error: function (response) {
                        var responseText;
                        responseText = JSON.parse(response.responseText);
                        alert("Error : " + responseText.Message);
                        $(".loading-overlay").hide();
                        return false;
                    },
                    failure: function (response) {
                        alert(response.d);
                        $(".loading-overlay").hide();
                        return false;
                    }
                });
            });

            $("#cmdCancel").click(function (evt) {
                ShowCurrentDate();
                $('#txtInspectionID').val(0);
                $("#txtInspectionDate").focus();
                $('#txtInspHeading').val('');
                $('#txtRemarks').val('');
                $("#txtInspectionDate,#txtRemarks").attr("disabled", false);
                return false;
            });

            $("#cmdClose").click(function (evt) {
                $("#dialogInspection").dialog('close'); $("#txtRemarks").val(''); ValLOANEEID = ''; return false;
            });

        });
        function ShowCurrentDate() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = dd + '/' + mm + '/' + yyyy;

            $("#txtInspectionDate").val(today);

            ModifyrowCount = '0';
        }

        $(document).ready(function () {

            $("#drpUser").change(function () {
                var Userid = $("#drpUser").val();
               
                var W = "{userid:" + Userid + "}";
                $.ajax({
                    type: "POST",
                    url: "NPASummaryReport.aspx/GET_AdmiApproval",
                    contentType: "application/json;charset=utf-8",
                    data: W,
                    dataType: "json",
                    success: function (data) {
                        var xmlDoc = $.parseXML(data.d);
                        var xml = $(xmlDoc);
                        var Table = xml.find("Table");
                        var AdminFlag = $(Table).find("AdminFlag").text();
                        var ApprovalFlag = $(Table).find("ApprovalFlag").text();
                        if (AdminFlag == '0') { $('#chkAdminflag').prop('checked', false); } else { $('#chkAdminflag').prop('checked', true);}
                        if (ApprovalFlag == '0') { $('#chkApproval').prop('checked', false); } else { $('#chkApproval').prop('checked', true); }
                        return false;
                    },
                    error: function (result) {
                        alert("Error Records Data");
                        return false;
                    }
                });
            });


            $("#txtInspectionDate").keydown(function (evt) {
                var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode > 48 || iKeyCode < 57)) {
                    alert("Type Not Allow ! Please Select Date");
                    return false;
                }
            });
                        
            ShowCurrentDate();
           
            $('#txtInspectionDate').datepicker({
                autoOpen: false,
                showOtherMonths: true,
                selectOtherMonths: true,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: true,
                duration: 'slow',
                dateFormat: 'dd/mm/yy',

                onSelect: function (event) {
                    $('#txtInspectionDate').datepicker("hide");
                    $("#txtInspHeading").focus();
                    var InspDate = '';
                    if ($('#txtInspectionDate').val() != '') {
                        var insDate = $("#txtInspectionDate").val().split("/");
                        InspDate = insDate[2] + "-" + insDate[1] + "-" + insDate[0];
                    }

                    ShowUser($("#txtLoaneeCode").val(), InspDate);
                }
            });

            $("#GridNPASummary").empty();
            $("#GridNPASummary").append("<tr style='background-color:navy;color:white;font-weight:bold;font-size:12px;'><th style='display:none'>Branch ID</th><th>Branch</th> <th>A</th><th>B</th><th>C</th><th>DA1</th><th>DA2</th><th>DA3</th><th>LA</th><th>Total</th></tr>");
            $("#txtFrmDate").focus();
            $("#txtFrmDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    //$("#txtToDate").focus();
                    $("#btnGenerateReport").focus();
                    return false;
                }
            });
            $("#txtInspectionDate").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#txtInspHeading").focus(); 
                    return false;
                }
            });
            $("#txtInspHeading").keypress(function (event) {
                if (event.keyCode === 13) {
                    $("#txtRemarks").focus(); 
                    return false;
                }
            });

            //$("#txtToDate").keypress(function (event) {
            //    if (event.keyCode === 13) {
            //        $("#btnGenerateReport").focus();
            //        return false;
            //    }
            //}); 

            $("#cmdpermission").click(function (event) {
                funuserPermission();
                return false;
            });

            function funuserPermission() {
                

                $("#dialogUserPer").dialog({
                    title: "User Inspection Permission",
                    width: 500,
                    //Height: 700,
                    resizable: false,
                    position: ['middle', 60],
                    buttons: {
                        Save: function () {
                            var AdminFlg = '';
                            var ApprovalFlg = '';
                            if ($("#drpUser").val().trim() == 0) { alert('Please seelct User Name'); $("#drpUser").focus(); return false; }
                            if ($("#chkAdminflag").is(':checked') == true) { AdminFlg = 1; } else { AdminFlg = 0; }
                            if ($("#chkApproval").is(':checked') == true) { ApprovalFlg = 1; } else { ApprovalFlg = 0; }
                            $(".loading-overlay").show();
                            var E = "{UserID:" + $("#drpUser").val() + ",AdminFlg:" + AdminFlg + ", ApprovalFlg: " + ApprovalFlg + " }";
                            $.ajax({
                                type: "POST",
                                url: pageUrl + '/Save_UserPer',
                                data: E,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    var msg = response.d;
                                    alert(msg);
                                    $(".loading-overlay").hide();
                                    return false;
                                },  
                                error: function (response) {
                                    var responseText;
                                    responseText = JSON.parse(response.responseText);
                                    alert("Error : " + responseText.Message);
                                    $(".loading-overlay").hide();
                                    return false;
                                },
                                failure: function (response) {
                                    alert(response.d);
                                    $(".loading-overlay").hide();
                                    return false;
                                }
                            });
                        },
                        Close: function () {
                            $("#dialogUserPer").dialog('close'); return false;
                        }
                    },
                    modal: true
                    
                });

               
            }
            
            $("#btnPrint").click(function (event) {
                
                if ($("#txtFrmDate").val() == "") {
                    alert("Please Enter Recovery From Year&Month!");
                    $("#txtFrmDate").focus();
                    return false;
                }
                //if ($("#txtToDate").val() == "") {
                //    alert("Please Enter Recovery To Year&Month!");
                //    $("#txtToDate").focus();
                //    return false;
                //}
                //if ($("#txtFrmDate").val().length < 6) {
                //    alert('Please Enter Valid Recovery From Year&Month!');
                //    $("#txtFrmDate").focus();
                //    return false;
                //}
                //if ($("#txtToDate").val().length < 6) {
                //    alert('Please Enter Valid Recovery To Year&Month!');
                //    $("#txtFrmDate").focus();
                //    return false;
                //}
                //if ($("#txtFrmDate").val() > $("#txtToDate").val()) {
                //    alert('Recovery From Year&Month Shoulb be Less Than Or Qual To Recovery To Year&Month!');
                //    $("#txtFrmDate").focus();
                //    return false;
                //}
               
                ShowReport();
                return false;
            });

            function ShowReport() {
                alert('Comming Soon.........Please Wait........!'); return false;
                var FormName = '';
                var ReportName = '';
                var ReportType = '';
                var RecFromDate = '';
                var RecToDate = '';
                if ($("#ddlReportName").val() == "RR") {
                    FormName = "NPASummaryReport.aspx";
                    ReportName = "NPASummaryReport";
                    ReportType = "NPA Summary Report";
                }
                RecFromDate = $('#txtFrmDate').val();
                RecToDate = '0';// $('#txtToDate').val();

                var E = '';
                E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "',  RecFromDate:'" + RecFromDate + "',  RecToDate:'" + RecToDate + "'}";
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/SetReportValue',
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var show = response.d;
                        if (show == "OK") {
                            window.open("ReportView.aspx?E=Y");
                        }
                    },
                    error: function (response) {
                        var responseText;
                        responseText = JSON.parse(response.responseText);
                        alert("Error : " + responseText.Message);
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            
        });

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>NPA Summary</td>
            </tr>
        </table>

         <div class="loading-overlay" style="width:100%;height:150%;">
            <div class="loadwrapper" style="width:100%;height:150%;">
                <div class="ajax-loader-outer">Loading...</div>
            </div>
        </div>
          
         <div id="dialogUserPer" style="display: none;">
         <table>
             <tr style="font-size: 12px;color:navy;font-weight:bold;">
                 <td>
                     <asp:CheckBox ID="chkAdminflag" autocomplete="off" runat="server" Width="170px" TextAlign="Right" ClientIDMode="Static" Text=" Inspection Record" CssClass="inputbox2"/>
                 </td>
                 <td>
                     <asp:CheckBox ID="chkApproval" autocomplete="off" runat="server" ClientIDMode="Static" TextAlign="Right" Text=" Inspection Approval" CssClass="inputbox2"/>
                 </td>
                 </tr>
             </table>
             <table>
             <tr style="font-size: 12px;color:navy;font-weight:bold;">
                <td align="right" style="font-size: 12px;color:navy;font-weight:bold;">User Name&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td ">
                    <asp:DropDownList ID="drpUser" autocomplete="off" runat="server" Height="25px" Style="width:210px"  Font-Bold="true" DataValueField="Userid" DataTextField="Name" ClientIDMode="Static" CssClass="inputbox2" Width="170px">
                         <asp:ListItem Text="Select User Name" Value="0"></asp:ListItem>
                     </asp:DropDownList> 
                </td>
             </tr>
         </table>
             <table>
                 <tr>
                     <asp:GridView ID="GridUserAprovShow" autocomplete="off" ClientIDMode="Static" runat="server" Font-Bold="true" Font-Size="12px" HeaderStyle-BackColor="Navy" HeaderStyle-ForeColor="White" Width="100%" Style="border: 1px solid #59bdcc"  AutoGenerateColumns="true">
                        <RowStyle HorizontalAlign="Center"></RowStyle>
                         <%-- <Columns>
                             <asp:BoundField HeaderText="User Name" ItemStyle-Width="100" />
                             <asp:BoundField HeaderText="Inspection Record" ItemStyle-Width="150" />
                             <asp:BoundField HeaderText="Inspection Approval" ItemStyle-Width="150" />
                         </Columns>--%>
                     </asp:GridView>
                 </tr>
             </table>
     </div>

         <div id="dialogNPADetail" style="display: none;max-height:100px;overflow:scroll;">
             <asp:RadioButton ID="RBInspection" autocomplete="off" runat="server" ClientIDMode="Static" Text=" Inspection "  onchange='RbSelectType();' Checked="true" Font-Bold="true" Font-Size="14px" ForeColor="Green" />
             <asp:RadioButton ID="RbLoaneeName" autocomplete="off" runat="server" ClientIDMode="Static" Text=" Loanee Name " onchange='RbSelectType1();' Font-Bold="true" Font-Size="14px" ForeColor="Green"/>
             <asp:RadioButton ID="RBOrgCode" autocomplete="off" runat="server" ClientIDMode="Static" Text=" Original Code" onchange='RbSelectType2();' Font-Bold="true" Font-Size="14px" ForeColor="Green"/>
             
         <asp:GridView ID="GridViewNPADetail" autocomplete="off" runat="server" Width="100%" style="border:1px solid #59bdcc" AutoGenerateColumns="false"  PageSize="5" AllowPaging="true">    
             <Columns>        
                 <asp:BoundField HeaderText="LoaneeID" ItemStyle-Width="10" />
                <asp:BoundField HeaderText="Loanee Code" ItemStyle-Width="200" />
                 <asp:BoundField HeaderText="Loanee Name" ItemStyle-Width="200" />
                 <asp:BoundField HeaderText="Health Code" ItemStyle-Width="300" />
                 <asp:BoundField HeaderText="Balance" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Principle Default" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Interest Default" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Other Default" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Gross Assets" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Provision" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Net Assets" ItemStyle-Width="300" />
                 
               
             </Columns>
         </asp:GridView>
     </div>

        <div id="dialogNPAOrgCode" style="display: none;max-height:100px;overflow:scroll;">
         <asp:GridView ID="GridViewNPAOrgCode" autocomplete="off" runat="server" Width="100%" style="border:1px solid #59bdcc" AutoGenerateColumns="false"  PageSize="5" AllowPaging="true">    
             <Columns>        
                 <asp:BoundField HeaderText="LoaneeID" ItemStyle-Width="10" />
                <asp:BoundField HeaderText="Loanee Code" ItemStyle-Width="200" />
                 <asp:BoundField HeaderText="Loanee Name" ItemStyle-Width="200" />
                 <asp:BoundField HeaderText="Health Code" ItemStyle-Width="300" />
                 <asp:BoundField HeaderText="Balance" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Principle Default" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Interest Default" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Other Default" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Gross Assets" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Provision" ItemStyle-Width="300" />
                <asp:BoundField HeaderText="Net Assets" ItemStyle-Width="300" />
             </Columns>
         </asp:GridView>
     </div>

        <div id="dialogInspection" style="display: none;">
            <asp:GridView ID="GridViewInsp" autocomplete="off" runat="server" Width="100%" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false" PageSize="5" AllowPaging="true">
                <Columns>
                    <asp:BoundField HeaderText="Loanee Code" ItemStyle-Width="200" />
                    <asp:BoundField HeaderText="Loanee Name" ItemStyle-Width="200" />
                    <asp:BoundField HeaderText="Health Code" ItemStyle-Width="300" />
                    <asp:BoundField HeaderText="Year & Month" ItemStyle-Width="300" />
                    <asp:BoundField HeaderText="Balance" ItemStyle-Width="300" />
                    <asp:BoundField HeaderText="Principle Default" ItemStyle-Width="300" />
                    <asp:BoundField HeaderText="Interest Default" ItemStyle-Width="300" />
                    <asp:BoundField HeaderText="Other Default" ItemStyle-Width="300" />
                </Columns>
            </asp:GridView>

            <table>
                <tr>
                    <td align="right" style="font-size: 12px;color:navy;font-weight:bold;">Inspection By&nbsp;<span class="require" >*&nbsp;</span><span>:</span>  </td>
                    <td ">
                        <asp:TextBox ID="txtInspectionBy" autocomplete="off" ReadOnly="true" ForeColor="Green" Font-Bold="true" Width="220px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                    </td>
                    <td align="right" style="font-size: 12px;color:navy;font-weight:bold;">&nbsp;&nbsp;Inspection Date&nbsp;<span class="require">*&nbsp;</span><span>:</span>  </td>
                    <td ">
                        <asp:TextBox ID="txtInspectionDate" autocomplete="off" ReadOnly="false" Font-Bold="true" MaxLength="10" placeholder="Inspection Date" Width="90px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                    </td>
                    
                    <td>
                        <asp:Button ID ="cmdSave" autocomplete="off" runat="server" ClientIDMode="Static" Text="Save" Width="50px" Height="22px" CssClass="save-button DefaultButton"/>
                        
                    </td>
                    <td>
                        <asp:Button ID ="cmdCancel" autocomplete="off" runat="server" ClientIDMode="Static" Text="Cancel" Width="60px" Height="22px" CssClass="save-button DefaultButton"/>
                    </td>
                    <td>
                        <asp:Button ID ="cmdClose" autocomplete="off" runat="server" ClientIDMode="Static" Text="Close" Width="50px" Height="22px" CssClass="save-button DefaultButton"/>
                    </td>
                    <td>
                        <asp:Button ID="cmdpermission" autocomplete="off" runat="server" Height="20px" Width="110px" Text="User Permission" ClientIDMode="Static" CssClass="save-button DefaultButton"></asp:Button>
                    </td>
                </tr>
                 </table>
             <table>
                <tr>
                    <td align="right" style="font-size: 12px;color:navy;font-weight:bold;">Inspection Heading&nbsp;<span class="require" >*&nbsp;</span><span>:</span>  </td>
                    <td>
                        <asp:TextBox ID="txtInspHeading" autocomplete="off" runat="server" ClientIDMode="Static" MaxLength="200" Width="700px" CssClass="inputbox2"></asp:TextBox>
                    </td>
                    <td align="right" style="font-size: 12px; color: navy; font-weight: bold;">&nbsp;&nbsp;Rrmarks <span class="require"></span><span></span>

                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <asp:TextBox ID="txtRemarks" autocomplete="off" Height="300px" Width="99%" Font-Size="13px" runat="server" ClientIDMode="Static" TextMode="MultiLine"></asp:TextBox>
                    <asp:TextBox ID="txtInspectionID" autocomplete="off" Width="10" runat="server" ClientIDMode="Static" Style="display: none;"></asp:TextBox>
                    <asp:TextBox ID="txtLoaneeCode" autocomplete="off" Width="10" runat="server" ClientIDMode="Static" Style="display: none;"></asp:TextBox>
                </tr>
                <asp:GridView ID="GridViewInspRec" autocomplete="off" runat="server" Width="100%" Style="border: 1px solid #59bdcc" AutoGenerateColumns="false" PageSize="5" AllowPaging="true">
                <Columns>
                    <asp:BoundField HeaderText="Inspection Date" ItemStyle-Width="100" />
                    <asp:BoundField HeaderText="Sr. No." ItemStyle-Width="100" />
                    <asp:BoundField HeaderText="Remarks" ItemStyle-Width="900" />
                    <asp:BoundField HeaderText="User Name" ItemStyle-Width="900" />
                </Columns>
            </asp:GridView>
            </table>
        </div>



        <br />
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
           <%-- <tr>
                <td align="left" style="font-size: 15px; color: navy; font-weight: bold;">Report Name&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td style="width: 50px;">
                    <asp:DropDownList ID="ddlReportName" runat="server" Width="200px" Height="25px" CssClass="inputbox2" ForeColor="#18588a">
                        <asp:ListItem Value="0">(Select Report Name)</asp:ListItem>
                        <asp:ListItem Value="RR">Recovery Report</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>   --%>
           
            <tr>
                <td align="right" style="font-size: 15px;color:navy;font-weight:bold;">Year&Month&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>
                <td ">
                    <asp:TextBox ID="txtFrmDate" autocomplete="off" ReadOnly="false" MaxLength="6" placeholder="YYYYYMM" Width="120px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                </td>
                <%--<td align="right" style="font-size: 15px;color:navy;font-weight:bold;">To Year&Month&nbsp;&nbsp;<span class="require">*&nbsp;&nbsp;</span><span>:</span>  </td>--%>
                <%--<td>
                    <asp:TextBox ID="txtToDate" ReadOnly="false" MaxLength="6" placeholder="YYYYYMM" Width="120px" ClientIDMode="Static" runat="server" CssClass="inputbox2" onkeypress="return isNumber(event)"></asp:TextBox>
                </td>--%>
                <td>
                    <asp:Button ID="btnGenerateReport" autocomplete="off" runat="server"  Text="Show" Width="100px" CssClass="save-button DefaultButton" OnClientClick="FunNPASummary();return false" />
                    <asp:Button ID="btnCancel" autocomplete="off" runat="server" Text="Cancel " Width="100px" CssClass="save-button" OnClick="btnCancel_Click" />
                    <asp:Button ID="btnPrint" autocomplete="off" runat="server" Text="Print" Width="100px" CssClass="save-button DefaultButton" />
                </td>
            </tr>
            <tr style="width:100%">
                <td></td>
                <td></td>
            </tr>
        </table>

        <table>
            <tr style="width: 100%">
                <td></td>
                <td></td>
            </tr>    
            <tr>
                <%--<td>
                    <asp:Button ID="btnGenerateReport" runat="server"  Text="Show" Width="100px" CssClass="save-button DefaultButton" OnClientClick="FunNPASummary();return false" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel " Width="100px" CssClass="save-button" OnClick="btnCancel_Click" />
                    <asp:Button ID="btnPrint" runat="server" Text="Print" Width="100px" CssClass="save-button DefaultButton" />
                </td>--%>
            </tr>
        </table>
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <div id="DialogAllPartNo">
                        <asp:GridView ID="GridNPASummary" autocomplete="off" runat="server" AutoGenerateColumns="false" 
                            PageSize="5" Width="100%" Style="border: 1px solid #59bdcc" AllowPaging="true">
                            <Columns>
                                <asp:BoundField HeaderText="BranchID" ItemStyle-Width="10" />
                                <asp:BoundField HeaderText="Branch" ItemStyle-Width="200" />
                                <asp:BoundField HeaderText="A" ItemStyle-Width="400" />
                                <asp:BoundField HeaderText="B" ItemStyle-Width="400" />
                                <asp:BoundField HeaderText="C" ItemStyle-Width="400" />
                                <asp:BoundField HeaderText="D1" ItemStyle-Width="400" />
                                <asp:BoundField HeaderText="D2" ItemStyle-Width="400" />
                                <asp:BoundField HeaderText="D3" ItemStyle-Width="400" />
                                <asp:BoundField HeaderText="LA" ItemStyle-Width="400" />
                                <asp:BoundField HeaderText="Total" ItemStyle-Width="500" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
