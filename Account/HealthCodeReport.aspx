﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="HealthCodeReport.aspx.cs" Inherits="HealthCodeReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script type="text/javascript" src="js/SubVoucherPrint.js?v=2"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            SearchHC();
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });

            $('#txtYearmonth').attr({ maxLength: 6 });

            $("#btnPrint").click(function (event) {
                if ($("#txtYearmonth").val() == '') {
                    alert("Please enter Year Month");
                    $("#txtYearmonth").focus();
                    return false;
                }
                ShowReport();
            });
            $("#txtHealthCode").on('click', function (ev) {
                if ($("#txtYearmonth").val() == '') {
                    SearchHC();
                } else { SearchHC(); }
            });
            
        });

        function ShowReport() {
            $(".loading-overlay").show();
            var FormName = '';
            var ReportName = '';
            var ReportType = '';

            FormName = "HealthCodeReport.aspx";
            if ($("#ddlReportName").val() == 'D') {
                ReportName = "HealthReportDetail";
                ReportType = "Health Code Report Detail";
            }
            else {
                ReportName = "HealthReport Summary";
                ReportType = "Health Code Report Summary";
            }
            var YearMonth = $('#txtYearmonth').val();
            var ChkChanged = $('#chkChangedOnly').is(":checked") ? 'Y' : 'N';
            if (hdnHC == '') { alert('Please Select Health Code Status'); $('#txtHealthCode').focus; return false;}
            var E = '';
            E = "{FormName:'" + FormName + "', ReportName:'" + ReportName + "', ReportType:'" + ReportType + "', YearMonth:'" + YearMonth + "', ChkChanged:'" + ChkChanged + "', HC:'" + $("#txtHealthCode").val().trim() + "', PWR:'" + $("#ddlPwrStatus").val() + "'}";
            //alert(E);
            $.ajax({
                type: "POST",
                url: pageUrl + '/SetReportValue',
                data: E,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var show = response.d;

                    if (show == "OK") {
                        window.open("ReportView.aspx?E=Y");
                    }

                },
                error: function (response) {
                    var responseText;
                    responseText = JSON.parse(response.responseText);
                    alert("Error : " + responseText.Message);
                    $(".loading-overlay").hide();
                },
                failure: function (response) {
                    alert(response.d);

                    $(".loading-overlay").hide();
                }
            });
        }

        var hdnHC = '';

        function SearchHC() {
            var YrMonth = "";
            if ($("#txtYearmonth").val() != '') {
                YrMonth = $("#txtYearmonth").val();
            } else { YrMonth = "0"; }
            $(".autocompleteHC").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "HealthCodeReport.aspx/HCAutoCompleteData",
                        data: "{YrMonth:'" + YrMonth + "', 'txtHealthCode':'" + document.getElementById('txtHealthCode').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('|')[0],
                                    val: item.split('|')[1]
                                }
                            }))
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (e, i) {
                    hdnHC = i.item.val;//$("#hdnHC").val(i.item.val);
                },
                minLength: 0
            }).click(function () {
                $(this).autocomplete('search', ($(this).val()));
            });
        }
    </script>

    <style type="text/css">
        .ui-autocomplete {
            height: 200px;
            /*width: 210px;*/
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #cbc7c7;
            font-size: 13px;
            font-weight: normal;
            color: #242424;
            background: #fff;
            -moz-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            -webkit-box-shadow: inset 0.9px 1px 3px #e4e4e4;
            box-shadow: inset 0.9px 1px 3px #e4e4e4;
            padding: 5px;
            font-family: Segoe UI, Lucida Grande, Arial, Helvetica, sans-serif;
            margin: 3px 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Health Code Report</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td colspan="6">
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td style="height: 15px;" align="left">Health Code Report</td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" autogeneraterows="False"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">

                                <tr>
                                    <td>
                                        <div id="searchYM" style="width: 100%;">
                                            <table align="center" width="100%">

                                                <tr>
                                                    <td class="labelCaption" >Year Month &nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td class="labelCaption">
                                                        <asp:TextBox autocomplete="off" ID="txtYearmonth" PlaceHolder="YYYYMM" Width="100px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                    <td class="labelCaption">Report Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>

                                                    <td class="labelCaption">
                                                        <asp:DropDownList autocomplete="off" ID="ddlReportName" Font-Bold="true" Width="180px" Height="25px" runat="server" CssClass="inputbox2" ForeColor="#18588a">
                                                            <asp:ListItem Text="Health Report Detail" Value="D"></asp:ListItem>
                                                            <asp:ListItem Text="HealthReport Summary" Value="S"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="labelCaption">Health Code Status&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td class="labelCaption">
                                                        <asp:TextBox ID="txtHealthCode" autocomplete="off" Width="100px" ClientIDMode="Static" runat="server" CssClass="inputbox2 autocompleteHC"></asp:TextBox>
                                                    </td>
                                                    <td class="labelCaption">PWR Status&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td class="labelCaption">
                                                        <asp:DropDownList ID="ddlPwrStatus" autocomplete="off" ClientIDMode="Static" runat="server" CssClass="textbox" Height="25" Width="178px"
                                                            DataTextField="PWR_STATUS_NAME" DataValueField="PWR_STATUS_CODE" DataSource='<%# PopulatePWR() %>' AppendDataBoundItems="true">
                                                            <%--<asp:ListItem Text="Select PWR Status" Value=""></asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chkChangedOnly" autocomplete="off" class="labelCaption" runat="server" Font-Size="small" Text="Changed Health Code." />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="6">

                                        <div style="float: right; margin-left: 200px;">
                                            <asp:Button ID="btnPrint" runat="server" Text="Show" CommandName="print" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" />

                                            <asp:Button ID="cmdCancelSearch" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button" OnClick="cmdCancelSearch_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>

                    </asp:FormView>
                </td>
            </tr>

        </table>
    </div>
</asp:Content>
