﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BondTypeSummaryReport.aspx.cs" Inherits="BondTypeSummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- <script type="text/javascript" src="js/VoucherPrint.js?v=3"></script>--%>
    <script type="text/javascript" src="js/BondTypeSummaryReport.js"></script>

    <style type="text/css">
        .textbox {
            border: 1px solid #c4c4c4;
            height: 15px;
            width: 210px;
            font-size: 13px;
            padding: 4px 4px 4px 4px;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            box-shadow: 0px 0px 8px #d9d9d9;
            -moz-box-shadow: 0px 0px 8px #d9d9d9;
            -webkit-box-shadow: 0px 0px 8px #d9d9d9;
        }

            .textbox:focus {
                outline: none;
                border: 1px solid #7bc1f7;
                box-shadow: 0px 0px 8px #7bc1f7;
                -moz-box-shadow: 0px 0px 8px #7bc1f7;
                -webkit-box-shadow: 0px 0px 8px #7bc1f7;
            }

        .hiddencol {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Bond Type Wise Summary Report & Bond Interest Annex</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td colspan="6">
                    <%--<table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td style="height: 15px;" align="left">Voucher Report Search</td>
                        </tr>
                    </table>--%>
                </td>
            </tr>
            <tr>
                <td>
                    <%-- <asp:formview id="dv" runat="server" width="99%" cellpadding="4" autogeneraterows="False"
                        defaultmode="Insert" horizontalalign="Center" gridlines="None">
                        <InsertItemTemplate>--%>
                    <table align="center" cellpadding="5" width="100%">

                        <tr>
                            <td>
                                <div id="searchYM" style="width: 100%;">
                                    <table align="center" width="100%">
                                        <tr>
                                            <td align="center" colspan="9"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="9" class="labelCaption">
                                                <%-- <hr class="borderStyle" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="labelCaption">Report Name&nbsp;&nbsp;<span class="require"></span> </td>
                                            <td class="labelCaption">:</td>
                                            <td align="left">
                                                <asp:DropDownList autocomplete="off" ID="ddlReportName" runat="server" ClientIDMode="Static" CssClass="textbox" Width="160px" Height="25px" ForeColor="#18588a">                                                  
                                                    <asp:ListItem Text="Bond Type Summary" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Bond Interest Annex" Value="2"></asp:ListItem>                                                   
                                                </asp:DropDownList>
                                            </td>
                                            <td class="labelCaption"> <div id="divdmy">As on Date</div><div id="divmY" style="display:none;">MonthYear</div> &nbsp;&nbsp;<span class="require"></span> </td>
                                            <td class="labelCaption">:</td>
                                            <td align="left">
                                                <div id="divtxtdmy" ><asp:TextBox ID="txtDate" autocomplete="off" Width="140" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox></div>
                                                <div id="divtxtmy" style="display:none"> <asp:TextBox ID="txtMonthYear" autocomplete="off" Width="140" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox></div>                                                
                                            </td>
                                            <td class="labelCaption"><div id="divXYZ" style="display:none;">Sector ID</div><div id="divBndType">Bond Type : </div>&nbsp;&nbsp;<span class="require"></span> </td>
                                           <%-- <td class="labelCaption">:</td>--%>
                                            <td align="left">
                                                <div id="divddlBond">
                                                <asp:DropDownList ID="ddlBondType" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="textbox" Width="150px" Height="25px" ForeColor="#18588a">
                                                    <asp:ListItem Text="--Select Bond Type--" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="All" Value="A"></asp:ListItem>
                                                    <asp:ListItem Text="SLR" Value="S"></asp:ListItem>
                                                    <asp:ListItem Text="NON-SLR" Value="N"></asp:ListItem>
                                                </asp:DropDownList>
                                                    </div>
                                                <div id="divtxtSecID" style="display:none;"> <asp:TextBox ID="txtSecID" autocomplete="off" Visible="false" Width="140" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox></div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td id="v4" align="center" colspan="9"></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="6">

                                <div style="float: left;padding-left:200px; margin-left: 200px;">
                                    <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="search" autocomplete="off"
                                        Width="100px" CssClass="save-button  DefaultButton" OnClientClick='javascript: return beforeSave()' />

                                     &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;

                                    <asp:Button ID="btnPrint" runat="server" Text="Print" CommandName="print" Enabled="false" autocomplete="off"
                                        Width="100px" CssClass="save-button DefaultButton" />

                                    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;

                                    <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                        Width="100px" CssClass="save-button" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <%-- </InsertItemTemplate>
                    </asp:formview>--%>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption" >
                    <hr class="borderStyle" />
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <div id="divGridSummary"  style="overflow-y: scroll; max-height: 300px; width: 100%; background-color: white;" class="headingCaption">
                        <asp:GridView ID="GridSummary" autocomplete="off" runat="server" ClientIDMode="Static" Width="100%" AutoGenerateColumns="false"
                          HeaderStyle-BackColor="#0066ff"   clientID="GridClient"  CssClass="Grid">                            
                            <Columns>
                                 <asp:BoundField HeaderText="Bond Name" ItemStyle-Width="30" />
                                <asp:BoundField HeaderText="ISIN NO." ItemStyle-Width="30" />
                                <asp:BoundField HeaderText="Amount" ItemStyle-Width="30" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>

