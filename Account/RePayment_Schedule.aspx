﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="RePayment_Schedule.aspx.cs" Inherits="RePayment_Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/jscript" src="js/RePayment_Schedule.js"></script>
    <style type="text/css">
        .hovertable {
            font-family: verdana,arial,sans-serif;
            font-size: 11px;
            color: #333333;
            border-width: 1px;
            border-color: #999999;
            border-collapse: collapse;
        }

            .hovertable th {
                background-color: #c3dde0;
                padding: 5px;
               border:1px solid #999999;
            }

            .hovertable tr {
                background-color: #c3dde0;
            }

           .hovertable tr:hover {
                    background-color: #a9c6c9;
                }

            .hovertable td {
                border-width: 1px;
                padding: 4px;
                border-style: solid;
                border-color: #a9c6c9;
            }
            .hovertable tbody {
                max-height:200px;
                overflow:auto;
            }

        .button {
            position: absolute;
        }
        .auto-style1 {
            font-family: Segoe UI;
            font-size: 12px;
            color: Navy;
            text-align: left;
        }
        .auto-style2 {
            font-family: Segoe UI;
            font-size: 12px;
            color: Navy;
            text-align: left;
        }
           .myButton1 {
            -moz-box-shadow: inset 0px 39px 0px -24px #6496e5;
            -webkit-box-shadow: inset 0px 39px 0px -24px #6496e5;
            box-shadow: inset 0px 39px 0px -24px #6496e5;
            background-color: #6496e5;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            border: 1px solid #6496e5;
            display: inline-block;
            cursor: pointer;
            color: #ffffff;
            font-family: "Lucida Bright", Georgia, serif;
            font-size: 11px;
            font-weight:bolder;
            padding: 6px 15px;
            text-decoration: none;
            text-shadow: 0px 1px 0px #b23e35;
        }

            .myButton1:hover {
                -moz-box-shadow: inset 0px 50px 0px -24px #b23e35;
            -webkit-box-shadow: inset 0px 50px 0px -24px #b23e35;
            box-shadow: inset 0px 50px 0px -28px #b23e35;
            background-color: #b23e35;
            }

            .myButton1:active {
                position: relative;
                top: 1px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
        <div class="loading-overlay">
        <div class="loadwrapper">
            <div class="ajax-loader-outer">
                Loading...
            </div>
        </div>
    </div>
    <table class="headingCaption" width="98%" align="center">
       
        <tr>
            <td style="padding:5px">RePayment Schedule</td>
        </tr>
    </table>
    <table width="98%" class="borderStyle" align="center">
        <tr>
            <td>
                <table width="100%" align="center">
                    <tr>
                        <td style="padding: 5px; width: 200px" class="labelCaption" colspan="2">Loanee Name &nbsp;&nbsp;<span class="require">*</span> </td>
                        <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                        <td style="padding: 5px;" align="left" colspan="4">
                            <asp:TextBox ID="txtLoaneeName" autocomplete="off" MaxLength="100" Width="580px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                        </td>
                        <td style="padding-right: 30px"></td>
                    </tr>
                </table>

                <div>
                    <table width="100%" align="center">
                        <tr>
                            <td colspan="7" class="labelCaption">
                                <hr class="borderStyle" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <table width="100%" align="center">
                        <tr>

                            <td style="padding: 5px; " class="auto-style1">Sanction Amount&nbsp;<span class="require">*</span> </td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:TextBox ID="txtSanctionAmount" autocomplete="off" MaxLength="100" Width="200" style="text-align:right" ClientIDMode="Static" runat="server" CssClass="inputbox2 Required"></asp:TextBox>
                            </td>
                            <td class="auto-style2">&nbsp;Repayment Term  &nbsp;<span class="require">*</span> </td>
                            <td class="labelCaption">:</td>
                            <td align="left">
                                <%--<asp:TextBox runat="server" ID="txtRepaymentTerm" ClientIDMode="Static" CssClass="inputbox2"></asp:TextBox>--%>
                                <asp:DropDownList ID="ddlRepaymentTerm" autocomplete="off" Width="209px" Height="25px" CssClass="inputbox2 Required" ForeColor="#18588a" runat="server">
                                    <asp:ListItem Text="(Select Repayment Term)" Value=""></asp:ListItem>
                                    <asp:ListItem Text="Monthly" Value="M"></asp:ListItem>
                                    <asp:ListItem Text="Quarterly" Value="Q"></asp:ListItem>
                                    <asp:ListItem Text="Half-yearly" Value="H"></asp:ListItem>
                                    <asp:ListItem Text="Yearly" Value="Y"></asp:ListItem>
                                    <asp:ListItem Text="Other" Value="O"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                   
                            <td style="padding: 5px; " class="auto-style1">First Disbursed Date  &nbsp;<span class="require">*</span> </td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:TextBox ID="txtFirstDisburseDate" autocomplete="off" MaxLength="100" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2 Required datepicker"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                                     <td class="labelCaption">Repayment Start Date</td>
                            <td class="labelCaption">:</td>
                            <td align="left">
                                <asp:TextBox runat="server" ID="txtRepStaartDt" autocomplete="off" ClientIDMode="Static" Width="200px" CssClass="inputbox2 Required"></asp:TextBox>
                                
                            </td>
                            <td style="padding: 5px; width:140px" class="auto-style2">Moratorium Period In Month &nbsp;<span class="require">*</span> </td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:TextBox ID="txtMoratoriumPeriod" autocomplete="off" MaxLength="100" Width="200" Enabled="false" ClientIDMode="Static" runat="server" CssClass="inputbox2 Required" ></asp:TextBox>
                            </td>
                            <td style="padding: 5px; width: 100px" class="labelCaption">Interest Rate  &nbsp;<span class="require">*</span> </td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 0px;" align="left">
                                <asp:TextBox ID="txtInterestRate" autocomplete="off" MaxLength="100" Width="200px" style="text-align:right" ClientIDMode="Static" runat="server" CssClass="inputbox2 allownumericwithdecimal Required"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px; " class="auto-style1">Rebate Rate  &nbsp;<span class="require">*</span> </td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:TextBox ID="txtRebateRate" autocomplete="off" MaxLength="100" Width="200" ClientIDMode="Static" style="text-align:right" runat="server" CssClass="inputbox2 allownumericwithdecimal Required"></asp:TextBox>
                            </td>
                            <td style="padding: 5px; " class="auto-style2">Penalty Rate  &nbsp;<span class="require">*</span> </td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 5px;" align="left">
                                <asp:TextBox ID="txtPenaltyRate" autocomplete="off" MaxLength="100" Width="200" style="text-align:right" ClientIDMode="Static" runat="server" CssClass="inputbox2 allownumericwithdecimal Required"></asp:TextBox>
                            </td>

                            <td style="padding: 5px; width: 100px" class="labelCaption">Effective From  &nbsp;<span class="require">*</span> </td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td align="left">
                                <asp:TextBox runat="server" ID="txtEffectiveFrom" autocomplete="off" ClientIDMode="Static" Width="200px" CssClass="inputbox2 datepicker1 Required"></asp:TextBox>
                                
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <table width="100%" align="center">
                        <tr>
                            <td colspan="7" class="labelCaption">
                                <hr class="borderStyle" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <table width="100%">
                        <tr>
                          <%--  <td style="padding: 5px; width: 100px" class="labelCaption">InstSrl   &nbsp;<span class="require"></span> </td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 5px;" align="left">
                               <%-- <asp:TextBox ID="InstSrl" MaxLength="100" Width="100" ClientIDMode="Static" runat="server" CssClass="inputbox2 clr "></asp:TextBox>--%
                                
                            </td>--%>

                            <td style="padding: 5px; width: 120px" class="labelCaption">No. Of Instalment</td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 5px;" width="217px" align="left">
                                <asp:TextBox ID="NoOfsInst" autocomplete="off" MaxLength="8" Width="200" ClientIDMode="Static" runat="server" CssClass="inputbox2 clr allownumericwithoutdecimal"></asp:TextBox>
                            </td>
                            <td style="padding: 5px; width: 142px" class="labelCaption">Instalment Amount</td>
                            <td style="padding: 5px; width: 5px" class="labelCaption">:</td>
                            <td style="padding: 5px;" align="left" width="180">
                                <asp:TextBox ID="InstAmt" autocomplete="off" MaxLength="15" Width="100%" ClientIDMode="Static" runat="server" style="text-align:right" CssClass="inputbox2 clr allownumericwithdecimal"></asp:TextBox>

                            </td>
                            <td>
                                <asp:Button ID="btnAdd" runat="server" autocomplete="off" Text="Add" CommandName="Add" Height="30px" Width="100px" CssClass="save-button DefaultButton" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="8">
                                <div>
                                    <table id="tblRepayment" autocomplete="off" runat="server" clientidmode="Static" class="hovertable" style="width:75%">
                                        <tr>
                                            <th autocomplete="off" runat="server" clientidmode="Static" style="width: 9%">Sr. No.</th>
                                            <th autocomplete="off" runat="server" clientidmode="Static" style="width: 37%;text-align:right">Number Of Instalment</th>
                                            <th autocomplete="off" runat="server" clientidmode="Static" style="width: 37%;text-align:right">Instalment Amount</th>
                                            <th autocomplete="off" runat="server" clientidmode="Static" style="width: 37%;text-align:right">Total Amount</th>
                                            <th autocomplete="off" runat="server" clientidmode="Static" style="width: 10%">Edit</th>
                                            <th autocomplete="off" runat="server" clientidmode="Static" style="width: 10%">Delete</th>


                                            
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <div align="right">
                    <table>
                        <tr align="right">
                            <td align="right">
                                <div align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CommandName="Add" ClientIDMode="Static" autocomplete="off"
                                        Width="100px" Height="30px" CssClass="save-button DefaultButton" />

                                    <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" ClientIDMode="Static" autocomplete="off"
                                        Width="100px" Height="30px" CssClass="save-button DefaultButton" />
                                </div>
                            </td>
                            <td style="width:10%">

                            </td>
                        </tr>
                    </table>
                </div>
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

