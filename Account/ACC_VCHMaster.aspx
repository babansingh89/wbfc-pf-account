﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="ACC_VCHMaster.aspx.cs" Inherits="ACC_VCHMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Gridstyle.css" rel="stylesheet" />
    <link href="css/GridTableStyle.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="js/Voucher.js?v=7"></script>
          
    <script type="text/javascript">  
        $(document).ready(function () {
            $("input").focus(function () {
                $(this).css("background-color", "#cccccc");
            });     
            $("input").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

            $("select").focus(function () {
                $(this).css("background-color", "#cccccc");
            });

            $("select").blur(function () {
                $(this).css("background-color", "#ffffff");
            });

        });
        window.onkeydown = function (event) {
            if (event.keyCode == 27) {
                //console.log('escape pressed');
                $("#dialogSearchGLSL").hide();
            }
        };

        //$(document).ready(function () {
        //    $("#txtGLSLDescSearch").click(function () {
        //    $("#dialogSearchGLSL").dialog({ keyboard: true });
        //    });
        //    $("#txtGLSLDescSearch").click(function () {
        //        $("#dialogSearchGLSL").dialog({ keyboard: false });
        //    });
        //});

      
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>VOUCHER MASTER</td>
            </tr>
        </table>
        <br />
        <div>
            <div class="loading-overlay">
                <div class="loadwrapper">
                    <div class="ajax-loader-outer">Loading...</div>
                </div>
            </div>
        </div>
        <div id="dialog-confirm" style="display:none;" >
          <p ><span class="ui-icon ui-icon-alert"  style="float:left; margin:0 7px 20px 0;"></span><p id="spanTxt" ></p></p>
            
        </div>
        <div id="dialog-report" style="display:none;" >
          <p ><p id="spanTxtRep" ></p></p>
            <div style="padding:10px;">
                <div style="float:left;">
                <asp:RadioButton ID="rdRep1" autocomplete="off" Text="&nbsp;&nbsp;Voucher Report" value="N" CssClass="rdRep" Checked="true" GroupName="rdRep" ClientIDMode="Static" runat="server" />
                <asp:RadioButton ID="rdRep2" autocomplete="off" Text="&nbsp;&nbsp;Exchange Advise" value="C" CssClass="rdRep"  GroupName="rdRep"  ClientIDMode="Static" runat="server" />                 
                </div>
                <div><asp:Button ID="btnRep" autocomplete="off" style="padding-left:20px;" Text="Show Report" CommandName="Report" runat="server"  Width="120px" CssClass="save-button DefaultButton" /></div>   
            </div>
        </div>
        <div id="dialogNaration" style="display:none;" >
          <div><asp:TextBox ID="txtNarration" runat="server" ClientIDMode="Static" autocomplete="off" Text="" style="text-transform:uppercase;" TextMode="MultiLine" Width="260px" Height="130px" ></asp:TextBox></div>
        </div>

        <div id="dialogInstrumentType" style="display:none;" >
            <table width="100%" cellpadding="5" style="border:solid 1px lightblue; padding-bottom:10px;" cellspacing="0">
                    <tr>
                        <td>
                        <table align="center" runat="server" autocomplete="off" id="tblInst" style="border:solid 1px lightblue;" class="tblInst" cellpadding="5" width="98%">
                            
                            <tr>
                                <td class="labelCaption" style="border:solid 1px lightblue;" ></td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Type&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Slip No&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Number&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Date&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Amount&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Drawee Bank/Branch&nbsp;&nbsp;</td>
                                <%--<td class="labelCaption" style="border:solid 1px lightblue;" >&nbsp;&nbsp;</td>--%>
                                <td class="labelCaption" style="border:solid 1px lightblue;" ></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="border:solid 1px lightblue;">
                                    <asp:DropDownList ID="ddlInstType" autocomplete="off"
                                        runat="server" AppendDataBoundItems="true" Width="150px" ClientIDMode="Static" CssClass="citybox2 required ">
                                        <asp:ListItem Text="Please Inst Type" Value=""></asp:ListItem>                                        
                                    </asp:DropDownList></td>
                                <td style="border:solid 1px lightblue;">
                                    <asp:TextBox ID="txtSlipNo" ClientIDMode="Static" autocomplete="off" style="text-transform:uppercase;" Width="90px" runat="server" CssClass="required inputbox2 hori"></asp:TextBox>
                                </td>
                                <td style="border:solid 1px lightblue;">
                                    <asp:TextBox ID="txtInstNo" ClientIDMode="Static" autocomplete="off" style="text-transform:uppercase;" Width="90px" runat="server" CssClass=" required inputbox2 hori"></asp:TextBox>
                                </td>
                                <td style="border:solid 1px lightblue;">
                                    <asp:TextBox ID="txtInstDate" ClientIDMode="Static" autocomplete="off" Width="90px" runat="server" CssClass="dpDate required inputbox2 hori"></asp:TextBox></td>
                                <td style="border:solid 1px lightblue;">
                                    <asp:TextBox ID="txtInstAmount" ClientIDMode="Static" autocomplete="off" Width="80px" style="text-align:right;" runat="server" CssClass=" required inputbox2 hori"></asp:TextBox>
                                </td>
                                <td style="border:solid 1px lightblue;">
                                    <asp:TextBox ID="txtDBankBR" ClientIDMode="Static" autocomplete="off" Width="180px" runat="server" style="text-transform:uppercase;" CssClass=" required inputbox2 hori"></asp:TextBox>
                                    
                                </td>
                                <td style="border:solid 1px lightblue;">
                                    <asp:Button id="btnInstAdd" autocomplete="off" Text="Add" ClientIDMode="Static" runat="server" Width="70px" Height="25px" CssClass="save-button DefaultButton" /></td>

                            </tr>
                            
                                
                          
                            </table>
                            
                       <%--<table align="center" cellpadding="5" width="90%">
                                <tr><td  colspan="3"  align="right" >
                                <div style="float:right;">
                                <button id="btnAdd" ToolTip="Add Details" onclick='window.bookingPage.AddEnquiryTokenDetails()' class="proceed-button checkout-continue-button DefaultButton">
								        <span class="button-text">ADD </span>
						        </button>
                                    </div></td></tr>
                                </table>--%>
                    
                        </td>
                    </tr>
                </table>
        </div>


        <div id="dialogFA" style="display:none;" >
            <table width="100%" cellpadding="5" style="border:solid 1px lightblue; padding-bottom:10px;" cellspacing="0">
                    <tr>
                        <td>
                        <table align="center" id="tblFA" runat="server" autocomplete="off" style="border:solid 1px lightblue;" class="tblFA" cellpadding="5" width="98%">
                            
                            <tr>
                                <td class="labelCaption" style="border:solid 1px lightblue;" ></td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Asset Description&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Location&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Supplier&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Depreciation Rate(%)&nbsp;&nbsp;</td>
                                <td class="labelCaption" style="border:solid 1px lightblue;" >Value&nbsp;&nbsp;</td>                               
                            </tr>
                            <tr>
                                <td></td>
                                 <td style="border:solid 1px lightblue;">
                                    <asp:TextBox ID="txtFAD" ClientIDMode="Static" autocomplete="off" Width="200px" runat="server" CssClass="required inputbox2 horiFA"></asp:TextBox>
                                </td>
                                <td style="border:solid 1px lightblue;">
                                    <asp:DropDownList ID="ddlLocation" autocomplete="off"
                                        runat="server" AppendDataBoundItems="true" Width="200px" ClientIDMode="Static" CssClass="citybox2 required horiFA">
                                        <asp:ListItem Text="Please Location" Value=""></asp:ListItem>                                        
                                    </asp:DropDownList>

                                </td>
                               <td style="border:solid 1px lightblue;">
                                    <asp:DropDownList ID="ddlSupplier" autocomplete="off" 
                                        runat="server" AppendDataBoundItems="true" Width="200px" ClientIDMode="Static" CssClass="citybox2 required horiFA">
                                        <asp:ListItem Text="Please Supplier" Value=""></asp:ListItem>                                        
                                    </asp:DropDownList>

                                </td>
                                 <td style="border:solid 1px lightblue;">
                                    <asp:TextBox ID="txtDRate" ClientIDMode="Static" autocomplete="off" Width="70px" style="text-align:right;" runat="server" CssClass=" required inputbox2 horiFA"></asp:TextBox>
                                </td>                                
                                <td style="border:solid 1px lightblue;">
                                    <asp:TextBox ID="txtFAValue" ClientIDMode="Static" autocomplete="off" Width="100px" style="text-align:right;" runat="server" CssClass=" required inputbox2 horiFA"></asp:TextBox>
                                </td>                                

                            </tr>
                            
                                
                          
                            </table>
                            
                       <%--<table align="center" cellpadding="5" width="90%">
                                <tr><td  colspan="3"  align="right" >
                                <div style="float:right;">
                                <button id="btnAdd" ToolTip="Add Details" onclick='window.bookingPage.AddEnquiryTokenDetails()' class="proceed-button checkout-continue-button DefaultButton">
								        <span class="button-text">ADD </span>
						        </button>
                                    </div></td></tr>
                                </table>--%>
                    
                        </td>
                    </tr>
                </table>
        </div>

        <div id="dialogSearchGLSL" style="display: none">
            <div style="padding:10px;">
                <div style="float:left;">
                <asp:RadioButton ID="rdName" autocomplete="off" Text="&nbsp;&nbsp;Search By Name" value="N" CssClass="rdGLCL"  GroupName="rdGL" ClientIDMode="Static" runat="server" />
                <asp:RadioButton ID="rdCode" autocomplete="off" Text="&nbsp;&nbsp;Search By Code" value="C" CssClass="rdGLCL" Checked="true" GroupName="rdGL"  ClientIDMode="Static" runat="server" />
                </div>
            </div>
        <div style="padding:10px;"><span id="GLSpan">Search By Description</span>&nbsp;&nbsp;<asp:TextBox ID="txtGLSLDescSearch" style="text-transform:uppercase;" runat="server" CssClass="textposition" MaxLength="150"></asp:TextBox>
            <br /><span style="font-size:12px; color:red;margin-left:40px;" id="spNoData"></span>
        </div>
            <div style="overflow:auto;height:300px;padding-top:40px;">
                    <asp:GridView ID="grdGLSL" clientID="grdGLSL" runat="server" ClientIDMode="Static" autocomplete="off" AutoGenerateColumns="false" CssClass="GlSl">
                        <Columns>
                            <asp:BoundField DataField="OLD_SL_ID" HeaderText="OLD SL ID" ItemStyle-Width="50" />
                            <asp:BoundField DataField="GL_ID" HeaderText="GL ID" ItemStyle-Width="50" />
                            <asp:BoundField DataField="GL_NAME" HeaderText="GL NAME" ItemStyle-Width="100" />
                            <asp:BoundField DataField="GL_TYPE" HeaderText="GL TYPE" ItemStyle-Width="30" />
                            <asp:BoundField DataField="SL_ID" HeaderText="SL ID" ItemStyle-Width="30" />
                        </Columns>
                    </asp:GridView>
            </div>
        </div>

         <div id="dialogSearchSub" style="display: none">
             <div style="padding:10px;">
                <div style="float:left;">
                <asp:RadioButton ID="rdSName" autocomplete="off" Text="&nbsp;&nbsp;Search By Name" value="N" CssClass="rdGLCL" GroupName="rdSGL" ClientIDMode="Static" runat="server" />
                <asp:RadioButton ID="rdSCode" autocomplete="off" Text="&nbsp;&nbsp;Search By Code" value="C" CssClass="rdGLCL" Checked="true" GroupName="rdSGL"  ClientIDMode="Static" runat="server" />
                </div>
            </div>
        <div style="padding:10px;"><span id="SubSpan">Search By Description</span>&nbsp;&nbsp;<asp:TextBox ID="txtSUBDescSearch" runat="server" CssClass="textposition" MaxLength="150"></asp:TextBox>
             <br /><span style="font-size:12px; color:red;margin-left:40px;" id="spSUBNoData"></span>
        </div>
        <div style="overflow:auto;height:300px;">
        <asp:GridView ID="grdSUB" clientID="grdSUB" ClientIDMode="Static" autocomplete="off" runat="server" AutoGenerateColumns="false" CssClass="SUB">
            <Columns>
                <asp:BoundField DataField="OLD_SUB_ID" HeaderText="OLD SUB ID" ItemStyle-Width="50" />                
                <asp:BoundField DataField="SUB_NAME" HeaderText="SUB NAME" ItemStyle-Width="100" />                
                <asp:BoundField DataField="SUB_ID" HeaderText="SUB ID" ItemStyle-Width="30" />
                <asp:BoundField DataField="SUB_TYPE" HeaderText="SUB TYPE" ItemStyle-Width="30" />
            </Columns>
        </asp:GridView>
        </div>
        </div>

        <asp:Literal runat="server" ID="ltlj"></asp:Literal>
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td class="labelCaption">Year Month &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtYearMonth" autocomplete="off" MaxLength="6" ClientIDMode="Static" runat="server" Width="70px" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    <td class="labelCaption">Vouncher Type &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlVchType" AppendDataBoundItems="true" autocomplete="off"  ClientIDMode="Static" Width="115px" runat="server" CssClass="citybox2">
                                            <asp:ListItem Text="Select Voucher" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Normal" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Instalment" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Interest" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="labelCaption">Voucher Date &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <nobr> 
                                        <asp:TextBox ID="txtVoucherDate" autocomplete="off" ClientIDMode="Static" runat="server" Width="80px" CssClass="dpDate inputbox2"></asp:TextBox>
                                        <asp:ImageButton  ID="btnDatePicker" CausesValidation="false" ImageUrl="~/images/date.png" 
                                        AlternateText="Click to show calendar"  CssClass="dpDate calendar" runat="server" />
                                        </nobr>
                                    </td>

                                    <td class="labelCaption">Voucher No &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtVoucherNo" autocomplete="off"  ClientIDMode="Static" runat="server" Width="155px" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                    <td align="left" colspan="3">
                                        <asp:Button ID="btnSearchVch" runat="server" Text="Search Voucher"  CommandName="Search" autocomplete="off"
                                                Width="120px" CssClass="save-button DefaultButton" />
                                    </td>
                                    <td align="left" colspan="3">
                                        <asp:Button ID="btnShowReport" runat="server" Text="Show Report"  CommandName="Report" autocomplete="off"
                                                Width="120px" CssClass="save-button DefaultButton" />
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                        
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <table id="table" width="99%" border="1" cellpadding="1" cellspacing="1" class="test">
                                        <tr>
                                            <td style="background-color: #ced0b7; width: 3%; height: 20px; border-bottom: solid 1px Navy; border-right: solid 1px Navy; padding: 10px; text-align: center; font-weight: bold;font-weight:bold;font-size:12px;" class="headFont"></>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td style="background-color: #ced0b7; width: 10%; height: 20px; border-bottom: solid 1px Navy; border-right: solid 1px Navy; padding: 10px; text-align: center; font-weight: bold;font-weight:bold;font-size:12px;" class="headFont">Code</>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td style="background-color: #ced0b7; width: 45%; height: 20px; border-bottom: solid 1px Navy; border-right: solid 1px Navy; padding: 10px; text-align: center; font-weight: bold;font-weight:bold;font-size:12px;" class="headFont">Description</>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td style="background-color: #ced0b7; width: 10%; height: 20px; border-bottom: solid 1px Navy; border-right: solid 1px Navy; padding: 10px; text-align: center; font-weight: bold;font-weight:bold;font-size:12px;" class="headFont">DR_CR</>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td style="background-color: #ced0b7; width: 10%; height: 20px; border-bottom: solid 1px Navy; border-right: solid 1px Navy; padding: 10px; text-align: right; font-weight: bold;font-weight:bold;font-size:12px;" class="headFont">Debit (Dr.)&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td style="background-color: #ced0b7; width: 10%; height: 20px; border-bottom: solid 1px Navy; border-right: solid 1px Navy; padding: 10px; text-align: right; font-weight: bold;font-weight:bold;font-size:12px;" class="headFont">Credit (Cr.)&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td style="background-color: #ced0b7; width: 3%; height: 20px; border-bottom: solid 1px Navy; border-right: solid 1px Navy; padding: 10px; text-align: center; font-weight: bold;font-weight:bold;font-size:12px;" class="headFont"></>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td style="width: 10%; border-bottom: solid 1px lightyellow;">
                                                <asp:TextBox ID="lblAccOldSlCode" Enabled="false" runat="server" Width="90%" CssClass="inputbox2" ClientIDMode="Static" Text=""></asp:TextBox>
                                                <asp:TextBox ID="lblOldSubCode" Enabled="false" runat="server" Width="90%" CssClass="inputbox2" ClientIDMode="Static" Text=""></asp:TextBox>
                                            </td>
                                            <td style="width: 45%; border-bottom: solid 1px lightyellow">
                                                <asp:TextBox ID="lblAccDesc_0" autocomplete="off" runat="server" ClientIDMode="Static" Text="" CssClass="inputbox2" Width="95%"></asp:TextBox>
                                                <asp:TextBox ID="lblSubAccDesc_0" autocomplete="off" runat="server" ClientIDMode="Static" Text="" CssClass="inputbox2" Width="95%"></asp:TextBox>
                                            </td>
                                            <td style="vertical-align: top;">
                                                <asp:DropDownList ID="ddlDRCR" autocomplete="off" runat="server" ClientIDMode="Static" Width="100px" CssClass="citybox2">
                                                    <asp:ListItem Text="Select" Value="" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="DR" Value="D"></asp:ListItem>
                                                    <asp:ListItem Text="CR" Value="C"></asp:ListItem>
                                                </asp:DropDownList></td>
                                            <%--onclick="DebitChangeValue(this)" onkeydown = "DebitEnterValue(this)"
                             onclick="CreditChangeValue(this)" onkeydown = "CreditEnterValue(this)"
                                            --%>
                                            <td style="vertical-align: top;">
                                                <input id="txtDebit_0" runat="server" clientidmode="Static" autocomplete="off" style="width: 120px; text-align: right;" onkeypress="return isNumberKey(event)" type="text" class="inputbox2" value="" /></td>
                                            <td style="vertical-align: top;">
                                                <input id="txtCredit_0" runat="server" clientidmode="Static" autocomplete="off" style="width: 120px; text-align: right;" onkeypress="return isNumberKey(event)" type="text" class="inputbox2" value="" /></td>
                                            <td style="width: 3%; border-bottom: solid 1px lightyellow;"></td>
                                        </tr>
                                        <table id="table1" width="99%" border="1" cellpadding="1" cellspacing="1">
                                            <tr>
                                                <td style="width: 90.7%; border-bottom: solid 1px lightyellow; text-align: left; font-weight: bold;" colspan="2">
                                                    <asp:Label ID="lblNarration" autocomplete="off" Font-Bold="true" Font-Size="12px" runat="server" ClientIDMode="Static" Text="Narration"></asp:Label>
                                                    <asp:TextBox ID="txtNarrationval" autocomplete="off" style="text-transform:uppercase;" MaxLength="250" ClientIDMode="Static" runat="server" Width="90%" CssClass="inputbox2"></asp:TextBox>
                                                </td>
                                                <td style="text-align: right; width: 5%;">
                                                    <asp:Label ID="lblTotal" autocomplete="off" runat="server" Font-Bold="true" Font-Size="12px" ClientIDMode="Static" Text="Total"></asp:Label>&nbsp;&nbsp;</td>
                                                <td style="font-weight: bold;text-align:right;">
                                                    <input id="txtTotalDebit" runat="server" clientidmode="Static" autocomplete="off" style="width: 125px; text-align: right; color: red; font-weight: bold" type="text" class="inputbox2" value="0" disabled="disabled"/></td> 
                                                <td style="font-weight: bold;text-align:right;">
                                                    <input id="txtTotalCredit" runat="server" clientidmode="Static" autocomplete="off" style="width: 125px; text-align: right; color: red; font-weight: bold" type="text" class="inputbox2" value="0" disabled="disabled"/></td> 
                                                <td style="text-align: right; width: 7%;">
                                            </tr>

                                        </table>
                                    </table>
                                </tr>
                                <tr>
                                    <td colspan="4"  class="labelCaption">
                                        <div style="padding-left:20px;font-weight:bold;" id="dvLoaneeOut">

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div style="float: right; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Save Voucher" CommandName="Add" autocomplete="off"
                                                Width="150px" CssClass="save-button DefaultButton" />
                                            <asp:Button ID="cmdUpdate" style="display:none;" runat="server" Text="Update Voucher" CommandName="Update" autocomplete="off"
                                                Width="150px" CssClass="save-button DefaultButton" />
                                            <asp:Button ID="cmdDelete" style="display:none;" runat="server" Text="Delete Voucher" CommandName="Delete" autocomplete="off"
                                                Width="150px" CssClass="save-button DefaultButton" />                                            
                                            <asp:Button ID="cmdRefresh" runat="server" Text="Refresh" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton"  />
                                    </div>
                                    </td>
                                    
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            
          
        </table>
    </div>
</asp:Content>

