﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using System.Web.Services;
using System.Web.Script.Services;
using DataAccess;
using System.Globalization;
using System.Text;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Data.SqlClient;

public partial class MST_Acc_Loan : System.Web.UI.Page
{
    static DataTable dtLoan;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }
            if (!IsPostBack)
            {

                PopulateGrid();

            }
            HttpContext.Current.Session["LoanDetail"] = null;
            dtLoan = (DataTable)HttpContext.Current.Session["LoanDetail"];
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }    

    protected DataTable drpload(string str)
    {
        try
        {
            if (str == "LoanType")
            {
                DataTable dt = DBHandler.GetResult("Load_LoanType");
                return dt;
            }
            else if (str == "District")
            {
                DataTable dt = DBHandler.GetResult("Load_DistrictAll");
                return dt;
            }
            else if (str == "Industry")
            {
                DataTable dt = DBHandler.GetResult("Load_Industry");
                return dt;
            }
            else if (str == "Constitution")
            {
                DataTable dt = DBHandler.GetResult("Load_Constitution");
                return dt;
            }
            else if (str == "Area")
            {
                DataTable dt = DBHandler.GetResult("Load_Area");
                return dt;
            }
            else if (str == "Sector")
            {
                DataTable dt = DBHandler.GetResult("Load_Sector");
                return dt;
            }  
            else if (str == "Purpose")
            {
                DataTable dt = DBHandler.GetResult("Load_Purpose");
                return dt;
            }
            else if (str == "pwrStatus")
            {
                DataTable dt = DBHandler.GetResult("Load_PWRStatus");
                return dt;
            }
            else if (str == "Consultant")
            {
                DataTable dt = DBHandler.GetResult("Load_Consultant");
                return dt;
            }
            else if (str == "AccStatus")
            {
                DataTable dt = DBHandler.GetResult("Load_AccountStatus");
                return dt;
            }
            else if (str == "Employee")
            {
                DataTable dt = DBHandler.GetResult("Load_EmployeeList");
                return dt;
            }
            else
            {
                DataTable dt = DBHandler.GetResult("Load_EmpSanctionBy");
                return dt;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    } 

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Save_MSTAccLoan(string LoaneeUnqID, string LooneeID, string GLID, string LooneeCode, string LooneeName, string LoanType, string LooneeOriCode,
                                          string OfficeAdd, string OfficeAdd2, string OfficeAdd3, string OfficePin, string OfficeTeleNo, string FactoryAdd, string FactoryAdd2, string FactoryAdd3, string FactoryPin, string FactoryTeleNo,
                                          string ImplementaiotnStatus, string CommOpDate, string District, string Industry, string Constitution, string Area, string Sector, string Purpose,
                                          string ApplicationDate, string ApplicationAmount, string ProjectCost, string SanctionDate, string SanctionAmount, string SanctionCancelDate, string SanctionCancelAmount, string RepaymentTerm, string RepStartDate,
                                          string Pri_Mortgaged, string Pri_Hypothctd, string Pri_Pledged, string CoL_Mortgaged, string CoL_Hypothctd, string CoL_Pledged,
                                          string ChiefPromoter, string PwrStatus, string Product, string Consultant, string ProjectEmployment, string ActualEmployment, string ProjectedImplementionDate, string ActualImplementionDate, string SanctionBy,
                                          string MobileEmailXml, string InspectionDate, string ProcessingOff, string DisburseOff, string RecoOff, string PrePayDate, string PrePayAmount, string PanNo, string GstNo)
    {
        DateTime dtApplicationDate = new DateTime();
        DateTime dtSanctionDate = new DateTime();
        DateTime dtSanctionCancelDate = new DateTime();  
        DateTime dtRepStartDate = new DateTime();
        DateTime dtProjectedImplementionDate = new DateTime();
        DateTime dtActualImplementionDate = new DateTime();
        DateTime dtInspectionDate = new DateTime();
        DateTime dtCommOpDate = new DateTime();
       
        if (ApplicationDate != "")
        {
            dtApplicationDate = DateTime.ParseExact(ApplicationDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        if (SanctionDate != "")
        {
            dtSanctionDate = DateTime.ParseExact(SanctionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        if (SanctionCancelDate != "")
        {
            dtSanctionCancelDate = DateTime.ParseExact(SanctionCancelDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        if (RepStartDate != "")
        {
            dtRepStartDate = DateTime.ParseExact(RepStartDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        if (ProjectedImplementionDate != "")
        {
            dtProjectedImplementionDate = DateTime.ParseExact(ProjectedImplementionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        if (ActualImplementionDate != "")
        {
            dtActualImplementionDate = DateTime.ParseExact(ActualImplementionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        if (InspectionDate != "")
        {
            dtInspectionDate = DateTime.ParseExact(InspectionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        if (CommOpDate != "")
        {
            dtCommOpDate = DateTime.ParseExact(CommOpDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }

        int UserID, SectorID;
        UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
        SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

        string JSonVal = "";

        string Loanee_ID = "";
        string GL_ID = "";
        string Loanee_Unq_ID = "";
        //string ConString = "";
        //ConString = DataAccess.DBHandler.GetConnectionString();    
        SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());
        try
        {
            if (LoaneeUnqID == "")
            {
                //SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());

                SqlCommand cmd = new SqlCommand();
                SqlCommand cmd1 = new SqlCommand();
                SqlCommand cmd2 = new SqlCommand();
                SqlTransaction transaction;
                SqlDataReader reader;
                db.Open();
                transaction = db.BeginTransaction();
                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Insert_MstAccLoan";

                    cmd.Parameters.Add("@LOANEE_CODE", SqlDbType.VarChar).Value = (LooneeCode.Equals("") ? DBNull.Value : (object)LooneeCode);
                    cmd.Parameters.Add("@LOANEE_NAME", SqlDbType.VarChar).Value = (LooneeName.Equals("") ? DBNull.Value : (object)LooneeName);
                    cmd.Parameters.Add("@LOAN_TYPE", SqlDbType.Int).Value = (LoanType.Equals("") ? DBNull.Value : (object)Convert.ToInt32(LoanType));
                    cmd.Parameters.Add("@ORIGINAL_CODE", SqlDbType.VarChar).Value = (LooneeOriCode.Equals("") ? DBNull.Value : (object)LooneeOriCode);

                    cmd.Parameters.Add("@OFF_ADDRESS_1", SqlDbType.VarChar).Value = (OfficeAdd.Equals("") ? DBNull.Value : (object)OfficeAdd);
                    cmd.Parameters.Add("@OFF_ADDRESS_2", SqlDbType.VarChar).Value = (OfficeAdd2.Equals("") ? DBNull.Value : (object)OfficeAdd2);
                    cmd.Parameters.Add("@OFF_ADDRESS_3", SqlDbType.VarChar).Value = (OfficeAdd3.Equals("") ? DBNull.Value : (object)OfficeAdd3);
                    cmd.Parameters.Add("@OFF_PIN_CODE", SqlDbType.VarChar).Value = (OfficePin.Equals("") ? DBNull.Value : (object)OfficePin);
                    cmd.Parameters.Add("@OFF_TEL_NO", SqlDbType.VarChar).Value = (OfficeTeleNo.Equals("") ? DBNull.Value : (object)OfficeTeleNo);
                    cmd.Parameters.Add("@FCY_ADDRESS_1", SqlDbType.VarChar).Value = (FactoryAdd.Equals("") ? DBNull.Value : (object)FactoryAdd);
                    cmd.Parameters.Add("@FCY_ADDRESS_2", SqlDbType.VarChar).Value = (FactoryAdd2.Equals("") ? DBNull.Value : (object)FactoryAdd2);
                    cmd.Parameters.Add("@FCY_ADDRESS_3", SqlDbType.VarChar).Value = (FactoryAdd3.Equals("") ? DBNull.Value : (object)FactoryAdd3);
                    cmd.Parameters.Add("@FCY_PIN_CODE", SqlDbType.VarChar).Value = (FactoryPin.Equals("") ? DBNull.Value : (object)FactoryPin);
                    cmd.Parameters.Add("@FCY_TEL_NO", SqlDbType.VarChar).Value = (FactoryTeleNo.Equals("") ? DBNull.Value : (object)FactoryTeleNo);

                    cmd.Parameters.Add("@IMPLI_STATUS", SqlDbType.VarChar).Value = (ImplementaiotnStatus.Equals("") ? DBNull.Value : (object)ImplementaiotnStatus);
                    cmd.Parameters.Add("@DOCO", SqlDbType.Date).Value = (CommOpDate.Equals("") ? DBNull.Value : (object)dtCommOpDate.ToShortDateString());

                    cmd.Parameters.Add("@DIST_CODE", SqlDbType.Int).Value = (District.Equals("") ? DBNull.Value : (object)Convert.ToInt32(District));
                    cmd.Parameters.Add("@INDUS_CODE_ID", SqlDbType.VarChar).Value = (Industry.Equals("") ? DBNull.Value : (object)Industry);
                    cmd.Parameters.Add("@CONST_CODE_ID", SqlDbType.Int).Value = (Constitution.Equals("") ? DBNull.Value : (object)Convert.ToInt32(Constitution));
                    cmd.Parameters.Add("@AREA_CODE", SqlDbType.VarChar).Value = (Area.Equals("") ? DBNull.Value : (object)Area);
                    cmd.Parameters.Add("@SECTOR_CODE", SqlDbType.Int).Value = (Sector.Equals("") ? DBNull.Value : (object)Convert.ToInt32(Sector));
                    cmd.Parameters.Add("@PURPOSE", SqlDbType.VarChar).Value = (Purpose.Equals("") ? DBNull.Value : (object)Purpose);

                    cmd.Parameters.Add("@APPLN_DT", SqlDbType.Date).Value = (ApplicationDate.Equals("") ? DBNull.Value : (object)dtApplicationDate.ToShortDateString());
                    cmd.Parameters.Add("@APPLN_AMT", SqlDbType.Money).Value = (ApplicationAmount.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(ApplicationAmount));
                    cmd.Parameters.Add("@PROJECT_COST", SqlDbType.Money).Value = (ProjectCost.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(ProjectCost));
                    cmd.Parameters.Add("@SANCT_DATE", SqlDbType.Date).Value = (SanctionDate.Equals("") ? DBNull.Value : (object)dtSanctionDate.ToShortDateString());
                    cmd.Parameters.Add("@SANCT_AMT", SqlDbType.Money).Value = (SanctionAmount.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(SanctionAmount));
                    cmd.Parameters.Add("@SANCAN_DT", SqlDbType.Date).Value = (SanctionCancelDate.Equals("") ? DBNull.Value : (object)dtSanctionCancelDate.ToShortDateString());
                    cmd.Parameters.Add("@SANCAN_AMT", SqlDbType.Money).Value = (SanctionCancelAmount.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(SanctionCancelAmount));
                    cmd.Parameters.Add("@REPAYMENT_TERM", SqlDbType.VarChar).Value = (RepaymentTerm.Equals("") ? DBNull.Value : (object)RepaymentTerm);
                    cmd.Parameters.Add("@REP_ST_DT", SqlDbType.Date).Value = (RepStartDate.Equals("") ? DBNull.Value : (object)dtRepStartDate.ToShortDateString());

                    cmd.Parameters.Add("@MPS_AMT", SqlDbType.Money).Value = (Pri_Mortgaged.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(Pri_Mortgaged));
                    cmd.Parameters.Add("@HPS_AMT", SqlDbType.Money).Value = (Pri_Hypothctd.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(Pri_Hypothctd));
                    cmd.Parameters.Add("@PPS_AMT", SqlDbType.Money).Value = (Pri_Pledged.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(Pri_Pledged));
                    cmd.Parameters.Add("@MCS_AMT", SqlDbType.Money).Value = (CoL_Mortgaged.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(CoL_Mortgaged));
                    cmd.Parameters.Add("@HCS_AMT", SqlDbType.Money).Value = (CoL_Hypothctd.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(CoL_Hypothctd));
                    cmd.Parameters.Add("@PCS_AMT", SqlDbType.Money).Value = (CoL_Pledged.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(CoL_Pledged));

                    cmd.Parameters.Add("@CHIEF_PROMOTER", SqlDbType.VarChar).Value = (ChiefPromoter.Equals("") ? DBNull.Value : (object)ChiefPromoter);
                    cmd.Parameters.Add("@PWR_STATUS_CODE", SqlDbType.VarChar).Value = (PwrStatus.Equals("") ? DBNull.Value : (object)PwrStatus);
                    cmd.Parameters.Add("@PRODUCT", SqlDbType.VarChar).Value = (Product.Equals("") ? DBNull.Value : (object)Product);
                    cmd.Parameters.Add("@CONSULTANT", SqlDbType.VarChar).Value = (Consultant.Equals("") ? DBNull.Value : (object)Consultant);
                    cmd.Parameters.Add("@EMPLOYMENT", SqlDbType.Money).Value = (ProjectEmployment.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(ProjectEmployment));
                    cmd.Parameters.Add("@ACTUAL_EMPLOYMENT", SqlDbType.Money).Value = (ActualEmployment.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(ActualEmployment));
                    cmd.Parameters.Add("@PROJ_COMM_OPERA_DT", SqlDbType.Date).Value = (ProjectedImplementionDate.Equals("") ? DBNull.Value : (object)dtProjectedImplementionDate.ToShortDateString());
                    cmd.Parameters.Add("@ACTU_COMM_OPERA_DT", SqlDbType.Date).Value = (ActualImplementionDate.Equals("") ? DBNull.Value : (object)dtActualImplementionDate.ToShortDateString());
                    cmd.Parameters.Add("@SANCTIONED_BY", SqlDbType.VarChar).Value = (SanctionBy.Equals("") ? DBNull.Value : (object)SanctionBy);

                    //cmd.Parameters.Add("@MOBILE_NO", SqlDbType.VarChar).Value = (MobileNo.Equals("") ? DBNull.Value : (object)MobileNo);
                    //cmd.Parameters.Add("@EMAIL_ID", SqlDbType.VarChar).Value = (EmailID.Equals("") ? DBNull.Value : (object)EmailID);

                    cmd.Parameters.Add("@XMLPhoneAndMail", MobileEmailXml);
                    //MobileEmailXml

                    cmd.Parameters.Add("@INSP_DATE", SqlDbType.Date).Value = (InspectionDate.Equals("") ? DBNull.Value : (object)dtInspectionDate.ToShortDateString());
                    cmd.Parameters.Add("@PO", SqlDbType.Int).Value = (ProcessingOff.Equals("") ? DBNull.Value : (object)Convert.ToInt32(ProcessingOff));
                    cmd.Parameters.Add("@DO", SqlDbType.Int).Value = (DisburseOff.Equals("") ? DBNull.Value : (object)Convert.ToInt32(DisburseOff));
                    cmd.Parameters.Add("@RO", SqlDbType.Int).Value = (RecoOff.Equals("") ? DBNull.Value : (object)Convert.ToInt32(RecoOff));

                    cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(SectorID);
                    cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(UserID);

                    cmd.Parameters.Add("@PrePayDate", SqlDbType.Date).Value = (PrePayDate.Equals("") ? DBNull.Value : (object)Convert.ToDateTime(PrePayDate));
                    cmd.Parameters.Add("@PrePayAmount", SqlDbType.Money).Value = Convert.ToDecimal(PrePayAmount);

                    cmd.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = (PanNo.Equals("") ? DBNull.Value : (object)PanNo);
                    cmd.Parameters.Add("@GstNo", SqlDbType.VarChar).Value = (GstNo.Equals("") ? DBNull.Value : (object)GstNo);

                    cmd.Connection = db;
                    cmd.Transaction = transaction;

                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Loanee_ID = reader["LOANEE_ID"].ToString();
                        GL_ID = reader["GL_ID"].ToString();
                        Loanee_Unq_ID = reader["LOANEE_UNQ_ID"].ToString();
                    }
                    reader.Close();

                    if (HttpContext.Current.Session["EnqID"] != null)
                    {
                        // Update MST_ACC_LOAN set EnqID=@EnqID where LoaneeID=@Loanee_ID
                        int EnqID = Convert.ToInt32(HttpContext.Current.Session["EnqID"]);
                        //DBHandler.Execute("Update_MST_ACC_Loan_EnqIDbyLoaneeID", Loanee_ID, EnqID);

                        cmd2 = new SqlCommand();
                        cmd2.CommandType = CommandType.StoredProcedure;
                        cmd2.CommandText = "Update_MST_ACC_Loan_EnqIDbyLoaneeID";
                        cmd2.Parameters.Add("@LOANEE_UNQ_ID", SqlDbType.BigInt).Value = Convert.ToInt32(Loanee_Unq_ID);
                        cmd2.Parameters.Add("@EnqID", SqlDbType.BigInt).Value = EnqID;
                        cmd2.Connection = db;
                        cmd2.Transaction = transaction;
                        cmd2.ExecuteNonQuery();
                    }

                    // ---------------------------------------------------------------------- Detail Part -------------------------------------------------------------

                    DataTable dtdetail = (DataTable)HttpContext.Current.Session["LoanDetail"];
                    if (dtdetail != null)           /// if there dont have any data into detail.
                    {
                        if (dtdetail.Rows.Count > 0)
                        {
                            DateTime dtFirstDisbDate = new DateTime();
                            DateTime dtInstallDueDate = new DateTime();
                            DateTime dtReStructureDate = new DateTime();
                            foreach (DataRow objDR in dtdetail.Rows)
                            {
                                if (objDR["FirstDisbDate"].ToString() != "")
                                {
                                    dtFirstDisbDate = DateTime.ParseExact(objDR["FirstDisbDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                if (objDR["InstallDueDate"].ToString() != "")
                                {
                                    dtInstallDueDate = DateTime.ParseExact(objDR["InstallDueDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }
                                if (objDR["ReStructureDate"].ToString() != "")
                                {
                                    dtReStructureDate = DateTime.ParseExact(objDR["ReStructureDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                }




                                cmd1 = new SqlCommand();
                                cmd1.CommandType = CommandType.StoredProcedure;
                                cmd1.CommandText = "Insert_MstAccLoanDeatil";

                                cmd1.Parameters.Add("@LOANEE_UNQ_ID", SqlDbType.BigInt).Value = Loanee_Unq_ID;
                                cmd1.Parameters.Add("@LOANEE_ID", SqlDbType.VarChar).Value = Loanee_ID;
                                cmd1.Parameters.Add("@GL_ID", SqlDbType.VarChar).Value = GL_ID;
                                cmd1.Parameters.Add("@YEAR_MONTH", SqlDbType.VarChar).Value = objDR["ProYear"].ToString();
                                cmd1.Parameters.Add("@INSTAL_NO_1", SqlDbType.Int).Value = (objDR["InstallmentNo1"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToInt32(objDR["InstallmentNo1"].ToString()));
                                cmd1.Parameters.Add("@INSTAL_AMT_1", SqlDbType.Money).Value = (objDR["InstallmentAmount1"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InstallmentAmount1"].ToString()));
                                cmd1.Parameters.Add("@FirstDisbDate", SqlDbType.Date).Value = (objDR["FirstDisbDate"].ToString().Equals("") ? DBNull.Value : (object)dtFirstDisbDate.ToShortDateString());

                                cmd1.Parameters.Add("@INTEREST_RATE", SqlDbType.Decimal).Value = (objDR["InterestRate"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDouble(objDR["InterestRate"]).ToString("0.00"));
                                cmd1.Parameters.Add("@INSTAL_NO_2", SqlDbType.Int).Value = (objDR["InstallmentNo2"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToInt32(objDR["InstallmentNo2"].ToString()));
                                cmd1.Parameters.Add("@INSTAL_AMT_2", SqlDbType.Money).Value = (objDR["InstallmentAmount2"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InstallmentAmount2"].ToString()));
                                cmd1.Parameters.Add("@INSTAL_DUE_DT", SqlDbType.Date).Value = (objDR["InstallDueDate"].ToString().Equals("") ? DBNull.Value : (object)dtInstallDueDate.ToShortDateString());

                                cmd1.Parameters.Add("@REBATE_RATE", SqlDbType.Decimal).Value = (objDR["RebateRate"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["RebateRate"].ToString()));
                                cmd1.Parameters.Add("@INSTAL_NO_3", SqlDbType.Int).Value = (objDR["InstallmentNo3"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToInt32(objDR["InstallmentNo3"].ToString()));
                                cmd1.Parameters.Add("@INSTAL_AMT_3", SqlDbType.Money).Value = (objDR["InstallmentAmount3"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InstallmentAmount3"].ToString()));
                                cmd1.Parameters.Add("@DisbAmount", SqlDbType.Money).Value = (objDR["DisbAmount"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["DisbAmount"].ToString()));

                                cmd1.Parameters.Add("@PENALTY_RATE", SqlDbType.Decimal).Value = (objDR["PenaltyRate"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["PenaltyRate"].ToString()));
                                cmd1.Parameters.Add("@INSTAL_NO_4", SqlDbType.Int).Value = (objDR["InstallmentNo4"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToInt32(objDR["InstallmentNo4"].ToString()));
                                cmd1.Parameters.Add("@INSTAL_AMT_4", SqlDbType.Money).Value = (objDR["InstallmentAmount4"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InstallmentAmount4"].ToString()));
                                cmd1.Parameters.Add("@HEALTH_CODE", SqlDbType.VarChar).Value = (objDR["HealthCode"].ToString().Equals("") ? DBNull.Value : (object)objDR["HealthCode"].ToString());

                                cmd1.Parameters.Add("@CUML_DR_AMT", SqlDbType.Money).Value = (objDR["CumlDebitAmount"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["CumlDebitAmount"].ToString()));
                                cmd1.Parameters.Add("@CUML_CR_AMT", SqlDbType.Money).Value = (objDR["CumlCreditAmount"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["CumlCreditAmount"].ToString()));
                                cmd1.Parameters.Add("@SERVICECHARGE", SqlDbType.VarChar).Value = (objDR["ServiceCharge"].ToString().Equals("") ? DBNull.Value : (object)objDR["ServiceCharge"].ToString());

                                cmd1.Parameters.Add("@PRIN_DEFA", SqlDbType.Money).Value = (objDR["PrincipleDefault"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["PrincipleDefault"].ToString()));
                                cmd1.Parameters.Add("@INT_DEFA", SqlDbType.Money).Value = (objDR["InterestDefault"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["InterestDefault"].ToString()));
                                cmd1.Parameters.Add("@OTHER_DEFA", SqlDbType.Money).Value = (objDR["OtherDefault"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["OtherDefault"].ToString()));
                                cmd1.Parameters.Add("@DEFAULT_STATUS_CODE", SqlDbType.VarChar).Value = (objDR["DefaultStatusCode"].ToString().Equals("") ? DBNull.Value : (object)objDR["DefaultStatusCode"].ToString());

                                cmd1.Parameters.Add("@ACCOUNT_STATUS_CODE", SqlDbType.VarChar).Value = (objDR["AccountStatusCode"].ToString().Equals("") ? DBNull.Value : (object)objDR["AccountStatusCode"].ToString());
                                cmd1.Parameters.Add("@STATUS", SqlDbType.VarChar).Value = (objDR["Status"].ToString().Equals("") ? DBNull.Value : (object)objDR["Status"].ToString());
                                cmd1.Parameters.Add("@RESTRUCTURE_DATE", SqlDbType.Date).Value = (objDR["ReStructureDate"].ToString().Equals("") ? DBNull.Value : (object)dtReStructureDate.ToShortDateString());

                                cmd1.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(SectorID);
                                cmd1.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(UserID);

                                cmd1.Parameters.Add("@QTD_PRODUCT", SqlDbType.Money).Value = (objDR["QTD_PRODUCT"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["QTD_PRODUCT"].ToString()));
                                cmd1.Parameters.Add("@QTD_PRINCIPAL", SqlDbType.Money).Value = (objDR["QTD_PRINCIPAL"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["QTD_PRINCIPAL"].ToString()));
                                cmd1.Parameters.Add("@QTD_INTEREST", SqlDbType.Money).Value = (objDR["QTD_INTEREST"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["QTD_INTEREST"].ToString()));
                                cmd1.Parameters.Add("@QTD_OTHERS", SqlDbType.Money).Value = (objDR["QTD_OTHERS"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["QTD_OTHERS"].ToString()));
                                cmd1.Parameters.Add("@INTT_CAL_MODE", SqlDbType.VarChar).Value = (objDR["INTT_CAL_MODE"].ToString().Equals("") ? DBNull.Value : (object)objDR["INTT_CAL_MODE"].ToString());
                                
                                cmd1.Connection = db;
                                cmd1.Transaction = transaction;
                                cmd1.ExecuteNonQuery();

                            }
                        }
                    }

                    transaction.Commit();
                    HttpContext.Current.Session["LoanDetail"] = null;
                    dtLoan = (DataTable)HttpContext.Current.Session["LoanDetail"];
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    db.Close();
                    HttpContext.Current.Session["LoanDetail"] = null;
                    dtLoan = (DataTable)HttpContext.Current.Session["LoanDetail"];
                    string result = "Fail";
                    JSonVal = result.ToJSON();
                }
                db.Close();

            }
            else if (LoaneeUnqID != "")  ///-------------------------  Update part of Loan Master ----------------------------------------
            {
                //SqlConnection db = new SqlConnection(DataAccess.DBHandler.GetConnectionString());

                SqlCommand cmd = new SqlCommand();
                SqlCommand cmd1 = new SqlCommand();
                SqlCommand cmd2 = new SqlCommand();
                SqlTransaction transaction;
                //SqlDataReader reader;
                db.Open();
                transaction = db.BeginTransaction();
                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "Update_MstAccLoan";

                    cmd.Parameters.Add("@Loanee_Unq_ID", SqlDbType.VarChar).Value = LoaneeUnqID;
                    cmd.Parameters.Add("@LOANEE_ID", SqlDbType.VarChar).Value = LooneeID;
                    cmd.Parameters.Add("@GL_ID", SqlDbType.VarChar).Value = GLID;
                    cmd.Parameters.Add("@LOANEE_CODE", SqlDbType.VarChar).Value = (LooneeCode.Equals("") ? DBNull.Value : (object)LooneeCode);
                    cmd.Parameters.Add("@LOANEE_NAME", SqlDbType.VarChar).Value = (LooneeName.Equals("") ? DBNull.Value : (object)LooneeName);
                    cmd.Parameters.Add("@LOAN_TYPE", SqlDbType.Int).Value = (LoanType.Equals("") ? DBNull.Value : (object)Convert.ToInt32(LoanType));
                    cmd.Parameters.Add("@ORIGINAL_CODE", SqlDbType.VarChar).Value = (LooneeOriCode.Equals("") ? DBNull.Value : (object)LooneeOriCode);

                    cmd.Parameters.Add("@OFF_ADDRESS_1", SqlDbType.VarChar).Value = (OfficeAdd.Equals("") ? DBNull.Value : (object)OfficeAdd);
                    cmd.Parameters.Add("@OFF_ADDRESS_2", SqlDbType.VarChar).Value = (OfficeAdd2.Equals("") ? DBNull.Value : (object)OfficeAdd2);
                    cmd.Parameters.Add("@OFF_ADDRESS_3", SqlDbType.VarChar).Value = (OfficeAdd3.Equals("") ? DBNull.Value : (object)OfficeAdd3);
                    cmd.Parameters.Add("@OFF_PIN_CODE", SqlDbType.VarChar).Value = (OfficePin.Equals("") ? DBNull.Value : (object)OfficePin);
                    cmd.Parameters.Add("@OFF_TEL_NO", SqlDbType.VarChar).Value = (OfficeTeleNo.Equals("") ? DBNull.Value : (object)OfficeTeleNo);
                    cmd.Parameters.Add("@FCY_ADDRESS_1", SqlDbType.VarChar).Value = (FactoryAdd.Equals("") ? DBNull.Value : (object)FactoryAdd);
                    cmd.Parameters.Add("@FCY_ADDRESS_2", SqlDbType.VarChar).Value = (FactoryAdd2.Equals("") ? DBNull.Value : (object)FactoryAdd2);
                    cmd.Parameters.Add("@FCY_ADDRESS_3", SqlDbType.VarChar).Value = (FactoryAdd3.Equals("") ? DBNull.Value : (object)FactoryAdd3);
                    cmd.Parameters.Add("@FCY_PIN_CODE", SqlDbType.VarChar).Value = (FactoryPin.Equals("") ? DBNull.Value : (object)FactoryPin);
                    cmd.Parameters.Add("@FCY_TEL_NO", SqlDbType.VarChar).Value = (FactoryTeleNo.Equals("") ? DBNull.Value : (object)FactoryTeleNo);

                    cmd.Parameters.Add("@IMPLI_STATUS", SqlDbType.VarChar).Value = (ImplementaiotnStatus.Equals("") ? DBNull.Value : (object)ImplementaiotnStatus);
                    cmd.Parameters.Add("@DOCO", SqlDbType.Date).Value = (CommOpDate.Equals("") ? DBNull.Value : (object)dtCommOpDate.ToShortDateString());

                    cmd.Parameters.Add("@DIST_CODE", SqlDbType.Int).Value = (District.Equals("") ? DBNull.Value : (object)Convert.ToInt32(District));
                    cmd.Parameters.Add("@INDUS_CODE_ID", SqlDbType.VarChar).Value = (Industry.Equals("") ? DBNull.Value : (object)Industry);
                    cmd.Parameters.Add("@CONST_CODE_ID", SqlDbType.Int).Value = (Constitution.Equals("") ? DBNull.Value : (object)Convert.ToInt32(Constitution));
                    cmd.Parameters.Add("@AREA_CODE", SqlDbType.VarChar).Value = (Area.Equals("") ? DBNull.Value : (object)Area);
                    cmd.Parameters.Add("@SECTOR_CODE", SqlDbType.Int).Value = (Sector.Equals("") ? DBNull.Value : (object)Convert.ToInt32(Sector));
                    cmd.Parameters.Add("@PURPOSE", SqlDbType.VarChar).Value = (Purpose.Equals("") ? DBNull.Value : (object)Purpose);

                    cmd.Parameters.Add("@APPLN_DT", SqlDbType.Date).Value = (ApplicationDate.Equals("") ? DBNull.Value : (object)dtApplicationDate.ToShortDateString());
                    cmd.Parameters.Add("@APPLN_AMT", SqlDbType.Money).Value = (ApplicationAmount.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(ApplicationAmount));
                    cmd.Parameters.Add("@PROJECT_COST", SqlDbType.Money).Value = (ProjectCost.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(ProjectCost));
                    cmd.Parameters.Add("@SANCT_DATE", SqlDbType.Date).Value = (SanctionDate.Equals("") ? DBNull.Value : (object)dtSanctionDate.ToShortDateString());
                    cmd.Parameters.Add("@SANCT_AMT", SqlDbType.Money).Value = (SanctionAmount.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(SanctionAmount));
                    cmd.Parameters.Add("@SANCAN_DT", SqlDbType.Date).Value = (SanctionCancelDate.Equals("") ? DBNull.Value : (object)dtSanctionCancelDate.ToShortDateString());
                    cmd.Parameters.Add("@SANCAN_AMT", SqlDbType.Money).Value = (SanctionCancelAmount.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(SanctionCancelAmount));
                    cmd.Parameters.Add("@REPAYMENT_TERM", SqlDbType.VarChar).Value = (RepaymentTerm.Equals("") ? DBNull.Value : (object)RepaymentTerm);
                    cmd.Parameters.Add("@REP_ST_DT", SqlDbType.Date).Value = (RepStartDate.Equals("") ? DBNull.Value : (object)dtRepStartDate.ToShortDateString());

                    cmd.Parameters.Add("@MPS_AMT", SqlDbType.Money).Value = (Pri_Mortgaged.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(Pri_Mortgaged));
                    cmd.Parameters.Add("@HPS_AMT", SqlDbType.Money).Value = (Pri_Hypothctd.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(Pri_Hypothctd));
                    cmd.Parameters.Add("@PPS_AMT", SqlDbType.Money).Value = (Pri_Pledged.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(Pri_Pledged));
                    cmd.Parameters.Add("@MCS_AMT", SqlDbType.Money).Value = (CoL_Mortgaged.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(CoL_Mortgaged));
                    cmd.Parameters.Add("@HCS_AMT", SqlDbType.Money).Value = (CoL_Hypothctd.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(CoL_Hypothctd));
                    cmd.Parameters.Add("@PCS_AMT", SqlDbType.Money).Value = (CoL_Pledged.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(CoL_Pledged));

                    cmd.Parameters.Add("@CHIEF_PROMOTER", SqlDbType.VarChar).Value = (ChiefPromoter.Equals("") ? DBNull.Value : (object)ChiefPromoter);
                    cmd.Parameters.Add("@PWR_STATUS_CODE", SqlDbType.VarChar).Value = (PwrStatus.Equals("") ? DBNull.Value : (object)PwrStatus);
                    cmd.Parameters.Add("@PRODUCT", SqlDbType.VarChar).Value = (Product.Equals("") ? DBNull.Value : (object)Product);
                    cmd.Parameters.Add("@CONSULTANT", SqlDbType.VarChar).Value = (Consultant.Equals("") ? DBNull.Value : (object)Consultant);
                    cmd.Parameters.Add("@EMPLOYMENT", SqlDbType.Money).Value = (ProjectEmployment.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(ProjectEmployment));
                    cmd.Parameters.Add("@ACTUAL_EMPLOYMENT", SqlDbType.Money).Value = (ActualEmployment.Equals("") ? DBNull.Value : (object)Convert.ToDecimal(ActualEmployment));
                    cmd.Parameters.Add("@PROJ_COMM_OPERA_DT", SqlDbType.Date).Value = (ProjectedImplementionDate.Equals("") ? DBNull.Value : (object)dtProjectedImplementionDate.ToShortDateString());
                    cmd.Parameters.Add("@ACTU_COMM_OPERA_DT", SqlDbType.Date).Value = (ActualImplementionDate.Equals("") ? DBNull.Value : (object)dtActualImplementionDate.ToShortDateString());
                    cmd.Parameters.Add("@SANCTIONED_BY", SqlDbType.VarChar).Value = (SanctionBy.Equals("") ? DBNull.Value : (object)SanctionBy);

                    //cmd.Parameters.Add("@MOBILE_NO", SqlDbType.VarChar).Value = (MobileNo.Equals("") ? DBNull.Value : (object)MobileNo);
                    //cmd.Parameters.Add("@EMAIL_ID", SqlDbType.VarChar).Value = (EmailID.Equals("") ? DBNull.Value : (object)EmailID);

                    cmd.Parameters.Add("@XMLPhoneAndMail", MobileEmailXml);

                    cmd.Parameters.Add("@INSP_DATE", SqlDbType.Date).Value = (InspectionDate.Equals("") ? DBNull.Value : (object)dtInspectionDate.ToShortDateString());
                    cmd.Parameters.Add("@PO", SqlDbType.Int).Value = (ProcessingOff.Equals("") ? DBNull.Value : (object)Convert.ToInt32(ProcessingOff));
                    cmd.Parameters.Add("@DO", SqlDbType.Int).Value = (DisburseOff.Equals("") ? DBNull.Value : (object)Convert.ToInt32(DisburseOff));
                    cmd.Parameters.Add("@RO", SqlDbType.Int).Value = (RecoOff.Equals("") ? DBNull.Value : (object)Convert.ToInt32(RecoOff));

                    cmd.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(SectorID);
                    cmd.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(UserID);

                    cmd.Parameters.Add("@PrePayDate", SqlDbType.Date).Value = (PrePayDate.Equals("") ? DBNull.Value : (object)Convert.ToDateTime(PrePayDate)); 
                    cmd.Parameters.Add("@PrePayAmount", SqlDbType.Money).Value = Convert.ToDecimal(PrePayAmount);
                    cmd.Parameters.Add("@PanNo", SqlDbType.VarChar).Value = (PanNo.Equals("") ? DBNull.Value : (object)PanNo);
                    cmd.Parameters.Add("@GstNo", SqlDbType.VarChar).Value = (GstNo.Equals("") ? DBNull.Value : (object)GstNo);
                    cmd.Connection = db;
                    cmd.Transaction = transaction;
                    cmd.ExecuteNonQuery();
                    //SqlDataAdapter da = new SqlDataAdapter(cmd);
                    //DataTable dt1 = new DataTable();
                    //da.Fill(dt1);



                    // ---------------------------------------------------------------------- Detail Part --------------------------------------------------------------------

                    DataTable dtdetail = (DataTable)HttpContext.Current.Session["LoanDetail"];
                    if (dtdetail != null)           /// if there dont have any data into detail.
                    {
                        if (dtdetail.Rows.Count > 0)
                        {
                            DateTime dtFirstDisbDate = new DateTime();
                            DateTime dtInstallDueDate = new DateTime();
                            DateTime dtReStructureDate = new DateTime();
                            foreach (DataRow objDR in dtdetail.Rows)
                            {
                                if (objDR["CheckStatus"].ToString() == "1")
                                {

                                    if (objDR["FirstDisbDate"].ToString() != "")
                                    {
                                        dtFirstDisbDate = DateTime.ParseExact(objDR["FirstDisbDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    if (objDR["InstallDueDate"].ToString() != "")
                                    {
                                        dtInstallDueDate = DateTime.ParseExact(objDR["InstallDueDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    if (objDR["ReStructureDate"].ToString() != "")
                                    {
                                        dtReStructureDate = DateTime.ParseExact(objDR["ReStructureDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }


                                    cmd2 = new SqlCommand();
                                    cmd2.CommandType = CommandType.Text;
                                    cmd2.Connection = db;
                                    cmd2.CommandText = "delete from wbfcerp.DAT_Acc_Loan where  LOANEE_UNQ_ID=" + LoaneeUnqID + " and YEAR_MONTH =" + objDR["ProYear"].ToString() + " and SectorID=" + Convert.ToInt32(SectorID) + "";
                                    cmd2.Transaction = transaction;
                                    //cmd2.CommandTimeout
                                    //db.ConnectionTimeout = 10000000;
                                    
                                    cmd2.ExecuteNonQuery();



                                    //delete from wbfcerp.DAT_Acc_Loan where LOANEE_ID = @LOANEE_ID AND GL_ID=@GL_ID AND SectorID = @SectorID;


                                    cmd1 = new SqlCommand();

                                    cmd1.CommandType = CommandType.StoredProcedure;
                                    cmd1.CommandText = "Insert_MstAccLoanDeatil";

                                    cmd1.Parameters.Add("@LOANEE_UNQ_ID", SqlDbType.BigInt).Value = LoaneeUnqID;
                                    cmd1.Parameters.Add("@LOANEE_ID", SqlDbType.VarChar).Value = LooneeID;
                                    cmd1.Parameters.Add("@GL_ID", SqlDbType.VarChar).Value = GLID;
                                    cmd1.Parameters.Add("@YEAR_MONTH", SqlDbType.VarChar).Value = objDR["ProYear"].ToString();
                                    cmd1.Parameters.Add("@INSTAL_NO_1", SqlDbType.Int).Value = (objDR["InstallmentNo1"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToInt32(objDR["InstallmentNo1"].ToString()));
                                    cmd1.Parameters.Add("@INSTAL_AMT_1", SqlDbType.Money).Value = (objDR["InstallmentAmount1"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InstallmentAmount1"].ToString()));
                                    cmd1.Parameters.Add("@FirstDisbDate", SqlDbType.Date).Value = (objDR["FirstDisbDate"].ToString().Equals("") ? DBNull.Value : (object)dtFirstDisbDate.ToShortDateString());

                                    cmd1.Parameters.Add("@INTEREST_RATE", SqlDbType.Money).Value = (objDR["InterestRate"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InterestRate"].ToString()));
                                    cmd1.Parameters.Add("@INSTAL_NO_2", SqlDbType.Int).Value = (objDR["InstallmentNo2"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToInt32(objDR["InstallmentNo2"].ToString()));
                                    cmd1.Parameters.Add("@INSTAL_AMT_2", SqlDbType.Money).Value = (objDR["InstallmentAmount2"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InstallmentAmount2"].ToString()));
                                    cmd1.Parameters.Add("@INSTAL_DUE_DT", SqlDbType.Date).Value = (objDR["InstallDueDate"].ToString().Equals("") ? DBNull.Value : (object)dtInstallDueDate.ToShortDateString());

                                    cmd1.Parameters.Add("@REBATE_RATE", SqlDbType.Money).Value = (objDR["RebateRate"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["RebateRate"].ToString()));
                                    cmd1.Parameters.Add("@INSTAL_NO_3", SqlDbType.Int).Value = (objDR["InstallmentNo3"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToInt32(objDR["InstallmentNo3"].ToString()));
                                    cmd1.Parameters.Add("@INSTAL_AMT_3", SqlDbType.Money).Value = (objDR["InstallmentAmount3"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InstallmentAmount3"].ToString()));
                                    cmd1.Parameters.Add("@DisbAmount", SqlDbType.Money).Value = (objDR["DisbAmount"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["DisbAmount"].ToString()));

                                    cmd1.Parameters.Add("@PENALTY_RATE", SqlDbType.Money).Value = (objDR["PenaltyRate"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["PenaltyRate"].ToString()));
                                    cmd1.Parameters.Add("@INSTAL_NO_4", SqlDbType.Int).Value = (objDR["InstallmentNo4"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToInt32(objDR["InstallmentNo4"].ToString()));
                                    cmd1.Parameters.Add("@INSTAL_AMT_4", SqlDbType.Money).Value = (objDR["InstallmentAmount4"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["InstallmentAmount4"].ToString()));
                                    cmd1.Parameters.Add("@HEALTH_CODE", SqlDbType.VarChar).Value = (objDR["HealthCode"].ToString().Equals("") ? DBNull.Value : (object)objDR["HealthCode"].ToString());

                                    cmd1.Parameters.Add("@CUML_DR_AMT", SqlDbType.Money).Value = (objDR["CumlDebitAmount"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["CumlDebitAmount"].ToString()));
                                    cmd1.Parameters.Add("@CUML_CR_AMT", SqlDbType.Money).Value = (objDR["CumlCreditAmount"].ToString().Equals("") ? DBNull.Value : (object)Convert.ToDecimal(objDR["CumlCreditAmount"].ToString()));
                                    cmd1.Parameters.Add("@SERVICECHARGE", SqlDbType.VarChar).Value = (objDR["ServiceCharge"].ToString().Equals("") ? DBNull.Value : (object)objDR["ServiceCharge"].ToString());

                                    cmd1.Parameters.Add("@PRIN_DEFA", SqlDbType.Money).Value = (objDR["PrincipleDefault"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["PrincipleDefault"].ToString()));
                                    cmd1.Parameters.Add("@INT_DEFA", SqlDbType.Money).Value = (objDR["InterestDefault"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["InterestDefault"].ToString()));
                                    cmd1.Parameters.Add("@OTHER_DEFA", SqlDbType.Money).Value = (objDR["OtherDefault"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["OtherDefault"].ToString()));
                                    cmd1.Parameters.Add("@DEFAULT_STATUS_CODE", SqlDbType.VarChar).Value = (objDR["DefaultStatusCode"].ToString().Equals("") ? DBNull.Value : (object)objDR["DefaultStatusCode"].ToString());

                                    cmd1.Parameters.Add("@ACCOUNT_STATUS_CODE", SqlDbType.VarChar).Value = (objDR["AccountStatusCode"].ToString().Equals("") ? DBNull.Value : (object)objDR["AccountStatusCode"].ToString());
                                    cmd1.Parameters.Add("@STATUS", SqlDbType.VarChar).Value = (objDR["Status"].ToString().Equals("") ? DBNull.Value : (object)objDR["Status"].ToString());
                                    cmd1.Parameters.Add("@RESTRUCTURE_DATE", SqlDbType.Date).Value = (objDR["ReStructureDate"].ToString().Equals("") ? DBNull.Value : (object)dtReStructureDate.ToShortDateString());

                                    cmd1.Parameters.Add("@SectorID", SqlDbType.Int).Value = Convert.ToInt32(SectorID);
                                    cmd1.Parameters.Add("@INSERTED_BY", SqlDbType.Int).Value = Convert.ToInt32(UserID);

                                    cmd1.Parameters.Add("@QTD_PRODUCT", SqlDbType.Money).Value = (objDR["QTD_PRODUCT"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["QTD_PRODUCT"].ToString()));
                                    cmd1.Parameters.Add("@QTD_PRINCIPAL", SqlDbType.Money).Value = (objDR["QTD_PRINCIPAL"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["QTD_PRINCIPAL"].ToString()));
                                    cmd1.Parameters.Add("@QTD_INTEREST", SqlDbType.Money).Value = (objDR["QTD_INTEREST"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["QTD_INTEREST"].ToString()));
                                    cmd1.Parameters.Add("@QTD_OTHERS", SqlDbType.Money).Value = (objDR["QTD_OTHERS"].ToString().Equals("") ? 0 : (object)Convert.ToDecimal(objDR["QTD_OTHERS"].ToString()));
                                    cmd1.Parameters.Add("@INTT_CAL_MODE", SqlDbType.VarChar).Value = (objDR["INTT_CAL_MODE"].ToString().Equals("") ? DBNull.Value : (object)objDR["INTT_CAL_MODE"].ToString());

                                    cmd1.Connection = db;
                                    cmd1.Transaction = transaction;
                                    cmd1.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                    transaction.Commit();
                    HttpContext.Current.Session["LoanDetail"] = null;
                    dtLoan = (DataTable)HttpContext.Current.Session["LoanDetail"];
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    db.Close();
                    HttpContext.Current.Session["LoanDetail"] = null;
                    dtLoan = (DataTable)HttpContext.Current.Session["LoanDetail"];
                    string result = "Fail";
                    JSonVal = result.ToJSON();
                }
                db.Close();
            }
        }
        catch (Exception ex)
        {
            db.Close();
            HttpContext.Current.Session["LoanDetail"] = null;
            dtLoan = (DataTable)HttpContext.Current.Session["LoanDetail"];
            throw new Exception(ex.Message);
        }

        string result2 = "Pass";
        JSonVal = result2.ToJSON();
        return JSonVal;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_LoanDetailsOnSession(string ProYear, string InstallmentNo1, string InstallmentAmount1, string FirstDisbDate,
                                                  string InterestRate, string InstallmentNo2, string InstallmentAmount2, string InstallDueDate,
                                                  string RebateRate, string InstallmentNo3, string InstallmentAmount3, string DisbAmount,
                                                  string PenaltyRate, string InstallmentNo4, string InstallmentAmount4, string HealthCode,
                                                  string CumlDebitAmount, string CumlCreditAmount, string ServiceCharge,
                                                  string PrincipleDefault, string InterestDefault, string OtherDefault, string DefaultStatusCode,
                                                  string AccountStatusCode, string Status, string ReStructureDate, string CheckStatus,
                                                  string QTD_PRODUCT, string QTD_PRINCIPAL,string QTD_INTEREST,string QTD_OTHERS, string INTT_CAL_MODE)
    {
        string JSONVal = ""; 
        try
        {

            if (HttpContext.Current.Session["LoanDetail"] != null)
            {
                DataTable dtdetail = (DataTable)HttpContext.Current.Session["LoanDetail"];
                if (dtdetail.Rows.Count > 0)
                {
                    DataRow dtrow = dtdetail.NewRow();
                    dtrow["ProYear"] = ProYear;
                    dtrow["InstallmentNo1"] = (InstallmentNo1 == "") ? 0 : Convert.ToInt32(InstallmentNo1);
                    dtrow["InstallmentAmount1"] = (InstallmentAmount1 == "") ? 0 : Convert.ToDecimal(InstallmentAmount1);
                    dtrow["FirstDisbDate"] = FirstDisbDate;

                    dtrow["InterestRate"] = (InterestRate == "") ? 0 : Convert.ToDecimal(InterestRate);
                    dtrow["InstallmentNo2"] = (InstallmentNo2 == "") ? 0 : Convert.ToInt32(InstallmentNo2);
                    dtrow["InstallmentAmount2"] = (InstallmentAmount2 == "") ? 0 : Convert.ToDecimal(InstallmentAmount2);
                    dtrow["InstallDueDate"] = InstallDueDate;

                    dtrow["RebateRate"] = (RebateRate == "") ? 0 : Convert.ToDecimal(RebateRate);
                    dtrow["InstallmentNo3"] = (InstallmentNo3 == "") ? 0 : Convert.ToInt32(InstallmentNo3);
                    dtrow["InstallmentAmount3"] = (InstallmentAmount3 == "") ? 0 : Convert.ToDecimal(InstallmentAmount3);
                    dtrow["DisbAmount"] = (DisbAmount == "") ? 0 : Convert.ToDecimal(DisbAmount);

                    dtrow["PenaltyRate"] = (PenaltyRate == "") ? 0 : Convert.ToDecimal(PenaltyRate);
                    dtrow["InstallmentNo4"] = (InstallmentNo4 == "") ? 0 : Convert.ToInt32(InstallmentNo4);
                    dtrow["InstallmentAmount4"] = (InstallmentAmount4 == "") ? 0 : Convert.ToDecimal(InstallmentAmount4);
                    dtrow["HealthCode"] = HealthCode;

                    dtrow["CumlDebitAmount"] = (CumlDebitAmount == "") ? 0 : Convert.ToDecimal(CumlDebitAmount);
                    dtrow["CumlCreditAmount"] = (CumlCreditAmount == "") ? 0 : Convert.ToDecimal(CumlCreditAmount);
                    //dtrow["TotalOS"] = TotalOS;
                    dtrow["ServiceCharge"] = ServiceCharge;

                    dtrow["PrincipleDefault"] = (PrincipleDefault == "") ? 0 : Convert.ToDecimal(PrincipleDefault);
                    dtrow["InterestDefault"] = (InterestDefault == "") ? 0 : Convert.ToDecimal(InterestDefault);
                    dtrow["OtherDefault"] = (OtherDefault == "") ? 0 : Convert.ToDecimal(OtherDefault);
                    dtrow["DefaultStatusCode"] = DefaultStatusCode;

                    dtrow["AccountStatusCode"] = AccountStatusCode;
                    dtrow["Status"] = Status;
                    dtrow["ReStructureDate"] = ReStructureDate;
                    dtrow["CheckStatus"] = CheckStatus;

                    dtrow["QTD_PRODUCT"] = QTD_PRODUCT;
                    dtrow["QTD_PRINCIPAL"] = QTD_PRINCIPAL;
                    dtrow["QTD_INTEREST"] = QTD_INTEREST;
                    dtrow["QTD_OTHERS"] = QTD_OTHERS;
                    dtrow["INTT_CAL_MODE"] = INTT_CAL_MODE;

                    dtdetail.Rows.Add(dtrow);
                    HttpContext.Current.Session["LoanDetail"] = dtdetail;
                }
            }
            else
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ProYear");
                dt.Columns.Add("InstallmentNo1");
                dt.Columns.Add("InstallmentAmount1");
                dt.Columns.Add("FirstDisbDate");

                dt.Columns.Add("InterestRate");
                dt.Columns.Add("InstallmentNo2");
                dt.Columns.Add("InstallmentAmount2");
                dt.Columns.Add("InstallDueDate");

                dt.Columns.Add("RebateRate");
                dt.Columns.Add("InstallmentNo3");
                dt.Columns.Add("InstallmentAmount3");
                dt.Columns.Add("DisbAmount");

                dt.Columns.Add("PenaltyRate");
                dt.Columns.Add("InstallmentNo4");
                dt.Columns.Add("InstallmentAmount4");
                dt.Columns.Add("HealthCode");

                dt.Columns.Add("CumlDebitAmount");
                dt.Columns.Add("CumlCreditAmount");
                //dt.Columns.Add("TotalOS");
                dt.Columns.Add("ServiceCharge");

                dt.Columns.Add("PrincipleDefault");
                dt.Columns.Add("InterestDefault");
                dt.Columns.Add("OtherDefault");
                dt.Columns.Add("DefaultStatusCode");

                dt.Columns.Add("AccountStatusCode");
                dt.Columns.Add("Status");
                dt.Columns.Add("ReStructureDate");
                dt.Columns.Add("CheckStatus");

                dt.Columns.Add("QTD_PRODUCT");
                dt.Columns.Add("QTD_PRINCIPAL");
                dt.Columns.Add("QTD_INTEREST");
                dt.Columns.Add("QTD_OTHERS");
                dt.Columns.Add("INTT_CAL_MODE");

                

                DataRow dtrow = dt.NewRow();
                dtrow["ProYear"] = ProYear;
                dtrow["InstallmentNo1"] = (InstallmentNo1 == "") ? 0 : Convert.ToDecimal(InstallmentNo1);
                dtrow["InstallmentAmount1"] = (InstallmentAmount1 == "") ? 0 : Convert.ToDecimal(InstallmentAmount1);
                dtrow["FirstDisbDate"] = FirstDisbDate;

                dtrow["InterestRate"] = (InterestRate == "") ? 0 : Convert.ToDecimal(InterestRate);
                dtrow["InstallmentNo2"] = (InstallmentNo2 == "") ? 0 : Convert.ToDecimal(InstallmentNo2);
                dtrow["InstallmentAmount2"] = (InstallmentAmount2 == "") ? 0 : Convert.ToDecimal(InstallmentAmount2);
                dtrow["InstallDueDate"] = InstallDueDate;

                dtrow["RebateRate"] = (RebateRate == "") ? 0 : Convert.ToDecimal(RebateRate);
                dtrow["InstallmentNo3"] = (InstallmentNo3 == "") ? 0 : Convert.ToDecimal(InstallmentNo3);
                dtrow["InstallmentAmount3"] = (InstallmentAmount3 == "") ? 0 : Convert.ToDecimal(InstallmentAmount3);
                dtrow["DisbAmount"] = (DisbAmount == "") ? 0 : Convert.ToDecimal(DisbAmount);

                dtrow["PenaltyRate"] = (PenaltyRate == "") ? 0 : Convert.ToDecimal(PenaltyRate);
                dtrow["InstallmentNo4"] = (InstallmentNo4 == "") ? 0 : Convert.ToDecimal(InstallmentNo4);
                dtrow["InstallmentAmount4"] = (InstallmentAmount4 == "") ? 0 : Convert.ToDecimal(InstallmentAmount4);
                dtrow["HealthCode"] = HealthCode;

                dtrow["CumlDebitAmount"] = (CumlDebitAmount == "") ? 0 : Convert.ToDecimal(CumlDebitAmount);
                dtrow["CumlCreditAmount"] = (CumlCreditAmount == "") ? 0 : Convert.ToDecimal(CumlCreditAmount);
                //dtrow["TotalOS"] = TotalOS;
                dtrow["ServiceCharge"] = ServiceCharge;

                dtrow["PrincipleDefault"] = (PrincipleDefault == "") ? 0 : Convert.ToDecimal(PrincipleDefault);
                dtrow["InterestDefault"] = (InterestDefault == "") ? 0 : Convert.ToDecimal(InterestDefault);
                dtrow["OtherDefault"] = (OtherDefault == "") ? 0 : Convert.ToDecimal(OtherDefault);
                dtrow["DefaultStatusCode"] = DefaultStatusCode;

                dtrow["AccountStatusCode"] = AccountStatusCode;
                dtrow["Status"] = Status;
                dtrow["ReStructureDate"] = ReStructureDate;
                dtrow["CheckStatus"] = CheckStatus;

                dtrow["QTD_PRODUCT"] = QTD_PRODUCT;
                dtrow["QTD_PRINCIPAL"] = QTD_PRINCIPAL;
                dtrow["QTD_INTEREST"] = QTD_INTEREST;
                dtrow["QTD_OTHERS"] = QTD_OTHERS;
                dtrow["INTT_CAL_MODE"] = INTT_CAL_MODE;

                

                //dtrow["ProYear"] = ProYear;
                //dtrow["InstallmentNo1"] = InstallmentNo1;
                //dtrow["InstallmentAmount1"] = InstallmentAmount1;
                //dtrow["FirstDisbDate"] = FirstDisbDate;

                //dtrow["InterestRate"] = InterestRate;
                //dtrow["InstallmentNo2"] = InstallmentNo2;
                //dtrow["InstallmentAmount2"] = InstallmentAmount2;
                //dtrow["InstallDueDate"] = InstallDueDate;

                //dtrow["RebateRate"] = RebateRate;
                //dtrow["InstallmentNo3"] = InstallmentNo3;
                //dtrow["InstallmentAmount3"] = InstallmentAmount3;
                //dtrow["DisbAmount"] = DisbAmount;

                //dtrow["PenaltyRate"] = PenaltyRate;
                //dtrow["InstallmentNo4"] = InstallmentNo4;
                //dtrow["InstallmentAmount4"] = InstallmentAmount4;
                //dtrow["HealthCode"] = HealthCode;

                //dtrow["CumlDebitAmount"] = CumlDebitAmount;
                //dtrow["CumlCreditAmount"] = CumlCreditAmount;
                ////dtrow["TotalOS"] = TotalOS;
                //dtrow["ServiceCharge"] = ServiceCharge;

                //dtrow["PrincipleDefault"] = PrincipleDefault;
                //dtrow["InterestDefault"] = InterestDefault;
                //dtrow["OtherDefault"] = OtherDefault;
                //dtrow["DefaultStatusCode"] = DefaultStatusCode;

                //dtrow["AccountStatusCode"] = AccountStatusCode;
                //dtrow["Status"] = Status;
                //dtrow["ReStructureDate"] = ReStructureDate;

                dt.Rows.Add(dtrow);
                HttpContext.Current.Session["LoanDetail"] = dt;
            }

            string result = "success";
            JSONVal = result.ToJSON();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string DeleteSessionLoanByYearMonth(string YearMonth)
    {
        //StringBuilder objSB = new StringBuilder();
        string JSONVal = string.Empty;
        string strYearMon = "";
        DataTable dtDetails = (DataTable)HttpContext.Current.Session["LoanDetail"];
        if (HttpContext.Current.Session["LoanDetail"] != null)
        {
            strYearMon = YearMonth;
            try
            {
                if (dtDetails.Rows.Count == 1)
                {
                    HttpContext.Current.Session["LoanDetail"] = null;
                    dtLoan = (DataTable)HttpContext.Current.Session["LoanDetail"];
                }
                else
                {
                    for (int i = 0; i <= dtDetails.Rows.Count - 1; i++)
                    {
                        if (strYearMon == dtDetails.Rows[i]["ProYear"].ToString())
                        {
                            dtDetails.Rows[i].Delete(); //Should just remove the row from the datatable.
                            dtDetails.AcceptChanges();
                        }
                    }
                    //dtDetails = dtDetails.AsEnumerable().Where(x => x.Field<String>("YEAR_MONTH") != strYearMon).CopyToDataTable();
                    HttpContext.Current.Session["LoanDetail"] = dtDetails;
                }
            }
            catch (Exception ex)
            {
                HttpContext.Current.Session["LoanDetail"] = null;
            }
        }
        return JSONVal;
    }

    [WebMethod]
    public static FetchValue[] FetchValueFromSession(string YearMonth)
    {
        List<FetchValue> Detail = new List<FetchValue>();
        DataTable dtGetData = (DataTable)HttpContext.Current.Session["LoanDetail"];
        for (int i = 0; i < dtGetData.Rows.Count; i++)
        {
            if (YearMonth == dtGetData.Rows[i]["ProYear"].ToString())
            {
                FetchValue DataObj = new FetchValue();
                DataObj.ProYear = dtGetData.Rows[i]["ProYear"].ToString();
                DataObj.InstallmentNo1 = dtGetData.Rows[i]["InstallmentNo1"].ToString();
                DataObj.InstallmentAmount1 = dtGetData.Rows[i]["InstallmentAmount1"].ToString();
                DataObj.FirstDisbDate = dtGetData.Rows[i]["FirstDisbDate"].ToString();

                DataObj.InterestRate = dtGetData.Rows[i]["InterestRate"].ToString();
                DataObj.InstallmentNo2 = dtGetData.Rows[i]["InstallmentNo2"].ToString();
                DataObj.InstallmentAmount2 = dtGetData.Rows[i]["InstallmentAmount2"].ToString();
                DataObj.InstallDueDate = dtGetData.Rows[i]["InstallDueDate"].ToString();

                DataObj.RebateRate = dtGetData.Rows[i]["RebateRate"].ToString();
                DataObj.InstallmentNo3 = dtGetData.Rows[i]["InstallmentNo3"].ToString();
                DataObj.InstallmentAmount3 = dtGetData.Rows[i]["InstallmentAmount3"].ToString();
                DataObj.DisbAmount = dtGetData.Rows[i]["DisbAmount"].ToString();

                DataObj.PenaltyRate = dtGetData.Rows[i]["PenaltyRate"].ToString();
                DataObj.InstallmentNo4 = dtGetData.Rows[i]["InstallmentNo4"].ToString();
                DataObj.InstallmentAmount4 = dtGetData.Rows[i]["InstallmentAmount4"].ToString();
                DataObj.HealthCode = dtGetData.Rows[i]["HealthCode"].ToString();

                DataObj.CumlDebitAmount = dtGetData.Rows[i]["CumlDebitAmount"].ToString();
                DataObj.CumlCreditAmount = dtGetData.Rows[i]["CumlCreditAmount"].ToString();
                //DataObj.TotalOS = dtGetData.Rows[i]["TotalOS"].ToString();
                DataObj.ServiceCharge = dtGetData.Rows[i]["ServiceCharge"].ToString();

                DataObj.PrincipleDefault = dtGetData.Rows[i]["PrincipleDefault"].ToString();
                DataObj.InterestDefault = dtGetData.Rows[i]["InterestDefault"].ToString();
                DataObj.OtherDefault = dtGetData.Rows[i]["OtherDefault"].ToString();
                DataObj.DefaultStatusCode = dtGetData.Rows[i]["DefaultStatusCode"].ToString();

                DataObj.AccountStatusCode = dtGetData.Rows[i]["AccountStatusCode"].ToString();
                DataObj.Status = dtGetData.Rows[i]["Status"].ToString();
                DataObj.ReStructureDate = dtGetData.Rows[i]["ReStructureDate"].ToString();

                DataObj.QTD_PRODUCT = dtGetData.Rows[i]["QTD_PRODUCT"].ToString();
                DataObj.QTD_PRINCIPAL = dtGetData.Rows[i]["QTD_PRINCIPAL"].ToString();
                DataObj.QTD_INTEREST = dtGetData.Rows[i]["QTD_INTEREST"].ToString();
                DataObj.QTD_OTHERS = dtGetData.Rows[i]["QTD_OTHERS"].ToString();
                DataObj.INTT_CAL_MODE = dtGetData.Rows[i]["INTT_CAL_MODE"].ToString();

                Detail.Add(DataObj);
            }
        }
        ////foreach (DataRow dtRow in dtGetData.Rows)
        ////{
        ////    FetchValue DataObj = new FetchValue();
        ////    DataObj.OLD_GL_ID = dtRow["OLD_GL_ID"].ToString();
        ////    DataObj.GL_ID = dtRow["GL_ID"].ToString();
        ////    Detail.Add(DataObj);
        ////}
        return Detail.ToArray();
    }
    public class FetchValue
    {
        public string ProYear { get; set; }
        public string InstallmentNo1 { get; set; }
        public string InstallmentAmount1 { get; set; }
        public string FirstDisbDate { get; set; }

        public string InterestRate { get; set; }
        public string InstallmentNo2 { get; set; }
        public string InstallmentAmount2 { get; set; }
        public string InstallDueDate { get; set; }

        public string RebateRate { get; set; }
        public string InstallmentNo3 { get; set; }
        public string InstallmentAmount3 { get; set; }
        public string DisbAmount { get; set; }

        public string PenaltyRate { get; set; }
        public string InstallmentNo4 { get; set; }
        public string InstallmentAmount4 { get; set; }
        public string HealthCode { get; set; }

        public string CumlDebitAmount { get; set; }
        public string CumlCreditAmount { get; set; }
        //public string TotalOS { get; set; }
        public string ServiceCharge { get; set; }

        public string PrincipleDefault { get; set; }
        public string InterestDefault { get; set; }
        public string OtherDefault { get; set; }
        public string DefaultStatusCode { get; set; }

        public string AccountStatusCode { get; set; }
        public string Status { get; set; }
        public string ReStructureDate { get; set; }

        public string QTD_PRODUCT { get; set; }
        public string QTD_PRINCIPAL { get; set; }
        public string QTD_INTEREST { get; set; }
        public string QTD_OTHERS { get; set; }
        public string INTT_CAL_MODE { get; set; }
    }


    //[WebMethod]
    //public static string[] LoaneeAutoCompleteData(string LoaneeCode)
    //{
    //    List<string> result = new List<string>();
    //    DataTable dtLoaneeData = DBHandler.GetResult("Search_LoaneeIDByCode", LoaneeCode, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
    //    foreach (DataRow dtRow in dtLoaneeData.Rows)
    //    {
    //        // result.Add(dtRow["LOANEE_NAME"].ToString());
    //        result.Add(string.Format("{0}|{1}", dtRow["LOANEE_NAME1"].ToString(), dtRow["LOANEE_UNQ_ID"].ToString()));

    //        //string str = dtRow["LOANEE_NAME"].ToString();
    //    }
    //    return result.ToArray();
    //}

    [WebMethod]
    public static GetLoanee[] LoaneeAutoCompleteData(string LoaneeCode)
    {
        List<GetLoanee> Loanee = new List<GetLoanee>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_LoaneeIDByCode", LoaneeCode, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));

        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            GetLoanee DataObj = new GetLoanee();
            DataObj.LOANEE_UNQ_ID = dtRow["LOANEE_UNQ_ID"].ToString();
            DataObj.LOANEE_NAME = dtRow["LOANEE_NAME1"].ToString();
            Loanee.Add(DataObj);
        }
        return Loanee.ToArray();
    }
    public class GetLoanee //Class for binding data
    {
        public string LOANEE_UNQ_ID { get; set; }
        public string LOANEE_NAME { get; set; }
        public string SiteAddress { get; set; }
    }












    [WebMethod]
    public static string[] LoaneeNameAutoCompleteData(string LoaneeName)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_LoaneeIDByName", LoaneeName, Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]));
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            // result.Add(dtRow["LOANEE_NAME"].ToString());
            result.Add(string.Format("{0}|{1}", dtRow["LOANEE_NAME1"].ToString(), dtRow["LOANEE_UNQ_ID"].ToString()));

            //string str = dtRow["LOANEE_NAME"].ToString();
        }
        return result.ToArray();
    }

    [WebMethod]
    public static string[] LoanTypeAutoCompleteData(string LoanType)
    {
        List<string> result = new List<string>();
        DataTable dtLoaneeData = DBHandler.GetResult("Search_LoanType", LoanType);
        foreach (DataRow dtRow in dtLoaneeData.Rows)
        {
            result.Add(string.Format("{0}|{1}", dtRow["LoanType"].ToString(), dtRow["LoanTypeID"].ToString()));
        }
        return result.ToArray();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static LoaneeSearchDetail[] LoaneeSeaarch(string loaneeUnqID)
    {
        HttpContext.Current.Session["LoanDetail"] = null;
        List<LoaneeSearchDetail> Detail = new List<LoaneeSearchDetail>();
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtLoaneeDetail = DBHandler.GetResult("Search_LoaneeDetail", loaneeUnqID, SectorID);
        dtLoaneeDetail.Columns.Add("CheckStatus");
        decimal totOS = 0;
        if (dtLoaneeDetail.Rows.Count > 0)
        {
            HttpContext.Current.Session["LoanDetail"] = dtLoaneeDetail;
            for (int i = 0; i < dtLoaneeDetail.Rows.Count; i++)
            {
                LoaneeSearchDetail DataObj = new LoaneeSearchDetail();
                DataObj.ProYear = dtLoaneeDetail.Rows[i]["ProYear"].ToString();
                DataObj.InstallmentAmount1 = ((dtLoaneeDetail.Rows[i]["InstallmentAmount1"].ToString()) == "") ? "0" : (dtLoaneeDetail.Rows[i]["InstallmentAmount1"].ToString());
                DataObj.H_code = dtLoaneeDetail.Rows[i]["HealthCode"].ToString();
                DataObj.dr = ((dtLoaneeDetail.Rows[i]["CumlDebitAmount"].ToString()) == "") ? "0" : (dtLoaneeDetail.Rows[i]["CumlDebitAmount"].ToString());
                DataObj.cr = ((dtLoaneeDetail.Rows[i]["CumlCreditAmount"].ToString()) == "") ? "0" : (dtLoaneeDetail.Rows[i]["CumlCreditAmount"].ToString());

                //totOS = Convert.ToDecimal(dtLoaneeDetail.Rows[i]["CumlDebitAmount"].ToString()) + Convert.ToDecimal(dtLoaneeDetail.Rows[i]["CumlCreditAmount"].ToString());
                totOS = 0;
                totOS = Convert.ToDecimal(DataObj.dr.ToString()) - Convert.ToDecimal(DataObj.cr.ToString());
                DataObj.tot = totOS.ToString();

                DataObj.Pri_Default = ((dtLoaneeDetail.Rows[i]["PrincipleDefault"].ToString()) == "") ? "0" : (dtLoaneeDetail.Rows[i]["PrincipleDefault"].ToString());
                DataObj.Inst_Default = ((dtLoaneeDetail.Rows[i]["InterestDefault"].ToString()) == "") ? "0" : (dtLoaneeDetail.Rows[i]["InterestDefault"].ToString());
                DataObj.Oth_Default = ((dtLoaneeDetail.Rows[i]["OtherDefault"].ToString()) == "") ? "0" : (dtLoaneeDetail.Rows[i]["OtherDefault"].ToString());

                DataObj.Status = dtLoaneeDetail.Rows[i]["Status"].ToString();


                Detail.Add(DataObj);
                totOS = 0;
            }
        }
        return Detail.ToArray();
    }

    public class LoaneeSearchDetail
    {
        public string ProYear { get; set; }
        public string InstallmentAmount1 { get; set; }
        public string H_code { get; set; }
        public string dr { get; set; }
        public string cr { get; set; }
        public string tot { get; set; }
        public string Pri_Default { get; set; }
        public string Inst_Default { get; set; }
        public string Oth_Default { get; set; }
        public string Status { get; set; }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static LoaneeMaster[] LoaneeMasterSearch(string loaneeUnqID)
    {

        List<LoaneeMaster> Detail = new List<LoaneeMaster>();
        int SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);
        DataTable dtLoaneeMaster = DBHandler.GetResult("Search_LoaneeMaster", loaneeUnqID, SectorID);

        if (dtLoaneeMaster.Rows.Count > 0)
        {

            for (int i = 0; i < dtLoaneeMaster.Rows.Count; i++)
            {
                LoaneeMaster DataObj = new LoaneeMaster();
                DataObj.LoaneeUnqID = dtLoaneeMaster.Rows[i]["LOANEE_UNQ_ID"].ToString();
                DataObj.LooneeID = dtLoaneeMaster.Rows[i]["LOANEE_ID"].ToString();
                DataObj.GLID = dtLoaneeMaster.Rows[i]["GL_ID"].ToString();
                DataObj.LooneeCode = dtLoaneeMaster.Rows[i]["LOANEE_CODE"].ToString();
                DataObj.LooneeName = dtLoaneeMaster.Rows[i]["LOANEE_NAME"].ToString();
                DataObj.LoanType = dtLoaneeMaster.Rows[i]["LOAN_TYPE"].ToString();
                DataObj.LooneeOriCode = dtLoaneeMaster.Rows[i]["ORIGINAL_CODE"].ToString();

                DataObj.OfficeAdd = dtLoaneeMaster.Rows[i]["OFF_ADDRESS_1"].ToString();
                DataObj.OfficeAdd2 = dtLoaneeMaster.Rows[i]["OFF_ADDRESS_2"].ToString();
                DataObj.OfficeAdd3 = dtLoaneeMaster.Rows[i]["OFF_ADDRESS_3"].ToString();
                DataObj.OfficePin = dtLoaneeMaster.Rows[i]["OFF_PIN_CODE"].ToString();
                DataObj.OfficeTeleNo = dtLoaneeMaster.Rows[i]["OFF_TEL_NO"].ToString();
                DataObj.FactoryAdd = dtLoaneeMaster.Rows[i]["FCY_ADDRESS_1"].ToString();
                DataObj.FactoryAdd2 = dtLoaneeMaster.Rows[i]["FCY_ADDRESS_2"].ToString();
                DataObj.FactoryAdd3 = dtLoaneeMaster.Rows[i]["FCY_ADDRESS_3"].ToString();
                DataObj.FactoryPin = dtLoaneeMaster.Rows[i]["FCY_PIN_CODE"].ToString();
                DataObj.FactoryTeleNo = dtLoaneeMaster.Rows[i]["FCY_TEL_NO"].ToString();

                DataObj.ImpleStatus = dtLoaneeMaster.Rows[i]["IMPLI_STATUS"].ToString();
                DataObj.CommOpDate = dtLoaneeMaster.Rows[i]["DOCO"].ToString();

                DataObj.District = dtLoaneeMaster.Rows[i]["DIST_CODE"].ToString();
                DataObj.Industry = dtLoaneeMaster.Rows[i]["INDUS_CODE_ID"].ToString();
                DataObj.Constitution = dtLoaneeMaster.Rows[i]["CONST_CODE_ID"].ToString();
                DataObj.Area = dtLoaneeMaster.Rows[i]["AREA_CODE"].ToString();
                DataObj.Sector = dtLoaneeMaster.Rows[i]["SECTOR_CODE"].ToString();
                DataObj.Purpose = dtLoaneeMaster.Rows[i]["PURPOSE"].ToString();

                DataObj.ApplicationDate = dtLoaneeMaster.Rows[i]["APPLN_DT"].ToString();
                DataObj.ApplicationAmount = dtLoaneeMaster.Rows[i]["APPLN_AMT"].ToString();
                DataObj.ProjectCost = dtLoaneeMaster.Rows[i]["PROJECT_COST"].ToString();
                DataObj.SanctionDate = dtLoaneeMaster.Rows[i]["SANCT_DATE"].ToString();
                DataObj.SanctionAmount = dtLoaneeMaster.Rows[i]["SANCT_AMT"].ToString();
                DataObj.SanctionCancelDate = dtLoaneeMaster.Rows[i]["SANCAN_DT"].ToString();
                DataObj.SanctionCancelAmount = dtLoaneeMaster.Rows[i]["SANCAN_AMT"].ToString();
                DataObj.RepaymentTerm = dtLoaneeMaster.Rows[i]["REPAYMENT_TERM"].ToString();
                DataObj.RepStartDate = dtLoaneeMaster.Rows[i]["REP_ST_DT"].ToString();

                DataObj.Pri_Mortgaged = dtLoaneeMaster.Rows[i]["MPS_AMT"].ToString();
                DataObj.Pri_Hypothctd = dtLoaneeMaster.Rows[i]["HPS_AMT"].ToString();
                DataObj.Pri_Pledged = dtLoaneeMaster.Rows[i]["PPS_AMT"].ToString();
                DataObj.CoL_Mortgaged = dtLoaneeMaster.Rows[i]["MCS_AMT"].ToString();
                DataObj.CoL_Hypothctd = dtLoaneeMaster.Rows[i]["HCS_AMT"].ToString();
                DataObj.CoL_Pledged = dtLoaneeMaster.Rows[i]["PCS_AMT"].ToString();

                DataObj.ChiefPromoter = dtLoaneeMaster.Rows[i]["CHIEF_PROMOTER"].ToString();
                DataObj.PwrStatus = dtLoaneeMaster.Rows[i]["PWR_STATUS_CODE"].ToString();
                DataObj.Product = dtLoaneeMaster.Rows[i]["PRODUCT"].ToString();
                DataObj.Consultant = dtLoaneeMaster.Rows[i]["CONSULTANT"].ToString();
                DataObj.ProjectEmployment = Convert.ToString(dtLoaneeMaster.Rows[i]["EMPLOYMENT"]).ToString(); //Convert.ToInt16(dtLoaneeMaster.Rows[i]["EMPLOYMENT"]).ToString();
                DataObj.ActualEmployment = dtLoaneeMaster.Rows[i]["ACTUAL_EMPLOYMENT"].ToString();
                DataObj.ProjectedImplementionDate = dtLoaneeMaster.Rows[i]["PROJ_COMM_OPERA_DT"].ToString();
                DataObj.ActualImplementionDate = dtLoaneeMaster.Rows[i]["ACTU_COMM_OPERA_DT"].ToString();
                DataObj.SanctionBy = dtLoaneeMaster.Rows[i]["SANCTIONED_BY"].ToString();

                DataObj.MobileNo = dtLoaneeMaster.Rows[i]["MOBILE_NO"].ToString();
                //DataObj.EmailID = dtLoaneeMaster.Rows[i]["EMAIL_ID"].ToString();
                DataObj.InspectionDate = dtLoaneeMaster.Rows[i]["INSP_DATE"].ToString();
                DataObj.ProcessingOff = dtLoaneeMaster.Rows[i]["PO"].ToString();
                DataObj.DisburseOff = dtLoaneeMaster.Rows[i]["DO"].ToString();
                DataObj.RecoOff = dtLoaneeMaster.Rows[i]["RO"].ToString();
                DataObj.PrePayDate = dtLoaneeMaster.Rows[i]["PrePayDate"].ToString();
                DataObj.PrePayAmount = dtLoaneeMaster.Rows[i]["PrePayAmount"].ToString();

                DataObj.PAN_NO = dtLoaneeMaster.Rows[i]["PAN_NO"].ToString();
                DataObj.GSTNo = dtLoaneeMaster.Rows[i]["GSTNo"].ToString();

                Detail.Add(DataObj);
            }
        }
        return Detail.ToArray();
    }

    public class LoaneeMaster
    {
        public string LoaneeUnqID { get; set; }
        public string LooneeID { get; set; }
        public string GLID { get; set; }
        public string LooneeCode { get; set; }
        public string LooneeName { get; set; }
        public string LoanType { get; set; }
        public string LooneeOriCode { get; set; }

        public string OfficeAdd { get; set; }
        public string OfficeAdd2 { get; set; }
        public string OfficeAdd3 { get; set; }
        public string OfficePin { get; set; }
        public string OfficeTeleNo { get; set; }
        public string FactoryAdd { get; set; }
        public string FactoryAdd2 { get; set; }
        public string FactoryAdd3 { get; set; }
        public string FactoryPin { get; set; }
        public string FactoryTeleNo { get; set; }

        public string ImpleStatus { get; set; }
        public string CommOpDate { get; set; }

        public string District { get; set; }
        public string Industry { get; set; }
        public string Constitution { get; set; }
        public string Area { get; set; }
        public string Sector { get; set; }
        public string Purpose { get; set; }

        public string ApplicationDate { get; set; }
        public string ApplicationAmount { get; set; }
        public string ProjectCost { get; set; }
        public string SanctionDate { get; set; }
        public string SanctionAmount { get; set; }
        public string SanctionCancelDate { get; set; }
        public string SanctionCancelAmount { get; set; }
        public string RepaymentTerm { get; set; }
        public string RepStartDate { get; set; }

        public string Pri_Mortgaged { get; set; }
        public string Pri_Hypothctd { get; set; }
        public string Pri_Pledged { get; set; }
        public string CoL_Mortgaged { get; set; }
        public string CoL_Hypothctd { get; set; }
        public string CoL_Pledged { get; set; }

        public string ChiefPromoter { get; set; }
        public string PwrStatus { get; set; }
        public string Product { get; set; }
        public string Consultant { get; set; }
        public string ProjectEmployment { get; set; }
        public string ActualEmployment { get; set; }
        public string ProjectedImplementionDate { get; set; }
        public string ActualImplementionDate { get; set; }
        public string SanctionBy { get; set; }

        public string MobileNo { get; set; }
        //public string EmailID { get; set; }
        public string InspectionDate { get; set; }
        public string ProcessingOff { get; set; }
        public string DisburseOff { get; set; }
        public string RecoOff { get; set; }
        public string PrePayDate { get; set; }
        public string PrePayAmount { get; set; }
        public string PAN_NO { get; set; }
        public string GSTNo { get; set; }
    }


    private void PopulateGrid()
    {
        try
        {
            GridView grdLoanDetail = (GridView)dv.FindControl("grdLoanDetail");
            DataTable dt = new DataTable();
            dt.Columns.Add("YEAR_MONTH");
            dt.Columns.Add("INSTAL_AMT_1");
            dt.Columns.Add("Health_Code");
            dt.Columns.Add("Cuml_DR");   
            dt.Columns.Add("Cuml_CR");
            dt.Columns.Add("TOT_OS");
            dt.Columns.Add("Pri_Default");
            dt.Columns.Add("Inst_Default");
            dt.Columns.Add("Oth_Default");
            dt.Columns.Add("Status");
            dt.Rows.Add();
            grdLoanDetail.DataSource = dt;
            grdLoanDetail.DataBind();
            //grdLoanDetail.DeleteRow(0);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefreshCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    //================================= Start Coding for Checking Repayment Schedule ============================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_CheckRepaymentSchedule(string loaneeUnqID, string SectorID)
    {
        DataSet dt = DBHandler.GetResults("CheckRepaymentSchedule", loaneeUnqID, SectorID);
        return dt.GetXml();
    }
    //Data = "{StrLoaneeID:'" + LoaneeID + "',ScheduleID:'',StrSanctionAmt:'',StrRepaymentTerm:'',StrRepayStartDate:''}";
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string Post_DataForSession(string StrLoaneeID, string ScheduleID, string StrSanctionAmt, string StrRepaymentTerm, string StrRepayStartDate)
    {
        HttpContext.Current.Session[SiteConstants.SSN_LOANEE_ID] = StrLoaneeID;
        HttpContext.Current.Session[SiteConstants.SSN_SCHEDULE_ID] = ScheduleID;
        HttpContext.Current.Session[SiteConstants.SSN_SANCTION_AMT] = StrSanctionAmt;
        HttpContext.Current.Session[SiteConstants.SSN_REPAYMENT_TERM] = StrRepaymentTerm;
        HttpContext.Current.Session[SiteConstants.SSN_REPAYMENT_START_DATE] = StrRepayStartDate;
        //string a = HttpContext.Current.Session[SiteConstants.SSN_LOANEE_ID].ToString();
        return "Save In Session";
    }


    //================================= End Coding for Checking Repayment Schedule ============================================//

    //================================= Start Coding For Report Print ========================================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)] 
    public static string SetReportValue(string FormName, string ReportName, string ReportType, Int32 LoaneeID, string YearMonth, int SectorID)
    {
        string JSONVal = "";
        try
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            int UserID;
            //int SectorID;           
            string StrPaperSize = "";
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            //SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            string StrFormula = "";// "{DAT_Acc_Loan.YEAR_MONTH}>='" + RecFromDate + "' and {DAT_Acc_Loan.YEAR_MONTH}<='" + RecToDate + "' and {MST_Sector.SectorID}=" + SectorID + "";                
            StrFormula= "{MST_Acc_Loan.LOANEE_UNQ_ID}=" + LoaneeID + " and {DAT_Acc_Loan.YEAR_MONTH}='" + YearMonth + "' and {MST_Sector.SectorID}=" + SectorID + "";
            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);

            //DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, "", "", "", "", "", "", "", "", "", "");

            JSONVal = "OK";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }
    //================================= End Coding For Report Print ========================================================//

}