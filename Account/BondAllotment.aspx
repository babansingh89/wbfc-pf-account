﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="BondAllotment.aspx.cs" Inherits="BondAllotment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
        }

        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            background-color: #fff;
            font-size: 10pt !important;
            font-family: Arial !important;
        }

            table th {
                background-color: #B8DBFD;
                color: #333;
                font-weight: bold;
            }

            table, table table td {
                border: 0px solid #ccc;
            }
        /*table, th, td {
            cellpadding:1px;
            cellspacing:1px;
        }*/
        th {
            background: lightskyblue;
        }

        .selected_row {
            background-color: #A1DCF2 !important;
        }

        .Gridposition {
            margin-left: 10px;
            width: 300px;
        }

        .textposition {
            margin-left: 1px;
            width: 295px;
        }

        .GridFolioDes {
            margin-left: 1px;
            width: 570px;
        }

        .Foliotextposition {
            margin-left: 2px;
            width: 565px;
        }

        .TextSearchVendor {
            margin-left: 1px;
            width: 516px;
        }

        .GridVendor {
            margin-left: 1px;
            width: 520px;
        }

        .GridAllIndent {
            margin-left: 1px;
            width: 800px;
        }

        .input3 {
            margin-left: 1px;
            width: 200px;
        }

        /*=================================================*/
        .Grid {
            border: solid 1px #59bdcc;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

            .Grid td {
                padding: 2px;
                border: solid 1px #59bdcc;
                color: navy;
                background-color: #e6f3be;
            }

            .Grid th {
                padding: 4px 2px;
                background: #609eb3;
                font-size: 0.9em;
            }

        .auto-style1 {
            width: 802px;
        }

        .auto-style2 {
            font-family: Segoe UI;
            font-size: 12px;
            color: Navy;
            text-align: left;
            width: 802px;
        }
    </style>
    
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
        }

        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
            background-color: #fff;
            font-size: 10pt !important;
            font-family: Arial !important;
        }

            table th {
                background-color: #B8DBFD;
                color: #333;
                font-weight: bold;
            }

            table, table table td {
                border: 0px solid #ccc;
            }
        /*table, th, td {
            cellpadding:1px;
            cellspacing:1px;
        }*/
        th {
            background: lightskyblue;
        }

        .selected_row {
            background-color: #A1DCF2 !important;
        }

        .Gridposition {
            margin-left: 10px;
            width: 300px;
        }

        .textposition {
            margin-left: 1px;
            width: 295px;
        }

        .GridFolioDes {
            margin-left: 1px;
            width: 570px;
        }

        .Foliotextposition {
            margin-left: 2px;
            width: 565px;
        }

        .TextSearchVendor {
            margin-left: 1px;
            width: 516px;
        }

        .GridVendor {
            margin-left: 1px;
            width: 520px;
        }

        .GridAllIndent {
            margin-left: 1px;
            width: 800px;
        }

        .input3 {
            margin-left: 1px;
            width: 200px;
        }

        /*=================================================*/
        .Grid {
            border: solid 1px #59bdcc;
            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

            .Grid td {
                padding: 2px;
                border: solid 1px #59bdcc;
                color: navy;
                background-color: thistle;
            }

            .Grid th {
                border: solid 1px black;
                padding: 4px 2px;
                background: #609eb3;
                font-size: 0.9em;
            }

        .active {
            display: none;
        }

        #dialogFloioNo {
            background: #fff;
            padding: 12px;
            max-height: 300px;
            overflow-y: scroll;
            z-index: 10000;
            border-radius: 5px;
            box-shadow: 0, 0, 8px, #111;
        }
    </style>
    <style type="text/css">
        .textbox {
            border: 1px solid #c4c4c4;
            height: 15px;
            width: 210px;
            font-size: 13px;
            padding: 4px 4px 4px 4px;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            box-shadow: 0px 0px 8px #d9d9d9;
            -moz-box-shadow: 0px 0px 8px #d9d9d9;
            -webkit-box-shadow: 0px 0px 8px #d9d9d9;
        }

            .textbox:focus {
                outline: none;
                border: 1px solid #7bc1f7;
                box-shadow: 0px 0px 8px #7bc1f7;
                -moz-box-shadow: 0px 0px 8px #7bc1f7;
                -webkit-box-shadow: 0px 0px 8px #7bc1f7;
            }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
        });

        function beforeSave() {
            $("#frmEcom").validate();
            //        $("#txtInstrumentType").rules("add", { required: true, messages: { required: "Please enter Instrument Type"} });
            //        $("#txtInstrumentTypeDesc").rules("add", { required: true, messages: { required: "Please enter Instrument Type Description"} });        
        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }

        // COPY

        $(document).ready(function () {
            $(".DefaultButton").click(function (event) {
                event.preventDefault();
            });
            AutoComplete_SchemeID();
        });

        function AutoComplete_SchemeID() {
            $("#txtSchemeID").autocomplete({
                source: function (request, response) {

                    var W = "{Scheme_OLD_ID:'" + $('#txtSchemeID').val().trim() + "'}";
                    //    alert(W);
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "BondAllotment.aspx/GET_SchemeIDDisplay",
                        data: W,
                        dataType: "json",
                        success: function (data) {
                            //response(data.d);
                            //   alert(data.d);
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split("|")[1] + "  <>  " + item.split("|")[2],
                                    val: item,
                                }
                            }))
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                //   appendTo: "#autocompleteSchemeID",
                minLength: 0,
                select: function (e, i) {
                    var arr = i.item.val.split("|");
                    $("#txtSchemeID").val(arr[1]);
                    $("#txtSchemeName").val(arr[2]);
                    $("#txtAmount").val(arr[3]);
                    $("#txtScheme_ID").val(arr[0]);
                    //return false;
                    $('#txtAmountTotalAllot').val("");
                    $('#txtAmountTotalApply').val("");
                    //BondAllotGrid(arr[0]);
                    BondAllotGrid($("#txtScheme_ID").val(), $("#ddlStatus").val());
                }
            }).click(function () {
                //$(this).data("autocomplete").search($(this).val());
            });
        }

        $(document).ready(function () {
            $("#ddlStatus").change(function (event) {
                if ($("#ddlStatus").val() != 'NA')
                { $("#cmdSave").attr('disabled', true); } else { $("#cmdSave").attr('disabled', false); }

                if ($("#txtScheme_ID").val() != "")
                {
                    BondAllotGrid($("#txtScheme_ID").val(), $("#ddlStatus").val());
                }

            });
        });

        function BondAllotGrid(SchemeID, Status) {
            var TotAlotAmt = '0';
            var TotIssueAmt = '0';
            var W = "{SchemeID:'" + SchemeID + "',Status:'" + Status + "'}";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "BondAllotment.aspx/GET_BondAllotGridpopu",
                data: W,
                dataType: "json",
                success: function (data) {
                    $("#lblTotalRows").text('');
                    $("#gvDetails").empty();
                    $("#gvDetails").append("<tr><th>DEPOSIT ID</th><TH>APPLICATION NO </th><th>ENCASEMENT DATE</th><th>APPLY AMOUNT</th><th>APPLICANT NAME </th><th>ALLOTMENT AMOUNT </th></tr>");
                    if ($("#ddlStatus").val() == 'NA') {
                        for (var i = 0; i < data.d.length; i++) {
                            $("#gvDetails").append("<tr><td>" + data.d[i].DEPOSIT_ID + "</td><td>" +
                                data.d[i].APPL_NO + "</td><td>" + data.d[i].APPLICATION_DATE + "</td><td>" +
                                data.d[i].APPL_AMOUNT + "</td><td>" + data.d[i].APPLICANT_NAME + "</td><td><input type='text' id='txtAllotAmnt' style='background-color:#d8bfd8;width:150px;height:25px; border:none; font-size:medium;' onkeyup='CountTotalApplyAndIssueAmount()'  value=" +
                                data.d[i].ISSU_AMOUNT + "></td><td><div onclick='SingleAllotment(" + data.d[i].DEPOSIT_ID + ")'>Allot This</div></td></tr>");
                            $("input[id*=txtAllotAmnt]").prop("disabled", true);
                            $("#lblTotalRows").text(data.d.length);
                        }

                    } else {
                        for (var i = 0; i < data.d.length; i++) {
                            $("#gvDetails").append("<tr><td>" + data.d[i].DEPOSIT_ID + "</td><td>" +
                                data.d[i].APPL_NO + "</td><td>" + data.d[i].APPLICATION_DATE + "</td><td>" +
                                data.d[i].APPL_AMOUNT + "</td><td>" + data.d[i].APPLICANT_NAME + "</td><td><input type='text' id='txtAllotAmnt' style='background-color:#d8bfd8;width:150px;height:25px; border:none; font-size:medium;' onkeyup='CountTotalApplyAndIssueAmount()'  value=" +
                                data.d[i].ISSU_AMOUNT + "></td></tr>");
                            TotAlotAmt = parseFloat(TotAlotAmt) + parseFloat(data.d[i].APPL_AMOUNT);
                            TotIssueAmt = parseFloat(TotIssueAmt) + parseFloat(data.d[i].ISSU_AMOUNT);
                            $("input[id*=txtAllotAmnt]").prop("disabled", true);
                            $("#lblTotalRows").text(data.d.length);
                        }
                    }
                    CountTotalApplyAndIssueAmount();
                },
                error: function (result) {
                    alert("Error");
                }
            });

        }

        function SingleAllotment(Deposit_ID) {
            var GridView = document.getElementById("gvDetails");
            for (var row = 1; row < GridView.rows.length; row++) {

                var GridApplyAmt_Cell0 = GridView.rows[row].cells[0];
                var DepositID = GridApplyAmt_Cell0.textContent.toString();

                //alert(DepositID +"/"+ Deposit_ID);
                var GridApplyAmt_Cell1 = GridView.rows[row].cells[3];
                var ApplyAmount = GridApplyAmt_Cell1.textContent.toString();

                var ValAllotAmt = $("input[id*=txtAllotAmnt]");
                var IssueAmount = ValAllotAmt[row - 1].value.trim();

                if (DepositID == Deposit_ID) {
                    //ValTds = valueTds;
                    //ValTdsID = GridViewTds.rows[row].cells[0].textContent.toString();
                    break;
                }
            }

            if (parseInt(IssueAmount) == 0 || IssueAmount == "")
            { alert("please enter allotment amount!!"); return false; }

            var TotalAmount = parseInt($('#txtAmount').val());
            if (parseInt(TotalAmount) >= parseInt(IssueAmount)) {
                if (parseInt(ApplyAmount) >= parseInt(IssueAmount)) {
                    AddBondAllotment($('#txtScheme_ID').val(), DepositID, IssueAmount);
                }
                else {
                    alert("Allotment amount should not greater than Apply amount!!!"); return false;
                }

            }
            else {
                alert(" Allotment Amount Should not greater than Total Amount!!!"); return false;
            }

            BondAllotGrid($("#txtScheme_ID").val(), $("#ddlStatus").val());
        }

        $(document).ready(function () {
            $("#cmdSave").click(function (event) {
                var GridGroup = document.getElementById("gvDetails");
                //alert(GridGroup.rows.length);
                if (GridGroup.rows.length < 2)
                { alert("There are no Record for allotment!!"); return false; }

                if ($('#txtAmountTotalAllot').val() != "" || $('#txtAmountTotalAllot').val() != 0) {
                    //var GridGroup = document.getElementById("gvDetails");
                    for (var row = 1; row < GridGroup.rows.length; row++) {

                        var GridApplyAmt_Cell0 = GridGroup.rows[row].cells[0];
                        var DepositID = GridApplyAmt_Cell0.textContent.toString();

                        var GridApplyAmt_Cell1 = GridGroup.rows[row].cells[3];
                        var ApplyAmount = GridApplyAmt_Cell1.textContent.toString();
                        //    alert(DepositID);
                        var ValAllotAmt = $("input[id*=txtAllotAmnt]");
                        var IssueAmount = ValAllotAmt[row - 1].value.trim();
                        if (IssueAmount.trim() == "")
                        { IssueAmount = '0'; }
                        var TotalAllotAmount = parseInt($('#txtAmountTotalAllot').val());
                        var TotalAmount = parseInt($('#txtAmount').val());

                        if (parseInt(TotalAmount) >= parseInt(TotalAllotAmount)) {

                            if (parseInt(ApplyAmount) >= parseInt(IssueAmount)) {
                                //if(parseInt($('#txtAmount').val()))
                                //alert('IssueAmount =' + IssueAmount + " ApplyAmount " + ApplyAmount + "  " + DepositID);
                                AddBondAllotment($('#txtScheme_ID').val(), DepositID, IssueAmount);
                            } else { alert("Allotment amount should not greater than Apply amount!!!"); return false; }
                            //    alert(UpdtValue);alert('IssueAmount =' + IssueAmount + " ApplyAmount " + ApplyAmount + "  " + DepositID);
                        } else { alert("Total Allotment Amount Should not greater than Total Amount!!!"); return false; }
                    }
                    alert("Allotment Successfull");
                } else { alert("There are no Record for allotment!!"); return false; }
            });
        });

        function AddBondAllotment(SchemeID, DepositID, IssueAmount) {

            var W = "{SchemeID:'" + SchemeID + "', DepositID:'" + DepositID + "', IssueAmount:'" + IssueAmount + "'}";
            //        alert(W);
            $.ajax({
                type: "POST",
                url: "BondAllotment.aspx/GET_BondAllotmentProceed",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (JSONVal) {
                    //alert("Allotment Successfull");
                },
                error: function (JSONVal) {
                }
            });
        }

        function CountTotalApplyAndIssueAmount() {
            var GridItemDetail = document.getElementById("gvDetails");
            var TotalAmount = '';
            var Sum = 0;
            //alert(GridItemDetail.rows.length);
            for (var row = 1; row < GridItemDetail.rows.length; row++) {
                var GridApplyAmount = GridItemDetail.rows[row].cells[3];
                var GValAllo = GridApplyAmount.textContent.toString();
                if (GValAllo == '') {
                    GValAllo = 0;
                }
                //        alert(GValAllo);
                if (TotalAmount == '') {
                    TotalAmount = GValAllo;
                }
                else {
                    TotalAmount = parseFloat(TotalAmount) + parseFloat(GValAllo);
                }
            }
            $("#txtAmountTotalApply").val(TotalAmount);

            for (var row = 1; row < GridItemDetail.rows.length; row++) {
                var txtAmountAllot = $("input[id*=txtAllotAmnt]");
                var Amount = txtAmountAllot[row - 1].value.trim();
                if (Amount == '')
                { Amount = 0; }
                Sum = parseFloat(Sum) + parseFloat(Amount);
                //        alert(Sum);
                $("#txtAmountTotalAllot").val(parseFloat(Sum));
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Bond Allotment </td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False"
                        OnModeChanging="dv_ModeChanging" DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                   
                                    <td colspan="2" >Scheme ID &nbsp;<span class="require">*</span>&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp; 
                                           <asp:TextBox ID="txtSchemeID" autocomplete="off" ClientIDMode="Static" Width="380px" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:TextBox ID="txtScheme_ID" autocomplete="off" Style="display: none;" ClientIDMode="Static" runat="server"></asp:TextBox>
                                        </td>
                                     <td>Status &nbsp;<span class="require">*</span>&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;&nbsp; 
                                        <asp:DropDownList autocomplete="off" ID="ddlStatus" Width="170px" Height="25px" CssClass="textbox" runat="server">
											<asp:ListItem Text="Not-Allotted Records" Value="NA"></asp:ListItem>
											<asp:ListItem Text="Allotted Records" Value="A"></asp:ListItem>
											<asp:ListItem Text="All Records" Value="AL"></asp:ListItem>											
										</asp:DropDownList>
                                    </td>
                                  <%--  <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtSchemeID" ClientIDMode="Static" Width="400px" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:TextBox ID="txtScheme_ID" Style="display: none;" ClientIDMode="Static" runat="server"></asp:TextBox>
                                        <%--<div id="autocompleteSchemeID" style="position:absolute;"></div>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td>
                                        <%--<asp:TextBox ID="txtSchemeName" MaxLength="100" ReadOnly="true" placeholder="Scheme Name" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>--%>
                                       Floated Amount &nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;
                                         <asp:TextBox ID="txtAmount" autocomplete="off" MaxLength="30"  ReadOnly="true" Width="150px"  ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                    <td>Total Apply Amount&nbsp;&nbsp;:&nbsp;&nbsp;
                                         <asp:TextBox ID="txtAmountTotalApply" autocomplete="off" Width="150px" ReadOnly="true" MaxLength="10" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox></td>
                                    <td>Total Allotment Amount &nbsp;&nbsp;:&nbsp;&nbsp;
                                           <asp:TextBox ID="txtAmountTotalAllot" autocomplete="off" Width="150px" MaxLength="10" ReadOnly="true" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                     
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; max-height: 300px; width: 100%">
                       
                        <asp:GridView ID="gvDetails" autocomplete="off" runat="server" CssClass="Grid" AutoGenerateColumns="false">
                            <HeaderStyle BackColor="#DC5807" Font-Bold="true" ForeColor="White" />
                            <Columns>
                                <asp:BoundField DataField="DEPOSIT_ID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="DEPOSIT ID" />
                                <asp:BoundField DataField="APPL_NO" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="APPLICATION NO" />
                                <asp:BoundField DataField="APPL_DT" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="ENCASEMENT DATE" />
                                <asp:BoundField DataField="APPL_AMOUNT" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="APPLY AMOUNT" />
                                <asp:BoundField DataField="APPLICANT_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="APPLICANT NAME" />
                                <asp:BoundField DataField="APPLICANT_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="ISSUE AMOUNT" />
                           
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                    <div>
                        Total No. of ROWs: 
                    <asp:Label ID="lblTotalRows" autocomplete="off" ClientIDMode="Static" runat="server" Font-Bold="true"></asp:Label>
                </div>

                    <%-- <section>--%>
                    <%-- <div style="" align="center">
                            Total Apply Amount&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtAmountTotalApply" Width="100px" ReadOnly="true" MaxLength="10" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Total Allotment Amount &nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtAmountTotalAllot" Width="100px" MaxLength="10" ReadOnly="true" ClientIDMode="Static" runat="server" CssClass="textbox"></asp:TextBox>
                        </div>--%>
                    <br />
                    <div style="padding-left: 500PX;">
                        <%-- <asp:Button ID="cmdSave" runat="server" Text="Update" CommandName="Add"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                />--%>
                        <%--<asp:Button ID="cmdSave" runat="server" Text="Save" CommandName="Add"
                            Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()' />--%>
                        <asp:Button ID="cmdCancel" runat="server" Text="Refresh" autocomplete="off"
                            Width="100px" CssClass="save-button" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                    </div>
                    <%--  </section>--%>                   
                </td>
            </tr>

            
        </table>
    </div>
    <div id="dialogSchemeID" style="display: none;">
        <asp:TextBox ID="TxtAllSearchSchemeID" autocomplete="off" runat="server" Style="width: 100%;" MaxLength="10"></asp:TextBox>
        <div style="overflow-y: scroll; max-height: 300px;">
            <asp:GridView ID="GridViewSchemeID" autocomplete="off" runat="server" Style="border: 1px solid #59bdcc; width: 100%;"
                AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="OLD_SCHEME_ID" HeaderText="OLD_SCHEME_ID" ItemStyle-Width="150" />
                    <asp:BoundField DataField="SCHEME_NAME" HeaderText="SCHEME_NAME" ItemStyle-Width="150" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
