﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Globalization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class SanctionReport : System.Web.UI.Page
{
    static string StrFormula = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }

            if (!IsPostBack)
            {
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod(EnableSession = true)]    
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetReportValue(string FormName, string ReportName, string ReportType, string SacFromDate, string SacToDate, string ReportID, string RptShortBy, string TotProjCost,int SecID)
    {
        string JSONVal = "";       
        try
        {
            System.Web.HttpContext.Current.Session[SiteConstants.StrFormName] = FormName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportName] = ReportName;
            System.Web.HttpContext.Current.Session[SiteConstants.StrReportType] = ReportType;
            int UserID;
            int SectorID;           
            string StrPaperSize = "";       
            StrPaperSize = "Page - A4";
            UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
            SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

            StrFormula = "";                
            DataTable DTCheckReportVal = DBHandler.GetResult("Get_Check_ReportValue", UserID, FormName, ReportName, ReportType, StrFormula, StrPaperSize);
            if (ReportID == "SR")
            { DataTable RepotQueryCall1 = DBHandler.GetResult("Get_Set_ReportValue", UserID, FormName, ReportName, SacFromDate, SacToDate, SecID, RptShortBy, TotProjCost, "", "", "", "", ""); }            
            
            JSONVal = "OK";
        }  
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }

}