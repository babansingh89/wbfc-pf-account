﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="MenuAccess.aspx.cs" Inherits="MenuAccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/MenuAccess.js?v=2"></script>
    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Menu Access</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>

                                    <td class="labelCaption">Module &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlModule" autocomplete="off" Width="200px" Height="25px" ClientIDMode="Static" AppendDataBoundItems="true" runat="server" CssClass="inputbox2" ForeColor="#18588a">
                                            <%-- <asp:ListItem Text="-Select User Type-" Value="0"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>

                                    <td class="labelCaption">User Name &nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUserName" autocomplete="off" Width="300px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px;" class="labelCaption">Menu List</td>
                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                    <td colspan="4" style="padding: 5px;" align="left">
                                        <div style="width: 930px; overflow-y: scroll; font-size: 10px" id="chkMenuContainer">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>

                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Submit" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton"  />

                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button " OnClick="cmdCancel_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>
