﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="Insurance_Master.aspx.cs" Inherits="Insurance_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

        });

        function beforeSave() {
            $("#frmEcom").validate();

            $("#txtInsCode").rules("add", { required: true, messages: { required: "Please enter Insurance Code" } });
            //$("#txtMaxSizeLimit").rules("add", { required: true, number: true, messages: { required: "Please enter Maximum Limit Size", number: "Please enter valid number" } });
            $("#txtInsName").rules("add", { required: true, messages: { required: "Please enter Insurance Name" } });
            $("#txtAdd1").rules("add", { required: true, messages: { required: "Please enter Address" } });
            $("#txtAdd2").rules("add", { required: true, messages: { required: "Please enter Address" } });
            $("#txtAdd3").rules("add", { required: true, messages: { required: "Please enter Address" } });

        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" Runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>INSURANCE MASTER</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False"
                       OnModeChanging="dv_ModeChanging"  DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                       
                         <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    
                                    <td class="labelCaption">Insurance Code&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox autocomplete="off" ID="txtInsCode" MaxLength="20" ClientIDMode="Static" runat="server" width="200px"   CssClass="inputbox2"></asp:TextBox>
                                    </td>
                            </tr>
                                <tr>
                                    <td class="labelCaption">Insurance Name&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox autocomplete="off" ID="txtInsName" MaxLength="40" ClientIDMode="Static" runat="server" width="300px"  CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>

                                <tr>
                                    <td class="labelCaption">Address1&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAdd1" autocomplete="off" MaxLength="30" ClientIDMode="Static" runat="server" width="500px" Height="30px" TextMode="MultiLine" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                </tr>

                                <tr>
                                    <td class="labelCaption">Address2&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAdd2" autocomplete="off" MaxLength="30" ClientIDMode="Static" runat="server" width="500px" Height="30px" TextMode="MultiLine" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                </tr>

                                <tr>
                                    <td class="labelCaption">Address3&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAdd3" autocomplete="off" MaxLength="30" ClientIDMode="Static" runat="server" width="500px" Height="30px" TextMode="MultiLine" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                </tr>

                            </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    
                                    <td class="labelCaption">Insurance Code&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInsCode" autocomplete="off" MaxLength="20" ClientIDMode="Static" Text='<%# Eval("INS_CODE") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                            </tr>
                                <tr>
                                    <td class="labelCaption">Insurance Name&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtInsName"  autocomplete="off" MaxLength="40" ClientIDMode="Static" Text='<%# Eval("INS_NAME") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>

                                </tr>

                                <tr>
                                    <td class="labelCaption">Address1&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAdd1" MaxLength="30" autocomplete="off" ClientIDMode="Static" Text='<%# Eval("INS_ADD1") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                </tr>

                                <tr>
                                    <td class="labelCaption">Address2&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAdd2" autocomplete="off" MaxLength="30" ClientIDMode="Static" Text='<%# Eval("INS_ADD2") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                </tr>

                                <tr>
                                    <td class="labelCaption">Address3&nbsp;&nbsp;<span class="require">*</span> </td>
                                    <td class="labelCaption">:</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtAdd3" autocomplete="off" MaxLength="30" ClientIDMode="Static" Text='<%# Eval("INS_ADD3") %>' runat="server" CssClass="inputbox2"></asp:TextBox>
                                    </td>
                                    
                                </tr>    
                            </table>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <tr>
                                    <td colspan="4" class="labelCaption">
                                        <hr class="borderStyle" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelCaption"><span class="require">*</span> indicates Mandatory Field</td>
                                    <td>&nbsp;</td>
                                   <td align="left">
                                        <div style="float: left; margin-left: 200px;">
                                            <asp:Button ID="cmdSave" runat="server" Text="Create" CommandName="Add" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSave_Click" />
                                               
                                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton"  />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </FooterTemplate>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="overflow-y: scroll; height: 400px; width: 100%">
                        <asp:GridView ID="tbl" runat="server" Width="100%" align="center" GridLines="Both" autocomplete="off"
                            AutoGenerateColumns="false" DataKeyNames="INS_ID" OnRowCommand="tbl_RowCommand"  CellPadding="2" CellSpacing="2" AllowPaging="false">
                            <AlternatingRowStyle BackColor="Honeydew" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="TableHeader" />
                                    <ItemTemplate>
                                        <asp:ImageButton CommandName='Select' autocomplete="off" ImageUrl="images/Edit.jpg" OnClientClick='javascript: return unvalidate()'
                                            runat="server" ID="btnEdit" CommandArgument='<%# Eval("INS_ID") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                 <HeaderStyle CssClass="TableHeader" />
                                <ItemTemplate>
                                    <asp:ImageButton CommandName='Del' autocomplete="off" ImageUrl="images/Delete.gif" runat="server" ID="btnDelete" 
                                    OnClientClick='<%#"return Delete("+(Eval("INS_ID")).ToString()+") " %>' 
                                    CommandArgument='<%# Eval("INS_ID") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>                                
                                <asp:BoundField DataField="INS_CODE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Insurance Code" />                                
                                <asp:BoundField DataField="INS_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Insurance Name" />                                
                                <asp:BoundField DataField="INS_ADD1" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Address1" />  
                                <asp:BoundField DataField="INS_ADD2" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Address2" /> 
                                <asp:BoundField DataField="INS_ADD3" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Address3" />                               
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

