﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;


public partial class Lmn_ConstitutionUnit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_Lmn_Constitution_ByID", ID);
                Session["ID"] = ID;
                dv.DataSource = dtResult;
                dv.DataBind();

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_Lmn_ConstitutionUnit", ID);
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {

                TextBox txtConstitutionUnit_Name = (TextBox)dv.FindControl("txtConstitutionUnit_Name");

                string consUnitName = txtConstitutionUnit_Name.Text.Trim();
                DataTable dtchk_consUnit = DBHandler.GetResult("Get_DupConsUnitCheck", consUnitName);

                if (dtchk_consUnit.Rows.Count > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This Constitution Unit " + " " + " " + consUnitName + " " + " " + " Already in use." + " " + " Please Provide Another Unit Name')</script>");
                }
                else
                {
                    DBHandler.Execute("Insert_Lmn_ConstitutionUnit", txtConstitutionUnit_Name.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                    cmdSave.Text = "Create";
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
                }
            }
            else if (cmdSave.CommandName == "Edit")
            {


                TextBox valtxtConstitutionUnit_Name = (TextBox)dv.FindControl("txtConstitutionUnit_Name");

                string consUnitName = valtxtConstitutionUnit_Name.Text.Trim();
                DataTable dtchk_consUnit = DBHandler.GetResult("Get_DupConsUnitCheck", consUnitName);

                if (dtchk_consUnit.Rows.Count > 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This Constitution Unit " + " " + " " + consUnitName + " " + " " + " Already in use." + " " + " Please Provide Another Unit Name')</script>");
                }
                else
                {
                    //TextBox valConstitutionUnitTypeID = (TextBox)dv.FindControl("ConstitutionUnitTypeID");
                    String ID = (String)Session["ID"];

                    //string ID = ((HiddenField)dv.FindControl("hdnID")).Value;

                    DBHandler.Execute("Update_Lmn_ConstitutionUnit", ID, valtxtConstitutionUnit_Name.Text, Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                    cmdSave.Text = "Create";
                    dv.ChangeMode(FormViewMode.Insert);
                    PopulateGrid();
                    dv.DataBind();
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                    Session["ID"] = null;
                }
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Lmn_Constitutionunit");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //protected DataTable drpload()
    //{
    //    try
    //    {
    //        DataTable dt = DBHandler.GetResult("LoadCountry");
    //        return dt;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}
}