﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Web.Services;
using System.Text;
using System.Web.Script.Services;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Data.SqlClient;

using System.Globalization;
public partial class ACC_Sl_Sub : System.Web.UI.Page
{
    static string hdnID = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("Get_Acc_sl_sub_BYID", ID);
                dv.DataSource = dtResult;
                dv.DataBind();
                hdnSubID.Text = ID;
                hdnID = dtResult.Rows[0]["SUB_ID"].ToString();
            }   
            else if (flag == 2)
            {
                DataTable dtSubID = DBHandler.GetResult("Get_CheckSubIDOnVoucher", ID);
                if (dtSubID.Rows.Count > 0)
                 {
                     ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Can Not Delete Record! There are many records with this Sub ID in Voucher Detail')</script>");

                 }
                 else
                 {
                     DBHandler.Execute("Delete_Acc_SL_Sub_id_ByID", ID);
                     dv.ChangeMode(FormViewMode.Insert);
                     PopulateGrid();
                     dv.DataBind();
                     ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                 }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            int UserID;
            int SectorID;
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                string ValSubID = "";
                TextBox txtOldSlSub = (TextBox)dv.FindControl("txtOldSlSub");
                TextBox txtSubName = (TextBox)dv.FindControl("txtSubName");
                TextBox txtSubType = (TextBox)dv.FindControl("txtSubType");
                //TextBox hdnSubID = (TextBox)dv.FindControl("hdnSubID");
                
                UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

                if (hdnSubID.Text == "")
                {
                    ValSubID = "0";
                }

                DataTable dtGetData = DBHandler.GetResult("Get_DupCheckOLD_SUB_ID", ValSubID, txtOldSlSub.Text);
                 if (dtGetData.Rows.Count > 0)
                 {
                     ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Sub ID  Already Exists!')</script>");
                 }
                 else
                 {
                     DataTable dtResult = DBHandler.GetResult("Insert_Acc_SL_Sub", txtOldSlSub.Text, txtSubName.Text, txtSubType.Text, UserID);
                     if (dtResult.Rows[0]["Ststus"].ToString() == "N")
                     {
                         ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This Old Sub ID already inserted.')</script>");
                         return;
                     }
                     cmdSave.Text = "Create";
                     dv.ChangeMode(FormViewMode.Insert);
                     PopulateGrid();
                     dv.DataBind();
                     ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Created Successfully.')</script>");
                 }
            }
            else if (cmdSave.CommandName == "Edit")
            {

                TextBox txtOldSlSub = (TextBox)dv.FindControl("txtOldSlSub");
                TextBox txtSubName = (TextBox)dv.FindControl("txtSubName");
                TextBox txtSubType = (TextBox)dv.FindControl("txtSubType");

                UserID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_INT_USER_ID]);
                SectorID = Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]);

                string ID = hdnID;


                DataTable dtResult = DBHandler.GetResult("Update_Acc_SL_Sub", ID, txtOldSlSub.Text, txtSubName.Text, txtSubType.Text, UserID);
                if (dtResult.Rows[0]["Ststus"].ToString() == "N")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('This Old Sub ID already inserted.')</script>");
                    return;
                }
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_AccSlSub");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    //protected DataTable drpload()
    //{
    //    try
    //    {
    //        DataTable dt = DBHandler.GetResult("LoadCountry");
    //        return dt;
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    //============================ Start Duplicate Check Old Sub ID Coding Start here===================================//
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string GET_DupChkOldSubID(string SubID, string OldSubID)
    {
        string ValOldsubID = "";
        DataTable dtGetData = DBHandler.GetResult("Get_DupCheckOLD_SUB_ID", SubID, OldSubID);
        if (dtGetData.Rows.Count > 0)
        {
            ValOldsubID = Convert.ToString(dtGetData.Rows[0]["OLD_SUB_ID"]);
        }

        JavaScriptSerializer jscript = new JavaScriptSerializer();
        return jscript.Serialize(ValOldsubID);
    }
    //============================End Duplicate Check Old Sub Coding Start here===================================//
}