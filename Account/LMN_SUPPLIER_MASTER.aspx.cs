﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using DataAccess;
using System.Data.SqlClient;


public partial class LMN_SUPPLIER_MASTER : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)  
            {
                PopulateGrid();
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {
                PopulateGrid(); 
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    private void UpdateDeleteRecord(int flag, string ID)
    {
        try
        {
            if (flag == 1)
            {
                DataTable dtResult = DBHandler.GetResult("GET_Supplier_master_By_ID", ID);
                Session["ID"] = dtResult.Rows[0]["VENDOR_CODE"].ToString();

                dv.DataSource = dtResult;
                dv.DataBind();
                if (dv.CurrentMode == FormViewMode.Edit)
                {
                    PopulateGrid();
                }
               

            }
            else if (flag == 2)
            {
                DBHandler.Execute("Delete_Supplier_master_by_ID", ID);
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Deleted Successfully.')</script>");
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    private void PopulateGrid()
    {
        try
        {
            DataTable dt = DBHandler.GetResult("Get_Supplier_master");
            tbl.DataSource = dt;
            tbl.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {

            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox TxtVendorName = (TextBox)dv.FindControl("TxtVendorName");
                TextBox TxtAddress1 = (TextBox)dv.FindControl("TxtAddress1");
                TextBox TxtAddress2 = (TextBox)dv.FindControl("TxtAddress2");
                TextBox TxtAddress3 = (TextBox)dv.FindControl("TxtAddress3");
                TextBox TxtPin = (TextBox)dv.FindControl("TxtPin");
                TextBox TxtPhone = (TextBox)dv.FindControl("TxtPhone");
                TextBox TxtPan = (TextBox)dv.FindControl("TxtPan");
                TextBox TxtVat = (TextBox)dv.FindControl("TxtVat");
                TextBox TxtCST = (TextBox)dv.FindControl("TxtCST");
                TextBox TxtST = (TextBox)dv.FindControl("TxtST");

                DBHandler.Execute("INSERT_Supplier_Master",
                TxtVendorName.Text.Equals("") ? DBNull.Value : (object)TxtVendorName.Text,
                TxtAddress1.Text.Equals("") ? DBNull.Value : (object)TxtAddress1.Text,
                TxtAddress2.Text.Equals("") ? DBNull.Value : (object)TxtAddress2.Text,
                TxtAddress3.Text.Equals("") ? DBNull.Value : (object)TxtAddress3.Text,
                TxtPin.Text.Equals("") ? DBNull.Value : (object)TxtPin.Text,
                TxtPhone.Text.Equals("") ? DBNull.Value : (object)TxtPhone.Text,
                TxtPan.Text.Equals("") ? DBNull.Value : (object)TxtPan.Text,
                TxtVat.Text.Equals("") ? DBNull.Value : (object)TxtVat.Text,
                TxtCST.Text.Equals("") ? DBNull.Value : (object)TxtCST.Text,
                TxtST.Text.Equals("") ? DBNull.Value : (object)TxtST.Text,
                Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]),
                Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));

                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Inserted Successfully.')</script>");
                cmdSave.Text = "Create";
                dv.ChangeMode(FormViewMode.Insert);
                dv.DataBind();
                PopulateGrid();


            }
            else if (cmdSave.CommandName == "Edit")
            {
                TextBox TxtVendorName = (TextBox)dv.FindControl("TxtVendorName");
                TextBox TxtAddress1 = (TextBox)dv.FindControl("TxtAddress1");
                TextBox TxtAddress2 = (TextBox)dv.FindControl("TxtAddress2");
                TextBox TxtAddress3 = (TextBox)dv.FindControl("TxtAddress3");
                TextBox TxtPin = (TextBox)dv.FindControl("TxtPin");
                TextBox TxtPhone = (TextBox)dv.FindControl("TxtPhone");
                TextBox TxtPan = (TextBox)dv.FindControl("TxtPan");
                TextBox TxtVat = (TextBox)dv.FindControl("TxtVat");
                TextBox TxtCST = (TextBox)dv.FindControl("TxtCST");
                TextBox TxtST = (TextBox)dv.FindControl("TxtST");
                string ID = (string)Session["ID"];

                DBHandler.Execute("Update_SUpplier_Master",
                ID,
                TxtVendorName.Text.Equals("") ? DBNull.Value : (object)TxtVendorName.Text,
                TxtAddress1.Text.Equals("") ? DBNull.Value : (object)TxtAddress1.Text,
                TxtAddress2.Text.Equals("") ? DBNull.Value : (object)TxtAddress2.Text,
                TxtAddress3.Text.Equals("") ? DBNull.Value : (object)TxtAddress3.Text,
                TxtPin.Text.Equals("") ? DBNull.Value : (object)TxtPin.Text,
                TxtPhone.Text.Equals("") ? DBNull.Value : (object)TxtPhone.Text,
                TxtPan.Text.Equals("") ? DBNull.Value : (object)TxtPan.Text,
                TxtVat.Text.Equals("") ? DBNull.Value : (object)TxtVat.Text,
                TxtCST.Text.Equals("") ? DBNull.Value : (object)TxtCST.Text,
                TxtST.Text.Equals("") ? DBNull.Value : (object)TxtST.Text,
                Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]),
                Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]));
                
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                cmdSave.Text = "Save";
                dv.ChangeMode(FormViewMode.Insert);
                PopulateGrid();
                dv.DataBind();
                Session["ID"] = null;

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void tbl_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                dv.ChangeMode(FormViewMode.Edit);
                UpdateDeleteRecord(1, e.CommandArgument.ToString());
                Button cmdSave = (Button)dv.FindControl("cmdSave");
                cmdSave.CommandName = "Edit";
                cmdSave.ToolTip = "Update";
                cmdSave.Text = "Update";
            }
            else if (e.CommandName == "Del")
            {
                UpdateDeleteRecord(2, e.CommandArgument.ToString());
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    protected DataTable drpload()
    {
        DataTable dt = DBHandler.GetResult("JOb_category_dropDownList");
        return dt;

    }
    protected DataTable drpload1()
    {
        DataTable dt = DBHandler.GetResult("JOB_descripTion_DropDownLIST");
        return dt;
    }
    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
}