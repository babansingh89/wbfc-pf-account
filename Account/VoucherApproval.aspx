﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="VoucherApproval.aspx.cs" Inherits="VoucherApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script type="text/javascript" src="js/gridviewScroll.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            FunYearMonth();

            $("input[name='chkSearch']").click(function () {
                if ($('#A').is(":checked")) {
                    $('#searchYM').show();
                    $('#searchDate').hide();
                     
                } else {
                    $('#searchYM').hide();
                    $('#searchDate').show();
                    // $('#panGrd').hide();
                    //  $("[id*=tbl] tr:has(td):last").hide();
                }
            });
           
            $("#ddlUserSector").change(function () {
               FunYearMonth();
                return false;
            });

        });


        function FunYearMonth() {
            if ($('#txtYearmonth').val() != '') { $('#txtYearmonth').select(); return false; }
            var SecID = '';
            SecID = $("#ddlUserSector").val(); 
               $(".loading-overlay").show();
            var W = "{SecID:"+ SecID +"}";
            $.ajax({
                type: "POST",
                url: "VoucherApproval.aspx/GET_YearMonth",
                contentType: "application/json;charset=utf-8",
                data: W,
                dataType: "json",
                success: function (data) {
                    if (data.d.length > 0) {
                        var strYearMonth = data.d;
                        $('#txtYearmonth').val(strYearMonth);
                        $('#txtYearmonth').select();
                        $('#txtYearmonth').focus();
                        var value = $("#txtYearmonth").val();
                        value = value.match(/.{1,4}/g).join(" ");
                        var a = value.split(" ");
                        specificDate = '01/' + a[1] + '/' + a[0];
                        Specific(specificDate);
                        $(".loading-overlay").hide();
                        return false;
                    }

                },
                error: function (data) {
                    alert("Error Records Data in Year And Month");
                    $(".loading-overlay").hide();
                    $('#txtYearmonth').focus();
                    return false;
                }
            });
        }





        //$(document).ready(function () {
        //    $('#txtAprvdON').datepicker({
        //        showOtherMonths: false,
        //        selectOtherMonths: false,
        //        closeText: 'X',
        //        showAnim: 'drop',
        //        showButtonPanel: true,
        //        duration: 'slow',
        //        dateformat: 'dd/mm/yyyy'
        //    });
        //});

        var specificDate = "";
        $(document).ready(function () {
            //$("#txtYearmonth").keyup(function () {
            $("#txtYearmonth").on('keyup blur', function () {
                if ($("#txtYearmonth").val().length == 6) {
                    //   alert("");
                    var value = $("#txtYearmonth").val();
                    value = value.match(/.{1,4}/g).join(" ");
                    //  alert(value);
                    var a = value.split(" ");
                    specificDate = '01/' + a[1] + '/' + a[0];
                    //  alert(specificDate);
                    //var s=$("#txtAprvdON").val();
                    //var q=s.split
                    Specific(specificDate);
                }
            })
        })

        function Specific(specificDate) {
            $('#ddlVoucharType').val(0);
            //$('#txtFrmDate, #txtToDate').datepicker('setDate', specificDate);
            $('#txtFrmDate').datepicker('setDate', specificDate);
           
            var m1 = specificDate.split("/");
            var mm = m1[1];
            var yy = new Date(specificDate).getFullYear();
            var ld = new Date(yy, mm, 0).getDate() + '/' + mm + '/' + yy;
            $('#txtToDate').datepicker('setDate', ld);
        }

        $(document).ready(function () {
            $('#txtFrmDate, #txtToDate').datepicker({
                showOtherMonths: false,
                selectOtherMonths: false,
                closeText: 'X',
                showAnim: 'drop',
                showButtonPanel: false,
                duration: 'slow',
                dateformat: 'dd/mm/yyyy',
                hideIfNoPrevNext: false
                //beforeShowMonth: Specific(specificDate),
            });
        });

        $(document).ready(function () {
            $('#txtYearmonth').attr({ maxLength: 6 });
            $('#txtYearmonth').keypress(function (evt) {

                if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57) && evt.charCode != 46) {
                    alert("Please Enter a Numeric Value");
                    return false;
                }

            });
        });

        function beforeSave() {
            $("#frmEcom").validate();
            $("#txtYearmonth").rules("add", { required: true, messages: { required: "Please enter Year & Month" } });
            $("#ddlVoucharType").rules("add", { required: true, messages: { required: "Please select vouchar Type" } });
            $("#txtFrmDate").rules("add", { required: true, messages: { required: "Please Select From Date" } });
            $("#txtToDate").rules("add", { required: true, messages: { required: "Please Select To Date" } });
            $("#ddlUserSector").rules("add", { required: true, messages: { required: "Please Select a Unit" } });
        }

        function Delete(id) {
            if (confirm("Are You sure you want to delete?")) {
                $("#frmEcom").validate().currentForm = '';
                return true;
            } else {
                return false;
            }
        }

        function unvalidate() {
            $("#frmEcom").validate().currentForm = '';
            return true;
        }




        //$(document).ready(function () {
        //    gridviewScroll();
        //});

        //function gridviewScroll() {
        //    $('#tbl').gridviewScroll({
        //        width: 420,
        //        startHorizontal: 0,
        //        barhovercolor: "#3399FF",
        //        barcolor: "#3399FF"
        //    });
        //}

        $(document).ready(function () {

            $('[id$=chkHeader]').click(function () {
                $("[id$='chkApprove']").prop('checked', this.checked);
            });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="98%" align="center">
            <tr>
                <td>Voucher Approval</td>
            </tr>
        </table>
        <br />
        <table width="98%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td colspan="6">
                    <table class="headingCaptionHead" width="98%" align="center">
                        <tr>
                            <td style="height: 15px;" align="left">Voucher Approval Search</td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    <asp:FormView ID="dv" runat="server" Width="99%" CellPadding="4" AutoGenerateRows="False" autocomplete="off"
                        DefaultMode="Insert" HorizontalAlign="Center" GridLines="None">
                        <InsertItemTemplate>
                            <table align="center" cellpadding="5" width="100%">
                                <%--<tr>
                                    <td align="center" colspan="6">--%>
                                <%--<asp:RadioButton ID="rdSearchYear" Checked="true" class="inputbox2" runat="server" Font-Size="small" GroupName="Search" Text="Search By Year Month" />
                                        <asp:RadioButton ID="rdSearchDate" class="inputbox2" runat="server" Font-Size="small" GroupName="Search" Text="Search By From Date and To Date" />--%>
                                <%--<input type='radio' name='chkSearch' id='A' checked="checked" /><font size="2px">Search By Year Month</font>
                                        <input type='radio' name='chkSearch' id='B' /><font size="2px">Search By From Date and To Date</font>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        <div id="searchYM" style="width: 100%;">
                                            <table align="center" width="100%">
                                                <tr>
                                                    <td class="labelCaption">Year & Month(YYYYMM) &nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtYearmonth" autocomplete="off" Width="120" autofocus="true" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>

                                                    <td class="labelCaption">Vouchar Type&nbsp;&nbsp;<span class="require"></span> </td>
                                                    <td class="labelCaption">:</td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlVoucharType" autocomplete="off" runat="server" ClientIDMode="Static" CssClass="inputbox2" Width="200px" Height="25px" ForeColor="#18588a">
                                                            <asp:ListItem Text="--Select Voucher Type--" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="Normal" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="Instalment" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Interest" Value="2"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px;" class="labelCaption">From Date&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                                    <td style="padding: 5px;" align="left">
                                                        <asp:TextBox ID="txtFrmDate" autocomplete="off" ReadOnly="false" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                    <td style="padding: 5px;" class="labelCaption">To Date&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                                    <td style="padding: 5px;" align="left">
                                                        <asp:TextBox ID="txtToDate" autocomplete="off" ReadOnly="false" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 5px;" class="labelCaption">Approved By&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                                    <td style="padding: 5px;" align="left">
                                                        <asp:TextBox ID="txtAprvdBy" autocomplete="off" ReadOnly="true" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                    <td style="padding: 5px;" class="labelCaption">Approved ON&nbsp;&nbsp;<span class="require">*</span> </td>
                                                    <td style="padding: 5px;" class="labelCaption">:</td>
                                                    <td style="padding: 5px;" align="left">
                                                        <asp:TextBox ID="txtAprvdON" autocomplete="off" ReadOnly="true" Width="120" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                        <%--<div id="searchDate" style="width: 100%; display: none;">
                                            <table align="center" cellpadding="5" width="100%">
                                                
                                            </table>
                                        </div>--%>

                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <div style="float: right; margin-left: 200px;">
                                            <asp:Button ID="cmdSearch" runat="server" Text="Search" CommandName="search" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdSearch_Click" />
                                            <asp:Button ID="cmdPrint" runat="server" Text="Print" CommandName="Print" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClientClick='javascript: return beforeSave()'
                                                OnClick="cmdPrint_Click" />
                                            <asp:Button ID="cmdCancelSearch" runat="server" Text="Cancel" autocomplete="off"
                                                Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancelSearch_Click" OnClientClick='javascript: return unvalidate()' />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                        <%--<FooterTemplate>
                            
                        </FooterTemplate>--%>
                    </asp:FormView>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="labelCaption">
                    <hr class="borderStyle" />
                </td>
            </tr>
            <tr>
                <td>
                    <div id="summeryDisp" style="font: 20"><%= result %> </div>
                </td>
            </tr>
            <tr>
                <td>

                    <asp:Panel ID="panGrd" runat="server" Width="100%" Visible="false">
                        <div style="overflow-y: scroll; height: 300px; width: 40%; float: left">
                            <asp:GridView ID="tbl" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                                AutoGenerateColumns="false" DataKeyNames="YEAR_MONTH, VCH_NO, VCH_TYPE, SectorID " OnRowCommand="tbl_RowCommand" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <SelectedRowStyle BackColor="#87CEEB" />

                                <Columns>
                                    <asp:BoundField DataField="YEAR_MONTH" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader11" HeaderText="Voucher Yearmonth" />
                                    <asp:BoundField DataField="VCH_NO" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="VchNo" />
                                    <asp:BoundField DataField="VCH_TYPE" Visible="false" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="VchNo" />
                                    <asp:BoundField DataField="VCH_DATE" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Date" />
                                    <asp:BoundField DataField="VoucherNo" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher No" />
                                    <asp:BoundField DataField="VCH_AMOUNT" ItemStyle-CssClass="labelCaptionright" DataFormatString="{0:0.00}" HeaderStyle-CssClass="TableHeader" HeaderText="Voucher Amount" />
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="TableHeader" />
                                        <HeaderTemplate>
                                            Approve<br />
                                            <asp:CheckBox ID="chkHeader" autocomplete="off" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkApprove" autocomplete="off" class="headFont" runat="server" Font-Size="small" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Show Detail">
                                        <HeaderStyle CssClass="TableHeader" />
                                        <ItemTemplate>
                                            <asp:ImageButton autocomplete="off" CommandName='Select' ImageUrl="images/view.png"
                                                runat="server" ID="btnEdit" CommandArgument='<%# Eval("VCH_NO") +";"+ Eval("YEAR_MONTH") +";"+ Eval("VCH_TYPE") +";"+ Eval("SectorID")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                </Columns>
                                <%--<HeaderStyle CssClass="GridviewScrollHeader" />
                            <RowStyle CssClass="GridviewScrollItem" />
                            <PagerStyle CssClass="GridviewScrollPager" />--%>
                            </asp:GridView>
                        </div>

                        <div style="overflow-y: scroll; height: 300px; width: 60%; float: right">

                            <asp:GridView ID="grdVchDetail" autocomplete="off" runat="server" Width="100%" align="center" GridLines="Both"
                                AutoGenerateColumns="false" CellPadding="2" CellSpacing="2" AllowPaging="false">
                                <AlternatingRowStyle BackColor="Honeydew" />
                                <Columns>
                                    <asp:BoundField DataField="OLD_SL_ID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="SL ID" />
                                    <asp:BoundField DataField="GL_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Name" />
                                    <asp:BoundField DataField="OLD_SUB_ID" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sub ID" />
                                    <asp:BoundField DataField="SUB_NAME" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Sub Name" />
                                    <asp:BoundField DataField="DR_CR" ItemStyle-CssClass="labelCaption" HeaderStyle-CssClass="TableHeader" HeaderText="Debit/Credit" />
                                    <asp:BoundField DataField="AMOUNT" ItemStyle-CssClass="labelCaptionright" DataFormatString="{0:0.00}" HeaderStyle-CssClass="TableHeader" HeaderText="Amount" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <table align="center" cellpadding="5" width="100%">
                            <tr>
                                <td colspan="4" class="labelCaption">
                                    <hr class="borderStyle" />
                                </td>
                            </tr>
                            <tr>

                                <td colspan="6">
                                    <div style="float: right; margin-left: 200px;">
                                        <asp:Button ID="cmdApprove" runat="server" Text="Approve" CommandName="Approve" autocomplete="off"
                                            Width="100px" CssClass="save-button DefaultButton"
                                            OnClick="cmdApprove_Click" />
                                        <asp:Button ID="cmdCancel" runat="server" Text="Cancel" autocomplete="off"
                                            Width="100px" CssClass="save-button DefaultButton" OnClick="cmdCancel_Click" OnClientClick='javascript: return unvalidate()' />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>

            <tr>
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>
