﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WBFCLMNAdmin.master" AutoEventWireup="true" ViewStateEncryptionMode="Always" CodeFile="LoanLedger.aspx.cs" Inherits="LoanLedger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="js/LoanLedger.js"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             $(".DefaultButton").click(function (event) {
                 event.preventDefault();
             });
         });
    </script>

</asp:Content>
   


<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="Server">
    <div align="center">
        <table class="headingCaption" width="100%" align="center">
            <tr>
                <td>LOAN LEDGER</td>
            </tr>
        </table>
      
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr>
                <td class="labelCaption" style="font-weight:bold;">Starting period <span class="require">*&nbsp;&nbsp;</span></td>
                <td class="labelCaption"><span>:</span> </td>
                <td>
                    <asp:TextBox ID="txtStartingPeriod" autocomplete="off" MaxLength="6" Width="200px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>
                <td class="labelCaption" style="font-weight:bold;">Ending period<span class="require">*&nbsp;&nbsp;</span></td>
                <td><span>:</span> </td>
                <td>
                    <asp:TextBox ID="txtEndingPeriod" autocomplete="off" MaxLength="6" Width="200px" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>

                </td>
                <%--   <td align="center">
                   <%--  <asp:Button ID="btnCopyData" runat="server" Text="Copy data" CommandName="Copy" Width="100px" CssClass="save-button"
                          OnClientClick='javascript: return checkBlunk_btnCopyData()' /><%--OnClientClick='javascript: return beforeSave()'
                  </td>--%>
            </tr>
            <tr>
                <td class="labelCaption" style="font-weight:bold;">Starting code <span class="require">*&nbsp;&nbsp;</span></td>
                <td><span>:</span></td>
                <td>
                    <asp:TextBox ID="txtStartingCode" autocomplete="off" Width="200Px" Style="text-transform: uppercase;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                    <asp:TextBox ID="txthdnStartingCode" autocomplete="off" Width="10px" Style="text-transform: uppercase; display: none;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>
                <td class="labelCaption" style="font-weight:bold;">Ending code<span class="require">*&nbsp;&nbsp;</span></td>
                <td><span>:</span> </td>
                <td>
                    <asp:TextBox ID="txtEndingCode" autocomplete="off" Width="200Px" Style="text-transform: uppercase;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                    <asp:TextBox ID="txthdnEndingCode" autocomplete="off" Width="10px" Style="display: none;" ClientIDMode="Static" runat="server" CssClass="inputbox2"></asp:TextBox>
                </td>
                <%--<td style="padding: 5px;" class="labelCaption" align="center"><%--Select Ledger<span class="require">&nbsp;&nbsp;</span><span>:</span> 
                <asp:CheckBox ID="chckSelectLedger" runat="server" />
                  </td>--%>
            </tr>
            <tr>
               <%-- <td class="labelCaption"></td>
                <td class="labelCaption"></td>--%>
                <td class="labelCaption" style="font-weight:bold;">Select Loan type indicator <span class="require">*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                 <td><span>:</span> </td>
                <td class="labelCaption">
                    <asp:DropDownList ID="ddlLoanTypeIndicator" autocomplete="off" runat="server" Width="210px" Height="25px" Font-Bold="true" CssClass="inputbox2" ForeColor="#18588a">
                        <asp:ListItem Value="0">(Select Type)</asp:ListItem>
                        <asp:ListItem Value="A">All</asp:ListItem>
                        <asp:ListItem Value="U">Auc</asp:ListItem>
                        <asp:ListItem Value="F">Frozen</asp:ListItem>
                        <asp:ListItem Value="S">Suit File</asp:ListItem>
                        <asp:ListItem Value="I">FITL</asp:ListItem>
                        <asp:ListItem Value="N">Normal</asp:ListItem>
                        <asp:ListItem Value="L">Soft Loan</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="labelCaption">
                    <asp:CheckBox ID="chckGreaterThanZero" autocomplete="off" runat="server" TextAlign="Right" Text=" Greater than zero" Font-Bold="true" />
                    
                </td>
                <td class="labelCaption"></td>
                 <td class="labelCaption">
                     <asp:CheckBox ID="CheckBox1" autocomplete="off" TextAlign="Right" runat="server" Text=" Transaction during period" Font-Bold="true"/>
                 </td>
            </tr>
          
           
             </table>
        <table width="100%" cellpadding="5" class="borderStyle" align="center" cellspacing="0">
            <tr style="text-align: center; width: 100%">
                <td style="text-align: center; width: 40%"></td>
                <td style="text-align: center; width: 100%">
                    <asp:Button ID="btnGenerateReport" ClientIDMode="Static" runat="server" Text="Report" Width="100px" CssClass="save-button DefaultButton"
                        autocomplete="off" OnClientClick='javascript: return GeneratReport()' /><%--OnClientClick='javascript: return beforeSave()'--%>
                    <%-- </td>
                 <td style="text-align:center;width:100%"> --%>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel " Width="100px" CssClass="save-button DefaultButton"
                        autocomplete="off" OnClientClick='javascript: return checkBlunk_btnCopyData()' /><%--OnClientClick='javascript: return beforeSave()'--%>

                </td>

            </tr>
        </table>
    </div>
</asp:Content>

