﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data; 
using WebUtility;
using DataAccess;
public partial class Change_Password : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session[SiteConstants.SSN_INT_USER_ID] == null)
            {
                Response.Redirect("Default.aspx", false);
            } 
        }
        catch(Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    
    protected void cmdSave_Click(object sender, EventArgs e)
    {
        try
        {
            Button cmdSave = (Button)sender;
            if (cmdSave.CommandName == "Add")
            {
                TextBox txtOld = (TextBox)dv.FindControl("txtOldPass");
                TextBox txtNew = (TextBox)dv.FindControl("txtNewPass");
                TextBox txtConfirm = (TextBox)dv.FindControl("txtConfirmPass");
                DataTable dt = DBHandler.GetResult("Get_Password", Session[SiteConstants.SSN_INT_USER_ID].ToString(), txtOld.Text);
                //string pass = dt.Rows[0][0].ToString();
                //pass = pass.Replace(" ", string.Empty);
                //if (txtOld.Text.Trim().Equals(pass))
                if (dt.Rows[0][0].ToString() == "1")
                {
                    DBHandler.Execute("Update_PassByUserID", Session[SiteConstants.SSN_INT_USER_ID].ToString(), txtNew.Text);
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Password Updated Successfully.')</script>");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Please Enter Your Old Password Correctly.')</script>");
                }
            }
           
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    protected void dv_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        try
        {
            if (e.NewMode != FormViewMode.Insert && e.NewMode != FormViewMode.Edit)
            {

            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    


    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

   


   


}