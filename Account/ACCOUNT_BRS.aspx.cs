﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using WebUtility;
using System.Web.Services;
using System.Web.Script.Services;
using DataAccess;
using System.Globalization;
using System.Text;
using System.Web.Script;   
using System.Web.Script.Serialization;
using System.Data.SqlClient;

public partial class ACCOUNT_BRS : System.Web.UI.Page
  
{
    static string hdnBankID = "";
    int globalval = 0;     

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {   
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "clientscript", "document.getElementById('savebrs').style.visibility = 'hidden';", true);
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "clientscript", "document.getElementById('savebrs').style.display  = 'none';", true);
            if (Session[SiteConstants.SSN_SECTOR_ID] == null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "MSG", "<script>alert('Please Select The Unit from the Drop Down.');window.location.href='Welcome.aspx';</script>");
                return;
            }  
            if (!IsPostBack)
            {
                DropDownList ddlInsRecvd = (DropDownList)dv.FindControl("ddlInsRecvd");
                ddlInsRecvd.SelectedValue = "D"; ;
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }
    
    protected void tbl_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //if ((e.Row.RowState & DataControlRowState.Edit) > 0)
            //{
                DropDownList ddList = (DropDownList)e.Row.FindControl("ddlInsType");        
                DataTable dt = DBHandler.GetResult("Get_DropDownInstrumentType_Pf");
                
                ddList.DataSource = dt;
                ddList.DataTextField = "INST_TYPE_DESC";
                ddList.DataValueField = "INST_TYPE";             
                ddList.DataBind();
                ddList.Items.Insert(0, new ListItem("Select", "0"));
                ddList.SelectedIndex = 0;



                TextBox txtYrMn = (TextBox)dv.FindControl("txtYrMn");
                //HiddenField hdnBankID = (HiddenField)dv.FindControl("hdnBankID");
                //HiddenField HdnGlid = (HiddenField)dv.FindControl("HdnGlid");
                DropDownList ddlInsRecvd = (DropDownList)dv.FindControl("ddlInsRecvd");
                int ddlsector = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);
                TextBox txtInstrumentNo = (TextBox)dv.FindControl("txtInstrumentNo");
                DropDownList ddlOption = (DropDownList)dv.FindControl("ddlOption");
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "clientscript", "document.getElementById('savebrs').style.display  = 'grid';", false);
                DataTable dtS = DBHandler.GetResult("Get_InstrumentDetail_Pf", txtYrMn.Text, hdnBankID, ddlInsRecvd.SelectedValue, ddlsector, 
                    txtInstrumentNo.Text.Equals("") ? DBNull.Value : (object)txtInstrumentNo.Text, ddlOption.SelectedValue,
                    HttpContext.Current.Session["Menu_For"].ToString());
                if (dtS.Rows.Count > 0)
                {
                  //  for (int i = 0; i < dtS.Rows.Count; i++)
                  //{

                            ddList.SelectedValue = dtS.Rows[globalval]["INST_TYPE"].ToString();
                            globalval=globalval+1;
                       
                    //}
                }
            //}
        }
    }   

     
    private void PopulateGrid()
    {
        try
        {
            TextBox hdnForCurrYrMn = (TextBox)dv.FindControl("hdnForCurrYrMn");
            TextBox txtYrMn = (TextBox)dv.FindControl("txtYrMn");
            //HiddenField hdnBankID = (HiddenField)dv.FindControl("hdnBankID");
            //HiddenField HdnGlid = (HiddenField)dv.FindControl("HdnGlid");
            DropDownList ddlInsRecvd = (DropDownList)dv.FindControl("ddlInsRecvd");
            int ddlsector = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);
            TextBox txtInstrumentNo = (TextBox)dv.FindControl("txtInstrumentNo");
            DropDownList ddlOption = (DropDownList)dv.FindControl("ddlOption");

            DataTable dt = DBHandler.GetResult("Get_InstrumentDetail_Pf", txtYrMn.Text, hdnBankID, ddlInsRecvd.SelectedValue, ddlsector, 
                txtInstrumentNo.Text.Equals("") ? DBNull.Value : (object)txtInstrumentNo.Text, ddlOption.SelectedValue,
                HttpContext.Current.Session["Menu_For"].ToString());
            tbl.DataSource = dt;
            tbl.DataBind();

            
            DataTable dtYearMonth = DBHandler.GetResult("GET_GreaterYearMonth_Pf", Convert.ToInt64(txtYrMn.Text), HttpContext.Current.Session["Menu_For"].ToString());
            if (dtYearMonth.Rows.Count > 0)
            {
                tbl.Enabled = false;
            }
            else {  
                tbl.Enabled = true;
            }
            //PopulateDropDown();
            if (dt.Rows.Count == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Msg", "<script>alert('This Record is Not Found.Please Select Proper BankCode.');window.location.href='ACCOUNT_BRS.aspx';</script>");
            }
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "clientscript", "document.getElementById('savebrs').style.display  = 'grid';", false);
            if (txtYrMn.Text == hdnForCurrYrMn.Text)
            {
                tbl.Enabled = true;
            }
            else{
                tbl.Enabled = false;
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Current Period ' + '" + hdnForCurrYrMn.Text + "' + ' !Can Not Change The Data For The Month Of ' + '" + txtYrMn.Text + "')</script>");
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect(ResolveUrl(Page.AppRelativeVirtualPath), false);
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }


    protected void cmdSearch_Click(object sender, EventArgs e)
    {
        try
        {
            PopulateGrid();

        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string[] BankCodeAutoCompleteData(string OLD_SL_ID)
    {
        List<string> result = new List<string>();
        DataTable dtBankData = DBHandler.GetResult("Search_BankIDByCode_Pf", OLD_SL_ID, HttpContext.Current.Session["Menu_For"].ToString());
        foreach (DataRow dtRow in dtBankData.Rows)
        {
         
            result.Add(string.Format("{0}|{1}", dtRow["GL_NAME1"].ToString(), dtRow["SL_ID"].ToString()));
        }
        return result.ToArray();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox txtYrMn = (TextBox)dv.FindControl("txtYrMn");
            TextBox hdnForCurrYrMn = (TextBox)dv.FindControl("hdnForCurrYrMn");
            if (hdnForCurrYrMn.Text == txtYrMn.Text)
            {
                foreach (GridViewRow grdRow in tbl.Rows)
                {
                    Label DD = (Label)(tbl.Rows[grdRow.RowIndex].Cells[0].FindControl("hdnID"));
                    string ddvalue = DD.Text;

                    Label VCHNO = (Label)(tbl.Rows[grdRow.RowIndex].Cells[1].FindControl("hdnVchNo"));
                    string vchnovalue = VCHNO.Text;

                    Label VCHTYPE = (Label)(tbl.Rows[grdRow.RowIndex].Cells[2].FindControl("hdnVchType"));
                    string vchtypevalue = VCHTYPE.Text;

                    DropDownList Sanval = (DropDownList)(tbl.Rows[grdRow.RowIndex].Cells[8].FindControl("ddlCleared"));
                    string clValue = Sanval.SelectedValue.ToString();

                    TextBox AA = (TextBox)(tbl.Rows[grdRow.RowIndex].Cells[9].FindControl("txtClrDt"));
                    string aavalue = AA.Text;
                    DateTime ddclrd = new DateTime();
                    if (aavalue != "")
                    {
                        ddclrd = DateTime.ParseExact(aavalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }

                    TextBox BB = (TextBox)(tbl.Rows[grdRow.RowIndex].Cells[10].FindControl("txtAmtVouched"));
                    string bbvalue = BB.Text;
                    if (bbvalue == "")
                    {
                        bbvalue = "0";
                    }



                    int SecID = Convert.ToInt32(Session[SiteConstants.SSN_SECTOR_ID]);
                    int UserID = Convert.ToInt32(Session[SiteConstants.SSN_INT_USER_ID]);

                    object o = aavalue.Equals("") ? DBNull.Value : (object)ddclrd.ToShortDateString();

                    DataTable dt = DBHandler.GetResult("Update_Account_Brs_Pf", vchnovalue, ddvalue, vchtypevalue, clValue, o,
                                                      decimal.Parse(bbvalue), txtYrMn.Text, UserID, SecID,
                                                      HttpContext.Current.Session["Menu_For"].ToString());

                }
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Record Updated Successfully.')</script>");
                tbl.DataSource = null;
                tbl.DataBind();
                TextBox txtBankSearch1 = (TextBox)dv.FindControl("txtBankSearch");
                txtBankSearch1.Focus();
            }
            else
            {

                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('Current Period ' + '" + hdnForCurrYrMn.Text + "' + ' !Can Not Change The Data For The Month Of ' + '" + txtYrMn.Text + "')</script>");
            }
        }
        catch (Exception ex)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>alert('" + ex.Message.Replace("'", "") + "')</script>");
        }
    }

    [WebMethod]
    public static string GET_SetBankID(int type, string BankID)
    {
        string msg = "";
        if (type == 0) { hdnBankID = ""; }
        else { hdnBankID = BankID; }
        return msg;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static string SetYearMon()
    {
        string JSONVal = "";

        try
        {

            DataTable dtVchDate = DBHandler.GetResult("Get_Year_Month_Pf", 
                Convert.ToInt32(HttpContext.Current.Session[SiteConstants.SSN_SECTOR_ID]),
                HttpContext.Current.Session["Menu_For"].ToString());

            if (dtVchDate.Rows.Count > 0)
            {
                var check = new 
                {
                    YEAR_MONTH = dtVchDate.Rows[0]["YEAR_MONTH"].ToString()
                };
                JSONVal = check.ToJSON();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return JSONVal;

    }


}